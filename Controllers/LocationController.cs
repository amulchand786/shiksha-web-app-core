﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;
namespace Shiksha.Controllers
{
    public class LocationController : Controller
    {
        // GET: Location
        public ActionResult Location()
        {
            return View();
        }
        public ActionResult State()
        {
            return View();
        }
        public ActionResult Zone()
        {
            return View();
        }
        public ActionResult District()
        {
            return View();
        }
        public ActionResult Thesil()
        {
            return View();
        }
        public ActionResult City()
        {
            return View();
        }
        public ActionResult Block()
        {
            return View();
        }
        public ActionResult NyayaPanchayat()
        {
            return View();
        }
        public ActionResult GramPanchayat()
        {
            return View();
        }
        public ActionResult RevenueVillage()
        {
            return View();
        }
        public ActionResult Village()
        {
            return View();
        }
        public ActionResult ReligiousPlace()
        {
            return View();
        }
    }
}