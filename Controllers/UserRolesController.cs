﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;

namespace Shiksha.Controllers
{
    public class UserRolesController : Controller
    {
        // GET: UserRoles
        public ActionResult UserRolesManagement()
        {
            return View();
        }

        public ActionResult User()
        {
            return View();
        }

        public ActionResult Role()
        {
            return View();
        }

        public ActionResult Permission()
        {
            return View();
        }
        public ActionResult RolePermission()
        {
            return View();
        }
    }
}