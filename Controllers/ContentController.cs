﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Shiksha.Models;

namespace Shiksha.Controllers
{
    public class ContentController : Controller
    {
        // GET: Content
        public ActionResult Content()
        {
            return View();
        }

        public ActionResult Management()
        {
            return View();
        }
        public ActionResult Unit()
        {
            return View();
        }
        public ActionResult SegmentType()
        {
            return View();
        }
        public ActionResult Source()
        {
            return View();
        }
        public ActionResult Stage()
        {
            return View();
        }
    }
}