using Microsoft.AspNetCore.Mvc;
using System.Text.Encodings.Web;

namespace Shiksha.Controllers
{
    public class LoginController : Controller
    {
        [HttpGet]
        public ActionResult Login()
        {            
            return View();
        }
        [HttpPost]
        public ActionResult Authenticate()
        {
            //loginServices.Authenticate("");
            return RedirectToAction("Dashboard", "Home");
        }
    }
}