﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNetCore.Mvc;

namespace Shiksha.Controllers
{
    public class SchoolController : Controller
    {
        // GET: School
        public ActionResult School()
        {
            return View();
        }

        public ActionResult ViewSchools()
        {
            return View();
        }

        public ActionResult SchoolConfiguration()
        {
            return View();
        }
    }
}