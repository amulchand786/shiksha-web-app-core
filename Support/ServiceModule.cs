using Autofac;
//using Shiksha.Data.Models;
//using Shiksha.Data.Repository.Infrastructure;
using System;
using System.Collections.Generic;
//using System.Data.Entity;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Shiksha.Support
{
    public class ServiceModule:Autofac.Module
    {
        protected override void Load(ContainerBuilder builder){      
            builder.RegisterAssemblyTypes(Assembly.Load("Shiksha.Business.Services"))
                     .Where(t => t.Name.EndsWith("Services"))
                     .AsImplementedInterfaces()
                     .InstancePerLifetimeScope();
        }
    }
}