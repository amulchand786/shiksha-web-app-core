//global variables
var schoolGradeFormId;
var addFormId;
var editFormId;
var gradeIdList;
var sectionIdList;
var addSectionsIds;
var editSectionsIds;
var editMapperId;
var schoolId;
var mappedGrades;
var mappedSchool;
var deletedGradeId;
var deletedSchoolId;
var deleteSchoolGradeId;
var addButtonId;

var editedRowId; 

var listTableName;
var sectionObj;


//global selectDivIds for smart filter
var schoolGradeSectionSubjectTeacherMappingPageContext = new Object();	
	schoolGradeSectionSubjectTeacherMappingPageContext.city 	="#divSelectCity";
	schoolGradeSectionSubjectTeacherMappingPageContext.block 	="#divSelectBlock";
	schoolGradeSectionSubjectTeacherMappingPageContext.NP 	="#divSelectNP";
	schoolGradeSectionSubjectTeacherMappingPageContext.GP 	="#divSelectGP";
	schoolGradeSectionSubjectTeacherMappingPageContext.RV 	="#divSelectRV";
	schoolGradeSectionSubjectTeacherMappingPageContext.village ="#divSelectVillage";
	schoolGradeSectionSubjectTeacherMappingPageContext.school ="#divSelectSchool";
	
	schoolGradeSectionSubjectTeacherMappingPageContext.schoolLocationType ="#selectBox_schoolLocationType";
	schoolGradeSectionSubjectTeacherMappingPageContext.elementSchool ="#selectBox_schoolsByLoc";
	schoolGradeSectionSubjectTeacherMappingPageContext.grade ="#selectBox_grades";
	schoolGradeSectionSubjectTeacherMappingPageContext.section ="#selectBox_section";
	schoolGradeSectionSubjectTeacherMappingPageContext.subject ="#selectBox_subject";
	schoolGradeSectionSubjectTeacherMappingPageContext.resetLocDivCity ="#divResetLocCity";
	schoolGradeSectionSubjectTeacherMappingPageContext.resetLocDivVillage ="#divResetLocVillage";
	
var addSchGrdSecModalElements =new Object();
	addSchGrdSecModalElements.section ="#mdlAddSchoolGradeSec #selectBox_section";
	addSchGrdSecModalElements.grade ="#mdlAddSchoolGradeSec #selectBox_grades";
	
var editSchGrdSecModalElements =new Object();
	editSchGrdSecModalElements.section ="#mdlEditSchoolGradeSec #selectBox_section";
	editSchGrdSecModalElements.grade ="#mdlEditSchoolGradeSec #selectBox_grades";

	
//initialise global variables
var initGlblVariables =function(){
	schoolGradeFormId ='listSchoolGradeForm';
	addFormId ='addSchoolGradeSecForm';
	editFormId ='editSchoolGradeSecForm';
	gradeIdList=[];schoolId='';
	mappedGrades=[];
	mappedSchool='';
	deletedGradeId='';
	deletedSchoolId='';
	deleteSchoolGradeId='';
	listTableName='schoolGradeTable';
	editedRowId='';
	addSectionsIds =[];editSectionsIds=[];editMapperId='';addButtonId='';
	//get all section on page load
	sectionObj =customAjaxCalling("section/list", null, "GET");
	sectionIdList=[];
	$.each(sectionObj,function(index,obj){
		sectionIdList.push(obj.sectionId);
	});
} 


//////////////////////
//Smart filter		//
//////////////////////

//show or hide div as per school location type
$(schoolGradeSectionSubjectTeacherMappingPageContext.schoolLocationType).change(function(){
	$(".outer-loader").show();
	deleteAllRow("schoolGradeTable");
	var typeCode =$(this).find('option:selected').data('code');
	reinitializeSmartFilter(elementIdMap);	
	$("#hrDiv").show();
	$('#divSchoolGradeSectionSubjectTeacherTable').hide();
	if(typeCode.toLowerCase() =='U'.toLowerCase()){			
		showDiv($(schoolGradeSectionSubjectTeacherMappingPageContext.city),$(schoolGradeSectionSubjectTeacherMappingPageContext.resetLocDivCity));
		hideDiv($(schoolGradeSectionSubjectTeacherMappingPageContext.block),$(schoolGradeSectionSubjectTeacherMappingPageContext.NP),$(schoolGradeSectionSubjectTeacherMappingPageContext.GP),$(schoolGradeSectionSubjectTeacherMappingPageContext.RV),$(schoolGradeSectionSubjectTeacherMappingPageContext.village),$(schoolGradeSectionSubjectTeacherMappingPageContext.resetLocDivVillage));
		displaySmartFilters();
		positionFilterDiv("U");
	}else{
		hideDiv($(schoolGradeSectionSubjectTeacherMappingPageContext.city),$(schoolGradeSectionSubjectTeacherMappingPageContext.resetLocDivCity));
		showDiv($(schoolGradeSectionSubjectTeacherMappingPageContext.block),$(schoolGradeSectionSubjectTeacherMappingPageContext.NP),$(schoolGradeSectionSubjectTeacherMappingPageContext.GP),$(schoolGradeSectionSubjectTeacherMappingPageContext.RV),$(schoolGradeSectionSubjectTeacherMappingPageContext.village),$(schoolGradeSectionSubjectTeacherMappingPageContext.resetLocDivVillage));
		displaySmartFilters();
		positionFilterDiv("R");
	}
});


//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionFilterDiv =function(type){
	if(type=="R"){		
		$(schoolGradeSectionSubjectTeacherMappingPageContext.NP).insertAfter( $(schoolGradeSectionSubjectTeacherMappingPageContext.block));
		$(schoolGradeSectionSubjectTeacherMappingPageContext.RV).insertAfter( $(schoolGradeSectionSubjectTeacherMappingPageContext.GP));
		$(schoolGradeSectionSubjectTeacherMappingPageContext.school).insertAfter( $(schoolGradeSectionSubjectTeacherMappingPageContext.village));
	}else{
		$(schoolGradeSectionSubjectTeacherMappingPageContext.school).insertAfter( $(schoolGradeSectionSubjectTeacherMappingPageContext.city));
	}
}
//displaying smart-filters
var displaySmartFilters =function(){
	$(".outer-loader").show();

	elementIdMap = {
				"State" : '#statesForZone',
				"Zone" : '#selectBox_zonesByState',
				"District" : '#selectBox_districtsByZone',
				"Tehsil" : '#selectBox_tehsilsByDistrict',
				"Block" : '#selectBox_blocksByTehsil',
				"City" : '#selectBox_cityByBlock',
				"Nyaya Panchayat" : '#selectBox_npByBlock',
				"Gram Panchayat" : '#selectBox_gpByNp',
				"Revenue Village" : '#selectBox_revenueVillagebyGp',
				"Village" : '#selectBox_villagebyRv',
				"School" : '#selectBox_schoolsByLoc'
	};
	displayLocationsOfSurvey($(schoolGradeSectionSubjectTeacherMappingPageContext.schoolLocationType), $('#divFilter'));

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){$(".outer-loader").hide(); $('#rowDiv1,#divFilter').show(); }, 100);	
};


//reset only location when click on reset button icon
var resetLocAndSchool =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
	//$('#divSchoolGradeSectionSubjectTeacherTable').hide();
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	$('#divSchoolGradeSectionSubjectTeacherTable').hide();
	deleteAllRow("schoolGradeTable");
	if($(resetButtonObj).parents("form").attr("id")=="listSchoolGradeForm"){		
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters();
	}
}


//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
		
	});	
}
//////////////////////
//Smart filter ended//
//////////////////////

//////////////////////
//Operations--CRUD	//
//////////////////////

var schoolName;
//show table as per school
$(schoolGradeSectionSubjectTeacherMappingPageContext.elementSchool).change(function(){
	$(".outer-loader").show();
	deleteAllRow(listTableName);	
	var schoolId =$(this).find('option:selected').data('id');
	schoolName =$(this).find('option:selected').data('name');
	var schoolGradeData =customAjaxCalling("getTeacherSubjectMappingBySchoolId/"+schoolId, null, "GET");
	if(schoolGradeData!=null && schoolGradeData!=""){
		bindGradesBySchoolToListBox($(schoolGradeSectionSubjectTeacherMappingPageContext.grade),schoolId);
		setOptionsForMultipleSelect($(schoolGradeSectionSubjectTeacherMappingPageContext.grade));		
		refreshMultiselect($(schoolGradeSectionSubjectTeacherMappingPageContext.grade));
		setOptionsForMultipleSelect($(schoolGradeSectionSubjectTeacherMappingPageContext.grade));
		$('#divSchoolGradeSectionSubjectTeacherTable').show();
		createSubjectTeacherRow(schoolGradeData);
		
	
	}	
	$(".outer-loader").hide();
});

var createSubjectTeacherRow = function(data) {
	var toatlTeacher=0;
	var rowHtml = "";
	var headers = [ "" ];
	var teacherLoopCount = 0;
	var datastring = '';
	
	
	$.each(data, function(index, teacher) {
		toatlTeacher=data[index].length;
    if(data[index].length>0){
    	 headers = [ "" ];
    	for(i=0;i<toatlTeacher;i++){
    		headers.push(teacher[i].name);
    		
    	}
    	$.each(teacher, function(index, teacherData) {
			rowHtml = '<thead><tr><th> Grade-Section-Subject / Teacher'
					+ headers.join('</th><th>') + '</th></tr></thead>';
			
		});
    	
		/*if (teacherLoopCount < teacher.length) {
			headers.push(teacher[teacherLoopCount].name)
            teacherLoopCount++;
		}
		$.each(teacher, function(index, teacherData) {
			rowHtml = '<thead><tr><th> Grade-Section-Subject / Teacher'
					+ headers.join('</th><th>') + '</th></tr></thead>';
			
		});*/
    }
	});
	var rows = {};
	var rowId=0;
	$.each(data, function(index, teacherList) {
		if(data[index].length>0){
			rowId++;
			
		rowHtml += '<tr data-id="'+rowId+'"><td>' + escapeHtmlCharacters(index) + '</td>';
		$.each(teacherList, function(indeX, teacherListItem) {
			var isMappedToSubject=(teacherListItem.isMappedToSubject==true?'checked="true"':'');
			//console.log(isMappedToSubject)
			var checkBoxId=rowId+"_"+teacherListItem.userId;
			var datastring = 'data-name="' + escapeHtmlCharacters(teacherListItem.name)
					+ '" data-userId ="' + teacherListItem.userId
				
					+ '" data-isMappedToSubject ="' + teacherListItem.isMappedToSubject
					+ '" data-schoolGradeSectionMapperId ="' + teacherListItem.schoolGradeSectionMapperId
					+ '" data-subjectId ="' + teacherListItem.subjectId
					+ '" data-sectionId ="' + teacherListItem.sectionId
					+ '" data-schoolId ="' + teacherListItem.schoolId
					+ '" data-checked ="' + teacherListItem.isMappedToSubject
					+ '" data-gradeId ="' + teacherListItem.gradeId + '"';
			rowHtml += '<td >' + '<input class="subjectTeacherMapping" onchange="teacherSubjectMapping('
			
			+teacherListItem.gradeId+','
			+teacherListItem.userId+','
			+teacherListItem.subjectId+','
			+teacherListItem.sectionId+','
			+teacherListItem.schoolId+','
			+teacherListItem.schoolGradeSectionMapperId+','
			+teacherListItem.isMappedToSubject+','
			+"'"+checkBoxId+"'"
			
			+')" type="checkbox" name="teacher" id="'+checkBoxId+'" '+isMappedToSubject
					+ datastring + '>' + '</td>';
			

		});
	}
	});
	if(toatlTeacher>0){
		$('#SchoolGradeSectionSubjectTeacherTable').show();
	$('#schoolGradeSectionSubjectTeacherTable').html(rowHtml)
	}else{
		$('#divSchoolGradeSectionSubjectTeacherTable').hide();
		console.log("88888888888");
		$('#msgText').text("Selected school have no teacher ");
		
		$('#mdlError').modal();
		
	}
	
}
var gradeId=0;
var userId=0;
var subjectId=0;
var sectionId=0;
var schoolId=0;
var schoolGradeSectionMapperId=0;
var isMapped=false;
var chkBoxId="";
// show Modal on edit or delete mapping
var teacherSubjectMapping=function(gradeID,userID,subjectID,sectionID,schoolID,schoolGradeSectionMapperID,isMappedToSubject,checkBoxId) {
	gradeId=gradeID;
	userId=userID;
	subjectId=subjectID;
	sectionId=sectionID;
	schoolId=schoolID;
	isCheckedFlag =isMappedToSubject;
	schoolGradeSectionMapperId=schoolGradeSectionMapperID;
	//isMapped=isMappedToSubject;
	//isMapped = $("#"+chkBoxId).is(":checked");
	chkBoxId=checkBoxId;
	
	isMapped = $("#"+chkBoxId).data('checked');	
	$("#teacherSubjectMappingModal").modal();
	 
	 
}
// on cancel preserve setting
$("#cancelButton").on('click',function() {	
	// var isCheckedFlag=this.checked;
	if(isMapped==true){
		$("#"+chkBoxId).prop("checked",true);
	}else{
		$("#"+chkBoxId).prop("checked",false);
	}
	$("#teacherSubjectMappingModal").modal('hide');
	
});
// issue 576 start
$("#crossBtn").on('click',function() {	
	// var isCheckedFlag=this.checked;
	if(isMapped==true){
		$("#"+chkBoxId).prop("checked",true);
	}else{
		$("#"+chkBoxId).prop("checked",false);
	}
	$("#teacherSubjectMappingModal").modal('hide');
	
});
// on apply add or delete mapping
$("#applyButton").click(function() {
	checkedValue = $("#"+chkBoxId).is(":checked");
	jsonData={
			'gradeId':gradeId,
			'userId':userId,
			'subjectId':subjectId,
			'sectionId':sectionId,
			'schoolId':schoolId,
			'schoolGradeSectionMapperId':schoolGradeSectionMapperId,
	} 
	$(".outer-loader").show();
	if(checkedValue==true){
		addSubjectTeacherMapping(jsonData);
		$(".outer-loader").hide();
	}  else{
		deleteSubjectTeacherMapping(jsonData); 
		
		$(".outer-loader").hide();
	}

});

var addSubjectTeacherMapping= function(data){
	var subjectTeacherMappingResponse =customAjaxCalling("schoolGradeSectionSubjectTeacherMapping",data,'POST');
	$("#"+chkBoxId).data('checked',checkedValue);
	
}

var deleteSubjectTeacherMapping= function(data){
	var subjectTeacherMappingResponse =customAjaxCalling("schoolGradeSectionSubjectTeacherMapping",data,'DELETE');
	$("#"+chkBoxId).data('checked',checkedValue);
}

/*var getNewRow =function(schoolGradeData){
	var newRow =
		"<tr id='"+schoolGradeData.schoolGradeSectionMapperId+"' data-id='"+schoolGradeData.schoolGradeSectionMapperId+"' data-schoolid='"+schoolGradeData.schoolID+"' data-gradeid='"+schoolGradeData.gradeVm.gradeId+"'  data-secids='"+schoolGradeData.sectionIds+"'data-isdelete_able='"+schoolGradeData.isDeleteable+"'>"+
			"<td></td>"+			
			"<td data-gradeid='"+schoolGradeData.gradeVm.gradeId+"'	title='"+escapeHtmlCharacters(schoolGradeData.gradeVm.gradeName)+"'>"+schoolGradeData.gradeVm.gradeName+"</td>"+
			"<td data-gradeid='"+schoolGradeData.gradeVm.gradeId+"'	title='"+escapeHtmlCharacters(schoolGradeData.gradeVm.gradeCode)+"'>"+schoolGradeData.gradeVm.gradeCode+"</td>"+
			"<td data-sectionids='"+schoolGradeData.sectionIds+"'	title='"+escapeHtmlCharacters(schoolGradeData.sectionNames)+"'>"+schoolGradeData.sectionNames+"</td>"+
			"<td><div class='btn-group'><button type='button' class='btn btn-link' style='display: block;' id='editSchoolGrade"+schoolGradeData.schoolGradeSectionMapperId+"'onclick=editSchoolGradeSetData(&quot;"+schoolGradeData.schoolGradeSectionMapperId+"&quot;,&quot;"+schoolGradeData.gradeVm.gradeId+"&quot;,&quot;"+schoolGradeData.sectionIds+"&quot;);><span class='glyphicon glyphicon-edit'></span>Edit</button><button type='button' class='btn btn-link'  style='display: block;' data-toggle='modal' data-target='#mdlDelSchoolGrade' id='deleteSchoolGrade"+schoolGradeData.schoolGradeSectionMapperId+"'	onclick=deleteSchoolGradeSetData(&quot;"+schoolGradeData.schoolGradeSectionMapperId+"&quot;);><span class='glyphicon glyphicon-remove'></span>Delete</button></div></td>"+
		"</tr>";
		return newRow;
};*/

//bindGrades and sections  to list box on add modal show
/*var getAllGrades =function(obj){	
	resetFormValidatonForLoc(addFormId,"grades");
	var isSchoolselected =$('#selectBox_schoolsByLoc').find('option:selected');
	var isSchoolLocselected =$('#selectBox_schoolLocationType').find('option:selected').val();
	if(isSchoolselected==null || isSchoolselected.length==0){
		if(isSchoolLocselected=="NONE"){$('#msgText').text('');$('#msgText').text('Please select school location and school.')}else{$('#msgText').text('');$('#msgText').text('Please select school.')}
		$('#mdlError').modal("show");
		return false;
	}else{
		var noOfRows =$('#schoolGradeTable').dataTable().fnSettings().fnRecordsTotal();
		var noOfGrades =$('#mdlAddSchoolGradeSec #selectBox_grades option').length-1;
		if(noOfRows==noOfGrades){
			$('#msgText').text('');$('#msgText').text('All grades have been added for selected school.')
			$('#mdlAddSchoolGradeSec').modal("hide");
			$('#mdlError').modal("show");
			return false;
		}else{
			$('#mdlAddSchoolGradeSec').modal("show");
		}
	}
	$(addSchGrdSecModalElements.grade).multiselect('destroy');
	bindGradeToListBox($(addSchGrdSecModalElements.grade),0);
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.grade));		
	refreshMultiselect($(addSchGrdSecModalElements.grade));
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.grade));
	
	//add sections
	$(addSchGrdSecModalElements.section).multiselect('destroy');
	bindSectionToListBox($(addSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.section));		
	refreshMultiselect($(addSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.section));
	
	//disable grade if already added for this school
	disableGrade(addSchGrdSecModalElements.grade);
}
*/
//disable grade in select box if already added for this school
/*var disableGrade =function(elementGrade){
	mappedGrades =[];gradeIdList=[];var i=0;
	$('#schoolGradeTable >tbody>tr').each(function(i,row){		
		mappedGrades.push( parseInt(row.getAttribute('data-gradeid')));
	});
	$(elementGrade).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			gradeIdList.push( parseInt($(option).data('id')));
		});
	});	
	var mappedSchool =$('#selectBox_schoolsByLoc').find('option:selected').data("id");
	
	$.grep(gradeIdList, function(el) {
		if ($.inArray(el, mappedGrades) != -1){
			var option = $(elementGrade).find('option[value="' +el+ '"]');
			option.prop('disabled', true);
			option.parent('label').parent('a').parent('li').remove();
		}
		i++;
	});
	$(elementGrade).multiselect('refresh');
}*/

//add grades
/*var addSchoolGrade =function(event){
	$(".outer-loader").show();
	gradeIdList=[];
	gradeId='';schoolId='';addSectionsIds=[];
	$(addSchGrdSecModalElements.grade).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				gradeIdList.push($(option).data('id'));
		});
	});
	
	$(addSchGrdSecModalElements.section).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				addSectionsIds.push($(option).data('id'));
		});
	});	
	gradeId =$('#selectBox_grades').find('option:selected').data("id");
	schoolId =$('#selectBox_schoolsByLoc').find('option:selected').data("id");
	if(addSectionsIds.length==0) addSectionsIds=null;
	var requestData ={
		"schoolID":schoolId,
		"gradeId":gradeId,
		"sectionIds":addSectionsIds
	};
	
	var responseData =customAjaxCalling("schoolgradesectionmapper", requestData, "POST");
	if(responseData!=null && responseData!=""){
		if(responseData[0].response=="shiksha-200"){
			$(responseData).each(function(index,dataObj){
				appendNewRow('schoolGradeTable',getNewRow(dataObj));
			});
			addButtonId =$(event.target).data('formValidation').getSubmitButton().data('id');
			if(addButtonId=="btnAddSchoolGradeSecSaveMoreButton"){
				getAllGrades(this);
			}else{			
				$('#mdlAddSchoolGradeSec').modal("hide");
			}
			$(".outer-loader").hide();	
		}else{
			$(".outer-loader").hide();
			$("#addSchoolGradeSecForm").find('#alertDiv').show();
			$("#addSchoolGradeSecForm").find('#successMessage').hide();		
			$("#addSchoolGradeSecForm").find('#errorMessage').show();
			$("#addSchoolGradeSecForm").find('#exception').show();
			$("#addSchoolGradeSecForm").find('#buttonGroup').show();
			$("#addSchoolGradeSecForm").find('#exception').text(responseData[0].responseMessage);
		}
	}
	$(window).scrollTop(0);
}*/



//edit school grade section
/*var editSchoolGradeSetData =function(rowId,gradeId,sectionIds){ 
	resetFormValidatonForLoc(editFormId,"grades");
	editMapperId=rowId;
	
	$(editSchGrdSecModalElements.grade).multiselect('destroy');
	bindGradeToListBox($(editSchGrdSecModalElements.grade),0);
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.grade));		
	refreshMultiselect($(editSchGrdSecModalElements.grade));
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.grade));
	
	//add sections
	$(editSchGrdSecModalElements.section).multiselect('destroy');
	bindSectionToListBox($(editSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.section));		
	refreshMultiselect($(editSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.section));
	
	//disable grade if already added for this school
	removeGrade(gradeId,editSchGrdSecModalElements.grade);
	makeSectionSelected(sectionIds,$(editSchGrdSecModalElements.section));
    
	$('#mdlEditSchoolGradeSec').modal("show");
}*/
//save edited data
/*var editSchoolGradeSection =function(){
	$(".outer-loader").show();
	editSectionsIds=[];
		
	$(editSchGrdSecModalElements.section).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				editSectionsIds.push($(option).data('id'));
		});
	});
	if(editSectionsIds.length==0) editSectionsIds=null;
	var requestData ={
		"schoolGradeSectionMapperId":editMapperId,
		"sectionIds":editSectionsIds
	};

	var responseData =customAjaxCalling("schoolgradesectionmapper", requestData, "PUT");
	if(responseData!=null && responseData!=""){
		if(responseData.response=="shiksha-200"){
			deleteRow("schoolGradeTable",editMapperId);	
			appendNewRow('schoolGradeTable',getNewRow(responseData));
			$('#mdlEditSchoolGradeSec').modal("hide");
			$(".outer-loader").hide();	
		}else{			
			$("#editSchoolGradeSecForm").find('#alertDiv').show();
			$("#editSchoolGradeSecForm").find('#successMessage').hide();		
			$("#editSchoolGradeSecForm").find('#errorMessage').show();
			$("#editSchoolGradeSecForm").find('#exception').show();
			$("#editSchoolGradeSecForm").find('#buttonGroup').show();
			$("#editSchoolGradeSecForm").find('#exception').text('');
			$("#editSchoolGradeSecForm").find('#exception').text(responseData.responseMessage);
			$(".outer-loader").hide();
		}
	}
	$(window).scrollTop(0);
	
}*/

//remove all grades except current edited grade in edit form
/*var removeGrade = function(selectedGrade,elementGrade){
	$(elementGrade).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			if($(option).data('id')!=null){
				var currentId=parseInt($(option).data('id'));
				if(currentId==parseInt(selectedGrade)){
					$(option).prop('selected', true);
				}else{
					$(option).prop('disabled', true);
					$(option).parent('label').parent('a').parent('li').remove();
				}
			}
		});

	});		
	$(elementGrade).multiselect('refresh');
};

//make sections seleted in edit form
var makeSectionSelected =function(selectedIds,elementSec){
	var i=0;
	selectedIds = $.map(selectedIds.split(","), function(el) { return parseInt(el, 10); });
	$.grep(sectionIdList, function(el) {
		if ($.inArray(el, selectedIds) != -1){
			var option = $(elementSec).find('option[value="' +el+ '"]');
			option.prop('selected', true);
		}
		i++;
	});
	$(elementSec).multiselect('refresh');
}







//delete grade from school
var deleteSchoolGradeSetData =function(schoolGradeMappingId){
	deleteSchoolGradeId =schoolGradeMappingId;
	
	var schName =$('#selectBox_schoolsByLoc').find('option:selected').text();
	$('#mdlDelSchoolGrade #msgDel').text('');
	$('#mdlDelSchoolGrade #msgDel').text('Do you want to remove selected grade from school "' +schName +'"?');
	hideDiv($('#mdlDelSchoolGrade #alertdiv'));
}
var deleteGrade =function(obj){
	$(".outer-loader").show();
	
	var responseData = customAjaxCalling("schoolgradesectionmapper/"+deleteSchoolGradeId, null, "DELETE");
	if(responseData.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("schoolGradeTable",deleteSchoolGradeId);		
		$('#mdlDelSchoolGrade').modal("hide");
	} else {
		showDiv($('#mdlDelSchoolGrade #alertdiv'));
		$("#mdlDelSchoolGrade").find('#errorMessage').show();
		$("#mdlDelSchoolGrade").find('#exception').text('');
		$("#mdlDelSchoolGrade").find('#exception').text(responseData.responseMessage);
		$("#mdlDelSchoolGrade").find('#buttonGroup').show();
	}
	$(".outer-loader").hide();
	$(window).scrollTop(0);
}

*/

///////////////////////////
$(function() {
	
	setDataTablePagination("#schoolGradeTable");
    
    setOptionsForMultipleSelect($(schoolGradeSectionSubjectTeacherMappingPageContext.schoolLocationType));
    setOptionsForMultipleSelect($(addSchGrdSecModalElements.section));
    $('#rowDiv1').show();
	initGlblVariables();

	//init bootstrap tooltip
	$('[data-toggle="tooltip"]').tooltip(); 

	/*$("#addSchoolGradeSecForm").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields: {
			grades: {
				validators: {
					callback: {
						message: '      ',
						callback: function(value, validator, $field) {
							// Get the selected options
							var options =$(addSchGrdSecModalElements.grade).val();
							return (options !=null);
						}
					}
				}
			},			
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addSchoolGrade(e);
	});
	
	
	
	$("#editSchoolGradeSecForm").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields: {
			grades: {
				validators: {
					callback: {
						message: '      ',
						callback: function(value, validator, $field) {
							// Get the selected options
							var options =$(editSchGrdSecModalElements.grade).val();
							return (options !=null);
						}
					}
				}
			},			
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		editSchoolGradeSection(e);
	});
*/
});