var pageContextElements = {
		form					: '#bulkUploadStudentForm',
		uploadBtn				:'#uploadBtn',
		uploadFileDiv           :"#uploadFile",
		inputFile               :"#exampleInputFile",
		cancelButton			:'#cancelButton',
		downloadButton			:'#downloadButton'
};


var selectBox ={
		school		: '#selectBox_schoolsByLoc',
		grade		: '#selectBox_grade',
		section		: '#selectBox_Section',
		academicYear: '#selectBox_AcademicYear'
		
};
var uploadStudentPageContext = {};
	uploadStudentPageContext.city = "#divSelectCity";
	uploadStudentPageContext.block = "#divSelectBlock";
	uploadStudentPageContext.NP = "#divSelectNP";
	uploadStudentPageContext.GP = "#divSelectGP";
	uploadStudentPageContext.RV = "#divSelectRV";
	uploadStudentPageContext.village = "#divSelectVillage";
	uploadStudentPageContext.school = "#divSelectSchool";
	
	uploadStudentPageContext.schoolLocationType = "#selectBox_schoolLocationType";
	uploadStudentPageContext.elementSchool = "#selectBox_schoolsByLoc";
	uploadStudentPageContext.grade = "#selectBox_grades";
	
	uploadStudentPageContext.resetLocDivCity = "#divResetLocCity";
	uploadStudentPageContext.resetLocDivVillage = "#divResetLocVillage";
	
var elementIdMap;
var resetButtonObj;
	//hide grade,section,upload file div
	var fnHideGrSecFile =function(){
		$('#divSelectGrade').hide();
		$('#divSelectSection').hide();
		$('#divSelectAcademicYear').hide();
		$('#uploadFile').hide();
		$('#successMessageDiv').hide();//hide success msg
		$('#errorMsg').hide(); //hide error msg
		$('#failureMessageDiv').hide();
	}	
	
	// remove and reinitialize smart filter
	var reinitializeSmartFilter = function(eleMap) {
		var ele_keys = Object.keys(eleMap);
		$(ele_keys).each(function(ind, ele_key) {
			$(eleMap[ele_key]).find("option:selected").prop('selected', false);
			$(eleMap[ele_key]).find("option[value]").remove();
			$(eleMap[ele_key]).multiselect('destroy');
			enableMultiSelect(eleMap[ele_key]);
		});
	}
	
	// displaying smart-filters
	var displaySmartFilters = function(lCode,lLevelName) {
		$(".outer-loader").show();
		elementIdMap = {
			"State" : '#statesForZone',
			"Zone" : '#selectBox_zonesByState',
			"District" : '#selectBox_districtsByZone',
			"Tehsil" : '#selectBox_tehsilsByDistrict',
			"Block" : '#selectBox_blocksByTehsil',
			"City" : '#selectBox_cityByBlock',
			"Nyaya Panchayat" : '#selectBox_npByBlock',
			"Gram Panchayat" : '#selectBox_gpByNp',
			"Revenue Village" : '#selectBox_revenueVillagebyGp',
			"Village" : '#selectBox_villagebyRv',
			"School" : '#selectBox_schoolsByLoc'
		};
		displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));

		var ele_keys = Object.keys(elementIdMap);
		$(ele_keys).each(function(ind, ele_key) {
			$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
			$(elementIdMap[ele_key]).multiselect('destroy');
			enableMultiSelect(elementIdMap[ele_key]);
		});
		setTimeout(function() {
			$(".outer-loader").hide();
			$('#rowDiv1,#divFilter').show();
		}, 100);
	};
	
//show or hide locations filter
//after UI chanaged......
$('input:radio[name=locType]').change(function() {		
		
		fnHideGrSecFile();
		$('#locColspFilter').removeClass('in');
		$('#locationPnlLink').text('Select Location');
		var typeCode =$(this).data('code');
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
		
		reinitializeSmartFilter(elementIdMap);
		$("#hrDiv").show();
		if (typeCode.toLowerCase() == 'U'.toLowerCase()) {
			showDiv($(uploadStudentPageContext.city),$(uploadStudentPageContext.resetLocDivCity));
			hideDiv($(uploadStudentPageContext.block),$(uploadStudentPageContext.NP),$(uploadStudentPageContext.GP),
					$(uploadStudentPageContext.RV),$(uploadStudentPageContext.village),$(uploadStudentPageContext.resetLocDivVillage));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			fnCollapseMultiselect();
		} else {
			hideDiv($(uploadStudentPageContext.city),$(uploadStudentPageContext.resetLocDivCity));
			showDiv($(uploadStudentPageContext.block),$(uploadStudentPageContext.NP),$(uploadStudentPageContext.GP),
					$(uploadStudentPageContext.RV),$(uploadStudentPageContext.village),$(uploadStudentPageContext.resetLocDivVillage));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			fnCollapseMultiselect();
		}
		$('.outer-loader').hide();
	});



var positionFilterDiv = function(type) {
	if (type == "R") {
		$(uploadStudentPageContext.NP).insertAfter($(uploadStudentPageContext.block));
		$(uploadStudentPageContext.RV).insertAfter($(uploadStudentPageContext.GP));
		$(uploadStudentPageContext.school).insertAfter($(uploadStudentPageContext.village));
	} else {
		$(uploadStudentPageContext.school).insertAfter($(uploadStudentPageContext.city));
	}
}


// reset only location when click on reset button icon
var resetLocAndSchool = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;
}
// invoking on click of resetLocation confirmation dialog box
var doResetLocation = function() {
	
	hideDiv($("#divSelectGrade"),$("#divSelectSection"));
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	//it is for smart filter utility..SHK-369
	$(pageContextElements.inputFile).val('');
	uploadStudent.resetForm(pageContextElements.form);
	$('#errorMsg').hide();
	if ($(resetButtonObj).parents("form").attr("id") == "bulkUploadStudentForm") {
		reinitializeSmartFilter(elementIdMap);
		var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
		var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
		displaySmartFilters(aLocTypeCode,aLLevelName);
		fnHideGrSecFile();
	}
	fnCollapseMultiselect();
}

var fnBindGradesToListBox =function(bEleId,gData){
	if(gData!=null || gData!=""){
	var	$select	=bEleId;
		$select.html('');
		$.each(gData,function(index,obj){
			$select.append('<option data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '">' +obj.gradeName+ '</option>');
		});
	}
}

//@Debendra
//customize the multiple select box
var fnSetMultipleSelect = function(ele){	
	$(ele).multiselect({
		maxHeight: 100,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,	
		filterPlaceholder: 'Search here...',
		onChange : function() {
			if (ele == '#selectBox_Section') {
				$('#uploadFile').show();
			}
		}
	});	
}

///////////////////////////////////////////////////////////////////

var getGradeList = function() {
	$('.outer-loader').show();
	$('#divSelectGrade').hide();
	$('#divSelectSection').hide();
	$('#divSelectAcademicYear').hide();
	var aSchoolId = $("#selectBox_schoolsByLoc").val();
	var bindToEleId = $('#selectBox_grade');
	var aGrades = customAjaxCalling("gradelist/"+aSchoolId,null,"GET");
	if(aGrades.length>0){
		fnBindGradesToListBox(bindToEleId,aGrades);
		fnSetMultipleSelect('#selectBox_grade');
		$('#divSelectGrade').show();
	}else{
		fnHideGrSecFile();
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no grades for the selected school.");
		$('#mdlError').modal().show();
	}
	fnCollapseMultiselect();
	$('.outer-loader').hide();
	return;
}

var fnBindSectionsToListBox =function(cEleId,secData){
	if(secData!=null || secData!=""){
	var	$select	=cEleId;
		$select.html('');
		$.each(secData,function(index,obj){
			$select.append('<option data-id="' + obj.sectionId + '"data-sectionName="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
		});
	}
}

var getSectionList = function(){
	$('.outer-loader').show();
	$('#divSelectSection').hide();
	$('#divSelectAcademicYear').hide();
	$('#uploadFile').hide();
	$('#selectBox_Section').find("option:selected").prop('selected', false);
	$('#selectBox_Section').multiselect('refresh');
	$('#selectBox_Section').multiselect('destroy');
	 
	
	var aoSchoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var aoGradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	
	var aBindToEleId = $('#selectBox_Section');
	var aRequestData ={
			"schoolId": aoSchoolId,
			"gradeId": aoGradeId
	}
	var aSections = customAjaxCalling("sectionlist", aRequestData,"POST");
	if(aSections.length>0){
		fnBindSectionsToListBox(aBindToEleId,aSections);
		fnSetMultipleSelect('#selectBox_Section');
		$('#divSelectSection').show();
		
		
	}else{
		$('#divSelectSection').hide();
		$('#divSelectAcademicYear').hide();
		$('#uploadFile').hide();
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no sections for the selected school and grade.");
		$('#mdlError').modal().show();
	}
	fnCollapseMultiselect();
	$('.outer-loader').hide();
	return;
}

var uploadStudent={
		formValidate : function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					uploadfile: {
						required: true,
						fileFormat:true
					},
				},
				messages: {
					uploadfile: {
						required:uploadMessages.fileRequired,
						fileFormat:uploadMessages.fileFormat.replace("@EXTENSION", ".xlsx"),
					},

				},
				errorPlacement: function(error, element) {
					$(error).insertAfter(element);
				},
				submitHandler : function(){
					uploadStudent.submitUploadFile($(pageContextElements.inputFile)[0].files[0]);
				}
			});
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			
		}, 
		submitUploadFile : function(file){

			var formData = new FormData();
			formData.append( 'uploadfile',file );
			formData.append( 'school',$(selectBox.school).find('option:selected').data('id') );
			formData.append( 'grade',$(selectBox.grade).find('option:selected').data('id') );
			formData.append( 'section',$(selectBox.section).find('option:selected').data('id') );
			formData.append( 'academicYear',$(selectBox.academicYear).val() );
			spinner.showSpinner();
			$.ajax({
				url: "uploadStudent",
				type: "POST",
				data: formData,
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				success: function (response) {
					spinner.hideSpinner();
					if(response.response == "shiksha-200"){
						$(pageContextElements.inputFile).val('');
						AJS.flag({
							type  : "success",
							title : "Success!",
							body  : response.responseMessage,
							close : 'auto'
						});		

					} else {
						if(response.responseMessages!=null?response.responseMessages.length>0:false){
							var item="";
							$.each(response.responseMessages,function(index,value){
								var num=index+1;
								  item=item+num+'.'+value+'<br>';
								 						
							});
							AJS.flag({
								type  : "error",
								title : "Error..!",
								body  : response.responseMessage+"<br> "+item,
								close : 'auto'
							});
							
						}else{
							uploadStudent.fnShowError(response.responseMessage);
						}
						

					}
				},
				error: function (error) {
					spinner.hideSpinner();
					console.log(error);
					uploadStudent.fnShowError(appMessgaes.serverError);

				}
			});
		},
	
		fnCancel:function(){
			$(pageContextElements.inputFile).val('');
			uploadStudent.resetForm(pageContextElements.form);
		},
		fnShowError:function(message){
			AJS.flag({
				type  : "error",
				title : "Error..!",
				body  : message,
				close : 'auto'
			});
		},	
}
 






var gradeAndSectionHide = function(){
	$('#divSelectGrade').hide();
	$('#divSelectSection').hide();
	$('#divSelectAcademicYear').hide();
	$('#uploadFile').hide();
}

var applyPermissions=function(){
	if (!editStudentPermission){
		$("#btnEditUser").remove();
	}
	if (!deleteStudentPermission){
		$("#deleteStudent").remove();
	}

}


/*$("#uploadBtn").click(function(){
	var filename=	$('#exampleInputFile').val();
	var file_size = $('#exampleInputFile')[0].files[0].size;
	 Check input file extension. If different from “.xls” or “.xlsx” display error message 
	var fileExtensionName = filename.split('.').pop();
	if (fileExtensionName!="xlsx") {
		$('#errorMsg').show();
		
		return false;
	}else if(file_size>3072000) {
		$('#alertIdForClientValidate,#alertIdForClientValidate .alert').show();
		return false;
	} 
	if(filename.length>0){
		$(".outer-loader").show(); 
		return true;
	}
});*/
$('#cancelButton').click(function () {
	$("#exampleInputFile").val('');
	uploadStudent.resetForm(pageContextElements.form);
	$('#errorMsg').hide(); //hide error msg
});




var getSelected = function() {
	$("#uploadFile").css("display","none");
	var selectedanswer = document.getElementById("selectBox_Section").selectedIndex;
	if (selectedanswer != null) {
		fnSetMultipleSelect($('#selectBox_AcademicYear'));
		$("#divSelectAcademicYear").show();
	}
};
var fnShowUploadFileDiv = function() {
	$("#uploadFile").css("display","block");
	
};




var fnDownloadStudentTemplate=function(){
	var villageName=$('#selectBox_villagebyRv').find('option:selected').text();
	var schoolName=$('#selectBox_schoolsByLoc').find('option:selected').text();
	var gradeName=$('#selectBox_grade').find('option:selected').text();
	var sectionName=$('#selectBox_Section').find('option:selected').text();

	var excelFileName=villageName+"_"+schoolName+"_"+gradeName+"_"+sectionName+".xlsx";
	
	
	window.location.href=baseContextPath+'/student/download?excelFileName='+excelFileName;
}





$(function() {	
	
	$(window).bind("pageshow", function() {
	    var form = $('form'); 
	    form[0].reset();
	});
	fnSetMultipleSelect($('#selectBox_grade'));
	//
	$('#rowDiv1').show();	
	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red").removeClass("glyphicon-plus-sign").removeClass("green");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
		
	});  
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');

		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red").removeClass("glyphicon-plus-sign").removeClass("green");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	});
	
	uploadStudent.formValidate(pageContextElements.form);

	$('input[name=uploadfile]').change(function() {
		uploadStudent.resetForm(pageContextElements.form);
	   
	});
	jQuery.validator.addMethod("fileFormat", function() {

    	var filename=	$(pageContextElements.inputFile).val();
    	var fileExtensionName = filename.split('.').pop();
		if (fileExtensionName!="xlsx") {
			return false;
		}
		return true;
	 
	}, "");
});
