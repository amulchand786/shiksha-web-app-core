
var ell;
var roleId=0;
var permissionId=0;
var permission = function(aRoleId, aPermissionId, permissionName, roleName,
		element,chkBoxId) {
	$("#role_per #errorMessageAdd").hide();
	$("#role_per #errorMessageDelete").hide();

	roleId=aRoleId;
	permissionId=aPermissionId;

	ell = element;

	if ($(element).is(":checked")) {
		$('#privilegeChange').text(columnMessages.addPrivilege.replace("@Permission",permissionName).replace("@Role",roleName));
	} else {
		$('#privilegeChange').text(columnMessages.removePrivilege.replace("@Permission",permissionName).replace("@Role",roleName));
	}
	$("#role_per").modal();



}
$("#applyButton").click(
		function() {
			var json = {
					"roleId" : roleId,
					"permissionId" : permissionId
			};
			var message;
			if (!$(ell).is(":checked")) {
				 message = customAjaxCalling("rolePermission", json, "DELETE");
				if(message){
					$("#role_per").modal("hide");
					setTimeout(function() {  
						 AJS.flag({
						    type: 'success',
						    title: appMessgaes.success,
						    body: '<p>Permission deleted</p>',
						    close :'auto'
						})
						}, 1000);
					
				}
				else{
					$("#role_per #errorMessageDelete").show();
				}
			} else {
				 message = customAjaxCalling("rolePermission", json, "POST");
				if(message){
					$("#role_per").modal("hide");
					setTimeout(function() {   //calls click event after a one sec
						 AJS.flag({
						    type: 'success',
						    title: appMessgaes.success,
						    body: '<p>Permission added</p>',
						    close :'auto'
						})
						}, 1000);
					
				}
				else{
					$("#role_per #errorMessageAdd").show();
				}
			}
		});



var getParmissionByRole = function(element) {
	var hierarchyName = element;
	
	 customAjaxCalling("permissionByHierarchy/" + hierarchyName,
			null, "GET");

}
//hiding the permission based in hierarchey
var hidePermissionByHierarchy = function() {
	$("#hierarchy").show();

}

var unchecked = function() {
	var checd = $(ell).is(':checked');

	if (!checd) {
		
		$(ell).prop('checked', true);

	} else {

		
		$(ell).removeAttr('checked');
		
	}

}



$(function() {

	setDataTablePagination("#tblAcademic");
	setDataTablePagination("#tblCampaign");
	setDataTablePagination("#tblContentManagment");
	setDataTablePagination("#tblLocations");
	setDataTablePagination("#tblReports");
	setDataTablePagination("#tblUsers");
	setDataTablePagination("#tblDesktop");
	setDataTablePagination("#tblTablet");
	setDataTablePagination("#tblAppDownload");

	var noOfColumns = $('#tblLocations thead tr th').length;
	fnColSpanIfDTEmpty('tblReports',noOfColumns);
	fnColSpanIfDTEmpty('tblTablet',noOfColumns);
	fnColSpanIfDTEmpty('tblDesktop',noOfColumns);
	fnColSpanIfDTEmpty('tblContentManagment',noOfColumns);
	fnColSpanIfDTEmpty('tblUsers',noOfColumns);
	fnColSpanIfDTEmpty('tblAcademic',noOfColumns);
	fnColSpanIfDTEmpty('tblUsers',noOfColumns);
	fnColSpanIfDTEmpty('tblCampaign',noOfColumns);
	fnColSpanIfDTEmpty('tblAppDownload',noOfColumns);
	

});
