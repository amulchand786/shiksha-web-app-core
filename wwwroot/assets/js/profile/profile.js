var changePassword = function() {
	var oldPassword = $('#currentpassword').val();
	var newPassword = $('#newpassword').val();
	var confirmPassword = $('#confirmPassword').val();

	var json = {
		"oldPassword" : oldPassword,
		"newPassword" : newPassword,
		"confirmPassword" : confirmPassword
	}

	var result = customAjaxCalling("changePassword", json, "PUT");

	if (result.response == "shiksha-200") {
		AJS.flag({
			type : "success",
			title : appMessgaes.success,
			body : result.responseMessage,
			close : 'auto'
		});
		setTimeout(function() {
			window.location = 'logout';
		}, 2000);
	} else {

		$('#errorPwdMsg').show().text(result.responseMessage);

	}
}
var fnDisableEditSaveBtn = function() {
	$('#editUserSaveBtn').prop('disabled', true);
}
var saveEditUserData = function() {
	var userName = $("#editUserName").val();
	var gender = $('#genderType').find("option:selected").val();
	var phone = $("#editPhone").val();
	var emergencyContact = $("#editEmergencyNo").val();
	var bloodGroup = $("#editBloodGroup").find("option:selected").val();
	var workAddress = $("#editWorkAddress").val();
	var localAddress = $("#editLocalAddress").val();
	var permanentAddress = $("#editPermanentAddress").val();
	var highestQualification = $("#editHighestQualification").val();
	var hqYearOfPassing = $("#editHQyearOfPassing").val();
	var professionalQualification = $("#editProfessionalQualification").val();
	var pqYearOfPassing = $("#editPQyearOfPassing").val();
	var experience = $("#editExperienceInYears").val();

	var userData = {
		"userName" : userName,
		"phone" : phone,
		"emergencyContact" : emergencyContact,
		"localAddress" : localAddress,
		"permanentAddress" : permanentAddress,
		"workAddress" : workAddress,
		"highestQualification" : highestQualification,
		"professionalQualification" : professionalQualification,
		"experience" : experience,
		"hqYearOfPassing" : hqYearOfPassing,
		"pqYearOfPassing" : pqYearOfPassing,
		"bloodGroup" : bloodGroup,
		"gender" : gender
	};

	var responseMessage = customAjaxCalling("user/profile", userData, "PUT");
	if(responseMessage){
		if (responseMessage.response == "shiksha-200") {
			AJS.flag({
				type : "success",
				title : appMessgaes.success,
				body : responseMessage.responseMessage,
				close : 'auto'
			});
			setTimeout(function(){
				window.location.reload();
			},50);
		} else {
			AJS.flag({
				type : "error",
				title : "Error..!",
				body : responseMessage.responseMessage,
				close : 'auto'
			});
		}
	} else {
		AJS.flag({
			type : "error",
			title : appMessgaes.oops,
			body : appMessgaes.serverError,
			close :'auto'
		})
	}
};
// Start Forgot Password
var forgotPassword = function() {
	var email = $('#email').val().trim();
	var json = {
		"email" : email
	}

	$('.outer-loader').show();
	/* setTimeout(function(){ */
	var result = customAjaxCalling("forgotPassword", json, "POST");

	if (result != null) {
		if (result.response == "shiksha-200") {
			$('#emailHelpText').hide();
			$('#email').prop('disabled', true);
			$('#errorPwdMsg').hide();
			$('#btnDiv').hide();
			$("#successPwdMsg").show();
			$("#successPwdMsg").text(result.responseMessage);
			/*
			 * }else if(result.response == "shiksha-400" &&
			 * result.responseMessage==null ) { $('#emailHelpText').hide();
			 * $('#errorPwdMsg').show().text("This email address is not
			 * registered with Shiksha.").fadeOut(3000);
			 * $('#emailHelpText').show().fadeIn(3000);
			 */
		} else {
			$('#emailHelpText').hide();
			$('#errorPwdMsg').show().text(result.responseMessage);
		}
	} else {
		$('#emailHelpText').hide();
		$('#errorPwdMsg').show().text(
				"Something went wrong with your internet !!");
	}
	$(".outer-loader").hide();

	/* }, 200); */

}

// set New Password
var setNewPassword = function() {

	var confirmPassword = $('#confirmPassword').val();
	var json = {
		"hashCode" : hashCode,
		"confirmPassword" : confirmPassword
	}
	var result = customAjaxCalling("setNewPassword", json, "POST");

	if (result.response == "shiksha-200") {
		$("#successPwdMsg").show();

		setTimeout(function() {
			window.location = 'logout';
		}, 2000);
	} else {

		$('#errorPwdMsg').show().text(result.responseMessage);

	}

}

var uploadProfileImage = function(file) {
	spinner.showSpinner();
	var formData = new FormData();
	formData.append('uploadfile', file);
	$.ajax({
		url : "user/profile/avtar",
		type : "POST",
		data : formData,
		enctype : 'multipart/form-data',
		processData : false,
		contentType : false,
		cache : false,
		async : true,
		success : function(response) {
			spinner.hideSpinner();
			console.log(response, "successfully uploaded");
			if (response.response == "shiksha-200") {

				$("#pImage,.profile-img").attr("src", response.photoUrl);

				AJS.flag({
					type : "success",
					title : appMessgaes.success,
					body : response.responseMessage,
					close : 'auto'
				});
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : response.responseMessage,
					close : 'auto'
				});
			}
		},
		error : function(){
			spinner.hideSpinner();
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close :'auto'
			});
		}
	});	
}

function resetTheFormFields() {
	$('#changePasswordForm').data('formValidation').resetForm(true);
};

$(function() {
	$("#forgotPasswordForm #email").focusin(function() {
		$('#errorPwdMsg').hide();
		$('#emailHelpText').show();
	});

	$("#changePasswordForm")
			.formValidation(
					{
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							oldPassword : {
								validators : {
									callback : {
										message : 'State does not contains these (", ?, \', /, \\) special characters',
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											return regx.test(value);
										}
									}
								}
							},
							newpassword : {
								validators : {
									callback : {
										message : validationMsg.password,
										callback : function(value) {
											// var regex = new

											var regex = new RegExp(
													/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@~`!#\$\^%&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]).*$/);
											return regex.test(value);
										}
									}
								}
							},
							confirmPassword : {
								validators : {
									notEmpty : {
										message : ' '
									},
									identical : {
										field : 'newpassword',
										// message : 'The Password and its
										// Confirm Password must be the same'
										message : ' '
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				changePassword();

			});

	$("#editUserForm")
			.formValidation(
					{
						framework : 'bootstrap',
						icon : {
							validating : 'glyphicon glyphicon-refresh'
						},
						excluded : ':disabled',
						fields : {
							editUserName : {
								validators : {
									notEmpty : {
										message : validationMsg.nameRequired
									},
									stringLength : {
										max : 250,
										message : validationMsg.nameLength
									},
									regexp : {
										regexp : /^[^'?\"/\\]*$/,
										message : validationMsg.nameConsist
									}
								}
							}, 
							editPhone : {
								validators : {
									callback : {

										message 	:	validationMsg.phoneNumber,
										callback 	:	function(value) {
											if ($('#editPhone').val() == null
													|| $('#editPhone').val() == "") {
												return true;
											}
											var regx = /^(?!0{10})[0789]\d{9}$/;
											return regx.test(value);
										}
									}
								}
							},
							editEmergencyNo : {
								validators : {
									callback : {
										message : validationMsg.emergencyNumber,
										callback : function(value) {
											var editEmergencyNo = parseFloat(value);
											if ($('#editEmergencyNo').val() == null
													|| $('#editEmergencyNo')
															.val() == "") {
												return true;
											} else {
												if (editEmergencyNo > 9999999999999) {
													return false;

												} else {
													var regx = /^\d+$/;
													return regx.test(value);
												}
											}

										}
									}
								}
							},
							editHQyearOfPassing : {
								validators : {
									callback : {
										message : validationMsg.year,
										callback : function(value) {
											if ($('#editHQyearOfPassing').val() == null
													|| $('#editHQyearOfPassing')
															.val() == "") {
												return true;
											}
											if ($('#editHQyearOfPassing').val() < 1000) {
												return false;
											}
											var regex = new RegExp(/^\d{4}$/);
											return regex.test(value);

										}
									}
								}
							},
							editPQyearOfPassing : {
								validators : {
									callback : {
										message : validationMsg.year,
										callback : function(value) {
											if ($('#editPQyearOfPassing').val() == null
													|| $('#editPQyearOfPassing')
															.val() == "") {
												return true;
											}
											if ($('#editPQyearOfPassing').val() < 1000) {
												return false;
											}

											var regex = new RegExp(/^\d{4}$/);
											return regex.test(value);

										}
									}
								}
							},
							editExperienceInYears : {
								validators : {
									callback : {
										message : validationMsg.yearsInNumber,
										callback : function(value) {
											var experienceInYears = parseFloat(value);
											if ($('#editExperienceInYears')
													.val() == null
													|| $(
															'#editExperienceInYears')
															.val() == "") {
												return true;
											} else {
												if (experienceInYears > 80) {
													return false
												} else {
													var regx = /^\s*(?=.*[0-9])\d*(?:\.\d{1,2})?\s*$/;

													return regx.test(value);
												}
											}
										}

									}
								}
							}
						}

					})
			.on(
					'err.field.fv',
					function(e, data) {
						$(data.element).closest('.form-group').removeClass('has-success').addClass('has-error');
					}).on(
					'err.validator.fv',
					function(e, data) {
						data.element.data('fv.messages')
								.find(
										'.help-block[data-fv-for="'
												+ data.field + '"]').hide()
								.filter(
										'[data-fv-validator="' + data.validator
												+ '"]').show();
						var isSaveBtnDisabled = $("#editUserSaveBtn").is(
								":disabled");
						
					}).on('success.validator.fv', function() {
				var isSaveBtnDisabled = $("#editUserSaveBtn").is(":disabled");
				
			}).on('success.form.fv', function(e) {
				e.preventDefault();
				saveEditUserData();
						
			});

	// for forgot password schreen
	$("#forgotPasswordForm").formValidation({
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			email : {
				validators : {
					notEmpty : {
						message : 'The Email is required'
					},
					emailAddress : {
						message : 'The value is not a valid email address'
					},
					regexp : {
						regexp : '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message : 'The value is not a valid email address'
					}
				}
			}
		}
	}).on(
			'err.validator.fv',
			function(e, data) {
				data.element.data('fv.messages').find(
						'.help-block[data-fv-for="' + data.field + '"]').hide()
						.filter('[data-fv-validator="' + data.validator + '"]')
						.show();

			}).on('success.form.fv', function(e) {
		e.preventDefault();
		forgotPassword();
	});

	$("#newPasswordForm")
			.formValidation(
					{
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							newpassword : {
								validators : {
									callback : {
										message : 'Password should be a minimum of 8 characters long and should include at least one each of an uppercase letter, a lowercase letter, a special character and a number.',
										callback : function(value) {
											// var regex = new

											var regex = new RegExp(
													/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@~`!#\$\^%&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]).*$/);
											return regex.test(value);
										}
									}
								}
							},
							confirmPassword : {
								validators : {
									notEmpty : {
										message : ' '
									},
									identical : {
										field : 'newpassword',
										message : ' '
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				setNewPassword();

			});
	
	 $("#profile-image1").on('change', 'input', function() {
		var filename=this.files[0].name;
		var sfilename=filename.split(".");
		if(['jpeg','jpg','png'].indexOf(sfilename[sfilename.length-1]) < 0){
				AJS.flag({
					type: "error",
					title: appMessgaes.error,
					body: validationMsg.unsupportedImageFile,
					close: 'auto'
				});
		} else if (this.files[0] && (this.files[0].size > 10485760)) {
			AJS.flag({
				type : "error",
				title : appMessgaes.error,
				body : validationMsg.unsupportedFileSize10MB,
				close : 'auto'
			});
		} else {
			if (this.files && this.files[0]) {
				uploadProfileImage(this.files[0]);
			}
		}
		$(this).val("");
	});
	$("#changePassword").on('click', 'a', function() {
		$("#changePasswordForm")[0].reset();
		$("#editPassword").modal("show");
	});
	$(".profileContainer").on('click', '#editDetails', function() {
		$("#details .has-error").removeClass("has-error");
		$("small.help-block").remove();
	    $("#editUserForm")[0].reset();
		$("#details").modal("show");

		setOptionsForMultipleSelect("#editBloodGroup");
		$("#editBloodGroup").multiselect("refresh");
		setOptionsForMultipleSelect("#genderType");
		$("#genderType").multiselect("refresh");

	});
	
	$("#changePasswordCancelButton").on("click", function(){
		$('#errorPwdMsg').hide();
		resetTheFormFields();
	});
	
	
	setOptionsForMultipleSelect("#editBloodGroup");
	$("#editBloodGroup").multiselect("refresh");
	setOptionsForMultipleSelect("#genderType");
	$("#genderType").multiselect("refresh");
});