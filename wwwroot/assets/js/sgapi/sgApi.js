var accessToken;
var tokenType;
var expireTime;
var getAccessTokenUrl;


var getAccessToken = function(URL,requestData,requestType,callback) {
	getAccessTokenUrl =URL;
	$.ajax({
		type : requestType,
		url : URL,
		data: requestData,
		contentType:"application/x-www-form-urlencoded",
		success : function(data) {
			//accessToken =response.access_token;
			//tokenType =response.token_type;
			callback(data);
		},
		error : function(data) {
			callback(data);
		}
	});	
};

function createAssessmentSgURICall(URL,requestData,requestType,callback) {
	console.log("Create assessment in SG...");
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(requestData),
	    contentType:"application/json",
	    success: function( sgResponse ) {
	    	callback(sgResponse);
	    },
	    error : function(sgResponse) {
	    	console.log("Create assessment in SG Error...");
	    	console.log(sgResponse.error_description);
			callback(sgResponse);
		}
	});
};





//get the clienId and app secret 
var initSGAPI =function(){
	var sgIntegration = customAjaxCalling("sgintegration/app",null,"GET");
	if(sgIntegration!=null && sgIntegration!=''){
		if(sgIntegration.response=="shiksha-200"){
			SG_CLIENT_ID = sgIntegration.appId;
			SG_CLIENT_SECRET = sgIntegration.secretKey;
			
			sgTokenBody =sgTokenBody.replace('CLIENT_ID',SG_CLIENT_ID);
			sgTokenBody =sgTokenBody.replace('CLIENT_SECRET',SG_CLIENT_SECRET);
			
			console.log('SGURL:'+sgTokenUrl);
			//get access token
			getAccessToken(sgTokenUrl,sgTokenBody,"POST",getSgTokenCallback);
		}else{
			alert("SGInitialization failed.."+sgIntegration.response);
		}
	}
}

var getSgTokenCallback =function(sgResponse){
	 console.log("accessToken");
	 if(sgResponse.statusText!="error"){
		 accessToken =sgResponse.access_token;
		 return true;
	 }
}


////////////////////////////////////////////////////////////////////




//create tag value in SG
function createTagValueInSgURICall(URL,createTagRquestData,requestType,callback) {
	console.log("Create tagvalue in SG...");
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(createTagRquestData),
	    contentType:"application/json",  
	    success: function( sgResponse ) {
	    	callback(sgResponse);
	    },
	    error : function(sgResponse) {
	    	console.log("Create tagvalue in SG Error...");	    	
			callback(sgResponse);
		}
	});
};
var createTagValueInSG =function(aRequestData){
	if(accessToken!=null && accessToken!='undefined'){
		createTagValueUrl =sgApiTagValueUrl+accessToken;
		createTagValueInSgURICall(createTagValueUrl,aRequestData,"POST",getSGCreateTagValueResponse);		
	}
}





//edit tag value in SG
function editTagValueInSgURICall(URL,editRequestData,requestType,callback) {
	console.log("Edit tagvalue in SG...");
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(editRequestData),
	    contentType:"application/json",
	    success: function( sgApiResponse ) {
	    	callback(sgApiResponse);
	    },
	    error : function(sgApiResponse) {
	    	console.log("Edit tagvalue in SG Error...");
			callback(sgApiResponse);
		}
	});
};

//edit tag value in SG
var editTagValueInSG =function(eRequestData){
	if(accessToken!=null && accessToken!='undefined'){
		editTagValueUrl =sgApiTagValueUrl+accessToken;
		editTagValueInSgURICall(editTagValueUrl,eRequestData,"PUT",getSGEditTagValueResponse);		
	}
}



//delete tag value in SG
function deleteTagValueInSgURICall(URL,delRequestData,requestType,callback) {
	console.log("Delete tagvalue in SG...");
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(delRequestData),
	    contentType:"application/json",
	    success: function( dSgApiResponse ) {
	    	callback(dSgApiResponse);
	    },
	    error : function(dSgApiResponse) {
	    	console.log("Delete tagvalue in SG Error...");
			callback(dSgApiResponse);
		}
	});
};

//edit tag value in SG
var deleteTagValueInSG =function(dRequestData){
	if(accessToken!=null && accessToken!='undefined'){
		deleteTagValueUrl =sgApiTagValueUrl+accessToken;
		deleteTagValueInSgURICall(deleteTagValueUrl,dRequestData,"DELETE",getSGDeleteTagValueResponse);		
	}
}


