var deletedCampaign;
var selectedChkBox;
var selectedChkBoxForSchoolDel;
var selectedChkBoxForAssmntDel;
var thLabels;

// load schoolDashboard.jsp
var loadSchoolDashboard = function(campaignId) {	
	window.location = 'schoolDashboard/' + campaignId;
}

//get campaignId and display dashboard---SHK-488
var invokeSchoolDashboard = function(e) {
	var campId = $(e).parent().data('id');
	loadSchoolDashboard(campId);
}

//load assessmentDashboard.jsp
//dashboard controller
var loadAssessmentDashboard = function(campaignId) {	
	window.location = 'assmntDashboard/' + campaignId;
}

var invokeAssmntDashboard = function(e) {
	var campId = $(e).parent().data('id');
	loadAssessmentDashboard(campId);
}

// delete selected campaign
var deleteCampaignSetData = function(campaignId, campaignName) {
	deletedCampaign = campaignId;
	$('#mdlDelcampaign #msgTxt').text('');
	$('#mdlDelcampaign #msgTxt').text('Do you want to delete project ' + campaignName + ' ?');
	$('#mdlDelcampaign').modal().show();
}
var deleteCampaign = function() {
	var responseObj = customAjaxCalling("campaign/" + deletedCampaign, null,"DELETE");
	if(responseObj){
		if (responseObj.response == "shiksha-200") {		
			AJS.flag({
				type  : "success",
				title : "Success",
				body  : "Project deleted successfully.",
				close : 'auto'
			});
			$('#mdlDelcampaign').modal("hide");
			$(".project-card[data-projectid='"+deletedCampaign+"']").remove();
		} else {
			AJS.flag({
				type  : "error",
				title : "Error",
				body  : responseObj.responseMessage,
				close : 'auto'
			});
		}
	}/* else {
		AJS.flag({
			type  : "error",
			title : appMessgaes.oops,
			body  : appMessgaes.serverError,
			close : 'auto'
		})
	}*/
	return;
}
$("#btnDownNext").on("click",function(){
	$( "#btnNext" ).trigger( "click" );
	
	
});
$("#btnPrevious").on("click",function(){
	$('#btnDownNext').html('Next <span class="glyphicon glyphicon-arrow-right">');
});

//select all header functionality for campaign school table
var fnDoSelectAll =function(status){	
	var rows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
	if(status){
		selectedChkBox =[];
		$.each(rows,function(i,row){
			
			var id="#chkboxDelSchool"+$(row).prop('id').toString();
			$(id).prop('checked',status);
			 selectedChkBox.push($(row).data('id'));
		});	
	}else if(selectedChkBox.length==0){
		$.each(rows,function(){			
			$('.chkbox').prop('checked',false);			
		});	
	}
}



// /////////////////////////////////////////////////////////////

// Data table for especially campaign school selection...select all feature
/////
//@Debendra
//set dataTable 
var fnDataTableColFilterForCampSchool =function(tableId,noOfCols,excludeCol,isXScroll){	
	$(tableId).on('order.dt',enableTooltip)
				.on( 'search.dt',enableTooltip )
				.on( 'page.dt',enableTooltip)
				.DataTable({
		"pagingType": "numbers",		
		"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		 "lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": true,
		 "bDestroy": true,
		 "bFilter": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,
		 "fnDrawCallback": function ( oSettings ) {
	            var isChecked =$('#chkboxSelectAll').is(':checked');
	        	fnDoSelectAll(isChecked);
	        },
	        "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
		    "aaSorting": [],
		    bSortCellsTop:true,
	});
	
	fnApplyColumnFilter(tableId,noOfCols,[0],['','School ']);
}

var fnAppendNewRow =function(tableName,newRow){
	var table =$("#"+tableName).DataTable();
	table.row.add($(newRow)).draw( false );	
}


//delete multiple schools checkbox functionality
var fnDoselectAllForSchoolDelete =function(status){	
	var rows = $("#campaignSchoolTable").dataTable().fnGetNodes();
	if(status){
		selectedChkBoxForSchoolDel =[];
		$.each(rows,function(i,row){			
			var id="#chkboxDelSchool"+$(row).prop('id').toString();
			$(id).prop('checked',status);
			selectedChkBoxForSchoolDel.push($(row).data('id'));
		});	
	}else if(selectedChkBoxForSchoolDel.length==0){
		$.each(rows,function(){			
			$('.chkboxDelSchool').prop('checked',false);			
		});	
	}
}



//Data table for campaign school....especially written for delete all functionality
//@Debendra
var fnDTForCampaignSchool =function(tableId){	
	$(tableId).dataTable({
		"pagingType": "numbers",		
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": false,
		 /*"scrollX": true,*/	
		 "bDestroy": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,
		 "fnDrawCallback": function ( oSettings ) {	       
		     var isCheckedDel =$('#chkboxSelectAllDelSchool').is(':checked');
		     fnDoselectAllForSchoolDelete(isCheckedDel);
	        },
	        "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
	        //"aaSorting": [[ 1, 'asc' ]]
		    "aaSorting": []
	});	
	$(".row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-left" id="btnDeleteAllCampaignSchool" onclick="fnDeleteAllSchools();"style="display: none;margin-left: -12px;">Delete selected schools</button>');
	
}


//delete multiple assessment checkbox functionality
var fnDoselectAllForAssmntDelete =function(status){	
	var rows = $("#campaignAssessmentTable").dataTable().fnGetNodes();
	if(status){
		selectedChkBoxForAssmntDel =[];
		$.each(rows,function(i,row){			
				
			$('.chkboxDelAssessment').prop('checked',true);
			selectedChkBoxForAssmntDel.push($(row).data('id'));
		});	
	}else if(selectedChkBoxForAssmntDel.length==0){
		$.each(rows,function(){			
			$('.chkboxDelAssessment').prop('checked',false);
		});	
	}
}



//Data table for campaign assessment....especially written for delete all functionality
//@Debendra
var fnDTForCampaignAssmnt =function(tableId){	
	$(tableId).dataTable({
		"pagingType": "numbers",		
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": false,
		 /*"scrollX": true,*/	
		 "bDestroy": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,
		 "fnDrawCallback": function ( oSettings ) {	       
		     var isCheckedDelAssmnt =$('#chkboxSelectAllDelAssmnt').is(':checked');
		     fnDoselectAllForAssmntDelete(isCheckedDelAssmnt);
	        },
	        "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
	        //"aaSorting": [[ 1, 'asc' ]]
		    "aaSorting": []
	});	
	$("#stepContentDiv3 .row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-right" id="btnDeleteAllCampaignAssmnt" onclick="fnDeleteAllAssmnts();"style="display: none;margin-left:-12px;">Delete All Question Papers</button>');
	
}



//datatable for edit camp school
var fnDataTableColFilterForEditCampSchool =function(tableId,noOfCols,excludeCol,isXScroll){	
	$(tableId).on('order.dt',enableTooltip)
				.on( 'search.dt',enableTooltip )
				.on( 'page.dt',enableTooltip)
				.DataTable({
		"pagingType": "numbers",		
		"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": true,
		 "bDestroy": true,
		 "bFilter": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,
		 "fnDrawCallback": function ( oSettings ) {
	            var isChecked =$('#chkboxSelectAll').is(':checked');
	        	fnDoSelectAll(isChecked);
	        },
	        "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
		    "aaSorting": [],
		    bSortCellsTop:true,
	});
}

//delete multiple schools checkbox functionality for edit Campaign
var fnDoselectAllForSchoolDeleteEditCampaign =function(status){	
	var rows = $("#campaignSchoolTable").dataTable().fnGetNodes();
	if(status){
		selectedChkBoxForSchoolDel =[];
		$.each(rows,function(i,row){			
			var id="#chkboxDel"+$(row).prop('id').toString();
			var x =$(id).prop('disabled')
			if(!x){
				$(id).prop('checked',status);
				selectedChkBoxForSchoolDel.push($(row).data('id'));
			}
		});	
	}else if(selectedChkBoxForSchoolDel.length==0){
		$.each(rows,function(){			
			$('.chkboxDel').prop('checked',false);			
		});	
	}
}

	//Data table for campaign school....especially written for delete all functionality for edit campaign
	
	var fnDTForEditCampaignSchool =function(tableId){	
		$(tableId).dataTable({
			"pagingType": "numbers",		
			"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
			 "bLengthChange": false,
			 /*"scrollX": true,*/	
			 "bDestroy": true,
			 "autoWidth": false,
			 "iDisplayLength": 100,
			 "stateSave": false,
			 "fnDrawCallback": function ( oSettings ) {	       
			     var isCheckedDel =$('#chkboxSelectAllDelSchool').is(':checked');
			   
			     fnDoselectAllForSchoolDeleteEditCampaign(isCheckedDel);
		        },
		        "aoColumnDefs": [{
			        'bSortable': false,
			        'aTargets': ['nosort']
			    }],
		        //"aaSorting": [[ 1, 'asc' ]]
			    "aaSorting": []
		});	
		$(".row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-left" id="btnDeleteAllCampaignSchool" onclick="fnDeleteAllSchools();"style="display: none;margin-left: -12px;">Delete selected schools</button>');
	}

	//delete multiple assessment checkbox functionality
	var fnDoselectAllForAssmntDeleteEditCampaign =function(status){	
		var rows = $("#campaignAssessmentTable").dataTable().fnGetNodes();
		if(status){
			selectedChkBoxForAssmntDel =[];
			$.each(rows,function(i,row){			
							
				$('.chkboxDelAssessment').prop('checked',true);
				selectedChkBoxForAssmntDel.push($(row).data('id'));
			});	
		}else if(selectedChkBoxForAssmntDel.length==0){
			$.each(rows,function(){			
				$('.chkboxDelAssessment').prop('checked',false);
			});	
		}
	}



	//Data table foredit campaign assessment....especially written for delete all functionality
	//@Debendra
	var fnDTForEditCampaignAssmnt =function(tableId){	
		$(tableId).dataTable({
			"pagingType": "numbers",		
			"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
			 "bLengthChange": false,
			 /*"scrollX": true,*/	
			 "bDestroy": true,
			 "autoWidth": false,
			 "iDisplayLength": 100,
			 "stateSave": false,
			 "fnDrawCallback": function ( oSettings ) {	       
			     var isCheckedDelAssmnt =$('#chkboxSelectAllDelAssmnt').is(':checked');
			     fnDoselectAllForAssmntDeleteEditCampaign(isCheckedDelAssmnt);
		        },
		        "aoColumnDefs": [{
			        'bSortable': false,
			        'aTargets': ['nosort']
			    }],
		        //"aaSorting": [[ 1, 'asc' ]]
			    "aaSorting": []
		});	
		$("#stepContentDiv3 .row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-right" id="btnDeleteAllCampaignAssmnt" onclick="fnDeleteAllAssmnts();"style="display: none;margin-left:-12px;">Delete All Assessments</button>');
	}


	
	
	
	
//data table for campaign..without export buttons...
	/**
	 * sets data table options along with column filters and header names
	 * @params
	 * @tId 	:	String	: Table id
	 * @nCols	:	Array	: Number of columns
	 * @exCols :	Array	: Excluded columns
	 * @hNames : 	Array	: Header column names	
	 * */
	var fnSetDTColFilterPaginationForCampaign =function(tId,nCols,exCols,hNames){
		var exportCols =[];
		for(var n=1;n<(nCols-1);n++){
			exportCols.push(n);
		}
		$(tId).on('order.dt',enableTooltip)
					.on( 'search.dt',enableTooltip )
					.on( 'page.dt',enableTooltip)
					.DataTable({
			"pagingType": "numbers",		
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
			 "bLengthChange": true,
			 "bDestroy": true,
			 "bFilter": true,
			 "autoWidth": false,
			 "iDisplayLength": 100,
			 "stateSave": false,
			 "iDisplayLength": 100,
			 "fnDrawCallback": function ( oSettings ) {
		            /* Need to redo the counters if filtered or sorted */
		            if ( oSettings.bSorted || oSettings.bFiltered ){
		                for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ){
		                    $('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
		                }
		            }
		        },
		        "aoColumnDefs": [{
			        'bSortable': false,
			        'aTargets': ['nosort']
			    }],
			    "aaSorting": [],
			    bSortCellsTop:true,			 
		});
		fnApplyColumnFilter(tId,nCols,exCols,hNames);
	}
	
	
	var fnDateParsing=function(data){
		var sArray =data.split('-');
		var monthData =sArray[1];
		var dateData =sArray[0];
		var yearData =sArray[2];
		var dateStringData =Date.parse(monthData+' '+dateData+' ,'+yearData);
		return dateStringData;
	}
	
	var isValidEndDate=function(){

		var startDateData=fnDateParsing($('#startDatePicker').datepicker('getFormattedDate'));
		var endDateData=fnDateParsing($('#endDatePicker').datepicker('getFormattedDate'));
		if (endDateData<startDateData) {
			return false;
		} 
		return true;

}
	var campaignStartDateValidation=function(){
		var startDate=$('#startDatePicker').datepicker('getFormattedDate');
		var jsonData={
				"startDate":startDate
		}
		var validationResult=	customAjaxCalling("campaign/validateCampaignStartDate", jsonData, "POST");
		$(".outer-loader").hide();
		$('#mdlError #msgText').text(" ");

		if(validationResult=="AFTER"){
			$('#mdlError #msgText').text("Project start date is future date. Please make Start Date as current date.");

			$('#mdlError').modal().show();
			return false;
		}
		else if(validationResult=="BEFORE"){
			$('#mdlError #msgText').text("Project start date is past date. Please make Start Date as current date.");
			$('#mdlError').modal().show();
			return false;

		}else if(validationResult=="CURRENT"){
			$('#mdlStartConfirmation').modal();
		}else if(validationResult=="ERROR"){
			$('#mdlError #msgText').text("Project start date is not valid");
			$('#mdlError').modal().show();
			return false;
		}


}	
	
$(function() {
	if(superRoleCode=='FS' || superRoleCode=='RC'){
		thLabels =['#','Project  ','Academic Year ','Status ','No. of School(s) ','No. of Question Paper(s) ','Last modified date','Progress ','Remarks '];
		fnSetDTColFilterPaginationForCampaign('#tableCampaign',9,[ 0,4, 5, 6,7,8 ],thLabels);
		
	}else{
	thLabels =['#','Project  ','Academic Year ','Status ','No. of School(s) ','No. of Question Paper(s) ','Last modified date','Progress ','Remarks ','Action'];
	fnSetDTColFilterPaginationForCampaign('#tableCampaign',10,[ 0,4, 5, 6,7,8 ,9],thLabels);
	}

	fnMultipleSelAndToogleDTCol('.search_init', function() {
		fnShowHideColumns('#tableCampaign', []);
	});
	fnUnbindDTSortListener();
	showDiv($('#tableDiv'));
	fnColSpanIfDTEmpty('tableCampaign',10);
	$(".outer-loader").hide();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$(".search-txt-input").on("focus",function(){
		$(this).parents(".search-section").addClass("active");
	});
	$(".search-txt-input").on("blur",function(){
		$(this).parents(".search-section").removeClass("active");
	});
	$(".search-txt-input").on("keyup",function(){
		var searchText = $(this).val().trim();
		$(".projects-cards > .project-card").each(function(){
			var campaignName = $(this).find(".subject-test").text();
			var campaignStatus = $(this).find(".progress-status-text").text();
			var progressNumber= $(this).find(".progress-number").text();
			var campaignAc = $(this).find(".subject-test-period").text();
			
			if(campaignName.toUpperCase().indexOf(searchText.toUpperCase()) > -1){
				$(this).show();
			}else if(campaignStatus.toUpperCase().indexOf(searchText.toUpperCase()) > -1){
				$(this).show();
			}else if(progressNumber.toUpperCase().indexOf(searchText.toUpperCase()) > -1){
				$(this).show();
			}else if(campaignAc.toUpperCase().indexOf(searchText.toUpperCase()) > -1){
				$(this).show();
			} else {
				$(this).hide();
			}
		});
	});
});

// load campaign.jsp on link of breadcum
var loadCampaignListPage = function() {
	window.location = baseContextPath + '/campaign';
	return true;
}

// load createCampaign.jsp on click of createCampaign btn
var loadCreateCampaignPage = function() {
	window.location = 'createCampaign';
}

// load editCampaign.jsp on click of Campaign select
var loadEditcampaignPage = function(campaignId) {
	$('.outer-loader').show()
	window.location = 'editCampaign/' + campaignId;
}




// get campaign comment
var getCommentCampaign = function(campaignId) {
	$("#mdlComment #startComment").text('');
	$("#mdlComment #closeComment").text('');
	var data = customAjaxCalling('getComment/' + campaignId, null, "GET");
	if(data.startComment!=null){
	$("#mdlComment #startComment").html(data.startComment.replace(':#SMALL','<small>').replace(':#BRSMALL','</small><br>'));
	}
	if(data.closeComment!=null){
	$("#mdlComment #closeComment").html(data.closeComment.replace(':#SMALL','<small>').replace(':#BRSMALL','</small><br>'));
	}
	
	
	$('#mdlComment').modal().show();
}






//select all header functionality for campaign school table
var fnDoSelectAllAddSchool =function(status){	
	var rows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
	if(status){
		selectedChkBox =[];
		$.each(rows,function(i,row){			
			var id="#chkboxAddSchoolWizard"+$(row).prop('id').toString();
			$(id).prop('checked',status);
			 selectedChkBox.push($(row).data('id'));
		});	
	}else if(selectedChkBox.length==0){
		$.each(rows,function(){			
			$('.chkbox').prop('checked',false);			
		});	
	}
}


//@Debendra
//set dataTable for add school wizard in create campaign 
var fnDTColFilterForAddSchoolWizard =function(tableId,noOfCols,excludeCol,isXScroll){	
	$(tableId).on('order.dt',enableTooltip)
				.on( 'search.dt',enableTooltip )
				.on( 'page.dt',enableTooltip)
				.DataTable({
		"pagingType": "numbers",		
		"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": true,
		 "bDestroy": true,
		 "bFilter": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,
		 "fnDrawCallback": function ( oSettings ) {
	            var isChecked =$('#chkboxSelectAll').is(':checked');
	        	fnDoSelectAllAddSchool(isChecked);
	        },
	        "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
		    "aaSorting": [],
		    bSortCellsTop:true,
	});
	
	fnApplyColumnFilter(tableId,noOfCols,[0],['','School ']);
}


