
//global variables
var isSelectAllClicked;

var STATUS_PENDING;

var addSchoolForm = "addSchoolForm";
var addAssessmentFormId = 'addAssessmentForCampaignForm';
var selectedLocType;
var selectedLocations;
var selectedSchooltypes;

var selectedSourceIds;
var selectedGradeIds;
var selectedSubjectIds;
var selectedUnitIds;
var selectedStageIds;
var isAssessmentShown;
var isSchoolShown;
var allInputEleNamesOfCityFilter =[];
var allInputEleNamesOfVillageFilter =[];
var allInputEleNamesOfCityFilterSchool=[];

var selectedChkBox;
var noOfSchools;
var selectedChkBoxForSchoolDel;
var selectedChkBoxForAssmntDel;
var selectedIds;
var lObj;
var elementIdMap;
var resetButtonObj;
var removeSchoolRowId;
var assmntList;
var asmntIdList;
var $ele;
var selectedAssessmetIds;
var campaignAssessments;
var selectedAssessment;
var rowNo;
var removeAssmntItem;
var removeAssessmentRowId;
var isSchoolAdded;
var isAssessmentAdded;
var toDayDate;
var prjSDate;
var cityName;
var villageName;
var campaignSchools;
var removeItem;
var currentYrStartDate;
var currentEndDate;

// to addcampaign data
var addCampaignName;
var startDate;
var endDate;
var selectedAssessments;
var selectedSchools;

// global selectDivIds
var addSchoolMdlSelectDivs = {};
addSchoolMdlSelectDivs.city = "#mdlAddSchoolForCamp #divSelectCity";
addSchoolMdlSelectDivs.block = "#mdlAddSchoolForCamp #divSelectBlock";
addSchoolMdlSelectDivs.NP = "#mdlAddSchoolForCamp #divSelectNP";
addSchoolMdlSelectDivs.GP = "#mdlAddSchoolForCamp #divSelectGP";
addSchoolMdlSelectDivs.RV = "#mdlAddSchoolForCamp #divSelectRV";
addSchoolMdlSelectDivs.village = "#mdlAddSchoolForCamp #divSelectVillage";
addSchoolMdlSelectDivs.schoolTypeDiv = "#mdlAddSchoolForCamp #divSchoolType";
addSchoolMdlSelectDivs.schoolDiv = "#mdlAddSchoolForCamp #divSchool";
addSchoolMdlSelectDivs.schoolElement = "#mdlAddSchoolForCamp #selectBox_schoolByLocAndType";

addSchoolMdlSelectDivs.schoolLocationType = "#mdlAddSchoolForCamp #selectBox_schoolLocationType";
addSchoolMdlSelectDivs.schoolType = "#mdlAddSchoolForCamp #selectBox_schoolType";

addSchoolMdlSelectDivs.resetLocDivCity = "#mdlAddSchoolForCamp #divResetLocCity";
addSchoolMdlSelectDivs.resetLocDivVillage = "#mdlAddSchoolForCamp #divResetLocVillage";

// global selectDivIds for assessment of campaign
var addAssessmentMdlSelectDivs = {};
addAssessmentMdlSelectDivs.source = "#mdlAddAssessmentForCampaign  #divSource";
addAssessmentMdlSelectDivs.grade = "#mdlAddAssessmentForCampaign  #divGrade";
addAssessmentMdlSelectDivs.subject = "#mdlAddAssessmentForCampaign  #divSubject";
addAssessmentMdlSelectDivs.unit = "#mdlAddAssessmentForCampaign  #divUnit";
addAssessmentMdlSelectDivs.stageDiv = "#mdlAddAssessmentForCampaign  #divStage";
addAssessmentMdlSelectDivs.assessment = "#mdlAddAssessmentForCampaign  #divAssessement";

// global selectElements for assessment of campaign
var addAssessmentMdlSelectElements = {};
addAssessmentMdlSelectElements.source = "#mdlAddAssessmentForCampaign  #selectBox_source";
addAssessmentMdlSelectElements.grade = "#mdlAddAssessmentForCampaign  #selectBox_grade";
addAssessmentMdlSelectElements.subject = "#mdlAddAssessmentForCampaign  #selectBox_subject";
addAssessmentMdlSelectElements.unit = "#mdlAddAssessmentForCampaign  #selectBox_unit";
addAssessmentMdlSelectElements.stage = "#mdlAddAssessmentForCampaign  #selectBox_stage";
addAssessmentMdlSelectElements.assessment = "#mdlAddAssessmentForCampaign  #selectBox_assessment";
addAssessmentMdlSelectElements.addButton = "#mdlAddAssessmentForCampaign  #addAssessment";

// initialize global vars
var initGlobalValues = function() {
	selectedLocType = '';
	selectedLocations = [];
	selectedSchooltypes = [];
	selectedSourceIds = [];
	selectedGradeIds = [];
	selectedSubjectIds = [];
	selectedUnitIds = [];
	selectedStageIds = [];
	isAssessmentShown = false;
	isSchoolShown = false;
	addCampaignName = '';
	startDate = '';
	endDate = '';
	selectedAssessments = [];
	selectedSchools = [];
	STATUS_PENDING = 'Yet to start';
	selectedChkBox =[];
	noOfSchools=0;
	selectedChkBoxForSchoolDel =[];
	selectedChkBoxForAssmntDel =[];
	allInputEleNamesOfCityFilter =['stateId','zoneId','districtId','tehsilId','cityId'];
	allInputEleNamesOfVillageFilter =['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
	isSelectAllClicked=false;
	allInputEleNamesOfCityFilterSchool =['blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
}

//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}

//displaying smart-filters
var displaySmartFilters = function(type,lCode,lLevelName) {
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddSchoolForCamp #statesForZone',
			"Zone" : '#mdlAddSchoolForCamp #selectBox_zonesByState',
			"District" : '#mdlAddSchoolForCamp #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddSchoolForCamp #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddSchoolForCamp #selectBox_blocksByTehsil',
			"City" : '#mdlAddSchoolForCamp #selectBox_cityByBlock',
			"Nyaya Panchayat" : '#mdlAddSchoolForCamp #selectBox_npByBlock',
			"Gram Panchayat" : '#mdlAddSchoolForCamp #selectBox_gpByNp',
			"Revenue Village" : '#mdlAddSchoolForCamp #selectBox_revenueVillagebyGp',
			"Village" : '#mdlAddSchoolForCamp #selectBox_villagebyRv'
		}
		
		displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));
	}

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
	});
	setTimeout(function() {$('#rowDiv1,#divFilter').show();}, 100);
};

//arrange filter div based on location type
//if rural, hiding city and moving elements to upper
var positionFilterDiv = function(type) {
	if (type == "add") {
		$(addSchoolMdlSelectDivs.NP).insertAfter(
				$(addSchoolMdlSelectDivs.block));
		$(addSchoolMdlSelectDivs.RV).insertAfter($(addSchoolMdlSelectDivs.GP));
	}
}
// ///////////////////////
// smart filter starts here//
// ///////////////////////
// show or hide div as per school location type

$('input:radio[name=locType]').change(function() {
	selectedIds=[];
	lObj =this;
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	var typeCode =$(this).data('code');
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	$('#schoolType').find('input[type=checkbox]:checked').removeAttr('checked');
	$("#"+addSchoolForm).data('formValidation').updateStatus('schoolType', 'VALID');
	selectedLocType = typeCode;
		reinitializeSmartFilter(elementIdMap);
		// reset form
						
		resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfCityFilter);
		resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfVillageFilter);
		
		if (typeCode.toLowerCase() == 'U'.toLowerCase()) {
			
			fnUpdateFVElementStatus(addSchoolForm,allInputEleNamesOfCityFilterSchool,'VALID');
			showDiv($(addSchoolMdlSelectDivs.city),	$(addSchoolMdlSelectDivs.resetLocDivCity));
			hideDiv($(addSchoolMdlSelectDivs.block),$(addSchoolMdlSelectDivs.NP),$(addSchoolMdlSelectDivs.GP),
					$(addSchoolMdlSelectDivs.RV),$(addSchoolMdlSelectDivs.village),$(addSchoolMdlSelectDivs.resetLocDivVillage));
			
			displaySmartFilters("add",$(this).data('code'),$(this).data('levelname'));
		} else {
			
			$("#"+addSchoolForm).data('formValidation').updateStatus('cityId', 'VALID');
			hideDiv($(addSchoolMdlSelectDivs.city),$(addSchoolMdlSelectDivs.resetLocDivCity));
			showDiv($(addSchoolMdlSelectDivs.block),$(addSchoolMdlSelectDivs.NP),$(addSchoolMdlSelectDivs.GP),
					$(addSchoolMdlSelectDivs.RV),$(addSchoolMdlSelectDivs.village),$(addSchoolMdlSelectDivs.resetLocDivVillage));
		
			displaySmartFilters("add",$(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("add");
		}
		fnCollapseMultiselect();
});





// reset only location when click on reset button icon
var resetLocation = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;

}
// invoking on click of resetLocation confirmation dialog box
var doResetLocation = function() {
	selectedIds=[];//it is for smart filter utility..SHK-369

	
	resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfCityFilter);
	resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfVillageFilter);
	reinitializeSmartFilter(elementIdMap);
	
	var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
	var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
	
	displaySmartFilters("add",aLocTypeCode,aLLevelName);
	
	
	
	if (aLocTypeCode != "" && aLocTypeCode != null) {
		if (aLocTypeCode.toLowerCase() == 'U'.toLowerCase()) {
			
			fnUpdateFVElementStatus(addSchoolForm,allInputEleNamesOfCityFilterSchool,'VALID');

		} else {
			
			 $("#"+addSchoolForm).data('formValidation').updateStatus('cityId', 'VALID');
		}
	}
	
	fnCollapseMultiselect();

}

var fnInitSchoolLocationTypeMulSelect = function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 100,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onDropdownHide: function() {
				fnGetSmartFilter();
			}
		});
	});
}


//for school type...on drop down hide, it will fetch all the schools
var initSchoolTypeMultiselect = function() {
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	resetFormValidatonForLoc(addSchoolForm, "schoolType");
	bindSchoolTypeForCampaignToListBox($(addSchoolMdlSelectDivs.schoolType), 0);
	$(addSchoolMdlSelectDivs.schoolType).multiselect({
		maxHeight : 120,
		includeSelectAllOption : true,
		enableFiltering : true,
		enableCaseInsensitiveFiltering : true,
		filterPlaceholder : 'Search here...',
	});
}

//get all schooltype and location and bind to respective list box at add school
var getSchoolTypeAndLoc = function() {

	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	$('#divFilter').hide();
	$('#locationStepWiz').trigger('click'); //reset wizard
	$('#chkboxSelectAll').prop('checked',false);
	$('input:radio[name=locType]').each(function(i,ele){$(ele).prop('checked',false)});
	resetFormById(addSchoolForm);
	bindSchoolTypeForCampaignToListBox($(addSchoolMdlSelectDivs.schoolType), 0);
	getAllSchoolTypeLocation();

	fnInitSchoolLocationTypeMulSelect($(addSchoolMdlSelectDivs.schoolLocationType));
	initSchoolTypeMultiselect();
	$(addSchoolMdlSelectDivs.schoolType).multiselect('refresh');
	$(addSchoolMdlSelectDivs.schoolLocationType).multiselect('refresh');
	selectedChkBox =[];
	
	$('#addCampaignSchoolWizard #btnNext').prop('disabled',false);
	
}

// reset form
var resetLocatonAndForm = function(type) {
	$('#divFilter').hide();
	if (type == "add") {
		resetFormById(addSchoolForm);
	}
	reinitializeSmartFilter(elementIdMap);
	getSchoolTypeAndLoc();
	
	
}


// ///////////////////////
// smart filter ends here//
// ///////////////////////






// remove school from table
var removeSchool = function(rowId, schoolName) {
	removeSchoolRowId = rowId;
	$('#mdlRemoveSchool #msgText').text('');
	$('#mdlRemoveSchool #msgText').text("Do you want to remove School " + schoolName + "?");
	$('#mdlRemoveSchool').modal().show();

}
$('#mdlRemoveSchool #btnRemoveSchool').on('click', function() {
	deleteRow("campaignSchoolTable", removeSchoolRowId);
	$('#mdlRemoveSchool').modal('hide');
	setTimeout(function() {   //calls click event after a one sec
		 AJS.flag({
		    type: 'success',
		    title: 'Success!',
		    body: '<p>School deleted</p>',
		    close :'auto'
		})
		}, 500);
	
	var tnRows = $("#campaignSchoolTable").dataTable().fnGetNodes().length;
	if(tnRows<=1){
		$('#btnDeleteAllCampaignSchool').css('display','none');
		$('#chkboxSelectAllDelSchool').prop('checked',false);
		selectedChkBoxForSchoolDel =[];
	}
});

//empty select box
var fnEmptyAll = function() {
	$.each(arguments, function(i, obj) {
		$(obj).empty();
	});
}

var destroyRefresh = function() {
	$.each(arguments, function(i, obj) {
		$(obj).multiselect('destroy');
	});
}

//Issue-376 started..
//get assessmentList added in table
var fnAddedAssessments = function() {
	selectedAssessments = [];
	asmntIdList = [];
	
	var nAssmntDtRowsInTable = $("#campaignAssessmentTable").dataTable().fnGetNodes();
	$.each(nAssmntDtRowsInTable,function(i,row){			
		var dataId =$(row).prop('id');
		selectedAssessments.push(parseInt(dataId));
	});
	return selectedAssessments;
}


var fnBindSourceToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.sourceId + '" data-id="'
				+ obj.sourceId + '"data-name="' + obj.sourceName + '">'
				+ escapeHtmlCharacters(obj.sourceName) + '</option>');
	});
	fnSetMultiselect(element);
	
	fnCollapseMultiselect();
}

//bind source
var fnBindSource = function() {
	assmntList = fnAddedAssessments();
	var rData = {
		"assementIds" : assmntList
	}
	var sources = customAjaxCalling("source/assessments", rData, "POST");
	fnEmptyAll(addAssessmentMdlSelectElements.source);
	if (sources!=null && sources != "") {
		if(sources[0]!=null && sources[0].response == "shiksha-200"){
			
				fnBindSourceToListBox(addAssessmentMdlSelectElements.source,sources);
			
		}		
		showDiv($(addAssessmentMdlSelectDivs.source));
		hideDiv($(addAssessmentMdlSelectDivs.stageDiv),
				$(addAssessmentMdlSelectDivs.grade),
				$(addAssessmentMdlSelectDivs.subject),
				$(addAssessmentMdlSelectDivs.unit));
		showDiv($(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	} else {
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more published latest version Question Papers. Please check wheather there are published latest version Question Papers or not.");
		$('#mdlError').modal().show();
	}
}

// ////////////////////////////////////////
var fnBindAssessmentToListBox = function(element, data) {
	
	$(element).empty();
	$ele = $(element);
	$.each(data, function(index, obj) {
		
		$ele.append('<option value="' + obj.assessmentId + '" data-id="'+ obj.assessmentId + '"data-sgCode="' + obj.sgAssessmentCode+ '" data-version="' + obj.assessmentVersion+ '" data-sourcename="' + obj.sourceName + '"data-stagename="'
				+ obj.stageName + '"data-gradename="' + obj.gradeName+ '"data-subjectname="' + obj.subjectName + '"data-modifieddate="' + obj.modifiedDate+ '"data-unitname="'
				+ obj.unitName + '"  value="' + obj.assessmentId + '">'+ obj.assessmentName + '</option>');
	});
	fnSetMultiselect(element);
	
	fnCollapseMultiselect();
}
// ///////////////////////////////
// Add Assessment For Campaign //
// ///////////////////////////////

var initAssessment = function() {
	// Issue-376........
	spinner.showSpinner();
	
	
	fnEmptyAll(addAssessmentMdlSelectElements.source,
			addAssessmentMdlSelectElements.stage,
			addAssessmentMdlSelectElements.grade,
			addAssessmentMdlSelectElements.subject,
			addAssessmentMdlSelectElements.unit,
			addAssessmentMdlSelectElements.assessment);
	resetFormById('addAssessmentForCampaignForm');

	destroyRefresh(addAssessmentMdlSelectElements.source,
			addAssessmentMdlSelectElements.stage,
			addAssessmentMdlSelectElements.grade,
			addAssessmentMdlSelectElements.subject,
			addAssessmentMdlSelectElements.unit,
			addAssessmentMdlSelectElements.assessment);
	$(addAssessmentMdlSelectElements.addButton).prop('disabled', true)
	fnBindSource();
	fnSetMultiselect(addAssessmentMdlSelectElements.stage)
	fnSetMultiselect(addAssessmentMdlSelectElements.grade)
	fnSetMultiselect(addAssessmentMdlSelectElements.subject)
	fnSetMultiselect(addAssessmentMdlSelectElements.unit)
	var assessmentIds=fnAddedAssessments();
	var jsonData={
		"assementIds":assessmentIds
	}
	var assessmentData = customAjaxCalling("assessment/getAllPublishedAssessmentList",jsonData, "POST");
	fnEmptyAll(addAssessmentMdlSelectElements.assessment);
	if (assessmentData!=null && assessmentData != "") {
		fnBindAssessmentToListBox(addAssessmentMdlSelectElements.assessment,assessmentData);
	}
	
	
	
	fnCollapseMultiselect();
	

	$('#addLocColspFilter').removeClass('in');
	
	spinner.hideSpinner();
}



//get the selected values and push to respective array for assessment
var getSelectedData = function(elementObj, collector) {
	$(elementObj).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all")
				collector.push(parseInt($(option).data('id')));
		});
	});
}

var fnBindStageToListBox = function(element, data) {
	
	
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.stageId + '" data-id="'
				+ obj.stageId + '"data-name="' + obj.stageName + '">'
				+ escapeHtmlCharacters(obj.stageName) + '</option>');
	});
	fnSetMultiselect(element);
	
	destroyRefresh(addAssessmentMdlSelectElements.stage,addAssessmentMdlSelectElements.grade,
			addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,
			addAssessmentMdlSelectElements.assessment);
	
	fnSetMultiselect(addAssessmentMdlSelectElements.stage);
	/*setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);*/
	fnCollapseMultiselect();
}

// bind stage
var fnBindStage = function() {
	spinner.showSpinner();
	selectedSourceIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	assmntList = fnAddedAssessments();

	var rData = {
			"assementIds" : assmntList,
			"sourceIds" : selectedSourceIds,
	}
	var stages = customAjaxCalling("stage/assessments", rData, "POST");
	fnEmptyAll(addAssessmentMdlSelectElements.stage);
	if (stages!=null && stages !="") {
		if (stages[0]!=null && stages[0].response == "shiksha-200") {
				fnBindStageToListBox(addAssessmentMdlSelectElements.stage, stages);
		}
		
		showDiv($(addAssessmentMdlSelectDivs.stageDiv));
		hideDiv($(addAssessmentMdlSelectDivs.grade),$(addAssessmentMdlSelectDivs.subject),$(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
	
	} else {
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Question Papers of selected Source.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');
	}
	spinner.hideSpinner();
}

var fnBindGradeToListBox = function(element, data) {
	$('.outer-loader').show();
	setTimeout(function(){$('.outer-loader').hide();},1000);
	
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.gradeId + '" data-id="'
				+ obj.gradeId + '"data-name="' + obj.gradeName + '">'
				+ escapeHtmlCharacters(obj.gradeName) + '</option>');
	});
	fnSetMultiselect(element);
	setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);
	fnCollapseMultiselect();
}

// bind grade
var fnBindGrade = function() {
	selectedSourceIds = [];
	selectedStageIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	assmntList = fnAddedAssessments();

	var rData = {
			"assementIds" : assmntList,
			"sourceIds" : selectedSourceIds,
			"stageIds" : selectedStageIds
	};
	var grades = customAjaxCalling("grade/assessments", rData, "POST");
	fnEmptyAll(addAssessmentMdlSelectElements.grade);
	if (grades!=null && grades!="") {
		if (grades[0]!=null && grades[0].response == "shiksha-200") {
			
				fnBindGradeToListBox(addAssessmentMdlSelectElements.grade, grades);
			
		}
		
		showDiv($(addAssessmentMdlSelectDivs.grade));
		hideDiv($(addAssessmentMdlSelectDivs.subject), $(addAssessmentMdlSelectDivs.unit),
				$(addAssessmentMdlSelectDivs.assessment));
		setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);

	} else {
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Question Papers of selected Source and Stage.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');

	}
}

var fnBindSubjectToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.subjectId + '" data-id="'
				+ obj.subjectId + '" data-name="' + obj.subjectName + '">'
				+ escapeHtmlCharacters(obj.subjectName) + '</option>');
	});
	fnSetMultiselect(element);
	setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);
	fnCollapseMultiselect();
}

// bind subject
var fnBindSubject = function() {
	selectedSourceIds = [];
	selectedStageIds = [];
	selectedGradeIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.grade, selectedGradeIds);
	assmntList = fnAddedAssessments();

	var rData = {
			"assementIds" : assmntList,
			"sourceIds" : selectedSourceIds,
			"stageIds" : selectedStageIds,
			"gradeIds" : selectedGradeIds,
	}
	var subjects = customAjaxCalling("subject/assessments", rData, "POST");
	fnEmptyAll(addAssessmentMdlSelectElements.subject);
	if (subjects!=null && subjects !="") {
		if (subjects[0]!=null && subjects[0].response == "shiksha-200") {
			
				fnBindSubjectToListBox(addAssessmentMdlSelectElements.subject,subjects);
			
		}
		
		showDiv($(addAssessmentMdlSelectDivs.subject));
		hideDiv($(addAssessmentMdlSelectDivs.unit),
				$(addAssessmentMdlSelectDivs.assessment));
		setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);

	} else {
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Subject(s) of selected Source,Stage and Grade.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');
		$('#mdlError').modal().show();
	}
}

var fnBindChapterToListBox = function(element, data) {
	$('.outer-loader').show();
	setTimeout(function(){$('.outer-loader').hide();},1000);
	
	
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.unitId + '" data-id="' + obj.unitId	+ '" data-name="' + obj.unitName + '">'+ escapeHtmlCharacters(obj.unitName) + '</option>');
	});
	fnSetMultiselect(element);
	
	fnCollapseMultiselect();
}
// bind chapters
var fnBindChapter = function() {
	selectedSourceIds = [];
	selectedStageIds = [];
	selectedGradeIds = [];
	selectedSubjectIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.grade, selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.subject, selectedSubjectIds);
	assmntList = fnAddedAssessments();

	var rData = {
			"assementIds" : assmntList,
			"sourceIds" : selectedSourceIds,
			"stageIds" : selectedStageIds,
			"gradeIds" : selectedGradeIds,
			"subjectIds" : selectedSubjectIds
	};
	var chapters = customAjaxCalling("unit/assessments", rData, "POST");
	fnEmptyAll(addAssessmentMdlSelectElements.unit);
	if (chapters!=null && chapters != "") {
		if (chapters[0]!=null && chapters[0].response == "shiksha-200") {
			
				fnBindChapterToListBox(addAssessmentMdlSelectElements.unit,chapters);
				
		}
		
		showDiv($(addAssessmentMdlSelectDivs.unit));
		hideDiv($(addAssessmentMdlSelectDivs.assessment));
		setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);
	} else {
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Chapter(s) of selected Source,Stage,grade and Subject.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');
		$('#mdlError').modal().show();
	}
}


// bind assessments
var fnBindAssessment = function() {
	selectedSourceIds = [];
	selectedGradeIds = [];
	selectedSubjectIds = [];
	selectedUnitIds = [];
	selectedStageIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.subject, selectedSubjectIds);
	getSelectedData(addAssessmentMdlSelectElements.grade, selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.unit, selectedUnitIds);

	assmntList = fnAddedAssessments();

	var rData = {
			"assementIds" : assmntList,
			"sourceIds" : selectedSourceIds,
			"stageIds" : selectedStageIds,
			"gradeIds" : selectedGradeIds,
			"subjectIds" : selectedSubjectIds,
			"unitIds" : selectedUnitIds
	};
	var assessments = customAjaxCalling("assessment/published/latestversion", rData, "POST");
	fnEmptyAll(addAssessmentMdlSelectElements.assessment);

	if (assessments!=null && assessments != "") {
		if (assessments[0]!=null && assessments[0].response == "shiksha-200") {
			
				fnBindAssessmentToListBox(addAssessmentMdlSelectElements.assessment, assessments);
			
		}
		
		showDiv($(addAssessmentMdlSelectDivs.assessment));
		

	} else {
		hideDiv($(addAssessmentMdlSelectDivs.assessment));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more Question Papers of selected Source,Stage,Grade,Subject and Chapter(s).All filter selections are inclusive.");
		$('#mdlError').modal().show();
	}
}

function fnBindDataToAllUpwordSmartFilters(){
	selectedAssessmetIds = [];
	getSelectedData(addAssessmentMdlSelectElements.assessment, selectedAssessmetIds);

	var rData = {
			"assementIds" : selectedAssessmetIds
	}
	var dataForAllFilters = customAjaxCalling("assessment/getAssessmentDto", rData, "POST");
	
	var sourceData = dataForAllFilters.sources;
	var stageData = dataForAllFilters.stages;
	var gradeData = dataForAllFilters.grades;
	var subjectData = dataForAllFilters.subjects;
	var unitData = dataForAllFilters.units;
	
//source
	destroyRefresh(addAssessmentMdlSelectElements.source);
	fnEmptyAll(addAssessmentMdlSelectElements.source);
	$ele = $(addAssessmentMdlSelectElements.source);
	$.each(sourceData, function(index, obj) {
		$ele.append('<option value="' + obj.sourceId + '" data-id="'
				+ obj.sourceId + '"data-name="' + obj.sourceName + '"selected=true>'
				+ escapeHtmlCharacters(obj.sourceName) + '</option>');
	});
	fnSetMultiselect(addAssessmentMdlSelectElements.source);
	fnCollapseMultiselect();
	showDiv($(addAssessmentMdlSelectDivs.source));
//stage
	destroyRefresh(addAssessmentMdlSelectElements.stage);
	fnEmptyAll(addAssessmentMdlSelectElements.stage);
	$ele = $(addAssessmentMdlSelectElements.stage);
	$.each(stageData, function(index, obj) {
		$ele.append('<option value="' + obj.stageId + '" data-id="'
				+ obj.stageId + '"data-name="' + obj.stageName + '"selected=true>'
				+ escapeHtmlCharacters(obj.stageName) + '</option>');
	});
	fnSetMultiselect(addAssessmentMdlSelectElements.stage);
	fnCollapseMultiselect();
	showDiv($(addAssessmentMdlSelectDivs.stageDiv));
	
//grade
	destroyRefresh(addAssessmentMdlSelectElements.grade);
	fnEmptyAll(addAssessmentMdlSelectElements.grade);
	$ele = $(addAssessmentMdlSelectElements.grade);
	$.each(gradeData, function(index, obj) {
		$ele.append('<option value="' + obj.gradeId + '" data-id="'
				+ obj.gradeId + '"data-name="' + obj.gradeName + '"selected=true>'
				+ escapeHtmlCharacters(obj.gradeName) + '</option>');
	});
	fnSetMultiselect(addAssessmentMdlSelectElements.grade);
	fnCollapseMultiselect();	
	showDiv($(addAssessmentMdlSelectDivs.grade));
	
//subject
	destroyRefresh(addAssessmentMdlSelectElements.subject);
	fnEmptyAll(addAssessmentMdlSelectElements.subject);
	$ele = $(addAssessmentMdlSelectElements.subject);
	$.each(subjectData, function(index, obj) {
		$ele.append('<option value="' + obj.subjectId + '" data-id="'
				+ obj.subjectId + '"data-name="' + obj.subjectName + '"selected=true>'
				+ escapeHtmlCharacters(obj.subjectName) + '</option>');
	});
	fnSetMultiselect(addAssessmentMdlSelectElements.subject);
	fnCollapseMultiselect();
	showDiv($(addAssessmentMdlSelectDivs.subject));
	
//chapter
	destroyRefresh(addAssessmentMdlSelectElements.unit);
	fnEmptyAll(addAssessmentMdlSelectElements.unit);
	$ele = $(addAssessmentMdlSelectElements.unit);
	$.each(unitData, function(index, obj) {
		$ele.append('<option value="' + obj.unitId + '" data-id="'
				+ obj.unitId + '"data-name="' + obj.unitName + '"selected=true>'
				+ escapeHtmlCharacters(obj.unitName) + '</option>');
	});
	fnSetMultiselect(addAssessmentMdlSelectElements.unit);
	fnCollapseMultiselect();
	showDiv($(addAssessmentMdlSelectDivs.unit));
	
	
}

var hideAlert = function() {
	$('#alertdiv #errorMessage').css('display', 'none');
}
var disableAddButton = function() {
	$(addAssessmentMdlSelectElements.assessment).empty();
	$(addAssessmentMdlSelectElements.addButton).prop('disabled', true);
}

//enable or disable delete all button for assessment
var fnEnableOrDisableDelAllAssmntButton =function(selectedAssmntArr){
	if(selectedAssmntArr.length>=2){
		$('#btnDeleteAllCampaignAssmnt').css('display','block');
	}else{
		$('#btnDeleteAllCampaignAssmnt').css('display','none');
	}
}

// set multiselect option for element
var fnSetMultiselect = function(ele) {
	var aMaxHeight = (ele == (addAssessmentMdlSelectElements.assessment))? 220:220;
	$(ele).multiselect({
				maxHeight : aMaxHeight,
				includeSelectAllOption : true,
				enableFiltering : true,
				enableCaseInsensitiveFiltering : true,
				includeFilterClearBtn : true,
				filterPlaceholder : 'Search here...',
				onDropdownHide : function() {
					
					if (ele == (addAssessmentMdlSelectElements.source)) {
						
						hideAlert();
						disableAddButton();
						if ($(addAssessmentMdlSelectElements.source).find('option:selected').length == 0) {
												
							fnEmptyAll(addAssessmentMdlSelectElements.stage, addAssessmentMdlSelectElements.grade, addAssessmentMdlSelectElements.subject,
									addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							destroyRefresh(addAssessmentMdlSelectElements.stage,addAssessmentMdlSelectElements.grade,
									addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,
									addAssessmentMdlSelectElements.assessment);
							
							
							hideDiv($(addAssessmentMdlSelectDivs.stageDiv),
									$(addAssessmentMdlSelectDivs.grade),
									$(addAssessmentMdlSelectDivs.subject),
									$(addAssessmentMdlSelectDivs.unit),
									$(addAssessmentMdlSelectDivs.assessment));
							
						} else {
							fnEmptyAll(addAssessmentMdlSelectElements.stage, addAssessmentMdlSelectElements.grade, addAssessmentMdlSelectElements.subject,
									addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
						
							fnBindStage();
							
						}
					} else if (ele == (addAssessmentMdlSelectElements.stage)) {
						hideAlert();
						disableAddButton();
						if ($(addAssessmentMdlSelectElements.stage).find('option:selected').length == 0) {
							hideDiv($(addAssessmentMdlSelectDivs.grade),$(addAssessmentMdlSelectDivs.subject),
									$(addAssessmentMdlSelectDivs.unit),	$(addAssessmentMdlSelectDivs.assessment));
							hideDiv($(addAssessmentMdlSelectDivs.assessment));	
							fnEmptyAll(addAssessmentMdlSelectElements.grade, addAssessmentMdlSelectElements.subject,
									addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							
							destroyRefresh(addAssessmentMdlSelectElements.grade,addAssessmentMdlSelectElements.subject,
									addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
							
						} else {
							fnEmptyAll(addAssessmentMdlSelectElements.grade, addAssessmentMdlSelectElements.subject,
									addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							destroyRefresh(addAssessmentMdlSelectElements.grade,addAssessmentMdlSelectElements.subject,
									addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
							fnBindGrade();
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.grade);
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.subject);
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.unit);
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);
						}
					} else if (ele == (addAssessmentMdlSelectElements.grade)) {
						hideAlert();
						disableAddButton();
						if ($(addAssessmentMdlSelectElements.grade).find('option:selected').length == 0) {
							hideDiv($(addAssessmentMdlSelectDivs.subject),$(addAssessmentMdlSelectDivs.unit),
									$(addAssessmentMdlSelectDivs.assessment));
									
					
							fnEmptyAll(addAssessmentMdlSelectElements.subject, addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							destroyRefresh(addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
							
						} else {
							destroyRefresh(addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							fnEmptyAll(addAssessmentMdlSelectElements.subject, addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							fnBindSubject();
						
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.unit);
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);
						}
					} else if (ele == (addAssessmentMdlSelectElements.subject)) {
						hideAlert();
						disableAddButton();
						if ($(addAssessmentMdlSelectElements.subject).find('option:selected').length == 0) {
							hideDiv($(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
							fnEmptyAll(addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							destroyRefresh(addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
						
						} else {
							fnEmptyAll(addAssessmentMdlSelectElements.unit, addAssessmentMdlSelectElements.assessment);
							destroyRefresh(addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
							fnBindChapter();
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.unit);
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);
						}
					} else if (ele == (addAssessmentMdlSelectElements.unit)) {
						hideAlert();
						disableAddButton();
						if ($(addAssessmentMdlSelectElements.unit).find('option:selected').length == 0) {
							hideDiv($(addAssessmentMdlSelectDivs.assessment));
							destroyRefresh(addAssessmentMdlSelectElements.assessment);
							setOptionsForMultipleSelect(addAssessmentMdlSelectElements.assessment);
							
						} else {
							destroyRefresh(addAssessmentMdlSelectElements.assessment);
							showDiv($(addAssessmentMdlSelectDivs.assessment));	
							fnBindAssessment();
						}
					} else if (ele == (addAssessmentMdlSelectElements.assessment)) {
						if ($(addAssessmentMdlSelectElements.assessment).find('option:selected').length > 0)
							
							fnBindDataToAllUpwordSmartFilters();
							$(addAssessmentMdlSelectElements.addButton).prop('disabled', false);
					}
					if($(addAssessmentMdlSelectElements.assessment).val()==null)
					{
						$(addAssessmentMdlSelectElements.addButton).prop('disabled', true);
					}
					
				}
			});
}
// Issue-376 end..


var createNewAssessmentRow = function(selectedAssessments) {
	
	if ($('#campaignAssessmentTable >tbody>tr').length == 1) {
		$("#campaignAssessmentTable tbody").find(' tr').remove();
	}
	rowNo = $('#campaignAssessmentTable tr:last').attr('id') == null ? 1 : $('#campaignAssessmentTable tr:last').attr('id');

	$.each(	selectedAssessments,function(index, obj) {
		var a = '<ul>', b = '</ul>', m = [],result;
		var chapterList= obj.unit;
		var temp ;
		temp = chapterList.split("#,");
		$.each(temp, function (index, value) {
			 m[index] = '<li>' + value + '</li>';
		});
		result=a+m+b;
		result=result.replace(/i>,/g , "i>");
		result=result.replace(/#/g , "");
		var row = "<tr id='"+ obj.id+ "' data-id='"	+ obj.id+ "'>"+
					"<td data-id='"+ obj.id+"'><center><input class='chkboxDelAssessment' type='checkbox' onchange='fnChkUnchkDelAssmnt("+obj.id+")'  id='chkboxDelAssmnt"+obj.id+"'></input></center></td>"+
					"<td title='"+ obj.grade+ "'>"+escapeHtmlCharacters(obj.grade)+ "</td>"+
					"<td title='"+ obj.subject+ "'>"+escapeHtmlCharacters(obj.subject)+ "</td>"+
					"<td >"+ result+ "</td>"+
					"<td title='"+ obj.stage+ "'>"+escapeHtmlCharacters(obj.stage)+ "</td>"+
					"<td title='"+ obj.source+ "'>"+escapeHtmlCharacters(obj.source)+ "</td>"+
					"<td title='"+ obj.name	+ "'>"+ escapeHtmlCharacters(obj.name)+ "</td>"+
					"<td title='"+obj.version+"'>"+obj.version+"</td>"+
					"<td title='"+obj.modifieddate+"'>"+obj.modifieddate+"</td>"+
					"<td style='width: 60px;'><div class='div-tooltip'><a  class='tooltip tooltip-top'	id='btnRemoveAssessment'"+
					"onclick=removeAssessment(&quot;"+ obj.id+ "&quot;,&quot;"+ escapeHtmlCharacters(obj.name)+ "&quot;); ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a>"+
					"<a  style='margin-left:10px;' class='tooltip tooltip-top'	id='btnPreviewAssessment"+ obj.assessmentId	+ "'"+
					"onclick=previewAssessment(&quot;"+ obj.id+ "&quot;,&quot;"+ escapeHtmlCharacters(obj.sgcode)+ "&quot;,&quot;"+ escapeHtmlCharacters(obj.name)+ "&quot;); >   <span class='tooltiptext'>Preview</span><i class='fa fa-desktop fontSize-14'></i></a></div></td>"+
					"</tr>";
		fnAppendNewRow("campaignAssessmentTable", row);
	});
}


// add assessments to table
var bindAssessmentToTable = function() {
	campaignAssessments = [];
	$(addAssessmentMdlSelectElements.assessment).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				selectedAssessment = {};
				selectedAssessment.id = $(this).data('id')
				selectedAssessment.name = $(this).text();
				selectedAssessment.version = $(this).data('version');
				selectedAssessment.sgcode = $(this).data('sgcode');
				selectedAssessment.source = $(this).data('sourcename');
				selectedAssessment.stage = $(this).data('stagename');
				selectedAssessment.grade = $(this).data('gradename');
				selectedAssessment.subject = $(this).data('subjectname');
				selectedAssessment.unit = $(this).data('unitname');
				selectedAssessment.modifieddate = $(this).data('modifieddate');
				campaignAssessments.push(selectedAssessment);
			}
		});
	});
	createNewAssessmentRow(campaignAssessments);
	$('#mdlAddAssessmentForCampaign').modal('hide');
	enableTooltip();
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////Select all and delete all functionality for assessments in campaign assessment adding////////////////////
//delete assessment from table by selecting checkbox
var fnChkUnchkDelAssmnt =function(assmntRId){
	var nAssmntRows = $("#campaignAssessmentTable").dataTable().fnGetNodes().length;
	var idxAssmnt = $.inArray(assmntRId, selectedChkBoxForAssmntDel);	
	if (idxAssmnt == -1) {
		selectedChkBoxForAssmntDel.push(assmntRId);
		selectedChkBoxForAssmntDel.length==nAssmntRows? $('#chkboxSelectAllDelAssmnt').prop('checked',true): $('#chkboxSelectAllDelAssmnt').prop('checked',false);
	} else {
		selectedChkBoxForAssmntDel.splice(idxAssmnt, 1);
		$('#chkboxSelectAllDelAssmnt').prop('checked',false);
	}
	fnEnableOrDisableDelAllAssmntButton(selectedChkBoxForAssmntDel);
}

//on click of select all in table header of campaign assessment table (after added)
var fnInvokeSelectAllDeleteAssmnt =function(){
	var isAssmntDelChecked =$('#chkboxSelectAllDelAssmnt').is(':checked');
	var nDTAssmntRows = $("#campaignAssessmentTable").dataTable().fnGetNodes();
	if(isAssmntDelChecked){
		fnDoselectAllForAssmntDelete(isAssmntDelChecked);
		fnEnableOrDisableDelAllAssmntButton(selectedChkBoxForAssmntDel);
	}else{
		selectedChkBoxForAssmntDel =[];
		$.each(nDTAssmntRows,function(i,row){
			var id="#chkboxDelAssmnt"+($(row).prop('id').toString());
			$(id).prop('checked',false);
		});
		fnEnableOrDisableDelAllAssmntButton(selectedChkBoxForAssmntDel);
	}		
}




//delete all selected assessment if deleteAll button pressed
var fnDeleteAllAssmnts =function(){	
	$('#mdlRemoveAllAssessment #msgText').text('');
	$('#mdlRemoveAllAssessment #msgText').text("Do you want to remove all selected Question Papers ?");
	$('#mdlRemoveAllAssessment').modal().show();
}
$('#mdlRemoveAllAssessment #btnRemoveAllAssessment').on('click',function(){	
	var isCheckedDelAllAssmnt =$('#chkboxSelectAllDelAssmnt').is(':checked');
	if(isCheckedDelAllAssmnt){
		deleteAllRow('campaignAssessmentTable');
		selectedChkBoxForAssmntDel =[];
		$('#chkboxSelectAllDelAssmnt').prop('checked',false);
	}else{
		var assmntNDtRows = $("#campaignAssessmentTable").dataTable().fnGetNodes();
		$.each(assmntNDtRows,function(i,row){			
			if($(row).find('.chkboxDelAssessment').is(':checked') === true){
				deleteRow('campaignAssessmentTable',$(row).prop('id'));
				removeAssmntItem =$(row).prop('id');
				selectedChkBoxForAssmntDel = jQuery.grep(selectedChkBoxForAssmntDel, function(value) {
					return value != removeAssmntItem;
				});
			}
		});

	}
	fnEnableOrDisableDelAllAssmntButton(selectedChkBoxForSchoolDel);
	
	$('#mdlRemoveAllAssessment').modal('hide');
	setTimeout(function() {   //calls click event after a one sec
		 AJS.flag({
		    type: 'success',
		    title: 'Success!',
		    body: '<p>Question Paper deleted</p>',
		    close :'auto'
		})
		}, 1000);
	
});


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////










// preview assessment
var previewAssessment = function(assessmentId, sgAssessmentCode) {
	// open the survey page in SG Engine
	previewSurveyInSg(sgAssessmentCode, assmentPreviewUrl);
}

// remove assessment from table
var removeAssessment = function(rowId, assessmentName) {
	removeAssessmentRowId = rowId;
	$('#mdlRemoveAssessment #msgText').text('');
	$('#mdlRemoveAssessment #msgText').text("Do you want to remove Question Paper " + assessmentName + "?");
	$('#mdlRemoveAssessment').modal().show();

}
$('#mdlRemoveAssessment #btnRemoveAssessment').on('click', function() {
	
	deleteRow("campaignAssessmentTable", removeAssessmentRowId);
	$('#mdlRemoveSchool').modal('hide');
		
	$('#mdlRemoveAssessment').modal('hide');
	
	setTimeout(function() {   //calls click event after a one sec
		 AJS.flag({
		    type: 'success',
		    title: 'Success!',
		    body: '<p>Question Paper deleted</p>',
		    close :'auto'
		})
		}, 1000);
});











//add campaign to db
var addCampaign = function(addCampaignName, startDate, endDate,	selectedAssessments, selectedSchools) {
	var academicyearId=	$('#selectBox_AcademicYear').find('option:selected').data('id');
	$(".outer-loader").show();
	var requestData = {
		"campaignName" : addCampaignName.trim(),
		"startDate" : startDate,
		"endDate" : endDate,
		"assementIds" : selectedAssessments,
		"schoolIds" : selectedSchools,
		"status" : STATUS_PENDING,
		"academicyearId":academicyearId
	}
	$(".outer-loader").show();
	var campaign = customAjaxCalling("campaign", requestData, "POST");
	
	if(campaign!=null){
		if (campaign.response == "shiksha-200") {
			$(".outer-loader").css('display','none');
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>Project created.</p>',
				    close :'auto'
				})
				}, 500);
								
			loadCampaignListPage();
		}
	}
	$(".outer-loader").css('display','none');
	$(window).scrollTop(0);
}

// ////////////////////////////
// add campaign data/////////
// ///////////////////////////

var addCampaignData = function() {
	isSchoolAdded = ($('#campaignSchoolTable').dataTable().fnGetData().length == 0) ? 'no': 'yes';
	isAssessmentAdded = ($('#campaignAssessmentTable').dataTable().fnGetData().length == 0) ? 'no': 'yes';

	$('#mdlError #msgText').text('');
	
		addCampaignName = $('#txt_campaignName ').val();
		startDate = $('#startDatePicker').datepicker('getFormattedDate');
		endDate = $('#endDatePicker').datepicker('getFormattedDate');
		selectedAssessments = [];
		selectedSchools = [];
	
		var nAssmntDtRows = $("#campaignAssessmentTable").dataTable().fnGetNodes();
		$.each(nAssmntDtRows,function(i,row){			
			var sAId=$(row).prop('id');	//srId:selected Assessment id		
			selectedAssessments.push(parseInt(sAId));
		});	
		
		var nDtRows = $("#campaignSchoolTable").dataTable().fnGetNodes();
		$.each(nDtRows,function(i,row){			
			var srId=$(row).prop('id');	//srId:selected school id		
			selectedSchools.push(parseInt(srId));
		});	
		$(".outer-loader").show();
		setTimeout(function(){
			addCampaign(addCampaignName, startDate, endDate,selectedAssessments, selectedSchools);
		},100);
	        	       
	}

//}


function restrictEndDate(e){
	var endDateText=$('[name="endDate"]').val();
	if(endDateText==""){
		e.preventDefault();
		$('#mdlError #msgText').text(" ");
		$('#mdlError #msgText').text("Please select End Date for Project.");
		$('#mdlError').modal().show();
		return false;
	}
	else{
	toDayDate=$('#startDatePicker').datepicker('getFormattedDate')
	prjSDate=$('#endDatePicker').datepicker('getFormattedDate')
	var sArray =toDayDate.split('-');
	var sdMonth =sArray[1];
	var sdDate =sArray[0];
	var sdYear =sArray[2];
	var startDate =Date.parse(sdMonth+' '+sdDate+' ,'+sdYear);

	var eArray =prjSDate.split('-');
	var edMonth =eArray[1];
	var edDate =eArray[0];
	var edYear =eArray[2];
	var endDate =Date.parse(edMonth+' '+edDate+' ,'+edYear);
	if (endDate<startDate) {
		e.preventDefault();
		$('#mdlError #msgText').text(" ");
		$('#mdlError #msgText').text("End Date can not be less than Start Date.");
		$('#mdlError').modal().show();
		return false;
	} else {
		return true;
	}
	}
}


//SHK-596
//check campaign name uniqueness
var fnIsUniqueName =function(e,cName){	
	var rData =customAjaxCalling("campaign/name/"+cName+"/0",null,"GET");
	if(rData !== true){
		e.preventDefault();
		$('#mdlError #msgText').text(" ");
		$('#mdlError #msgText').text("Project name already exist. (Please specify other.)");
		$('#mdlError').modal().show();
		return false;
	}else{
		return true;
	}
}



//get selected locations for schools
var fnGetSelectedLocations =function(){
	if (selectedLocType.toLowerCase() == 'U'.toLowerCase()) {
		if ($(elementIdMap.City).find('option:selected').length > 0) {
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations = [];
			$(elementIdMap.City).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all")
						selectedLocations.push(parseInt($(option).val()));
				});
			});
		}
	} else {
		if ($(elementIdMap.Village).find('option:selected').length > 0) {
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations = [];
			$(elementIdMap.Village).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all")
						selectedLocations.push(parseInt($(option).val()));
				});
			});
		}
	}// else
}
//get selected schoolType
var fnGetSelectedSchoolType =function(){
	selectedSchooltypes = [];
	
	$('#schoolType input:checked').each(function() {
		selectedSchooltypes.push($(this).data('id'));
	});
}
//get addedd schools from school table
var fnGetSelectedSchools =function(){	
	selectedSchools = [];
	
	var abDtRows = $("#campaignSchoolTable").dataTable().fnGetNodes();
	$.each(abDtRows,function(i,row){		
		selectedSchools.push(parseInt($(row).prop('id')));	
	});
}

//bind row to the add school table in wizard
var fnBindSchoolToTable =function(dSchool,e){
	deleteAllRow('addSchoolWizardTable');
	selectedChkBox =[];
	var flag=0;
	$.each(dSchool,	function(index, obj) {

		if(obj.isMember){
			flag =flag+1;
			cityName =(obj.cityName==null)?"N/A":obj.cityName ;
			villageName =(obj.villageName==null)?"N/A":obj.villageName;
			var row = "<tr id='"+ obj.id+ "' data-id='"	+obj.id+ "' data-school='"	+obj.schoolName+ "' data-statename='"+obj.stateName+"'" +
			"data-zonename='" + obj.zoneName+"' data-distname='"+ obj.districtName+"'" +
			"data-tehsilname='"+ obj.tehsilName+"' data-blockname='"+ obj.blockName+"'" +
			"data-cityname='"+ cityName+"' data-nyaypanchayatname='"+ obj.nyayPanchayatName+"'" +
			"data-grampanchayatname='"+obj.gramPanchayatName+"'" +
			"data-revenuevillagename='" + obj.revenueVillageName+"'" +
			"data-villagename='"+ villageName+"'>"+					
			"<td data-id='"+ obj.id+"'><center><input class='chkbox' type='checkbox' onchange='fnChkUnchk("+obj.id+")'  id='chkboxAddSchoolWizard"+obj.id+"'></input></center></td>"+
			"<td title='"+ obj.schoolName+ "'>"+ obj.schoolName+ "</td>"+
			"</tr>";
			fnAppendNewRow("addSchoolWizardTable", row);
		}
	});
	if(flag==0){
		e.preventDefault();
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no Schools of the above selections.");
		$('#mdlError').modal().show();
	}else{
		fnDTColFilterForAddSchoolWizard('#addSchoolWizardTable',2,[0],false);
		fnMultipleSelAndToogleDTCol('.search_init',function(){
			fnHideColumns('#addSchoolWizardTable',[]);
		});
	}
}
//get school for campaign and bind to the table for adding to campaign
var fnBindSchoolForCampaign =function(e){
	fnGetSelectedLocations();
	fnGetSelectedSchoolType();
	fnGetSelectedSchools();
var academicyearId=	$('#selectBox_AcademicYear').find('option:selected').data('id');
	


	var rData ={
			"locations":selectedLocations,
			"schoolTypes":selectedSchooltypes,
			"schoolIds":selectedSchools,
			"academicyearId":academicyearId
	};
	var schoolVm = customAjaxCalling("school/campaign/"+selectedLocType,rData,"POST");
	
	if (schoolVm.length != 0) {
		noOfSchools =schoolVm.length;
		
		fnBindSchoolToTable(schoolVm,e);		
	}else{
		e.preventDefault();
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no Schools of the above selections.");
		$('#mdlError').modal().show();		
	}
}



////////////////////////////////////////


var fnChkUnchk =function(id){	 	 
	 var idx = $.inArray(id, selectedChkBox);	
	 if (idx == -1) {
		 selectedChkBox.push(id);
		 selectedChkBox.length==noOfSchools? $('#chkboxSelectAll').prop('checked',true): $('#chkboxSelectAll').prop('checked',false);
	 } else {
		 selectedChkBox.splice(idx, 1);
		 $('#chkboxSelectAll').prop('checked',false);
	 }
}

//on click of select all in table header for adding schools to campaign wizard...
var fnInvokeSelectAll =function(){
	var isChecked =$('#chkboxSelectAll').is(':checked');
	var rows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
	if(isChecked){
		fnDoSelectAllAddSchool(isChecked);
	}else{
		selectedChkBox =[];
		$.each(rows,function(i,row){
			
			var id="#chkboxAddSchoolWizard"+($(row).prop('id').toString());
			$(id).prop('checked',false);
		});	
	}		
}

var createNewSchoolRow = function(selectedSchools) {
	if ($('#campaignSchoolTable >tbody>tr').length == 1) {
		$("#campaignSchoolTable tbody").find(' tr').remove();
	}
	$.each(selectedSchools,	function(index, obj) {
		var row = "<tr id='"+ obj.id+ "' data-id='"	+obj.id+ "'>"+
					"<td data-id='"+ obj.id+"'><center><input class='chkboxDelSchool' type='checkbox' onchange='fnChkUnchkDelSchool("+obj.id+")'  id='chkboxDelSchool"+obj.id+"'></input></center></td>"+ 
					"<td title='"+ obj.state+ "'>"+ obj.state+ "</td>"+
					
					"<td title='"+ obj.dist+ "'>"+ obj.dist+"</td>"+
				
					"<td title='"+ obj.city+ "'>"+ obj.city+ "</td>"+
					"<td title='"+ obj.village+ "'>"+ obj.village+ "</td>"+
					"<td title='"+ obj.name+ "'>"+ obj.name+ "</td>"+
					"<td><div  class='div-tooltip'><a  class='tooltip tooltip-top'	'id='btnRemoveSchool'"+
					"onclick=removeSchool(&quot;"+ obj.id+ "&quot;,&quot;"+ escapeHtmlCharacters(obj.name)+ "&quot;);> <span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"+
					"</tr>";
		fnAppendNewRow("campaignSchoolTable", row);
	});
}

//////////////////////
//add selected schools to the table
var fnAddSchool = function() {
	campaignSchools = [];
	var rows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
	

	$.each(rows,function(i,row){
					
		 if($(row).find('.chkbox').prop('checked')){
			selectedSchools = {};
			selectedSchools.id = $(row).data('id')
			selectedSchools.name = $(row).data('school');
			selectedSchools.state = $(row)	.data('statename');
			selectedSchools.zone = $(row).data('zonename');
			selectedSchools.dist = $(row).data('distname');
			selectedSchools.tehsil = $(row).data('tehsilname');
			selectedSchools.block = $(row).data('blockname');
			selectedSchools.city = $(row).data('cityname');
			selectedSchools.nyaypanchayat = $(row).data('nyaypanchayatname');
			selectedSchools.grampanchayat = $(row).data('grampanchayatname');
			selectedSchools.revenuevillage = $(row).data('revenuevillagename');
			selectedSchools.village = $(row).data(	'villagename');
			campaignSchools.push(selectedSchools);
		}
	});

	
	
	createNewSchoolRow(campaignSchools);
	$('#mdlAddSchoolForCamp').modal('hide');
}





//SHK-594
//Cancel creating campaign
//will navigate to campaign list page
var fnInvokeCreateCancel =function(){
	$('#mdlCancelCreateCamp').modal().show();
}
var cancelCreateCampaign =function(){
	$('#mdlCancelCreateCamp').modal().hide();
	loadCampaignListPage();
}

//enable or disable delete all button for school
var fnEnableOrDisableDelAllButton =function(selectedArr){
	if(selectedArr.length>=2){
		$('#btnDeleteAllCampaignSchool').css('display','block');
	}else{
		$('#btnDeleteAllCampaignSchool').css('display','none');
	}
}

//delete school from table by selecting checkbox
var fnChkUnchkDelSchool =function(rId){
	var nRows = $("#campaignSchoolTable").dataTable().fnGetNodes().length;
	 var idx = $.inArray(rId, selectedChkBoxForSchoolDel);	
	 if (idx == -1) {
		 selectedChkBoxForSchoolDel.push(rId);
		 selectedChkBoxForSchoolDel.length==nRows? $('#chkboxSelectAllDelSchool').prop('checked',true): $('#chkboxSelectAllDelSchool').prop('checked',false);
	 } else {
		 selectedChkBoxForSchoolDel.splice(idx, 1);
		 $('#chkboxSelectAllDelSchool').prop('checked',false);
	 }
	 fnEnableOrDisableDelAllButton(selectedChkBoxForSchoolDel);
}

//on click of select all in table header of campaign school table (after added)
var fnInvokeSelectAllDeleteSchool =function(){
	var isDelChecked =$('#chkboxSelectAllDelSchool').is(':checked');
	var rows = $("#campaignSchoolTable").dataTable().fnGetNodes();
	if(isDelChecked){
		fnDoselectAllForSchoolDelete(isDelChecked);
		fnEnableOrDisableDelAllButton(selectedChkBoxForSchoolDel);
	}else{
		selectedChkBoxForSchoolDel =[];
		$.each(rows,function(i,row){
			var id="#chkboxDelSchool"+($(row).prop('id').toString());
			$(id).prop('checked',false);
		});
		fnEnableOrDisableDelAllButton(selectedChkBoxForSchoolDel);
	}		
}




//delete all selected schools if deleteAll button pressed
var fnDeleteAllSchools =function(){	
	$('#mdlRemoveAllSchool #msgText').text('');
	$('#mdlRemoveAllSchool #msgText').text("Do you want to remove all selected School(s) ?");
	$('#mdlRemoveAllSchool').modal().show();
}
$('#mdlRemoveAllSchool #btnRemoveSchool').on('click',function(){	
	var isCheckedDelAll =$('#chkboxSelectAllDelSchool').is(':checked');
	if(isCheckedDelAll){
		deleteAllRow('campaignSchoolTable');
		selectedChkBoxForSchoolDel =[];
		 $('#chkboxSelectAllDelSchool').prop('checked',false);
	}else{
		var aDtRows = $("#campaignSchoolTable").dataTable().fnGetNodes();
		$.each(aDtRows,function(i,row){			
			if($(row).find('.chkboxDelSchool').is(':checked') === true){
				deleteRow('campaignSchoolTable',$(row).prop('id'));
				removeItem =$(row).prop('id');
				selectedChkBoxForSchoolDel = jQuery.grep(selectedChkBoxForSchoolDel, function(value) {
					  return value != removeItem;
				});
			}
		});
		
	}
	
	fnEnableOrDisableDelAllButton(selectedChkBoxForSchoolDel);
	$('#mdlRemoveAllSchool').modal('hide');
	setTimeout(function() {   //calls click event after a one sec
		 AJS.flag({
		    type: 'success',
		    title: 'Success!',
		    body: '<p>Schools deleted</p>',
		    close :'auto'
		})
		}, 500);
	
	
	
});

$('#endDatePicker').datepicker({
	allowPastDates: false,
	momentConfig : {
		culture : 'en', // change to specific culture
		format : 'DD-MMM-YYYY' // change for specific format
	},
	restricted: [{
		from: -Infinity,
		to: currentYrStartDate
	}]
}).on('changed.fu.datepicker dateClicked.fu.datepicker',function() {
	var endDate = $('#endDatePicker').datepicker('getFormattedDate');
	if (endDate != "Invalid Date") {
		$("#campaignForm").data('formValidation').updateStatus('endDate', 'VALID');
	}
});

function isPastDate(prjSDate,toDayDate){

	 
	var sArray =toDayDate.split('-');
	var sdMonth =sArray[1];
	var sdDate =sArray[0];
	var sdYear =sArray[2];
	var startDate =Date.parse(sdMonth+' '+sdDate+' ,'+sdYear);

	var eArray =prjSDate.split('-');
	var edMonth =eArray[1];
	var edDate =eArray[0];
	var edYear =eArray[2];
	var endDate =Date.parse(edMonth+' '+edDate+' ,'+edYear);
	if (endDate>=startDate) {
		
		return false;
	} else {
		return true;
	}
	
}



var fnSetDateByAcademicYear=function(){
	currentYrStartDate=$('#selectBox_AcademicYear').find('option:selected').data('startdate');
	currentEndDate=$('#selectBox_AcademicYear').find('option:selected').data('enddate');
	var dateMonthAsWord = moment(new Date()).format('DD-MMM-YYYY');
	var isp=isPastDate(currentYrStartDate,dateMonthAsWord);
	if(isp){
		currentYrStartDate=	dateMonthAsWord;
	}
	
	
	$('#startDatePicker').datepicker({
		allowPastDates: false,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		},
		restricted: [{
			from: -Infinity,
			to: currentYrStartDate
		}]
	});
	$('#startDatePicker').datepicker('setDate',currentYrStartDate);
	
	
	
	$('#endDatePicker').datepicker({
		allowPastDates: false,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		},
		restricted: [{
			from: -Infinity,
			to: currentYrStartDate
		}]
	});
	
	
	$('#endDatePicker').datepicker('setDate', currentEndDate);
	$('[name="endDate"]').val('');
	$("#campaignForm").formValidation('revalidateField', 'startDate');
	$("#campaignForm").formValidation('revalidateField', 'endDate');
	$("#campaignForm").formValidation('revalidateField', 'campaignName');
}

$(function() {
	initGlobalValues();
	
		
	setOptionsForMultipleSelect('#selectBox_AcademicYear');
	$('[name="endDate"]').val('');
	
	 $('#startDatePicker').datepicker({
		allowPastDates: false,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		},
		restricted: [{
			from: -Infinity,
			to: currentYrStartDate
		}]
	}).on('changed.fu.datepicker dateClicked.fu.datepicker',function() {
		var startDate = $('#startDatePicker').datepicker('getFormattedDate');
		if (startDate != "Invalid Date") {
			$("#campaignForm").data('formValidation').updateStatus('startDate', 'VALID');
			}
	});
	$('#endDatePicker').datepicker({
		allowPastDates: false,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		},
		restricted: [{
			from: -Infinity,
			to: currentYrStartDate
		}]
	});
		
	fnDTForCampaignSchool("#campaignSchoolTable");
	fnDTForCampaignAssmnt("#campaignAssessmentTable");
	
	fnColSpanIfDTEmpty('campaignAssessmentTable',10);
	fnColSpanIfDTEmpty('campaignSchoolTable',7);
	
	
	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
	});  
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
	});

	

	$('#campaignForm').formValidation({
		framework : 'bootstrap',
		icon : {
			validating : 'glyphicon glyphicon-refresh'
		},
		// This option will not ignore invisible fields which
		// belong to inactive panels
		excluded : ':hidden',
		fields : {
			campaignName : {
				validators : {
					notEmpty : {
						message : '  '
					},
					stringLength: {
                        max: 250,
                        message: 'Project name must be less than 250 characters'
                    },
					callback : {
						message : 'Project name does not contains these (", ?, \', /, \\) characters two consecutive spaces',
						callback : function(value, validator, $field) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);
						}
					},
					remote: {
						message: 'Project name already exist. (Please specify other.)',
	                    url: 'campaign/isUniqueCampaignName',
	                    data: function(validator, $field, value) {
	                        return {
	                        	campaignName:value ,
	                        	campaignId:0
	                        	
	                        };
	                    },
	                   type: 'POST'
	                }
				}
			},
			startDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},
			endDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},

		}
	}).on('changed.fu.datepicker dateClicked.fu.datepicker',function() {
		var startDate = $('#startDatePicker').datepicker('getFormattedDate');
		var endDate = $('#endDatePicker').datepicker('getFormattedDate');
		var eDate=$('[name="endDate"]').val();
		if(eDate==""){
			return false
		}
		
		var isValidDate=isValidEndDate();
		
		if (startDate != "Invalid Date") {
			$("#campaignForm").data('formValidation').updateStatus('startDate', 'VALID');
			
		}
		if (endDate != "Invalid Date") {
				if(isValidDate){
					$('#endDateMsg').css('display','none');
					$('#btnStart').prop('disabled',false);
					$('#btnNext').prop('disabled',false);
					$("#campaignForm").data('formValidation').updateStatus('endDate', 'VALID');
				}else{
					$('#btnStart').prop('disabled',true);
					$('#endDateMsg').css('display','block');
					$("#campaignForm").data('formValidation').updateStatus('endDate', 'INVALID');
				}
			}
	});

	$('#createCampaignWizard').wizard().on('actionclicked.fu.wizard',function(e, data) {
		$('#btnNext').prop('disabled',false);
		var fv = $('#campaignForm').data('formValidation'), // FormValidation
		// instance
		step = data.step, // Current step
		// The current step container
		$container = $('#campaignForm').find('.step-pane[data-step="' + step + '"]');

		// Validate the container
		fv.validateContainer($container);
		if((step==2 || step==3 ) && data.direction!='previous'){    
			$('#btnDownNext').text("Save");
		}else{
			$('#btnDownNext').html('Next <span class="glyphicon glyphicon-arrow-right">');
		}
		var isValidStep = fv.isValidContainer($container);
		
		if (isValidStep === false || isValidStep === null) {
			// Do not jump to the target panel
			e.preventDefault();
		}
	})
	// Triggered when clicking the Complete button
	.on('finished.fu.wizard',function(e) {
		$('#btnNext').prop('disabled',true);
		var fv = $('#campaignForm').data('formValidation'), 
		step = $('#campaignForm').wizard('selectedItem').step,
		$container = $('#campaignForm').find('.step-pane[data-step="' + step + '"]');

		// Validate the last step container
		fv.validateContainer($container);

		var isValidStep = fv.isValidContainer($container);
		if (isValidStep === true) {		
			e.preventDefault();			
			addCampaignData();
		}
	});



	//for add school wizard of campaign
	$("#" + addSchoolForm).formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			schoolType: {
                validators: {
                    choice: {
                        min: 1,
                        max: 2,
                        message: ' '
                    }
                }
            },
            locType: {
                validators: {
                    notEmpty: {
                        message: ' '
                    }
                }
            }
		}
	});
	$("#addAssessmentForCampaignForm").formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();	
		bindAssessmentToTable();
		
	});
	
	$('#addCampaignSchoolWizard').wizard().on('actionclicked.fu.wizard',function(e, data) {
		var fv = $('#addSchoolForm').data('formValidation'), // FormValidation
		// instance
		step = data.step, // Current step
		// The current step container
		$container = $('#addSchoolForm').find('.step-pane[data-step="' + step + '"]');
		
		// Validate the container
		fv.validateContainer($container);

		var isValidStep = fv.isValidContainer($container);		
		if (isValidStep === false || isValidStep === null) {
			// Do not jump to the target panel
			e.preventDefault();
		}
		if (step == 1 && isValidStep) {
			fnBindSchoolForCampaign(e);
		}
		if (step == 2 && isValidStep && data.direction!='previous') {
			var isOneSchoolSelected =false;
			var aaDtRows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
			$.each(aaDtRows,function(i,row){			
				if($(row).find('.chkbox').is(':checked') === true){
					isOneSchoolSelected =true;
				}
			});
			if(!isOneSchoolSelected){
				e.preventDefault();
				$('#mdlError #msgText').text('');
				$('#mdlError #msgText').text("Please select at least one School.");
				$('#mdlError').modal().show();
			}
		}
		
		//if not selected any school, notification...
	})
	// Triggered when clicking the Complete button
	.on('finished.fu.wizard',function(e) {
		$('#addCampaignSchoolWizard #btnNext').prop('disabled',true);
		
		var fv = $('#addSchoolForm').data('formValidation'),
		step = $('#addSchoolForm').wizard('selectedItem').step, 
		$container = $('#addSchoolForm').find('.step-pane[data-step="' + step + '"]');

		// Validate the last step container
		fv.validateContainer($container);

		var isValidStep = fv.isValidContainer($container);
		if (isValidStep === true) {			
			fnAddSchool()
		}
	});
	
	$('#collapseFilter').on('hidden.bs.collapse', function () {
	    $('#collapseFilter #locationPnlLink').text("Select Location");
	    $("#collapseFilter #collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green");
		$("#collapseFilter #collapseSpanSign").addClass("glyphicon-plus-sign").addClass("green");
    });
	
$('#collapseFilter').on('shown.bs.collapse', function () {
		$('#collapseFilter #locationPnlLink').text("Hide Location");
       $("#collapseFilter #collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green ");
       $("#collapseFilter #collapseSpanSign").addClass("glyphicon-minus-sign").addClass("red");
    	
});
$("#createCampaignWizard").show();
});
$('#mdlResetLoc').on('hidden.bs.modal', function () {
	$("body").addClass("modal-open");
	
	});
