var selectedStates;
var selectedZones;
var selectedDist;
var selectedTehsil;
var selectedCity;
var selectedBlock;
var selectedNp;
var selectedGp;
var selectedRv;
var selectedVillage;
var parentId;

var subElements = {};

var selectedIds = [], selectedType = "";
var iscontainsListBox =false; //user at the time of edit modal save for any bool type...

var locationInfoData = {};

var maxLevelType =  "";

var elementIdMap = {
	"State" : '#mdlAddSchoolForCamp #statesForZone',
	"Zone" : '#mdlAddSchoolForCamp #selectBox_zonesByState',
	"District" : '#mdlAddSchoolForCamp #selectBox_districtsByZone',
	"Tehsil" : '#mdlAddSchoolForCamp #selectBox_tehsilsByDistrict',
	"Block" : '#mdlAddSchoolForCamp #selectBox_blocksByTehsil',
	"Nyaya Panchayat" : '#mdlAddSchoolForCamp #selectBox_npByBlock',
	"Gram Panchayat" : '#mdlAddSchoolForCamp #selectBox_gpByNp',
	"Revenue Village" : '#mdlAddSchoolForCamp #selectBox_revenueVillagebyGp',
	"Village" : '#mdlAddSchoolForCamp #selectBox_villagebyRv'
};


var getLocationByTypeAndId = function(id, type){
	var res_location;
	$(locationInfoData[type]).each(function(index, location){
		if(parseInt(location.id) == id){
			res_location = location;
			return false;
		}
	});
	return res_location;
};

var getLocationByName = function(name){
	return locationInfoData[name];
};

var getLocationByTypeAndParentId = function(id, type){
	var keys = Object.keys(locationInfoData);
	var locationMap = {};
	
	$(keys).each(function(keyIndex, key){
		$(locationInfoData[key]).each(function(index, location){
			if(parseInt(location.parentLocationid) == id && location.parentLocationType == type){
				var locType = location.locationType;
				if(locationMap[locType] == null || typeof locationMap[locType] == "undefined"){
					locationMap[locType] = [];
				}
				locationMap[locType].push(location);
			}
		});
	});
	return locationMap;
};

var getAllLocations = function(){	
	return locationInfoData;
};

var setMaxLevelType = function(levelType){	
	maxLevelType = levelType;
};

var getMaxLevelType = function(){
	return maxLevelType;
};


var unSelectAll = function(){
	$(".outer-loader").show();
	var allLocations = getAllLocations();
	var keys = Object.keys(allLocations);
	$(keys).each(function(index, key){
		$(allLocations[key]).each(function(index, current){
			$(elementIdMap[key]).find("option[value="+current.id+"]").prop('selected', false);
		});
	});
};


var doDisable =function(key,selectedArray,selectedParents){
	$(key).find('option').each(function(i,ele){
		
			if($(ele).is(':selected')){
				selectedArray.push(parseInt($(ele).val()));
			}
			parentId =parseInt($(ele).data('parentid'));
			if (jQuery.inArray(parentId, selectedParents)== -1) {
				$(ele).prop('disabled', true);
				$(ele).addClass("disabled");
			}else{
				$(ele).prop('disabled', false);
				$(ele).removeClass("disabled");
			}
		/*}*/		
	});
	return selectedArray;
}

var fnDisable =function(){
	selectedStates =[];
	
	$(elementIdMap["State"]).find('option').each(function(i,ele){		
			if($(ele).is(':selected')){
				selectedStates.push(parseInt($(ele).val()));
			}else{
				$(ele).prop('disabled', true);
				$(ele).addClass("disabled");
			}
	});
	
	if(selectedStates.length==0){
		doResetLocation();
		return ;
	}
		
	selectedZones =[];
	selectedZones =doDisable(elementIdMap["Zone"],selectedZones,selectedStates);
	
	selectedDist =[];
	selectedDist =doDisable(elementIdMap["District"],selectedDist,selectedZones);
	
	selectedTehsil =[];
	selectedTehsil =doDisable(elementIdMap["Tehsil"],selectedTehsil,selectedDist);
		
	if(selectedLocType.toLowerCase() =='U'.toLowerCase()){
		selectedCity =[];
		selectedCity =doDisable(elementIdMap["City"],selectedCity,selectedTehsil);
	}
	
	selectedBlock =[];
	selectedBlock =doDisable(elementIdMap["Block"],selectedBlock,selectedTehsil);
	
	selectedNp =[];
	selectedNp =doDisable(elementIdMap["Nyaya Panchayat"],selectedNp,selectedBlock);
	
	selectedGp =[];
	selectedGp =doDisable(elementIdMap["Gram Panchayat"],selectedGp,selectedNp);
	
	selectedRv =[];
	selectedRv =doDisable(elementIdMap["Revenue Village"],selectedRv,selectedGp);
	
	selectedVillage =[];
	selectedVillage =doDisable(elementIdMap["Village"],selectedVillage,selectedRv);
		
}

/*
 * 
 * Validations for filters
 * */
var fnUpdateFvStatus =function(element){

	if($(element).length > 0 && $(element).val()!=null)

	{
		$('#addSchoolForm').data('formValidation').updateStatus($(element).prop('name'), 'VALID');
	}
	else
		{
		$('#addSchoolForm').data('formValidation').updateStatus($(element).prop('name'), 'INVALID');
		}

};
var enableMultiSelect = function(arg){
	var dropupFlag =(arg=="#mdlAddSchoolForCamp #selectBox_villagebyRv")?true:(arg=="#mdlAddSchoolForCamp #selectBox_cityByBlock")?true:
					(arg=="#mdlAddSchoolForCamp #selectBox_gpByNp")?true:(arg=="#mdlAddSchoolForCamp #selectBox_revenueVillagebyGp")?true:false;
	var aMaxHeight;
	if((arg=="#mdlAddSchoolForCamp #selectBox_villagebyRv")||(arg=="#mdlAddSchoolForCamp #selectBox_cityByBlock")||(arg=="#mdlAddSchoolForCamp #selectBox_gpByNp")||(arg=="#mdlAddSchoolForCamp #selectBox_revenueVillagebyGp")){
		if($(arg).find('option:not(.disabled)').length>4){
			aMaxHeight =200;
		}else{
			aMaxHeight=200;
		}
	}else{
		aMaxHeight=200;
	}
	
	$(arg).multiselect({
		maxHeight: aMaxHeight,/*$(arg).find('option:not(.disabled)').length>4?150:70,*/
		includeSelectAllOption: true,
		dropUp :false,
		enableCaseInsensitiveFiltering: true,
		filterPlaceholder: 'Search here...',
		onDropdownHide: function() {
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
				unSelectAll();
				$(selectedIds).each(function(index, id){
					biDirectionalMapping(id, selectedType);
				});
				fnDisable();
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(function(ind, ele_key){					
					$(elementIdMap[ele_key]).multiselect('destroy');
					enableMultiSelect(elementIdMap[ele_key]);
					fnUpdateFvStatus(elementIdMap[ele_key]);
				});				
							
				fnCollapseMultiselect();
				$('.outer-loader').hide();
		}
	});	
};
var displayLocations = function(element, locations){
	$(element).parent().show();
	var aSortedData =fnSortSmartFilterByName(locations);	
	$(aSortedData).each(function(index, location){
		if(location.locationType=="School"){
			if(location.isMember){
				$('<option data-id="'+location.id+'" data-parentid="'+location.parentLocationid+'" data-childtype="'+location.childLocationType+'" data-childids="'+location.childLocationid+'">').val(location.id).text(location.locationName).appendTo(element);
			}
		}else{
			$('<option data-id="'+location.id+'" data-parentid="'+location.parentLocationid+'" data-childtype="'+location.childLocationType+'" data-childids="'+location.childLocationid+'">').val(location.id).text(location.locationName).appendTo(element);
		}
	});
	enableMultiSelect(element);	
};

var displayLocationsUptoLevel = function(levelname){
	var keys = Object.keys(locationInfoData);
	maxLevelType = levelname;
	$(keys).each(function(index, key){
		displayLocations(elementIdMap[key],locationInfoData[key]);
	});
}; 

var displayLocationsOfSurvey = function(locCode,currentElement, divElement) {	
	$("#divFilter").prop("hidden", false);
	$($(divElement).children()).each(function(index, child){
		var selectContainer = $(child).find(".select");
		selectContainer.multiselect('destroy');
		selectContainer.html();
		selectContainer.empty();
		$(child).hide();
	});
	
	
	locationInfoData = sendGETRequest("getAllLocationsByLocTypeCode/"+locCode, "GET");
	if(locationInfoData != null && locationInfoData != ""){
		
		displayLocationsUptoLevel("Village");
	}else{
		$($(divElement).children()).each(function(index, emptyChild){
			var emptyContainer = $(emptyChild).find(".select");
			emptyContainer.multiselect('refresh');	
			$(emptyChild).show();
		});
	}
	$(".outer-loader").hide();
};



$(".select").change(function(){	
	selectedType = "";
	selectedIds = [];
	$(this).multiselect('refresh');
	if($(this).val() != null)
		selectedIds = $(this).val();
	selectedType = $(this).attr("data-locationName");
});


var getLocationByParent = function(id, type){
	
	return getLocationByTypeAndParentId(id, type);
};

var recursiveSelection = function(subLocations){
	var subLocationMap = null, subLocs = [];
	$(subLocations).each(function(index, location){
		if(location.locationType == maxLevelType){
			return;
		}
		subLocationMap = getLocationByParent(parseInt(location.id), location.locationType);
		var keys = Object.keys(subLocationMap);
		var key = keys[0];
		$(subLocationMap[key]).each(function(index, current){
			$(elementIdMap[key]).find("option[value="+current.id+"]").prop('selected', true);
			
		});
		return recursiveSelection(subLocationMap[key]);
	});
};


var filterParentElements = function(loc_id, loc_type, callBack){
	var result;
	while(parseInt(loc_id) != 0){
		var location = getLocationByTypeAndId(parseInt(loc_id), loc_type);
		var element = elementIdMap[location.locationType];
		
		$($(element).children()).each(function(index, current){
			if(parseInt($(current).val()) == location.id){
				$(current).prop('selected', true);
				return false;
			}
		});
		loc_id = location.parentLocationid;
		loc_type = location.parentLocationType;
		if(parseInt(loc_id) == 0){
			result = "Success";
			callBack(result);
		}
	}
};



var selectAllSubLocations = function(element, subLocs){
	$(element).multiselect('destroy');
	$(subLocs).each(function(index, current){
		$(element).find("option[value="+current.id+"]").prop('selected', true);
	});
	enableMultiSelect(element);

	
};

var biDirectionalMapping = function(loc_id, loc_type){
	if(loc_id != "multiselect-all"){// multiselect-all is input field default value
		//of "Select-all" option provided by bootstrap multiselect
		
		var locations = [];
		var obj = {"id":loc_id, "locationType":loc_type}; 
		locations.push(obj);
		function callBack(result){
			if(result == "Success"){
				recursiveSelection(locations);		
			}
		}
		filterParentElements(loc_id, loc_type, callBack);		
		
	}else{
		$('.outer-loader').show();
		setTimeout(function(){
			var allLocations = getAllLocations();
			var keys = Object.keys(allLocations);
			$(keys).each(function(index, key){
				selectAllSubLocations(elementIdMap[key], allLocations[key]);
			});
			$('.outer-loader').hide();
			return;
		}, 50);
	}
};







var doDestroyRefresh =function(ele){
	$(ele).multiselect('refresh');
	$(ele).multiselect('destroy');	
}
var disableNonSelected =function(){
	if($(elementIdMap["State"]).find('option:selected').length==0){
		doResetLocation();
		return false;
	}
	$(elementIdMap["State"]).find('option:not(:selected)').each(function(index,ele){		
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["State"]);
		
	
			
	
	$(elementIdMap["Zone"]).find('option:not(:selected)').each(function(index,ele){
			doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Zone"]);

	
	
	$(elementIdMap["District"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["District"]);

	
	
	$(elementIdMap["Tehsil"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Tehsil"]);

	
	$(elementIdMap["Block"]).find('option:not(:selected)').each(function(index,ele){		
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Block"]);

	
	
	$(elementIdMap["Nyaya Panchayat"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Nyaya Panchayat"]);
	
	
	
	$(elementIdMap["Gram Panchayat"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Gram Panchayat"]);

	
	
	$(elementIdMap["Revenue Village"]).find('option:not(:selected)').each(function(index,ele){
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Revenue Village"]);
	

	
	$(elementIdMap["Village"]).find('option:not(:selected)').each(function(index,ele){
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Village"]);

	
	
	$(elementIdMap["City"]).find('option:not(:selected)').each(function(index,ele){
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["City"]);

}

