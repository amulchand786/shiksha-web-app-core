
//global variables
var STATUS_PENDING;
var elementIdMap;
var addSchoolForm = "addSchoolForm";
var addAssessmentFormId = 'addAssessmentForCampaignForm';
var selectedLocType;
var selectedLocations;
var selectedSchooltypes;

var selectedSourceIds;
var selectedGradeIds;
var selectedSubjectIds;
var selectedUnitIds;
var selectedStageIds;
var isAssessmentShown;
var isSchoolShown;

// to addcampaign data
var addCampaignName;
var startDate;
var endDate;
var selectedAssessments;
var selectedSchools;
var schoolIdList;
var resetButtonObj;
var campaignSchools;
var removeSchoolRowId;
var rowNo;
var campaignAssessments;
var selectedAssessment;
var asmntIdList;
var $ele;
var assmntList;
var removeAssessmentRowId;
var isSchoolAdded;
var isAssessmentAdded;

// global selectDivIds
var addSchoolMdlSelectDivs = {};
addSchoolMdlSelectDivs.city = "#mdlAddSchoolForCamp #divSelectCity";
addSchoolMdlSelectDivs.block = "#mdlAddSchoolForCamp #divSelectBlock";
addSchoolMdlSelectDivs.NP = "#mdlAddSchoolForCamp #divSelectNP";
addSchoolMdlSelectDivs.GP = "#mdlAddSchoolForCamp #divSelectGP";
addSchoolMdlSelectDivs.RV = "#mdlAddSchoolForCamp #divSelectRV";
addSchoolMdlSelectDivs.village = "#mdlAddSchoolForCamp #divSelectVillage";
addSchoolMdlSelectDivs.schoolTypeDiv = "#mdlAddSchoolForCamp #divSchoolType";
addSchoolMdlSelectDivs.schoolDiv = "#mdlAddSchoolForCamp #divSchool";
addSchoolMdlSelectDivs.schoolElement = "#mdlAddSchoolForCamp #selectBox_schoolByLocAndType";

addSchoolMdlSelectDivs.schoolLocationType = "#mdlAddSchoolForCamp #selectBox_schoolLocationType";
addSchoolMdlSelectDivs.schoolType = "#mdlAddSchoolForCamp #selectBox_schoolType";

addSchoolMdlSelectDivs.resetLocDivCity = "#mdlAddSchoolForCamp #divResetLocCity";
addSchoolMdlSelectDivs.resetLocDivVillage = "#mdlAddSchoolForCamp #divResetLocVillage";

// global selectDivIds for assessment of campaign
var addAssessmentMdlSelectDivs = {};
addAssessmentMdlSelectDivs.source = "#mdlAddAssessmentForCampaign  #divSource";
addAssessmentMdlSelectDivs.grade = "#mdlAddAssessmentForCampaign  #divGrade";
addAssessmentMdlSelectDivs.subject = "#mdlAddAssessmentForCampaign  #divSubject";
addAssessmentMdlSelectDivs.unit = "#mdlAddAssessmentForCampaign  #divUnit";
addAssessmentMdlSelectDivs.stageDiv = "#mdlAddAssessmentForCampaign  #divStage";
addAssessmentMdlSelectDivs.assessment = "#mdlAddAssessmentForCampaign  #divAssessement";

// global selectElements for assessment of campaign
var addAssessmentMdlSelectElements = {};
addAssessmentMdlSelectElements.source = "#mdlAddAssessmentForCampaign  #selectBox_source";
addAssessmentMdlSelectElements.grade = "#mdlAddAssessmentForCampaign  #selectBox_grade";
addAssessmentMdlSelectElements.subject = "#mdlAddAssessmentForCampaign  #selectBox_subject";
addAssessmentMdlSelectElements.unit = "#mdlAddAssessmentForCampaign  #selectBox_unit";
addAssessmentMdlSelectElements.stage = "#mdlAddAssessmentForCampaign  #selectBox_stage";
addAssessmentMdlSelectElements.assessment = "#mdlAddAssessmentForCampaign  #selectBox_assessment";
addAssessmentMdlSelectElements.addButton = "#mdlAddAssessmentForCampaign  #addAssessment";

// initialize global vars
var initGlobalValues = function() {
	selectedLocType = '';
	selectedLocations = [];
	selectedSchooltypes = [];
	selectedSourceIds = [];
	selectedGradeIds = [];
	selectedSubjectIds = [];
	selectedUnitIds = [];
	selectedStageIds = [];
	isAssessmentShown = false;
	isSchoolShown = false;
	addCampaignName = '';
	startDate = '';
	endDate = '';
	selectedAssessments = [];
	selectedSchools = [];
	STATUS_PENDING = 'Yet to start';
}


var setOptionsForSchoolMultipleSelect = function() {
	$(arguments).each(function(index, arg) {
		$(arg).multiselect({
			maxHeight : 150,
			includeSelectAllOption : true,
			enableFiltering : true,
			enableCaseInsensitiveFiltering : true,
			includeFilterClearBtn : true,
			filterPlaceholder : 'Search here...',
			dropUp : true,
		});
	});
}

//disable the schools if it has been added to table
var disableSchoolFromListBox = function() {
	
	selectedSchools = [];
	schoolIdList = [];
	var i = 0;
	$('#campaignSchoolTable >tbody>tr').each(function(i, row) {
		selectedSchools.push(parseInt(row.getAttribute('data-id')));
	});
	$(addSchoolMdlSelectDivs.schoolElement).each(function(index, ele) {
		$(ele).find("option").each(function(ind, option) {
			if ($(option).val() != "multiselect-all")
				schoolIdList.push(parseInt($(option).data('id')));
		});
	});
	$.grep(schoolIdList, function(el) {
		if ($.inArray(el, selectedSchools) != -1) {
			var option = $(addSchoolMdlSelectDivs.schoolElement).find(
					'option[value="' + el + '"]');
			option.prop('disabled', true);
			$(option).addClass("disabled");
		}
		i++;
	});
	$(addSchoolMdlSelectDivs.schoolElement).find('.disabled').each(
			function(i, ele) {
				$(ele).remove();
			});
	$(addSchoolMdlSelectDivs.schoolElement).multiselect('destroy');
	setOptionsForSchoolMultipleSelect(addSchoolMdlSelectDivs.schoolElement);
}
//for school type...on drop down hide, it will fetch all the schools
var initSchoolTypeMultiselect = function() {
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	resetFormValidatonForLoc(addSchoolForm, "schoolType");
	bindSchoolTypeForCampaignToListBox($(addSchoolMdlSelectDivs.schoolType), 0);
	$(addSchoolMdlSelectDivs.schoolType)
			.multiselect(
					{
						maxHeight : 120,
						includeSelectAllOption : true,
						dropUp : true,
						enableFiltering : true,
						enableCaseInsensitiveFiltering : true,
						filterPlaceholder : 'Search here...',
						onDropdownHide : function() {
							isSchoolShown = true;
							selectedSchooltypes = [];
							$(addSchoolMdlSelectDivs.schoolElement)
									.multiselect('destroy');
							resetFormValidatonForLoc(addSchoolForm, "school");
							$(addSchoolMdlSelectDivs.schoolType)
									.each(
											function(index, ele) {
												$(ele)
														.find("option:selected")
														.each(
																function(ind,
																		option) {
																	if ($(
																			option)
																			.val() != "multiselect-all")
																		selectedSchooltypes
																				.push(parseInt($(
																						option)
																						.val()));
																});
											});
							if (selectedSchooltypes.length == 0) {
								hideDiv($(addSchoolMdlSelectDivs.schoolDiv));
								return;
							}
							var s = getAndBindSchoolByLocAndType(
									$(addSchoolMdlSelectDivs.schoolElement),
									selectedLocType, selectedLocations,
									selectedSchooltypes);
							
							setOptionsForSchoolMultipleSelect($(addSchoolMdlSelectDivs.schoolElement));
							disableSchoolFromListBox();
							showDiv($(addSchoolMdlSelectDivs.schoolDiv));
							if (s.length == 0) {
								$('#mdlError #msgText').text('');
								$('#mdlError #msgText')
										.text(
												"There are no schools of the above selections.");
								hideDiv($(addSchoolMdlSelectDivs.schoolDiv));
								$('#mdlError').modal().show();
							}
						}
					});
}
//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}

//displaying smart-filters
var displaySmartFilters = function(type) {
	
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddSchoolForCamp #statesForZone',
			"Zone" : '#mdlAddSchoolForCamp #selectBox_zonesByState',
			"District" : '#mdlAddSchoolForCamp #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddSchoolForCamp #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddSchoolForCamp #selectBox_blocksByTehsil',
			"City" : '#mdlAddSchoolForCamp #selectBox_cityByBlock',
			"Nyaya Panchayat" : '#mdlAddSchoolForCamp #selectBox_npByBlock',
			"Gram Panchayat" : '#mdlAddSchoolForCamp #selectBox_gpByNp',
			"Revenue Village" : '#mdlAddSchoolForCamp #selectBox_revenueVillagebyGp',
			"Village" : '#mdlAddSchoolForCamp #selectBox_villagebyRv'
		};
		displayLocationsOfSurvey($(addSchoolMdlSelectDivs.schoolLocationType),
				$('#divFilter'));

	}

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$('#rowDiv1,#divFilter').show();
	}, 100);

};

//arrange filter div based on location type
//if rural, hiding city and moving elements to upper
var positionFilterDiv = function(type) {
	if (type == "add") {
		$(addSchoolMdlSelectDivs.NP).insertAfter(
				$(addSchoolMdlSelectDivs.block));
		$(addSchoolMdlSelectDivs.RV).insertAfter($(addSchoolMdlSelectDivs.GP));
	}
}
// ///////////////////////
// smart filter starts here//
// ///////////////////////
// show or hide div as per school location type
$(addSchoolMdlSelectDivs.schoolLocationType).change(
		function() {
			
			hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),
					$(addSchoolMdlSelectDivs.schoolDiv));

			// reset multiselct
			$(addSchoolMdlSelectDivs.schoolElement).multiselect('destroy');
			$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
			initSchoolTypeMultiselect();

			var typeCode = $(this).find('option:selected').data('code');
			selectedLocType = typeCode;
			reinitializeSmartFilter(elementIdMap);
			$("#hrDiv").show();

			// reset form
			resetFormValidatonForLoc(addSchoolForm, "villageId");
			resetFormValidatonForLoc(addSchoolForm, "cityId");
			resetFormValidatonForLoc(addSchoolForm, "schoolType");
			resetFormValidatonForLoc(addSchoolForm, "school");

			if (typeCode.toLowerCase() == 'U'.toLowerCase()) {
				$("#" + addSchoolForm).data('formValidation').updateStatus(
						'villageId', 'VALID');
				showDiv($(addSchoolMdlSelectDivs.city),
						$(addSchoolMdlSelectDivs.resetLocDivCity));
				hideDiv($(addSchoolMdlSelectDivs.block),
						$(addSchoolMdlSelectDivs.NP),
						$(addSchoolMdlSelectDivs.GP),
						$(addSchoolMdlSelectDivs.RV),
						$(addSchoolMdlSelectDivs.village),
						$(addSchoolMdlSelectDivs.resetLocDivVillage));
				displaySmartFilters("add");
			} else {
				$("#" + addSchoolForm).data('formValidation').updateStatus(
						'cityId', 'VALID');
				hideDiv($(addSchoolMdlSelectDivs.city),
						$(addSchoolMdlSelectDivs.resetLocDivCity));
				showDiv($(addSchoolMdlSelectDivs.block),
						$(addSchoolMdlSelectDivs.NP),
						$(addSchoolMdlSelectDivs.GP),
						$(addSchoolMdlSelectDivs.RV),
						$(addSchoolMdlSelectDivs.village),
						$(addSchoolMdlSelectDivs.resetLocDivVillage));
				displaySmartFilters("add");
				positionFilterDiv("add");
			}

			if ($(elementIdMap["State"]).find('option:selected').length == 0) {
				hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),
						$(addSchoolMdlSelectDivs.schoolDiv));
			}
		});





// reset only location when click on reset button icon
var resetLocation = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;

}
// invoking on click of resetLocation confirmation dialog box
var doResetLocation = function() {
	$("#mdlResetLoc").modal("hide");
	hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),$(addSchoolMdlSelectDivs.schoolDiv));
	$('.outer-loader').show();
	/* if($(resetButtonObj).parents("form").attr("id")==addSchoolForm){ */
	resetFormValidatonForLoc(addSchoolForm, "villageId");
	resetFormValidatonForLoc(addSchoolForm, "cityId");
	reinitializeSmartFilter(elementIdMap);
	displaySmartFilters("add");
	var locTypeCode = $(addSchoolMdlSelectDivs.schoolLocationType).find(
			'option:selected').data('code');
	if (locTypeCode != "" && locTypeCode != null) {
		if (locTypeCode.toLowerCase() == 'U'.toLowerCase()) {
			$("#" + addSchoolForm).data('formValidation').updateStatus(
					'villageId', 'VALID');
		} else {
			$("#" + addSchoolForm).data('formValidation').updateStatus(
					'cityId', 'VALID');
		}
	}
	$('.outer-loader').hide();
	/* } */
}

//get all schooltype and location and bind to respective list box at add school
var getSchoolTypeAndLoc = function() {		
	$('#divFilter').hide();
	$(".outer-loader").show();
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	$(addSchoolMdlSelectDivs.schoolLocationType).multiselect('destroy');
	hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),$(addSchoolMdlSelectDivs.schoolDiv));
	$('#divFilter').hide();
	$('#hrDiv').hide();
	resetFormById(addSchoolForm);
	bindSchoolTypeForCampaignToListBox($(addSchoolMdlSelectDivs.schoolType), 0);
	getAllSchoolTypeLocation();
	setOptionsForAllMultipleSelect($(addSchoolMdlSelectDivs.schoolLocationType));
	initSchoolTypeMultiselect();
	$(addSchoolMdlSelectDivs.schoolType).multiselect('refresh');
	$(addSchoolMdlSelectDivs.schoolLocationType).multiselect('refresh');
	$(".outer-loader").hide();
	$('#mdlAddSchoolForCamp').modal().show();
}

// reset form
var resetLocatonAndForm = function(type) {
	$('#divFilter').hide();
	$('#mdlAddSchoolForCamp').modal().hide();
	if (type == "add") {
		resetFormById(addSchoolForm);
	}
	reinitializeSmartFilter(elementIdMap);
	getSchoolTypeAndLoc();
}



// ///////////////////////
// smart filter ends here//
// ///////////////////////

// show or hide schoolTypeDiv as per location
var showOrHideSchoolDiv = function() {
	hideDiv($(addSchoolMdlSelectDivs.schoolDiv));
	hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
	initSchoolTypeMultiselect();
	if (selectedLocType.toLowerCase() == 'U'.toLowerCase()) {
		if ($(elementIdMap.City).find('option:selected').length > 0) {
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations = [];
			$(elementIdMap.City).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all")
						selectedLocations.push(parseInt($(option).val()));
				});
			});
		}
	} else {
		if ($(elementIdMap.Village).find('option:selected').length > 0) {
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations = [];
			$(elementIdMap.Village).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all")
						selectedLocations.push(parseInt($(option).val()));
				});
			});
		}
	}// else
};


var createNewSchoolRow = function(selectedSchools) {
	if ($('#campaignSchoolTable >tbody>tr').length == 1) {
		$("#campaignSchoolTable tbody").find(' tr').remove();
	}
	$
			.each(
					selectedSchools,
					function(index, obj) {
						var row = "<tr id='"
								+ obj.id
								+ "' data-id='"
								+ obj.id
								+ "'>"
								+ "<td></td>"
								+ "<td title='"
								+ obj.state
								+ "'>"
								+ obj.state
								+ "</td>"
								+
								/* "<td title='"+obj.zone+"'>"+obj.zone+"</td>"+ */
								"<td title='"
								+ obj.dist
								+ "'>"
								+ obj.dist
								+ "</td>"
								+
								/* "<td title='"+obj.tehsil+"'>"+obj.tehsil+"</td>"+ */
								/* "<td title='"+obj.block+"'>"+obj.block+"</td>"+ */
								"<td title='"
								+ obj.city
								+ "'>"
								+ obj.city
								+ "</td>"
								+
								/* "<td title='"+obj.nyaypanchayat+"'>"+obj.nyaypanchayat+"</td>"+ */
								/* "<td title='"+obj.grampanchayat+"'>"+obj.grampanchayat+"</td>"+ */
								/* "<td title='"+obj.revenuevillage+"'>"+obj.revenuevillage+"</td>"+ */
								"<td title='"
								+ obj.village
								+ "'>"
								+ obj.village
								+ "</td>"
								+ "<td title='"
								+ obj.name
								+ "'>"
								+ obj.name
								+ "</td>"
								+ "<td><div  style='display:inline-flex;padding-left:0px;position:absolute;'><a  class='tooltip tooltip-top'	'id='btnRemoveSchool'"
								+ "onclick=removeSchool(&quot;"
								+ obj.id
								+ "&quot;,&quot;"
								+ escapeHtmlCharacters(obj.name)
								+ "&quot;);> <span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"
								+ "</tr>";
						appendNewRow("campaignSchoolTable", row);
					});
}



// add selected schools to the table
var addSchool = function() {
	
	campaignSchools = [];
	$(addSchoolMdlSelectDivs.schoolElement)
			.each(
					function(index, ele) {
						$(ele)
								.find("option:selected")
								.each(
										function(ind, option) {
											if ($(option).val() != "multiselect-all") {
												selectedSchools = {};
												selectedSchools.id = $(this)
														.data('id')
												selectedSchools.name = $(this)
														.text();
												selectedSchools.state = $(this)
														.data('statename');
												selectedSchools.zone = $(this)
														.data('zonename');
												selectedSchools.dist = $(this)
														.data('distname');
												selectedSchools.tehsil = $(this)
														.data('tehsilname');
												selectedSchools.block = $(this)
														.data('blockname');
												selectedSchools.city = $(this)
														.data('cityname');
												selectedSchools.nyaypanchayat = $(
														this).data(
														'nyaypanchayatname');
												selectedSchools.grampanchayat = $(
														this).data(
														'grampanchayatname');
												selectedSchools.revenuevillage = $(
														this).data(
														'revenuevillagename');
												selectedSchools.village = $(
														this).data(
														'villagename');
												campaignSchools
														.push(selectedSchools);
											}
										});
					});
	createNewSchoolRow(campaignSchools);
	$('#mdlAddSchoolForCamp').modal('hide');
	enableTooltip();
}


// remove school from table
var removeSchool = function(rowId, schoolName) {
	removeSchoolRowId = rowId;
	$('#mdlRemoveSchool #msgText').text('');
	$('#mdlRemoveSchool #msgText').text(
			"Do you want to remove school " + schoolName + "?");
	$('#mdlRemoveSchool').modal().show();

}
$('#mdlRemoveSchool #btnRemoveSchool').on('click', function() {
	
	deleteRow("campaignSchoolTable", removeSchoolRowId);
	$('#mdlRemoveSchool').modal('hide');
	$('#success-msg').show();
	$('#showMessage').text("School deleted");
	$("#success-msg").fadeOut(3000);
	$('#notify').fadeIn(3000);
});


//empty select box
var fnEmptyAll = function() {
	$.each(arguments, function(i, obj) {
		$(obj).empty();
	});
}

var destroyRefresh = function() {
	$.each(arguments, function(i, obj) {
		$(obj).multiselect('destroy');
	});
	
}
var hideAlert = function() {
	$('#alertdiv #errorMessage').css('display', 'none');
}
var disableAddButton = function() {
	$(addAssessmentMdlSelectElements.addButton).prop('disabled', true);
}

var createNewAssessmentRow = function(selectedAssessments) {
	
	if ($('#campaignAssessmentTable >tbody>tr').length == 1) {
		$("#campaignAssessmentTable tbody").find(' tr').remove();
	}
	rowNo = $('#campaignAssessmentTable tr:last').attr('id') == null ? 1 : $('#campaignAssessmentTable tr:last').attr('id');

	$.each(selectedAssessments,function(index, obj) {
		var row = "<tr id='"+ obj.id+ "' data-id='"+ obj.id+ "'>"
		+ "<td></td>"
		+ "<td title='"+obj.source+ "'>"+ obj.source+"</td>"
		+ "<td title='"+obj.stage+"'>"+obj.stage+"</td>"
		+ "<td title='"+obj.grade+"'>"+obj.grade+"</td>"
		+ "<td title='"+obj.subject+"'>"+obj.subject+"</td>"
		+ "<td title='"+obj.unit+"'>"+obj.unit+"</td>"
		+ "<td title='"+obj.name+"'>"+obj.name+"</td>"
		/* "<td title='"+obj.sgcode+"'>"+obj.sgcode+"</td>"+ */// Issue:468
		"<td style='width: 160px;'><div style='display:inline-flex;padding-left:0px;position:absolute;'><a  class='tooltip tooltip-top'	id='btnRemoveAssessment'"
		+ "onclick=removeAssessment(&quot;"+obj.id+"&quot;,&quot;"+escapeHtmlCharacters(obj.name)+ "&quot;); ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a>"
		+ "<a  style='margin-left:10px;' class='tooltip tooltip-top'	id='btnPreviewAssessment"+obj.assessmentId+"'"
		+ "onclick=previewAssessment(&quot;"+obj.id+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgcode)+"&quot;,&quot;"+ escapeHtmlCharacters(obj.name)+"&quot;); >   <span class='tooltiptext'>Preview</span><span class='fa fa-slideshare fa-fw font-size12' ></span></a></div></td>"
		+ "</tr>";
		appendNewRow("campaignAssessmentTable", row);
	});
}
//add assessments to table
var bindAssessmentToTable = function() {
	campaignAssessments = [];
	$(addAssessmentMdlSelectElements.assessment).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				selectedAssessment = {};
				selectedAssessment.id = $(this).data('id')
				selectedAssessment.name = $(this).text();
				selectedAssessment.sgcode = $(this).data('sgcode');
				selectedAssessment.source = $(this).data('sourcename');
				selectedAssessment.stage = $(this).data('stagename');
				selectedAssessment.grade = $(this).data('gradename');
				selectedAssessment.subject = $(this).data('subjectname');
				selectedAssessment.unit = $(this).data('unitname');
				campaignAssessments.push(selectedAssessment);
			}
		});
	});
	createNewAssessmentRow(campaignAssessments);
	$('#mdlAddAssessmentForCampaign').modal('hide');
	enableTooltip();
}
//Issue-376 started..
//get assessmentList added in table
var fnAddedAssessments = function() {
	selectedAssessments = [];
	asmntIdList = [];
	
	$('#campaignAssessmentTable >tbody>tr').each(function(i, row) {
		if (row.getAttribute('data-id') != null)
			selectedAssessments.push(row.getAttribute('data-id'));
	});
	
	return selectedAssessments;
}

//get the selected values and push to respective array for assessment
var getSelectedData = function(elementObj, collector) {
	$(elementObj).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all")
				
				collector.push(parseInt($(option).data('id')));
		});
	});
}

var fnBindStageToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.stageId + '" data-id="'
				+ obj.stageId + '"data-name="' + obj.stageName + '">'
				+ escapeHtmlCharacters(obj.stageName) + '</option>');
	});
	fnSetMultiselect(element);
}


//bind stage
var fnBindStage = function() {
	
	selectedSourceIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	assmntList = fnAddedAssessments();

	var rData = {
		"assementIds" : assmntList,
		"sourceIds" : selectedSourceIds,
	}
	var stages = customAjaxCalling("stage/assessments", rData, "POST");
	
	fnEmptyAll(addAssessmentMdlSelectElements.stage);

	if (stages != "") {
		if (stages[0].response == "shiksha-200") {
			fnBindStageToListBox(addAssessmentMdlSelectElements.stage, stages);
		}
		showDiv($(addAssessmentMdlSelectDivs.stageDiv));
		hideDiv($(addAssessmentMdlSelectDivs.grade),
				$(addAssessmentMdlSelectDivs.subject),
				$(addAssessmentMdlSelectDivs.unit),
				$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	} else {
		
		/*
		 * $('#mdlError #msgText').text(''); $('#mdlError #msgText').text("There
		 * are no more assessments of selected source.All assessments has been
		 * added."); $('#mdlError').modal().show();
		 */
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception')
				.text(
						"There are no more assessments of selected source.All assessments has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');
	}
}


var fnBindGradeToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.gradeId + '" data-id="'
				+ obj.gradeId + '"data-name="' + obj.gradeName + '">'
				+ escapeHtmlCharacters(obj.gradeName) + '</option>');
	});
	fnSetMultiselect(element);
}
//bind grade
var fnBindGrade = function() {

	selectedSourceIds = [];
	selectedStageIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	assmntList = fnAddedAssessments();

	var rData = {
		"assementIds" : assmntList,
		"sourceIds" : selectedSourceIds,
		"stageIds" : selectedStageIds
	};
	var grades = customAjaxCalling("grade/assessments", rData, "POST");

	fnEmptyAll(addAssessmentMdlSelectElements.grade);

	if (grades != "") {
		if (grades[0].response == "shiksha-200") {
			fnBindGradeToListBox(addAssessmentMdlSelectElements.grade, grades);
		}
		showDiv($(addAssessmentMdlSelectDivs.grade));
		hideDiv($(addAssessmentMdlSelectDivs.subject),
				$(addAssessmentMdlSelectDivs.unit),
				$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	} else {

		/*
		 * $('#mdlError #msgText').text(''); $('#mdlError #msgText').text("There
		 * are no more assessments of selected source and stage.All assessments
		 * has been added."); $('#mdlError').modal().show();
		 */
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception')
				.text(
						"There are no more assessments of selected source and stage.All assessments has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');

	}
}


var fnBindSubjectToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.subjectId + '" data-id="'
				+ obj.subjectId + '" data-name="' + obj.subjectName + '">'
				+ escapeHtmlCharacters(obj.subjectName) + '</option>');
	});
	fnSetMultiselect(element);
}

//bind subject
var fnBindSubject = function() {

	selectedSourceIds = [];
	selectedStageIds = [];
	selectedGradeIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.grade, selectedGradeIds);
	assmntList = fnAddedAssessments();

	var rData = {
		"assementIds" : assmntList,
		"sourceIds" : selectedSourceIds,
		"stageIds" : selectedStageIds,
		"gradeIds" : selectedGradeIds,

	}
	var subjects = customAjaxCalling("subject/assessments", rData, "POST");

	fnEmptyAll(addAssessmentMdlSelectElements.subject);

	if (subjects != "") {
		if (subjects[0].response == "shiksha-200") {
			fnBindSubjectToListBox(addAssessmentMdlSelectElements.subject,
					subjects);
		}
		showDiv($(addAssessmentMdlSelectDivs.subject));
		hideDiv($(addAssessmentMdlSelectDivs.unit),
				$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	} else {

		/*
		 * $('#mdlError #msgText').text(''); $('#mdlError #msgText').text("There
		 * are no more assessments of selected source,stage and grade.All
		 * assessments has been added."); $('#mdlError').modal().show();
		 */
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception')
				.text(
						"There are no more assessments of selected source,stage and grade.All assessments has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');

	}
}


var fnBindChapterToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.unitId + '" data-id="' + obj.unitId
				+ '" data-name="' + obj.unitName + '">'
				+ escapeHtmlCharacters(obj.unitName) + '</option>');
	});
	fnSetMultiselect(element);
}
//bind chapters
var fnBindChapter = function() {

	selectedSourceIds = [];
	selectedStageIds = [];
	selectedGradeIds = [];
	selectedSubjectIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.grade, selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.subject, selectedSubjectIds);
	assmntList = fnAddedAssessments();

	var rData = {
		"assementIds" : assmntList,
		"sourceIds" : selectedSourceIds,
		"stageIds" : selectedStageIds,
		"gradeIds" : selectedGradeIds,
		"subjectIds" : selectedSubjectIds

	};
	var chapters = customAjaxCalling("unit/assessments", rData, "POST");

	fnEmptyAll(addAssessmentMdlSelectElements.unit);

	if (chapters != "") {
		if (chapters[0].response == "shiksha-200") {
			fnBindChapterToListBox(addAssessmentMdlSelectElements.unit,
					chapters);
		}
		showDiv($(addAssessmentMdlSelectDivs.unit));
		hideDiv($(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	} else {
		/*
		 * $('#mdlError #msgText').text(''); $('#mdlError #msgText').text("There
		 * are no more assessments of selected source,stage,grade and
		 * subject.All assessments has been added.");
		 * $('#mdlError').modal.show();
		 */
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception')
				.text(
						"There are no more assessments of selected source,stage,grade and subject.All assessments has been added.");
		$('#alertdiv #errorMessage').css('display', 'block');

	}
}

var fnBindAssessmentToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.assessmentId + '" data-id="'
				+ obj.assessmentId + '"data-sgCode="' + obj.sgAssessmentCode
				+ '"data-sourcename="' + obj.sourceName + '"data-stagename="'
				+ obj.stageName + '"data-gradename="' + obj.gradeName
				+ '"data-subjectname="' + obj.subjectName + '"data-unitname="'
				+ obj.unitName + '"  value="' + obj.assessmentId + '">'
				+ obj.assessmentName + '</option>');
	});
	fnSetMultiselect(element);
}

//bind assessments
var fnBindAssessment = function() {

	selectedSourceIds = [];
	selectedGradeIds = [];
	selectedSubjectIds = [];
	selectedUnitIds = [];
	selectedStageIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.subject, selectedSubjectIds);
	getSelectedData(addAssessmentMdlSelectElements.grade, selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.unit, selectedUnitIds);

	assmntList = fnAddedAssessments();

	var rData = {
		"assementIds" : assmntList,
		"sourceIds" : selectedSourceIds,
		"stageIds" : selectedStageIds,
		"gradeIds" : selectedGradeIds,
		"subjectIds" : selectedSubjectIds,
		"unitIds" : selectedUnitIds

	};
	var assessments = customAjaxCalling("getAssessmentList", rData, "POST");

	fnEmptyAll(addAssessmentMdlSelectElements.assessment);

	if (assessments != "") {
		if (assessments[0].response == "shiksha-200") {
			fnBindAssessmentToListBox(
					addAssessmentMdlSelectElements.assessment, assessments);
		}
		showDiv($(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();

	} else {

		$('#mdlError #msgText').text('');
		$('#mdlError #msgText')
				.text(
						"There are no more assessments of selected source,stage,grade,subject and chapter.All assessments has been added.");
		$('#mdlError').modal.show();
	}
}
//set multiselect option for element
var fnSetMultiselect = function(ele) {
	$(ele)
			.multiselect(
					{
						maxHeight : 150,
						includeSelectAllOption : true,
						enableFiltering : true,
						enableCaseInsensitiveFiltering : true,
						includeFilterClearBtn : true,
						filterPlaceholder : 'Search here...',
						onDropdownHide : function() {
							if (ele == (addAssessmentMdlSelectElements.source)) {
								hideAlert();
								disableAddButton();
								if ($(addAssessmentMdlSelectElements.source)
										.find('option:selected').length == 0) {
									hideDiv(
											$(addAssessmentMdlSelectDivs.stageDiv),
											$(addAssessmentMdlSelectDivs.grade),
											$(addAssessmentMdlSelectDivs.subject),
											$(addAssessmentMdlSelectDivs.unit),
											$(addAssessmentMdlSelectDivs.assessment));
									/*
									 * hideAlert(); disableAddButton();
									 */
									destroyRefresh(
											addAssessmentMdlSelectElements.stage,
											addAssessmentMdlSelectElements.grade,
											addAssessmentMdlSelectElements.subject,
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
								} else {
									
									destroyRefresh(
											addAssessmentMdlSelectElements.stage,
											addAssessmentMdlSelectElements.grade,
											addAssessmentMdlSelectElements.subject,
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
									fnBindStage();
								}
							} else if (ele == (addAssessmentMdlSelectElements.stage)) {
								hideAlert();
								disableAddButton();
								if ($(addAssessmentMdlSelectElements.stage)
										.find('option:selected').length == 0) {
									hideDiv(
											$(addAssessmentMdlSelectDivs.grade),
											$(addAssessmentMdlSelectDivs.subject),
											$(addAssessmentMdlSelectDivs.unit),
											$(addAssessmentMdlSelectDivs.assessment));
									destroyRefresh(
											addAssessmentMdlSelectElements.grade,
											addAssessmentMdlSelectElements.subject,
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
									
								} else {
									
									destroyRefresh(
											addAssessmentMdlSelectElements.grade,
											addAssessmentMdlSelectElements.subject,
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
									fnBindGrade();
								}
							} else if (ele == (addAssessmentMdlSelectElements.grade)) {
								hideAlert();
								disableAddButton();
								if ($(addAssessmentMdlSelectElements.grade)
										.find('option:selected').length == 0) {
									hideDiv(
											$(addAssessmentMdlSelectDivs.subject),
											$(addAssessmentMdlSelectDivs.unit),
											$(addAssessmentMdlSelectDivs.assessment));
									destroyRefresh(
											addAssessmentMdlSelectElements.subject,
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
									
								} else {
									
									destroyRefresh(
											addAssessmentMdlSelectElements.subject,
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
									fnBindSubject();
								}
							} else if (ele == (addAssessmentMdlSelectElements.subject)) {
								hideAlert();
								disableAddButton();
								if ($(addAssessmentMdlSelectElements.subject)
										.find('option:selected').length == 0) {
									hideDiv(
											$(addAssessmentMdlSelectDivs.unit),
											$(addAssessmentMdlSelectDivs.assessment));
									destroyRefresh(
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
									
								} else {
									
									destroyRefresh(
											addAssessmentMdlSelectElements.unit,
											addAssessmentMdlSelectElements.assessment);
									fnBindChapter();
								}
							} else if (ele == (addAssessmentMdlSelectElements.unit)) {
								hideAlert();
								disableAddButton();
								if ($(addAssessmentMdlSelectElements.grade)
										.find('option:selected').length == 0) {
									hideDiv($(addAssessmentMdlSelectDivs.assessment));
									destroyRefresh(addAssessmentMdlSelectElements.assessment);
									
								} else {
									
									destroyRefresh(addAssessmentMdlSelectElements.assessment);
									fnBindAssessment();
								}
							} else if (ele == (addAssessmentMdlSelectElements.assessment)) {
								if ($(addAssessmentMdlSelectElements.grade)
										.find('option:selected').length > 0)
									$(addAssessmentMdlSelectElements.addButton)
											.prop('disabled', false);
							}
						}
					});
}
//Issue-376 end..


var fnBindSourceToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.sourceId + '" data-id="'
				+ obj.sourceId + '"data-name="' + obj.sourceName + '">'
				+ escapeHtmlCharacters(obj.sourceName) + '</option>');
	});
	fnSetMultiselect(element);
}
// ////////////////////////////////////////

//bind source
var fnBindSource = function() {
	assmntList = fnAddedAssessments();
	var rData = {
		"assementIds" : assmntList
	}
	var sources = customAjaxCalling("source/assessments", rData, "POST");
	
	fnEmptyAll(addAssessmentMdlSelectElements.source);

	if (sources != "") {
		if (sources[0].response == "shiksha-200") {
			fnBindSourceToListBox(addAssessmentMdlSelectElements.source,
					sources);
		}
		showDiv($(addAssessmentMdlSelectDivs.source));
		hideDiv($(addAssessmentMdlSelectDivs.stageDiv),
				$(addAssessmentMdlSelectDivs.grade),
				$(addAssessmentMdlSelectDivs.subject),
				$(addAssessmentMdlSelectDivs.unit),
				$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	} else {
		
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText')
				.text(
						"There are no more assessments.All assessments has been added.");
		$('#mdlError').modal().show();
	}
}
// ///////////////////////////////
// Add Assessment For Campaign //
// ///////////////////////////////

var initAssessment = function() {
	// set multiselect options
	/*
	 * setOptionsForAssessmentMultiSelect($(addAssessmentMdlSelectElements.source),$(addAssessmentMdlSelectElements.grade),
	 * $(addAssessmentMdlSelectElements.subject),$(addAssessmentMdlSelectElements.unit),
	 * $(addAssessmentMdlSelectElements.stage),$(addAssessmentMdlSelectElements.assessment));
	 * resetFormValidatonForLoc(addAssessmentFormId,"assessment");
	 * resetFormValidatonForLoc(addAssessmentFormId,"source");
	 * resetFormValidatonForLoc(addAssessmentFormId,"stage");
	 * resetFormValidatonForLoc(addAssessmentFormId,"grade");
	 * resetFormValidatonForLoc(addAssessmentFormId,"subject");
	 * resetFormValidatonForLoc(addAssessmentFormId,"unit");
	 * hideDiv($(addAssessmentMdlSelectDivs.assessment));
	 * isAssessmentShown=false;
	 * 
	 * $('#addAssessment').css('display','none');
	 * $('#btnViewAssessment').css('display','block');
	 */

	// Issue-376........
	fnEmptyAll(addAssessmentMdlSelectElements.source,
			addAssessmentMdlSelectElements.stage,
			addAssessmentMdlSelectElements.grade,
			addAssessmentMdlSelectElements.subject,
			addAssessmentMdlSelectElements.unit,
			addAssessmentMdlSelectElements.assessment);
	resetFormById('addAssessmentForCampaignForm');

	destroyRefresh(addAssessmentMdlSelectElements.source,
			addAssessmentMdlSelectElements.stage,
			addAssessmentMdlSelectElements.grade,
			addAssessmentMdlSelectElements.subject,
			addAssessmentMdlSelectElements.unit,
			addAssessmentMdlSelectElements.assessment);
	$(addAssessmentMdlSelectElements.addButton).prop('disabled', true)
	fnBindSource();

}





//disable the assessments if it has been added to table
var disableAssessmentFromListBox = function() {

	selectedAssessments = [];
	asmntIdList = [];
	var i = 0;
	$('#campaignAssessmentTable >tbody>tr').each(function(i, row) {
		selectedAssessments.push(parseInt(row.getAttribute('data-id')));
	});
	$(addAssessmentMdlSelectElements.assessment).each(function(index, ele) {
		$(ele).find("option").each(function(ind, option) {
			if ($(option).val() != "multiselect-all")
				asmntIdList.push(parseInt($(option).data('id')));
		});
	});
	$.grep(asmntIdList, function(el) {
		if ($.inArray(el, selectedAssessments) != -1) {
			var option = $(addAssessmentMdlSelectElements.assessment).find(
					'option[value="' + el + '"]');
			option.prop('disabled', true);
			option.parent('label').parent('a').parent('li').remove();
		}
		i++;
	});
	$(addAssessmentMdlSelectElements.assessment).multiselect('refresh');
}



// get assessment on basis of filters
var getAssessment = function() {
	// if already assessment selected, then add to table
	if (isAssessmentShown) {

		bindAssessmentToTable();
		return true;
	}

	isAssessmentShown = true;
	selectedSourceIds = [];
	selectedGradeIds = [];
	selectedSubjectIds = [];
	selectedUnitIds = [];
	selectedStageIds = [];
	getSelectedData(addAssessmentMdlSelectElements.source, selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage, selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.subject, selectedSubjectIds);
	getSelectedData(addAssessmentMdlSelectElements.grade, selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.unit, selectedUnitIds);

	$(addAssessmentMdlSelectElements.assessment).multiselect('destroy');
	resetFormValidatonForLoc(addAssessmentFormId, "assessment");

	// bind assessment to assessment list box
	getAndBindAssessmentByParentFilter(
			$(addAssessmentMdlSelectElements.assessment), selectedSourceIds,
			selectedStageIds, selectedGradeIds, selectedSubjectIds,
			selectedUnitIds);

	setOptionsForAllMultipleSelect($(addAssessmentMdlSelectElements.assessment));
	showDiv($(addAssessmentMdlSelectDivs.assessment));
	disableAssessmentFromListBox();

	$('#addAssessment').css('display', 'block');
	$('#btnViewAssessment').css('display', 'none');

}




// customize all multiple select box for assessment
// on any change, assessment selectbox will hide
/*
 * var setOptionsForAssessmentMultiSelect = function(){
 * $(arguments).each(function(index,arg){ $(arg).multiselect({ maxHeight: 100,
 * includeSelectAllOption: true, dropUp: true, enableFiltering: true,
 * enableCaseInsensitiveFiltering: true, includeFilterClearBtn: true,
 * numberDisplayed: 2, filterPlaceholder: 'Search here...', disableIfEmpty:
 * true, onChange: function(event) { if(isAssessmentShown==true){
 * $("#"+addAssessmentFormId).data('formValidation').updateStatus("assessment",
 * 'VALID'); hideDiv($(addAssessmentMdlSelectDivs.assessment));
 * $('#addAssessment').css('display','none');
 * $('#btnViewAssessment').css('display','block'); isAssessmentShown =false; } }
 * }); }); }
 */




// preview assessment
var previewAssessment = function(assessmentId, sgAssessmentCode) {
	// open the survey page in SG Engine
	previewSurveyInSg(sgAssessmentCode, assmentPreviewUrl);
}

// remove assessment from table
var removeAssessment = function(rowId, assessmentName) {
	removeAssessmentRowId = rowId;
	$('#mdlRemoveAssessment #msgText').text('');
	$('#mdlRemoveAssessment #msgText').text(
			"Do you want to remove assessment " + assessmentName + "?");
	$('#mdlRemoveAssessment').modal().show();

}
$('#mdlRemoveAssessment #btnRemoveAssessment').on('click', function() {

	deleteRow("campaignAssessmentTable", removeAssessmentRowId);
	$('#mdlRemoveAssessment').modal('hide');
});


//add campaign to db
var addCampaign = function(addCampaignName, startDate, endDate,
		selectedAssessments, selectedSchools) {
	var requestData = {
		"campaignName" : addCampaignName,
		"startDate" : startDate,
		"endDate" : endDate,
		"assementIds" : selectedAssessments,
		"schoolIds" : selectedSchools,
		"status" : STATUS_PENDING
	}
	var campaign = customAjaxCalling("campaign", requestData, "POST");
	if (campaign.response == "shiksha-200") {
		
		/*
		 * var newRow =getNewRow(school); var table
		 * =$("#schoolTable").DataTable(); table.row.add($(newRow)).draw( false );
		 */
		loadCampaignListPage();
	}
	$(window).scrollTop(0);
}
// ////////////////////////////
// add campaign data/////////
// ///////////////////////////

var addCampaignData = function() {

	isSchoolAdded = ($('#campaignSchoolTable').dataTable().fnGetData().length == 0) ? 'no'
			: 'yes';
	isAssessmentAdded = ($('#campaignAssessmentTable').dataTable().fnGetData().length == 0) ? 'no'
			: 'yes';

	$('#mdlError #msgText').text('');
	if (isSchoolAdded == 'no' && isAssessmentAdded == 'no') {
		$(".outer-loader").hide();
		$('#mdlError #msgText').text(
				"Please select school(s) and assessment(s) for campaign.");
		$('#mdlError').modal().show();
		return;
	} else if (isSchoolAdded == 'no') {
		$(".outer-loader").hide();
		$('#mdlError #msgText').text("Please select school(s) for campaign.");
		$('#mdlError').modal().show();
		return;
	} else if (isAssessmentAdded == 'no') {
		$(".outer-loader").hide();
		$('#mdlError #msgText').text(
				"Please select assessment(s) for campaign.");
		$('#mdlError').modal().show();
		return;
	} else {
		addCampaignName = $('#txt_campaignName ').val();
		startDate = $('#statrtDatePicker').datepicker('getFormattedDate');
		endDate = $('#endDatePicker').datepicker('getFormattedDate');
		selectedAssessments = [];
		selectedSchools = [];
		$('#campaignAssessmentTable >tbody>tr').each(function(i, row) {
			selectedAssessments.push(parseInt(row.getAttribute('data-id')));
		});
		$('#campaignSchoolTable >tbody>tr').each(function(i, row) {
			selectedSchools.push(parseInt(row.getAttribute('data-id')));
		});

		 addCampaign(addCampaignName, startDate, endDate,
				selectedAssessments, selectedSchools);
	}

}


var restrictEndDate = function(e) {
	if (Date.parse($('#endDatePicker').datepicker('getDate')) <= Date.parse($(
			'#statrtDatePicker').datepicker('getDate'))) {
		e.preventDefault();
		$('#mdlError #msgText').text(" ");
		$('#mdlError #msgText').text(
				"End date can not be same/less than start date.");
		$('#mdlError').modal().show();
		return false;
	} else {
		return true;
	}
}

$(function() {
	initAndSetOptionsOfDatePicker($('#statrtDatePicker'), $('#endDatePicker'));

	$('#statrtDatePicker').datepicker({
		allowPastDates : false,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		}
	}).on(
			'changed.fu.datepicker dateClicked.fu.datepicker',
			function() {
				var startDate = $('#statrtDatePicker').datepicker(
						'getFormattedDate');
				if (startDate != "Invalid Date") {
					$("#campaignForm").data('formValidation').updateStatus(
							'startDate', 'VALID');
					// $('#endDatePicker').datepicker('setDate', new
					// Date($('#statrtDatePicker').datepicker('getFormattedDate')));
				}
			});

	setDataTablePagination("#campaignSchoolTable");
	setDataTablePagination("#campaignAssessmentTable");
	initGlobalValues();
	
	setOptionsForSchoolMultipleSelect($(addSchoolMdlSelectDivs.schoolElemet));

	$("#" + addSchoolForm).formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addSchool();

	});

	$("#" + addAssessmentFormId).formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		
		bindAssessmentToTable();
	});

	$('#campaignForm')
			.formValidation(
					{
						framework : 'bootstrap',
						icon : {
							validating : 'glyphicon glyphicon-refresh'
						},
						// This option will not ignore invisible fields which
						// belong to inactive panels
						excluded : ':disabled',
						fields : {
							campaignName : {
								validators : {
									notEmpty : {
										message : 'The campaign name is required'
									},
									regexp : {
										// regexp: /^[a-zA-Z\s]+$/,
										regexp : /^[^'?\"/\\]*$/,
										message : 'The campaign name does not contains these (", ?, \', /, \\) special characters'
									}
								}
							},
							startDate : {
								validators : {
									notEmpty : {
										message : 'The start date is required'
									},
								}
							},
							endDate : {
								validators : {
									notEmpty : {
										message : 'The end date is required'
									},
								}
							},

						}
					});

	$('#createCampaignWizard')
			.wizard()
			// Triggered when clicking the Next/Prev buttons
			.on(
					'actionclicked.fu.wizard',
					function(e, data) {
						var fv = $('#campaignForm').data('formValidation'), // FormValidation
																			// instance
						step = data.step, // Current step
						// The current step container
						$container = $('#campaignForm').find(
								'.step-pane[data-step="' + step + '"]');

						// Validate the container
						fv.validateContainer($container);

						var isValidStep = fv.isValidContainer($container);
						if (step == 1) {
							restrictEndDate(e);
							$('.dataTables_empty').attr('colspan', 33);
						}
						if (isValidStep === false || isValidStep === null) {
							// Do not jump to the target panel
							e.preventDefault();
						}
					})
			// Triggered when clicking the Complete button
			.on(
					'finished.fu.wizard',
					function() {
						var fv = $('#campaignForm').data('formValidation'), step = $(
								'#campaignForm').wizard('selectedItem').step, $container = $(
								'#campaignForm').find(
								'.step-pane[data-step="' + step + '"]');

						// Validate the last step container
						fv.validateContainer($container);

						var isValidStep = fv.isValidContainer($container);
						if (isValidStep === true) {
							spinnerStart();
							addCampaignData()
						}
					});
});
