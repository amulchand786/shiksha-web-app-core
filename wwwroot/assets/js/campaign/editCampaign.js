//global variables
var STATUS_START;
var STATUS_CLOSE;
var IN_PROGRESS_STATUS;

var addSchoolForm="addSchoolForm";
var addAssessmentFormId='addAssessmentForCampaignForm';
var selectedLocType;
var selectedLocations;
var selectedSchooltypes;

var selectedSourceIds;
var selectedGradeIds;
var selectedSubjectIds;
var selectedUnitIds;
var selectedStageIds;
var isAssessmentShown;
var isSchoolShown;

//to addcampaign data
var addCampaignName;
var startDate;
var endDate;
var selectedAssessments;
var selectedSchools;
var status;




//global selectDivIds
var addSchoolMdlSelectDivs = new Object();	
	addSchoolMdlSelectDivs.city 	="#mdlAddSchoolForCamp #divSelectCity";
	addSchoolMdlSelectDivs.block 	="#mdlAddSchoolForCamp #divSelectBlock";
	addSchoolMdlSelectDivs.NP 	="#mdlAddSchoolForCamp #divSelectNP";
	addSchoolMdlSelectDivs.GP 	="#mdlAddSchoolForCamp #divSelectGP";
	addSchoolMdlSelectDivs.RV 	="#mdlAddSchoolForCamp #divSelectRV";
	addSchoolMdlSelectDivs.village ="#mdlAddSchoolForCamp #divSelectVillage";
	addSchoolMdlSelectDivs.schoolTypeDiv ="#mdlAddSchoolForCamp #divSchoolType";
	addSchoolMdlSelectDivs.schoolDiv ="#mdlAddSchoolForCamp #divSchool";
	addSchoolMdlSelectDivs.schoolElement ="#mdlAddSchoolForCamp #selectBox_schoolByLocAndType";
	
	
	addSchoolMdlSelectDivs.schoolLocationType ="#mdlAddSchoolForCamp #selectBox_schoolLocationType";
	addSchoolMdlSelectDivs.schoolType ="#mdlAddSchoolForCamp #selectBox_schoolType";
	
	addSchoolMdlSelectDivs.resetLocDivCity ="#mdlAddSchoolForCamp #divResetLocCity";
	addSchoolMdlSelectDivs.resetLocDivVillage ="#mdlAddSchoolForCamp #divResetLocVillage";
	
	
	
//global selectDivIds for assessment of campaign
var addAssessmentMdlSelectDivs = new Object();	
	addAssessmentMdlSelectDivs.source 	="#mdlAddAssessmentForCampaign  #divSource";
	addAssessmentMdlSelectDivs.grade 	="#mdlAddAssessmentForCampaign  #divGrade";
	addAssessmentMdlSelectDivs.subject 	="#mdlAddAssessmentForCampaign  #divSubject";
	addAssessmentMdlSelectDivs.unit 	="#mdlAddAssessmentForCampaign  #divUnit";
	addAssessmentMdlSelectDivs.stage 	="#mdlAddAssessmentForCampaign  #divStage";
	addAssessmentMdlSelectDivs.assessment ="#mdlAddAssessmentForCampaign  #divAssessement";

//global selectElements for assessment of campaign
var addAssessmentMdlSelectElements = new Object();
	addAssessmentMdlSelectElements.source 	="#mdlAddAssessmentForCampaign  #selectBox_source";
	addAssessmentMdlSelectElements.grade 	="#mdlAddAssessmentForCampaign  #selectBox_grade";
	addAssessmentMdlSelectElements.subject 	="#mdlAddAssessmentForCampaign  #selectBox_subject";
	addAssessmentMdlSelectElements.unit 	="#mdlAddAssessmentForCampaign  #selectBox_unit";
	addAssessmentMdlSelectElements.stage 	="#mdlAddAssessmentForCampaign  #selectBox_stage";
	addAssessmentMdlSelectElements.assessment ="#mdlAddAssessmentForCampaign  #selectBox_assessment";
	addAssessmentMdlSelectElements.addButton ="#mdlAddAssessmentForCampaign  #addAssessment";	
		
	

//initialize global vars	
var initGlobalValues =function(){
	selectedLocType ='';
	selectedLocations =[];
	selectedSchooltypes =[];
	selectedSourceIds =[];
	selectedGradeIds =[];
	selectedSubjectIds =[];
	selectedUnitIds =[];
	selectedStageIds =[];
	isAssessmentShown =false;isSchoolShown=false;
	addCampaignName ='';
	startDate ='';
	endDate ='';
	selectedAssessments =[];
	selectedSchools =[];
	isBtnStartClicked =false;
	STATUS_START='In progress';
	STATUS_CLOSE='Completed';
	status ='';
}
	
/////////////////////////
//smart filter starts here//
/////////////////////////
//show or hide div as per school location type
$(addSchoolMdlSelectDivs.schoolLocationType).change(function(){
	//$(".outer-loader").show();
	hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),$(addSchoolMdlSelectDivs.schoolDiv));
	
	//reset multiselct
	$(addSchoolMdlSelectDivs.schoolElement).multiselect('destroy');
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	initSchoolTypeMultiselect();
	
	
	
	var typeCode =$(this).find('option:selected').data('code');
	selectedLocType =typeCode;
	reinitializeSmartFilter(elementIdMap);	
	$("#hrDiv").show();
	
	//reset form
	resetFormValidatonForLoc(addSchoolForm,"villageId");
	resetFormValidatonForLoc(addSchoolForm,"cityId");
	resetFormValidatonForLoc(addSchoolForm,"schoolType");
	resetFormValidatonForLoc(addSchoolForm,"school");
	
	if(typeCode.toLowerCase() =='U'.toLowerCase()){
		$("#"+addSchoolForm).data('formValidation').updateStatus('villageId', 'VALID');
		showDiv($(addSchoolMdlSelectDivs.city),$(addSchoolMdlSelectDivs.resetLocDivCity));
		hideDiv($(addSchoolMdlSelectDivs.block),$(addSchoolMdlSelectDivs.NP),$(addSchoolMdlSelectDivs.GP),$(addSchoolMdlSelectDivs.RV),$(addSchoolMdlSelectDivs.village),$(addSchoolMdlSelectDivs.resetLocDivVillage));
		displaySmartFilters("add");
	}else{
		$("#"+addSchoolForm).data('formValidation').updateStatus('cityId', 'VALID');
		
		hideDiv($(addSchoolMdlSelectDivs.city),$(addSchoolMdlSelectDivs.resetLocDivCity));
		showDiv($(addSchoolMdlSelectDivs.block),$(addSchoolMdlSelectDivs.NP),$(addSchoolMdlSelectDivs.GP),$(addSchoolMdlSelectDivs.RV),$(addSchoolMdlSelectDivs.village),$(addSchoolMdlSelectDivs.resetLocDivVillage));
		displaySmartFilters("add");
		positionFilterDiv("add");
	}
	if($(elementIdMap["State"]).find('option:selected').length==0){		
		hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),$(addSchoolMdlSelectDivs.schoolDiv));
	}
});


//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionFilterDiv =function(type){
	if(type=="add"){		
		$(addSchoolMdlSelectDivs.NP).insertAfter( $(addSchoolMdlSelectDivs.block));
		$(addSchoolMdlSelectDivs.RV).insertAfter( $(addSchoolMdlSelectDivs.GP));
	}
}



//displaying smart-filters
var displaySmartFilters =function(type){
	//$(".outer-loader").show();
	if(type=="add"){
		elementIdMap = {
				"State" : '#mdlAddSchoolForCamp #statesForZone',
				"Zone" : '#mdlAddSchoolForCamp #selectBox_zonesByState',
				"District" : '#mdlAddSchoolForCamp #selectBox_districtsByZone',
				"Tehsil" : '#mdlAddSchoolForCamp #selectBox_tehsilsByDistrict',
				"Block" : '#mdlAddSchoolForCamp #selectBox_blocksByTehsil',
				"City" : '#mdlAddSchoolForCamp #selectBox_cityByBlock',
				"Nyaya Panchayat" : '#mdlAddSchoolForCamp #selectBox_npByBlock',
				"Gram Panchayat" : '#mdlAddSchoolForCamp #selectBox_gpByNp',
				"Revenue Village" : '#mdlAddSchoolForCamp #selectBox_revenueVillagebyGp',
				"Village" : '#mdlAddSchoolForCamp #selectBox_villagebyRv'				
		};
		displayLocationsOfSurvey($(addSchoolMdlSelectDivs.schoolLocationType), $('#divFilter'));
	}
		
			
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){/*$(".outer-loader").hide();*/ $('#rowDiv1,#divFilter').show(); }, 100);
	
};


//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	$("#mdlResetLoc").modal("hide");
	hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),$(addSchoolMdlSelectDivs.schoolDiv));
	$('.outer-loader').show();
	/*if($(resetButtonObj).parents("form").attr("id")==addSchoolForm){*/
		resetFormValidatonForLoc(addSchoolForm,"villageId");
		resetFormValidatonForLoc(addSchoolForm,"cityId");
		reinitializeSmartFilter(elementIdMap);displaySmartFilters("add");
		var locTypeCode =$(addSchoolMdlSelectDivs.schoolLocationType).find('option:selected').data('code');
		if(locTypeCode!="" && locTypeCode!=null){
			if(locTypeCode.toLowerCase() =='U'.toLowerCase()){
				$("#"+addSchoolForm).data('formValidation').updateStatus('villageId', 'VALID');								
			}else{
				$("#"+addSchoolForm).data('formValidation').updateStatus('cityId', 'VALID');		
			}
		}
		$('.outer-loader').hide();
	/*}*/
}



//reset form
var resetLocatonAndForm =function(type){
	$('#divFilter').hide();
	if(type=="add"){
		resetFormById(addSchoolForm);	
	}
	reinitializeSmartFilter(elementIdMap);
	getSchoolTypeAndLoc();
}


//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
}

/////////////////////////
//smart filter ends here//
/////////////////////////

//show or hide schoolTypeDiv as per location
var showOrHideSchoolDiv =function(){
	hideDiv($(addSchoolMdlSelectDivs.schoolDiv));
	initSchoolTypeMultiselect();
	if(selectedLocType.toLowerCase() =='U'.toLowerCase()){
		if($(elementIdMap.City).find('option:selected').length>0){
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations =[];
			$(elementIdMap.City).each(function(index,ele){
				$(ele).find("option:selected").each(function(ind,option){
					if($(option).val()!="multiselect-all")
						selectedLocations.push( parseInt($(option).val()));
				});
			});
		}
	}else{		
		if($(elementIdMap.Village).find('option:selected').length>0){
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations =[];
			$(elementIdMap.Village).each(function(index,ele){
				$(ele).find("option:selected").each(function(ind,option){
					if($(option).val()!="multiselect-all")
						selectedLocations.push( parseInt($(option).val()));
				});
			});
		}			
	}//else
};








//get all schooltype and location and bind to respective list box at add school
var getSchoolTypeAndLoc =function(){	
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	$(addSchoolMdlSelectDivs.schoolLocationType).multiselect('destroy');
	hideDiv($(addSchoolMdlSelectDivs.schoolTypeDiv),$(addSchoolMdlSelectDivs.schoolDiv));
	$('#divFilter').hide();
	$('#hrDiv').hide();
	resetFormById(addSchoolForm);
	bindSchoolTypeForCampaignToListBox($(addSchoolMdlSelectDivs.schoolType),0);
	getAllSchoolTypeLocation();	
	setOptionsForAllMultipleSelect($(addSchoolMdlSelectDivs.schoolLocationType));
	initSchoolTypeMultiselect();
	$(addSchoolMdlSelectDivs.schoolType).multiselect('refresh');
	$(addSchoolMdlSelectDivs.schoolLocationType).multiselect('refresh');
}



//for school type...on drop down hide, it will fetch all the schools 
var initSchoolTypeMultiselect=function(){
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	resetFormValidatonForLoc(addSchoolForm,"schoolType");	
	bindSchoolTypeForCampaignToListBox($(addSchoolMdlSelectDivs.schoolType),0);
	$(addSchoolMdlSelectDivs.schoolType).multiselect({
		maxHeight: 120,
		includeSelectAllOption: true,
		dropUp: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		numberDisplayed: 1,		
		filterPlaceholder: 'Search here...',
		disableIfEmpty: true,
		onDropdownHide: function(event) {
			isSchoolShown =true;
			selectedSchooltypes =[];
			$(addSchoolMdlSelectDivs.schoolElement).multiselect('destroy');
			resetFormValidatonForLoc(addSchoolForm,"school");
			$(addSchoolMdlSelectDivs.schoolType).each(function(index,ele){
				$(ele).find("option:selected").each(function(ind,option){
					if($(option).val()!="multiselect-all")
						selectedSchooltypes.push( parseInt($(option).val()));
				});
			});
			if(selectedSchooltypes.length==0){
				hideDiv($(addSchoolMdlSelectDivs.schoolDiv));
				return;
			}
			var ss =getAndBindSchoolByLocAndType($(addSchoolMdlSelectDivs.schoolElement),selectedLocType,selectedLocations,selectedSchooltypes);
			//setOptionsForAllMultipleSelect($(addSchoolMdlSelectDivs.schoolElement));
			setOptionsForSchoolMultipleSelect($(addSchoolMdlSelectDivs.schoolElement));
			disableSchoolFromListBox();
			showDiv($(addSchoolMdlSelectDivs.schoolDiv));
			if(ss.length==0){
				$('#mdlError #msgText').text('');
				$('#mdlError #msgText').text("There are no schools of the above selections.");
				hideDiv($(addSchoolMdlSelectDivs.schoolDiv));
				$('#mdlError').modal().show();
			}
		}	
	});
}

var setOptionsForSchoolMultipleSelect = function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 150,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			dropUp: true,
		});
	});
}


//disable the schools if it has been added to table
var disableSchoolFromListBox =function(){
	console.log("disable schools...");
	selectedSchools =[];schoolIdList=[];var i=0;
	$('#campaignSchoolTable >tbody>tr').each(function(i,row){		
		selectedSchools.push( parseInt(row.getAttribute('data-id')));
	});
	$(addSchoolMdlSelectDivs.schoolElement).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				schoolIdList.push( parseInt($(option).data('id')));
		});
	});	
	$.grep(schoolIdList, function(el) {
		if ($.inArray(el, selectedSchools) != -1){
			var option = $(addSchoolMdlSelectDivs.schoolElement).find('option[value="' +el+ '"]');
			option.prop('disabled', true);
			$(option).addClass("disabled");
		}
		i++;
	});
	$(addSchoolMdlSelectDivs.schoolElement).find('.disabled').each(function(i,ele){
		$(ele).remove();
	});
	$(addSchoolMdlSelectDivs.schoolElement).multiselect('destroy');
	setOptionsForSchoolMultipleSelect(addSchoolMdlSelectDivs.schoolElement);
}
//add selected schools to the table
var addSchool =function(event){
	console.log("add school to campaign.....");
	campaignSchools =[];
	$(addSchoolMdlSelectDivs.schoolElement).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				selectedSchools =new Object();
				selectedSchools.id =$(this).data('id')
				selectedSchools.name =$(this).text();
				selectedSchools.state =$(this).data('statename');
				selectedSchools.zone =$(this).data('zonename');
				selectedSchools.dist =$(this).data('distname');
				selectedSchools.tehsil =$(this).data('tehsilname');
				selectedSchools.block =$(this).data('blockname');
				selectedSchools.city =$(this).data('cityname');
				selectedSchools.np =$(this).data('nyaypanchayatname');
				selectedSchools.gp =$(this).data('grampanchayatname');
				selectedSchools.rv =$(this).data('revenuevillagename');
				selectedSchools.village =$(this).data('villagename');
				campaignSchools.push(selectedSchools);
			}			
		});
	});
	createNewSchoolRow(campaignSchools);
	$('#mdlAddSchoolForCamp').modal('hide');
	enableTooltip();

}
var createNewSchoolRow =function(selectedSchools){
	if($('#campaignSchoolTable >tbody>tr').length==1){
		$("#campaignSchoolTable tbody").find(' tr').remove();
	}	
	$.each(selectedSchools,function(index,obj){					
			var row ="<tr id='"+obj.id+"' data-id='"+obj.id+"'>"+
				"<td></td>"+
				"<td title='"+obj.state+"'>"+obj.state+"</td>"+
				/*"<td title='"+obj.zone+"'>"+obj.zone+"</td>"+*/
				"<td title='"+obj.dist+"'>"+obj.dist+"</td>"+
				
				
				"<td title='"+obj.city+"'>"+obj.city+"</td>"+
				
				/*"<td title='"+obj.gp+"'>"+obj.gp+"</td>"+*/
				
				"<td title='"+obj.village+"'>"+obj.village+"</td>"+
				"<td title='"+obj.name+"'>"+obj.name+"</td>"+
				"<td><div  style='display:inline-flex;padding-left:0px;position:absolute;'><a   class='tooltip tooltip-top'	id='btnRemoveSchool'" +
				"onclick=removeSchool(&quot;"+obj.id+"&quot;,&quot;"+escapeHtmlCharacters(obj.name)+"&quot;); > <span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a></td>"+
				
				"</tr>";
			appendNewRow("campaignSchoolTable",row);
			isCampaignEdited=true;
	});		
}


//preview assessment
var previewAssessment =function(assessmentId,sgAssessmentCode,name){
	// open the survey page in SG Engine
	previewSurveyInSg(sgAssessmentCode,assmentPreviewUrl);
}



//remove school from table
var removeSchool =function(rowId,schoolName){
	removeSchoolRowId=rowId;
	var isSchoolRemovable =$('#campaignSchoolTable').dataTable().fnGetData().length==1?false:true;
	if(isSchoolRemovable == true){
		$('#mdlRemoveSchool #msgText').text('');	
		$('#mdlRemoveSchool #msgText').text("Do you want to remove school "+schoolName+ "?");
		$('#mdlRemoveSchool').modal().show();
	}else{
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There will be no more schools associated with this campaign if you delete it.At least one school should be there. First add a school and then delete the desired school.");
		$('#mdlError').modal().show()
	}
	
}
$('#mdlRemoveSchool #btnRemoveSchool').on('click',function(){
	//$(".outer-loader").show();

	var responseData =customAjaxCalling("editCampaign/"+editedCampaignId+"/school/"+removeSchoolRowId,null,"PUT");
	if(responseData.response=="shiksha-200"){
		deleteRow("campaignSchoolTable",removeSchoolRowId);
		$('#mdlRemoveSchool').modal('hide');
		isCampaignEdited =true;
		//$(".outer-loader").hide();
	}else{
		
	}
});





//////////////////////////////////////////



/////////////////////////////////
// Add Assessment For Campaign //
/////////////////////////////////

var initAssessment =function(){	
	//set multiselect options
	/*setOptionsForAssessmentMultiSelect($(addAssessmentMdlSelectElements.source),$(addAssessmentMdlSelectElements.grade),
									$(addAssessmentMdlSelectElements.subject),$(addAssessmentMdlSelectElements.unit),
									$(addAssessmentMdlSelectElements.stage),$(addAssessmentMdlSelectElements.assessment));
	resetFormValidatonForLoc(addAssessmentFormId,"assessment");
	resetFormValidatonForLoc(addAssessmentFormId,"source");
	resetFormValidatonForLoc(addAssessmentFormId,"stage");
	resetFormValidatonForLoc(addAssessmentFormId,"grade");
	resetFormValidatonForLoc(addAssessmentFormId,"subject");
	resetFormValidatonForLoc(addAssessmentFormId,"unit");
	hideDiv($(addAssessmentMdlSelectDivs.assessment));
	
	$('#addAssessment').css('display','none');
	$('#btnViewAssessment').css('display','block');
	
	$('#mdlAddAssessmentForCampaign').modal().show();*/

	//Issue-376........ don't remove...work under progress
	fnEmptyAll(addAssessmentMdlSelectElements.source,addAssessmentMdlSelectElements.stage,addAssessmentMdlSelectElements.grade,
			addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
	resetFormById(addAssessmentFormId);
	
	destroyRefresh(addAssessmentMdlSelectElements.source,addAssessmentMdlSelectElements.stage,addAssessmentMdlSelectElements.grade,
			addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
	$(addAssessmentMdlSelectElements.addButton).prop('disabled',true)
	fnBindSource();
}





//Issue-376 started..
//get assessmentList added in table
var fnAddedAssessments =function(){
	selectedAssessments =[];asmntIdList=[];var i=0;
	$('#campaignAssessmentTable >tbody>tr').each(function(i,row){
		if(row.getAttribute('data-id')!=null)
			selectedAssessments.push(row.getAttribute('data-id'));
	});
	console.log("assessments in table ....");
	console.log(selectedAssessments);
	return selectedAssessments;
}

//bind source
var fnBindSource =function(){
	assmntList =fnAddedAssessments();
	var rData ={
			"assementIds":assmntList
	}
	var sources =customAjaxCalling("source/assessments",rData,"POST");
	console.log(sources);
	fnEmptyAll(addAssessmentMdlSelectElements.source);
	
	if (sources!=""){
		if(sources[0].response=="shiksha-200"){			
			fnBindSourceToListBox(addAssessmentMdlSelectElements.source,sources);
		}
		showDiv($(addAssessmentMdlSelectDivs.source));
		hideDiv($(addAssessmentMdlSelectDivs.stage),$(addAssessmentMdlSelectDivs.grade),$(addAssessmentMdlSelectDivs.subject),
				$(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	}else{
		console.log("No sources remains....");
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more Question Paper.All Question Papers has been added.");
		$('#mdlError').modal().show();
	}
}
var fnBindSourceToListBox =function(element,data){
	$ele =$(element);
	$.each(data,function(index,obj){		
		$ele.append('<option value="'+obj.sourceId+'" data-id="' + obj.sourceId + '"data-name="' +obj.sourceName+ '">' +escapeHtmlCharacters(obj.sourceName)+ '</option>');
	});
	fnSetMultiselect(element);
}

//bind stage
var fnBindStage =function(){
	console.log("Bind Stage....");
	selectedSourceIds =[];
	getSelectedData(addAssessmentMdlSelectElements.source,selectedSourceIds);	
	assmntList =fnAddedAssessments();
	
	var rData ={
			"assementIds":assmntList,
			"sourceIds":selectedSourceIds,
	}
	var stages =customAjaxCalling("stage/assessments",rData,"POST");
	console.log(stages);
	fnEmptyAll(addAssessmentMdlSelectElements.stage);
	
	if (stages!=""){
		if(stages[0].response=="shiksha-200"){			
			fnBindStageToListBox(addAssessmentMdlSelectElements.stage,stages);
		}
		showDiv($(addAssessmentMdlSelectDivs.stage));
		hideDiv($(addAssessmentMdlSelectDivs.grade),$(addAssessmentMdlSelectDivs.subject),
				$(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	}else{
		console.log("No stage remains....");
		/*$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more assessments of selected source.All assessments has been added.");
		$('#mdlError').modal().show();*/
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Question Papers of selected source.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display','block');
	}
}
var fnBindStageToListBox =function(element,data){
	$ele =$(element);
	$.each(data,function(index,obj){		
		$ele.append('<option value="'+obj.stageId+'" data-id="' + obj.stageId + '"data-name="' +obj.stageName+ '">' +escapeHtmlCharacters(obj.stageName)+ '</option>');
	});
	fnSetMultiselect(element);
}


//bind grade
var fnBindGrade =function(){
	console.log("Bind grade....");	
	selectedSourceIds =[]; selectedStageIds =[];
	getSelectedData(addAssessmentMdlSelectElements.source,selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage,selectedStageIds);
	assmntList =fnAddedAssessments();
	
	var rData ={
			"assementIds":assmntList,
			"sourceIds":selectedSourceIds,
			"stageIds" : selectedStageIds
	};
	var grades =customAjaxCalling("grade/assessments",rData,"POST");
	console.log(grades);
	fnEmptyAll(addAssessmentMdlSelectElements.grade);
	
	if (grades!=""){
		if(grades[0].response=="shiksha-200"){			
			fnBindGradeToListBox(addAssessmentMdlSelectElements.grade,grades);
		}
		showDiv($(addAssessmentMdlSelectDivs.grade));
		hideDiv($(addAssessmentMdlSelectDivs.subject),$(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	}else{
		console.log("No grades remains....");
		/*$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more assessments of selected source and stage.All assessments has been added.");
		$('#mdlError').modal().show();*/
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Question Papers of selected source and stage.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display','block');
		
	}
}
var fnBindGradeToListBox =function(element,data){
	$ele =$(element);
	$.each(data,function(index,obj){		
		$ele.append('<option value="'+obj.gradeId+'" data-id="' + obj.gradeId + '"data-name="' +obj.gradeName+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
	});
	fnSetMultiselect(element);
}



//bind subject
var fnBindSubject =function(){
	console.log("Bind subjects....");
	selectedSourceIds =[];selectedStageIds =[];selectedGradeIds =[];	
	getSelectedData(addAssessmentMdlSelectElements.source,selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage,selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.grade,selectedGradeIds);
	assmntList =fnAddedAssessments();
	
	var rData ={
			"assementIds":assmntList,
			"sourceIds":selectedSourceIds,
			"stageIds" : selectedStageIds,
			"gradeIds" : selectedGradeIds,
			
	}
	var subjects =customAjaxCalling("subject/assessments",rData,"POST");
	console.log(subjects);
	fnEmptyAll(addAssessmentMdlSelectElements.subject);
	
	if (subjects!=""){
		if(subjects[0].response=="shiksha-200"){			
			fnBindSubjectToListBox(addAssessmentMdlSelectElements.subject,subjects);
		}
		showDiv($(addAssessmentMdlSelectDivs.subject));
		hideDiv($(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	}else{
		console.log("No subjects remains....");
		/*$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more assessments of selected source,stage and grade.All assessments has been added.");
		$('#mdlError').modal().show();*/
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Question Papers of selected source,stage and grade.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display','block');
		
	}
}
var fnBindSubjectToListBox =function(element,data){
	$ele =$(element);
	$.each(data,function(index,obj){		
		$ele.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-name="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
	});
	fnSetMultiselect(element);
}


//bind chapters
var fnBindChapter =function(){
	console.log("Bind subjects....");
	selectedSourceIds =[];selectedStageIds =[];selectedGradeIds =[];selectedSubjectIds =[];	
	getSelectedData(addAssessmentMdlSelectElements.source,selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage,selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.grade,selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.subject,selectedSubjectIds);
	assmntList =fnAddedAssessments();
	
	var rData ={
			"assementIds":assmntList,
			"sourceIds":selectedSourceIds,
			"stageIds" : selectedStageIds,
			"gradeIds" : selectedGradeIds,
			"subjectIds":selectedSubjectIds
			
	};
	var chapters =customAjaxCalling("unit/assessments",rData,"POST");
	console.log(chapters);
	fnEmptyAll(addAssessmentMdlSelectElements.unit);
	
	if (chapters!=""){
		if(chapters[0].response=="shiksha-200"){			
			fnBindChapterToListBox(addAssessmentMdlSelectElements.unit,chapters);
		}
		showDiv($(addAssessmentMdlSelectDivs.unit));
		hideDiv($(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
	}else{
		console.log("No chapters remains....");
		/*$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more assessments of selected source,stage,grade and subject.All assessments has been added.");
		$('#mdlError').modal.show();*/
		$('#alertdiv #exception').text('');
		$('#alertdiv #exception').text("There are no more Question Papers of selected source,stage,grade and subject.All Question Papers has been added.");
		$('#alertdiv #errorMessage').css('display','block');
		
	}
}
var fnBindChapterToListBox =function(element,data){
	$ele =$(element);
	$.each(data,function(index,obj){		
		$ele.append('<option value="'+obj.unitId+'" data-id="' + obj.unitId + '"data-name="' +obj.unitName+ '">' +escapeHtmlCharacters(obj.unitName)+ '</option>');
	});
	fnSetMultiselect(element);
}

//bind assessments
var fnBindAssessment =function(){
	console.log("Bind assessments.............");
	selectedSourceIds =[];selectedGradeIds =[];selectedSubjectIds =[];
	selectedUnitIds =[];selectedStageIds =[];
	getSelectedData(addAssessmentMdlSelectElements.source,selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage,selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.subject,selectedSubjectIds);
	getSelectedData(addAssessmentMdlSelectElements.grade,selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.unit,selectedUnitIds);
	
	assmntList =fnAddedAssessments();
	
	var rData ={
			"assementIds":assmntList,
			"sourceIds":selectedSourceIds,
			"stageIds":selectedStageIds,
			"gradeIds":selectedGradeIds,
			"subjectIds":selectedSubjectIds,
			"unitIds":selectedUnitIds
			
	};
	var assessments =customAjaxCalling("getAssessmentList",rData,"POST");
	console.log(assessments);
	fnEmptyAll(addAssessmentMdlSelectElements.assessment);
	
	if (assessments!=""){
		if(assessments[0].response=="shiksha-200"){			
			fnBindAssessmentToListBox(addAssessmentMdlSelectElements.assessment,assessments);
		}
		showDiv($(addAssessmentMdlSelectDivs.assessment));
		$('#mdlAddAssessmentForCampaign').modal().show();
		
	}else{
		console.log("No assessments remains....");
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more Question Papers of selected source,stage,grade,subject and chapter.All Question Papers has been added.");
		$('#mdlError').modal.show();
	}
}
var fnBindAssessmentToListBox =function(element,data){
	$ele =$(element);
	$.each(data,function(index,obj){		
		$ele.append('<option value="'+obj.assessmentId+'"data-id="' + obj.assessmentId + '"data-sgCode="' + obj.sgAssessmentCode +'"data-sourcename="' + obj.sourceName +'"data-stagename="' + obj.stageName +'"data-gradename="' + obj.gradeName + '"data-subjectname="' + obj.subjectName +'"data-unitname="' + obj.unitName + '"  value="' + obj.assessmentId + '">' +obj.assessmentName + '</option>');
	});
	fnSetMultiselect(element);
}



//empty select box
var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}

var destroyRefresh =function(){
	$.each(arguments,function(i,obj){
		$(obj).multiselect('destroy');
	});
	//$(ele).multiselect('destroy');	
}
var hideAlert =function(){
	$('#alertdiv #errorMessage').css('display','none');
}
var disableAddButton = function(){
	$(addAssessmentMdlSelectElements.addButton).prop('disabled',true);
}

//set multiselect option for element
var fnSetMultiselect =function(ele){	
	$(ele).multiselect({
		maxHeight: 150,
		includeSelectAllOption: true,		
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',
		onDropdownHide : function(event) {
			if(ele == (addAssessmentMdlSelectElements.source)){
				hideAlert();
				disableAddButton();
				if($(addAssessmentMdlSelectElements.source).find('option:selected').length==0){
					hideDiv($(addAssessmentMdlSelectDivs.stage),$(addAssessmentMdlSelectDivs.grade),$(addAssessmentMdlSelectDivs.subject),
							$(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
				/*	hideAlert();
					disableAddButton();*/
					destroyRefresh(addAssessmentMdlSelectElements.stage,addAssessmentMdlSelectElements.grade,
							addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
				}else{
					/*hideAlert();*/
					destroyRefresh(addAssessmentMdlSelectElements.stage,addAssessmentMdlSelectElements.grade,
							addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
					fnBindStage();
				}
			}else if(ele == (addAssessmentMdlSelectElements.stage)){
				hideAlert();
				disableAddButton();
				if($(addAssessmentMdlSelectElements.stage).find('option:selected').length==0){
					hideDiv($(addAssessmentMdlSelectDivs.grade),$(addAssessmentMdlSelectDivs.subject),
						$(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
					destroyRefresh(addAssessmentMdlSelectElements.grade,addAssessmentMdlSelectElements.subject,
									addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
					//hideAlert();
				}else{
					//hideAlert();
					destroyRefresh(addAssessmentMdlSelectElements.grade,addAssessmentMdlSelectElements.subject,
							addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
					fnBindGrade();
				}
			}else if(ele == (addAssessmentMdlSelectElements.grade)){
				hideAlert();
				disableAddButton();
				if($(addAssessmentMdlSelectElements.grade).find('option:selected').length==0){
					hideDiv($(addAssessmentMdlSelectDivs.subject),$(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
					destroyRefresh(addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
					//hideAlert();
				}else{
					//hideAlert();
					destroyRefresh(addAssessmentMdlSelectElements.subject,addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
					fnBindSubject();
				}
			}else if(ele == (addAssessmentMdlSelectElements.subject)){
				hideAlert();
				disableAddButton();
				if($(addAssessmentMdlSelectElements.subject).find('option:selected').length==0){
					hideDiv($(addAssessmentMdlSelectDivs.unit),$(addAssessmentMdlSelectDivs.assessment));
					destroyRefresh(addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
					//hideAlert();
				}else{
					//hideAlert();
					destroyRefresh(addAssessmentMdlSelectElements.unit,addAssessmentMdlSelectElements.assessment);
					fnBindChapter();
				}
			}else if(ele == (addAssessmentMdlSelectElements.unit)){
				hideAlert();
				disableAddButton();
				if($(addAssessmentMdlSelectElements.grade).find('option:selected').length==0){
					hideDiv($(addAssessmentMdlSelectDivs.assessment));
					destroyRefresh(addAssessmentMdlSelectElements.assessment);
					//hideAlert();
				}else{
					//hideAlert();
					destroyRefresh(addAssessmentMdlSelectElements.assessment);
					fnBindAssessment();
				}
			}else if(ele == (addAssessmentMdlSelectElements.assessment)){
				if($(addAssessmentMdlSelectElements.grade).find('option:selected').length >0)
					$(addAssessmentMdlSelectElements.addButton).prop('disabled',false);
			}
		}
	});
}
//Issue-376 end..


































//get assessment on basis of filters
var getAssessment =function(obj){
	//if already assessment selected, then add to table
	if(isAssessmentShown==true){
		console.log(isAssessmentShown);
		bindAssessmentToTable();
		return true;
	}
	
	isAssessmentShown =true;
	selectedSourceIds =[];selectedGradeIds =[];selectedSubjectIds =[];
	selectedUnitIds =[];selectedStageIds =[];
	getSelectedData(addAssessmentMdlSelectElements.source,selectedSourceIds);
	getSelectedData(addAssessmentMdlSelectElements.stage,selectedStageIds);
	getSelectedData(addAssessmentMdlSelectElements.subject,selectedSubjectIds);
	getSelectedData(addAssessmentMdlSelectElements.grade,selectedGradeIds);
	getSelectedData(addAssessmentMdlSelectElements.unit,selectedUnitIds);
	
	$(addAssessmentMdlSelectElements.assessment).multiselect('destroy');
	resetFormValidatonForLoc(addAssessmentFormId,"assessment");
	
	
	//bind assessment to assessment list box
	getAndBindAssessmentByParentFilter($(addAssessmentMdlSelectElements.assessment),selectedSourceIds,
										selectedStageIds,selectedGradeIds,
										selectedSubjectIds,selectedUnitIds);
	
	setOptionsForAllMultipleSelect($(addAssessmentMdlSelectElements.assessment));
	showDiv($(addAssessmentMdlSelectDivs.assessment));
	disableAssessmentFromListBox();
	$('#addAssessment').css('display','block');
	$('#btnViewAssessment').css('display','none');
	
}
//disable the assessments if it has been added to table
var disableAssessmentFromListBox =function(){
	console.log("disable assessment...");
	selectedAssessments =[];asmntIdList=[];var i=0;
	$('#campaignAssessmentTable >tbody>tr').each(function(i,row){		
		selectedAssessments.push( parseInt(row.getAttribute('data-id')));
	});
	$(addAssessmentMdlSelectElements.assessment).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				asmntIdList.push( parseInt($(option).data('id')));
		});
	});	
	$.grep(asmntIdList, function(el) {
		if ($.inArray(el, selectedAssessments) != -1){
			var option = $(addAssessmentMdlSelectElements.assessment).find('option[value="' +el+ '"]');
			option.prop('disabled', true);
			option.parent('label').parent('a').parent('li').remove();
		}
		i++;
	});
	$(addAssessmentMdlSelectElements.assessment).multiselect('refresh');	
}

//get the selected values and push to respective array for assessment
var getSelectedData =function(elementObj,collector){
	$(elementObj).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				//collector.push( parseInt($(option).val()));
				collector.push( parseInt($(option).data('id')));
		});
	});
}
//customize all multiple select box for assessment
//on any change, assessment selectbox will hide
var setOptionsForAssessmentMultiSelect = function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 100,
			includeSelectAllOption: true,
			dropUp: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			numberDisplayed: 2,		
			filterPlaceholder: 'Search here...',
			disableIfEmpty: true,
			onChange: function(event) {
				if(isAssessmentShown==true){					
					$("#"+addAssessmentFormId).data('formValidation').updateStatus("assessment", 'VALID');
					hideDiv($(addAssessmentMdlSelectDivs.assessment));
					$('#addAssessment').css('display','none');
					$('#btnViewAssessment').css('display','block');
					isAssessmentShown =false;
				}
			}
		});		
	});
}

//add assessments to table
var bindAssessmentToTable =function(){
	campaignAssessments =[];
	$(addAssessmentMdlSelectElements.assessment).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				selectedAssessment =new Object();
				selectedAssessment.id =$(this).data('id')
				selectedAssessment.name =$(this).text();
				selectedAssessment.sgcode =$(this).data('sgcode');
				selectedAssessment.source =$(this).data('sourcename');
				selectedAssessment.stage =$(this).data('stagename');
				selectedAssessment.grade =$(this).data('gradename');
				selectedAssessment.subject =$(this).data('subjectname');
				selectedAssessment.unit =$(this).data('unitname');
				campaignAssessments.push(selectedAssessment);
			}			
		});
	});
	createNewAssessmentRow(campaignAssessments);
	$('#mdlAddAssessmentForCampaign').modal('hide');
	enableTooltip();

}


var createNewAssessmentRow =function(selectedAssessments){
	var assessmentRowNo=0;
	if($('#campaignAssessmentTable >tbody>tr').length==1){
		$("#campaignAssessmentTable tbody").find(' tr').remove();
	}
	rowNo =$('#campaignAssessmentTable tr:last').attr('id')==null?1:$('#campaignAssessmentTable tr:last').attr('id');
	
		$.each(selectedAssessments,function(index,obj){
			var disableRemoveButton='';
			if(campaignStatus==IN_PROGRESS_STATUS){
				disableRemoveButton='disabled="disabled"';
			}
			var row ="<tr id='"+obj.id+"' data-id='"+obj.id+"'>"+
				"<td></td>"+
				"<td title='"+obj.source+"'>"+obj.source+"</td>"+
				"<td title='"+obj.stage+"'>"+obj.stage+"</td>"+
				"<td title='"+obj.grade+"'>"+obj.grade+"</td>"+
				"<td title='"+obj.subject+"'>"+obj.subject+"</td>"+
				"<td title='"+obj.unit+"'>"+obj.unit+"</td>"+
				"<td title='"+obj.name+"'>"+obj.name+"</td>"+
				/*"<td title='"+obj.sgcode+"'>"+obj.sgcode+"</td>"+*/ //Issue:468
				"<td><div   style='display:inline-flex;padding-left:0px;position:absolute;'><a   class='tooltip tooltip-top'"+disableRemoveButton	+"id='btnRemoveAssessment'" +
				"onclick=removeAssessment(&quot;"+obj.id+"&quot;,&quot;"+escapeHtmlCharacters(obj.name)+"&quot;); >  <span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a>"+
				"<a   style='margin-left:10px;' class='tooltip tooltip-top'	id='btnPreviewAssessment"+obj.assessmentId+"'"+
				"onclick=previewAssessment(&quot;"+obj.id+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgcode)+"&quot;,&quot;"+escapeHtmlCharacters(obj.name)+"&quot;); >  <span class='tooltiptext'>Preview</span><span class='fa fa-slideshare fa-fw font-size12' ></span></a></div></td>"+
				"</tr>";
			appendNewRow("campaignAssessmentTable",row);
			isCampaignEdited=true;
		});		
}


//preview assessment
var previewAssessment =function(assessmentId,sgAssessmentCode,name){
	// open the survey page in SG Engine
	previewSurveyInSg(sgAssessmentCode,assmentPreviewUrl);
}
//preview survey in sg
var previewSurveyInSg =function(sgAssessmentCode,previewUrl){
	previewUrl =previewUrl.replace('SURVEY_CODE',sgAssessmentCode);
	window.open(previewUrl);	
}




//remove assessment from table
var removeAssessment =function(rowId,assessmentName){
	removeAssessmentRowId=rowId;
	var isAssmntRemovable =$('#campaignAssessmentTable').dataTable().fnGetData().length==1?false:true;
	if(isAssmntRemovable == true){
		$('#mdlRemoveAssessment #msgText').text('');	
		$('#mdlRemoveAssessment #msgText').text("Do you want to remove Question Papers "+assessmentName+ "?");
		$('#mdlRemoveAssessment').modal().show();
	}else{
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There will be no more Question Papers associated with this campaign if you delete it.At least one assessment should be there. First add a assessment and then delete the desired assessment.");
		$('#mdlError').modal().show()
	}
	
	
	
}
$('#mdlRemoveAssessment #btnRemoveAssessment').on('click',function(e){	
	
	var responseData =customAjaxCalling("editCampaign/"+editedCampaignId+"/assessment/"+removeAssessmentRowId,null,"PUT");
	if(responseData.response=="shiksha-200"){
		deleteRow("campaignAssessmentTable",removeAssessmentRowId);
		$('#mdlRemoveAssessment').modal('hide');
		//$(".outer-loader").hide();
	}else{
		
	}
});


///////////////////////////////////////
// disable if start button is clicked//
//////////////////////////////////////
$('#btnStart').on('click',function(){
	campaignStatus =STATUS_START;
	 //this.disabled = true;
	 isBtnStartClicked =true;
	  $('#startCampaign').modal();
	 var allSelectedAssessments =[];allSelectedSchools =[];
		$('#campaignAssessmentTable >tbody>tr').each(function(i,row){		
			allSelectedAssessments.push((row.getAttribute('data-name')));
		});
		$('#campaignSchoolTable >tbody>tr').each(function(i,row){		
			allSelectedSchools.push( (row.getAttribute('data-name')));
		});
		var allSelectedSchool = allSelectedSchools ;
		var allSelectedAssessment = allSelectedAssessments;
		$('#startCampaignSchools').text(allSelectedSchool);
		$('#startCampaignAssessments').text(allSelectedAssessment);
	//editCampaignData(this);
});
$('#btnStartCampCancel').on('click',function(){
	campaignStatus ='Pending';
	 //this.disabled = true;
	 isBtnStartClicked =false;
	 return true;
});





$("#btnClose").on('click',function(){
	var startButtonPropIsDisabled=$('#btnStart').prop('disabled');
	
   if(startButtonPropIsDisabled==true)
	   {
	   $('#closeCampaignConfiramation').modal();
	   
		$('#colseCampaignRemarkDiv').hide();
		$('#confirmationMessageDiv').show();
	   }
});
$("#closeCampaignConfiramationBtn").on('click',function(){
	$('#confirmationMessageDiv').hide();
	$('#colseCampaignRemarkDiv').show();
});
$("#closeCamPaignOkButton").on('click',function(){
	$('.outer-loader').show();	
	var closeButtonPropIsDisabled=$('#btnClose').prop('disabled');
	campaignStatus =STATUS_CLOSE;
	$('#btnClose').prop('disabled',false);
	var closeComment= $('#closeCampaignTextAreaMsgtxt').val();
	var requestData ={
			"campaignId":editedCampaignId,
			"status":status,
			"closeComment":closeComment

	}
	campaignData =customAjaxCalling("closeCampaign", requestData, "POST");
	if(campaignData.response=="shiksha-200"){
		$('#closeCampaignConfiramation').modal('hide');
		loadCampaignListPage();
	}
	$('.outer-loader').hide();
	return;
});
////////////////////////////////////////
//disable or enable start/close button on load
///////////////////////////////////////
var disableOrEnableStatusButton =function(){
	if(campaignStatus==STATUS_START){
		$('#btnStart').prop('disabled',true)
		$('#btnClose').prop('disabled',false)
	}
}





//////////////////////////////
// edit campaign data/////////
/////////////////////////////

var editCampaignData =function(event){
	//$(".outer-loader").show();
	console.log("edit campaign.......... ");

	isSchoolAdded =($('#campaignSchoolTable').dataTable().fnGetData().length==0)?'no':'yes';
	isAssessmentAdded =($('#campaignAssessmentTable').dataTable().fnGetData().length==0)?'no':'yes';
	
	$('#mdlError #msgText').text('');
	if(isSchoolAdded=='no'&& isAssessmentAdded=='no'){
		$('#mdlError #msgText').text("Please select school(s) and assessment(s) for campaign.");		
		$('#startCampaign').modal('hide');
		$('#mdlError').modal().show();
		return false;
	}else if(isSchoolAdded=='no'){
		$('#mdlError #msgText').text("Please select school(s) for campaign.");		
		$('#startCampaign').modal('hide');
		$('#mdlError').modal().show();
		return false;
	}else if(isAssessmentAdded=='no'){
		$('#mdlError #msgText').text("Please select assessment(s) for campaign.");		
		$('#startCampaign').modal('hide');
		$('#mdlError').modal().show();
		return false;
	}else{			
			editCampaignName =$('#txtEditcampaignName ').val();
			startDate =$('#statrtDatePicker').datepicker('getFormattedDate');
			endDate =$('#endDatePicker').datepicker('getFormattedDate');
			var statusC ='';		
			statusC = campaignStatus;
			console.log(statusC);
			selectedAssessments =[];selectedSchools =[];
			$('#campaignAssessmentTable >tbody>tr').each(function(i,row){		
				selectedAssessments.push( parseInt(row.getAttribute('data-id')));
			});
			$('#campaignSchoolTable >tbody>tr').each(function(i,row){		
				selectedSchools.push( parseInt(row.getAttribute('data-id')));
			});
			
			console.log(selectedAssessments.length);
			console.log(selectedSchools.length);
			editCampaign(editCampaignName,startDate,endDate,selectedAssessments,selectedSchools,statusC);
	}
	
}
//add campaign to db
var editCampaign =function(editCampaignName,startDate,endDate,assmnts,schools,statusCode){
	var requestData ={
		"campaignId":editedCampaignId,
		"campaignName":editCampaignName,
		"startDate":startDate,
		"endDate":endDate,
		"assementIds":assmnts,
		"schoolIds":schools,
		"status":statusCode
	}
	var startButtonPropIsDisabled=$('#btnStart').prop('disabled');
	var campaign;
	if(statusCode==STATUS_START && startButtonPropIsDisabled==true){
		 campaign =customAjaxCalling("campaign/editInProgressCampaign", requestData, "POST");
	}else if(isBtnStartClicked ==true){
		campaign =customAjaxCalling("campaign/start", requestData, "PUT");
		isBtnStartClicked =false;
	}else{
		campaign =customAjaxCalling("campaign", requestData, "PUT");
	}
	if(campaign.response=="shiksha-200"){
		$(".outer-loader").hide();
		loadCampaignListPage();
	}else {
		$(".outer-loader").hide();		
	}
	$(window).scrollTop(0);
}



//restrict end date
var restrictEndDate =function(e){	
	if(Date.parse($('#endDatePicker').datepicker('getDate'))<=Date.parse($('#statrtDatePicker').datepicker('getDate'))){
		e.preventDefault();
		$('#mdlError #msgText').text(" ");
		$('#mdlError #msgText').text("End date can not be same/less than start date.");		
		$('#mdlError').modal().show();
		return false;
	}else{
		return true;
	}
}


//check if any modification has been occured and without saving user is navigating
//then open a pop-up and make confirmation
var checkIsEdited =function(e,obj){
	
	var flagFoo =(isCampaignEdited==true)?true:
								editCampaignName!=($('#txtEditcampaignName ').val())?true:
									editStartDate!=($('#statrtDatePicker').datepicker('getFormattedDate'))?true:
										editEndDate!=($('#endDatePicker').datepicker('getFormattedDate'))?true:false;
	if(flagFoo==true){
		e.preventDefault();
		$('#mdlEditConfirmation #msgText').text(" ");
		$('#mdlEditConfirmation #msgText').text("You have not saved the changes. Do you want to save the changes ?");		
		$('#mdlEditConfirmation').modal().show();
	}
	fooNavigation =obj;
}
$("#btnYes").on('click',function(e){
	$('#mdlEditConfirmation').modal().hide();
	var flagFoo =restrictEndDate(e);
	if(flagFoo==true){
		editCampaignData();
		if($(fooNavigation).attr('id')=='home'||$(fooNavigation).attr('id')=='navHome')
			window.location =fooNavigation;
		else
			window.location =home;
	}
});
$("#btnNo").on('click',function(){
	if($(fooNavigation).attr('id')=='home'||$(fooNavigation).attr('id')=='navHome')
		window.location =fooNavigation;
	else
		window.location =home;
});

var startButtonPropIsDisabled=$('#btnStart').prop('disabled');
$(function() {
	//initAndSetOptionsOfDatePicker($('#statrtDatePicker'), $('#endDatePicker'));
	
	isCampaignEdited =false;
	($('#home, #navHome, #navCampaign, #navMasters, #navReports, #navRightMenu')).click(function(event){
		checkIsEdited(event,this);
	});
		
	
	
	$('#statrtDatePicker').datepicker({
		allowPastDates: false,
		momentConfig: {
			culture: 'en', // change to specific culture
			format: 'DD-MMM-YYYY' // change for specific format
		}
	}) .on('changed.fu.datepicker dateClicked.fu.datepicker', function(e, date) {
		var startDate =$('#statrtDatePicker').datepicker('getFormattedDate');
		if(startDate !="Invalid Date"){
			$("#editCampaignForm").data('formValidation').updateStatus('startDate', 'VALID');
		}
	});
	$('#endDatePicker').datepicker({
		allowPastDates: false,
		momentConfig: {
			culture: 'en', // change to specific culture
			format: 'DD-MMM-YYYY' // change for specific format
		}
	}) .on('changed.fu.datepicker dateClicked.fu.datepicker', function(e, date) {
		var endDate =$('#endDatePicker').datepicker('getFormattedDate');
		if(endDate !="Invalid Date"){
			$("#editCampaignForm").data('formValidation').updateStatus('endDate', 'VALID');
		}

	});
	
	
    setDataTablePagination("#campaignSchoolTable");
    setDataTablePagination("#campaignAssessmentTable");
	initGlobalValues();
	setOptionsForAllMultipleSelect($(addSchoolMdlSelectDivs.schoolElement));	
	disableOrEnableStatusButton();
	$('#txtEditcampaignName').val(editCampaignName);
	$('#statrtDatePicker').datepicker('setDate', editStartDate);
	$('#endDatePicker').datepicker('setDate', editEndDate);
	
	
	$("#"+addSchoolForm).formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},		
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addSchool(e);

	});
	
	$("#"+addAssessmentFormId).formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},		
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		//getAssessment(e);
		bindAssessmentToTable(e);
	});
	
	
	$('#editCampaignForm').formValidation({
		framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            //invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        // This option will not ignore invisible fields which belong to inactive panels
        excluded: ':disabled',
        fields : {        
			campaignName: {
                validators: {
                    notEmpty: {
                        message: 'The campaign name is required'                        
                    },
                    regexp: {
                        //regexp: /^[a-zA-Z\s]+$/,
                    	regexp: /^[^'?\"/\\]*$/,
                        message: 'The campaign name does not contains these (", ?, \', /, \\) special characters'
                    }
                }
            },
            startDate: {
           	 validators: {
                    notEmpty: {
                        message: 'The start date is required'
                    },
                }
           },
           endDate: {
               validators: {
                   notEmpty: {
                       message: 'The end date is required'
                   },
               }
           },
		}     
    });

    $('#editCampaignWizard')
        .wizard()
        // Triggered when clicking the Next/Prev buttons
        .on('actionclicked.fu.wizard', function(e, data) {
            var fv         = $('#editCampaignForm').data('formValidation'), // FormValidation instance
                step       = data.step,                              // Current step
                // The current step container
                $container = $('#editCampaignForm').find('.step-pane[data-step="' + step +'"]');

            // Validate the container
            fv.validateContainer($container);
            if(step==1){
            	restrictEndDate(e);
            	$('#editCampaignRefName').text($('#txtEditcampaignName ').val());
            }
            var isValidStep = fv.isValidContainer($container);
            if (isValidStep === false || isValidStep === null) {
                // Do not jump to the target panel
                e.preventDefault();
            }
        })

        // Triggered when clicking the Complete button
        .on('finished.fu.wizard', function(e) {
            var fv         = $('#editCampaignForm').data('formValidation'),
                step       = $('#editCampaignForm').wizard('selectedItem').step,
                $container = $('#editCampaignForm').find('.step-pane[data-step="' + step +'"]');

            // Validate the last step container
            fv.validateContainer($container);

            var isValidStep = fv.isValidContainer($container);
            if (isValidStep === true) {
            	editCampaignData(e)
            }
        });
	
});