var isAddTagg = function(tableId, element) {

	if (!$(element).is(":checked")) {
		var message = customAjaxCalling("tagg/" + tableId, null, "DELETE");

	} else {

		var message = customAjaxCalling("tagg/" + tableId, null, "POST");

	}

};

var addUserCredential = function() {
	var url = $('#url').val();
	var userName = $('#userName').val();
	var password = $('#password').val();
	

	var jsonData = {
			"endPoint": url,
		"login" : userName,
		"password" : password,
	};
	$("#userCredentialButton").show();
	var message = customAjaxCalling("credentials", jsonData, "POST");
	if (message.response == "shiksha-200") {
		/*
		 * $("#user_credential").hide(); $("#userCredential").show();
		 * $('#userCredential #showLogin').text(message.login);
		 * $('#userCredential #showPassword').text(message.password);
		 * $('#userCredential #mdPassword').text(message.md5Password);
		 */
		$("#userCredentialForm #successMessage").show();

		setTimeout(function() {

			location.reload();

		}, 1500)

	} else {
		$("#userCredentialForm #errorMessage").show();
		setTimeout(function() {
			location.reload();
		}, 1500)
	}

};

var addAppCredential = function() {
	var appId = $("#appID").val();
	var secret_key = $("#secret_key").val();
	$("#surveyGamezApiSaveButton").show();
	var jsonData = {
		"appId" : appId,
		"secretKey" : secret_key
	}
	var message = customAjaxCalling("apiCredentials", jsonData, "POST");
	if (message.response == "shiksha-200") {
		/*
		 * $("#api_credential").hide(); $("#apiCredential").show();
		 * $('#apiCredential #showAppId').text(message.appId); $('#apiCredential
		 * #showSecretId').text(message.secretKey);
		 */
		$("#apiCredentialForm #successMessage").show();

		setTimeout(function() {

			location.reload();

		}, 1500)

	} else {
		$("#userCredentialForm #errorMessage").show();
		setTimeout(function() {
			location.reload();
		}, 1500)
	}
};

$('#userCredentialForm').formValidation({
	framework : 'bootstrap',
	feedbackIcons : {
		valid : 'glyphicon glyphicon-ok',
		invalid : 'glyphicon glyphicon-remove',
		validating : 'glyphicon glyphicon-refresh'
	},

	fields : {
		url : {
			validators : {
				callback : {
					message : '      ',
					callback : function() {
						// Get the selected options
						var options = $(
								'#url')
								.val();
						return (options != "NONE");
					}
				}
			}
		},

		userName : {
			validators : {
				callback : {
					message : 'User Name should not empty.',
					callback : function(value, validator, $field) {
						var regex = /^[A-Za-z0-9_@./#&+-]{1,100}$/;
						return regex.test(value);
					}
				}
			}
		},

		password : {
			validators : {
				callback : {
					message : 'Password  should not empty.',
					callback : function(value, validator, $field) {
						var regex = /^[A-Za-z0-9_@./#&+-]{1,50}$/;
						return regex.test(value);
					}
				}
			}
		},
	}
}).on('err.form.fv', function(e) {
	e.preventDefault();
}).on('success.form.fv', function(e) {
	e.preventDefault();
	addUserCredential();
});

$('#apiCredentialForm').formValidation({
	framework : 'bootstrap',
	feedbackIcons : {
		valid : 'glyphicon glyphicon-ok',
		invalid : 'glyphicon glyphicon-remove',
		validating : 'glyphicon glyphicon-refresh'
	},

	fields : {
		secret_key : {
			validators : {
				callback : {
					message : 'Secret Key should not empty.',
					callback : function(value, validator, $field) {
						var regex = /^[A-Za-z0-9_@./#&+-]{1,50}$/;
						return regex.test(value);
					}
				}
			}
		},

		appID : {
			validators : {
				callback : {
					message : 'App ID  should not empty.',
					callback : function(value, validator, $field) {
						var regex = /^[A-Za-z0-9_@./#&+-]{1,50}$/;
						return regex.test(value);
					}
				}
			}
		},
	}
}).on('err.form.fv', function(e) {
	e.preventDefault();
}).on('success.form.fv', function(e) {
	e.preventDefault();
	addAppCredential();
});
