var  colorClass='';
var tableName ='tblPlanAssmnt';

var formId='planAssessmentForm';
var START_STATUS='Yet to start';
var IN_PROGRESS_STATUS='In progress';
var COMPLETED_STATUS='Completed';
var SKIPPED_STATUS='Skipped'
	var CLOSED_BY_FS='Closed By FS';
var CLOSED_BY_RC='Closed By RC';
var pageContextElements =new Object();
pageContextElements.campaign ='#selectBox_campaign';




/////////////////////////////
//get all plan assessment data

var getAllAssessmentPlannerData= function(){
	$(".outer-loader").show();

	var campaignDataName=$('#selectBox_campaign').find('option:selected').data("name");
	var campaignDataStartDate=$('#selectBox_campaign').find('option:selected').data("startdate");
	var campaignDataEndDate=$('#selectBox_campaign').find('option:selected').data("enddate");
	var selectedCampaignId=$('#selectBox_campaign').find('option:selected').val();
	$('#campaignName').html(campaignDataName);
	$('#campaignStartDate').html(campaignDataStartDate);
	$('#campaignEndDate').html(campaignDataEndDate);
	var requestData ={
			"campaignId":selectedCampaignId,
	}
	colorClass = $('#t0').prop('class');
	var responseData =customAjaxCalling("assessmentplanner",requestData, "POST");
	$('#planAssessmentTableDiv').css('display','block');
	$('#rowDiv1').css("display","block");
	$('#campaignNamePanel1,#campaignNamePanel2').css("display","block");
	deleteAllRow(tableName);
	$.each(responseData,function(index,plannerData){

		createNewRow(plannerData);

		applyPermissions(plannerData.assessmentPlannerId);
	});
	fnzUpdateDtTable();
	setShowAllTagColor(colorClass);
	$(".outer-loader").hide();
	return;

};
var getAllAssessmentplannerByCampaignId=function(){
	var selectedCampaignId=$('#selectBox_campaign').find('option:selected').val();
	window.location="assessmentplanner/"+selectedCampaignId;
}
//create new row
var createNewRow =function(obj){
	//$(".outer-loader").show();

	if($('#tblPlanAssmnt >tbody>tr').length==1){
		$("#tblPlanAssmnt tbody").find(' tr').remove();
	}	
	totalStudentsResponded =(obj.totalStudentsResponded)==0?0:obj.totalStudentsResponded;
	
	var btnSkip='';
	var btnStart='';
	var btnEdit='';
	var btnCloseByRC='';
	var btnCloseByFS='';
	var hideCloseByRC='';
	var hideCloseByFS='';
	var hideClosePlan='';
	var	btnDetails='';
	if(totalStudentsResponded==0){
		hideCloseByRC='display:none';
		hideCloseByFS='display:none';
	}else{
		hideCloseByRC='';
		hideCloseByFS='';
	}
	if(closeAssessmentplanByPermission==false ){
		hideCloseByRC='display:none';
     }
	
	if(zRoleCode!="FS"){
		hideCloseByFS='display:none';
	}else{
		if(closeAssessmentplanByPermission && closeAssessmentPlanByFsPermission==false ){
			hideCloseByFS='display:none';
		}else{
			hideCloseByFS="";
		}
	}

	var assessmentPlannerId=obj.assessmentPlannerId;
	var spanDiv="";
	var schoolName=escapeHtmlCharacters(obj.schoolVm.schoolName);
	var gradeName=escapeHtmlCharacters(obj.gradeName);
	var sectionName=escapeHtmlCharacters(obj.sectionVm.sectionName);
	var statusDataId="id='"+obj.assessmentPlannerId+"_status'";
	var statusName="";

	if(obj.status==CLOSED_BY_FS){
		statusName=IN_PROGRESS_STATUS;
	}else if(obj.status==CLOSED_BY_RC){
		statusName=COMPLETED_STATUS;;
	}else{
		statusName=obj.status
	}
	if(totalStudentsResponded==0){
		hideCloseByRC='display:none';
		hideCloseByFS='display:none';
	}
	var editIcon='<a  class="tooltip tooltip-top" id="btnEdit'+obj.assessmentPlannerId+'"'+
	"onclick=editAssessmentPlanner(&quot;"+obj.assessmentPlannerId+"&quot;,&quot;"+obj.startDate+"&quot;,&quot;"+obj.endDate+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.campaignVm.startDate+"&quot;,&quot;"+obj.campaignVm.endDate+"&quot;);>"+
	'<span class="tooltiptext">Edit</span><span class="glyphicon glyphicon-edit font-size12"></span></a>'

	var skipIcon='<a  style="margin-left:10px;" class="tooltip tooltip-top" id="btnSkip'+obj.assessmentPlannerId+'"'+
	"onclick=skipAssessmentPlan(&quot;"+obj.assessmentPlannerId+"&quot;,&quot;"+obj.gradeId+"&quot;,&quot;"+obj.statusId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+schoolName+"&quot;,&quot;"+gradeName+"&quot;,&quot;"+sectionName+"&quot;);>"+
	'<span class="tooltiptext">Skip</span><span class="glyphicon glyphicon-share-alt font-size12"></span></a>'
	var viewDetailsIcon='<a  style="margin-left:10px;" class="tooltip tooltip-top" id="btnDetails'+obj.assessmentPlannerId+'"'+
	"onclick=detailViewPlan(&quot;"+obj.assessmentPlannerId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.schoolVm.schoolName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.gradeName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.sectionVm.sectionName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.subjectName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.stageName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.unitName)+"&quot;);>"+
	'<span class="tooltiptext">View details</span><span class="glyphicon glyphicon-eye-open font-size12"></span></a>'

	var startIcon='<a  style="margin-left:10px;" class="tooltip tooltip-top" id="btnStart'+obj.assessmentPlannerId+'"'+
	"onclick=startAssmentPlan(&quot;"+obj.assessmentPlannerId+"&quot;,&quot;"+obj.gradeId+"&quot;,&quot;"+obj.statusId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.stageId+"&quot;,&quot;"+obj.subjectId+"&quot;);>"+
	'<span class="tooltiptext">Start</span><span class="glyphicon glyphicon-play font-size12"></span></a>'

	var closeByRCIcon='<a  style="margin-left:10px; '+hideCloseByRC+'" class="tooltip tooltip-top" id="btnCloseByRC'+obj.assessmentPlannerId+'"'+
	"onclick=closeByRC(&quot;"+obj.assessmentPlannerId+"&quot;,&quot;"+obj.gradeId+"&quot;,&quot;"+obj.statusId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.totalStudents+"&quot;,&quot;"+obj.totalStudents+"&quot;);>"+
	'<span class="tooltiptext">Close Question Paper</span><span class="glyphicon glyphicon-remove font-size12"></span></a>'

	var closeByFSIcon='<a  style="margin-left:10px; '+hideCloseByFS+'" class="tooltip tooltip-top" id="btnCloseByFS'+obj.assessmentPlannerId+'"'+
	"onclick=closeByFS(&quot;"+obj.assessmentPlannerId+"&quot;,&quot;"+obj.gradeId+"&quot;,&quot;"+obj.statusId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.totalStudents+"&quot;,&quot;"+obj.totalStudents+"&quot;);>"+
	'<span class="tooltiptext">Close Question Paper</span><span class="glyphicon glyphicon-remove font-size12"></span></a>'
   
	var commentIcon='<a  style="margin-left:10px;" class="tooltip tooltip-top" id="btnComment'+obj.assessmentPlannerId+'"'+
	"onclick=commentAssessment(&quot;"+obj.assessmentPlannerId+"&quot;,&quot;"+obj.gradeId+"&quot;,&quot;"+obj.statusId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;);>"+
	'<span class="tooltiptext">Comment</span> <span class="glyphicon glyphicon-list-alt"></span></a>'

	if(obj.status==START_STATUS ){
		btnSkip=skipIcon;
		btnStart=startIcon;
		btnEdit=editIcon;
		btnCloseByRC=closeByRCIcon;
		btnCloseByFS=closeByFSIcon;
		btnDetails=viewDetailsIcon;
		spanDiv="<span class='label label-default text-uppercase' style='margin-top: 2px;'>Pending</span>";
		//spanDiv="Pending";
		

	}else if(obj.status==IN_PROGRESS_STATUS){
		btnSkip="";
		btnStart="";
		btnEdit=editIcon;
		btnCloseByRC=closeByRCIcon;
		btnCloseByFS=closeByFSIcon;
		btnDetails=viewDetailsIcon;
		spanDiv="<span class='label label-warning text-uppercase' style='margin-top: 2px;'>"+statusName+"</span>";
		//spanDiv="In progress";

	}else if(obj.status==SKIPPED_STATUS){
		btnSkip="";
		btnStart="";
		btnEdit="";
		btnCloseByRC="";
		btnCloseByFS="";
		btnDetails=viewDetailsIcon;
		spanDiv="<span class='label label-info text-uppercase' style='margin-top: 2px;'>"+statusName+"</span>"							


	}else if(obj.status==COMPLETED_STATUS){
		btnSkip="";
		btnStart="";
		btnEdit="";
		btnCloseByRC="";
		btnCloseByFS="";
		btnDetails=viewDetailsIcon;
		spanDiv="<span class='label label-success text-uppercase' style='margin-top: 2px;'>"+statusName+"</span>";
		// spanDiv="Completed";

	}
	else if(obj.status==CLOSED_BY_RC){
		btnSkip="";
		btnStart="";
		btnEdit="";
		btnCloseByRC="";
		btnCloseByFS="";
		btnDetails=viewDetailsIcon;
		spanDiv="<span class='label label-success text-uppercase' style='margin-top: 2px;'>"+statusName+"</span>";
		// spanDiv="Completed";

	}
	else if(obj.status==CLOSED_BY_FS){
		btnSkip="";
		btnStart="";
		btnEdit="";
		btnCloseByRC=closeByRCIcon;
		btnCloseByFS="";
		btnDetails=viewDetailsIcon;
		spanDiv="<span class='label label-warning text-uppercase' style='margin-top: 2px;'>"+statusName+"</span>";
		// spanDiv="Completed";

	}
	
	var row =
		"<tr id='"+obj.assessmentPlannerId+"' data-id='"+obj.assessmentPlannerId+"'>"+
		"<td></td>"+
		"<td title='"+obj.campaignVm.campaignName+"'>"+obj.campaignVm.campaignName+"</td>"+
		"<td title='"+obj.sourceName+"'>"+obj.sourceName+"</td>"+
		"<td title='"+obj.stageName+"'>"+obj.stageCode+"</td>"+
		"<td title='"+obj.schoolVm.schoolName+"'>"+obj.schoolVm.schoolName+"</td>"+
		"<td title='"+obj.gradeName+"'>"+obj.gradeName+"</td>"+
		"<td title='"+obj.sectionVm.sectionName+"'>"+obj.sectionVm.sectionName+"</td>"+
		"<td title='"+obj.subjectName+"'>"+obj.subjectName+"</td>"+
		"<td title='"+obj.assessmentName+"'>"+obj.assessmentName+"</td>"+
		"<td  id='"+obj.assessmentPlannerId+"_status' title='"+statusName+"' >"+spanDiv+"</td>"+
		/*"<td title='"+obj.totalStudents+"'>"+obj.totalStudents+"</td>"+*/
		"<td title='"+obj.totalStudentsResponded+"'>"+totalStudentsResponded+" of "+obj.totalStudents+"</td>"+
		"<td id='"+obj.assessmentPlannerId+"_startDate' title='"+obj.startDate+"'>"+obj.startDate+"</td>"+
		"<td id='"+obj.assessmentPlannerId+"_endDate' title='"+obj.endDate+"'>"+obj.endDate+"</td>"+
		"<td >" +
		'<div  class="div-tooltip">'+btnCloseByRC+btnCloseByFS+
		"</div ></td><td>"  +
		'<div  class="div-tooltip">'+btnEdit+btnSkip+btnStart+commentIcon+ btnDetails+
		"</div ></td>" +                                                                                                              
		"</tr>";                                                                                                                                                                 
	appendNewRow(tableName,row);
	
}



var applyPermissions=function(assessmentPId){
	if (!editPlanAssessmentPermission){
		$("#btnEdit"+assessmentPId).remove();
	}
	if (!skipPlanAssessmentPermission){
		$("#btnSkip"+assessmentPId).remove();
	}
	if (!startPlanAssessmentPermission){
		$("#btnStart"+assessmentPId).remove();


	}
	if (!closeAssessmentPlanByRcPermission){
		$("#btnCloseByRC"+assessmentPId).remove();
	}
	if (!closeAssessmentPlanByFsPermission){
		$("#btnCloseByFS"+assessmentPId).remove();
	}


}




var startAssessmentPlannerId;
var startGradeId;
var startStatusId;
var totalStudents;
var totalStudentsResponded;
var startStageId;
var startSubjectId;
var campaignStartDate;
var campaignEndDate;
var assessmentDataId;
var schoolDataId;
var sectionDataId;
var chapterDataId;
//edit assessmentplanner enddate and startDate
var editAssessmentPlanner=function(id,editStartDate,editEndDate,name,cmpstartDate,cmpendDate){
	$('#editAssesmentPlannerSaveButton').prop('disabled',true);
	campaignStartDate=cmpstartDate;
	campaignEndDate=cmpendDate;
	$('#startDateEndDateValidationMsgDiv').hide();
	$('#sdateBetweenCampValidationMsgDiv').hide();
	var assessmentStartDate=(editStartDate=="N/A"?new moment(new Date()).format('DD-MMM-YYYY'):editStartDate);
	var assessmentEndDate=(editEndDate=="N/A"?new moment(new Date()).format('DD-MMM-YYYY'):editEndDate);
	initAssessmentDatePicker('#editAssessmentForm #startDatePicker','#editAssessmentForm','startDate',assessmentStartDate);
	initAssessmentDatePicker('#editAssessmentForm #endDatePicker','#editAssessmentForm','endDate',assessmentEndDate);
	//validateEndDate(e)
	$('#editAssessmentPlannerId').val(id);
    $("#timeLineEndDiv").removeClass("has-error");
	$("#edit_assessment").modal().show();
}


var skipAssessmentPlan=function(assessmentPlannerId,gradeId,statusId,assismentName,schoolName,gradeName,sectionName){
	startGradeId=gradeId;
	startStatusId=statusId;
	startAssessmentPlannerId=assessmentPlannerId;
	var textmessage='Do you want to Skip Assessment '+ assismentName+' for School: '+schoolName+' Grade: '+gradeName+' Section: '+sectionName+'?';
	var successMessage=+ assismentName+' Assessment Skipped';
	$('#skipAssessmentMessage').text(textmessage);
	$("#skipAssessmentModal").modal();


}
var detailViewPlan=function(assessmentPlannerId,assessmentName,schoolName,gradeName,sectionName,subjectName,stageName,unitName){
	window.location=baseContextPath+"/report/questionPaperPlanWiseResult/"+assessmentPlannerId;
	/*startAssessmentPlannerId=assessmentPlannerId;
	$('#schoolNameSpan').text(schoolName);
	$('#gradeNameSpan').text(gradeName);
	$('#sectionNameSpan').text(sectionName);
	$('#subjectNameSpan').text(subjectName);
	$('#assessmentNameSpan').html(assessmentName);
	$('#stageNameSpan').text(stageName);
	

	var a = '<ul>', b = '</ul>', m = [],result='';
	var allUnitData='';
	var temp = new Array();
	temp = unitName.split(",");
	$.each(temp, function (index, value) {
		 m[index] = '<li style="margin-left: 60px;">' + value + '</li>';
		 allUnitData=allUnitData+m[index];
	});
	$('#unitNameSpan').html(allUnitData);
	//	$('#unitNameSpan').html(unitName);

	deleteAllRow('tableDetailView');
	var jsonData={
			"assessmentPlannerId":startAssessmentPlannerId
	}
	var viewPlanData =customAjaxCalling("getDetailsviewByPlanId",jsonData,'POST');
	if(viewPlanData!=null){
		$.each(viewPlanData,function(index,obj){
			var score=(obj.marks!=null?obj.marks:"")
			var result="";
			var comment=(obj.teacherComments!=null?obj.teacherComments:"");
			if(obj.statusName=="Completed"){

				result=(obj.flagNeedsMoreTests=='true' || obj.flagNeedsMoreTests==true?"Below":"Above")
			}
			var msgStatus=	addClasToStatus(obj.statusName);
			
			var row =
				"<tr id='"+obj.roll+"' data-id='"+obj.roll+"'>"+
				"<td title='"+obj.roll+"'>"+obj.roll+"</td>"+
				"<td title='"+obj.studentName+"'>"+obj.studentName+"</td>"+
				"<td title='"+obj.statusName+"'>"+msgStatus+"</td>"+
				"<td title='"+score+"'>"+score+"</td>"+
				"<td title='"+comment+"'>"+comment+"</td>"+
				"<td title='"+result+"'>"+result+"</td>"+

				"</tr>";                                                                                                                                                                 
			appendNewRow('tableDetailView',row);
		});

		setDataTablePaginationDetailsView("#tableDetailView");
		fnColSpanIfDTEmpty('tableDetailView',6);
		
		$("#mdlDetailView").modal();
	}
*/
}
var setDataTablePaginationDetailsView =function(tableId){	
	$(tableId).dataTable({
		"pagingType": "numbers",		
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		 "lengthMenu": [5,10,20,30,40,50,60,70,80,90,100  ],
		 "bLengthChange": true,
		 /*"scrollX": true,*/	
		 "bDestroy": true,
		 "autoWidth": false,
		 "stateSave": false,
		 "iDisplayLength": 100,
		 "fnDrawCallback": function ( oSettings ) {
	           },
	        "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
	       
		    "aaSorting": []
	});	
}
var startAssmentPlan=function(assessmentPlannerId,gradeId,statusId,name,stageId,subjectId){
	resetFormById('startAssessmentForm');
	$("#textAreaMsgtxt").val('');
	startAssessmentPlannerId=assessmentPlannerId;
	startGradeId=gradeId;
	startStatusId=statusId;
	startStageId=stageId;
	startSubjectId=subjectId;
	if(2==parseInt(stageId)){
	var jsonData={
			"assessmentPlannerId":startAssessmentPlannerId,
	 }
	var isPTExistedForPAT =customAjaxCalling("checkPATHaveAnyPTAssessment",jsonData,'POST');
	
	if(isPTExistedForPAT==false|| isPTExistedForPAT=='false'){
		$('#start_Assessment').modal('hide');
		$('#msgText').text("Assessment is not started because PT assessment is not completed or not found");
		$('.outer-loader').hide();
		$('#mdlError').modal();
	}else{
		fnShowStartAssessmentPlanModal(name);
		}
	}else{
		fnShowStartAssessmentPlanModal(name);
		
	}
}
var fnShowStartAssessmentPlanModal=function(name){
	var textmessage='Do you want to Start Assessment '+ name+'?';
	var successMessage=+ name+' Assessment started';
	$('#assessmentMessage').text(textmessage);
	limitTextArea($('#textAreaMsg'));
	$('#start_Assessment').modal();
}
var closeByRC=function(assessmentPlannerId,gradeId,statusId,name,totalStudents,totalStudentsResponded){
	$('#closeByRCTextAreaMsgtxt').val('');
	var remainstudent=(totalStudents-totalStudentsResponded);
	var studentsNotRespondedMsg=remainstudent+" Student pendding for assessment";
	startAssessmentPlannerId=assessmentPlannerId;
	startGradeId=gradeId;
	startStatusId=statusId;
	var textmessage='Do you want to Close Assessment '+ name+'?';
	var successMessage=+ name+' Assessment started';
	$('#assessmentMessage').text(textmessage);
	limitTextArea($('#closeByRCTextAreaMsg'));
	if(remainstudent>0){
		$('#closeByRCTtotalStudentsResponded').text(studentsNotRespondedMsg);
	}
	resetFormById('close_byRCAssessmentForm');
	$('#close_byRCAssessment').modal();
}

var closeByFS=function(assessmentPlannerId,gradeId,statusId,name,totalStudents,totalStudentsResponded){
	//$('#start_Assessment').modal();
	$('#closeByFSTextAreaMsgtxt').val('');
	var remainstudent=(totalStudents-totalStudentsResponded);
	var studentsNotRespondedMsg=remainstudent+" Student pendding for assessment";
	startAssessmentPlannerId=assessmentPlannerId;
	startGradeId=gradeId;
	startStatusId=statusId;
	var textmessage='Do you want to Close Assessment '+ name+'?';
	var successMessage=+ name+' Assessment colsed by FS';
	$('#closeByFSAssessmentMessage').text(textmessage);
	limitTextArea($('#closeByFSTextAreaMsg'));

	$('.modal').on('hidden.bs.modal', function(){
		$(this).find('form')[0].reset();
	});
	if(remainstudent>0){
		$('#closeByFSTtotalStudentsResponded').text(studentsNotRespondedMsg);
	}
	resetFormById('close_byFSAssessmentForm');
	$('#close_byFSAssessment').modal();

}


var editAssessmentStartAndEndDate=function(){
	$(".outer-loader").show();
	var editAssessmentPlannerId=$('#editAssessmentPlannerId').val();
	var assessmentStartDate=$('#startDatePicker').datepicker('getFormattedDate');
	var assessmentEndDate=$('#endDatePicker').datepicker('getFormattedDate');
	var jsonData={
			"assessmentPlannerId":editAssessmentPlannerId,
			"startDate":assessmentStartDate,
			"endDate":assessmentEndDate
	};
	var editAssessmentPlanData =customAjaxCalling("editAssessmentPlanner",jsonData,'POST');
	if(editAssessmentPlanData!=null){
		if(editAssessmentPlanData.response=="shiksha-200"){
			$(".outer-loader").hide();
			var editAssessmentId=editAssessmentPlanData.assessmentPlannerId;
			deleteRow(tableName, editAssessmentId);
			createNewRow(editAssessmentPlanData);
			fnzUpdateDtTable();
			$("#edit_assessment").modal('hide')
		}else{
			$(".outer-loader").hide();
		}
	}else{
		$(".outer-loader").hide();
	}
}

var skipAssessment=function(){
	$('.outer-loader').show();
	var jsonData={
			"assessmentPlannerId":startAssessmentPlannerId,
			"gradeId":startGradeId,
			"statusId":startStatusId
	};
	var skipAssesmentPlanData =customAjaxCalling("skipAssessment",jsonData,'POST');
	if(skipAssesmentPlanData!=null){
		if(skipAssesmentPlanData.response=="shiksha-200"){
			var skipAssesmentId=skipAssesmentPlanData.assessmentPlannerId;
			deleteRow(tableName, skipAssesmentId);
			createNewRow(skipAssesmentPlanData);
			fnzUpdateDtTable();
			$('.outer-loader').hide();
			$('#skipAssessmentModal').modal('hide');
			$('#success-msg').show();
			$('#showMessage').text("Assessment Skipped");
			$("#success-msg").fadeOut(3000);


		}else{
			$('.outer-loader').hide();

		}
	}
	$('.outer-loader').hide();
	return;
}
var startAssessment=function(){

	$('.outer-loader').show();	
	var startComment=$("#textAreaMsgtxt").val();
	var jsonData={
			"assessmentPlannerId":startAssessmentPlannerId,
			"gradeId":startGradeId,
			"statusId":startStatusId,
			"stageId":startStageId,
			"subjectId":startSubjectId,
			"startComment":startComment

	}
	var startAssesmentPlanData =customAjaxCalling("startAssessmentplan",jsonData,'POST');
	if(startAssesmentPlanData!=null){
		if(startAssesmentPlanData.response=="shiksha-200"){
			var startAssesmentPlanId=startAssesmentPlanData.assessmentPlannerId;
			deleteRow(tableName, startAssesmentPlanId);
			createNewRow(startAssesmentPlanData);
			//fnzUpdateDtTable();
         
			$('#start_Assessment').modal('hide');
			$('#success-msg').show();
			$('#showMessage').text("Assessment started");
			$("#success-msg").fadeOut(3000);
			//	$('#'+startAssessmentPlannerId+' td:nth-child(12)').text(msg);
			//updateStatusColumn(msg);
			fnzUpdateDtTable();
		}else if(startAssesmentPlanData.isSchoolHaveStudent==true){
			$('#start_Assessment').modal('hide');
			$('#msgText').text("Assessment is not started because assessment have no student");
			$('.outer-loader').hide();
			$('#mdlError').modal();

		}else if(startAssesmentPlanData.isSubjectHaveTeacher==true){
			$('#start_Assessment').modal('hide');
			$('#msgText').text("Assessment is not started because subject have no teacher");
			$('.outer-loader').hide();
			$('#mdlError').modal();

		}else if(startAssesmentPlanData.response=="Assessment not started"){
			$('#start_Assessment').modal('hide');
			$('#msgText').text("Assessment is not started because PT assessment is not completed or not found");
			$('.outer-loader').hide();
			$('#mdlError').modal();

		}
		else if(startAssesmentPlanData.response=="Assessment not found"){
			$('#start_Assessment').modal('hide');
			$('#msgText').text("Assessment is not started because PT assessment is not found");
			$('.outer-loader').hide();
			$('#mdlError').modal();

		}else if(startAssesmentPlanData.response=="shiksha-400") {
			$('#start_Assessment').modal('hide');
			$('#msgText').text("PAT assessment is not started because  there are no student for PAT assessment");
			$('.outer-loader').hide();
			$('#mdlError').modal();

		}else {
			$('#start_Assessment').modal('hide');
			$('#msgText').text("An error occurred while processing this request. Please contact to IT support");
			$('.outer-loader').hide();
			$('#mdlError').modal();

		}
	}
	$('.outer-loader').hide();
	return;
}
var closeByRCAssessment=function(assessmentPlannerId,gradeId,statusId,name){
	$(".outer-loader").show();
	var rCCloseComment= $('#closeByRCTextAreaMsgtxt').val();
	var jsonData={
			"assessmentPlannerId":startAssessmentPlannerId,
			"gradeId":startGradeId,
			"statusId":startStatusId,
			"rCCloseComment":rCCloseComment
	};
	var closeByRCAssessmentPlanData =customAjaxCalling("closeAssessmentplanByPermission",jsonData,'POST');
	if(closeByRCAssessmentPlanData!=null){
		if(closeByRCAssessmentPlanData.response=="shiksha-200"){
			//var msg=startAssesmentPlanData.responseMessage;
			var closeByRCAssessmentPlanId=closeByRCAssessmentPlanData.assessmentPlannerId;
			deleteRow(tableName, closeByRCAssessmentPlanId);
			createNewRow(closeByRCAssessmentPlanData);
			$(".outer-loader").hide();
			$('#close_byRCAssessment').modal('hide');
			$('#success-msg').show();
			$('#showMessage').text("Assessment colsed by RC");
			$("#success-msg").fadeOut(3000);
			fnzUpdateDtTable();

		}else{
			$(".outer-loader").hide();
		}
	}
	$(".outer-loader").hide();
}
/*var disableActionButton=function(btnSkip,btnStart,btnEdit,btnCloseByRC,btnCloseByFS){

	$(btnSkip+startAssessmentPlannerId).remove();
	$(btnStart+startAssessmentPlannerId).remove();
	$(btnEdit+startAssessmentPlannerId).remove();
	$(btnCloseByRC+startAssessmentPlannerId).remove();
	$(btnCloseByFS+startAssessmentPlannerId).remove();
	$(btnSkip+startAssessmentPlannerId).addClass('notactive');
	$(btnStart+startAssessmentPlannerId).addClass('notactive');
	$(btnEdit+startAssessmentPlannerId).addClass('notactive');
	$(btnCloseByRC+startAssessmentPlannerId).addClass('notactive');
	$(btnCloseByFS+startAssessmentPlannerId).addClass('notactive');
}*/
var updateStatusColumn=function(msg){
	var message="";
	if(msg==CLOSED_BY_FS){
		message=IN_PROGRESS_STATUS
	}else if(msg==CLOSED_BY_RC){
		message=COMPLETED_STATUS;
	}else{
		message=msg
	}
	var statusClass	=addClasToStatus(message);
	$('#'+startAssessmentPlannerId+"_status").find("span").remove();
	$('#'+startAssessmentPlannerId+"_status").append(statusClass);
	$('#'+startAssessmentPlannerId+"_status").prop("title",message)
}
var closeByFSAssessment=function(assessmentPlannerId,gradeId,statusId,name){
	$('.outer-loader').show();
	var fsCloseComment= $('#closeByFSTextAreaMsgtxt').val();
	var jsonData={
			"assessmentPlannerId":startAssessmentPlannerId,
			"gradeId":startGradeId,
			"statusId":startStatusId,
			"fSCloseComment":fsCloseComment
	};
	var closeByFSAssessmentPlanData =customAjaxCalling("closeByFSAssessment",jsonData,'POST');
	if(closeByFSAssessmentPlanData!=null){
		if(closeByFSAssessmentPlanData.response=="shiksha-200"){
			var closeByFSAssessmentPlanId=closeByFSAssessmentPlanData.assessmentPlannerId;
			deleteRow(tableName, closeByFSAssessmentPlanId);
			createNewRow(closeByFSAssessmentPlanData);
			$('.outer-loader').hide();
			$('#close_byFSAssessment').modal('hide');
			$('#success-msg').show();
			$('#showMessage').text("Assessment started");
			$("#success-msg").fadeOut(3000);
		}else{
			$('.outer-loader').hide();
		}
	}
	$('.outer-loader').hide();
	return;

}
var addClasToStatus=function(msg){
	if(msg==START_STATUS){

		return "<span class='label label-default text-uppercase'>"+msg+"</span>"

	}
	else if(msg==IN_PROGRESS_STATUS){
		return "<span class='label label-warning text-uppercase'>"+msg+"</span>"

	}
	else if(msg==COMPLETED_STATUS){
		return "<span class='label label-success text-uppercase'>"+msg+"</span>"

	}
	else if(msg==SKIPPED_STATUS){
		return "<span class='label label-info text-uppercase'>Absent</span>"

	}
}
Number.prototype.between  = function (min, max) {
	return this >= min && this <= max;
};
var fnDateParsing=function(data){
	var sArray =data.split('-');
	var monthData =sArray[1];
	var dateData =sArray[0];
	var yearData =sArray[2];
	var dateStringData =Date.parse(monthData+' '+dateData+' ,'+yearData);
	return dateStringData;
}
function validateEndDate(e){
	startDateData=$('#startDatePicker').datepicker('getFormattedDate')
	endDateData=$('#endDatePicker').datepicker('getFormattedDate')
	var startDate =fnDateParsing(startDateData);
	var endDate =fnDateParsing(endDateData);
	var campStartDate=fnDateParsing(campaignStartDate);
	var campEndDate=fnDateParsing(campaignEndDate);
	var startDateIsBetween=startDate.between(campStartDate,campEndDate);
	var endDateIsBetween=endDate.between(campStartDate,campEndDate);

	/*if(startDateIsBetween==false ){
		fnShowStartDateEndValidationMsgShow('<small>Start Date & End Date of Assessment should be between Campaign\'s Start Date & End Date.</small>');
		e.preventDefault();
		return false;
	}else if(endDateIsBetween==false){
		fnShowStartDateEndValidationMsgShow('<small>Start Date & End Date of Assessment should be between Campaign\'s Start Date & End Date.</small>');
		e.preventDefault();
		return false;
	}else*/ 
	if (endDate<startDate) {
		fnShowStartDateEndValidationMsgShow('<small>End Date should be greater than Start Date.</small>');
		e.preventDefault();
		return false;
	} else {
		$('#startDateEndDateValidationMsg').text('')
		$('#editAssesmentPlannerSaveButton').prop('disabled',false);
		$('#startDateEndDateValidationMsgDiv').css('display','none');
		$('#timeLineEndDiv').removeClass('has-error');
		return true;
	}
}
fnShowStartDateEndValidationMsgShow=function(msg){
	$('#startDateEndDateValidationMsg').text('')
	$('#editAssesmentPlannerSaveButton').prop('disabled',true);
	$('#timeLineEndDiv').addClass('has-error');
	$('#startDateEndDateValidationMsg').css('display','block');
	$('#startDateEndDateValidationMsgDiv').css('display','block');
	$('#startDateEndDateValidationMsg').html(msg)

}


var initAssessmentDatePicker=function(datePickerId,editformId,field,existedDate){

	$(datePickerId).datepicker({
		allowPastDates: false,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		},
		restricted: [{
			from: -Infinity,
			to: currentYrStartDate
		},
		{
			from: currentYrEndDate,
			to: Infinity
		}]
	}) .on('changed.fu.datepicker dateClicked.fu.datepicker', function(e, date) {
		$(editformId).formValidation('revalidateField', field);
	validateEndDate(e);

	});
	$(datePickerId).datepicker('setDate',existedDate);
}


/*var limitTextAreaStart =function(msgId){
	var maxchars = 300;
	$('textarea').keyup(function () {
		var tlength = $(this).val().length;
		$(this).val($(this).val().substring(0, maxchars));
		var tlength = $(this).val().length;
		remain = maxchars - parseInt(tlength);
		$(msgId).text(remain);
	});
}*/

//popup click on comment icon
var commentAssessment=function(planAssessmentId,gradeId,statusId,assessmentName){

	var comment = customAjaxCalling('assessmentCommentById/'+planAssessmentId, null, "GET");

	var rcComment=comment.rCCloseComment;
	var fsComment=comment.fSCloseComment
	var teacherComment=comment.teacherComments;
	var startComment=comment.startComment;
	$('#assessmentCommentModel #startComment').text('' );
	$('#assessmentCommentModel #pendingStudentMsg').text('' );
	$('#assessmentCommentModel #fsCloseComment').text('' );
	$('#assessmentCommentModel #rcCloseComment').text('' );
	$('#assessmentCommentModel #teacherComments').text('' );
	var studentPending=comment.studentPending;


	if(rcComment!=null){
		$('#assessmentCommentModel #rcCloseComment').html(rcComment.replace(':#SMALL','<small>').replace(':#BRSMALL','</small><br>') );
	}
	if(fsComment!=null){
		$('#assessmentCommentModel #fsCloseComment').html(fsComment.replace(':#SMALL','<small>').replace(':#BRSMALL','</small><br>') );
		var pendingStudentMsg= "<br><small>Assessment closed -"+studentPending+" Student not avalible for test</small>";
		var testCompletedMsg="All students have answered. Good to close.";
		var studentMessage=(studentPending>0?pendingStudentMsg:testCompletedMsg);
		$('#assessmentCommentModel #pendingStudentMsg').html(studentMessage );

	}
	if(teacherComment!=null){
		$('#assessmentCommentModel #teacherComments').html(teacherComment);

	}
	if(startComment!=null){
		$('#assessmentCommentModel #startComment').html(startComment.replace(':#SMALL','<small>').replace(':#BRSMALL','</small><br>') );
	}
	$("#assessmentCommentModel").modal().show();
};


/*//appenNew row for plan assessment
var fnAppendRowPlanAssessment =function(tName,aRow){
	var table =$("#"+tName).DataTable();
	table.row.add($(aRow)).draw( false );
	var currentRows = table.data().toArray();  // current table data
	var newRow=currentRows.pop();
	currentRows.unshift(newRow);
	table.clear();
	table.rows.add(currentRows);
	table.draw();
	//var length = currentRows.length;
	if(length>0){
		var temp=currentRows[0];
		currentRows[0]=currentRows[length-1];
		currentRows[length-1]=temp;
		table.clear();
		table.rows.add(currentRows);
		table.draw();	
	}

}*/


//update data table
var fnzUpdateDtTable =function(){
	//SHK-933
	fnUpdateDTColFilterAssessmentPlan('#tblPlanAssmnt',[1,2,6],14,[0,5,10,11,12,13],thLabels);
}
//SHK-933
var fnUpdateDTColFilterAssessmentPlan =function(tblId,hddnCols,numCols,xcdCols,hdrNameList){	
	var colList =[];
	for(var index=0;index<numCols;index++){
		colList.push(index);	
	}	
	fnShowColumns(tblId,colList);		
	fnSetDTColFilterPaginationAssessmentPlan(tblId,numCols,xcdCols,hdrNameList);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns(tblId,hddnCols);
	});
	fnSwitchToCurrentPage(tblId); //SHK-936 
}
//SHK-933
var fnSetDTColFilterPaginationAssessmentPlan =function(tId,nCols,exCols,hNames){
	var exportCols =[];
	for(var n=1;n<(nCols-1);n++){
		exportCols.push(n);
	}
	$(tId).on('order.dt',enableTooltip)
	.on( 'search.dt',enableTooltip )
	.on( 'page.dt',enableTooltip)
	.DataTable({
		"pagingType": "numbers",		
		"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		"bLengthChange": true,
		"bDestroy": true,
		"bFilter": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"fnDrawCallback": function ( oSettings ) {
			/* Need to redo the counters if filtered or sorted */
			if ( oSettings.bSorted || oSettings.bFiltered ){
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ){
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		"aaSorting": [],
		bSortCellsTop:true,
		buttons: [
		          {
		        	  extend: 'copyHtml5',
		        	  exportOptions: {
		        		  columns: exportCols
		        	  }
		          },
		          {
		        	  extend: 'excelHtml5',
		        	  exportOptions: {
		        		  columns: exportCols
		        	  }
		          },
		          {
		        	  extend: 'csvHtml5',
		        	  exportOptions: {
		        		  columns: exportCols
		        	  }
		          },
		          {
		        	  extend: 'pdfHtml5',
		        	  orientation: 'landscape',
		        	  pageSize: 'A3',
		        	  exportOptions: {
		        		  columns: exportCols,		                    
		        	  }
		          },
		          {
		        	  extend: 'print',
		        	  orientation: 'landscape',		                  
		        	  exportOptions: {
		        		  columns: exportCols,		                    
		        	  }
		          },
		          ]
	});
	fnApplyColumnFilterAssessmentPlan(tId,nCols,exCols,hNames);
}
//SHK-933
var fnApplyColumnFilterAssessmentPlan =function(tbId,noCols,xlCol,hdrNames){

	var obj=$(tbId).dataTable();
	var aoCol =new Array();
	for(var ai=0;ai<noCols;ai++){
		if (jQuery.inArray(ai, xlCol)!='-1') {
			aoCol.push(null);
		} else {
			//aoCol.push({type:"select"});
			aoCol.push({type:"select",multiple: true});
		}	
	}
	obj.columnFilter({
		sPlaceHolder : 'head:after',
		aoColumns: aoCol,
		bUnique : true,
		bSort:true,	   
	});
	for(var bj=0;bj<noCols;bj++){
		var l =hdrNames[bj];
		var k=bj;
		if (jQuery.inArray(bj, xlCol) ==-1) {
			$('#h'+(k+1)).prepend(l);
		}
	}
}
/////////////////////
$(function() {
	enableMultiSelectCampaign(pageContextElements.campaign);
	setDataTablePaginationDetailsView("#tableDetailView");
	thLabels =['#','Project  ','Source  ','Stage ','School ','Grade ','Section ','Subject ','Question Paper ','Status ','Responses ','Start Date ','End Date ','Close Question Papper','Action'];
	fnSetDTColFilterPaginationAssessmentPlan('#tblPlanAssmnt',15,[0,5,10,11,12,13,14],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tblPlanAssmnt',[1,2,6]);
		//fnUpdateDTColFilter('#tblPlanAssmnt',[1,2,6]);
	});
	fnColSpanIfDTEmpty('tableDetailView',6);
	$('#tblPlanAssmnt .dataTables_empty').prop('colspan',14);
	showDiv($('#tableDiv'));
	$(".outer-loader").hide();
	initAssessmentDatePicker('#editAssessmentForm #startDatePicker','#editAssessmentForm','startDate');
	initAssessmentDatePicker('#editAssessmentForm #endDatePicker','#editAssessmentForm','endDate');


	//if plan assessment has been invoked from campaign page
	if(pSelectedCampaignId!=0){
		$('#selectBox_campaign').find('option').each(function(i,ele){
			if(parseInt($(ele).val())==pSelectedCampaignId){
				$(ele).prop('selected','selected');
			}
		});
		$('#selectBox_campaign').multiselect('refresh');		
		getAllAssessmentPlannerData();
	}

	$('#campaignDiv').css('display','block');


	$('#editAssessmentForm')
	.formValidation({
		framework: 'bootstrap',
		icon: {
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			startDate: {
				validators: {
					notEmpty: {
						message: '  '
					},
					date: {
						format: 'DD-MMM-YYYY',
						max: 'endDate',
						message: 'The start date is not a valid'
					}
				}
			},
			endDate: {
				validators: {
					notEmpty: {
						message: ' '
					},
					date: {
						format: 'DD-MMM-YYYY',
						min: 'startDate',
						message: 'The end date is not a valid'
					}
				}
			}
		}
	})
	.on('success.field.fv', function(e, data) {
		if (data.field === 'startDate' && !data.fv.isValidField('endDate')) {
			data.fv.revalidateField('endDate');
		}

		if (data.field === 'endDate' && !data.fv.isValidField('startDate')) {
			data.fv.revalidateField('startDate');
		}
	});



	limitTextArea($('#start_Assessment #textAreaMsg'));

	//setDataTablePaginationOfAssmntPlan("#"+tableName,10);

	//setOptionsForAllMultipleSelect($(pageContextElements.campaign),$(pageContextElements.school),
	//								$(pageContextElements.grade),$(pageContextElements.subject));	
	$("#planAssessmentForm").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},		
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		//addSchool(e);

	});
	$("#close_byRCAssessmentForm")
	.formValidation(
			{    
				excluded:':disabled',
				feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
				},fields : {
					closeByRCText : {
						validators : {
							notEmpty : {
								message : ' '

							},callback : {
								message : 'Remark '+invalidInput,
								callback : function(value, validator,
										$field) {
									var regx = /^[^'?\"/\\]*$/;
									if(/\ {2,}/g.test(value))
										return false;
									return regx.test(value);
								}
							}
						}
					}
				},
			}).on('success.form.fv', function(e) {
				e.preventDefault();
				closeByRCAssessment();

			});
	$("#close_byFSAssessmentForm")
	.formValidation(
			{    
				excluded:':disabled',
				feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
				},fields : {
					closeByFSText : {
						validators : {
							notEmpty : {
								message : ' '

							},callback : {
								message : 'Remark '+invalidInput,
								callback : function(value, validator,
										$field) {
									var regx = /^[^'?\"/\\]*$/;
									if(/\ {2,}/g.test(value))
										return false;
									return regx.test(value);
								}
							}
						}
					}
				},
			}).on('success.form.fv', function(e) {
				e.preventDefault();
				closeByFSAssessment();

			});
	$("#startAssessmentForm")
	.formValidation(
			{    
				excluded:':disabled',
				feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
				},fields : {
					startAssessmentText : {
						validators : {
							notEmpty : {
								message : ' '

							},callback : {
								message : 'Remark '+invalidInput,
								callback : function(value, validator,
										$field) {
									var regx = /^[^'?\"/\\]*$/;
									if(/\ {2,}/g.test(value))
										return false;
									return regx.test(value);
								}
							}
						}
					}
				},
			}).on('success.form.fv', function(e) {
				e.preventDefault();

				startAssessment();

			});
});


