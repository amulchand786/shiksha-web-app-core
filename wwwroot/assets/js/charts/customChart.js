/*!
 * customChart.js
 * Copyright 2017 Sevya
 * @author : Debendra
 * @dependency: Chart.js
 */


/*!
 * Customization for legend call back
 * */
var lineChart ={
	

		/**
		 * @override (legendCallback of Chart.js)
		 * */
		legendCallback: function(chart) {
            console.log(chart);
            var html = [];
            html.push('<ul class="' + chart.id + '-legend">');
            
            for (var i=0; i<chart.data.datasets.length; i++) {
            	html.push('<li id='+chart.legend.legendItems[i].datasetIndex+'onclick="lineChart.updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')"><span style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
				if (chart.data.datasets[i].label) {
					html.push(chart.data.datasets[i].label);
				}
				html.push('</li>');
               /* if (chart.data.datasets[i].label) {
                    legendHtml.push('<li onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')">' + chart.data.datasets[i].label + '</li>');
                }*/
            }
            html.push('</ul>');                                                          
            return html.join("");                                                        
        }, 
		

	  /** Show/hide chart by click legend
	   * @param e : mouse event
	   * @param index : datasetIndex
	   * */
	   updateDataset : function(e, index) {
		  
		   fnUpdateStrikeLegend(e);
		   
		   
	        var index = index;
	        //myLine is the instance of chart , always keep it as window.myLine :- chart instance of Window adding window
	        var me = e.view.myLine;
	        var metaData = me.getDatasetMeta(index);
	        // See controller.isDatasetVisible comment
	        metaData.hidden = metaData.hidden === null? !me.data.datasets[index].hidden : null;
	        // We hid a dataset ... rerender the chart
	        me.update();
	    },
	    
	    
	    /** update Legend
		   * @param legendId : legendId container to bind legends
		   * */
		   updateLegend : function(legendId) {
			  // document.getElementById("legend").innerHTML = window.myLine.generateLegend();
			   document.getElementById(legendId).innerHTML = window.myLine.generateLegend();
			   /*document.getElementById("chart-area6").style.maxHeight = canvasHeight;
				document.getElementById("chart-area6").style.maxWidth = canvasWidth;*/
			   setCanvasStyle("chart-area6");
			   
			   $('#'+legendId).closest("#canvas-holder").find(".panelheadingcolor").removeClass("hide-legend");
		    },
		    getOptions:function(){
		    	var lineChartOptions = {
				        responsive: true,
				     				       
						legendCallback: function(chart) {
				            console.log(chart);
				            var html = [];
				            html.push('<ul class="' + chart.id + '-legend">');
				            
				            for (var i=0; i<chart.data.datasets.length; i++) {
				            	html.push('<li class="strikeout" onclick="lineChart.updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')"><span class="strikeout" style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
								if (chart.data.datasets[i].label) {
									html.push(chart.data.datasets[i].label);
								}
								html.push('</li>');
				               /* if (chart.data.datasets[i].label) {
				                    legendHtml.push('<li onclick="updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')">' + chart.data.datasets[i].label + '</li>');
				                }*/
				            }
				            html.push('</ul>');                                                          
				            return html.join("");                                                        
				        }, 
				        legend: {                                                                              
				            display: false,
				            
				        },
				        scales: {
				        	 xAxes: [{
				                    ticks: {
				                        fontColor: "black",				                   
				                    }
				                }],
							yAxes: [{
								ticks: {									
									fontColor: "black",
									callback: function(value, index, values) {
										return value +"%";
									},
									beginAtZero:true,
									suggestedMin: 0,
									suggestedMax: 10,
								}
							}]
						},
						tooltips: {
							mode: 'label',
							callbacks: {
								title: function(tooltipItems, data) {
									return data.labels[tooltipItems[0].index] + ' ';
								},
								label: function(tooltipItem, data) { 
									return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel+"%";
								},
							},
						},
						hover: {
							mode: 'dataset'
						},
						
				    };
		    	return lineChartOptions;
		    },
		
};




//chart-2
var studentAttendenceTrendObj ={
		/** Show/hide chart by click legend
		 * @param e : mouse event
		 * @param index : datasetIndex
		 * */
		updateDataset : function(e, index) {
			
			
			fnUpdateStrikeLegend(e);
			
			
			
			var index = index;
			var me = e.view.studentAttendenceTrendChart;
			var metaData = me.getDatasetMeta(index);
			metaData.hidden = metaData.hidden === null? !me.data.datasets[index].hidden : null;
			me.update();
		},
		/** update Legend
		 * @param legendId : legendId container to bind legends
		 * */
		updateChartLegend : function(legendId) {
			// document.getElementById("legend").innerHTML = window.myLine.generateLegend();
			document.getElementById(legendId).innerHTML = window.studentAttendenceTrendChart.generateLegend();
			/*document.getElementById("chart-area2").style.maxHeight = canvasHeight;
			document.getElementById("chart-area2").style.maxWidth = canvasWidth;*/
			setCanvasStyle("chart-area2");
			$('#'+legendId).closest("#canvas-holder").find(".panelheadingcolor").removeClass("hide-legend");
		},

};



//chart-3

var myChart3Doughnut ={
		/** update Legend
		 * @param legendId : legendId container to bind legends
		 * */
		updateChart3Legend : function(legendId) {			
			document.getElementById(legendId).innerHTML = window.chart3Doughnut.generateLegend();
		},

		/** Show/hide chart by click legend
		 * @param e : mouse event
		 * @param index : datasetIndex
		 * */
		updateChart3Dataset : function(e, index) {
			var me = e.view.chart3Doughnut;
			me.getElementsAtEvent(e);
			
		/*	var index = index;
			var me = e.view.chart3Doughnut;
			var meta = me.getDatasetMeta(index);
			// See controller.isDatasetVisible comment
			meta.hidden === 'boolean'? !meta.hidden : !me.data.datasets[index].hidden;
			
			// We hid a dataset ... rerender the chart
*/			me.update();
		},

};

//chart-4
var chart4Bar ={
		updateDataset : function(e, index) {


			fnUpdateStrikeLegend(e);


			var index = index;
			var me = e.view.chart4;
			var metaData = me.getDatasetMeta(index);
			metaData.hidden = metaData.hidden === null? !me.data.datasets[index].hidden : null;
			me.update();
		},
		/** update Legend
		 * @param legendId : legendId container to bind legends
		 * */
		updateChartLegend : function(legendId) {
			// document.getElementById("legend").innerHTML = window.myLine.generateLegend();
			document.getElementById(legendId).innerHTML = window.chart4.generateLegend();
			/*document.getElementById("chart-area4").style.maxHeight = canvasHeight;
			document.getElementById("chart-area4").style.maxWidth = canvasWidth;*/
			
			setCanvasStyle("chart-area4");
			$('#'+legendId).closest("#canvas-holder").find(".panelheadingcolor").removeClass("hide-legend");
		},

};






//chart-5
var chart5Bar ={
		updateDataset : function(e, index) {

			
			fnUpdateStrikeLegend(e);

			var index = index;
			var me = e.view.chart5;
			var metaData = me.getDatasetMeta(index);
			metaData.hidden = metaData.hidden === null? !me.data.datasets[index].hidden : null;
			me.update();
		},
		/** update Legend
		 * @param legendId : legendId container to bind legends
		 * */
		updateChartLegend : function(legendId) {
			// document.getElementById("legend").innerHTML = window.myLine.generateLegend();
			document.getElementById(legendId).innerHTML = window.chart5.generateLegend();
			/*document.getElementById("chart-area5").style.maxHeight = canvasHeight;
			document.getElementById("chart-area5").style.maxWidth = canvasWidth;*/
			setCanvasStyle("chart-area5");
			$('#'+legendId).closest("#canvas-holder").find(".panelheadingcolor").removeClass("hide-legend");
		},

};



function fnUpdateStrikeLegend(e){
	 
	
	
	   //update class of target
	   if($(e.target).hasClass('strikeme')){
		   $(e.target).removeClass('strikeme');
		   $(e.target).addClass('strikeout');
		   if(e.target.nodeName=="SPAN"){
			   $(e.target.parentElement).removeClass('strikeme');
			   $(e.target.parentElement).addClass('strikeout');
		   }else{
			   $(e.target.children).removeClass('strikeme');
			   $(e.target.children).addClass('strikeout');
		   }
	   }else{
		   $(e.target).removeClass('strikeout');
		   $(e.target).addClass('strikeme');
		   if(e.target.nodeName=="SPAN"){
			   $(e.target.parentElement).removeClass('strikeout');
			   $(e.target.parentElement).addClass('strikeme');
		   }else{
			   $(e.target.children).removeClass('strikeout');
			   $(e.target.children).addClass('strikeme');
		   }
	   }
}



////////////////////////

fnLegendPopoverInitPerformanceAcrossProject=function(){
	$('#popover-legendStudentsPerformanceAcrossProject').popover({
	    html: true,
	    title:'Legend',
	    placement: 'top',
	    content: function () {
	    	 return $('.legendStudentsPerformanceAcrossProject').html();
	    },
	    template: '<div class="popover legend-popover" ><div class="arrow arrow-legendStudentsPerformanceAcrossProject"></div><div class="popover-inner"><h3 class="popover-title title-legendStudentsPerformanceAcrossProject"></h3><div class="popover-content"><p></p></div></div></div>',
		    
	}).on('click',function(){
		 $('.title-legendStudentsPerformanceAcrossProject').append('<button type="button" onclick="fnHideLegendPopOver();" class="close">&times;</button>');
		hideOtherPopOver('popover-legendStudentsPerformanceAcrossProject');
	});
}
fnLegendPopoverInitPerformanceAcrossPhases=function(){
	$('#popover-legendStudentsPerformanceAcrossPhases').popover({
	    html: true,
	    title:'Legend',
	    placement: 'top',
	    content: function () {
	    	 return $('.legendStudentsPerformanceAcrossPhases').html();
	    },
	    template: '<div class="popover legend-popover" ><div class="arrow arrow-legendStudentsPerformanceAcrossPhases"></div><div class="popover-inner"><h3 class="popover-title title-legendStudentsPerformanceAcrossPhases"></h3><div class="popover-content"><p></p></div></div></div>',
		    
	}).on('click',function(){
		 $('.title-legendStudentsPerformanceAcrossPhases').append('<button type="button" onclick="fnHideLegendPopOver();" class="close">&times;</button>');
		hideOtherPopOver('popover-legendStudentsPerformanceAcrossProject');
	});
}

fnLegendPopoverInitStudentAttendenceTrend=function(){
	$('#popover-legendStudentAttendenceTrend').popover({
	    html: true,
	    title:'Legend',
	    placement: 'top',
	    content: function () {
	    	 return $('.legendStudentAttendenceTrend').html();
	    },
	    template: '<div class="popover legend-popover" ><div class="arrow arrow-legendStudentAttendenceTrend"></div><div class="popover-inner"><h3 class="popover-title title-legendStudentAttendenceTrend"></h3><div class="popover-content"><p></p></div></div></div>',
		    
	}).on('click',function(){
		 $('.title-legendStudentAttendenceTrend').append('<button type="button" onclick="fnHideLegendPopOver();" class="close">&times;</button>');
		hideOtherPopOver('popover-legendStudentAttendenceTrend');
	});
}
fnHideLegendPopOver=function(){
	$(".popover").popover('hide');
}




