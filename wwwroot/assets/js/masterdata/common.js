var invalidInput = 'does not contains these (", ?, \', /, \\) characters and two consecutive spaces';

var invalidInputMessage = 'contains atleast one alpha character and does not contains these (", ?, \', /, \\) characters and two consecutive spaces';
var notifications =
	[{
		"content": "dfmhjdh",
		"date": "Just now"
	},
	{
		"content": "jgfdjhs",
		"date": "2 days ago"
	},
	{
		"content": "fdvddv",
		"date": "2 days ago"
	},
	{
		"content": "dfmhjdh",
		"date": "Just now"
	},
	{
		"content": "jgfdjhs",
		"date": "2 days ago"
	},
	{
		"content": "fdvddv",
		"date": "2 days ago"
	},
	{
		"content": "dfmhjdh",
		"date": "Just now"
	},
	{
		"content": "jgfdjhs",
		"date": "2 days ago"
	},
	{
		"content": "fdvddv",
		"date": "2 days ago"
	}
	]

var spinner = {

	hideSpinner: function () {
		setTimeout(function () { $('#outer-loader').hide() }, 200);

	},
	showSpinner: function () {
		$('#outer-loader').show();
	},
	hideUploadSpinner: function () {
		setTimeout(function () { $('#upload-loader').hide() }, 200);

	},
	showUploadSpinner: function () {
		$('#upload-loader').show();
	},
};


$.wait = function (ms) {
	var defer = $.Deferred();
	setTimeout(function () { defer.resolve(); }, ms);
	return defer;
};

var customAjaxCalling = function (URL, jsonData, requestType) {

	spinner.showSpinner();
	var msg = null;
	$.ajax({
		type: requestType,
		url: URL,
		contentType: "application/json",
		async: false,
		data: JSON.stringify(jsonData),
		success: function (data) {
			maxInactiveUserInterval=sessionTimeOutValue;
			/*if ($(".icon-close").length > 0 && URL.toLowerCase().indexOf("notification") == -1) {
				$('.icon-close').trigger('click');
			}*/
			msg = data;

		},
		error: function (data) {
			msg = data;
			spinner.hideSpinner();
			networkstatus();
		}
	});
	spinner.hideSpinner();
	return msg;
};

/**
 * @author: Amul
 * check internet connection for mobile LeaderShip Dashboard
 * it is marker interface
 * */
function networkstatus() {

}
var sendGETRequest = function (URL, requestType) {
	var response = null;
	$.ajax({
		type: requestType,
		url: URL,
		async: false,
		success: function (data) {
			response = data;
		},
		error: function (data) {
			response = data;
		}
	});
	return response;
}

var downloadExcel = function (url, requestBody, requestType) {
	$.ajax({
		type: requestType,
		url: url,
		contentType: "application/json",
		async: false,
		data: JSON.stringify(requestBody),
		success: function (response) {
			window.location = "downloadTabularReport/" + response;
		},
		error: function (error) {
			console.log("Error in generating tabular report", error);
		}
	});
}

//@Debendra
//disable inputs after success 
var disableInputs = function () {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].prop('disabled', true);
	}
}

//@Debendra
//hide messageDiv on 
//@element:- elemnt on which keypress ..@toHide:div to hide
var hideAllInfoOnKeyPress = function (element, toHide) {
	var flag = false;
	for (var index = 0; index < element.length; index++) {
		if (!flag) {
			element[index].keypress(function () {
				for (var index2 = 0; index < toHide.length; index2++) {
					toHide[index2].hide();
				}
			});
			flag = true;
		}
	}
}


//@Debendra
//set dataTable 
var setDataTablePagination = function (tableId) {
	$(tableId).dataTable({
		"pagingType": "numbers",
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
		"bLengthChange": true,
		/*"scrollX": true,*/
		"bDestroy": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"fnDrawCallback": function (oSettings) {
			/* Need to redo the counters if filtered or sorted */
			if (oSettings.bSorted || oSettings.bFiltered) {
				for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
					$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
				}
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		"aaSorting": []
	});
}

var setDataTablePaginationForStudent = function (tableId) {
	$(tableId).dataTable({
		"pagingType": "numbers",
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
		"bLengthChange": true,
		"bDestroy": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"fnDrawCallback": function (oSettings) { },
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		"aaSorting": []
	});
}

var setDataTablePaginationStudentReport = function (tableId) {
	$(tableId).dataTable({
		"pagingType": "numbers",
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
		"bLengthChange": true,
		"bDestroy": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		"aaSorting": []
	});
}


var fnApplyColumnFilter = function (tbId, noCols, xlCol, hdrNames) {

	var obj = $(tbId).dataTable();
	var aoCol = [];
	for (var ai = 0; ai < noCols; ai++) {
		if (jQuery.inArray(ai, xlCol) != '-1') {
			aoCol.push(null);
		} else {

			aoCol.push({ type: "select", multiple: true });
		}
	}
	obj.columnFilter({
		sPlaceHolder: 'head:after',
		aoColumns: aoCol,
		bUnique: true,
		bSort: true,
	});
	for (var bj = 0; bj < noCols; bj++) {
		var l = hdrNames[bj];
		var k = bj;
		if (jQuery.inArray(bj, xlCol) == -1) {
			$('#h' + (k + 1)).prepend(l);
		}
	}
	$(".multiselect-container").unbind("mouseleave");
	$(".multiselect-container").on("mouseleave", function () {
		$(this).click();
	});
}

//@author:Debendra
/*
 * data table for grouping rows , column filtering
 * @params:
 * 		t:tableId (string)
 * 		nc: total no.of cols (number)
 * 		xc: excluded cols (array)
 * 		xs: X-axis scrool bar (boolean)
 * 		hc : hide col index (number)
 * 
 * */
var rowGroupingDataTable = function (t, nc, xc, hc, hNames) {

	$(t).dataTable({
		"pagingType": "numbers",
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
		"bLengthChange": false,
		/* "scrollX": xs,	*/
		"bDestroy": true,
		"bFilter": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		/* "aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort'],
				"visible": false,"targets": hc
			}],*/
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],

		"fnDrawCallback": function (oSettings) {
			/* Need to redo the counters if filtered or sorted */
			if (oSettings.bSorted || oSettings.bFiltered) {
				for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
					$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
				}
			}

			var api = this.api();
			var rows = api.rows({ page: 'current' }).nodes();
			var last = null;
			api.column(hc, { page: 'current' }).data().each(function (group, i) {

				if (last !== group) {
					var userId = (rows[i].id).split('_')[0];
					var uName = userId + "^" + (rows[i].id).split('_')[2];
					var mappingButton = "";
					var forgetButton = "";
					var userRoleId = $('#selectBox_roleType').val();
					var isSameRoleUser = "";
					if (loginUserRoleId == userRoleId) {
						isSameRoleUser = "notactive";
					}
					if (userRoleId == 9) {
						mappingButton = "<a class='action-mapping tooltip tooltip-top' style='padding-right: 0%; margin-top: -6px; position: absolute; margin-left: 60px;' onclick=mappingData()  class='tooltip tooltip-top' ><span class='tooltiptext'>" + appMessgaes.mapping + "</span><span class='glyphicon glyphicon-link font-size12'></span></a>";
					}

					var isLoggedUser = (rows[i].id).split('_')[1];
					if (superRoleCode == "SU") {
						forgetButton = "<a class='action-password tooltip tooltip-top' style='padding-right: 0%; margin-top: -4px; position: absolute; margin-left: 43px;' onclick=resetPasswordData(" + (userId) + ")  class='tooltip tooltip-top' ><span class='tooltiptext'>" + appMessgaes.resetPass + "</span><span class='fa fa-undo font-size12'></span></a>";
					}
					if (isLoggedUser == "Yes") {
						$(rows).eq(i).before(

							"<tr id='" + (userId) + "' data-id='" + (userId) + "' class='group'><td colspan='" + (nc - 2) + "'>" + group + "</td><td>" + mappingButton + "<a class='action-delete tooltip tooltip-top notactive " + isSameRoleUser + "' style='padding-right: 0%; margin-top: -6px; position: absolute; margin-left: 23px;'   class='tooltip tooltip-top' ><span class='tooltiptext'>" + appMessgaes.delet + "</span><span class='glyphicon glyphicon-trash font-size12'></span></a><a class='action-edit tooltip tooltip-top notactive " + isSameRoleUser + "' style='margin-top: -6px;position: absolute;margin-left: 2px;' onclick=editData(" + (userId) + ",event) ><span class='tooltiptext'>" + appMessgaes.edit + "</span><span class='glyphicon glyphicon-edit font-size12'></span></a></td></tr>");
						last = group;
					} else {
						$(rows).eq(i).before(
							"<tr id='" + (userId) + "' data-id='" + (userId) + "' class='group'><td colspan='" + (nc - 2) + "'>" + group + "</td><td>" + mappingButton + forgetButton + "<a class='action-delete tooltip tooltip-top " + isSameRoleUser + "' style='padding-right: 0%; margin-top: -6px; position: absolute; margin-left: 23px;' onclick=deleteData('" + uName + "')  class='tooltip tooltip-top' ><span class='tooltiptext'>" + appMessgaes.delet + "</span><span class='glyphicon glyphicon-trash font-size12'></span></a><a class='action-edit tooltip tooltip-top " + isSameRoleUser + "' style='margin-top: -6px;position: absolute;margin-left: 2px;' onclick=editData(" + (userId) + ",event) ><span class='tooltiptext'>" + appMessgaes.edit + "</span><span class='glyphicon glyphicon-edit font-size12'></span></a></td></tr>");
						last = group;
					}
				}
			});

			reArrangeColspan();

		},
		"order": [1, 'asc'],
	});
	fnApplyColumnFilter(t, nc, xc, hNames);


}

//@Tharun
function reArrangeColspan() {
	var countOfCols = $("#userTable thead th").length;

	$("#userTable tr.group td:nth-child(1)").attr("colspan", countOfCols - 1);
}

/*@Debendra
 *set columnfilter for data table columns
 */
var setColumnFilterForDataTable = function (tId, nCols, xlCol) {
	var obj = $(tId).dataTable();
	var aoCol = [];
	for (var i = 0; i < nCols; i++) {
		if (jQuery.inArray(i, xlCol) != '-1') {
			aoCol.push(null);
		} else {

			aoCol.push({ type: "select", multiple: true });
		}
	}
	obj.columnFilter({
		sPlaceHolder: 'head:before',
		//sPlaceHolder: "foot:after",
		aoColumns: aoCol,
		bUnique: true,
		bSort: true,
	});

}

//@Debendra
//set dataTable 
var setDataTableColFilterPagination = function (tableId, noOfCols, excludeCol, isXScroll) {
	$(tableId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			//"scrollX": isXScroll,	
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function (oSettings) {
				/* Need to redo the counters if filtered or sorted */
				if (oSettings.bSorted || oSettings.bFiltered) {
					for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
						$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
					}
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],
			bSortCellsTop: true,

			/*  initComplete: function () {
					this.api().columns(excludeCol).every( function () {
						var column = this;
						var select = $('<select class="deb"><option value=""></option></select>')
							.appendTo( $(column.header()).empty() )
							.on( 'click', function (e) {
								var val = $.fn.dataTable.util.escapeRegex($(this).val());
	
								column.search( val ? '^'+val+'$' : '', true, false ).draw();
								e.stopPropagation();
							} );
	
						column.data().unique().sort().each( function ( d, j ) {
							select.append( '<option>'+d+'</option>' )
						} );
					} );
				}*/
		});
	setColumnFilterForDataTable(tableId, noOfCols, excludeCol);
}


var fnApplyStyleToogleCol = function (aoTable, inCol) {
	//issue : SHK-603
	var bcVis = aoTable.fnSettings().aoColumns[inCol].bVisible;
	if (bcVis) {

		$("#divToogle a#t" + inCol).removeClass("label-default");
		$("#divToogle a#t" + inCol).addClass("label-success");


		var i;
		for (i = 1; i <= $("#divToogle").find('a').length - 1; i++) {
			var flag = aoTable.fnSettings().aoColumns[i].bVisible;
			if (!flag) {

				return;
			}
		}
		$("#divToogle a#t0").removeClass("label-default");
		$("#divToogle a#t0").addClass("label-success");
	} else {
		$("#divToogle a#t" + inCol).removeClass("label-success");
		$("#divToogle a#t" + inCol).addClass("label-default");
		$("#divToogle a#t0").removeClass("label-success");
		$("#divToogle a#t0").addClass("label-default");
	}
	$("#divToogle a#t" + inCol).blur();
	$("#divToogle a#t0").blur();
}

//@author: Debendra
//for showing data table columns
var fnShowColumns = function (tableId, colList) {
	var tableObj = $(tableId).dataTable();
	$.each(colList, function (i, val) {
		tableObj.fnSetColumnVis(val, true);
		fnApplyStyleToogleCol($(tableId).dataTable(), i);
	});
}

//@Debendra
//customize the multiple select box and call back for hiding columns in data table
//for tooggle column functionality
var fnMultipleSelAndToogleDTCol = function (arg, callback) {
	$(arg).multiselect({
		maxHeight: 180,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',
		nonSelectedText: '',
		buttonTitle: function (options, select) {
			if (options.length > 0) {
				$(select).parents("th").find("button").addClass("filter-enabled").empty().html("<b class='glyphicon glyphicon-filter'></b>");
			} else {
				$(select).parents("th").find("button").removeClass("filter-enabled").empty().html("<b class='caret'></b>");
			}
		},
		onDropdownHide: function (event) {

		}
	});
	callback();
}

/*
 * @Debendra
 * hide columns in data table
 * */
var fnHideColumns = function (tableId, hideColList) {
	var tableObj = $(tableId).dataTable();
	$.each(hideColList, function (i, val) {
		tableObj.fnSetColumnVis(val, false);
		fnApplyStyleToogleCol($(tableId).dataTable(), val);
	});
}


/*@Debendra
 *update columnfilter for data table columns after CRUD operations
 ** Parameters:"
 * @tableId                 String		id of table      
 * @isHiddenCols            Boolean      True/False
 * @hiddenColList           Array      	Default hidden column lists
 * @noOfCols                Integer      Total number of columns
 * @excludeCols             Array       	Excluded column filter lists
 */
var fnUpdateColumnFilter = function (tableId, isHiddenCols, hiddenColList, noOfCols, excludeCols) {

	var colList = [];
	for (var index = 0; index < noOfCols; index++) {
		colList.push(index);
	}

	fnShowColumns(tableId, colList);
	setColumnFilterForDataTable(tableId, noOfCols, excludeCols);
	fnMultipleSelAndToogleDTCol('.search_init', function () {
		fnHideColumns(tableId, hiddenColList);
	});
}




//for hiding data table columns
var fnShowHideColumns = function (tableId, hideColList) {
	var tableObj = $(tableId).dataTable();
	$.each(hideColList, function (i, val) {
		tableObj.fnSetColumnVis(val, false);
	});
}




//@author: Debendra
//show/ hide columns in data table
//Toogle Column
/**
 * @params
 * iCol:	Integer	:Index of col
 * @tableId: String : Table id
 * */
function fnShowHide(iCol, tableId) {

	var ooTable = $('#' + tableId).DataTable(); //Keep on current page after changing toogles
	var tPgNum = ooTable.page();


	/* Get the DataTables object again - this is not a recreation, just a get of the object */
	var oTable = $('#' + tableId).dataTable();
	var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
	oTable.fnSetColumnVis(iCol, bVis ? false : true);



	fnApplyStyleToogleCol(oTable, iCol);
	oTable.fnPageChange(tPgNum);
}

function fnShowAll(cols, tableId) {

	var obTable = $('#' + tableId).DataTable(); //SHK-759
	var taPgNum = obTable.page();

	var oTable = $('#' + tableId).dataTable();
	$.each(cols, function (i, iCol) {
		var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
		oTable.fnSetColumnVis(iCol, bVis = true);
		$("#divToogle a#t" + iCol).removeClass("label-default");
		$("#divToogle a#t" + iCol).addClass("label-success");
		$("#divToogle a#t0").removeClass("label-primary");
		$("#divToogle a#t0").addClass("label-success");
	});
	oTable.fnPageChange(taPgNum);
}


//show div
var showDiv = function () {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].show();
	}
}

//hide div
var hideDiv = function () {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].hide();
	}
}

/* 
 *	@Tharun
 *	this function used to enable tooltip 
 */

function enableTooltip() {
	$('[data-toggle="tooltip"]').tooltip();
}

//show success message and reset one field
function resetOneField(modalId, fieldId) {
	$("#" + modalId).find('.alert-success').parent().show();
	$("#" + modalId).find(".btn").removeAttr("disabled").removeClass("disabled");
	$('#' + fieldId).val("").parent("div").removeClass("has-success").find("i").removeClass("glyphicon-ok");
	$("#" + modalId).find('.alert-success').parent().fadeOut(1500);
}

//@author:Debendra
//map html entities to respective ele
var escapeHtmlCharacters = function (text) {
	var map = {
		'&': '&amp;',
		'<': '&lt;',
		'>': '&gt;',
		"'": '&#039;',
		' ': '&nbsp;',
		'©': '&#169;',
		'®': '&#174;',
		';': '&#59;',
		':': '&#58;',
		'=': '&#61;',
		'@': '&#64;',
		'`': '&#96;',
		'_': '&#95;',
		'[': '&#91;',
		']': '&#93;'
	};
	if (typeof text == 'string') {
		text = text.replace(/[&<>"' ]/g, function (m) {
			return map[m];
		});
	}
	return text;
}

//decode html entities to normal char
var decodeHtmlCharacters = function (text) {
	var map = {
		'&amp;': '&',
		'&lt;': '<',
		'&gt;': '>',
		'&#039;': "'",
		'&nbsp;': ' ',
		'&#169;': '©',
		'&#174;': '®',
		'&#59;': ';',
		'&#58;': ':',
		'&#61;': '=',
		'&#64;': '@',
		'&#96;': '`',
		'&#95;': '_',
		'&#91;': '[',
		'&#93;': ']'
	};
	if (typeof text == 'string') {
		text = text.replace(/[&<>"' ]/g, function (m) {
			return map[m];
		});
	}
	return text;
}


//open multi select on focus of mouse
//@Debendra
var fnCollapseMultiselect = function () {

	$(".multiselect-container").unbind("mouseleave");

	$(".multiselect-container").on("mouseleave", function () {
		//$(this).click();
	});
}

//@Debendra
//customize the multiple select box
var setOptionsForMultipleSelect = function (arg) {
	$(arg).multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',
	});
	fnCollapseMultiselect();
}

//@Tharun
//customize the multiple select box for use Screen
var setOptionsForMultipleSelectForUserRole = function (arg) {
	$(arg).multiselect({
		maxHeight: 330,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',
	});
}

//following method used in user module & some other also 
var setOptionsForMultipleSelectWithoutSearch = function (arg) {
	$(arg).multiselect({
		maxHeight: 200,
		//includeSelectAllOption: true,

		//enableFiltering: true,
		//enableCaseInsensitiveFiltering: true,
		//includeFilterClearBtn: true,

		//filterPlaceholder: 'Search here...',
		disableIfEmpty: true,
	});
}

var setOptionsForMultipleSelectWithoutSearchUser = function (arg, formName, height) {
	$(arg).multiselect({
		maxHeight: 200,
		disableIfEmpty: true,
		onDropdownShow: function () {
			$(formName).scrollTop(height);
		}
	});
}


var setOptionsForDropUpMultipleSelect = function () {
	$(arguments).each(function (index, arg) {
		$(arg).multiselect({
			maxHeight: 100,
			includeSelectAllOption: true,
			enableFiltering: true,
			dropUp: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
		});
	});
	fnCollapseMultiselect();
}


//@Debendra
//customize all multiple select box
var setOptionsForAllMultipleSelect = function () {
	$(arguments).each(function (index, arg) {
		$(arg).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
		});
	});
	fnCollapseMultiselect();
}

var destroyAndRefreshMultiselect = function () {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].multiselect('destroy');
		arguments[i].multiselect('refresh');
	}
}

var refreshMultiselect = function () {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].multiselect('refresh');
	}
}

//@author: Debendra
//initialize and set fuel ux date picker
var initAndSetOptionsOfDatePicker = function () {
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].datepicker({
			allowPastDates: false,
			momentConfig: {
				culture: 'en', // change to specific culture
				format: 'DD-MMM-YYYY' // change for specific format
			}
		});
	}
}


//@author:Debnedra
//reset form validation for location smart filters
var resetFormValidatonForLoc = function (formId, lastChildLocName) {
	$("#" + formId).data('formValidation').updateStatus(lastChildLocName, 'NOT_VALIDATED');
}


//make elements validation success...
var fnUpdateFVElementStatus = function (pFormId, eleArr, pStatus) {
	$.each(eleArr, function (i, ele) {
		$("#" + pFormId).data('formValidation').updateStatus(ele, pStatus);
	});
}



//@author:Tharun
//reset form validation for location smart filters
var resetFormValidatonForLocFilters = function (formId, elementsName) {
	$.each(elementsName, function (index, value) {
		$("#" + value).val('');
		$("#" + formId).data('formValidation').updateStatus(value, 'IGNORED');
	});

}

function resetFormById(formId) {
	$("#" + formId).find('#errorMessage').hide();
	$("#" + formId).find('#alertdivFailure').hide();
	$("#" + formId).data('formValidation').resetForm(true);
}

//@author:Debendra
//reset form validations for all input type except locations
var resetFvForAllInputExceptLoc = function (formId, elementsName) {
	$.each(elementsName, function (i, value) {
		$("#" + value).val('');
		$("#" + formId).data('formValidation').updateStatus(value, 'NOT_VALIDATED');
	});
}

//@author:Debendra
//append new row to respective data table
var appendNewRow = function (tableName, aRow) {
	var oaTable = $("#" + tableName).DataTable();

	tbPgNum = oaTable.page();


	oaTable.row.add($(aRow)).draw(false);
	var currentRows = oaTable.data().toArray();  // current table data
	//Issue-SHK-964
	var newRow = currentRows.pop();
	var info = $('#' + tableName).DataTable().page.info();
	var startRow = info.start;
	currentRows.splice(startRow, 0, newRow);

	oaTable.clear();
	oaTable.rows.add(currentRows);

	oaTable.draw(false);



}
var tbPgNum = 0;
//switch to cuurent page in data table after append row in add/edit 
var fnSwitchToCurrentPage = function (pTableName) {
	var obTable = $(pTableName).dataTable();
	obTable.fnPageChange(tbPgNum);
}

//@author:Debendra
//delete row from data table
var deleteRow = function (tableName, deleteRowId) {
	var oDtable = $("#" + tableName).DataTable();

	tbPgNum = oDtable.page();

	oDtable.row('#' + deleteRowId).remove().draw(false);
}

//@author:Debendra
//delete row from data table
var deleteAllRow = function (tableName) {

	var table = $("#" + tableName).DataTable();
	table.search('').draw();
	$('input[type=search]').val('');
	table.rows().remove().draw(false);
}



//@author:Debendra
//show successmessage
var displaySuccessMsg = function () {
	$('#successMsg').text("");
	$('#successMsg').text("Information added.");
	$("#success-msg").fadeOut(3000);
}
/*
$(document).keydown(function(event){
    if(event.keyCode==123){
        return false;
    }
    else if (event.ctrlKey && event.shiftKey && event.keyCode==73){        
             return false;
    }
});

$(document).on("contextmenu",function(e){        
   e.preventDefault();
});*/


//limit text area input characters
var limitTextArea = function (msgId) {
	$(msgId).text('');
	var maxchars = 300;
	$(msgId).text(maxchars + " characters remaining.");
	$('textarea').keyup(function () {

		$(this).val($(this).val().substring(0, maxchars));
		var tlength = $(this).val().length;
		remain = maxchars - parseInt(tlength);
		$(msgId).text(remain + " characters remaining.");
	});
}



//preview survey in sg
var previewSurveyInSg = function (sgAssessmentCode, previewUrl) {
	var previewUrlForIframe = previewUrl.replace('SURVEY_CODE', sgAssessmentCode);
	$("#surveypreviewiframe").attr("src", previewUrlForIframe + "?deviceMode=touch");
	$("#surveyPreviewPopup").modal("show");
}

//custom redirect with post method
var customRedirect = function (redirectUrl, arg, value) {
	var form = $('<form action="' + redirectUrl + '" method="post">' +
		'<input type="hidden" name="' + arg + '" value="' + value + '"></input>' + '</form>');
	$('body').append(form);
	$(form).submit();
};


var customRedirectGet = function (redirectUrl, arg, value) {
	var form = $('<form action="' + redirectUrl + '" method="get">' +
		'<input type="hidden" name="' + arg + '" value="' + value + '"></input>' + '</form>');
	$('body').append(form);
	$(form).submit();
};







/**
 * @author : Debendra
 * @version : v1.0
 * After style changed of DT column filter and header row label integration 
 * 
 */
/**
 * sets data table options along with column filters and header names
 * @params
 * @tId 	:	String	: Table id
 * @nCols	:	Array	: Number of columns
 * @exCols :	Array	: Excluded columns
 * @hNames : 	Array	: Header column names	
 * */
var fnSetDTColFilterPagination = function (tId, nCols, exCols, hNames) {
	var exportCols = [];
	for (var n = 1; n < (nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function (oSettings) {
				/* Need to redo the counters if filtered or sorted */
				if (oSettings.bSorted || oSettings.bFiltered) {
					for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
						$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
					}
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],

			bSortCellsTop: true,
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'A3',
					exportOptions: {
						columns: exportCols,
					}
				},
				{
					extend: 'print',
					orientation: 'landscape',
					exportOptions: {
						columns: exportCols,
					}
				},
			]
		});
	fnApplyColumnFilter(tId, nCols, exCols, hNames);


	$(".multiselect-container").unbind("mouseleave");
	$(".multiselect-container").on("mouseleave", function () {
		$(this).click();
	});
}





var fnSetDTColFilterPaginationWithoutExportButton = function (tId, nCols, exCols, hNames) {
	var exportCols = [];
	for (var n = 1; n < (nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function (oSettings) {
				/* Need to redo the counters if filtered or sorted */
				if (oSettings.bSorted || oSettings.bFiltered) {
					for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
						$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
					}
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],
			bSortCellsTop: true,

		});
	fnApplyColumnFilter(tId, nCols, exCols, hNames);
}
var fnSetDTColFilterPaginationWithoutExportButtonFooterCalBack = function (tId, nCols, exCols, hNames, thSum) {
	var exportCols = [];
	for (var n = 1; n < (nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"><"pull-right"><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			/*"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],*/
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function (oSettings) {
				/* Need to redo the counters if filtered or sorted */

			},
			"footerCallback": function (tfoot, data, start, end, display) {
				var api = this.api();
				var intVal = function (i) {
					return typeof i === 'string' ?
						i.replace(/[\$,%,]/g, '') * 1 :
						typeof i === 'number' ?
							i : 0;
				};

				// Total over all pages
				$.each(thSum, function (index, aSum) {
					total = api.column(aSum).data().reduce(function (a, b) {
						return intVal(a) + intVal(b);
					}, 0);

					// Total over this page
					pageTotal = api.column(aSum, { page: 'current' }).data().reduce(function (a, b) {
						return intVal(a) + intVal(b);
					}, 0);

					// Update footer
					$(api.column(aSum).footer()).html(total);
				});

			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],
			bSortCellsTop: true,

		});
	fnApplyColumnFilter(tId, nCols, exCols, hNames);
}

var fnSetDTColFilterPaginationWithoutExportButtonFooterCalBackAndSerial = function (tId, nCols, exCols, hNames, thSum) {
	var exportCols = [];
	for (var n = 1; n < (nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function () {
				/* Need to redo the counters if filtered or sorted */

			},
			"footerCallback": function (tfoot, data, start, end, display) {
				var api = this.api();
				var intVal = function (i) {
					return typeof i === 'string' ?
						i.replace(/[\$,%,]/g, '') * 1 :
						typeof i === 'number' ?
							i : 0;
				};

				// Total over all pages
				$.each(thSum, function (index, aSum) {
					total = api.column(aSum).data().reduce(function (a, b) {
						return intVal(a) + intVal(b);
					}, 0);

					// Total over this page
					pageTotal = api.column(aSum, { page: 'current' }).data().reduce(function (a, b) {
						return intVal(a) + intVal(b);
					}, 0);

					// Update footer
					$(api.column(aSum).footer()).html(total);
				});

			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],
			bSortCellsTop: true,

		});
	fnApplyColumnFilter(tId, nCols, exCols, hNames);
}



/*@Debendra
 *update columnfilter for data table columns after CRUD operations
 ** Parameters:"
 * @tblId            	   String		id of table      
 * @hddnCols		           Array      	Default hidden column lists
 * @numCols                Integer      Total number of columns
 * @xcdCols                Array       	Excluded column filter lists
 * @hdrNameList			  Array			Header column list
 */
var fnUpdateDTColFilter = function (tblId, hddnCols, numCols, xcdCols, hdrNameList) {
	var colList = [];
	for (var index = 0; index < numCols; index++) {
		colList.push(index);
	}
	fnShowColumns(tblId, colList);
	fnSetDTColFilterPagination(tblId, numCols, xcdCols, hdrNameList);
	fnMultipleSelAndToogleDTCol('.search_init', function () {
		fnHideColumns(tblId, hddnCols);
	});


	fnSwitchToCurrentPage(tblId); //SHK-936 

	$(".multiselect-container").unbind("mouseleave");
	$(".multiselect-container").on("mouseleave", function () {
		$(this).click();
	});

}
//unbind sort event on data table header click
var fnUnbindDTSortListener = function () {
	$('th').unbind("click.DT");
}
//end:   After style changed of DT column filter and header row label integration 



//@Mukteshwar
//After Add/Edit/Delete set color for "SHOW ALL" column Tag
function setShowAllTagColor(colorClass) {
	$('#t0').removeClass();
	$('#t0').addClass(colorClass);

}

/////////////////
/*
 * Display newly added element as list items after saveAndAddNew functionality
 * @Params:
 * @mdlId	:	String	: Modal Id
 * */

var fnDisplaySaveAndAddNewElement = function (mdlId, eleName) {
	if (!isInfoPanelClosed) {
		$("#cList").prepend("<li>" + "- " + eleName + "</li>");
		$(mdlId).find('#divSaveAndAddNewMessage').show();
	}
}
var fnInitSaveAndAddNewList = function () {
	isInfoPanelClosed = false;
	$('#cList').empty();
	$('#divSaveAndAddNewMessage').hide();
}
var fnClose = function (modalId) {
	isInfoPanelClosed = true;
	$(modalId).find('#divSaveAndAddNewMessage').hide();
}

//colspan for empty data tables
var fnColSpanIfDTEmpty = function (tableId, noOfCols) {
	var tId = "#" + tableId + " .dataTables_empty";
	$(tId).prop('colspan', noOfCols);
}

var fnDisplaySaveAndAddNewElementAui = function (mdlId, eleName) {
	$(mdlId).find("#cList").prepend("<li>" + "- " + eleName + "</li>");
	$(mdlId).find('#divSaveAndAddNewMessage').show();

}


var fnCookieManagement = function () {
	var timer = setInterval(function () {

		var timeoutCookie = getCookie("TimeoutCookie");



		if (timeoutCookie == "") {


			$('#timeout-message').modal('show');

			window.clearInterval(timer);
		}

	}, 5000);
}

function getCookie(cname) {
	var name = cname + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ')
			c = c.substring(1);
		if (c.indexOf(name) == 0)
			return c.substring(name.length, c.length);
	}
	return "";
}
var fnLogin = function () {
	window.location = '/';
}

//check user is online /offline
var isOnline = function (URL, type) {
	$.ajax({
		type: type,
		url: URL,
		async: true,
		success: function (data) {
			if (data == "ONLINE") {
				$('#online').css('display', 'none')				
			}
		},
		error: function () {
			$('#online').css('display', 'block');			
		}
	});
};

//reload the page to login if session timed out
var fnDoReload = function (e) {
	window.location = '/';
}


//do session invalidate once session timed out
//especially for F5 keys........
var fnDoInvalidateSession = function (URL, type) {
	$.ajax({
		type: type,
		url: URL,
		async: false,
		success: function (data) {
		},
		error: function (data) {
		}
	});
}


//sort locations by name
var fnSortSmartFilterByName = function (aData) {
	var aSortData = aData.slice(0);
	aSortData.sort(function (a, b) {
		if (a != null && b != null) {
			var x = a.locationName.toLowerCase();
			var y = b.locationName.toLowerCase();
			return x < y ? -1 : x > y ? 1 : 0;
		}
	});
	return aSortData;
}




var fnUnbindCollapseMultiselect = function () {
	$(".multiselect").unbind("mouseenter");
}



var fnLogOutSGOnRedirection = function () {
	var frmTarget = sgEndPointURL + 'logout';
	spinner.showSpinner();
	if (isLoginPage == '' || isLoginPage == 'false') {
		if (isQPPage != '' && isQPPage == 'true' && isShikshaSGClient != 'true') {
			return false;
		} else if (isQBPage != '' && isQBPage == 'true' && isShikshaSGClient != 'true') {
			return false;
		} else {
			if (isShikshaSGClient == 'true') {
				$("#sgForm").prop("action", frmTarget);
				$("#sgForm").submit();
			}
		}

	}
	spinner.hideSpinner();
}
var fnDoSGLogOut = function (pFrmTarget) {

	if ((isQPPage == 'true' || isQBPage == 'true') && isShikshaSGClient == 'true') {


		$("#sgForm").prop("action", pFrmTarget);
		$("#sgForm").submit();
	}
}

var fnSetReportDTColFilterPagination = function (tId, nCols, exCols, hNames) {
	var exportCols = [];
	for (var n = 1; n < (nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function () {
				/* Need to redo the counters if filtered or sorted */

			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],
			bSortCellsTop: true,
			buttons: []

		});
	fnApplyColumnFilter(tId, nCols, exCols, hNames);
}








var shk = {
	/**
	 * Empty the items in select box
	 * */
	fnEmptyAllSelectBox: function () {
		$.each(arguments, function (i, obj) {
			$(obj).empty();
		});
	},
};

/////////////////////////// delete notification ///////////////////
var fnDeleteNotification = function (element, notificationId) {
	$(element).parent().remove();
	spinner.hideSpinner();
	var notificationData = customAjaxCalling("notification/" + notificationId, null, "DELETE");
	if (notificationData != "-1") {
		if (notificationData == "0") {
			location.reload();
		}
		$(element).parent().remove();
	}
	spinner.hideSpinner();
}
/////////////////////////// delete all notification ///////////////////
var fnDissmissAllNotification = function () {
	spinner.showSpinner();
	var notificationData = customAjaxCalling("notification", null, "DELETE");
	if (notificationData == "shiksha-200") {
		location.reload();
	} else {
		AJS.flag({
			type 	: "error",
			title 	: "Error!",
			body 	: "OOPs!!Server error.",
			close 	: 'auto'
		});
	}
	spinner.hideSpinner();
}
var checkedNotification = '';
function checkNotificationData() {
	$.ajax({
		url: 'notification/count',
		success: function (data) {
			var number = data;
			if(data)
				$(".notifications-count").show();
			else
				$(".notifications-count").hide();
			if(data > 9)
				number = "9+";
			$('.notifications-count').text(number);
		}
	});

}


function updateNotificationData(callback) {
	checkedNotification = '';
	spinner.showSpinner();
	$.ajax({
		url: 'notification',
		type: 'PUT', 
		success: function (data) {
			if (data.response == "shiksha-200") {
				$('.notifications-menu-items-list').html("");
				if(data.notifications.length>0){
				$.each(data.notifications, function (index, value) {
					var notify = '<li class="menu-item" data-notificationid="'+value.id+'"><span id="delNotification" class="html-click-hide">×</span><a href="javascript:void(0);" class="notification-item"><div class="lms-notification-detail-section"><div class="lms-notification-detail">' + value.message + '</div><div class="lms-notification-date">' + value.createdDate + '</div></div></a></li>'
					$('.notifications-menu-items-list').append(notify);
				});
				$('.no-notifications').hide();
				$('#clearAllNoti').show();
				}else{
					$('.no-notifications').show();
					$('#clearAllNoti').hide();
				}
				spinner.hideSpinner();
				$('.lms-notification-options').toggleClass('active');
			} else {
				spinner.hideSpinner();
				AJS.flag({
					type  : "error",
					title : "Error..!",
					body  : data.responseMessage,
					close : 'auto'
				});
			}
		},
		error: function () {
			spinner.hideSpinner();
			AJS.flag({
				type 	: "error",
				title 	: "Error!",
				body 	: "OOPs!!Server error.",
				close 	: 'auto'
			});
		}
	});
}
function fnUpdateIsShown() {
	$.ajax({
		url: 'updateIsShownForAllNotification',
		success: function () {
			$(".notifications-count").hide();

		},
		error: function () {
			$(".notifications-count").show();
		}
	});
}
function fnShowHideNotificationBadge() {
	if (totalNotification == 0) {
		$('#notificationBadge').css('display', 'none');
	} else {
		$('#notificationBadge').css('display', 'inline-block');
	}
}
var genratePopoverContent = function () {

	if (checkedNotification != "") {
		if (!checkedNotification.isNotified) {
			$("#btnDeleteAll").css("display", "none");
			$('.popover-content').empty();
			var elem = '<div><i class="fa fa-info-circle" aria-hidden="true"> You have no notifications. </i><div>';
			$('.popover-content').append(elem);

		} else if (checkedNotification.isNotified) {
			$("#btnDeleteAll").css("display", "block");
			$('.popover-content').empty();
			totalNotification = checkedNotification.totalNotificationCount;
			$.each(checkedNotification.notifications, function (index, popoverData) {
				var element = '<div class="aui-message aui-message-info closeable poover-notification"><p>' + popoverData.message + '</p><span class="aui-icon icon-close" onclick="fnDeleteNotification(this,' + popoverData.id + ');" role="button" tabindex="0"></span><span class="" style=""><small>' + popoverData.createdDate + '</small></span></div>';
				$('.popover-content').append(element);
			});
		}
	}
}



var reportCustomAjaxCalling = function (URL, jsonData, requestType) {
	var msg = null;
	$.ajax({
		type: requestType,
		url: URL,
		contentType: "application/json",
		async: false,
		data: JSON.stringify(jsonData),
		success: function (data) {
			msg = data;
			//return data;


		},
		error: function (data) {

			msg = data;

			$(".outer-loader").css('display', 'none');
		}
	});
	$(".outer-loader").css('display', 'none');
	return msg;
};

/**
* @author : Tharun
* @version : v1.0
* After style changed of DT column filter and header row label integration 
* 
*/
/**
* sets data table options along with column filters and header names
* @params
* @tId 	:	String	: Table id
* @nCols	:	Array	: Number of columns
* @exCols :	Array	: Excluded columns
* @hNames : 	Array	: Header column names	
* */
var fnSetDTColFilterPaginationForMultiDelete = function (tId, nCols, exCols, hNames) {
	var exportCols = [];
	for (var n = 1; n < (nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,

			"fnDrawCallback": function (oSettings) {
				var isCheckedDel = $('#chkboxSelectAllDelSchoolForSchool').is(':checked');
				fnDoselectAllForSchoolDeleteForMultiSelect(isCheckedDel);
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],

			bSortCellsTop: true,
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'A3',
					exportOptions: {
						columns: exportCols,
					}
				},
				{
					extend: 'print',
					orientation: 'landscape',
					exportOptions: {
						columns: exportCols,
					}
				},
			]
		});
	if (window.navigator.userAgent.indexOf("rv:11.0") > 0) {
		if (tId == "#schoolTable") {
			$(".row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-left" id="btnDeleteAllCampaignSchoolForSchool" onclick="fnDeleteAllSchoolsForSchool();"style="display: none;margin-left: -12px;margin-top:25px;margin-left:-160px;">Delete selected schools</button>');
		} else {
			$(".row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-left" id="btnDeleteAllCampaignSchoolForSchool" onclick="fnDeleteAllSchoolsForSchool();"style="display: none;margin-left: -12px;margin-top:25px;margin-left:-160px;">Delete selected students</button>');

		}
	} else {
		if (tId == "#schoolTable") {
			$(".row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-left" id="btnDeleteAllCampaignSchoolForSchool" onclick="fnDeleteAllSchoolsForSchool();"style="display: none;margin-left: -12px;">Delete selected schools</button>');
		} else {
			$(".row.view-filter .pull-left").append('<button type="button" class="btn btn-link pull-left" id="btnDeleteAllCampaignSchoolForSchool" onclick="fnDeleteAllSchoolsForSchool();"style="display: none;margin-left: -12px;">Delete selected students</button>');

		}
	}
	fnApplyColumnFilter(tId, nCols, exCols, hNames);
}

var fnUpdateDTColFilterForMultiDelete = function (tblId, hddnCols, numCols, xcdCols, hdrNameList) {
	var colList = [];
	for (var index = 0; index < numCols; index++) {
		colList.push(index);
	}
	fnShowColumns(tblId, colList);
	fnSetDTColFilterPaginationForMultiDelete(tblId, numCols, xcdCols, hdrNameList);
	fnMultipleSelAndToogleDTCol('.search_init', function () {
		fnHideColumns(tblId, hddnCols);
	});


	fnSwitchToCurrentPage(tblId); //SHK-936 
}






var fnDTForStudetTable = function (tId, nCols, exCols, hNames) {

	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function (oSettings) {
				/* Need to redo the counters if filtered or sorted */
				if (oSettings.bSorted || oSettings.bFiltered) {
					for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
						$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
					}
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			},
			{
				"searchable": false, "targets": 0
			}],
			"aaSorting": [],

		});
	$(".pull-left .dataTables_length").append('<button type="button" class="btn btn-link" onclick="updateAllEditedRollNumber();" id="updateRollNumberBtn">Update Roll Numbers</button>');
	fnApplyColumnFilter(tId, nCols, exCols, hNames);


	//collapse select filter
	$(".multiselect-container").unbind("mouseleave");
	$(".multiselect-container").on("mouseleave", function () {
		$(this).click();
	});
}

/**
 * Update Data table
 * */
var fnUpdateStudentDt = function (tblId, hddnCols, numCols, xcdCols, hdrNameList) {
	var colList = [];
	for (var index = 0; index < numCols; index++) {
		colList.push(index);
	}

	fnDTForStudetTable(tblId, numCols, xcdCols, hdrNameList);
	fnMultipleSelAndToogleDTCol('.search_init', function () {
		fnHideColumns(tblId, hddnCols);
	});
	fnSwitchToCurrentPage(tblId);

	$(".multiselect-container").unbind("mouseleave");
	$(".multiselect-container").on("mouseleave", function () {
		$(this).click();
	});
}
var setOptionsForMultipleSelectWithScrol = function (arg, formName, height) {
	$(arg).multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',
		onDropdownShow: function () {
			$(formName).scrollTop(height);
		}
	});
}

var fnSetAssessmentDashBoardDTColFilterPagination = function (tId, nCols, exCols, hNames) {
	var exportCols = [];
	for (var n = 1; n < (nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function (oSettings) {
				/* Need to redo the counters if filtered or sorted */
				if (oSettings.bSorted || oSettings.bFiltered) {
					for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
						$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
					}
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],
			bSortCellsTop: true,
			buttons: []

		});
	fnApplyColumnFilter(tId, nCols, exCols, hNames);
}
//data table with initial sort
//@Debendra
var fnDTWithInitialSort = function (tableId, sortColIndex, sortType) {
	$(tableId).dataTable({
		"pagingType": "numbers",
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
		"bLengthChange": true,
		"bDestroy": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"fnDrawCallback": function (oSettings) {
			/* Need to redo the counters if filtered or sorted */
			if (oSettings.bSorted || oSettings.bFiltered) {
				for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
					$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
				}
			}
		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		"aaSorting": [[sortColIndex, sortType]]
	});
}
var enableMultiSelectCampaign = function (arg) {
	$(arg).multiselect({
		maxHeight: 150,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		numberDisplayed: 2,
		filterPlaceholder: 'Search here...',
		disableIfEmpty: true,
		onDropdownHide: function () {

		}
	});
};
//hide scrollbar from body
var fnUpdateBody = function () {
	$('body').addClass("modal-open");
}


//custom search bar
function fnCustomSearch() {
	$(".search").keyup(function () {
		var searchTerm = $(".search").val();
		var listItem = $('.results tbody').children('tr');
		var searchSplit = searchTerm.replace(/ /g, "'):containsi('")

		$.extend($.expr[':'], {
			'containsi': function (elem, i, match, array) {
				return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
			}
		});

		$(".results tbody td").removeAttr("style");
		if ($(".search").val().length != 0) {
			$(".results tbody td:containsi('" + searchSplit + "')").each(function (e) {
				$(this).css('background', 'yellow');
			});
		} else {
			$(".results tbody td").removeAttr("style");
		}

	});
}
function fnClearCustomSearch(tableClass) {
	$(".search").val('');
	$.each(tableClass, function (i, e) {
		$(e + " tbody td").removeAttr("style");
	});
}

function changeLocale(val) {
	var d = shiksha.invokeAjax("changeLocale?applang=" + val, null, "POST");
	if (d == "true") location.reload();
}

(function () {
	$.each($('.modal'), function (indeX, ele) {

		$('.modal').on('hide.bs.modal', function (e) {
			$(this).find(".modal-body").scrollTop(0);
		});
	})

})();
$(document).ready(function () {
	$("#mdlResetLoc,#mdlWarningUnamp").on("shown.bs.modal", function () {
		$(".modal-backdrop:last-child").css({ "z-index": "9999" });
	});
	if (jQuery.validator) {
		jQuery.validator.addMethod("accept", function (value, element, param) {
			//return value.match(new RegExp("^" + param + "$"));
			//var regx=/^[^'?\"/\\]*$/;
			return  value.match(param);
		}, "");
	}
	$('.select').on('change', function () {
		$(this).trigger("blur");
	});
	$('.datepicker').on('changeDate', function () {
		$(this).trigger("blur");
	});

	$(".header-nav-link.lang-change-icon").on("click", function () {

		if ($(this).hasClass("en")) {

			changeLocale('hi');
		} else {

			changeLocale('en');
		}
		//$(this).toggleClass("active");
	});
	$('.notifications-icon').on('click', function () {
		if (!$('.lms-notification-options').hasClass('active')) {
			updateNotificationData();
			fnUpdateIsShown();
		} else {
			$('.lms-notification-options').toggleClass('active');
		}
	});
	
	$("#clearAllNoti").on("click",fnDissmissAllNotification);
	
	$(".notifications-menu-items-list").on("click","#delNotification",function(e){
		e.stopPropagation();
		e.preventDefault();
		fnDeleteNotification(this, $(this).closest("li").attr("data-notificationid"))
	});
	$("#mdlResetLoc,#mdlWarningUnamp").on("hidden.bs.modal", function () {
		if($(".modal-backdrop").length){
			$("body").addClass("modal-open");
		}
	});

});