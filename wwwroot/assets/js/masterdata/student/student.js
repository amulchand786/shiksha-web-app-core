//global variables
var colorClass = '';
var allInputEleNamesForStudent = [];
var schoolGradeFormId;
var addFormId;
var editFormId;
var gradeIdList;
var sectionIdList;
var addSectionsIds;
var editSectionsIds;
var editMapperId;
var schoolId;
var mappedGrades;
var mappedSchool;
var deletedGradeId;
var deletedSchoolId;
var deleteSchoolGradeId;
var addButtonId;
var sectionId;
var academicYear;
var gradeId;
var selectedChkBoxForSchoolDel = [];

var editedRowId;

var listTableName;
var sectionObj;
var elementIdMap;
var resetButtonObj;
var thLabels;
var json;
// global selectDivIds for smart filter
allInputEleNamesForStudent = [ 'addStudentId', 'addStudentName',
		'addFatherName', 'addMotherName', 'addAge', 'selectBox_forAddCast',
		'addSelectBox_forGender', 'startDate', 'selectBox_forReligion',
		'addAddress' ];
var studentDatePicker = {};
studentDatePicker.modifyDate = "#modifiedDatePicker";
studentDatePicker.createdDate = "#createdDatePicker";

var schoolGradePageContext = {};
schoolGradePageContext.city = "#divSelectCity";
schoolGradePageContext.block = "#divSelectBlock";
schoolGradePageContext.NP = "#divSelectNP";
schoolGradePageContext.GP = "#divSelectGP";
schoolGradePageContext.RV = "#divSelectRV";
schoolGradePageContext.village = "#divSelectVillage";
schoolGradePageContext.school = "#divSelectSchool";

schoolGradePageContext.schoolLocationType = "#selectBox_schoolLocationType";
schoolGradePageContext.elementSchool = "#selectBox_schoolsByLoc";
schoolGradePageContext.grade = "#selectBox_grades";

schoolGradePageContext.resetLocDivCity = "#divResetLocCity";
schoolGradePageContext.resetLocDivVillage = "#divResetLocVillage";

var addSchGrdSecModalElements = {};
addSchGrdSecModalElements.section = "#mdlAddSchoolGradeSec #selectBox_section";
addSchGrdSecModalElements.grade = "#mdlAddSchoolGradeSec #selectBox_grades";

var editSchGrdSecModalElements = {};
editSchGrdSecModalElements.section = "#mdlEditSchoolGradeSec #selectBox_section";
editSchGrdSecModalElements.grade = "#mdlEditSchoolGradeSec #selectBox_grades";

// initialise global variables
var initGlblVariables = function() {
	schoolGradeFormId = 'listSchoolGradeForm';
	addFormId = 'addSchoolGradeSecForm';
	editFormId = 'editSchoolGradeSecForm';
	gradeIdList = [];
	schoolId = '';
	mappedGrades = [];
	mappedSchool = '';
	deletedGradeId = '';
	deletedSchoolId = '';
	deleteSchoolGradeId = '';
	listTableName = 'schoolGradeTable';
	editedRowId = '';
	addSectionsIds = [];
	editSectionsIds = [];
	editMapperId = '';
	addButtonId = '';
	// get all section on page load
	sectionObj = customAjaxCalling("section/list", null, "GET");
	sectionIdList = [];
	$.each(sectionObj, function(index, obj) {
		sectionIdList.push(obj.sectionId);
	});
}

var fnInitFilterDatePicker = function(pDatePickerId) {
	var todayDate = new Date();
	$(pDatePickerId).datepicker({
		allowPastDates : true,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		},
	})
	$(pDatePickerId).datepicker('setDate', todayDate);
}

var resetAndHideDatePicker = function() {
	$('#divCreatedDate').hide();
	$('#divModifiedDate').hide();
	fnInitFilterDatePicker(studentDatePicker.createdDate);
	fnInitFilterDatePicker(studentDatePicker.modifyDate);
	$('[name="createdDate"]').val('');

}

// hide grade,section,upload file div
var fnHideGrSecFile = function() {
	$('#divSelectGrade').hide();
	$('#divSelectSection').hide();
	$('#divSelectSection').hide();
	$('#divSelectAcademicYear').hide();
	$('#editStudentButton').hide();
	$('#addStudentButton').hide();
	resetAndHideDatePicker();
	$('#rowDiv2').hide();
}

// remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}

// displaying smart-filters
var displaySmartFilters = function(lCode, lLevelName) {
	$(".outer-loader").show();

	elementIdMap = {
		"State" : '#statesForZone',
		"Zone" : '#selectBox_zonesByState',
		"District" : '#selectBox_districtsByZone',
		"Tehsil" : '#selectBox_tehsilsByDistrict',
		"Block" : '#selectBox_blocksByTehsil',
		"City" : '#selectBox_cityByBlock',
		"Nyaya Panchayat" : '#selectBox_npByBlock',
		"Gram Panchayat" : '#selectBox_gpByNp',
		"Revenue Village" : '#selectBox_revenueVillagebyGp',
		"Village" : '#selectBox_villagebyRv',
		"School" : '#selectBox_schoolsByLoc'
	};
	displayLocationsOfSurvey(lCode, lLevelName, $('#divFilter'));

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};

var positionFilterDiv = function(type) {
	if (type == "R") {
		$(schoolGradePageContext.NP).insertAfter(
				$(schoolGradePageContext.block));
		$(schoolGradePageContext.RV).insertAfter($(schoolGradePageContext.GP));
		$(schoolGradePageContext.school).insertAfter(
				$(schoolGradePageContext.village));
	} else {
		$(schoolGradePageContext.school).insertAfter(
				$(schoolGradePageContext.city));
	}
}

// Smart filter //

// school location type on change...get locations...
$('#divSchoolLocType input:radio[name=locType]').change(
		function() {

			fnHideGrSecFile();
			$('#locColspFilter').removeClass('in');
			$('#locationPnlLink').text('Select Location');
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span")
					.removeClass("glyphicon-minus-sign").removeClass("red");
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span")
					.addClass("glyphicon-plus-sign").addClass("green");

			$('#tabPane').css('display', 'none');

			var typeCode = $(this).data('code');
			$('.outer-loader').show();
			reinitializeSmartFilter(elementIdMap);
			$("#hrDiv").show();
			if (typeCode.toLowerCase() == 'U'.toLowerCase()) {
				showDiv($(schoolGradePageContext.city),
						$(schoolGradePageContext.resetLocDivCity));
				hideDiv($(schoolGradePageContext.block),
						$(schoolGradePageContext.NP),
						$(schoolGradePageContext.GP),
						$(schoolGradePageContext.RV),
						$(schoolGradePageContext.village));
				displaySmartFilters($(this).data('code'), $(this).data(
						'levelname'));
				positionFilterDiv("U");
				fnCollapseMultiselect();
			} else {
				hideDiv($(schoolGradePageContext.city),
						$(schoolGradePageContext.resetLocDivCity));
				showDiv($(schoolGradePageContext.block),
						$(schoolGradePageContext.NP),
						$(schoolGradePageContext.GP),
						$(schoolGradePageContext.RV),
						$(schoolGradePageContext.village));
				displaySmartFilters($(this).data('code'), $(this).data(
						'levelname'));
				positionFilterDiv("R");
				fnCollapseMultiselect();
				resetAndHideDatePicker();
			}
			$('.outer-loader').hide();
			return;
		});

// custum start @Mukteshwar################################################

var getGradeList = function() {
	resetAndHideDatePicker();
	$('#rowDiv2').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();

	var schoolId = $("#selectBox_schoolsByLoc").val();
	var bintToElementId = $('#selectBox_grade');
	$('.outer-loader').show();

	bindGradesBySchoolToListBox(bintToElementId, schoolId, 0);
	setOptionsForMultipleSelect('#selectBox_grade');
	if ($('#selectBox_grade').children('option').length == 0) {
		var schoolName = $('#selectBox_schoolsByLoc').find('option:selected')
				.text();
		$('#msgText').text(
				"School " + schoolName + " is not mapped to any Grade");
		$('#mdlError').modal();
	} else {
		$('#divSelectGrade').show();
	}
	$('.outer-loader').hide();

	fnCollapseMultiselect();

}
var getSectionList = function() {
	$('#rowDiv2').hide();// hideTable
	$('#divSelectSection').hide();
	$('#divSelectAcademicYear').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
	resetAndHideDatePicker();
	$('#divSelectAcademicYear').hide();// hide academic year dropdown
	$('#addStudentBtn').prop('disabled', true);// Hide AddButton
	$("#bulkUploadButton").prop('disabled', true);// Hide BulkUpload Button
	$('#selectBox_Section').find("option:selected").prop('selected', false);
	$('#selectBox_Section').multiselect('refresh').multiselect('destroy');
	// $('#selectBox_Section')

	var schoolId = parseInt($("#selectBox_schoolsByLoc")
			.find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data(
			'id'));
	var bindToElementId = $('#selectBox_Section');

	$('.outer-loader').show();

	bindSourceBySchoolAndGradeToListBox(bindToElementId, schoolId, gradeId, 0);
	setOptionsForMultipleSelect('#selectBox_Section');
	$('.outer-loader').hide();
	$('#divSelectSection').show();

	fnCollapseMultiselect();

}

var getAcademicYearList = function() {
	$('#rowDiv2').hide();// hideTable
	$('#divSelectAcademicYear').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
	resetAndHideDatePicker();
	$('#addStudentBtn').prop('disabled', true);// Hide AddButton
	$("#bulkUploadButton").prop('disabled', true);// Hide BulkUpload Button
	$('#selectBox_AcademicYear').find("option:selected")
			.prop('selected', false);
	$('#selectBox_AcademicYear').multiselect('refresh').multiselect('destroy');
	deleteAllRow('studentTable');
	var bindToElementId = $('#selectBox_AcademicYear');
	var currentYear = customAjaxCalling("academicYear/currentAcademicYear",
			null, "GET");
	bindAcademicYearToLustBix(bindToElementId, currentYear.academicYearId);
	setOptionsForMultipleSelect('#selectBox_AcademicYear');
	if ($('#selectBox_AcademicYear').children('option').length != 0) {
		$('#addStudentBtn').prop('disabled', false);// Show AddButton
		$("#bulkUploadButton").prop('disabled', false);// Show BulkUpload
		// Button
		$('#addStudentButton').show();
		$('#editStudentButton').show(); // show add/view buttons
	}

	$('.outer-loader').hide();
	$('#divSelectAcademicYear').show();
	$('#divCreatedDate').show();

}

var fnDateParsing = function(data) {
	var sArray = data.split('-');
	var monthData = sArray[1];
	var dateData = sArray[0];
	var yearData = sArray[2];
	var dateStringData = Date.parse(monthData + ' ' + dateData + ' ,'
			+ yearData);
	return dateStringData;
}

var isValidModifiedDate = function() {

	var createdDateData = fnDateParsing($(studentDatePicker.createdDate)
			.datepicker('getFormattedDate'));
	var modifyDateData = fnDateParsing($(studentDatePicker.modifyDate)
			.datepicker('getFormattedDate'));
	if (modifyDateData < createdDateData) {
		return false;
	}
	return true;

}

var uploadStudentImage = function(file, studentId) {
	spinner.showSpinner();
	var formData = new FormData();
	formData.append('uploadfile', file);
	$.ajax({
		url : "student/avtar?student=" + studentId,
		type : "POST",
		data : formData,
		enctype : 'multipart/form-data',
		processData : false,
		contentType : false,
		cache : false,
		async : true,
		success : function(response) {
			spinner.hideSpinner();
			console.log(response, "successfully uploaded");
			if (response.response == "shiksha-200") {
				$("#sImage,#esImage").attr("src", response.photoUrl).attr("data-photourl",response.photoUrl);
				if(studentId){
					AJS.flag({
						type : "success",
						title : "Success!",
						body  : response.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Error..!",
					body : response.responseMessage,
					close : 'auto'
				});
			}
		},
		error : function() {
			spinner.hideSpinner();
			AJS.flag({
				type : "error",
				title : "Oops..!",
				body : appMessgaes.serverError,
				close : 'auto'
			})
		}
	});
}

// enable or disable delete all button for school
var fnEnableOrDisableDelAllButtonForStudent = function(selectedArr) {
	if (selectedArr.length >= 2) {
		$('#btnDeleteAllCampaignSchoolForSchool').css('display', 'block');

	} else {
		$('#btnDeleteAllCampaignSchoolForSchool').css('display', 'none');

	}
}

var createStudentRow = function(student) {

	var showDate = new Date();
	showDate = new Date(showDate.setDate(showDate.getDate() - 1097));
	showDate = new moment(showDate).format('DD-MMM-YYYY');
	var studentName = student.name == null ? " " : student.name;
	var fatherName = student.fatherName == null ? " " : student.fatherName;
	var studentDateOfBirth = (student.dateOfBirth != null ? student.dateOfBirth
			: showDate);
	var studAddress = (student.homeAddress != null ? (student.homeAddress)
			.replace(/\\n/g, ' ') : " ");
	var motherName = student.motherName == null ? " " : student.motherName;
	var genderInCapitalize = '';
	if (student.gender != null) {
		genderInCapitalize = student.gender.toLowerCase();
		genderInCapitalize = genderInCapitalize.charAt(0).toUpperCase()
				+ genderInCapitalize.slice(1);
	}
	
	var image = student.photoUrl ? student.photoUrl : ""; 
	var row = "<tr id='"
			+ student.studentId
			+ "' data-id='"
			+ student.studentId
			+ "'>"
			+ "<td class='checkBoxInTd' id='"
			+ student.studentId
			+ "'><center> <input class='chkboxDelSchoolForStudent' id='chkboxDelSchoolForSchool"
			+ student.studentId
			+ "'  type='checkbox'   onchange='fnChkUnchkDelSchoolForStudent("
			+ student.studentId
			+ ");'></input></center></td>"
			+ "<td data-studentName='"
			+ student.name
			+ "' title='"
			+ student.name
			+ "'>"
			+ studentName
			+ "</td>"
			+ "<td data-fatherName='"
			+ student.fatherName
			+ "'	title='"
			+ student.fatherName
			+ "'>"
			+ fatherName
			+ "</td>"
			+ "<td data-motherName='"
			+ student.motherName
			+ "'title='"
			+ student.motherName
			+ "'>"
			+ student.motherName
			+ "</td>"
			+ "<td data-rollNumber='"
			+ student.rollNumber
			+ "'title='"
			+ student.rollNumber
			+ "'>"
			+ student.rollNumber
			+ "</td>"
			+ "<td data-dateOfBirth='"
			+ student.dateOfBirth
			+ "'	title='"
			+ student.dateOfBirth
			+ "'>"
			+ studentDateOfBirth
			+ "</td>"
			+ "<td data-gender='"
			+ genderInCapitalize
			+ "'	title='"
			+ genderInCapitalize
			+ "'>"
			+ genderInCapitalize
			+ "</td>"
			+ "<td data-homeAddress='"
			+ student.homeAddress
			+ "'title='"
			+ student.homeAddress
			+ "'>"
			+ studAddress
			+ "</td>"
			+ "<td data-modifiedBy='"
			+ student.modifiedBy
			+ "'title='"
			+ student.modifiedBy
			+ "'>"
			+ student.modifiedBy
			+ "</td>"
			+ "<td data-uploadedBy='"
			+ student.uploadedBy
			+ "'title='"
			+ student.uploadedBy
			+ "'>"
			+ student.uploadedBy
			+ "</td>"
			+ " <td><div class='div-tooltip'><a class='tooltip tooltip-top' data-photourl='"
			+ image
			+ "' id='btnEditUser'"
			+ "onclick='editStudentData(&quot;"
			+ student.studentId
			+ "&quot;,&quot;"
			+ escapeHtmlCharacters(studentName)
			+ "&quot;,&quot;"
			+ escapeHtmlCharacters(fatherName)
			+ "&quot;,&quot;"
			+ escapeHtmlCharacters(motherName)
			+ "&quot;,&quot;"
			+ student.rollNumber
			+ "&quot;,&quot;"
			+ studentDateOfBirth
			+ "&quot;,&quot;"
			+ escapeHtmlCharacters(studAddress)
			+ "&quot;,&quot;"
			+ student.gender
			+ "&quot;,&quot;"
			+ student.age
			+ "&quot;,&quot;"
			+ student.religion
			+ "&quot;,&quot;"
			+ student.cast
			+ "&quot;,&quot;"
			+ student.academicYear
			+ "&quot;,&quot;"
			+ student.academicYearId
			+ "&quot;,this)'; ><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;'' class='tooltip tooltip-top'	id='deleteStudent"
			+ "' onclick=deleteStudentData(&quot;"
			+ student.studentId
			+ "&quot;,&quot;"
			+ escapeHtmlCharacters(studentName)
			+ "&quot;,&quot;"
			+ student.rollNumber
			+ "&quot;); ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"
			+ "</tr>";
	return row;
}

var fnAppendNewRow = function(tableName, aRow) {
	var oaTable = $("#" + tableName).DataTable();
	oaTable.page();
	oaTable.row.add($(aRow)).draw(false);
	var currentRows = oaTable.data().toArray(); // current table data
	// Issue-SHK-964
	var newRow = currentRows.pop();
	var info = $('#' + tableName).DataTable().page.info();
	var startRow = info.start;
	currentRows.splice(startRow, 0, newRow);

	oaTable.clear();
	oaTable.rows.add(currentRows);

	oaTable.draw(false);

}

var removeActionButton = function(buttonId) {

	$("#studentTable tr").each(function() {
		$(this).find("#" + buttonId).remove();

	});
}

var applyPermissions = function() {

	if (!editStudentPermission) {
		removeActionButton("btnEditUser");

	}
	if (!deleteStudentPermission) {
		removeActionButton("deleteStudent");
	}

}

var studentListBySchoolGradeSectionAndYear = function() {
	var isValidDate = isValidModifiedDate();
	if (isValidDate) {
		$('.outer-loader').show();
		var schoolId = parseInt($("#selectBox_schoolsByLoc").find(
				'option:selected').data('id'));
		var gradeId = parseInt($("#selectBox_grade").find('option:selected')
				.data('id'));
		var sectionId = parseInt($("#selectBox_Section")
				.find('option:selected').data('id'));
		var academicYearId = parseInt($("#selectBox_AcademicYear").find(
				'option:selected').data('id'));

		var modifiedDate = $(studentDatePicker.modifyDate).datepicker(
				'getFormattedDate');
		var createdDate = $('[name="createdDate"]').val();
		if (createdDate != "") {
			createdDate = $(studentDatePicker.createdDate).datepicker(
					'getFormattedDate');
		}

		var json = {
			"schoolId" : schoolId,
			"gradeId" : gradeId,
			"sectionId" : sectionId,
			"academicYearId" : academicYearId,
			"createdDate" : createdDate,
			"modifiedDate" : modifiedDate
		};

		$('.outer-loader').show();

		var result = customAjaxCalling("studentlistSortByName", json, "POST");
		if (result == 0) {
			$('#msgText').text("Student data not available");
			$('#mdlError').modal();
			$('.outer-loader').hide();
		}

		deleteAllRow('studentTable');
		selectedChkBoxForSchoolDel = [];
		fnEnableOrDisableDelAllButtonForStudent(selectedChkBoxForSchoolDel);
		$("#chkboxSelectAllDelSchoolForStudent").prop("checked", false);
		$("#addStudentBtn").removeAttr('disabled');
		$("#bulkUploadButton").removeAttr('disabled');

		$.each(result, function(index, obj) {
			var row = createStudentRow(obj);
			fnAppendNewRow('studentTable', row);

		});

		applyPermissions();

		// below code apply datatable & filter
		if (deleteMultipleStudentPermission) {

			fnUpdateDTColFilterForMultiDelete('#studentTable', [ 5, 8 ], 11, [
					0, 10 ], thLabels);
		} else {

			fnUpdateDTColFilter('#studentTable', [ 5, 8 ], 11, [ 0, 10 ],
					thLabels);
		}

		$('#rowDiv2').show();
		$('#studentTable').dataTable().fnDraw();
		$(".multiselect-container").unbind("mouseleave");
		$(".multiselect-container").on("mouseleave", function() {
			$(this).click();
		});
		$('.outer-loader').hide();

		$(window).scrollTop(60);

		return;

	}

}

var viewOrEditStudent = function() {
	$('#addStudentButton').show();
	$('#editStudentButton').show();
	$('#linkToUploadTeacher').show();
	$('#addStudentBtn').prop('disabled', false);

}

var gradeAndSectionHide = function() {
	$('#rowDiv2').hide();
	$('#divSelectGrade').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
	resetAndHideDatePicker();

}

// check school grade selecte or not
var addStudent = function() {

	var isSchoolselected = $('#selectBox_schoolsByLoc').val();
	var isGradeselected = $('#selectBox_grade').find('option:selected').val();
	var isSectionselected = $('#selectBox_Section').val();

	/*
	 * if (isLocationSelected == null || isLocationSelected == " " ||
	 * isLocationSelected == "NONE") { $('#msgText').text('');
	 * $('#msgText').text('Please select Location Type.');
	 * $('#mdlError').modal("show"); return false; }else
	 */if (isSchoolselected == null || isSchoolselected == " "
			|| isSchoolselected == "NONE") {
		$('#msgText').text('');
		$('#msgText').text('Please select school .');
		$('#mdlError').modal("show");
		return false;
	} else if (isGradeselected == null || isGradeselected == " "
			|| isGradeselected == "NONE") {
		$('#msgText').text('');
		$('#msgText').text('Please select Grade .');
		$('#mdlError').modal("show");
		return false;
	} else if (isSectionselected == null || isSectionselected == " "
			|| isSectionselected == "NONE") {
		$('#msgText').text('');
		$('#msgText').text('Please select Section .');
		$('#mdlError').modal("show");
		return false;
	} else {

		resetFormById('addStudentForm');
		setOptionsForMultipleSelectWithoutSearch('#selectBox_forReligion');
		setOptionsForMultipleSelectWithoutSearch('#selectBox_forAddCast');
		$("#sImage").attr("src","web-resources/images/user.png").removeAttr("data-photourl");
		$('#add_student').modal().show();
	}
}
// add student function
var addStudentData = function(event) {

	schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected')
			.data('id'));
	sectionId = parseInt($("#selectBox_Section").find('option:selected').data(
			'id'));
	gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	academicYear = parseInt($("#selectBox_AcademicYearForAddStudent").find(
			'option:selected').data('id'));
	var rollNo = parseInt($('#addStudentId').val());
	var studentName = $('#addStudentName').val();
	var fatherName = $('#addFatherName').val();
	var motherName = $('#addMotherName').val();

	var cast = $('#selectBox_forAddCast').find('option:selected').val();
	var gender = $('input[name=gender]:checked').val();
	var religion = $("#selectBox_forReligion").val();
	var dob = $('#statrtDatePicker').datepicker('getFormattedDate');
	var age = parseInt($('#addAge').val());
	var address = $('#addAddress').val();
	var photoUrl = $("#sImage").attr("data-photourl") ? $("#sImage").attr("data-photourl") : "";
	json = {
		"sectionId" : sectionId,
		"rollNumber" : rollNo,
		"religion" : religion,
		"name" : studentName,
		"fatherName" : fatherName,
		"motherName" : motherName,
		"schoolId" : schoolId,
		"gradeId" : gradeId,
		"age" : age,
		"dateOfBirth" : dob,
		"homeAddress" : address,
		"gender" : gender,
		"cast" : cast,
		"academicYearId" : academicYear,
		"photoUrl" : photoUrl
	}
	var response = customAjaxCalling("studentadd", json, "POST");
	var btnAddMoreStudentId = $(event.target).data('formValidation')
			.getSubmitButton().data('id');
	if (response !== null && response !== "") {// conditon for handle null
		if (response.response == "shiksha-200") {
			$(".outer-loader").hide();

			var newRow = createStudentRow(response);
			fnAppendNewRow("studentTable", newRow);
			selectedChkBoxForSchoolDel = [];
			fnEnableOrDisableDelAllButtonForStudent(selectedChkBoxForSchoolDel);
			$("#chkboxSelectAllDelSchoolForStudent").prop("checked", false);
			applyPermissions();

			if (deleteMultipleStudentPermission) {
				fnUpdateDTColFilterForMultiDelete('#studentTable', [ 5, 8 ],
						11, [ 0, 10 ], thLabels);
			} else {
				fnUpdateDTColFilter('#studentTable', [ 5, 8 ], 11, [ 0, 10 ],
						thLabels);
			}
			setShowAllTagColor(colorClass);
			displaySuccessMsg();
			$(".multiselect-container").unbind("mouseleave");
			$(".multiselect-container").on("mouseleave", function() {
				$(this).click();
			});
			if (btnAddMoreStudentId == "addStudentSaveMoreButton") {
				$("#add_student").find('#errorMessage').hide();

				// reset fields after add
				$('#addStudentForm').bootstrapValidator('resetForm', true);

				$("#selectBox_forAddCast").multiselect('destroy').multiselect(
						'refresh');
				$("#selectBox_forReligion").multiselect('destroy').multiselect(
						'refresh');
				$("#sImage").attr("src","web-resources/images/user.png").removeAttr("data-photourl");
				fnDisplaySaveAndAddNewElement('#add_student', studentName);

			} else {
				$('#add_student').modal("hide");
				setTimeout(function() {
					AJS.flag({
						type : 'success',
						title : 'Success!',
						body : '<p>' + response.responseMessage + '</p>',
						close : 'auto'
					})
				}, 1000);

				// reset fields after add
				$('#addStudentForm').bootstrapValidator('resetForm', true);

				$("#selectBox_forAddCast").multiselect('destroy').multiselect(
						'refresh');
				$("#selectBox_forReligion").multiselect('destroy').multiselect(
						'refresh');

			}
		} else {
			showDiv($('#add_student #alertdiv'));
			$("#add_student").find('#successMessage').hide();
			$("#add_student").find('#okButton').hide();
			$("#add_student").find('#errorMessage').show();
			$("#add_student").find('#exception').show();
			$("#add_student").find('#buttonGroup').show();
			$("#add_student").find('#exception').text(response.responseMessage);
		}
	}
	$(".outer-loader").hide();
}
var destroyReInitializeMultiselect = function(stringId) {
	$("#" + stringId).multiselect('destroy');
	$("#" + stringId).multiselect('refresh');
}

var studentId;
var editStudentData = function() {
	var id = arguments[0];
	studentId = id;
	var name = arguments[1];
	var fatherName = arguments[2];
	var motherName = arguments[3];
	var rollNumber = arguments[4];

	var dateOfBirth = arguments[5];
	var adderess = arguments[6];
	var gender = arguments[7];
	var age = arguments[8];
	var relegion = arguments[9];
	var cast = arguments[10];
	var academicYear = arguments[11];
	var academicYearId = arguments[12];
	var element = arguments[13];
	
	resetFormById('editStudentForm');
	setOptionsForMultipleSelectWithoutSearch('#selectBox_forCastEdit');
	setOptionsForMultipleSelectWithoutSearch('#selectBox_forReligionEdit');

	$("#selectBox_AcademicYearForEditStudent").empty();
	$("#selectBox_AcademicYearForEditStudent").multiselect('destroy');
	$("#selectBox_AcademicYearForEditStudent").append(
			'<option data-id="' + academicYearId + '" value="' + academicYearId
					+ '"selected>' + academicYear + '</option>');
	$("#selectBox_AcademicYearForEditStudent").multiselect('refresh');

	var showDate = new Date();
	showDate = new Date(showDate.setDate(showDate.getDate() - 1097));
	showDate = new moment(showDate).format('DD-MMM-YYYY');
	if (dateOfBirth == null
			|| dateOfBirth == 'null'
			|| $('#EditDateOfBirthDatePicker').datepicker('getFormattedDate') == 'Invalid Date') {
		dateOfBirth = showDate;

	}
	$('#editStudentForm').bootstrapValidator('resetForm', true);
	$('#edit_student #editStudentName').val(name);
	var photoUrl = $(element).attr("data-photourl");
	console.log(photoUrl);
	if (photoUrl) {
		$("#esImage").attr("src", photoUrl).attr("data-photourl",photoUrl);
	} else {
		$("#esImage").attr("src", "web-resources/images/user.png").removeAttr("data-photourl");
	}
	$('#edit_student #editMotherName').val(motherName);
	$('#edit_student #editFatherName').val(fatherName);
	$('#edit_student #editAge').val(age);
	$('#startDateEdit').find('#EditDateOfBirthDatePicker').datepicker(
			'setDate', dateOfBirth);
	$('#edit_student #editAddress').val(adderess);
	$('#edit_student #editStudentId').val(rollNumber);

	$('#selectBox_forCastEdit').find('option').each(function(ind, obj) {
		if ($(obj).val() == cast) {
			$(obj).prop('selected', true);
		}
	});

	$('#selectBox_forCastEdit').multiselect('destroy');
	$('#selectBox_forCastEdit').multiselect('refresh');
	setOptionsForMultipleSelect('#selectBox_forCastEdit');

	if (gender == "MALE") {
		$('#EditgenderMale').prop('checked', true);
	} else if (gender == "FEMALE") {
		$('#EditgenderFemale').prop('checked', true);
	} else {
		$('#EditgenderMale').prop('checked', false);
		$('#EditgenderFemale').prop('checked', false);
	}

	$('#selectBox_forReligionEdit').find('option').each(function(ind, obj) {
		if ($(obj).val() == relegion) {
			$(obj).prop('selected', true);
		}
	});
	$('#selectBox_forReligionEdit').multiselect('destroy');
	$('#selectBox_forReligionEdit').multiselect('refresh');
	setOptionsForMultipleSelect('#selectBox_forReligionEdit');
	$('#addStateSaveMoreButton').prop('disabled', false);
	$("#edit_student").modal().show();
	fnCollapseMultiselect();

}
var editStudent = function() {

	var schoolId = $('#selectBox_schoolsByLoc').find('option:selected').data(
			'id');
	var gradeId = $('#selectBox_grade').find('option:selected').data('id');
	sectionId = $('#selectBox_Section').find('option:selected').data('id');
	var academicYearId = $('#selectBox_AcademicYearForEditStudent').find(
			'option:selected').data('id');
	var name = $('#edit_student #editStudentName').val();
	var motherName = $('#edit_student #editMotherName').val();
	var fatherName = $('#edit_student #editFatherName').val();
	var age = $('#edit_student #editAge').val();
	var studentDob = $('#EditDateOfBirthDatePicker').datepicker(
			'getFormattedDate');
	var dateOfBirth = (studentDob != "Invalid Date" ? studentDob : null);
	var address = $('#edit_student #editAddress').val();
	var rollNumber = parseInt($('#edit_student #editStudentId').val());

	var cast = $('#selectBox_forCastEdit').val();
	var gender = $('input[name=gender]:checked').val();
	var religion = $('#selectBox_forReligionEdit').val();
	var photoUrl = $("#esImage").attr("src");
	json = {
		"studentId" : studentId,
		"rollNumber" : rollNumber,
		"religion" : religion,
		"name" : name,
		"fatherName" : fatherName,
		"motherName" : motherName,
		"schoolId" : schoolId,
		"gradeId" : gradeId,
		"sectionId" : sectionId,
		"age" : age,
		"dateOfBirth" : dateOfBirth,
		"homeAddress" : address,
		"gender" : gender,
		"cast" : cast,
		"academicYearId" : academicYearId,
		"photoUrl":photoUrl
	}

	var result = customAjaxCalling("editStudent", json, "PUT");
	if (result !== null && result !== "") {// conditon for handle null
		if (result.response == "shiksha-200") {
			$(".outer-loader").hide();
			deleteRow("studentTable", result.studentId);
			var newRow = createStudentRow(result);
			fnAppendNewRow("studentTable", newRow);
			selectedChkBoxForSchoolDel = [];
			fnEnableOrDisableDelAllButtonForStudent(selectedChkBoxForSchoolDel);
			$("#chkboxSelectAllDelSchoolForStudent").prop("checked", false);
			applyPermissions();
			displaySuccessMsg();

			if (deleteMultipleStudentPermission) {
				fnUpdateDTColFilterForMultiDelete('#studentTable', [ 5, 8 ],
						11, [ 0, 10 ], thLabels);
			} else {
				fnUpdateDTColFilter('#studentTable', [ 5, 8 ], 11, [ 0, 10 ],
						thLabels);
			}
			setShowAllTagColor(colorClass);
			$(".multiselect-container").unbind("mouseleave");
			$(".multiselect-container").on("mouseleave", function() {
				$(this).click();
			});
			$('#edit_student').modal("hide");

			setTimeout(function() {
				AJS.flag({
					type : 'success',
					title : 'Success!',
					body : '<p>' + result.responseMessage + '</p>',
					close : 'auto'
				})
			}, 1000);

			$(window).scrollTop(0);
		} else {
			$('#addStateSaveMoreButton').prop('disabled', true);
			showDiv($('#edit_student #alertdiv'));
			$("#edit_student").find('#successMessage').hide();
			$("#edit_student").find('#errorMessage').show();
			$("#edit_student").find('#exception').show();
			$("#edit_student").find('#buttonGroup').show();
			$("#edit_student").find('#exception').text(result.responseMessage);

		}
	}
	$(".outer-loader").hide();
}
var deleteStudentId = null;
var deleteStudentName = null;
var deleteStudentRollNo = null;
var deleteStudentData = function(studentId, name, rollNo) {
	deleteStudentId = studentId;
	deleteStudentName = name;
	deleteStudentRollNo = rollNo;
	$('#delete-student #deleteStudentMessage').text(
			"Do you want to delete " + name + " ?");
	$("#delete-student").modal().show();
	hideDiv($('#delete-student #alertdiv'));
};
$(document)
		.on(
				'click',
				'#deleteStudentButton',
				function() {
					$(".outer-loader").show();
					var schoolId = $('#selectBox_schoolsByLoc').find(
							'option:selected').data('id');
					var gradeId = $('#selectBox_grade').find('option:selected')
							.data('id');
					var sectionId = $('#selectBox_Section').find(
							'option:selected').data('id');
					var academicYearId = $('#selectBox_AcademicYear').find(
							'option:selected').data('id');
					var json = {
						"schoolId" : schoolId,
						"gradeId" : gradeId,
						"sectionId" : sectionId,
						"academicYearId" : academicYearId,
						"studentId" : deleteStudentId
					}
					var studentResponse = customAjaxCalling("studentdelete",
							json, "DELETE");
					if (studentResponse != null) {// conditon for handle null
						if (studentResponse.response == "shiksha-200") {
							$(".outer-loader").hide();
							// Delete Row from data table and refresh the table
							// without
							// refreshing the whole table
							deleteRow("studentTable", deleteStudentId);

							selectedChkBoxForSchoolDel = jQuery.grep(
									selectedChkBoxForSchoolDel,
									function(value) {
										return value != deleteStudentId;
									});
							fnEnableOrDisableDelAllButtonForStudent(selectedChkBoxForSchoolDel);
							if (deleteMultipleStudentPermission) {
								fnUpdateDTColFilterForMultiDelete(
										'#studentTable', [ 5 ], 8, [ 0, 7 ],
										thLabels);
							} else {
								fnUpdateColumnFilter('#studentTable', true,
										[ 5 ], 8, [ 0, 7 ]);
							}
							setShowAllTagColor(colorClass);
							$(".multiselect-container").unbind("mouseleave");
							$(".multiselect-container").on("mouseleave",
									function() {
										$(this).click();
									});
							$('#delete-student').modal("hide");

							setTimeout(function() {
								AJS.flag({
									type : 'success',
									title : 'Success!',
									body : '<p>Student deleted</p>',
									close : 'auto'
								})
							}, 1000);

						} else {
							showDiv($('#delete-student #alertdiv'));
							$("#delete-student").find('#errorMessage').show();
							$("#delete-student").find('#exception').text(
									studentResponse.responseMessage);
							$("#delete-student").find('#buttonGroup').show();
						}
					}
					$(".outer-loader").hide();
				});

var initInputAftrSave = function() {
	// refreshMultiselect($('#selectBox_forGender'));Atauto:-
	$("#addSelectBox_forGender").multiselect('destroy');
	$("#addSelectBox_forGender").each(function() {
		this.selectedIndex = 0
	});
	enableMultiSelect($("#addSelectBox_forGender"));

	$("#selectBox_forAddCast").multiselect('destroy');
	$("#selectBox_forAddCast").each(function() {
		this.selectedIndex = 0
	});
	enableMultiSelect($("#selectBox_forAddCast"));

	$("#selectBox_forReligion").multiselect('destroy');
	$("#selectBox_forReligion").each(function() {
		this.selectedIndex = 0
	});
	enableMultiSelect($("#selectBox_forReligion"));

	$("#add_student #addStudentForm").find("#alertDiv #errorMessage").hide();
	$("#add_student #addStudentForm").data('formValidation').resetForm();
}
var initInputAftrSaveAndAddNew = function() {
	$("#addSelectBox_forGender").multiselect('destroy');
	$("#addSelectBox_forGender").each(function() {
		this.selectedIndex = 0
	});
	enableMultiSelect($("#addSelectBox_forGender"));

	$("#selectBox_forAddCast").multiselect('destroy');
	$("#selectBox_forAddCast").each(function() {
		this.selectedIndex = 0
	});
	enableMultiSelect($("#selectBox_forAddCast"));

	$("#selectBox_forReligion").multiselect('destroy');
	$("#selectBox_forReligion").each(function() {
		this.selectedIndex = 0
	});
	enableMultiSelect($("#selectBox_forReligion"));
	$("#sImage").attr("src","web-resources/images/user.png").removeAttr("data-photourl");
	$("#add_student").modal().show();

	$("#add_student #addStudentForm").find("#alertDiv #errorMessage").hide();
	$("#add_student #addStudentForm").data('formValidation').resetForm();

}

// reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addStudentForm');
	resetFormById('editStudenttForm');
	reinitializeSmartFilter(elementIdMap);
	if (type != "add") {
		initSmartFilter("edit");
	}

}

// reset only location when click on reset button icon
var resetLocAndSchool = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;

}
// invoking on click of resetLocation confirmation dialog box
var doResetLocation = function() {
	$('#addStudentBtn').prop('disabled', true);
	$("#bulkUploadButton").prop('disabled', true);
	$("#selectBox_grade").multiselect('destroy').multiselect('refresh');
	$("#selectBox_Section").multiselect('destroy').multiselect('refresh');
	$("#selectBox_AcademicYear").multiselect('destroy').multiselect('refresh');
	$("#divSelectGrade").hide();
	$("#divSelectSection").hide();
	$("#divSelectAcademicYear").hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
	resetAndHideDatePicker();
	$("#rowDiv2").hide();
	enableMultiSelect($("#selectBox_Section"));
	enableMultiSelect($("#selectBox_grade"));
	deleteAllRow("studentTable");
	if ($(resetButtonObj).parents("form").attr("id") == "listSudentForm") {
		reinitializeSmartFilter(elementIdMap);
		var aLocTypeCode = $('input:radio[name=locType]:checked').data('code');
		var aLLevelName = $('input:radio[name=locType]:checked').data(
				'levelname');
		displaySmartFilters(aLocTypeCode, aLLevelName);

	}
	fnCollapseMultiselect();
}

function resetFormData() {

	resetFormById('addStudentForm');
	$("#selectBox_forReligion").multiselect('destroy').multiselect('refresh');
	$("#selectBox_forAddCast").multiselect('destroy').multiselect('refresh');
	$('#editStudentForm').animate({
		scrollTop : 0
	}, 'fast');
	$('#addStudentForm').animate({
		scrollTop : 0
	}, 'fast');

}

var calculateAge = function(dString) {
	var sArray = dString.split('-');
	var dMonth = sArray[1];
	var dDate = sArray[0];
	var dYear = sArray[2];
	var dStr = dMonth + ' ' + dDate + ' ' + dYear;
	var t = new Date(dStr);
	var now = new Date();
	var age = Math.floor((now - t) / (365.25 * 24 * 60 * 60 * 1000));
	return age;
}

var initDatePicker = function() {

	var tomorrow = new Date();
	tomorrow = new Date(tomorrow.setDate(tomorrow.getDate() - 1096));
	var showDate = new Date();
	showDate = new Date(showDate.setDate(showDate.getDate() - 1097));
	showDate = new moment(showDate).format('DD-MMM-YYYY');
	for (var i = 0; i < arguments.length; i++) {
		arguments[i].datepicker({
			allowPastDates : true,
			date : showDate,
			restricted : [/*
							 * { from: -Infinity, to: aYearAgo },
							 */
			{
				from : tomorrow,
				to : Infinity
			} ],
			momentConfig : {
				culture : 'en', // change to specific culture
				format : 'DD-MMM-YYYY' // change for specific format
			}

		}).on(
				'changed.fu.datepicker dateClicked.fu.datepicker',
				function() {
					$('#addStudentForm').formValidation('revalidateField',
							'startDate');
					$('#editStudentForm').formValidation('revalidateField',
							'startDate');
					var startDate = $('#statrtDatePicker').datepicker(
							'getFormattedDate');
					var editDate = $('#EditDateOfBirthDatePicker').datepicker(
							'getFormattedDate');

					var sAge = calculateAge(startDate);
					var sEditAge = calculateAge(editDate);
					$('#addAge').val('');
					$('#addAge').val(sAge);
					excluded: true, $('#edit_student #editAge');
					$('#edit_student #editAge').val(sEditAge);
				});

	}
}

// delete multiple schools checkbox functionality
var fnDoselectAllForSchoolDeleteForMultiSelect = function(status) {

	var rows = $("#studentTable").dataTable().fnGetNodes();

	if (status) {
		selectedChkBoxForSchoolDel = [];
		$.each(rows, function(i, row) {

			selectedChkBoxForSchoolDel.push($(row).data('id'));

		});

	} else if (selectedChkBoxForSchoolDel.length == 0) {

		$.each(rows, function() {
			$('.chkboxDelSchoolForStudent').prop('checked', false);
		});
	}
}

// Handle click on "Select all" control
$('#chkboxSelectAllDelSchoolForStudent').on('click', function() {
	selectedChkBoxForSchoolDel = [];

	var isChecked = $('#chkboxSelectAllDelSchoolForStudent').is(':checked');

	var dRows = $("#studentTable").DataTable().rows({
		'search' : 'applied'
	}).nodes();
	$('input[type="checkbox"]', dRows).prop('checked', this.checked);

	fnDoselectAllForSchoolDeleteForMultiSelect(isChecked);
	fnEnableOrDisableDelAllButtonForStudent(selectedChkBoxForSchoolDel);
});

// delete school from table by selecting checkbox
var fnChkUnchkDelSchoolForStudent = function(rId) {

	var nRows = $("#studentTable").dataTable().fnGetNodes().length;
	var idx = $.inArray(rId, selectedChkBoxForSchoolDel);
	if (idx == -1) {
		selectedChkBoxForSchoolDel.push(rId);
		selectedChkBoxForSchoolDel.length == nRows ? $(
				'#chkboxSelectAllDelSchoolForStudent').prop('checked', true)
				: $('#chkboxSelectAllDelSchoolForStudent').prop('checked',
						false);
	} else {
		selectedChkBoxForSchoolDel.splice(idx, 1);
		$('#chkboxSelectAllDelSchoolForStudent').prop('checked', false);
	}
	fnEnableOrDisableDelAllButtonForStudent(selectedChkBoxForSchoolDel);

}

// delete all selected schools if deleteAll button pressed
var fnDeleteAllSchoolsForSchool = function() {
	$('#mdlRemoveAllSchool #msgText').text('');
	$('#mdlRemoveAllSchool #msgText').text(
			"Do you want to remove all selected Student(s) ?");
	$('#mdlRemoveAllSchool').modal().show();
}

// delete all selected schools if deleteAll button pressed

$('#mdlRemoveAllSchool #btnRemoveSchool')
		.on(
				'click',
				function() {
					var aDtRows = $("#studentTable").dataTable().fnGetNodes();
					var studentIds = [];
					$.each(aDtRows, function(i, row) {
						if ($(row).find('.chkboxDelSchoolForStudent').is(
								':checked') === true) {
							studentIds.push($(row).prop('id'));
						}
					});
					var schoolId = parseInt($("#selectBox_schoolsByLoc").find(
							'option:selected').data('id'));
					var academicYearId = parseInt($("#selectBox_AcademicYear")
							.find('option:selected').data('id'));

					var json = {
						"schoolId" : schoolId,
						"studentIds" : studentIds,
						"academicYearId" : academicYearId
					};

					var studentData = customAjaxCalling(
							"student/multipleDelete", json, "DELETE");

					if (studentData != null && studentData != "") {
						var studentNames = studentData.studentNames;
						if (studentData.studentIds.length != 0) {
							$(studentData.studentIds)
									.each(
											function(index, dataObj) {
												deleteRow("studentTable",
														dataObj);
												selectedChkBoxForSchoolDel = jQuery
														.grep(
																selectedChkBoxForSchoolDel,
																function(value) {
																	return value != dataObj;
																});
												fnEnableOrDisableDelAllButtonForStudent(selectedChkBoxForSchoolDel);
											});
						}

						if (studentNames.length != 0) {
							$('#mdlRemoveAllSchool').modal('hide');
							$("#studentNames").text("");
							$("#studentNames").html(studentNames);
							$("#mdlErrorRemoveAllStudent").modal();
						} else {
							AJS.flag({
								type : 'success',
								title : 'Success!',
								body : '<p>Students deleted</p>',
								close : 'auto'
							})
						}
						$("#chkboxSelectAllDelSchoolForStudent").prop(
								'checked', false);
						$("#btnDeleteAllCampaignSchoolForSchool").hide();
						$('#mdlRemoveAllSchool').modal("hide");
					}

				});

// /////////////////////
$(studentDatePicker.modifyDate).datepicker({
	allowPastDates : true,
	momentConfig : {
		culture : 'en', // change to specific culture
		format : 'DD-MMM-YYYY' // change for specific format
	}
}).on(
		'changed.fu.datepicker dateClicked.fu.datepicker',
		function() {
			var modifiedDate = $(studentDatePicker.modifyDate).datepicker(
					'getFormattedDate');
			var isValidDate = isValidModifiedDate();
			if (modifiedDate != "Invalid Date") {
				if (isValidDate) {
					$('#modifiedDateMsg').css('display', 'none');
					$("#editAndDeleteStudentBtn").removeClass("disabled");
					$("#divModifiedDate").removeClass("has-error");
				} else {
					$('#modifiedDateMsg').css('display', 'block');
					$("#editAndDeleteStudentBtn").addClass("disabled");
					$("#divModifiedDate").addClass("has-error");

				}
			}
		});
$(studentDatePicker.createdDate).datepicker({
	allowPastDates : true,
	momentConfig : {
		culture : 'en', // change to specific culture
		format : 'DD-MMM-YYYY' // change for specific format
	}
}).on(
		'changed.fu.datepicker dateClicked.fu.datepicker',
		function() {
			var createdDate = $(studentDatePicker.createdDate).datepicker(
					'getFormattedDate');
			var isValidDate = isValidModifiedDate();
			$('#divModifiedDate').show();
			if (createdDate != "Invalid Date") {
				if (isValidDate) {
					$('#modifiedDateMsg').css('display', 'none');
					$("#editAndDeleteStudentBtn").removeClass("disabled");
					$("#divModifiedDate").removeClass("has-error");
				} else {
					$('#modifiedDateMsg').css('display', 'block');
					$("#editAndDeleteStudentBtn").addClass("disabled");
					$("#divModifiedDate").addClass("has-error");
				}
			}
		});

$(function() {
	fnInitFilterDatePicker(studentDatePicker.createdDate);
	fnInitFilterDatePicker(studentDatePicker.modifyDate);

	$(window).bind("pageshow", function() {
		var form = $('form');
		form[0].reset();
	});
	thLabels = [ '#', 'Student Name  ', 'Father name  ', 'Mother Name ',
			'Roll No ', 'DOB ', 'Gender ', 'Address ', 'ModifiedBy ',
			'UploadedBy/CreatedBy ', 'Action' ];

	if (deleteMultipleStudentPermission) {
		fnSetDTColFilterPaginationForMultiDelete('#studentTable', 11,
				[ 0, 10 ], thLabels);
	} else {
		fnSetDTColFilterPagination('#studentTable', 11, [ 0, 10 ], thLabels);
	}

	fnMultipleSelAndToogleDTCol('.search_init', function() {
		fnHideColumns('#studentTable', [ 5, 8 ]);
	});
	fnColSpanIfDTEmpty('studentTable', 11);

	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red").removeClass("glyphicon-plus-sign").removeClass("green");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
		
	});  
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');

		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red").removeClass("glyphicon-plus-sign").removeClass("green");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	});

	colorClass = $('#t0').prop('class');

	// initialize and set fuel ux date picker

	initDatePicker($('#statrtDatePicker'));
	initDatePicker($('#EditDateOfBirthDatePicker'));

	setOptionsForMultipleSelect($(schoolGradePageContext.schoolLocationType));
	setOptionsForMultipleSelect($('#selectBox_grade'));
	setOptionsForMultipleSelect($('#selectBox_AcademicYear'));
	setOptionsForMultipleSelectWithoutSearch('#selectBox_AcademicYearForAddStudent');
	setOptionsForMultipleSelectWithoutSearch('#selectBox_AcademicYearForEditStudent');
	$('#rowDiv1').show();
	// init bootstrap tooltip
	$('[data-toggle="tooltip"]').tooltip();

	$("#addStudentId").keypress(function() {
		hideDiv($('#add_student #alertdiv'));
	})
	$("#editStudentId").keypress(function() {
		hideDiv($('#edit_student #alertdiv'));
	})

	$("#addStudentForm")
			.formValidation(
					{
						excluded : ':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							rollNumber : {
								verbose : false,
								validators : {
									notEmpty : {
										message : ' '
									},
									callback : {
										message : 'Roll number must be between 1 and 999999999',
										callback : function(value) {
											var rollNum = parseFloat(value);
											if (rollNum > 999999999) {
												return false;
											} else {
												var regx = /^[0-9]*[1-9][0-9]*$/;
												return regx.test(value);
											}
										}
									},
									remote : {
										message : 'Roll Number already exist. (Please specify other)',
										url : 'student/rollNumber',
										data : function(validator, $field,
												value) {
											var addSchoolId = parseInt($(
													"#selectBox_schoolsByLoc")
													.find('option:selected')
													.data('id'));
											var addGradeId = parseInt($(
													"#selectBox_grade").find(
													'option:selected').data(
													'id'));
											var addSectionId = parseInt($(
													"#selectBox_Section").find(
													'option:selected').data(
													'id'));
											var addAcademicYearId = parseInt($(
													"#selectBox_AcademicYearForAddStudent")
													.find('option:selected')
													.data('id'));

											return {
												schoolId : addSchoolId,
												gradeId : addGradeId,
												sectionId : addSectionId,
												academicYearId : addAcademicYearId,
												rollNumber : value,
												studentId : 0
											};

										},
										type : 'POST'
									}
								}
							},
							addStudentName : {
								validators : {
									stringLength : {
										max : 250,
										message : 'Student name must be less than 250 characters'
									},
									callback : {
										message : 'Student ' + invalidInput,
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											if (/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							startDate : {
								excluded : false,
								validators : {
									notEmpty : {
										message : ' '
									}

								}
							},

							age : {
								validators : {
									callback : {
										message : 'Student name does not contains these (", ?, \', /, \\) special characters',
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											return regx.test(value);
										}
									}
								}
							},

							fatherName : {
								validators : {
									stringLength : {
										max : 250,
										message : 'Father name must be less than 250 characters'
									},
									callback : {
										message : 'Father name ' + invalidInput,
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											if (/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							motherName : {
								validators : {
									stringLength : {
										max : 250,
										message : 'Mother name must be less than 250 characters'
									},
									callback : {
										message : 'Mother name ' + invalidInput,
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											if (/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							cast : {
								validators : {
									callback : {
										message : '      ',
										callback : function() {
											// Get the selected options
											var options = $(
													'#selectBox_forAddCast')
													.val();
											return (options != "NONE");
										}
									}
								}
							},
							gender : {
								validators : {
									notEmpty : {
										message : '  '
									}
								}
							},
							religion : {
								validators : {
									callback : {
										message : '      ',
										callback : function() {
											// Get the selected options
											var options = $(
													'#selectBox_forReligion')
													.val();
											return (options != "NONE");
										}
									}
								}
							},
							address : {
								validators : {
									callback : {
										message : 'Address does not contains these (", ?, \', /, \\) special characters',

										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;

											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addStudentData(e);
			});
	$("#editStudentForm")
			.formValidation(
					{
						excluded : ':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							editStudentId : {
								verbose : false,
								validators : {
									notEmpty : {
										message : ' '
									},
									callback : {
										message : 'Roll number must be between 1 and 999999999',
										callback : function(value) {
											var rollNum = parseFloat(value);
											if (rollNum > 999999999) {
												return false;
											} else {
												var regx = /^[0-9]*[1-9][0-9]*$/;
												return regx.test(value);
											}
										}
									},
									remote : {
										message : 'Roll Number already exist. (Please specify other)',
										url : 'student/rollNumber',
										data : function(validator, $field,
												value) {
											var editSchoolId = $(
													'#selectBox_schoolsByLoc')
													.find('option:selected')
													.data('id');
											var editGradeId = $(
													'#selectBox_grade').find(
													'option:selected').data(
													'id');
											var editSectionId = $(
													'#selectBox_Section').find(
													'option:selected').data(
													'id');
											var editAcademicYearId = $(
													'#selectBox_AcademicYearForEditStudent')
													.find('option:selected')
													.data('id');
											return {
												schoolId : editSchoolId,
												gradeId : editGradeId,
												sectionId : editSectionId,
												academicYearId : editAcademicYearId,
												rollNumber : value,
												studentId : studentId
											};

										},
										type : 'POST'
									}
								}
							},
							editStudentName : {
								validators : {
									stringLength : {
										max : 250,
										message : 'Student name must be less than 250 characters'
									},
									callback : {
										message : 'Student name '
												+ invalidInput,
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											if (/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							startDate : {
								validators : {
									notEmpty : {
										message : 'Date Of Birth is required'
									}
								}
							},
							editFatherName : {
								validators : {
									stringLength : {
										max : 250,
										message : 'Father name must be less than 250 characters'
									},
									callback : {
										message : 'Father name ' + invalidInput,
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											if (/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							editMotherName : {
								validators : {
									stringLength : {
										max : 250,
										message : 'Mother name must be less than 250 characters'
									},
									callback : {
										message : 'Mother name ' + invalidInput,
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;
											if (/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							editCast : {
								validators : {
									callback : {
										message : '      ',
										callback : function() {
											// Get the selected options
											var options = $(
													'#selectBox_forCastEdit')
													.val();
											return (options != "NONE");
										}
									}
								}
							},
							gender : {
								validators : {
									notEmpty : {
										message : 'The gender is required'
									}
								}
							},
							editReligion : {
								validators : {
									callback : {
										message : '      ',
										callback : function() {
											// Get the selected options
											var options = $(
													'#selectBox_forReligionEdit')
													.val();
											return (options != null);
										}
									}
								}
							},
							editAddress : {
								validators : {
									callback : {
										message : 'Address does not contains these (", ?, \', /, \\) special characters',
										callback : function(value) {
											var regx = /^[^'?\"/\\]*$/;

											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				editStudent();
			});
	$("#student-image1").on('change', 'input', function() {
		var filename=this.files[0].name;
		var sfilename=filename.split(".");
		if(['jpeg','jpg','png'].indexOf(sfilename[sfilename.length-1]) < 0){
				AJS.flag({
					type: "error",
					title: appMessgaes.error,
					body: "File not Supported",
					close: 'auto'
				});
		} else if (this.files[0] && (this.files[0].size > 10485760)) {
			AJS.flag({
				type : "error",
				title : appMessgaes.error,
				body : "Image size should be less than 10mb",
				close : 'auto'
			});
		} else {
			if (this.files && this.files[0]) {
				uploadStudentImage(this.files[0], "");
			}
		}
		$(this).val("");
	});
	$("#editStudent-image1").on('change', 'input', function() {
		var filename=this.files[0].name;
		var sfilename=filename.split(".");
		if(['jpeg','jpg','png'].indexOf(sfilename[sfilename.length-1]) < 0){
				AJS.flag({
					type: "error",
					title: "Error..!",
					body: "File not Supported",
					close: 'auto'
				});
		} else if (this.files[0] && (this.files[0].size > 10485760)) {
			AJS.flag({
				type : "error",
				title : "Error..!",
				body : "Image size should be less than 10mb",
				close : 'auto'
			});
		} else {
			if (this.files && this.files[0]) {
				uploadStudentImage(this.files[0], studentId);
			}
		}
		$(this).val("");
	});
	$(".multiselect-container").unbind("mouseleave");
	$(".multiselect-container").on("mouseleave", function() {
		$(this).click();
	});

});
