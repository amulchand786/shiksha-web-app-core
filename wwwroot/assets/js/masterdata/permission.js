var thLabels;

//model popup for on click Add permission
var initInput =function(){	
	$("#add-permission").modal().show();
	$("#addPermissionName").val('');
	$("#addURLName").val('');
	
	$("#add-permission #addPermissionDataForm").find("#alertDiv #errorMessage").hide();
	$("#add-permission #addPermissionDataForm").data('formValidation').resetForm();
}

//create a row after save
var getPermissionRow =function(permission){
	
	var row = 
		"<tr id='"+permission.permissionId+"' data-id='"+permission.permissionId+"'>"+
		"<td></td>"+
		"<td data-permisionId='"+permission.permissionId+"'	title='"+permission.permissionName+"'>"+permission.permissionName+"</td>"+
		"<td data-statecode='"+permission.permissionCode+"' title='"+permission.permissionCode+"'>"+permission.permissionCode+"</td>"+		
		"<td><div class='btn-group'><button type='button' class='btn btn-link'	style='display: block;' id='btnEditState'" +
		"onclick=editPermissionData(&quot;"+permission.permissionId+"&quot;,&quot;"+escapeHtmlCharacters(permission.permissionName)+"&quot;,&quot;"+permission.url+"&quot;,&quot;"+permission.hierarchy+"&quot;);><span class='glyphicon glyphicon-edit font-size12'></span>Edit</button><button type='button' class='btn btn-link'	style='display: block;' data-toggle='modal'		data-target='#delete-permission' id=deleteState"+permission.permissionId+"'	onclick=deletePermissionData(&quot;"+permission.permissionId+"&quot;,&quot;"+permission.permissionName+"&quot;);><span class='glyphicon glyphicon-remove font-size12'></span>Delete</button></div></td>"+
		"</tr>";
	
	return row;
}
var addPermissionData=function(event){
	
    var permissionName = $('#addPermissionName').val();
    var url = $('#addURLName').val();
    var hierarchy = $('#addHierarchyName').val();  
  
    var json ={ 
    	 
    	  "permissionName" : permissionName,
    	  "url" : url,
    	  "hierarchy" : hierarchy
    	     };     
   
    var btnAddMorePermissionId =$(event.target).data('formValidation').getSubmitButton().data('id');
    
    
    var permission=customAjaxCalling("permission",json, "POST");   
    
    if(permission.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		
		//start
		
		var newRow =getPermissionRow(permission);
		appendNewRow("permissionTable",newRow);
				
	if(btnAddMorePermissionId=="addPermissionSaveMoreButton"){
			
		showDiv($('#add-permission #alertdiv'));
		$("#add-permission").find('#errorMessage').hide();
		$("#add-permission").find('#successMessage').show();
		$("#add-permission").find('#success').text(permission.responseMessage);
		$("#add-permission").find('#successMessage').fadeOut(2000);
		$('#notify').fadeIn(1000);
		initInput();
		}else{			
			$('#add-permission').modal("hide");
			$('#success-msg').show();
			$('#showMessage').text(permission.responseMessage);
			$("#success-msg").fadeOut(3000);
			$('#notify').fadeIn(3000);
		}	
	}else {
		$(".outer-loader").hide();
		showDiv($('#mdlAddState #alertdiv'));
		$("#add-permission").find('#successMessage').hide();
		$("#add-permission").find('#okButton').hide();
		$("#add-permission").find('#errorMessage').show();
		$("#add-permission").find('#exception').show();
		$("#add-permission").find('#buttonGroup').show();
		$("#add-permission").find('#exception').text(permission.responseMessage);
	}

		
		//end
		
		

	/*	
		displaySuccessMsg();	
		
		$('#add-permission').modal("hide");
		$('#success-msg').show();
		$('#showMessage').text(permission.responseMessage);
		$("#success-msg").fadeOut(4000);
		 location.reload();
		 
		 
		
		
 	} else {
 		
		 $(".outer-loader").hide();
		
 		$("#add-permission").find('#successMessage').hide();
 		$("#add-permission").find('#okButton').hide();
 		$("#add-permission").find('#errorMessage').show();
 		$("#add-permission").find('#exception').show();
 		$("#add-permission").find('#buttonGroup').show();
 		$("#add-permission").find('#exception').text(permission);
 		

 	}*/
 	
 };
 
 
 
 
var editPermissionId=null;
var editPermissionUrl=null;
var editPermissionHierarchy=null;
var editPermissionName=null;


var deletePermissionName=null;
var deletePermissionId=null;



var editPermissionData=function(permissionId,permissionName,permissionUrl,permissionHierarchy){	
	
	$("#edit-permission").modal().show();
	  $("#edit-permission #editPermissionDataForm").data('formValidation').resetForm();

	editPermissionId=permissionId;
	editPermissionUrl=permissionUrl;
	editPermissionName=permissionName;
	editPermissionHierarchy=permissionHierarchy;
	
	$('#editPermissionName').val(permissionName);	
	$('#editURLName').val(permissionUrl);
	$('#editHierarchyName').val(permissionHierarchy);
	
	
	};
var editData=function(){

    var permissionName = $('#editPermissionName').val(); 
    var editpermissionsURL=$('#editURLName').val();
	var editpremissionhierar=$('#editHierarchyName').val();  
	var json ={ 
    	  "permissionId" : editPermissionId,
    	  "permissionName" : permissionName,
    	  "url" : editpermissionsURL,
    	  "hierarchy" : editpremissionhierar
    };
	var permission= customAjaxCalling("permission", json, "PUT");
	if(permission.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		
		//start
		deleteRow("permissionTable",permission.permissionId);
		
		var newRow =getPermissionRow(permission);
		appendNewRow("permissionTable",newRow);
		displaySuccessMsg();	
				
		//end
			
			$('#edit-permission').modal("hide");
			$('#success-msg').show();
			$('#showMessage').text(permission.responseMessage);
			$("#success-msg").fadeOut(5000);
			$('#notify').fadeIn(3000);
			
    		
    		} else {
    		
       		$(".outer-loader").hide();
    		$("#edit-permission").find('#successMessage').hide();
    		$("#edit-permission").find('#okButton').hide();
    		$("#edit-permission").find('#errorMessage').show();
    		$("#edit-permission").find('#exception').show();
    		$("#edit-permission").find('#buttonGroup').show();
    		$("#edit-permission").find('#exception').text(permission.responseMessage);
    		}

 };

var deletePermissionData=function(permissionId,permissionName){
	deletePermissionId=permissionId;
	deletePermissionName=permissionName;
	$('#delete-permission #deletePermissionMessage').text(columnMessages.deleteConfirm.replace("@NAME",permissionName));
};
$(document).on('click','#deletePermissionButton', function(){
	
    var json ={ 
    	  "permissionId" : deletePermissionId,
    	  "permissionName" : deletePermissionName    	  
    	  };
   
    var permission=customAjaxCalling("permission", json, "DELETE");
    
    if(permission.response=="shiksha-200"){
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("permissionTable",deletePermissionId);
		
		$('#delete-permission').modal("hide");
		$('#success-msg').show();
		$('#showMessage').text(permission.responseMessage);
		$("#success-msg").fadeOut(3000);
		$('#notify').fadeIn(3000);
	
    
    }
    
   else {
			 $(".outer-loader").hide();
			 $("#delete-permission").find('#okButton').hide();
			 $("#delete-permission").find('#errorMessage').show();
			 $("#delete-permission").find('#exception').text(message);
			 $("#delete-permission").find('#buttonGroup').show();
	 	}
 });



var fnSetDTColFilter=function (tId, nCols, exCols, hNames){
	var exportCols = [];
	for (var n = 1; n <=(nCols - 1); n++) {
		exportCols.push(n);
	}
	$(tId).on('order.dt', enableTooltip)
		.on('search.dt', enableTooltip)
		.on('page.dt', enableTooltip)
		.DataTable({
			"pagingType": "numbers",
			"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l>B<"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
			"lengthMenu": [5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
			"bLengthChange": true,
			"bDestroy": true,
			"bFilter": true,
			"autoWidth": false,
			"iDisplayLength": 100,
			"stateSave": false,
			"fnDrawCallback": function (oSettings) {
				/* Need to redo the counters if filtered or sorted */
				if (oSettings.bSorted || oSettings.bFiltered) {
					for (var i = 0, iLen = oSettings.aiDisplay.length; i < iLen; i++) {
						$('td:eq(0)', oSettings.aoData[oSettings.aiDisplay[i]].nTr).html(i + 1);
					}
				}
			},
			"aoColumnDefs": [{
				'bSortable': false,
				'aTargets': ['nosort']
			}],
			"aaSorting": [],

			bSortCellsTop: true,
			buttons: [
				{
					extend: 'copyHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'excelHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'csvHtml5',
					exportOptions: {
						columns: exportCols
					}
				},
				{
					extend: 'pdfHtml5',
					orientation: 'landscape',
					pageSize: 'A3',
					exportOptions: {
						columns: exportCols,
					}
				},
				{
					extend: 'print',
					orientation: 'landscape',
					exportOptions: {
						columns: exportCols,
					}
				},
			]
		});
	fnApplyColumnFilter(tId, nCols, exCols, hNames);


	$(".multiselect-container").unbind("mouseleave");
	$(".multiselect-container").on("mouseleave", function () {
		$(this).click();
	});
};




$(function() {
	thLabels =['#',columnMessages.hierarchy ,columnMessages.permission];
	fnSetDTColFilter('#permissionTable',3,[0],thLabels);
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnShowHideColumns('#permissionTable',[]);
	});
	$('#tableDiv').show();
	
    $("#addPermissionDataForm") .formValidation({
    	feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			permissionName : {
				validators : {
					callback : {
						message : columnMessages.specialSymbol,
						callback : function(value) {
							var regx=/^[a-zA-Z][a-zA-Z0-9]*$/;
							return regx.test(value);}
					}
				}
			}
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();
             addPermissionData(e);
      
        });
    $("#editPermissionDataForm") .formValidation({
    	feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			permissionName : {
				validators : {
					callback : {
						message : columnMessages.specialSymbol,
						callback : function(value) {
							var regx=/^[a-zA-Z][a-zA-Z0-9]*$/;
							return regx.test(value);}
					}
				}
			}
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();
             editData();
      
        });
});