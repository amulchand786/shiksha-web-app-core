var pageContextElements = {	
		addButton 						: '#addNewNugget',
		table 							: '#nuggetTable',
		toolbar							: "#toolbarNugget"
};
var addModal ={
		form 			: '#addNuggetForm',
		modal   		: '#mdlAddNugget',
		saveMoreButton	: 'addNuggetSaveMoreButton',
		saveButton		: 'addNuggetSaveButton',
		eleName		   	: '#addNuggetName',
		eleDescription	: '#addDescription',
		eleSequence		: '#addNuggetSequence',
		eleSource		: '#addSource',
		eleGrade		: '#addGrade',
		eleSubject		: '#addSubject',
		eleSubjectDiv	: '#subjectDiv',
		eleUnit			: '#addUnit',
		eleUnitDiv		: '#unitDiv',
		eleNuggetDiv	: '#nuggetDiv',
};
var editModal ={
			form	 : '#editNuggetForm',
			modal	 : '#mdlEditNugget',
			eleName	 : '#editNuggetName',
			eleId	 : '#nuggetId',
			eleDescription	: '#editDescription',
			eleSequence		: '#editNuggetSequence',
			eleSource		: '#editSource',
			eleGrade		: '#editGrade',
			eleSubject		: '#editSubject',
			eleSubjectDiv	: '#subjectDiv',
			eleUnit			: '#editUnit',
			eleUnitDiv		: '#unitDiv',
			eleNuggetDiv	: '#nuggetDiv',
};
var deleteModal ={
			modal	: '#mdlDeleteNugget',
			confirm : '#deleteNuggetButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var nuggetIdForDelete;

var nuggetFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var nugget={
		name:'',
		nuggetId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',nugget.initAddModal);
			$(deleteModal.confirm).on('click',nugget.deleteFunction);
			$(addModal.eleGrade).on('change',nugget.showHideSubjects);
			$(addModal.eleSubject).on('change',nugget.showHideUnits);
			$(addModal.eleUnit).on('change',nugget.showHideNugget);
			$(editModal.eleGrade).on('change',nugget.showHideSubjectsInEdit);
			$(editModal.eleSubject).on('change',nugget.showHideUnitsInEdit);
		},
		deleteFunction : function(){
			nugget.fnDelete(nuggetIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			nugget.resetForm(addModal.form);
			shikshaPlus.fnGetShikshaSources(addModal.eleSource,0);
			setOptionsForMultipleSelect(addModal.eleSource);
			$(addModal.eleSource).multiselect("refresh");
			shikshaPlus.fnGetGrades(addModal.eleGrade,0);
			setOptionsForMultipleSelect(addModal.eleGrade);
			$(addModal.eleGrade).multiselect("refresh");
			$(addModal.eleSubjectDiv).hide();
			$(addModal.eleUnitDiv).hide();
			$(addModal.eleNuggetDiv).hide();
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		showHideSubjects: function(){
			shikshaPlus.fnGetSubjectsByGradeId(addModal.eleSubject,0,$(addModal.eleGrade).val());
			setOptionsForMultipleSelect(addModal.eleSubject);
			$(addModal.eleSubject).multiselect("rebuild");
			$(addModal.eleSubjectDiv).show();
		},
		showHideUnits: function(){
			shikshaPlus.fnGetUnits(addModal.eleUnit,0,$(addModal.eleGrade).val(),$(addModal.eleSubject).val());
			setOptionsForMultipleSelect(addModal.eleUnit);
			$(addModal.eleUnit).multiselect("rebuild");
			$(addModal.eleUnitDiv).show();
		},
		showHideNugget: function(){
			$(addModal.eleNuggetDiv).show();
		},
		showHideSubjectsInEdit: function(){
			shikshaPlus.fnGetSubjectsByGradeId(editModal.eleSubject,0,$(editModal.eleGrade).val());
			setOptionsForMultipleSelect(editModal.eleSubject);
			$(editModal.eleSubject).multiselect("rebuild");
			shikshaPlus.fnGetUnits(editModal.eleUnit,0,0,0);
			setOptionsForMultipleSelect(editModal.eleUnit);
			$(editModal.eleUnit).multiselect("rebuild");
		},
		showHideUnitsInEdit: function(){
			shikshaPlus.fnGetUnits(editModal.eleUnit,0,$(editModal.eleGrade).val(),$(editModal.eleSubject).val());
			setOptionsForMultipleSelect(editModal.eleUnit);
			$(editModal.eleUnit).multiselect("rebuild");
			
		},
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name"		  : $(addModal.eleName).val(),
					"description" : $(addModal.eleDescription).val(),
					"sequence"	  : $(addModal.eleSequence).val(),
					"source" 	  : { "sourceId": $(addModal.eleSource).val() },
					"unit" 		  : {"unitId": $(addModal.eleUnit).val()},
					"grade"    	  : {"gradeId": $(addModal.eleGrade).val()},
					"subject" 	  : {"subjectId": $(addModal.eleSubject).val()},
			}
			var addAjaxCall = shiksha.invokeAjax("shiksha/nugget", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						
						$(addModal.eleSubjectDiv).hide();
						$(addModal.eleUnitDiv).hide();
						$(addModal.eleNuggetDiv).hide();
						$(addModal.modal).modal("show");
					}
					nugget.resetForm(addModal.form);
					$(addModal.eleSource).multiselect("rebuild");
					$(addModal.eleGrade).multiselect("rebuild");
					nugget.refreshTable(pageContextElements.table);
					$(addModal.modal).find("#divSaveAndAddNewMessage").show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(nuggetId){
			nugget.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shiksha/nugget/"+nuggetId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.name);
					$(editModal.eleId).val(editAjaxCall.nuggetId);
					$(editModal.eleDescription).val(editAjaxCall.description);
					$(editModal.eleSequence).val(editAjaxCall.sequence);
					shikshaPlus.fnGetShikshaSources(editModal.eleSource,editAjaxCall.source.sourceId);
					setOptionsForMultipleSelect(editModal.eleSource);
					shikshaPlus.fnGetGrades(editModal.eleGrade,editAjaxCall.grade.gradeId);
					setOptionsForMultipleSelect(editModal.eleGrade);
					shikshaPlus.fnGetSubjectsByGradeId(editModal.eleSubject,editAjaxCall.subject.subjectId,$(editModal.eleGrade).val());
					setOptionsForMultipleSelect(editModal.eleSubject);
					shikshaPlus.fnGetUnits(editModal.eleUnit,editAjaxCall.unit.unitId,$(editModal.eleGrade).val(),$(editModal.eleSubject).val());
					setOptionsForMultipleSelect(editModal.eleUnit);
					$(editModal.eleSource).multiselect("rebuild");
					$(editModal.eleGrade).multiselect("rebuild");
					$(editModal.eleSubject).multiselect("rebuild");
					$(editModal.eleUnit).multiselect("rebuild");
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"		:$(editModal.eleName).val(),
					"nuggetId"	:$(editModal.eleId).val(),
					"sequence"	  : $(editModal.eleSequence).val(),
					"description" : $(editModal.eleDescription).val(),
					"source" 	: { "sourceId": $(editModal.eleSource).val() },
					"unit" 		: {"unitId": $(editModal.eleUnit).val()},
					"grade" 	: {"gradeId": $(editModal.eleGrade).val()},
					"subject" 	: {"subjectId": $(editModal.eleSubject).val()},
			}

			var updateAjaxCall = shiksha.invokeAjax("shiksha/nugget", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					nugget.resetForm(editModal.form);
					nugget.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: appMessgaes.success,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(nuggetId,name){
			var msg = $("#deleteNuggetMessage").data("message");
			$("#deleteNuggetMessage").html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			nuggetIdForDelete=nuggetId;
		},
		fnDelete :function(nuggetId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shiksha/nugget/"+nuggetId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					nugget.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					source:{
						required:true,
					},
					grade:{
						required:true,
					},
					subject:{
						required:true,
					},
					unit:{
						required:true,
					},
					nuggetName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					nuggetSequence:{
						required: true,
						noSpace:true,
					},
					nuggetDescription: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}
				},

				messages: {
					source:{
						required:nuggetMessages.source,
					},
					grade:{
						required:nuggetMessages.grade,
					},
					subject:{
						required:nuggetMessages.subject,
					},
					unit:{
						required:nuggetMessages.unit,
					},
					nuggetName: {
						required: nuggetMessages.nameRequired,
						noSpace: nuggetMessages.nameNoSpace,
						minlength: nuggetMessages.nameMinLength,
						maxlength: nuggetMessages.nameMaxLength,

					},
					nuggetSequence:{
						required: nuggetMessages.sequenceRequiered,
						noSpace:nuggetMessages.sequenceRequiered,
					},
					nuggetDescription: {
						required: nuggetMessages.descriptionRequired,
						minlength: nuggetMessages.descriptionMinLength,
						maxlength: nuggetMessages.descriptionMaxLength,
						noSpace: nuggetMessages.descriptionNoSpace,
					}
				},
				errorPlacement: function(error, element) {
					$(element).parent().append(error);
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						nugget.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						nugget.fnUpdate();
					}
				}
			});
		},
		nuggetActionFormater: function(value, row) {
			
			return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
			+'<li><a onclick="nugget.initEdit(\''+row.nuggetId+'\')" href="javascript:void(0)">Edit</a></li>'
			+'<li><a onclick="nugget.initDelete(\''+row.nuggetId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">Delete</a></li>'
			+'</ul>';
		},
		nuggetNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	nugget.formValidate(addModal.form);
	nugget.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	nugget.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar ,
		url : "shiksha/nugget/page",
		method : "post",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		formatShowingRows : nuggetFilterObj.formatShowingRows,
		clickToSelect: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});