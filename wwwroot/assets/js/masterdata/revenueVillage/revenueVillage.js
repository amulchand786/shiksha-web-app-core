//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForRV =[];
var allInputEleNamesOfFilter =[]; 
var btnAddMoreRVId;
var deleteRVId;
var selectedIds;
var elementIdMap={};
var editrevenueVillageId=0;
var thLabels;
var deleteRevenueVillageId=0;
var deleteRevenueVillageName='';
//initialize global variables
var initGloabalVarsRV =function(){
	resetButtonObj='';
	allInputEleNamesForRV=['addRevenueVillageName'];
	allInputEleNamesOfFilter=['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId','panchayatId'];
	btnAddMoreRVId ='';
	deleteRVId='';
}


//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}

//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddRevenueVillage #statesForZone',
			"Zone" : '#mdlAddRevenueVillage #selectBox_zonesByState',
			"District" : '#mdlAddRevenueVillage #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddRevenueVillage #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddRevenueVillage #selectBox_blocksByTehsil',
			"Nyaya Panchayat" : '#mdlAddRevenueVillage #selectBox_npByBlock',
			"Gram Panchayat" : '#mdlAddRevenueVillage #selectBox_gpByNp',
		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
			"State" : '#mdlEditRevenueVillage #statesForZone',
			"Zone" : '#mdlEditRevenueVillage #selectBox_zonesByState',
			"District" : '#mdlEditRevenueVillage #selectBox_districtsByZone',
			"Tehsil" : '#mdlEditRevenueVillage #selectBox_tehsilsByDistrict',
			"Block" : '#mdlEditRevenueVillage #selectBox_blocksByTehsil',
			"Nyaya Panchayat" : '#mdlEditRevenueVillage #selectBox_npByBlock',
			"Gram Panchayat" : '#mdlEditRevenueVillage #selectBox_gpByNp',
		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};


var getRVRow =function(revenueVillage){
	var row = 
		"<tr id='"+revenueVillage.revenueVillageId+"' data-id='"+revenueVillage.revenueVillageId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+revenueVillage.stateId+"'	title='"+revenueVillage.stateName+"'>"+revenueVillage.stateName+"</td>"+
		"<td data-zoneid='"+revenueVillage.zoneId+"'	title='"+revenueVillage.zoneName+"'>"+revenueVillage.zoneName+"</td>"+
		"<td data-districtid='"+revenueVillage.districtId+"' title='"+revenueVillage.districtName+"' >"+revenueVillage.districtName+"</td>"+
		"<td data-tehsilid='"+revenueVillage.tehsilId+"' title='"+revenueVillage.tehsilName+"' >"+revenueVillage.tehsilName+"</td>"+
		"<td data-blockid='"+revenueVillage.blockId+"' title='"+revenueVillage.blockName+"' >"+revenueVillage.blockName+"</td>"+
		"<td data-nyaypanchayatid='"+revenueVillage.nyayPanchayatId+"' title='"+revenueVillage.nyayPanchayatName+"' >"+revenueVillage.nyayPanchayatName+"</td>"+
		"<td data-grampanchayatid='"+revenueVillage.gramPanchayatId+"' title='"+revenueVillage.gramPanchayatName+"' >"+revenueVillage.gramPanchayatName+"</td>"+
		"<td data-revenuevillageidid='"+revenueVillage.revenueVillageId+"' title='"+revenueVillage.revenueVillageName+"' >"+revenueVillage.revenueVillageName+"</td>"+
		"<td data-revenuevillagecode='"+revenueVillage.revenueVillageCode+"' title='"+revenueVillage.revenueVillageCode+"'>"+revenueVillage.revenueVillageCode+"</td>"+		

		"<td><div class='div-tooltip'><a class='tooltip tooltip-top'	  id='btnEditRevenueVillage'" +
		"onclick=editRevenueVillageData(&quot;"+revenueVillage.stateId+"&quot;,&quot;"+revenueVillage.zoneId+"&quot;,&quot;"+revenueVillage.districtId+"&quot;,&quot;"+revenueVillage.tehsilId+"&quot;,&quot;"+revenueVillage.blockId+"&quot;,&quot;"+revenueVillage.nyayPanchayatId+"&quot;,&quot;"+revenueVillage.gramPanchayatId+"&quot;,&quot;"+revenueVillage.revenueVillageId+"&quot;,&quot;"+escapeHtmlCharacters(revenueVillage.revenueVillageName)+"&quot;); ><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top'	 id=deleteRevenueVillage	onclick=deleteRevenueVillageData(&quot;"+revenueVillage.revenueVillageId+"&quot;,&quot;"+escapeHtmlCharacters(revenueVillage.revenueVillageName)+"&quot;);><span class='glyphicon glyphicon-trash font-size12' ><span class='tooltiptext'>"+appMessgaes.delet+"</span></span></a></div></td>"+
		"</tr>";
	
	return row;
}


var applyPermissions=function(){
	if (!editRvPermission){
	$("#btnEditRevenueVillage").remove();
		}
	if (!deleteRvPermission){
	$("#deleteRevenueVillage").remove();
		}
	}

// smart filter utility--- start here

var initSmartFilter = function(type) {
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){
		resetFormValidatonForLocFilters("addRevenueVillageForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addRevenueVillageForm",allInputEleNamesForRV);
		displaySmartFilters("add")
		fnCollapseMultiselect();
	}else{		
		
	resetFvForAllInputExceptLoc("addRevenueVillageForm",['editRevenueVillageName']);
	displaySmartFilters("edit")
	fnCollapseMultiselect();
	}
}



//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	if($(resetButtonObj).parents("form").attr("id")=="addRevenueVillageForm"){
		resetFormValidatonForLocFilters("addRevenueVillageForm",allInputEleNamesOfFilter);
		
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");
	}else{
		resetFormValidatonForLoc("editRevenueVillageForm","panchayatId");
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
	}
	fnCollapseMultiselect();
	
	
}



// reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addRevenueVillageForm');
	resetFormById('editRevenueVillageForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}


// smart filter utility--- end here

// perform CRUD

// add RV
var addRevenueVillage = function(event) {
	$('.outer-loader').show();
	var gramPanchayatId = $('#mdlAddRevenueVillage #selectBox_gpByNp').val();
	var villageName = $('#mdlAddRevenueVillage #addRevenueVillageName').val().trim().replace(/\u00a0/g," ");
	var json = {
		"gramPanchayatId" : gramPanchayatId,
		"revenueVillageName" : villageName,
	};
	
	
	//update data table without reloading the whole table
	//save&add new
	btnAddMoreRVId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var revenueVillage = customAjaxCalling("revenueVillage", json, "POST");
	if(revenueVillage!=null){
	if(revenueVillage.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		var newRow =getRVRow(revenueVillage);
		appendNewRow("tblRevenueVillage",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#tblRevenueVillage',[2,4,5,6,7],11,[0,10],thLabels);
		setShowAllTagColor(colorClass);
		if(btnAddMoreRVId=="addRevenueVillageSaveMoreButton"){
			$("#mdlAddRevenueVillage").find('#errorMessage').hide();
			
			resetFvForAllInputExceptLoc("addRevenueVillageForm",allInputEleNamesForRV);
			fnDisplaySaveAndAddNewElement('#mdlAddRevenueVillage',villageName);		
		}else{			
			$('#mdlAddRevenueVillage').modal("hide");
			
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: appMessgaes.success,
				    body: '<p>'+ revenueVillage.responseMessage+'</p>',
				    close :'auto'
				 })
				}, 1000);
			
		}		
	}else {
		$(".outer-loader").hide();
		showDiv($('#mdlAddRevenueVillage #alertdiv'));
		$("#mdlAddRevenueVillage").find('#successMessage').hide();
		$("#mdlAddRevenueVillage").find('#okButton').hide();
		$("#mdlAddRevenueVillage").find('#errorMessage').show();
		$("#mdlAddRevenueVillage").find('#exception').show();
		$("#mdlAddRevenueVillage").find('#buttonGroup').show();
		$("#mdlAddRevenueVillage").find('#exception').text(revenueVillage.responseMessage);
	}
	}
	$(".outer-loader").hide();
	};


// edit
// get data for edit
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editTehsilId = 0;
var editBlockId = 0;
var editNyayPanchayatId = 0;
var editGramPanchayatId = 0;
var editRevenueVillageId = 0;
var editRevenueVillageData = function() {
	var stateId = arguments[0];
	var zoneId = arguments[1];
	var districtId = arguments[2];	
	var tehsilId =arguments[3];
	var blockId=arguments[4];
	var nyayPanchayatId=arguments[5];
	var gramPanchayatId=arguments[6];
	var revenueVillageId=arguments[7];
	var revenueVillageName=arguments[8];	
	
	initSmartFilter("edit");
	var idMap = {
		'#mdlEditRevenueVillage #statesForZone' : stateId,
		'#mdlEditRevenueVillage #selectBox_zonesByState' : zoneId,
		'#mdlEditRevenueVillage #selectBox_districtsByZone' : districtId,
		'#mdlEditRevenueVillage #selectBox_tehsilsByDistrict' : tehsilId,
		'#mdlEditRevenueVillage #selectBox_blocksByTehsil' : blockId,
		'#mdlEditRevenueVillage #selectBox_npByBlock' : nyayPanchayatId,
		'#mdlEditRevenueVillage #selectBox_gpByNp' : gramPanchayatId,

	};
	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
							.prop('selected', true);
				enableMultiSelect(ele_key);
			});

	editrevenueVillageId = revenueVillageId;

	$('#mdlEditRevenueVillage #editRevenueVillageName').val(revenueVillageName);
	$("#editRevenueVillageSaveButton").prop("disabled",false);
	$("#editRevenueVillageSaveButton").removeClass("disabled");
	$("#mdlEditRevenueVillage").modal();
	fnCollapseMultiselect();

};

// edit RV

var editVillage = function() {
	$('.outer-loader').show();
	var gramPanchayatId = $('.editRevenueVillage #selectBox_gpByNp').val();
	var revenueVillageName = $('#editRevenueVillageName').val().trim().replace(/\u00a0/g," ");
	var revenueVillageId = editrevenueVillageId;
	var json = {
			"gramPanchayatId" : gramPanchayatId,
			"revenueVillageName" : revenueVillageName,
			"revenueVillageId" : revenueVillageId
	};


	var revenueVillage = customAjaxCalling("revenueVillage", json, "PUT");
	if(revenueVillage!=null){
	if(revenueVillage.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("tblRevenueVillage",revenueVillage.revenueVillageId);
		var newRow =getRVRow(revenueVillage);
		appendNewRow("tblRevenueVillage",newRow);
		applyPermissions();		
		fnUpdateDTColFilter('#tblRevenueVillage',[2,4,5,6,7],11,[0,10],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	

		$('#mdlEditRevenueVillage').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ revenueVillage.responseMessage+'</p>',
			    close :'auto'
			 })
			}, 1000);
		
	}		
	else {
		$(".outer-loader").hide();
		showDiv($('#mdlEditRevenueVillage #alertdiv'));
		$("#mdlEditRevenueVillage").find('#successMessage').hide();
		$("#mdlEditRevenueVillage").find('#errorMessage').show();
		$("#mdlEditRevenueVillage").find('#exception').show();
		$("#mdlEditRevenueVillage").find('#buttonGroup').show();
		$("#mdlEditRevenueVillage").find('#exception').text(revenueVillage.responseMessage);
	}
	}
	$(".outer-loader").hide();
};
	


// get data for delete
var deleteRevenueVillageData = function(RevenueVillageId, RevenueVillageName) {
	deleteRevenueVillageId = RevenueVillageId;
	deleteRevenueVillageName = RevenueVillageName;
	$('#deleteRevenueVillageMessage').text(columnMessages.deleteConfirm.replace("@NAME",RevenueVillageName) + " ?");
	$("#mdlDelRevenueVillage").modal().show();
	hideDiv($('#mdlDelRevenueVillage #alertdiv'));
};

// delete Rv

$('#deleteRevenueVillageButton').click(function() {
	$('.outer-loader').show();


	var revenueVillage = customAjaxCalling("revenueVillage/"+deleteRevenueVillageId, null, "DELETE");
	if(revenueVillage!=null){
	if(revenueVillage.response=="shiksha-200"){
		$(".outer-loader").hide();						

		deleteRow("tblRevenueVillage",deleteRevenueVillageId);		
		fnUpdateDTColFilter('#tblRevenueVillage',[2,4,5,6,7],11,[0,10],thLabels);
		setShowAllTagColor(colorClass);
		$('#mdlDelRevenueVillage').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ revenueVillage.responseMessage+'</p>',
			    close :'auto'
			 })
			}, 1000);
		
	} else {
		showDiv($('#mdlDelRevenueVillage #alertdiv'));
		$("#mdlDelRevenueVillage").find('#errorMessage').show();
		$("#mdlDelRevenueVillage").find('#exception').text(revenueVillage.responseMessage);
		$("#mdlDelRevenueVillage").find('#buttonGroup').show();
	}
	}
	$(".outer-loader").hide();
});




// form validation

$(function() {
		
	thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.district,columnMessages.tehsil,columnMessages.block, columnMessages.nyayPanchayat,columnMessages.gramPanchayat,columnMessages.revenueVillage,columnMessages.revenueVillageCode,columnMessages.action];
	fnSetDTColFilterPagination('#tblRevenueVillage',11,[0,10],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tblRevenueVillage',[2,4,5,6,7]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$( "#addRevenueVillageName" ).keypress(function() {		
		hideDiv($('#mdlAddRevenueVillage #alertdiv'),$('#mdlEditRevenueVillage #alertdiv'));
	})
		$( "#editRevenueVillageForm" ).keypress(function() {		
		hideDiv($('#mdlEditRevenueVillage #alertdiv'));
	})
	initGloabalVarsRV();
	$("#addRevenueVillageForm")
			.formValidation(
					{	excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							addRevenueVillageName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.revenueVillage+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addRevenueVillage(e);

			});
	$("#editRevenueVillageForm")
			.formValidation(
					{	excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							editRevenueVillageName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.revenueVillage+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				editVillage();

			});
});
