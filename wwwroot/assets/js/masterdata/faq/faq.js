var pageContextElements = {	
		addButton 	: '#addNewFAQ',
		table 		: '#faqTable',
		toolbar		: "#toolbarFaq"
};
var addModal ={
		form 			: '#addFaqForm',
		modal   		: '#mdlAddFaq',
		saveMoreButton	: 'addFaqSaveMoreButton',
		saveButton		: 'addFaqSaveButton',
		eleQuestion		: '#addQuestion',
		eleAnswer		: '#addAnswer',
		message		    : "#divSaveAndAddNewMessage"
};
var editModal ={
			form	 : '#editFaqForm',
			modal	 : '#mdlEditFaq',
			eleId	 : '#faqId',
			eleQuestion		: '#editQuestion',
			eleAnswer		: '#editAnswer'
			
};
var deleteModal ={
			modal	: '#mdlDeleteFaq',
			confirm : '#deleteFaqButton',
			message	: "#deleteFaqMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var faqIdForDelete;
var faqFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var faq={
		name:'',
		faqId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',faq.initAddModal);
			$(deleteModal.confirm).on('click',faq.deleteFunction);

		},
		deleteFunction : function(){
			faq.fnDelete(faqIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			faq.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"question" :$(addModal.eleQuestion).val(),
					"answer"   : $(addModal.eleAnswer).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("faq", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : messages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					faq.resetForm(addModal.form);
					faq.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.question);	
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(faqId){
			faq.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("faq/"+faqId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleQuestion).val(editAjaxCall.question);
					$(editModal.eleAnswer).val(editAjaxCall.answer);
					$(editModal.eleId).val(editAjaxCall.faqid);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"faqid"		:$(editModal.eleId).val(),
					"question" :$(editModal.eleQuestion).val(),
					"answer"   : $(editModal.eleAnswer).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("faq", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					faq.resetForm(editModal.form);
					faq.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title : messages.sucessAlert,
						body  : updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(faqId,question){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',question));
			$(deleteModal.modal).modal("show");
			faqIdForDelete=faqId;
		},
		fnDelete :function(faqId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("faq/"+faqId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					faq.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : messages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					question: {
						required: true,
						noSpace:true,
						minlength: 1,
						maxlength: 500,
						
					},
					answer: {
						required: true,
						noSpace:true,
						minlength: 1,
						maxlength: 1500,
						
					},
						
				},

				messages: {
					question: {
						required: messages.questionRequired,
						noSpace:messages.noSpace,
						minlength: messages.minLength,
						maxlength: messages.maxLength,
					},
					answer: {
						required: messages.answerRequired,
						noSpace:messages.answerNoSpace,
						minlength:messages.answerMinLength,
						maxlength: messages.answerMaxLength,
						
					},
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						faq.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						faq.fnUpdate();
					}
				}
			});
		},
		faqActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="faq.initEdit(\''+ row.faqid+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="faq.initDelete(\''+ row.faqid+ '\',\''+ row.question+ '\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		faqNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	faq.formValidate(addModal.form);
	faq.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	faq.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar,
		url : "faq/list",
		method : "post",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns:false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: false,
		queryParamsType :'limit',
		//formatShowingRows : faqFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});