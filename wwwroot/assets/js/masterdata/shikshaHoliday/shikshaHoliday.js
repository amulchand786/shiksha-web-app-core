//TODO-remove unused variables
//TODO-remove commented codes
var start = new Date(); 
var end = new Date();
var holidayId=0;
var pageContextElements = {
		table		:'#holidayTable',
		addBtn		:'#addHoliday',
		eleYear		:'#selectYear'
};
var addModal = {
		modal				:'#mdlAddHoliday',
		form				:'#addHolidayForm',
		eleEndDateCheckBox	:'#endDateCheckBox',
		eleEndDate			:'#addEndDate',
		eleStartDate		:'#addStartDate',
		eleName				:'#addHolidayName',
		eleYear				:'#mdlAddSelectYear',
		eleSaveBtn			:'addHolidaySaveButton',
		eleSaveMoreBtn		:'addHolidaySaveMoreButton',
		eleEnableEndDate	:'#enableEndDate',
		eleMessage			: "#divSaveAndAddNewMessage"
};
var editModal = {
		modal				:'#mdlEditHoliday',
		form				:'#editHolidayForm',
		eleHolidayId		:'#editHolidayId',
		eleEndDateCheckBox	:'#editEndDateCheckBox',
		eleEndDate			:'#editEndDate',
		eleStartDate		:'#editStartDate',
		eleName				:'#editHolidayName',
		eleYear				:'#mdlEditYear',
		eleEndDateDiv		:'#editEndDateDiv'
};
var deleteModal = {
		modal		:'#mdlDelHoliday',
		confirm		: '#btnDelHoliday',
		message		:'#deleteHolidayMessage'
};
var optionSelected="option:selected";
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');
var validator;
var $table;


var holidayFilter={
		toolBar		: "#toolbarHoliday",
		searchBox	: "#filter-search",
		searchBtn	: "#filter-search-btn",
		init : function(){
			$(this.searchBtn).on("click",this.filterData);
			$(this.searchBox).on("keyup",this.searchKeyUp);
			$(this.toolBar).show();
		},
		filterQueryParams : function(params){
			params['search'] = $(holidayFilter.searchBox).val();
			return params;
		},
		filterData : function(){
			$table.bootstrapTable("refresh");
		},
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(value, row, index, field){
			return {
				classes:"dropdown dropdown-td"
			}
		},
		searchKeyUp : function(e){
			if (e.keyCode == 13) {
		        e.preventDefault();
		        holidayFilter.filterData();
		    }
		} 
};
var holiday ={
		holidayId:0,
		holidayName:'',
		init:function(){
			console.log('self.init');
			self =holiday;
			setOptionsForMultipleSelect($(pageContextElements.eleYear));
			$(pageContextElements.addBtn).on('click',self.initAddModal);

			$(addModal.eleEndDateCheckBox).on('click',self.enableEndDate);
			$(editModal.eleEndDateCheckBox).on('click',self.enableEndDateEdit);
			$(deleteModal.confirm).on('click',self.deleteFunction);
		},
		enableEndDateEdit: function(){
			if($(this).is(':checked')) {
				$("#editEndDateDiv").show();
				$("#editStartDateLabel").text("Start Date");
			} else {
				$(editModal.eleEndDate).val('');
				$("#editStartDateLabel").text("Date");
				$("#editEndDateDiv").hide();
			}
		},
		enableEndDate: function(){
			if($(this).is(':checked')) {
				$(".add-enddate-div").show();
				$("#addStartDateLabel").text("Start Date");
			} else {
				$("#addStartDateLabel").text("Date");
				$(addModal.eleEndDate).val('');
				$(".add-enddate-div").hide();
			}
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			$("#addStartDateLabel").text("Date");
			$(addModal.eleEndDate).val('');
			$(".add-enddate-div").hide();
			$(addModal.form).trigger("reset");
			holiday.enableDatePicker();
			holiday.resetForm(addModal.form);
			$(addModal.modal).modal("show");
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleName).val(),
					"startDate":$(addModal.eleStartDate).val(),
					"endDate":$(addModal.eleEndDate).val()
			};
			var addAjaxCall = shiksha.invokeAjax("shiksha/holiday", ajaxData, "POST");

			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.eleSaveBtn){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type : "success",
							title : appMessgaes.success	,
							body : addAjaxCall.responseMessage,
							close :'auto'
						})
					}
					if(submitBtnId == addModal.eleSaveMoreBtn){
						$(addModal.modal).modal("show");
						$(addModal.eleMessage).show();
						fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					}
					$(".add-enddate-div").hide();
					$(addModal.form).trigger("reset");
					holiday.enableDatePicker();
					holiday.refreshTable(pageContextElements.table);

					
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(holidayId){
			$(editModal.eleEndDate).val('');
			$("#editStartDateLabel").text("Date");
			$("#editEndDateDiv").hide();
			$(editModal.modal).modal("show");
			setOptionsForMultipleSelect(editModal.eleYear);
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shiksha/holiday/"+holidayId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleHolidayId).val(editAjaxCall.holidayId);
					$(editModal.eleName).val(editAjaxCall.name);
					$(editModal.eleStartDate).val(editAjaxCall.startDate);
					if(editAjaxCall.endDate && editAjaxCall.endDate != editAjaxCall.startDate){
						$(editModal.eleEndDate).val(editAjaxCall.endDate);
						$(editModal.eleEndDateCheckBox).prop("checked",true);
						$(editModal.eleEndDateDiv).show();
					}else{
						$(editModal.eleEndDateCheckBox).prop("checked",false);
						$(editModal.eleEndDateDiv).hide();
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"holidayId"	:$(editModal.eleHolidayId).val(),
					"name"		:$(editModal.eleName).val(),
					"startDate"	:$(editModal.eleStartDate).val(),
					"endDate"	:$(editModal.eleEndDate).val()
			}

			var updateAjaxCall = shiksha.invokeAjax("shiksha/holiday", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					holiday.refreshTable(pageContextElements.table);
					AJS.flag({
						type : "success",
						title : appMessgaes.success	,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					});
				}
				holiday.enableDatePicker();
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				},function(){
					window.location.reload();
				})
			}
		},
		initDelete : function(id,name){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			holidayId=id;
			holidayName=name;
		},
		deleteFunction : function(){
			holiday.fnDelete(holidayId,holidayName);
		},
		fnDelete :function(holidayId,holidayName){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shiksha/holiday/"+holidayId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					holiday.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			validator=$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {

					name: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace: true
					},
					startDate: {
						required: true,
						validDate : true,
						//date:true
					},
					endDate: {
						required: function(){
							return $(validateFormName).find('[name="endDateCheckBox"]').is(":checked");
						},
						validDate : true,
						//date:true
					}
				},

				messages: {
					name: {
						required: messages.nameRequired,
						noSpace:messages.noSpace,
						minlength: messages.minLength,
						maxlength: messages.maxLength,

					},
					startDate: {
						required: messages.startDateRequired,
						validDate : messages.dateFormat,
						//date: messages.dateFormat,

					},
					endDate: {
						required: messages.endDateRequired,
						validDate : messages.dateFormat,
						//date: messages.dateFormat,
					}
				},
				errorPlacement: function(error, element) {
				    if($(element).hasClass("datepicker")){
				    	$(element).parent().parent().append(error);
				    } else {
				    	$(error).insertAfter(element);
				    }
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						holiday.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						holiday.fnUpdate();
					}
				}
			});
		},
		holidayActionFormater: function(value, row, index) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="holiday.initEdit(\''+row.holidayId+'\')" href="javascript:void(0)">Edit</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="holiday.initDelete(\''+row.holidayId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">Delete</a></li>';
				action+='</ul>';
			}
			return action;
		},
		getDataByYear: function(){
			selectedYear= $(pageContextElements.eleYear).find(optionSelected).val();
			$(pageContextElements.table).bootstrapTable('refresh', {
				url : "shiksha/holiday/year/"+selectedYear,
				method:'post',
				queryParamsType :'limit',
            });
			
			
		},
		enableDatePicker : function(){
			$('.datepicker').datepicker("remove");
			$('.datepicker').datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start
			}).on("hide",function(){
				$(this).trigger("blur");
			});
		}
};

$( document ).ready(function() {

	holiday.init();
	holidayFilter.init();"Please enter a valid date in the format DD/MM/YYYY"
	var  selectedYear=$(pageContextElements.eleYear).find(optionSelected).val();;
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: "#toolbarHoliday",
		url : "shiksha/holiday/year/"+selectedYear,
		method : "post",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 50,
		clickToSelect: false,
		formatShowingRows : holidayFilter.formatShowingRows,
		queryParams : holidayFilter.filterQueryParams,
		queryParamsType :'limit',
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
	jQuery.validator.addMethod("validDate", function(value, element) {
        return this.optional(element) || moment(value,"DD/MMM/YYYY").isValid();
    },messages.dateFormat );
	
	holiday.formValidate(addModal.form);
	holiday.formValidate(editModal.form);
		
	$(addModal.eleStartDate).on('changeDate', function(){
		$(addModal.eleEndDate).datepicker('setStartDate', $(this).val());
	}); 
	$(addModal.eleEndDate).on('changeDate', function(){
		$(addModal.eleStartDate).datepicker('setEndDate', $(this).val());
	});	
	$submitActors.click(function() {
		submitActor = this;
	});
});
