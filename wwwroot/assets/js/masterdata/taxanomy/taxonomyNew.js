var  colorClass='';
var gEvent=null;
var thLabels=[];
var initInput =function(){	
	
	$("#mdlAddTaxonomy").modal().show();
	$("#addTaxonomyName").val('');
	$("#addTaxonomyDescription").val('');
	$("#textAreaMsg").text(300);
	hideDiv($('#mdlAddTaxonomy #AddTextAreaMsg'));
	$("#mdlAddTaxonomy #addTaxonomyForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddTaxonomy #addTaxonomyForm").data('formValidation').resetForm();		
	 
}

$('#addTaxonomyDescription').keypress(function(e) {
	 var code = e.keyCode || e.which;
	 if(code == 13) {
		 var taxonomyDescription = $('#addTaxonomyDescription').val()+" ";
		 $('#addTaxonomyDescription').val(taxonomyDescription);		 
	 }
	
	});


/////////////

//create a row after save
var getTaxonomyRow =function(taxonomy){
	var taxonomyDescriptions = (taxonomy.taxonomyDescription).replace(/\\n/g, ' ');
	var row = 
		"<tr id='"+taxonomy.taxonomyId+"' data-id='"+taxonomy.taxonomyId+"'>"+
		"<td></td>"+
		"<td data-taxonomyid='"+taxonomy.taxonomyId+"'	title='"+taxonomy.taxonomyName+"'>"+taxonomy.taxonomyName+"</td>"+
		
		"<td data-taxonomycode='"+taxonomy.taxonomyCode+"' title='"+taxonomy.taxonomyCode+"'>"+taxonomy.taxonomyCode+"</td>"+		
        "<td data-taxonomydescription='"+taxonomy.taxonomyDescription+"' title='"+taxonomy.taxonomyDescription+"'>"+taxonomyDescriptions+"</td>"+	
		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top btnEditTaxonomy'	  id='btnEditTaxonomy'" +
		"onclick='editTaxonomyData(&quot;"+taxonomy.taxonomyId+"&quot;,&quot;"+escapeHtmlCharacters(taxonomy.taxonomyName)+"&quot;,&quot;"+taxonomy.taxonomyName+"&quot;,&quot;"+escapeHtmlCharacters(taxonomy.taxonomyDescription)+"&quot;);'>   <span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a style='margin-left:10px;' class='tooltip tooltip-top btnDeleteTaxonomy'	 id=deleteTaxonomy"+taxonomy.taxopnomyId+"'	onclick='deleteTaxonomyData(&quot;"+taxonomy.taxonomyId+"&quot;,&quot;"+escapeHtmlCharacters(taxonomy.taxonomyName)+"&quot;,&quot;"+taxonomy.taxonomyName+"&quot;);'> <span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	return row;
}


var applyPermissions=function(){
	if (!editTaxonomyPermission){
	$(".btnEditTaxonomy").remove();
	}
	if (!deleteTaxonomyPermission){
	$(".btnDeleteTaxonomy").remove();
	}
	}

var addTaxonomyData = function(event) {
	$(".outer-loader").show();

	var taxonomyName = $('#addTaxonomyName').val().trim().replace(/\u00a0/g," ");
	var taxonomyDescription = $('#addTaxonomyDescription').val().replace(/\u00a0/g," ");

	var json = {

			"taxonomyName" : taxonomyName,
			"taxonomyDescription" : taxonomyDescription,
	};


	//update datatable without reloading the whole table
	//save&add new
var	btnAddMoreTaxonomyId =$(event.target).data('formValidation').getSubmitButton().data('id');

	var taxonomy = customAjaxCalling("taxonomy", json, "POST");
	if(taxonomy!=null){
		if(taxonomy.response=="shiksha-200"){
			$(".outer-loader").hide();

			var newRow =getTaxonomyRow(taxonomy);
			appendNewRow("taxonomyTable",newRow);
			applyPermissions();
			
			fnUpdateDTColFilter('#taxonomyTable',[],5,[0,4],thLabels);
			setShowAllTagColor(colorClass);
			if(btnAddMoreTaxonomyId=="addTaxonomySaveMoreButton"){
				$("#mdlAddTaxonomy").find('#errorMessage').hide();
				fnDisplaySaveAndAddNewElement('#mdlAddTaxonomy',taxonomyName);		

				initInput();
			}else{	
				$("#mdlAddTaxonomy").modal("hide");		
				setTimeout(function() {   
					AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>'+ taxonomy.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);

			}	
		}else {
			$(".outer-loader").hide();
			showDiv($('#mdlAddTaxonomy #alertdiv'));
			$("#mdlAddTaxonomy").find('#successMessage').hide();
			$("#mdlAddTaxonomy").find('#okButton').hide();
			$("#mdlAddTaxonomy").find('#errorMessage').show();
			$("#mdlAddTaxonomy").find('#exception').show();
			$("#mdlAddTaxonomy").find('#buttonGroup').show();
			$("#mdlAddTaxonomy").find('#exception').text(taxonomy.responseMessage);
		}
	}
	$(".outer-loader").hide();
};




/////////////////////////////////////////////////////



var editTaxonomyId = null;
var deleteTaxonomyName = null;
var deleteTaxonomyId = null;
var prevTaxonomyTagValue=null;
var editTaxonomyData = function(taxonomyId, taxonomyName ,sgEditTaxoName,taxonomyDescription) {
	
	prevTaxonomyTagValue =sgEditTaxoName.replace(/\u00a0/g," ");
	
	$('#editTaxonomyName').val(taxonomyName);
	$('#editTaxonomyDescription').val(taxonomyDescription);
	$('#editTextAreaMsg').text(300 - taxonomyDescription.length);
	editTaxonomyId = taxonomyId;
	$("#mdlEditTaxonomy").modal().show();
	hideDiv($('#mdlEditTaxonomy #editTextAreaMesg'));
	$("#mdlEditTaxonomy #editTaxonomyForm").data('formValidation').resetForm();
};




var editData = function() {
	$(".outer-loader").show();
	var taxonomyName = $('#editTaxonomyName').val().trim().replace(/\u00a0/g," ");
	var taxonomyDescription = $('#editTaxonomyDescription').val().replace(/\u00a0/g," ");
	var json = {
		"taxonomyId" : editTaxonomyId,
		"taxonomyName" : taxonomyName,
		"taxonomyDescription" : taxonomyDescription,
	};
	
	var taxonomy = customAjaxCalling("taxonomy", json, "PUT");
	if(taxonomy!=null){
	if(taxonomy.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("taxonomyTable",taxonomy.taxonomyId);
		var newRow =getTaxonomyRow(taxonomy);
		appendNewRow("taxonomyTable",newRow);
		applyPermissions();
		displaySuccessMsg();	
		
		fnUpdateDTColFilter('#taxonomyTable',[],5,[0,4],thLabels);
		setShowAllTagColor(colorClass);
			$('#mdlEditTaxonomy').modal("hide");
			
			setTimeout(function() {   
				AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ taxonomy.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
						
		}		
	else {
		
		$(".outer-loader").hide();
		showDiv($('#mdlEditTaxonomy #alertdiv'));
		$("#mdlEditTaxonomy").find('#successMessage').hide();
		$("#mdlEditTaxonomy").find('#errorMessage').show();
		$("#mdlEditTaxonomy").find('#exception').show();
		$("#mdlEditTaxonomy").find('#buttonGroup').show();
		$("#mdlEditTaxonomy").find('#exception').text(taxonomy.responseMessage);
	}
	}$(".outer-loader").hide();
};






/////////////////////////////////////////////////////



/////////////////////////////////////////////////////



var deleteTaxonomyData = function(taxonomyId, taxonomyName,sgDelTaxoName) {
	deleteTaxonomyId = taxonomyId;
	deleteTaxonomyName = taxonomyName;
	$('#mdlDeleteTaxonomy #deleteTaxonomyMessage').text(
			"Do you want to delete " + taxonomyName + " ?");
	$("#mdlDeleteTaxonomy").modal().show();
	hideDiv($('#mdlDeleteTaxonomy #alertdiv'));
};
$(document).on('click', '#deleteTaxonomyButton', function() {
	$(".outer-loader").show();
		
    var taxonomy = customAjaxCalling("taxonomy/"+deleteTaxonomyId, null, "DELETE");
    if(taxonomy!=null){
	if(taxonomy.response=="shiksha-200"){
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("taxonomyTable",deleteTaxonomyId);
		
		fnUpdateDTColFilter('#taxonomyTable',[],5,[0,4],thLabels);
		setShowAllTagColor(colorClass);
		
		
		$('#mdlDeleteTaxonomy').modal("hide");

		setTimeout(function() {   
			AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+ taxonomy.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);				
		
		
	} else {
		showDiv($('#mdlDeleteTaxonomy #alertdiv'));
		$("#mdlDeleteTaxonomy").find('#errorMessage').show();
		$("#mdlDeleteTaxonomy").find('#exception').text(taxonomy.responseMessage);
		$("#mdlDeleteTaxonomy").find('#buttonGroup').show();
	}
    }
	$(".outer-loader").hide();
});


var TaxonomyDescriptionArea =function(msgId){
	var maxchars = 300;
	$('textarea').on("keyup paste change drop",function (e) {
		if(e.type == "drop")
		    e.preventDefault();
	    var tlength = $(this).val().length;
	    $(this).val($(this).val().substring(0, maxchars));
	   
	   var remain = maxchars - parseInt(tlength);
	    $(msgId).text(remain);
	});
}







/////////////




var fnHideTaxonomyAlertDiv =function(){
	hideDiv($('#mdlAddTaxonomy #alertdiv'));
	hideDiv($('#mdlEditTaxonomy #alertdiv'));
}    




$(function() {
	
	thLabels =['#',' Name  ',' Taxonomy Code  ',' Description  ','Action'];
	fnSetDTColFilterPagination('#taxonomyTable',5,[0,4],thLabels);
	
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#taxonomyTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	$('#taxonomyTable').dataTable().fnDraw();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	
	TaxonomyDescriptionArea($('#mdlAddTaxonomy #textAreaMsg'));
	TaxonomyDescriptionArea($('#mdlEditTaxonomy #editTextAreaMsg'));

	$( "#mdlAddTaxonomy #addTaxonomyDescription" ).keypress(function() {
		showDiv($('#mdlAddTaxonomy #AddTextAreaMsg'));
	})
	
	$( "#mdlEditTaxonomy #editTaxonomyDescription" ).keydown(function() {
		showDiv($('#mdlEditTaxonomy #editTextAreaMesg'));
	})
	
	fnHideTaxonomyAlertDiv();
	
	
	$("#addTaxonomyForm")
			.formValidation(
					{
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							addTaxonomyName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: 'Taxonomy name must be less than 250 characters'
				                    },
									callback : {
										message : 'Taxonomy '+invalidInputMessage,
										callback : function(value) {
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							addTaxonomyDescription : {
								validators : {
									callback : {
										message : 'Description contains atleast one alpha character and does not contains these (", ?, \', /, \\) characters and two consecutive spaces',
										callback : function(value) {
											if(value!=""){
												var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
											}else{
												return true;
											}
										}
									}
								}
							},
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				
				addTaxonomyData(e);

			});
	$("#editTaxonomyForm")
			.formValidation(
					{
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							editTaxonomyName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: 'Taxonomy name must be less than 250 characters'
				                    },
									callback : {
										message : 'Taxonomy '+invalidInputMessage,
										callback : function(value) {
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							},
							editTaxonomyDescription : {
								validators : {
									callback : {
										message : 'Description contains atleast one alpha character and does not contains these (", ?, \', /, \\) characters and two consecutive spaces',
										callback : function(value) {
											if(value!=""){
												var regx=/^[^'?\"/\\]*$/;
												if(/\ {2,}/g.test(value))
													return false;
												return regx.test(value);
												}else{
													return true;
												}
											}
									}
								}
							},
							
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				
				editData();

			});
});