//global variables
var  colorClass='';
var unitIdList;
var unitObj;
//initialise global variables
var initGlblVariables =function(){
	//get all section on page load
	unitObj =customAjaxCalling("unit/list", null, "GET");
	
unitIdList=[];
$.each(unitObj,function(index,obj){
	unitIdList.push(obj.unitId);
});
}

//deleting gradesubjectmapping by id
var deleteGradeName = null;
var deleteGradeId = null;
var gradeId=null;
var subjectId=null;


//show table as per grade
$(selectBoxGrade).change(function(){
	//$(".outer-loader").show();
	//$('#gradesubjectTable thead').empty();
	$('#tableAndToggleDiv').hide();//Hide  Toggle & Table
	 $('#btnAddSubjecttoGrade').prop('disabled', false);
	deleteAllRow('gradesubjectTable');	
	//$('#gradesubjectTable thead').append('<tr><th style="width: 30px;"></th><th style="width: 160px;">Subject</th><th  class="nosort" style="width: 160px;"></th><th style="width: 160px;">Subject Code</th><th>Chapter(s)</th></tr>'+'<tr><th style="width: 30px;">#</th><th style="width: 160px;">Subject</th><th  class="nosort" style="width: 160px;"></th><th style="width: 160px;">Subject Code</th><th>Chapter(s)</th></tr>');
			
	var gradeId =$(this).find('option:selected').data('id');
	var gradeSubjectData = customAjaxCalling("gradesubjectmapper/" + gradeId, null,"GET");
		if(gradeSubjectData!=null && gradeSubjectData!=""){
		if(gradeSubjectData[0].response=="shiksha-200"){
			$(gradeSubjectData).each(function(index,dataObj){
				appendNewRow('gradesubjectTable',getNewRow(dataObj));
				applyPermissions();
			});
		}
		
		//below code for set datatable & filters
		//setDataTableColFilterPagination('#gradesubjectTable',5,[0,4],false);
		fnSetDTColFilterPagination('#gradesubjectTable',5,[0,4],thLabels);
		setShowAllTagColor(colorClass);
		fnMultipleSelAndToogleDTCol('.search_init',function(){
			fnShowHideColumns('#gradesubjectTable',[]);
		});
		
		enableTooltip();
	}	
		fnColSpanIfDTEmpty('gradesubjectTable',5);
		$('#tableAndToggleDiv').show();//show  Toggle & Table
		$('#addsubjectgradebuttonId').show();//show Add Button
		
});


var getNewRow =function(gradeSubjectData){
	var a = '<ul style="padding-left:15px;">', b = '</ul style="padding-left:15px;">', m = [],result='';
	var chapterList= gradeSubjectData.unitNames;
	var temp = new Array();
	if(chapterList!=null){
		temp = chapterList.split("#,");
	}
	
	$.each(temp, function (index, value) {
		 m[index] = '<li>' + value + '</li>';
	});
	result=a+m+b;
	result=result.replace(/i>,/g , "i>");
	result=result.replace(/#/g , "");
	
	var newRow =
		"<tr id='"+gradeSubjectData.id+"_"+gradeSubjectData.subjectId+"' data-id='"+gradeSubjectData.id+"' data-gradeid='"+gradeSubjectData.gradeId+"' data-subjectid='"+gradeSubjectData.subjectId+"'  data-unitsids='"+gradeSubjectData.unitsIds+"'>"+
		"<td></td>"+		
			"<td data-subjectid='"+gradeSubjectData.subjectId+"'	title='"+escapeHtmlCharacters(gradeSubjectData.subjectName)+"'>"+gradeSubjectData.subjectName+"</td>"+
			"<td data-subjectid='"+gradeSubjectData.subjectId+"'	title='"+escapeHtmlCharacters(gradeSubjectData.subjectCode)+"'>"+gradeSubjectData.subjectCode+"</td>"+
			"<td data-unitids='"+gradeSubjectData.unitIds+"'	title='"+escapeHtmlCharacters(gradeSubjectData.unitNames)+"'>"+result+"</td>"+
			"<td><div  class='div-tooltip'><a  class='tooltip tooltip-top'  id='editGradeSubject'onclick=editGradeSubjectData(&quot;"+gradeSubjectData.id+"_"+gradeSubjectData.subjectId+"&quot;,&quot;"+gradeSubjectData.subjectId+
			"&quot;,'"+JSON.stringify(gradeSubjectData.associatedWithAsmt)+
			"',&quot;"+gradeSubjectData.unitIds+
			"&quot;);><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a style='margin-left:10px;' class='tooltip tooltip-top'   id='deleteSchoolGrade'	onclick=deleteGradeSubjectMappingData(&quot;"+gradeSubjectData.id+"_"+gradeSubjectData.subjectId+"&quot,&quot;"+escapeHtmlCharacters(gradeSubjectData.subjectName)+"&quot;,&quot;"+gradeSubjectData.gradeId+"&quot,&quot;"+gradeSubjectData.subjectId+"&quot;); ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	chapterList='';
		return newRow;
};


var removeActionButton=function(buttonId){

	$("#gradesubjectTable tr").each(function(index,item){
		$(this).find("#"+buttonId).remove();

	});
}


var applyPermissions=function(){
	if (!editGradeSubjectUnitMappingPermission){
		removeActionButton("editGradeSubject");
		//$("#editGradeSubject").remove();
	}
	if (!deleteGradeSubjectUnitMappingPermission){
		removeActionButton("deleteSchoolGrade");
		//$("#deleteSchoolGrade").remove();
	}
	}

// onclick addgradesubject button

var getAllSubjects =function(obj){
	
	resetFormValidatonForLoc('addGradeSubjectMapping',"subjects");
	resetFormValidatonForLoc('addGradeSubjectMapping',"unit");
	enableTooltip();
	
	//$("#addGradeSubjectMapping").modal().show();
	//adding subject to listbox
	$("#addGradeSubjectMapping #selectBox_subject").multiselect('destroy');
	bindSubjectToListBox($("#addGradeSubjectMapping #selectBox_subject"),0);
	setOptionsForMultipleSelect($("#addGradeSubjectMapping #selectBox_subject"));		
	refreshMultiselect($("#addGradeSubjectMapping #selectBox_subject"));
	setOptionsForMultipleSelect($("#addGradeSubjectMapping #selectBox_subject"));
	fnCollapseMultiselect();
	
	//add unit to listbox
	$("#addGradeSubjectMapping #selectBox_unit").multiselect('destroy');
	bindUnitsToListBox($("#addGradeSubjectMapping #selectBox_unit"));
	setOptionsForMultipleSelect($("#addGradeSubjectMapping #selectBox_unit"));		
	refreshMultiselect($("#addGradeSubjectMapping #selectBox_unit"));
	setOptionsForMultipleSelect($("#addGradeSubjectMapping #selectBox_unit"));
	fnCollapseMultiselect();
/*	//disable Subject if already added for this grade */
	disableSubject($("#addGradeSubjectMapping #selectBox_subject"));
	
	var isGradeselected =$('#selectBoxGrade').find('option:selected').val();
	
	$('#addGradeSubjectMapping #addGradeSubjectMessage').text("Add Grade Subject Chapter(s) Mapping : "+isGradeselected );
	
	$('#addGradeSubjectMapping #addGradeSubjectMessage').prop("title",isGradeselected);
	//var isSchoolLocselected =$('#selectBox_schoolLocationType').find('option:selected').val();
	/*if(isGradeselected==null || isGradeselected.length==0){*/
		if(isGradeselected=="NONE"){$('#msgText').text('');$('#msgText').text('Please select Grade ')
		$('#mdlError').modal("show");
			return false;
}else

{
	
		var noOfRows =$('#gradesubjectTable').dataTable().fnSettings().fnRecordsTotal();
	
		var noOfSubjects =($('#addGradeSubjectMapping #selectBox_subject option').length)-1;
	
		if(noOfRows==noOfSubjects){
			
			$('#msgText').text('');$('#msgText').text('All subjects have been added for selected grade.')
			$('#addGradeSubjectMapping').modal("hide");
			$('#mdlError').modal("show");
			return false;
		}
		
		else{
			$('#addGradeSubjectMapping').modal("show");
			// $("#addGradeSubjectMapping #addGradeSubjecMappingForm").data('formValidation').resetForm();
		}
	}
	
	

}

//disable subject in select box if already added for this school
var disableSubject =function(elementSubject){
	
	mappedSubjects =[];subjectIdList=[];var i=0;
	/*$('#gradesubjectTable >tbody>tr').each(function(i,row){		
		mappedSubjects.push( parseInt(row.getAttribute('data-subjectid')));
	});*/
	
	var nRows =$("#gradesubjectTable").dataTable().fnGetNodes(); 
	$.each(nRows,function(i,row){		
		var id=$(row).prop('id');
		var subjectId =id.split('_')[1];
		mappedSubjects.push( parseInt(subjectId));
	});	

	$(elementSubject).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			subjectIdList.push( parseInt($(option).data('id')));
		});
		
	});	
	/*var mappedSchool =$('#selectBox_schoolsByLoc').find('option:selected').data("id");*/
	
	$.grep(subjectIdList, function(el) {
		if ($.inArray(el, mappedSubjects) != -1){
			var option = $(elementSubject).find('option[value="' +el+ '"]');
			option.prop('disabled', true);
			option.parent('label').parent('a').parent('li').remove();
		}
		i++;
	});
	$(elementSubject).multiselect('refresh');
}

// adding grade id and subject id in gradesubject mapping

var addgradeSubjectMaping = function(event) {
	$(".outer-loader").show();
	setTimeout(function(){
	var addUnitIds=[];subjectId='';
	$("#addGradeSubjectMapping #selectBox_unit").each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				addUnitIds.push($(option).data('id'));
		});
	});	
	 subjectId = $("#selectBox_subject").find("option:selected").data('id');
	var gradeId = $("#selectBoxGrade").find("option:selected").data('id');
	
	if(addUnitIds.length==0) addUnitIds=null;
	
	var json = {
		"gradeId" : gradeId,
		"subjectId" : subjectId,
		"unitIds":addUnitIds
	};
	
	 btnAddMoreGradeSubject =$(event.target).data('formValidation').getSubmitButton().data('id');
	//alert(btnAddMoreGradeSubject);
	
	var responseData = customAjaxCalling("gradeSubjectMapping", json, "POST");
	
	
	if(responseData!=null && responseData!=""){
		
		if(responseData[0]!=null && responseData[0]!=""){
		if(responseData[0].response=="shiksha-200")
			$(responseData).each(function(index,dataObj){
				appendNewRow('gradesubjectTable',getNewRow(dataObj));
				applyPermissions();
				//	fnUpdateColumnFilter('#gradesubjectTable',false,[],5,[0,4]);
				fnUpdateDTColFilter('#gradesubjectTable',[],5,[0,4],thLabels);
				setShowAllTagColor(colorClass);
				
			});
			addButtonId =$(event.target).data('formValidation').getSubmitButton().data('id');
			if(addButtonId=="addGSMSaveMoreButton"){
				
			/*	$("#addGradeSubjectMapping").find('#successMessage').show();
				$("#addGradeSubjectMapping").find('#success').text(responseData[0].responseMessage);*/
				
				/*$("#addGradeSubjectMapping").find('#successMessage').fadeOut(2000);
				$('#notify').fadeIn(1000);*/
				
				getAllSubjects(this);
			}else{			
				$('#addGradeSubjectMapping').modal("hide");
				$('#success-msg').show();
				
				$('#showMessage').text(responseData[0].responseMessage);
				$("#success-msg").fadeOut(3000);
				$('#notify').fadeIn(3000);
			}
			$(".outer-loader").hide();	
	}	
	}else{
			$(".outer-loader").hide();
			$("#addGradeSubjecMappingForm").find('#alertDiv').show();
			$("#addGradeSubjecMappingForm").find('#successMessage').hide();		
			$("#addGradeSubjecMappingForm").find('#errorMessage').show();
			$("#addGradeSubjecMappingForm").find('#exception').show();
			$("#addGradeSubjecMappingForm").find('#buttonGroup').show();
			$("#addGradeSubjecMappingForm").find('#exception').text(responseData[0].responseMessage);
		}
	
	$(".outer-loader").hide();
	$(window).scrollTop(0);
	enableTooltip();
	}, 100);
};

//for edit
var editGradeMapperId = null;
var subjectname = null;
var subId=null;



var editSubjectId = null;
var deleteStateName = null;
var deleteStateId = null;



var editGradeSubjectData =function(gradeMapperId,subjectId,assmtMappedIds,unitIds){
	 //$("#addLeagendDataForm").find("#questionId").attr("data-val",questionId);
	

		zUnitIds =unitIds;
	assmtUnitIds=[];
	

	
	$.each(JSON.parse(assmtMappedIds),function(index,gradeDataItem){
		if(gradeDataItem==true){
			assmtUnitIds.push(parseInt(index));
		}
		
	});
	
	
	$("#edit_gradeSubjectMapping #editSubjectGradeMappingData").find("#alertDiv #errorMessage").hide();
	$("#edit_gradeSubjectMapping").find('#divInfo').hide();
	aEditRowId =gradeMapperId;
	editGradeMapperId =gradeMapperId.split('_')[0];
	
	resetFormValidatonForLoc('editSubjectGradeMappingData',"subjects");
	resetFormValidatonForLoc('editSubjectGradeMappingData',"unit");
//	editGradeMapperId=gradeMapperId;
	$("#edit_gradeSubjectMapping").modal().show();
	$("#edit_gradeSubjectMapping #selectBox_subjects").multiselect('destroy');
	bindSubjectToListBox($("#edit_gradeSubjectMapping #selectBox_subjects"),0);
	setOptionsForMultipleSelect($("#edit_gradeSubjectMapping #selectBox_subjects"));		
	refreshMultiselect($("#edit_gradeSubjectMapping #selectBox_subjects"));
	setOptionsForMultipleSelect($("#edit_gradeSubjectMapping #selectBox_subjects"));
	fnCollapseMultiselect();
	//add sections
	$("#edit_gradeSubjectMapping #selectBox_unit").multiselect('destroy');
	bindUnitsToListBox($("#edit_gradeSubjectMapping #selectBox_unit"));
	fnSetMSelectToChapter($("#edit_gradeSubjectMapping #selectBox_unit"));		
	refreshMultiselect($("#edit_gradeSubjectMapping #selectBox_unit"));
	fnSetMSelectToChapter($("#edit_gradeSubjectMapping #selectBox_unit"));
	fnCollapseMultiselect();
	//disable grade if already added for this school
	removeSubject(subjectId,"#edit_gradeSubjectMapping #selectBox_subjects");
	
	makeUnitSelected(unitIds,$("#edit_gradeSubjectMapping #selectBox_unit"));
	/*makeUnitDisabled(AssmtUnitIds,$("#edit_gradeSubjectMapping #selectBox_unit"));*/
    
	subId=subjectId;
	//$("#selectBox_EditSubject option[data-id='"+ gradeMapperId.toString().trim() + "']").attr('selected','selected')
	var isGradesName =$('#selectBoxGrade').find('option:selected').val();
	$('#edit_gradeSubjectMapping #editGradeSubjectMessage').text("Edit Grade Subject Chapter(s) Mapping : "+isGradesName );
	$('#edit_gradeSubjectMapping #editGradeSubjectMessage').prop("title",isGradesName);
	$("#edit_gradeSubjectMapping").modal().show();
	  $("#edit_gradeSubjectMapping #editSubjectGradeMappingData").data('formValidation').resetForm();
}

//$(document).on('click', '#editSubjectSaveButton', function() {
	
	var editSubjectGradeMapping = function(event){
	$(".outer-loader").show();
	editUnitIds=[];
		
	$("#edit_gradeSubjectMapping #selectBox_unit").each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				editUnitIds.push($(option).data('id'));
		});
	});
	if(editUnitIds.length==0) editUnitIds=null;
	var json ={
		"id":editGradeMapperId,
		"unitIds":editUnitIds
	};
	
	var responseData = customAjaxCalling("gradeSubjectMapping", json, "PUT");
	if(responseData!=null && responseData!=""){
		if(responseData.response=="shiksha-200"){
			deleteRow("gradesubjectTable",aEditRowId);	
			appendNewRow('gradesubjectTable',getNewRow(responseData));
			applyPermissions();
			//fnUpdateColumnFilter('#gradesubjectTable',false,[],5,[0,4]);
			fnUpdateDTColFilter('#gradesubjectTable',[],5,[0,4],thLabels);
			setShowAllTagColor(colorClass);
			$('#edit_gradeSubjectMapping').modal("hide");
			$('#success-msg').show();
			$('#showMessage').text(responseData.responseMessage);
			$("#success-msg").fadeOut(3000);
			$('#notify').fadeIn(3000);
			
			$(".outer-loader").hide();	
		}else{			
			$("#editSubjectGradeMappingData").find('#alertDiv').show();
			$("#editSubjectGradeMappingData").find('#successMessage').hide();		
			$("#editSubjectGradeMappingData").find('#errorMessage').show();
			$("#editSubjectGradeMappingData").find('#exception').show();
			$("#editSubjectGradeMappingData").find('#buttonGroup').show();
			$("#editSubjectGradeMappingData").find('#exception').text('');
			$("#editSubjectGradeMappingData").find('#exception').text(responseData.responseMessage);
			$(".outer-loader").hide();
		}
	}
	$(window).scrollTop(0);
	enableTooltip();
};



//remove all subjects except current edited subject in edit form
var removeSubject = function(selectedSubject,elementSubject){
	$(elementSubject).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			if($(option).data('id')!=null){
				var currentId=parseInt($(option).data('id'));
				if(currentId==parseInt(selectedSubject)){
					$(option).prop('selected', true);
				}else{
					$(option).prop('disabled', true);
					$(option).parent('label').parent('a').parent('li').remove();
				}
			}
		});
	});		
	$(elementSubject).multiselect('refresh');
};

//make units seleted in edit form
var makeUnitSelected =function(selectedIds,elementUnit){

	var i=0;
	
	selectedIds = $.map(selectedIds.split(","), function(el) { return parseInt(el, 10); });

		$.grep(unitIdList, function(el) {
		if ($.inArray(el, selectedIds) != -1){
			var option = $(elementUnit).find('option[value="' +el+ '"]');
			option.prop('selected', true);
		}
		i++;
	});
	$(elementUnit).multiselect('refresh');
}


var makeUnitDisabled =function(diabledIds,elementUnit){

	var i=0;
	diabledIds = $.map(diabledIds, function(el) { return parseInt(el, 10); });
		$.grep(unitIdList, function(el) {
		if ($.inArray(el, diabledIds) != -1){
			var option = $(elementUnit).find('option[value="' +el+ '"]');
			option.prop('selected', true);
		}
		i++;
	});
	$(elementUnit).multiselect('refresh');
}





var deleteGradeSubjectMappingData = function(gradeSubjectMappingId, subjectName,grdId,subId) {
	aDeleteRowId =gradeSubjectMappingId;
	deleteGradeId =gradeSubjectMappingId.split('_')[0];
	gradeId=grdId;
	subjectId=subId;
	//deleteGradeId = gradeSubjectMappingId;
	deleteGradeName = subjectName;
	  
	var deleteGradesName =$('#selectBoxGrade').find('option:selected').val();
	$('#delete_gradeSubjectMapping #deleteGradeSubjectMessageHeader').text("Delete Grade Subject Chapter(s) Mapping : "+deleteGradesName );
	$('#delete_gradeSubjectMapping #deleteGradeSubjectMessageHeader').prop("title",deleteGradesName);
	$('#delete_gradeSubjectMapping #deleteGradeSubjectMessage').text(
			"Do you want to delete " + subjectName + " ?");
	$("#delete_gradeSubjectMapping").modal().show();
	hideDiv($('#delete_gradeSubjectMapping #alertdiv'));
};
$(document).on('click', '#deletegradeSubjMappId', function(event) {
   $(".outer-loader").show();

	var json = {
		"id" : deleteGradeId,
		"subjectName" : deleteGradeName,
		"gradeId" : gradeId,
		"subjectId" : subjectId,
	};
	var deleteGradeSubjectMapping = customAjaxCalling("gradeSubjectMapping", json, "DELETE");
	if(deleteGradeSubjectMapping!=null && deleteGradeSubjectMapping!=""){
	if(deleteGradeSubjectMapping.response=="shiksha-200"){
		$(".outer-loader").hide();						
	
		deleteRow("gradesubjectTable",aDeleteRowId);
		//fnUpdateColumnFilter('#gradesubjectTable',false,[],5,[0,4]);
		fnUpdateDTColFilter('#gradesubjectTable',[],5,[0,4],thLabels);
		setShowAllTagColor(colorClass);
		$('#delete_gradeSubjectMapping').modal("hide");
		$('#success-msg').show();
		$('#showMessage').text(deleteGradeSubjectMapping.responseMessage);
		$("#success-msg").fadeOut(3000);
		$('#notify').fadeIn(3000);
		
	}
		
	 else {
		$(".outer-loader").hide();
		
		showDiv($('#delete_gradeSubjectMapping #alertdiv'));
		$("#delete_gradeSubjectMapping").find('#errorMessage').show();
		$("#delete_gradeSubjectMapping").find('#exception').text(deleteGradeSubjectMapping.responseMessage);
		$("#delete_gradeSubjectMapping").find('#buttonGroup').show();
	}
	}
});




$(function() {
	
	$('#tableAndToggleDiv').hide();//Hide  Toggle & Table
	$('#addsubjectgradebuttonId').hide();//Hide add Button
	thLabels =['#','Subject  ','Subject Code  ','Chapter(s)  ','Action'];
	fnSetDTColFilterPagination('#gradesubjectTable',5,[0,4],thLabels);
	//below code for set datatable & filters
	//setDataTableColFilterPagination('#gradesubjectTable',5,[0,4],false);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#gradesubjectTable',[]);
	});
	fnColSpanIfDTEmpty('gradesubjectTable',5);
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	$('#gradesubjectTable').dataTable().fnDraw();

	  setOptionsForMultipleSelect($("#addGradeSubjectMapping #selectBox_unit"));
	  setOptionsForMultipleSelect($("#selectBoxGrade"));
	 
	  $('#rowDiv1').show();
	  fnCollapseMultiselect();
	//setDataTablePagination('#gradeSubjectMappinTable');
	  initGlblVariables();
	$("#addGradeSubjectMapping")
			.formValidation(
					{ excluded: ':disabled',
						feedbackIcons : {
							
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							subjects: {
								validators: {
									callback: {
										message: '      ',
										callback: function(value, validator, $field) {
											// Get the selected options
											var options =$('#addGradeSubjectMapping #selectBox_subject').val();
											return (options !=null);
										}
									}
								}
							},
							unit: {
								validators: {
									callback: {
										message: '      ',
										callback: function(value, validator, $field) {
											// Get the selected options
											var options =$('#addGradeSubjectMapping #selectBox_unit').val();
											return (options !=null);
										}
									}
								}
							}
							
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addgradeSubjectMaping(e);

			});
	$("#editSubjectGradeMappingData")
			.formValidation(
					{excluded: ':disabled',
						feedbackIcons : {
							
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							subjects: {
								validators: {
									callback: {
										message: '      ',
										callback: function(value, validator, $field) {
											// Get the selected options
											var options =$('#edit_gradeSubjectMapping #selectBox_subjects').val();
											
											return (options !=null);
										}
									}
								}
							},
							unit: {
								validators: {
									callback: {
										message: '      ',
										callback: function(value, validator, $field) {
											// Get the selected options
											var options =$('#edit_gradeSubjectMapping #selectBox_unit').val();
											return (options !=null);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				editSubjectGradeMapping(e);

			});
});












function doSelected(sId){
	$('#edit_gradeSubjectMapping #selectBox_unit').find('option').each(function(i,ele){
//	$("#editSubjectGradeMappingData").data('formValidation').updateStatus('unit', 'VALID');
		if($(ele).val() == sId){
			
			$(ele).prop("checked",true);
		}
		
	});
	
}
var fnSetMSelectToChapter = function(arg){	
	$(arg).multiselect({
		maxHeight: 100,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,	
		filterPlaceholder: 'Search here...',
		 onChange: function(option, checked, select) {
			 $("#edit_gradeSubjectMapping").find('#divInfo').hide();
           
			
			 if(($(option).val())=="multiselect-all")
	            {
	            	
	            	// $('#edit_gradeSubjectMapping #info').text("This chapter have been mapped with Assessment. So you cannot delete this");
	             	//$("#edit_gradeSubjectMapping").find('#divInfo').show();
	            	
	         	if (assmtUnitIds.length > 0){
	           	/* $('#edit_gradeSubjectMapping #info').text("Selected chapters have been mapped with Assessment. So you cannot delete this");
	         	$("#edit_gradeSubjectMapping").find('#divInfo').show();*/
	           		
	           			
	           		makeUnitDisabled(assmtUnitIds,$("#edit_gradeSubjectMapping #selectBox_unit"));
	            	$("#editSubjectGradeMappingData").data('formValidation').updateStatus('unit', 'VALID');
	            	return;
	           		/*}*/
	         	}
	            }
			
			 aSelected =parseInt($(option).val());
			
				 
             var bFoo =false;
             if(jQuery.inArray(aSelected, assmtUnitIds) !== -1){
            	 $('#edit_gradeSubjectMapping #info').text("This chapter have been mapped with Assessment. So you cannot delete this");
        	$("#edit_gradeSubjectMapping").find('#divInfo').show();
            		
            	 bFoo=true;
             }
             if(bFoo==true)
            	 makeUnitDisabled(assmtUnitIds,$("#edit_gradeSubjectMapping #selectBox_unit"));
         }

	});	
}
