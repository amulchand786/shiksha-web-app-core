var pageContextElements = {
		table:$('#shikshaPlusStageTable'),
		addBtn:$("#addShikshaPlusStage"),
		toolbar : "#toolbarShikshaPlusStage",
};
var addModal={
		form : $('#addShikshaPlusStageForm'),
		modal : $('#mdlAddShikshaPlusStage'),
		eleName :$("#addShikshaPlusStageName"),
		eleDescription :$("#addShikshaPlusStageDescription")
};

var editModal={
		form : $('#editShikshaPlusStageForm'),
		modal : $('#mdlEditShikshaPlusStage'),
		eleId : $('#editShikshaPlusStageId'),
		eleName :$("#editShikshaPlusStageName"),
		eleDescription :$('#editShikshaPlusStageDescription')
};
var deleteModal={
		modal : $('#mdlDelShikshaPlusStage'),
		confirm : $('#btnDelShikshaPlusStage')
};
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');
var $table;
var self;
var stageId;
var shikshaPlusStageFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var shikshaPlusStage ={
		stageId:0,
		init:function(){
			self =shikshaPlusStage;
			
			pageContextElements.addBtn.on('click',self.initAddModal);
			deleteModal.confirm.on('click',shikshaPlusStage.deleteFunction);
		},

		refreshAddModalForm: function(){
			addModal.form.resetForm();
		},

		initAddModal :function(){
			fnInitSaveAndAddNewList();
			shikshaPlusStage.resetForm(addModal.form);	
			addModal.modal.modal("show");
		},


		initDelete : function(stageIdDelete,name){
			var msg = $("#deleteShikshaplusStageMessage").data("message");
			$("#deleteShikshaplusStageMessage").html(msg.replace('@NAME@',name));
			deleteModal.modal.modal("show");
			stageId=stageIdDelete;
		},

		deleteFunction : function(){
			shikshaPlusStage.fnDelete(stageId);
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":addModal.eleName.val(),
					"description":addModal.eleDescription.val()
			};
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/stage", ajaxData, "POST");

			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == "addShikshaPlusStageSaveButton"){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type : "success",
							title : stageMessages.sucessAlert,
							body : addAjaxCall.responseMessage,
							close :'auto'
						});
					}
					if(submitBtnId == "addShikshaPlusStageSaveMoreButton"){
						$(addModal.modal).modal("show");
						$(addModal.modal).find("#divSaveAndAddNewMessage").show();
						fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					}
					shikshaPlusStage.resetForm(addModal.form);
					shikshaPlusStage.refreshTable(pageContextElements.table);

					
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(stageId){
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/stage/"+stageId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					editModal.eleId.val(editAjaxCall.stageId);
					editModal.eleName.val(editAjaxCall.name);
					editModal.eleDescription.val(editAjaxCall.description);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"stageId":editModal.eleId.val(),
					"name":editModal.eleName.val(),
					"description":editModal.eleDescription.val()
			}

			var updateAjaxCall = shiksha.invokeAjax("shikshaplus/stage", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					shikshaPlusStage.resetForm(editModal.form);
					shikshaPlusStage.refreshTable(pageContextElements.table);
					AJS.flag({
						type : "success",
						title : stageMessages.sucessAlert,	
						body : updateAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				},function(){
					window.location.reload();
				})
			}
		},
		fnDelete :function(stageId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/stage/"+stageId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					shikshaPlusStage.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : stageMessages.sucessAlert,	
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {

					name: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace: true
					},
					/*description: {
						required: true,
						minlength: 2,
						maxlength: 255,
						noSpace: true
					}*/
				},

				messages: {
					name: {
						required: 	stageMessages.nameRequired,
						noSpace:	stageMessages.nameNoSpace,
						minlength: 	stageMessages.nameMinLength,
						maxlength: 	stageMessages.nameMaxLength,

					},
					/*description: {
						required: 	stageMessages.descriptionRequired,
						minlength:	stageMessages.descriptionMinLength,
						maxlength:	stageMessages.descriptionMaxLength,
						noSpace:	stageMessages.descriptionNoSpace,

					}*/
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						shikshaPlusStage.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						shikshaPlusStage.fnUpdate();
					}
				}
			});
		},
		shikshaPlusStageActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="shikshaPlusStage.initEdit(\''+row.stageId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="shikshaPlusStage.initDelete(\''+row.stageId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		}
};

$( document ).ready(function() {

	shikshaPlusStage.init();
	$table = pageContextElements.table.bootstrapTable({
		toolbar: pageContextElements.toolbar,
		url : "shikshaplus/stage",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		formatShowingRows : shikshaPlusStageFilterObj.formatShowingRows,
		clickToSelect: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");

	shikshaPlusStage.formValidate(addModal.form);
	shikshaPlusStage.formValidate(editModal.form);
	
	$submitActors.click(function() {
		submitActor = this;
	});
});
