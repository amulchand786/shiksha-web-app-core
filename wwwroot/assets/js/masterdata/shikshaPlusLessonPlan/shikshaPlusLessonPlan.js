var currentChapter = 0;
var lessonPlanFilter = {
		acYearSelect 	: "#filterBox_acYear",
		schoolsSelect  	: "#filterBox_schools",
		levelSelect	 	: "#filterBox_level",
		sectionSelect  	: "#filterBox_section",
		groupSelect		: "#filterBox_group",
		acYearSelectDiv : "#filter-acYearDiv",
		schoolsSelectDiv: "#filter-schoolsDiv",
		levelSelectDiv 	: "#filter-levelDiv",
		sectionSelectDiv: "#filter-sectionDiv",
		groupSelectDiv	: "#filter-groupDiv",
		searchBtn 		: "#filter-search-btn",
		searchBoxDiv	: "#filter-searchDiv",
		searchBox		: "#filter-search",
		toolbar		 	: "#conteFilterContainer",
		table			: "#teachPlanChapterList",
		planContainer   : "#lesson-container",
		createContainer	: ".shk-create-plan-container",
		viewContainer	: ".shk-view-plan-container",
		editContainer	: ".shk-edit-plan-container",
		submitBtn		: "#submitBtn",
		lessonTitle		: '#lessonTitle',
		viewLessonTitle	: '#viewLessonTitle',
		editLessonTitle	: '#editLessonTitle',
		viewObjective	: '#viewObjective',
		viewDuration	: '#viewDuration',
		objective		: '#objective',
		durationDays	: '#durationDays',
		rowCount		: 0,
		lessonDownload : "#lesson-download",
		init : function(){
			this.enableMultiSelectForFilters(this.levelSelect,lessonCol.level+": none",this.levelChange);
			this.enableMultiSelectForFilters(this.groupSelect,lessonCol.group+": none",this.groupChange);
			$(this.searchBtn).on("click",this.filterData);
			$(this.lessonDownload).on("click",this.download);
		},
		download : function(){
			var levelId=$(lessonPlanFilter.levelSelect).val();
			var groupId=$(lessonPlanFilter.groupSelect).val();
			if(levelId!=null && groupId!=null ){
				window.location = baseContextPath +"/shikshaplus/lessonplan/level/"+levelId+"/group/"+groupId+"/download";
			}else if(levelId==null){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : lessonCol.levelRequired,
					close : 'auto'
				});
			}else if(groupId==null){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : lessonCol.groupRequired,
					close : 'auto'
				});
			}
		},
		acyearChange : function(){
			if($(lessonPlanFilter.subjectSelect).val()){
				lessonPlanFilter.filterData();
			}
		},
		schoolChange : function(){
			$(lessonPlanFilter.searchBtn).hide();
			$(lessonPlanFilter.sectionSelectDiv).hide();
			$(lessonPlanFilter.subjectSelectDiv).hide();
			$(lessonPlanFilter.table).hide();
			lessonPlanFilter.emptySelect(lessonPlanFilter.gradeSelect);
			lessonPlanFilter.emptySelect(lessonPlanFilter.sectionSelect);
			lessonPlanFilter.emptySelect(lessonPlanFilter.subjectSelect);
			bindGradesBySchoolToListBox($(lessonPlanFilter.gradeSelect),$(lessonPlanFilter.schoolsSelect).val(),0)
			$(lessonPlanFilter.gradeSelect).multiselect("rebuild");
			$(lessonPlanFilter.gradeSelectDiv).show();
		},
		levelChange : function(){
			$(lessonPlanFilter.searchBtn).hide();
			$(lessonPlanFilter.subjectSelectDiv).hide();
			$(lessonPlanFilter.planContainer).hide();
			$(lessonPlanFilter.searchBoxDiv).attr("style","display:none !important;");
			$(lessonPlanFilter.searchBox).val("");
			lessonPlanFilter.emptySelect(lessonPlanFilter.sectionSelect);
			lessonPlanFilter.emptySelect(lessonPlanFilter.subjectSelect);
			shikshaPlus.fnGetGroupsByLevelIdInFilter($(lessonPlanFilter.groupSelect),0,$(lessonPlanFilter.levelSelect).val());
			$(lessonPlanFilter.groupSelect).multiselect("rebuild");
			$(lessonPlanFilter.groupSelectDiv).removeClass("hide");
		},
		sectionChange : function(){
			$(lessonPlanFilter.searchBtn).hide();
			$(lessonPlanFilter.subjectSelectDiv).show();
		},
		groupChange : function(){
			lessonPlanFilter.filterData();
			$(lessonPlanFilter.searchBoxDiv).show();
			$(lessonPlanFilter.searchBtn).show();
		},
		filterData : function(){
			var lessons = customAjaxCalling("shikshaplus/lessonplan/level/"+$(lessonPlanFilter.levelSelect).val()+"/group/"+$(lessonPlanFilter.groupSelect).val()+"?criteria="+$(lessonPlanFilter.searchBox).val().replace(/\s\s+/g, ' ').trim() ,null,"GET");
			$(lessonPlanFilter.planContainer).show();
			lessonPlanFilter.showLessonsList(lessons);	
			$(lessonPlanFilter.lessonDownload).show();
		},
		showLessonsList : function(chapters){
			$(createPlan.submitBtn).hide();
			$(lessonPlanFilter.planContainer).empty().show();
			$.each(chapters,function(index, chapter){
				var lpTemp = $(".lms-lesson-content-template").clone();
				$(lpTemp).find("a").attr("href","#lesson-content"+index);
				$(lpTemp).find("form").attr("id","lesson-content"+index);
				$(lpTemp).removeClass("lms-lesson-content-template").show().attr("data-chapterid",chapter.chapterId).attr("data-chapter-number",chapter.chapterNumber);
				$(lpTemp).find(".lesson-title").attr("data-title",chapter.name).text(chapter.chapterNumber+"-"+chapter.name);
				if(!index){
					$(lpTemp).find(".action-prerequisites").hide();
				}
				$(lessonPlanFilter.planContainer).append(lpTemp);
				
			});
			$('.panel').on('shown.bs.collapse', function () {
			   createPlan.showOpenLessonPlan($(this).attr("data-chapterid"));
			});
			$('.panel').on('hidden.bs.collapse', function () {
				$(this).find("form").empty();
				$(createPlan.submitBtn).hide();
				spinner.hideSpinner();
				   
			});
			$('.panel a').on('click', function () {
				   spinner.showSpinner();
			});
		},
		emptySelect : function(ele){
			$(ele).empty();
		},
		enableMultiSelectForFilters : function(element,nonSelectedText,onChangeCallback){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				nonSelectedText : nonSelectedText,
				onChange : onChangeCallback
			});	
		},
		fnGetSelectedFiteredValue : function(ele){
			var arr =[];
			var selector = "option" ;
			if($(ele).val()!=null)
				selector = "option:selected";
			$(ele).find(selector).each(function(ind,option){
				if($(option).val()!="multiselect-all"){		
					arr.push(parseInt($(option).val()));
				}
			});
			return arr;
		},
		fnAddDaysInView : function(lessonPlanDays){
			$('.view-days-continer').empty();
			$.each(lessonPlanDays,function(index,object){
				$('.view-days-continer').append('<div class="day'+(index+1)+' day1 container-fluid"><span>Day : '+object.day+'</span><p id="learningObjective">'+object.dayObjective+'</p>'+
												'<table class="col-xs-12 days-table'+(index+1)+' day-acitivity-container" ><tr><th>'+lessonCol.activity+'</th><th>'+lessonCol.duration+'</th><th>'+lessonCol.requiredResources+'</th><th>'+lessonCol.remarks+'</th></tr></table></div><br>');
				$.each(object.activities,function(ind,obj){
					var tr='<tr><td>'+obj.name+'</td><td>'+obj.duration+'</td><td>'+obj.resources+'</td><td>'+obj.activityDetails+'</td></tr>'
					$('.view-days-continer .days-table'+(index+1)+' tbody').append(tr);
				})
				
			})
		},
		fnSaveDraft : function(){
			var lessonplanDOM = $(".lesson-panel[data-chapterid='"+currentChapter+"']");
			var ajaxData={
					shikshaPlusChapter :{ },
					lessonPlanDays:[],
					deletedDays : deleteDay.deleteDayIds,
					deletedItemIds : deleteDayActivity.deletedActivityIds
				};

			ajaxData.shikshaPlusChapter.description=$(lessonplanDOM).find("#planObjective").val().trim();
			ajaxData.shikshaPlusChapter.duration=$(lessonplanDOM).find(".lesson-day").length;
			ajaxData.shikshaPlusChapter.name=$(lessonplanDOM).find(".lesson-title").attr("data-title");
			ajaxData.shikshaPlusChapter.chapterId=currentChapter;
			ajaxData.shikshaPlusChapter.chapterNumber=$(lessonplanDOM).attr('data-unit-number');
			
			ajaxData.shikshaPlusChapter.description=$.trim(ajaxData.shikshaPlusChapter.description.replace(/[\t\n]+/g,' '));
			$(lessonplanDOM).find(".lesson-day").each(function(index){
				var dayPlan = {
						day 			: index+1,
						dayObjective 	: $(this).find("#dayObjective").val().replace(/\s\s+/g, ' ').trim(),
						activities 		: [],
						sequence 		: index+1
				};
				if(parseInt($(this).attr("data-dayid"))){
					dayPlan.id			= $(this).attr("data-dayid");
				}
				$(this).find("#ativityTable tbody tr").each(function(x){
					var activity = {
							activityDetails : $(this).find("#activityDesc").val().replace(/\s\s+/g, ' ').trim(),
							resources 		: $(this).find("#activityResources").val().replace(/\s\s+/g, ' ').trim(),
							duration		: $(this).find("#activityDuration").val(),
							sequence		: x+1,
							segmentType		: {
								segmentTypeId	: $(this).find("#activityType").val(),
							}
					};
					if($(this).data("lessonplanitemid")){
						activity.shikshaLessonPlanItemId = $(this).data("lessonplanitemid");
					}
					
					if($(this).find("#activitySegment").val()){
						activity.shikshaPlusSegment = {
								shikshaPlusSegmentId:$(this).find("#activitySegment").val()
						};
					}
					dayPlan.activities.push(activity);
					
				});
				
				ajaxData.lessonPlanDays.push(dayPlan);
				
			});
			
			var saveType = "saveAsDraft";
			var callType = "POST";
			if($(lessonplanDOM).attr("data-isedit") == "true"){
				saveType = "updateDraft";
				callType = "PUT";
			}
			
			var updateCall = shiksha.invokeAjax("shikshaplus/lessonplan?action="+saveType,ajaxData,callType);
			spinner.hideSpinner();
			if(updateCall != null){
				if(updateCall.response == 'shiksha-200'){
					createPlan.showOpenLessonPlan(currentChapter);
					AJS.flag({
						type 	: "success",
						title 	: lessonCol.sucessAlert,
						body 	: updateCall.responseMessage,
						close 	: 'auto'
					});
				}else{
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.error,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		fnDiscardDraft : function(){
			var discardCall=shiksha.invokeAjax("shikshaplus/lessonplan/discard?chapter="+currentChapter,null,"DELETE");
			spinner.hideSpinner();
			if(discardCall != null){
				if(discardCall.response == 'shiksha-200'){
					createPlan.showOpenLessonPlan(currentChapter);
					AJS.flag({
						type 	: "success",
						title 	: lessonCol.sucessAlert,
						body 	: discardCall.responseMessage,
						close 	: 'auto'
					});
				}else{
					AJS.flag({
						type 	: "error",
						title 	: "Error!",
						body 	: discardCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Error!",
					body 	: "OOPs!!Server error.",
					close 	: 'auto'
				})
			}
		},
		fnSubmit : function(){
			lessonPlanFilter.validateForm();
			var lessonplanDOM = $(".lesson-panel[data-chapterid='"+currentChapter+"']");
			if($(lessonplanDOM).attr("data-isedit") == "true"){
				createPlan.fnUpdate();
				return false;
			}
			var ajaxData={
					shikshaPlusChapter :{ },
					lessonPlanDays:[],
				};
			var flag = true;
			ajaxData.shikshaPlusChapter.description=$(lessonplanDOM).find("#planObjective").val().trim();
			ajaxData.shikshaPlusChapter.duration=$(lessonplanDOM).find(".lesson-day").length;
			ajaxData.shikshaPlusChapter.name=$(lessonplanDOM).find(".lesson-title").attr("data-title");
			ajaxData.shikshaPlusChapter.chapterId=parseInt(currentChapter);
			ajaxData.shikshaPlusChapter.chapterNumber=parseInt($(lessonplanDOM).attr('data-chapter-number'));
			
			if(!ajaxData.shikshaPlusChapter.description){
				$(lessonplanDOM).find("#planObjective").addClass("lms-error");
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : lessonCol.planObjectiveRequired,
					close : 'auto'
				});
				return false;
			} else if(ajaxData.shikshaPlusChapter.description.trim().length > 5000){
				$(lessonplanDOM).find("#planObjective").addClass("lms-error");
				AJS.flag({
					type  : "error",
					title : "Error..!",
					body  : lessonCol.planObjectiveLength,
					close : 'auto'
				});
				return false;
			} else {
				$(lessonplanDOM).find("#planObjective").removeClass("lms-error");
			} 
			
			ajaxData.shikshaPlusChapter.description=$.trim(ajaxData.shikshaPlusChapter.description.replace(/[\t\n]+/g,' '));
			
			
				$(lessonplanDOM).find(".lesson-day").each(function(index){
					var dayPlan = {
							day : index+1,
							dayObjective 	: $(this).find("#dayObjective").val().replace(/\s\s+/g, ' ').trim(),
							activities 		: [],
							sequence 		: index+1
					};
					if(!dayPlan.dayObjective){
						$(this).find("#dayObjective").addClass("lms-error");
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  :  lessonCol.objectiveForDayRequired.replace("@DAY",dayPlan.day),
							close : 'auto'
						});
						flag=false;
						return false;
					} else if(dayPlan.dayObjective.trim().length > 5000){
						$(this).find("#dayObjective").addClass("lms-error");
						AJS.flag({
							type  : "error",
							title : "Error..!",
							body  : lessonCol.objectiveLengthForDay.replace("@DAY",dayPlan.day),
							close : 'auto'
						});
						flag=false;
						return false;
					} else {
						$(this).find("#dayObjective").removeClass("lms-error");
					}
					$(this).find("#ativityTable tbody tr").each(function(x){
						var activity = {
								activityDetails : $(this).find("#activityDesc").val().replace(/\s\s+/g, ' ').trim(),
								resources 		: $(this).find("#activityResources").val().replace(/\s\s+/g, ' ').trim(),
								duration		: $(this).find("#activityDuration").val(),
								sequence		: x+1,
								segmentType		: {
									segmentTypeId	: $(this).find("#activityType").val(),
								}
						};
						
                       //////////////////////////////////////////////
						
						if(!activity.activityDetails){
							$(this).find("#activityDesc").addClass("lms-error");
							AJS.flag({
								type  : "error",
								title : "Error..!",
								body  :lessonCol.instructionForDayRequired.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
								close : 'auto'
							});
							flag=false;
							return false
							
						} else if(activity.activityDetails.trim().length > 5000){
							$(this).find("#activityDesc").addClass("lms-error");
							AJS.flag({
								type  : "error",
								title : "Error..!",
								body  :lessonCol.instructionLengthForDay.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
								close : 'auto'
							});
							flag=false;
							return false
							
						} else {
							$(this).find("#activityDesc").removeClass("lms-error");
						}
                         /////////////////////////////
						
						if(!activity.duration){
							$(this).find("#activityDuration").addClass("lms-error");
	                           AJS.flag({
								type  : "error",
								title : "Error..!",
								body  :lessonCol.durationForDayRequired.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
								close : 'auto'
							});
							flag=false;
							return false
						
						}else{
							$(this).find("#activityDuration").removeClass("lms-error");
							
						}
						
						var regexp = /^([01][0-9]|[02][0-3]):[0-5][0-9]$/;
					    var correct = regexp.test(activity.duration);
					   
					    if(!correct){
					    	$(this).find("#activityDuration").addClass("lms-error");
					    	AJS.flag({
								type  : "error",
								title : "Error..!",
								body  :lessonCol.durationForDayInValid.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
								close : 'auto'
							});
							flag=false;
							return false
					    }
					    else{
					    	$(this).find("#activityDuration").removeClass("lms-error");
					    }
						////////////////////////////////////////
						if(!activity.resources){
							$(this).find("#activityResources").addClass("lms-error");
							AJS.flag({
								type  : "error",
								title : "Error..!",
								body  :lessonCol.resourcesForDayRequired.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
								close : 'auto'
							});
							flag=false;
							return false
							
						} else if(activity.resources.trim().length > 5000){
							$(this).find("#activityResources").addClass("lms-error");
							AJS.flag({
								type  : "error",
								title : "Error..!",
								body  :lessonCol.resourcesLengthForDay.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
								close : 'auto'
							});
							flag=false;
							return false
							
						} else {
							$(this).find("#activityResources").removeClass("lms-error");
						}
						
						 if($(this).find("#activitySegment").val()){
							activity.shikshaPlusSegment = {
									shikshaPlusSegmentId :$(this).find("#activitySegment").val()
							};
						}
						dayPlan.activities.push(activity);
						
					});
					if(!flag)
						return false;
					
					ajaxData.lessonPlanDays.push(dayPlan);
					
				});
				
				if(!flag)
					return false;
				
				var addCall=shiksha.invokeAjax("shikshaplus/lessonplan?action=submit",ajaxData,"POST");
				spinner.hideSpinner();
				if(addCall != null){
					if(addCall.response == 'shiksha-200'){
						createPlan.showOpenLessonPlan(currentChapter);
						AJS.flag({
							type 	: "success",
							title 	: lessonCol.sucessAlert,
							body 	: addCall.responseMessage,
							close 	: 'auto'
						});
					}else{
						AJS.flag({
							type 	: "error",
							title 	: appMessgaes.error,
							body 	: addCall.responseMessage,
							close 	: 'auto'
						})
					}
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: appMessgaes.serverError,
						close 	: 'auto'
					})
				}
		},
		validateForm : function(formId){

			var lessonplanDOM = $(".lesson-panel[data-chapterid='"+currentChapter+"']");
			var ajaxData={
					unit :{ },
					lessonPlanDays:[],
				};
			var flag = true;
			ajaxData.unit.unitDescription=$(lessonplanDOM).find("#planObjective").val().trim();
			ajaxData.unit.unitDuration=$(lessonplanDOM).find(".lesson-day").length;
			ajaxData.unit.unitName=$(lessonplanDOM).find(".lesson-title").attr("data-title");
			ajaxData.unit.unitId=currentChapter;
			ajaxData.unit.unitNumber=$(lessonplanDOM).attr('data-unit-number');
			
			if(!ajaxData.unit.unitDescription){
				$(lessonplanDOM).find("#planObjective").addClass("lms-error");
				
			} else if(ajaxData.unit.unitDescription.trim().length > 5000){
				$(lessonplanDOM).find("#planObjective").addClass("lms-error");
				
			} else {
				$(lessonplanDOM).find("#planObjective").removeClass("lms-error");
			}
			ajaxData.unit.unitDescription=$.trim(ajaxData.unit.unitDescription.replace(/[\t\n]+/g,' '));
				$(lessonplanDOM).find(".lesson-day").each(function(index){
					var dayPlan = {
							day 			: index+1,
							dayObjective 	: $(this).find("#dayObjective").val().replace(/\s\s+/g, ' ').trim(),
							activities 		: [],
							sequence 		: index+1
					};
					if(parseInt($(this).attr("data-dayid"))){
						dayPlan.id			= $(this).attr("data-dayid");
					}
					if(!dayPlan.dayObjective){
						$(this).find("#dayObjective").addClass("lms-error");
						
					} else if(dayPlan.dayObjective.trim().length > 5000){
						$(this).find("#dayObjective").addClass("lms-error");
						
					} else {
						$(this).find("#dayObjective").removeClass("lms-error");
					}
					
					$(this).find("#ativityTable tbody tr").each(function(x){
						var activity = {
								activityDetails : $(this).find("#activityDesc").val().replace(/\s\s+/g, ' ').trim(),
								resources 		: $(this).find("#activityResources").val().replace(/\s\s+/g, ' ').trim(),
								duration		: $(this).find("#activityDuration").val(),
								sequence		: x+1,
								segmentType		: {
									segmentTypeId	: $(this).find("#activityType").val(),
								}
						};
						if($(this).data("lessonplanitemid")){
							activity.shikshaLessonPlanItemId = $(this).data("lessonplanitemid");
						}
						//////////////////////////////////////////////
						
						if(!activity.activityDetails){
							$(this).find("#activityDesc").addClass("lms-error");
							
							
						} else if(activity.activityDetails.trim().length > 5000){
							$(this).find("#activityDesc").addClass("lms-error");
							
							
						} else {
							$(this).find("#activityDesc").removeClass("lms-error");
						}
	                   /////////////////////////////
						
						if(!activity.duration){
							$(this).find("#activityDuration").addClass("lms-error");
	                           
						
						}else{
							$(this).find("#activityDuration").removeClass("lms-error");
							
						}
						
						var regexp = /^([01][0-9]|[02][0-3]):[0-5][0-9]$/;
					    var correct = regexp.test(activity.duration);
					   
					    if(!correct){
					    	$(this).find("#activityDuration").addClass("lms-error");
					    	
					    }
					    else{
					    	$(this).find("#activityDuration").removeClass("lms-error");
					    }
						////////////////////////////////////////
						
						if(!activity.resources){
							$(this).find("#activityResources").addClass("lms-error");
							
						} else if(activity.resources.trim().length > 5000){
							$(this).find("#activityResources").addClass("lms-error");
							
						} else {
							$(this).find("#activityResources").removeClass("lms-error");
						}
						
					     
						
						if($(this).find("#activitySegment").val()){
							activity.segment = {
								segmentId:$(this).find("#activitySegment").val()
							};
						}
						dayPlan.activities.push(activity);
						
					});
					if(!flag)
						return false;
					
					ajaxData.lessonPlanDays.push(dayPlan);
					
				});
			
			
		
		
		
	
		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					objective: {
						required: true,
						noSpace:true,
					},
					durationDays: {
						required: true,
						noSpace:true,
					},
					learnobjective:{
						required: true,
						noSpace:true,
					},
					
				},

				messages: {
					objective: {
						required: lessonCol.objectiveRequired,
						noSpace:lessonCol.objectiveRequired,
					},
					durationDays: {
						required:lessonCol.daysRequired,
						noSpace:"",
					},
					learnobjective:{
						required: "",
						noSpace: "",
					},
				},
				submitHandler : function(){
					if(validateFormName == '.create-lesson-form')
					{
						lessonPlanFilter.fnSubmit();
					}
					else{
						createPlan.fnUpdate();
					}
					
				}
			});
		},
}

var createPlan = {
	durationDaysSelect : "#durationDays",
	submitBtn		: ".lms-submit",
	lessonTitle		: '#lessonTitle',
	objective		: '#objective',
	addDaysBtn		: "#addDaysBtn",
	noOfDays		: "#noOfDays",
	addDaysModal 	: "#mdlAddDays",
	init : function(){
		$(document).on("click",".add-activity",this.addNewActivity);
		$(document).on("click",".action-add-day",this.addNewDayInit);
		$(document).on("click",this.addDaysBtn,this.addNewEditDay);
		$(document).on("click",".lesson-plan-submit",lessonPlanFilter.fnSubmit);
		$(document).on("click",".lesson-plan-draft",lessonPlanFilter.fnSaveDraft);
		$(document).on("click",".discard-draft",lessonPlanFilter.fnDiscardDraft);
		$(document).on("click",".action-edit-plan",this.initEdit);
		$(document).on("change","#activityType",this.bindSegmentsByType);
		$(document).on("change","#activitySegment",this.bindSegmentSeq);
	},
	bindSegmentsByType : function(){
		var	segments = shiksha.invokeAjax("shikshaplus/segment/chapter/"+currentChapter+"/segmenttype/"+$(this).val(), null, "GET");
		var segmentStr = "<option value=''>Select Segment</option>";
		$.each(segments,function(k,segment){
			segmentStr += "<option value='"+segment.shikshaPlusSegmentId+"' data-sequence='"+segment.sequence+"'>"+segment.shikshaPlusSegmentName+"</option>"
		});		
		$(this).closest("tr").find("#activitySegment").empty().append(segmentStr);
		$(this).closest("tr").find("#segmentSeq").val("");
		spinner.hideSpinner();
	},
	bindSegmentSeq :function(){
		var segmentSeq = $(this).find("option:selected").attr("data-sequence")? $(this).find("option:selected").attr("data-sequence") : "";
		$(this).closest("tr").find("#segmentSeq").val(segmentSeq).text(segmentSeq);
	},
	addNewActivity : function(){
		spinner.showSpinner();
		var unitId = currentChapter;
		var newActivityTemp = $(this).parent().find("#ativityTable tbody tr:first-child").clone().removeAttr("data-lessonplanitemid");
		var	segmentTypes = shiksha.invokeAjax("segment/type?exclude=0", null, "GET");
		var segmentTypeStr = "";
		$.each(segmentTypes,function(k,segment){
			segmentTypeStr += "<option value='"+segment.segmentTypeId+"'>"+segment.segmentTypeName+"</option>"
		});
		
		var	segments = shiksha.invokeAjax("shikshaplus/segment/chapter/"+currentChapter+"/segmenttype/"+segmentTypes[0]['segmentTypeId'], null, "GET");
		var segmentStr = "<option value=''>Select Segment</option>";
		$.each(segments,function(k,segment){
			segmentStr += "<option value='"+segment.shikshaPlusSegmentId+"' data-sequence='"+segment.sequence+"'>"+segment.shikshaPlusSegmentName+"</option>"
		});
		$(newActivityTemp).find("#activityType").empty().append(segmentTypeStr);
		$(newActivityTemp).find("#activitySegment").empty().append(segmentStr);
		$(newActivityTemp).find("#activityDesc").val("");
		$(newActivityTemp).find("#activityDuration").val("");
		$(newActivityTemp).find("#activityResources").val("");
		$(newActivityTemp).find("#segmentSeq").val("");
		$(newActivityTemp).find(".delete-activity").css("display","table-cell");
		$(this).parent().find("#ativityTable tbody").append(newActivityTemp);
		createPlan.enableActivitiesDragDrop($(this).closest(".lesson-day").attr("data-dayid"));
		spinner.hideSpinner();
	},
	addNewDayInit : function(e){
		$(createPlan.noOfDays).val(1);
		e.stopPropagation();
		e.preventDefault();
		spinner.hideSpinner();
		$(createPlan.addDaysModal).modal("show");
	},
	addNewEditDay : function(e){
		/*e.stopPropagation();
		e.preventDefault();*/
		var noOfDaysAdd = $(createPlan.noOfDays).val();
		var unitId = currentChapter;
		var flag = false;
		if($(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lesson-day").length == 30){
			spinner.hideSpinner();
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : lessonCol.days30Required,
				close : 'auto'
			});
			return false;
		} else {
			var totalDays = $(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lesson-day").length;
			var maxDays = 30 - totalDays;
			if(maxDays < noOfDaysAdd){
				noOfDaysAdd = maxDays;
				flag = true;
			}
		}
		spinner.showSpinner();
		var	segmentTypes = shiksha.invokeAjax("segment/type?exclude=0", null, "GET");
		var segmentTypeStr = "";
		$.each(segmentTypes,function(k,segment){
			segmentTypeStr += "<option value='"+segment.segmentTypeId+"'>"+segment.segmentTypeName+"</option>"
		});
		
		var	segments = shiksha.invokeAjax("shikshaplus/segment/chapter/"+currentChapter+"/segmenttype/"+segmentTypes[0]['segmentTypeId'], null, "GET");
		var segmentStr = "<option value=''>Select Segment</option>";
		$.each(segments,function(k,segment){
			segmentStr += "<option value='"+segment.shikshaPlusSegmentId+"' data-sequence='"+segment.sequence+"'	>"+segment.shikshaPlusSegmentName+"</option>"
		});
		while(noOfDaysAdd --){
			var dayEditTemplate = $(".lesson-day-edit-template").clone().removeClass("lesson-day-edit-template").attr("data-dayid",createPlan.randomStr()).show();
			$(dayEditTemplate).find("#dayNo").text($(".lesson-panel[data-chapterid='"+unitId+"'] form").find(".lesson-day").length + 1);
			$(dayEditTemplate).find("#ativityTable tbody tr:first-child").find("#activityType").append(segmentTypeStr);
			$(dayEditTemplate).find("#ativityTable tbody tr:first-child").find("#activitySegment").append(segmentStr);
			$(".lesson-panel[data-chapterid='"+unitId+"'] form").append(dayEditTemplate);
		}
		
		var noOfDays = "1 "+lessonCol.day;
		if($(".lesson-panel[data-chapterid='"+unitId+"'] form .lesson-day").length > 1){
			noOfDays = $(".lesson-panel[data-chapterid='"+unitId+"'] form .lesson-day").length+" "+lessonCol.days;
		}
		$(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lms-lesson-days").text(noOfDays);
		
		$(createPlan.addDaysModal).modal("hide");
		$(createPlan.submitBtn).not(".discard-draft").show();
		//$(".discard-draft").hide();
		spinner.hideSpinner();
		if(flag){
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : lessonCol.days30Required,
				close : 'auto'
			});
		}	
		
	}, 
	showOpenLessonPlan : function(chapterId){
		deleteDay.deleteDayIds = [];
		deleteDayActivity.deletedActivityIds = [];
		currentChapter = chapterId;
		$(".lesson-panel[data-chapterid='"+chapterId+"'] form").empty();
		var lessonplan = customAjaxCalling("shikshaplus/lessonplan/?chapter="+chapterId,null,"GET");
		var daysText = "1  "+lessonCol.day;
		if(lessonplan.noOfDays > 1){
			daysText = lessonplan.noOfDays + " "+lessonCol.days;
		}
		$(".lesson-panel[data-chapterid='"+chapterId+"']").find(".lms-lesson-days").text(daysText);
		$(".lesson-panel[data-chapterid='"+chapterId+"']").find("#noOfPrerequisites").text(lessonplan.noOfPrerequisites);
		if(lessonplan.lessonPlanDays.length >0){
			$(createPlan.submitBtn).hide();
			$(".lesson-panel[data-chapterid='"+chapterId+"']").find(".action-add-day").hide();
			$(".lesson-panel[data-chapterid='"+chapterId+"']").find(".action-delete-plan").show();
			$(".lesson-panel[data-chapterid='"+chapterId+"']").find(".action-edit-plan").show();
			$(".lesson-panel[data-chapterid='"+currentChapter+"']").attr("data-isedit","true");
			$(".lesson-panel[data-chapterid='"+chapterId+"'] form").append($(".lesson-obj-view-template").clone().removeClass("lesson-obj-view-template").show());
			$(".lesson-panel[data-chapterid='"+chapterId+"'] form").find("#planObjective").text(lessonplan.shikshaPlusChapter.description);
			$.each(lessonplan.lessonPlanDays,function(i,dayObj){
				var datTemp = $(".lesson-day-view-template").clone().removeClass("lesson-day-view-template").show().attr("data-dayid",dayObj.id);
				$(datTemp).find("#dayNo").text(i+1);
				$(datTemp).find("#dayObjective").text(dayObj.dayObjective);
				$.each(dayObj.activities,function(j,activitybj){
					
					var segmentObj = {shikshaPlusSegmentId : "", sequence : "",shikshaPlusSegmentName : ""};
					var segmentStr = "<option value=''>Select Segment</option>";
					$.each(activitybj.segmentDetails.segments,function(k,segment){
						if(activitybj.segmentDetails.selectedSegment == segment.shikshaPlusSegmentId)
							segmentObj = segment;
						segmentStr += "<option value='"+segment.shikshaPlusSegmentId+"' data-sequence='"+segment.sequence+"'>"+segment.shikshaPlusSegmentName+"</option>"
					});
					
					var segmentTypeObj = {segmentTypeName : ""};
					
					$.each(activitybj.segmentDetails.segmentTypes,function(k,segmentType){
						if(activitybj.segmentDetails.selectedSegmentType == segmentType.segmentTypeId){
							segmentTypeObj = segmentType;
							return false;
						}
					});
					
					if(j){
						var newActivity = $(datTemp).find("#ativityTable tbody tr:first-child").clone();
						$(newActivity).attr("data-lessonplanitemid",activitybj.shikshaLessonPlanItemId);
						$(newActivity).find("#activityDuration").text(activitybj.duration);
						$(newActivity).find("#activityResources").text(activitybj.resources);
						$(newActivity).find("#activityType").text(segmentTypeObj.segmentTypeName);
						$(newActivity).find("#activitySegment").text(segmentObj.shikshaPlusSegmentName);
						$(newActivity).find("#activityDesc").text(activitybj.activityDetails);
						$(newActivity).find("#segmentSeq").text(segmentObj.sequence);
						$(datTemp).find("#ativityTable tbody").append(newActivity);
					} else {
						
						$(datTemp).find("#ativityTable tbody tr:first-child").attr("data-lessonplanitemid",activitybj.shikshaLessonPlanItemId);
						$(datTemp).find("#ativityTable tbody tr:first-child").find("#activityDuration").text(activitybj.duration);
						$(datTemp).find("#ativityTable tbody tr:first-child").find("#activityResources").text(activitybj.resources);
						$(datTemp).find("#ativityTable tbody tr:first-child").find("#activityType").text(segmentTypeObj.segmentTypeName);
						$(datTemp).find("#ativityTable tbody tr:first-child").find("#activitySegment").text(segmentObj.shikshaPlusSegmentName);
						$(datTemp).find("#ativityTable tbody tr:first-child").find("#activityDesc").text(activitybj.activityDetails);
						$(datTemp).find("#ativityTable tbody tr:first-child").find("#segmentSeq").text(segmentObj.sequence);
					}
				});
				$(datTemp).find("#segmentCount").text(dayObj.noOfSegments);
				$(".lesson-panel[data-chapterid='"+chapterId+"'] form").append(datTemp);
			});
			createPlan.highlightSearchText();
		} else {
			$(".lesson-panel[data-chapterid='"+chapterId+"']").find(".action-add-day").removeClass('lms-disabled').show();
			$(".lesson-panel[data-chapterid='"+chapterId+"']").find(".action-edit-plan").hide();
			$(".lesson-panel[data-chapterid='"+chapterId+"']").find(".action-delete-plan").hide();
			$(".lesson-panel[data-chapterid='"+currentChapter+"']").attr("data-isedit","false");
			var	segmentTypes = shiksha.invokeAjax("segment/type?exclude=0", null, "GET");
			var segmentTypeStr = "";
			$.each(segmentTypes,function(k,segment){
				segmentTypeStr += "<option value='"+segment.segmentTypeId+"'>"+segment.segmentTypeName+"</option>"
			});
			
			var	segments = shiksha.invokeAjax("shikshaplus/segment/chapter/"+currentChapter+"/segmenttype/"+segmentTypes[0]['segmentTypeId'], null, "GET");
			var segmentStr = "<option value=''>Select Segment</option>";
			$.each(segments,function(k,segment){
				segmentStr += "<option value='"+segment.shikshaPlusSegmentId+"'  data-sequence='"+segment.sequence+"'>"+segment.shikshaPlusSegmentName+"</option>"
			});
			
			$(".lesson-panel[data-chapterid='"+chapterId+"'] form").append($(".lesson-obj-edit-template").clone().removeClass("lesson-obj-edit-template").show());
			$(".lesson-panel[data-chapterid='"+chapterId+"'] form").find("#planObjective").val(lessonplan.shikshaPlusChapter.description);
			$(".lesson-panel[data-chapterid='"+chapterId+"'] form").append($(".lesson-day-edit-template").clone().removeClass("lesson-day-edit-template").attr("data-dayid",createPlan.randomStr()).show());
			$(".lesson-panel[data-chapterid='"+chapterId+"'] form").find("#ativityTable tbody tr:first-child").find("#activityType").append(segmentTypeStr);
			$(".lesson-panel[data-chapterid='"+chapterId+"'] form").find("#ativityTable tbody tr:first-child").find("#activitySegment").append(segmentStr);
			$(createPlan.submitBtn).show();
			$(".discard-draft").hide()
			
		}
	},
	highlightSearchText : function(){
		var $context = $(".lms-lesson-heading-section:not(.collapsed)").parent();
		var searchText = $(lessonPlanFilter.searchBox).val();
		$context.unmark({
	      done: function() {
	    	  $context.mark(searchText, {});
	      }
	    });
	},
	checkTimeFormat :function(){
		$('.timeformat').on('input',function(e) {
			var regexp = /^([01][0-9]|[02][0-3]):[0-5][0-9]$/;
		    var correct = regexp.test($(this).val());
		    if(!correct || !$(this).val()){
		    	$(this).addClass("focusedElement");
		    }
		    else{
		    	$(this).removeClass("focusedElement");
		    }
		});
		
	},
	initEdit: function(e){
		deleteDay.deleteDayIds = [];
		deleteDayActivity.deletedActivityIds = [];
		e.preventDefault();
		e.stopPropagation();
		$(".lms-lesson-heading-section:not(.collapsed)").parent().unmark();
		$(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".action-edit-plan").hide();
		$(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".action-add-day").show();//.addClass("lms-disabled");
		$(createPlan.submitBtn).show();
		spinner.showSpinner();
		var lessonplan = customAjaxCalling("shikshaplus/lessonplan/?chapter="+currentChapter,null,"GET");
		$(".lesson-panel[data-chapterid='"+currentChapter+"'] form").empty();
		var lessonPlanForm = $(".lesson-panel[data-chapterid='"+currentChapter+"'] form");
		$(lessonPlanForm).append($(".lesson-obj-edit-template").clone().removeClass("lesson-obj-edit-template").show());
		$(lessonPlanForm).find("#planObjective").val(lessonplan.shikshaPlusChapter.description);
		if(!lessonplan.isDraft){
			$(".discard-draft").hide();
		}
		var	segmentTypes = shiksha.invokeAjax("segment/type?exclude=0", null, "GET");
		var segmentTypeStr = "";
		$.each(segmentTypes,function(k,segment){
			segmentTypeStr += "<option value='"+segment.segmentTypeId+"'>"+segment.segmentTypeName+"</option>"
		});
		
		/*var	segments = shiksha.invokeAjax("shikshaplus/segment/chapter/"+currentChapter+"/segmenttype/"+segmentTypes[0]['segmentTypeId'], null, "GET");
		var segmentStr = "<option value=''>Select Segment</option>";
		$.each(segments,function(k,segment){
			segmentStr += "<option value='"+segment.shikshaPlusSegmentId+"'>"+segment.shikshaPlusSegmentName+"</option>";
		});*/
		var noOfDays = "1 "+lessonCol.day;
		if(lessonplan.lessonPlanDays.length > 1){
			noOfDays = lessonplan.lessonPlanDays.length+" "+lessonCol.days;
		}
		$(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lms-lesson-days").text(noOfDays);
		$.each(lessonplan.lessonPlanDays,function(p,dayObj){
			var dayTemplate = $(".lesson-day-edit-template").clone().removeClass("lesson-day-edit-template").show();
			$(dayTemplate).attr("data-dayid",dayObj.id).find("#dayObjective").val(dayObj.dayObjective);
			$(dayTemplate).find("#dayNo").text(dayObj.day);
			$.each(dayObj.activities,function(q,activityObj){
				
				var segmentObj = {shikshaPlusSegmentId : "", sequence : "",shikshaPlusSegmentName : ""};
				var segmentStr = "<option value=''>Select Segment</option>";
				$.each(activityObj.segmentDetails.segments,function(k,segment){
					if(activityObj.segmentDetails.selectedSegment == segment.shikshaPlusSegmentId)
						segmentObj = segment;
					segmentStr += "<option value='"+segment.shikshaPlusSegmentId+"' data-sequence='"+segment.sequence+"'>"+segment.shikshaPlusSegmentName+"</option>"
				});
								
				if(q){
					var acitivtyTemplate =  $(dayTemplate).find("#ativityTable tbody tr:first-child").clone().attr("data-lessonplanitemid",activityObj.shikshaLessonPlanItemId);
					
					$(acitivtyTemplate).attr("data-lessonplanitemid",activityObj.shikshaLessonPlanItemId).find("#activityType").empty().append(segmentTypeStr);				
					
					$(acitivtyTemplate).find("#activitySegment").empty().append(segmentStr);
					
					$(acitivtyTemplate).find("#activitySegment").val(activityObj.segmentDetails.selectedSegment);
					$(acitivtyTemplate).find("#activityType").val(activityObj.segmentDetails.selectedSegmentType);
					$(acitivtyTemplate).find("#segmentSeq").val(segmentObj.sequence);
					$(acitivtyTemplate).find("#activityDuration").val(activityObj.duration);
					$(acitivtyTemplate).find("#activityResources").val(activityObj.resources);
					$(acitivtyTemplate).find("#activityDesc").val(activityObj.activityDetails);
					$(dayTemplate).find("#ativityTable tbody").append(acitivtyTemplate);
				} else {
					$(dayTemplate).find("#ativityTable tbody tr:first-child").attr("data-lessonplanitemid",activityObj.shikshaLessonPlanItemId).find("#activityType").empty().append(segmentTypeStr);
					
					$(dayTemplate).find("#ativityTable tbody tr:first-child").find("#activitySegment").empty().append(segmentStr);
					$(dayTemplate).find("#ativityTable tbody tr:first-child").find("#activitySegment").val(segmentObj.shikshaPlusSegmentId);
					$(dayTemplate).find("#ativityTable tbody tr:first-child").find("#activityType").val(activityObj.segmentDetails.selectedSegmentType);
					$(dayTemplate).find("#ativityTable tbody tr:first-child").find("#activityDuration").val(activityObj.duration);
					$(dayTemplate).find("#ativityTable tbody tr:first-child").find("#activityResources").val(activityObj.resources);
					$(dayTemplate).find("#ativityTable tbody tr:first-child").find("#activityDesc").val(activityObj.activityDetails);
					$(dayTemplate).find("#ativityTable tbody tr:first-child").find("#segmentSeq").val(segmentObj.sequence);
					
					if(lessonplan.isTeachingPlanExists)
						$(dayTemplate).find("#ativityTable tbody tr:first-child").find(".delete-activity").hide();
				}
			});
			$(".lesson-panel[data-chapterid='"+currentChapter+"'] form").append(dayTemplate);
			createPlan.enableActivitiesDragDrop(dayObj.id);
		});
		spinner.hideSpinner();		
	},
	fnUpdate:function(){
		var lessonplanDOM = $(".lesson-panel[data-chapterid='"+currentChapter+"']");
		var ajaxData={
				shikshaPlusChapter :{ },
				lessonPlanDays:[],
				deletedDays : deleteDay.deleteDayIds,
				deletedItemIds : deleteDayActivity.deletedActivityIds
			};
		var flag = true;
		ajaxData.shikshaPlusChapter.description=$(lessonplanDOM).find("#planObjective").val().trim();
		ajaxData.shikshaPlusChapter.duration=$(lessonplanDOM).find(".lesson-day").length;
		ajaxData.shikshaPlusChapter.name=$(lessonplanDOM).find(".lesson-title").attr("data-title");
		ajaxData.shikshaPlusChapter.chapterId=currentChapter;
		ajaxData.shikshaPlusChapter.chapterNumber=$(lessonplanDOM).attr('data-unit-number');
		
		if(!ajaxData.shikshaPlusChapter.description){
			$(lessonplanDOM).find("#planObjective").addClass("lms-error");
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : lessonCol.planObjectiveRequired,
				close : 'auto'
			});
			return false;
		} else if(ajaxData.shikshaPlusChapter.description.trim().length > 5000){
			$(lessonplanDOM).find("#planObjective").addClass("lms-error");
			AJS.flag({
				type  : "error",
				title : "Error..!",
				body  : lessonCol.planObjectiveLength,
				close : 'auto'
			});
			return false;
		} else {
			$(lessonplanDOM).find("#planObjective").removeClass("lms-error");
		}
		ajaxData.shikshaPlusChapter.description=$.trim(ajaxData.shikshaPlusChapter.description.replace(/[\t\n]+/g,' '));
			$(lessonplanDOM).find(".lesson-day").each(function(index){
				var dayPlan = {
						day 			: index+1,
						dayObjective 	: $(this).find("#dayObjective").val().replace(/\s\s+/g, ' ').trim(),
						activities 		: [],
						sequence 		: index+1
				};
				if(parseInt($(this).attr("data-dayid"))){
					dayPlan.id			= $(this).attr("data-dayid");
				}
				if(!dayPlan.dayObjective){
					$(this).find("#dayObjective").addClass("lms-error");
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : lessonCol.objectiveForDayRequired.replace("@DAY",dayPlan.day),
						close : 'auto'
					});
					flag=false;
					return false;
				} else if(dayPlan.dayObjective.trim().length > 5000){
					$(this).find("#dayObjective").addClass("lms-error");
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : lessonCol.objectiveLengthForDay.replace("@DAY",dayPlan.day),
						close : 'auto'
					});
					flag=false;
					return false;
				} else {
					$(this).find("#dayObjective").removeClass("lms-error");
				}
				$(this).find("#ativityTable tbody tr").each(function(x){
					var activity = {
							activityDetails : $(this).find("#activityDesc").val().replace(/\s\s+/g, ' ').trim(),
							resources 		: $(this).find("#activityResources").val().replace(/\s\s+/g, ' ').trim(),
							duration		: $(this).find("#activityDuration").val(),
							sequence		: x+1,
							segmentType		: {
								segmentTypeId	: $(this).find("#activityType").val(),
							}
					};
					if($(this).data("lessonplanitemid")){
						activity.shikshaLessonPlanItemId = $(this).data("lessonplanitemid");
					}
					
					 //////////////////////////////////////////////
					
					if(!activity.activityDetails){
						$(this).find("#activityDesc").addClass("lms-error");
						AJS.flag({
							type  : "error",
							title : "Error..!",
							body  :lessonCol.instructionForDayRequired.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
							close : 'auto'
						});
						flag=false;
						return false
						
					} else if(activity.activityDetails.trim().trim().length > 5000){
						$(this).find("#activityDesc").addClass("lms-error");
						AJS.flag({
							type  : "error",
							title : "Error..!",
							body  :lessonCol.instructionLengthForDay.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
							close : 'auto'
						});
						flag=false;
						return false
						
					} else {
						$(this).find("#activityDesc").removeClass("lms-error");
					}
					////////////////////////////////////////

					
					if(!activity.duration){
						$(this).find("#activityDuration").addClass("lms-error");
                           AJS.flag({
							type  : "error",
							title : "Error..!",
							body  :lessonCol.durationForDayRequired.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
							close : 'auto'
						});
						flag=false;
						return false
					
					}else{
						$(this).find("#activityDuration").removeClass("lms-error");
						
					}
					
					var regexp = /^([01][0-9]|[02][0-3]):[0-5][0-9]$/;
				    var correct = regexp.test(activity.duration);
				   
				    if(!correct){
				    	$(this).find("#activityDuration").addClass("lms-error");
				    	AJS.flag({
							type  : "error",
							title : "Error..!",
							body  :lessonCol.durationForDayInValid.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
							close : 'auto'
						});
						flag=false;
						return false
				    }
				    else{
				    	$(this).find("#activityDuration").removeClass("lms-error");
				    }
				    /////////////////////////////////////////////////////////
				    
					if(!activity.resources){
						$(this).find("#activityResources").addClass("lms-error");
						AJS.flag({
							type  : "error",
							title : "Error..!",
							body  :lessonCol.resourcesForDayRequired.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
							close : 'auto'
						});
						flag=false;
						return false
						
					} else if(activity.resources.trim().length > 5000){
						$(this).find("#activityResources").addClass("lms-error");
						AJS.flag({
							type  : "error",
							title : "Error..!",
							body  :lessonCol.resourcesLengthForDay.replace("@DAY",dayPlan.day).replace("@ACTIVITY",(x+1)),
							close : 'auto'
						});
						flag=false;
						return false
						
					} else {
						$(this).find("#activityResources").removeClass("lms-error");
					}
					
					
					if($(this).find("#activitySegment").val()){
						activity.shikshaPlusSegment = {
								shikshaPlusSegmentId:$(this).find("#activitySegment").val()
						};
					}
					dayPlan.activities.push(activity);
					
				});
				if(!flag)
					return false;
				
				ajaxData.lessonPlanDays.push(dayPlan);
				
			});
			
			if(!flag)
				return false;
			
			var updateCall = shiksha.invokeAjax("shikshaplus/lessonplan?action=submit",ajaxData,"PUT");
			spinner.hideSpinner();
			if(updateCall != null){
				if(updateCall.response == 'shiksha-200'){
					createPlan.showOpenLessonPlan(currentChapter);
					AJS.flag({
						type 	: "success",
						title 	: lessonCol.sucessAlert,
						body 	: updateCall.responseMessage,
						close 	: 'auto'
					});
				}else{
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.error,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
	},
	enableActivitiesDragDrop : function(dayId){
		$( "div[data-dayid='"+dayId+"'] tbody" ).sortable({
			  axis: "y",
			  helper: "clone",
			  containment: "div[data-dayid='"+dayId+"']",
			  items: "> tr",
			  handle : ".move-activity",
			  forcePlaceholderSize: true,
			  placeholder: "ui-sortable-placeholder",
			  revert: false,
			  stop: function( event, ui ) {}
		});
	},
	randomStr : function() {
		  var text = "";
		  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		  for (var i = 0; i < 5; i++)
		    text += possible.charAt(Math.floor(Math.random() * possible.length));

		  return text;
	}
};

var deleteLessonPlan  = {
		deleleteLessonPlantBtn 	: ".action-delete-plan",
		deleteLessonPlanModal 	: "#mdlDeleteLessonPlan",
		deleteLessonPlanConfirmBtn 	: "#deleteLessonPlanButton",
		deleteLessonPlanId : "#deleteLessonPlanId",
		init : function(){
			$(document).on("click",".action-delete-plan",this.initDelete);
			$(document).on("click","#deleteLessonPlanButton",this.fnDeleteLessonPlan);
		},
		initDelete : function(e){
			e.preventDefault();
			e.stopPropagation();
			var lessonPlanId=currentChapter;
			var name = $(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lesson-title").attr("data-title");
			var msg = $("#deleteLessonPlanMessage").data("message");
			$("#deleteLessonPlanMessage").html(msg.replace('@NAME@',name));//+"<p style='margin-top: 5px;'>"+"<str class = 'lp-delete-warning'>NOTE</code>:"+lessonCol.noDraft+"</p>");
			$(deleteLessonPlan.deleteLessonPlanId).val(lessonPlanId);
			$(deleteLessonPlan.deleteLessonPlanModal).modal("show");
			spinner.hideSpinner();
		},
		fnDeleteLessonPlan : function(){
			spinner.showSpinner();
			
			var deleteLessonPlanId = $(deleteLessonPlan.deleteLessonPlanId).val();
			
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/lessonplan/?chapter="+deleteLessonPlanId, null, "DELETE");
			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-200"){
					createPlan.showOpenLessonPlan(currentChapter);
					$(deleteLessonPlan.deleteLessonPlanModal).modal("hide");
					AJS.flag({
						type  : "success",
						title : lessonCol.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};


var deleteDay  = {
		deleleteDaytBtn 	: ".action-delete-day",
		deleteDayModal 	: "#mdlDeleteDay",
		deleteDayConfirmBtn 	: "#deleteDayButton",
		deleteDayId : "#deleteDayId",
		deleteDayIds:[],
		
		init : function(){
			$(document).on("click",this.deleleteDaytBtn,this.initDelete);
			$(this.deleteDayConfirmBtn).on("click",this.fnDeleteDay);
		},
		initDelete : function(){
			if($(this).parents("form").find(".lesson-day").length < 2){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : lessonCol.day1Required,
					close : 'auto'
				});
				return false;
			}
			var dayId=$(this).parents(".lesson-day").data("dayid");
			
			if(!parseInt(dayId)){
				$(this).parents(".lesson-day").remove();
				var noOfDays = "1 "+lessonCol.day;
				if($(".lesson-panel[data-chapterid='"+currentChapter+"'] form .lesson-day").length > 1){
					noOfDays = $(".lesson-panel[data-chapterid='"+currentChapter+"'] form .lesson-day").length+" "+lessonCol.days;
				}
				$(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lms-lesson-days").text(noOfDays);
				$(".lesson-panel[data-chapterid='"+currentChapter+"'] .lesson-day").each(function(index){
					$(this).find("#dayNo").text(index+1);
				});
				return false;
			}
			
			var name = $(this).parents(".lesson-day").find(".day-title").text();
			var msg = $("#deleteDayMessage").data("message");
			$("#deleteDayMessage").html(msg.replace('@NAME@',name));
			$(deleteDay.deleteDayId).val(dayId);
			$(deleteDay.deleteDayModal).modal("show");
		},
		fnDeleteDay : function(){
			spinner.showSpinner();
			
			var deleteDayId = $(deleteDay.deleteDayId).val();
			/*deleteDay.deleteDayIds.push(deleteDayId);
			
			$(".lesson-day[data-dayid='"+deleteDayId+"']").remove();
			var noOfDays = "1 Day";
			if($(".lesson-panel[data-chapterid='"+currentChapter+"'] .lesson-day").length > 1){
				noOfDays = $(".lesson-panel[data-chapterid='"+currentChapter+"'] .lesson-day").length+" "+lessonCol.days;
			}
			$(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lms-lesson-days").text(noOfDays);
			$(".lesson-panel[data-chapterid='"+currentChapter+"'] .lesson-day").each(function(index){
				$(this).find("#dayNo").text(index+1);
			});
			$(deleteDay.deleteDayModal).modal("hide");*/
			
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/lessonplan/day/"+deleteDayId, null, "DELETE");
			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-400"){
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
					$(deleteDay.deleteDayModal).modal("hide");
				} else if(deleteAjaxCall.response == "shiksha-200"){
					deleteDay.deleteDayIds.push(deleteDayId);
					$(".lesson-day[data-dayid='"+deleteDayId+"']").remove();
					var noOfDays = "1 Day";
					if($(".lesson-panel[data-chapterid='"+currentChapter+"'] .lesson-day").length > 1){
						noOfDays = $(".lesson-panel[data-chapterid='"+currentChapter+"'] .lesson-day").length+" "+lessonCol.days;
					}
					$(".lesson-panel[data-chapterid='"+currentChapter+"']").find(".lms-lesson-days").text(noOfDays);
					$(".lesson-panel[data-chapterid='"+currentChapter+"'] .lesson-day").each(function(index){
						$(this).find("#dayNo").text(index+1);
					});
					
					$(deleteDay.deleteDayModal).modal("hide");
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};


var deleteDayActivity  = {
		deleleteDayActivitytBtn 	: ".delete-activity",
		deleteDayActivityModal 	: "#mdlDeleteDayActivity",
		deleteDayActivityConfirmBtn 	: "#deleteDayActivityButton",
		deleteDayActivityId : "#lessonplanitemid",
		deletedActivityIds : [],
		init : function(){
			$(document).on("click",this.deleleteDayActivitytBtn,this.initDelete);
			$(this.deleteDayActivityConfirmBtn).on("click",this.fnDeleteDayActivity);
		},
		initDelete : function(){
			if($(this).parents("tbody").find("tr").length == 1){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : lessonCol.minimumOneActivity,
					close : 'auto'
				});
				return false;
			}
			if($(this).parents("tr").data("lessonplanitemid"))
				deleteDayActivity.deletedActivityIds.push($(this).parents("tr").data("lessonplanitemid"));
			$(this).parents("tr").remove();
			return false;
			/*var dayActivityId=$(this).parents("tr").data("lessonplanitemid");
			if(!dayActivityId){
				$(this).parents("tr").remove();
				return false;
			}
			$("#deleteDayActivityMessage").html("Do you want to delete Activity?");
			$(deleteDayActivity.deleteDayActivityId).val(dayActivityId);
			$(deleteDayActivity.deleteDayActivityModal).modal("show");*/
		},
		fnDeleteDayActivity : function(){
			$("tr[data-lessonplanitemid='"+$(deleteDayActivity.deleteDayActivityId).val()+"']").remove();
			$(deleteDayActivity.deleteDayActivityModal).modal("hide");
		}
};

var prerequisite = {
		addPrerequisite :".action-prerequisites",
		editPrerequisite :"#prerequisiteBtnEdit",
		prerequisiteModal :"#mdlPrerequisites",
		tblPrerequisities : "#tblPrerequisities",
		addMorePrerequisite : "#addMorePrerequisite",
		savePrerequisiteBtn : "#savePrerequisite",
		preChapter : "#pre-chapter",
		preNugget  : "#pre-nugget",
		preSegment : "#pre-segment",
		preSegmentType : "#pre-segment-type",
		preDeleteAction : "#delete-prerequisite",
		chapters : [],
		chapterOptions : "",
		deletedPrerequisites : [],
		init : function(){
			$(document).on("click",this.addPrerequisite,this.initAdd);
			$(document).on("click",this.editPrerequisite,this.initAdd);
			$(document).on("click",this.addMorePrerequisite,this.addMore);
			$(document).on("click",this.savePrerequisiteBtn,this.savePrerequisites);
			$(document).on("click",this.preDeleteAction,this.deletePrerequisites);
			
			$(document).on("change",this.preChapter,this.showNuggets);
			$(document).on("change",this.preNugget,this.showSegments);
			$(document).on("change",this.preSegmentType,this.showSegments);
			
		},
		showNuggets : function(){
			$(this).parents("tr").find("#pre-nugget option:not(:first-child),#pre-segment option:not(:first-child)").remove();
			$(this).parents("tr").find("#pre-segment option:not(:first-child)").remove();
			var chapterId = $(this).val();
			if(chapterId){
				var nuggets = shiksha.invokeAjax("shikshaplus/nugget/chapter/"+chapterId,null,"GET");
				console.log(nuggets);
				$.each(nuggets,function(index,nugget){
					$(prerequisite.tblPrerequisities).find("tbody tr:last-child #pre-nugget").append("<option value='"+nugget.nuggetId+"'>"+nugget.name+"</option>");
				});
			}
			spinner.hideSpinner();
		},
		showSegments : function(){
			$(this).parents("tr").find("#pre-segment option:not(:first-child)").remove();
			var chapterId =$(this).parents("tr").find("#pre-chapter").val();
			var nuggetId = $(this).parents("tr").find("#pre-nugget").val();
			var segmentTypeId = $(this).parents("tr").find("#pre-segment-type").val();
			if(nuggetId && segmentTypeId && chapterId){
				//SHK-2734
			//	var segments = shiksha.invokeAjax("shikshaplus/segment/nugget/"+nuggetId+"/segmenttype/"+segmentTypeId,null,"GET");
				var segments = shiksha.invokeAjax("shikshaplus/segment/chapter/"+chapterId+"/nugget/"+nuggetId+"/segmenttype/"+segmentTypeId,null,"GET");
				 $.each(segments,function(index,segment){
					$(prerequisite.tblPrerequisities).find("tbody tr:last-child #pre-segment").append("<option value='"+segment.shikshaPlusSegmentId+"'>"+segment.shikshaPlusSegmentName+"</option>");
				});
			}
			spinner.hideSpinner();
		},
		initAdd : function(e){
			e.preventDefault();
			e.stopPropagation();
			prerequisite.deletedPrerequisites = [];
			prerequisite.chapterOptions = "";
			$(prerequisite.tblPrerequisities).find("tbody").empty();
			var curentChapter = currentChapter;
			var previousChapters = shiksha.invokeAjax("shikshaplus/chapter/"+curentChapter+"/previous",null,"GET");
			$.each(previousChapters,function(index,chapter){
				if(curentChapter != chapter.chapterId){
					prerequisite.chapterOptions += "<option value='"+chapter.chapterId+"'>"+chapter.name+"</option>";
				} else {
					return false;
				}
			});
			var getPrerequisites = shiksha.invokeAjax("shikshaplus/lessonplan/chapter/"+curentChapter+"/prerequisite",null,"GET");
			if(getPrerequisites && getPrerequisites.segments.length){
				$.each(getPrerequisites.segments,function(index,preSegment){
					var row = '<tr data-prerequ-mapper="'+preSegment.lessonPlanChapterPrerequisiteMapperId+'" data-prerequ-segment="'+preSegment.shikshaPlusSegmentId+'"><td>'+preSegment.shikshaPlusNugget.shikshaPlusChapter.name+'</td><td>'+preSegment.shikshaPlusNugget.name+'</td><td>'+preSegment.segmentType.segmentTypeName+'</td><td>'+preSegment.shikshaPlusSegmentName+'</td><td><a id="delete-prerequisite"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
					$(prerequisite.tblPrerequisities).find("tbody").append(row);
				})
			}
			spinner.hideSpinner();
			$(prerequisite.prerequisiteModal).modal("show");
		},
		savePrerequisites : function(){
			if(prerequisite.checkUniqueSegment()){
				var ajaxData = {
					chapterId : currentChapter,
					segmentIds : [],
					deletedMapperIds : prerequisite.deletedPrerequisites					
				};
				$(prerequisite.tblPrerequisities).find("tbody tr").each(function(){
					if(!$(this).data("prerequ-mapper")){
						ajaxData.segmentIds.push($(this).find("#pre-segment").val());
					}
				});
				
				var saveAjax = shiksha.invokeAjax("shikshaplus/lessonplan/chapter/prerequisite ",ajaxData,"POST");
				if(saveAjax != null){
					if(saveAjax.response == "shiksha-200"){
							$(".lesson-panel[data-chapterid='"+currentChapter+"']").find("#noOfPrerequisites").text($(prerequisite.tblPrerequisities).find("tbody tr").length);
							$(prerequisite.prerequisiteModal).modal("hide");
							AJS.flag({
								type  : "success",
								title : lessonCol.sucessAlert,
								body  : saveAjax.responseMessage,
								close : 'auto'
							});
												
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : saveAjax.responseMessage,
							close : 'auto'
						});
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
				spinner.hideSpinner();
								
				
			}			
		},
		addMore : function(){
			if(prerequisite.checkUniqueSegment()){
				var	segmentTypes = shiksha.invokeAjax("segment/type?exclude=0", null, "GET");
				var segmentTypeStr = "";
				$.each(segmentTypes,function(k,segment){
					segmentTypeStr += "<option value='"+segment.segmentTypeId+"'>"+segment.segmentTypeName+"</option>";
				});
				if($(prerequisite.tblPrerequisities).find("tbody tr:last-child #pre-segment").length){
					$(prerequisite.tblPrerequisities).find("tbody tr:last-child").attr("data-prerequ-segment",$(prerequisite.tblPrerequisities).find("tbody tr:last-child #pre-segment").val());
				}
				$(prerequisite.tblPrerequisities).find("tbody tr").addClass("disabled-row");
				var row = '<tr><td><select id="pre-chapter" class="form-control"><option value="">--Choose--</option>'+prerequisite.chapterOptions+'</select></td><td><select id="pre-nugget" class="form-control"><option value="">--Choose--</option></select></td><td><select id="pre-segment-type" class="form-control">'+segmentTypeStr+'</select></td><td><select id="pre-segment" class="form-control"><option value="">--Choose--</option></select></td><td><a id="delete-prerequisite"><span class="glyphicon glyphicon-trash"></span></a></td></tr>';
				$(prerequisite.tblPrerequisities).find("tbody").append(row);
			}
			spinner.hideSpinner();
		},
		deletePrerequisites : function(){
			if($(this).parents("tr").data("prerequ-mapper")){
				prerequisite.deletedPrerequisites.push($(this).parents("tr").data("prerequ-mapper"));
				$(this).parents("tr").remove();
			} 
			$(this).parents("tr").remove();
		},
		checkUniqueSegment : function(){
			var selectedSegment = $(prerequisite.tblPrerequisities).find("tbody tr:last-child #pre-segment").val();
			if(!selectedSegment && $(prerequisite.tblPrerequisities).find("tbody tr:last-child #pre-segment").length){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : lessonCol.segmentRequired,
					close : 'auto'
				});
				return false;
			}else if($(prerequisite.tblPrerequisities).find("tbody tr[data-prerequ-segment='"+selectedSegment+"']").length){
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : lessonCol.uniqueSegment,
						close : 'auto'
					});
					return false;
			}
			return true;
		}
		
}

$(document).ready(function() {
	lessonPlanFilter.init();
	createPlan.init();
	deleteLessonPlan.init();
	deleteDay.init();
	deleteDayActivity.init();
	prerequisite.init();
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
	
	$(document).on('input','#activityDuration',function() {
		var regexp = /^([01][0-9]|[02][0-3]):[0-5][0-9]$/;
	    var correct = regexp.test($(this).val());
	    if(!correct || !$(this).val()){
	    	$(this).addClass("lms-error");
	    }
	    else{
	    	$(this).removeClass("lms-error");
	    }
	});
	 
	
	$(document).on('input','#activityDesc,#dayObjective,#activityResources,#planObjective',function() {
	    if($(this).val().trim().length > 5000){
	    	$(this).addClass("lms-error");
	    }
	    else{
	    	$(this).removeClass("lms-error");
	    }
	});
});

