var  colorClass='';
var gEvent;
var prevTagValue='';

var thLabels;
var initInput =function(){
	$("#mdlAddSource").modal().show();
	$("#addSourceName").val('');
	$("#mdlAddSource #addSourceForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddSource #addSourceForm").data('formValidation').resetForm();		
	 
}



//create a row after save
var getSourceRow =function(source){
	var row = 
		"<tr id='"+source.sourceId+"' data-id='"+source.sourceId+"'>"+
		"<td></td>"+
		"<td data-sourceid='"+source.sourceId+"'	title='"+source.sourceName+"'>"+source.sourceName+"</td>"+
		
		"<td data-sourcecode='"+source.sourceCode+"' title='"+source.sourceCode+"'>"+source.sourceCode+"</td>"+		

		"<td><div class='div-tooltip'><a   class='tooltip tooltip-top btnEditSource' id='btnEditSource'" +
		"onclick='editSourceData(&quot;"+source.sourceId+"&quot;,&quot;"+escapeHtmlCharacters(source.sourceName)+"&quot;,&quot;"+source.sourceName+"&quot;);'><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top btnDeleteSource' id=deleteSource"+source.sourceId+"'	onclick='deleteSourceData(&quot;"+source.sourceId+"&quot;,&quot;"+escapeHtmlCharacters(source.sourceName)+"&quot;,&quot;"+source.sourceName+"&quot;);'><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	return row;
}


var applyPermissions=function(){
	if (!editSourcePermission){
	$(".btnEditSource").remove();
	}
	if (!deleteSourcePermission){
	$(".btnDeleteSource").remove();
	}
}

/////////////

var addSourceData=function(event){	
	spinner.showSpinner();	
	var sourceName = $('#addSourceName').val().trim().replace(/\u00a0/g," ");   
    var json ={ 
    	   "sourceName" : sourceName,    	  
    };   
    
    
    //update data table without reloading the whole table
  	//save&add new
  var	btnAddMoreSourceId =$(event.target).data('formValidation').getSubmitButton().data('id');
  	var source = customAjaxCalling("source", json, "POST");
  	if(source!=null){
  		if(source.response=="shiksha-200"){
  			spinner.hideSpinner();	
  			var newRow =getSourceRow(source);
  			appendNewRow("sourceTable",newRow);
  			applyPermissions();
  			fnUpdateDTColFilter('#sourceTable',[],4,[0,3],thLabels);
  			setShowAllTagColor(colorClass);
  			if(btnAddMoreSourceId=="addSourceSaveMoreButton"){
  				$("#mdlAddSource").find('#errorMessage').hide();
  				fnDisplaySaveAndAddNewElement('#mdlAddSource',sourceName);		
  				initInput();
  			}else{	
  				$("#mdlAddSource").modal("hide");
  				setTimeout(function() {   
  					 AJS.flag({
  					    type: 'success',
  					    title: 'Success!',
  					    body: '<p>'+ source.responseMessage+'</p>',
  					    close :'auto'
  					 })
  					}, 1000);	
  				
  			}	
  		}else {
  			spinner.hideSpinner();	
  			showDiv($('#mdlAddSource #alertdiv'));
  			$("#mdlAddSource").find('#successMessage').hide();
  			$("#mdlAddSource").find('#okButton').hide();
  			$("#mdlAddSource").find('#errorMessage').show();
  			$("#mdlAddSource").find('#exception').show();
  			$("#mdlAddSource").find('#buttonGroup').show();
  			$("#mdlAddSource").find('#exception').text(source.responseMessage);
  		}
  	}
  	spinner.hideSpinner();	
  };

 
var editSourceId=null;
var deleteSourceName=null;
var deleteSourceId=null;
var editSourceData=function(sourceId,sourceName,sgEditName){
	prevTagValue =sgEditName;
	$('#editSourceName').val(sourceName);	
	editSourceId=sourceId;
	$("#mdlEditSource").modal().show();
	$("#mdlEditSource #editSourceForm").data('formValidation').resetForm();	
};

/////////////





var editData=function(){
	spinner.showSpinner();	
    var sourceName = $('#editSourceName').val().trim().replace(/\u00a0/g," ");  
	var json ={ 
    	  "sourceId" : editSourceId,
    	  "sourceName" : sourceName,
    };


	var source = customAjaxCalling("source", json, "PUT");
	if(source!=null){
	if(source.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("sourceTable",source.sourceId);
		var newRow =getSourceRow(source);
		appendNewRow("sourceTable",newRow);
		applyPermissions();		
		fnUpdateDTColFilter('#sourceTable',[],4,[0,3],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	
		
			$('#mdlEditSource').modal("hide");
			setTimeout(function() {   
					 AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>'+ source.responseMessage+'</p>',
					    close :'auto'
					 })
					}, 1000);	
			
			
	}		
	else {

		showDiv($('#mdlEditSource #alertdiv'));
		$("#mdlEditSource").find('#successMessage').hide();
		$("#mdlEditSource").find('#errorMessage').show();
		$("#mdlEditSource").find('#exception').show();
		$("#mdlEditSource").find('#buttonGroup').show();
		$("#mdlEditSource").find('#exception').text(source.responseMessage);
	}
  }
	spinner.hideSpinner();	
};


/////////////////////////////////////////////////////
//edit source 



var deleteSourceData=function(sourceId,sourceName,sgDelName){
	deleteSourceId=sourceId;
	deleteSourceName=sourceName;
	$('#mdlDeleteSource #deleteSourceMessage').text("Do you want to delete "+sourceName+ " ?");
	$("#mdlDeleteSource").modal().show();
	hideDiv($('#mdlDeleteSource #alertdiv'));
	
};

/////////////////////////////////////////////////////
//delete source

$(document).on('click','#deleteSourceButton', function(){
	
	spinner.showSpinner();
    var source = customAjaxCalling("source/"+deleteSourceId, null, "DELETE");
    if(source!=null){
	if(source.response=="shiksha-200"){
		spinner.hideSpinner();					
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("sourceTable",deleteSourceId);
		
		$('#mdlDeleteSource').modal("hide");		
		fnUpdateDTColFilter('#sourceTable',[],4,[0,3],thLabels);
		setShowAllTagColor(colorClass);
		setTimeout(function() {   
			 AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+ source.responseMessage+'</p>',
			    close :'auto'
			 })
			}, 1000);			
		
		
	} else {
		showDiv($('#mdlDeleteSource #alertdiv'));
		$("#mdlDeleteSource").find('#errorMessage').show();
		$("#mdlDeleteSource").find('#exception').text(source.responseMessage);
		$("#mdlDeleteSource").find('#buttonGroup').show();
	}
    }
    spinner.hideSpinner();	
});





/////////////




var fnHideAlertDiv =function(){
	hideDiv($('#mdlAddSource #alertdiv'));
	hideDiv($('#mdlEditSource #alertdiv'));
}    
   




$(function() {
	
	thLabels =['#','Source  ','Source Code  ','Action'];
	fnSetDTColFilterPagination('#sourceTable',4,[0,3],thLabels);
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#sourceTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();

	fnHideAlertDiv();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$("#addSourceForm") .formValidation({
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			addSourceName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: 'Source name must be less than 250 characters'
                    },
					callback : {
						message : 'Source '+invalidInputMessage,
						callback : function(value) {
							
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))	
							return false;
							return regx.test(value);}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addSourceData(e);
		
	});
    $("#editSourceForm") .formValidation({
    	feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			editSourceName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: 'Source name must be less than 250 characters'
                    },
					callback : {
						message : 'Source '+invalidInputMessage,
						callback : function(value) {
							
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))	
							return false;
							return regx.test(value);}
					}
				}
			}
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();         
             editData();
      
        });
});