var  colorClass='';
var elementInput=[];
var thLabels=[];
elementInput.push("addSectionName");
var initInput =function(){	
	
	$("#mdlAddSection").modal().show();
	$("#addSectionName").val('');
	$("#mdlAddSection #addSectionForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddSection #addSectionForm").data('formValidation').resetForm();		
	 
}

//create a row after save
var getSectionRow =function(section){
	var row = 
		"<tr id='"+section.sectionId+"' data-id='"+section.sectionId+"'>"+
		"<td></td>"+
		"<td data-sectionid='"+section.sectionId+"'	title='"+section.sectionName+"'>"+section.sectionName+"</td>"+
		
		"<td data-sectioncode='"+section.sectionCode+"' title='"+section.sectionCode+"'>"+section.sectionCode+"</td>"+		

		"<td><div class='div-tooltip' ><a  class='tooltip tooltip-top btnEditSection'  id='btnEditSection'" +
		"onclick=editSectionData(&quot;"+section.sectionId+"&quot;,&quot;"+escapeHtmlCharacters(section.sectionName)+"&quot;);><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12' ></span></a><a style='margin-left:10px;' class='tooltip tooltip-top btnDeleteSection' id=deleteSection"+section.sectionId+"'	onclick='deleteSectionData(&quot;"+section.sectionId+"&quot;,&quot;"+escapeHtmlCharacters(section.sectionName)+"&quot;)'; ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'  ></span></a></div></td>"+
		"</tr>";
	return row;
}


var applyPermissions=function(){
	if (!editSectionPermission){
	$(".btnEditSection").remove();
	}
	if (!deleteSectionPermission){
	$(".btnDeleteSection").remove();
	}
	}


var addSectionData=function(event){
	var sectionId=0;
    var sectionName = $('#addSectionName').val().trim().replace(/\u00a0/g," ");
    var json ={ 
    	  "sectionId" : sectionId,
    	  "sectionName" : sectionName,    	  
    	  };     
	//update datatable without reloading the whole table
	//save&add new
var	btnAddMoreSectionId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var section = customAjaxCalling("section", json, "POST");
	if(section!=null){
	if(section.response=="shiksha-200"){
		$(".outer-loader").hide();

		var newRow =getSectionRow(section);
		appendNewRow("sectionTable",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#sectionTable',[],4,[0,3],thLabels);
		setShowAllTagColor(colorClass);

		if(btnAddMoreSectionId=="addSectionSaveMoreButton"){
			
			$("#mdlAddSection").find('#errorMessage').hide();
			fnDisplaySaveAndAddNewElement('#mdlAddSection',sectionName);		
			initInput();
		}else{	
			$("#mdlAddSection").modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ section.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
							

		}	
	}else {
		showDiv($('#mdlAddSection #alertdiv'));
		$("#mdlAddSection").find('#successMessage').hide();
		$("#mdlAddSection").find('#okButton').hide();
		$("#mdlAddSection").find('#errorMessage').show();
		$("#mdlAddSection").find('#exception').show();
		$("#mdlAddSection").find('#buttonGroup').show();
		$("#mdlAddSection").find('#exception').text(section.responseMessage);
	}
	}
	$(".outer-loader").hide();
	enableTooltip();
};




var editSectionId=null;
var deleteSectionName=null;
var deleteSectionId=null;
var editSectionData=function(sectionId,sectionName){	
	$('#editSectionName').val(sectionName);	
	editSectionId=sectionId;
	$("#mdlEditSection").modal().show();
	
	 $("#mdlEditSection #editSectionForm").data('formValidation').resetForm();
};
var editData=function(){
    var sectionName = $('#editSectionName').val().trim().replace(/\u00a0/g," ");  
	var json ={ 
    	  "sectionId" : editSectionId,
    	  "sectionName" : sectionName,
    };

	var section = customAjaxCalling("section", json, "PUT");
	if(section!=null){
	if(section.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("sectionTable",section.sectionId);
		var newRow =getSectionRow(section);
		appendNewRow("sectionTable",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#sectionTable',[],4,[0,3],thLabels);
		setShowAllTagColor(colorClass);

		displaySuccessMsg();	
		
			$('#mdlEditSection').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ section.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);			
		
		}		
	else {
		showDiv($('#mdlEditSection #alertdiv'));
		$("#mdlEditSection").find('#successMessage').hide();
		$("#mdlEditSection").find('#errorMessage').show();
		$("#mdlEditSection").find('#exception').show();
		$("#mdlEditSection").find('#buttonGroup').show();
		$("#mdlEditSection").find('#exception').text(section.responseMessage);
	}
	}
	$(".outer-loader").hide();
	enableTooltip();
};
	
var deleteSectionData=function(sectionId,sectionName){
	deleteSectionId=sectionId;
	deleteSectionName=sectionName;
	$('#mdlDeleteSection #deleteSectionMessage').text("Do you want to delete "+sectionName+ " ?");
	$("#mdlDeleteSection").modal().show();
	hideDiv($('#mdlDeleteSection #alertdiv'));
};
$(document).on('click','#deleteSectionButton', function(){
	$(".outer-loader").show();
	var section = customAjaxCalling("section/"+deleteSectionId, null, "DELETE");
	if(section!=null){
	if(section.response=="shiksha-200"){
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("sectionTable",deleteSectionId);
		
		fnUpdateDTColFilter('#sectionTable',[],4,[0,3],thLabels);
		setShowAllTagColor(colorClass);
		$('#mdlDeleteSection').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+ section.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
			
	} else {
		showDiv($('#mdlDeleteSection #alertdiv'));
		$("#mdlDeleteSection").find('#errorMessage').show();
		$("#mdlDeleteSection").find('#exception').text(section.responseMessage);
		$("#mdlDeleteSection").find('#buttonGroup').show();
	}
	}
	$(".outer-loader").hide();
});



$(function() {
	

	thLabels =['#','Section  ','Section Code  ','Action'];
	fnSetDTColFilterPagination('#sectionTable',4,[0,3],thLabels);
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#sectionTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();



	$( "#addSectionName" ).keypress(function() {		
		hideDiv($('#mdlAddSection #alertdiv'));
	})
		$( "#editSectionForm" ).keypress(function() {		
		hideDiv($('#mdlEditSection #alertdiv'));
	})
	$( ".multiselect-container" ).unbind( "mouseleave");
			$( ".multiselect-container" ).on( "mouseleave", function() {
				$(this).click();
			});
    $("#addSectionForm") .formValidation({
    	feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			addSectionName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: 'Section name must be less than 250 characters'
                    },
					callback : {
						message : 'Section '+invalidInputMessage,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}							
					}
				}
			}
		}
    }).on('success.form.fv', function(e) {
             e.preventDefault();
             addSectionData(e);
      
   });
    $("#editSectionForm") .formValidation({
    	feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			editSectionName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: 'Section name must be less than 250 characters'
                    },
					callback : {
						message : 'Section '+invalidInputMessage,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			}
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();
             editData();
      
        });
});