//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForNP =[];
var allInputEleNamesOfFilter =[]; 
var btnAddMoreNPId;
var deleteNPId;
var thLabels;
var selectedIds;
var deleteNyayPanchayatId;
var deleteNyayPanchayatName;
var elementIdMap;
//initialize global variables
var initGloabalVarsNP =function(){
	resetButtonObj='';
	allInputEleNamesForNP=['addNyayPanchayatName'];
	allInputEleNamesOfFilter=['stateId','zoneId','districtId','tehsilId','blockId'];
	btnAddMoreNPId ='';
	deleteNPId='';
}

//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}



//create a row after save
var getNyayPanchayatRow =function(nyayPanchayat){
	var row = 
		"<tr id='"+nyayPanchayat.nyayPanchayatId+"' data-id='"+nyayPanchayat.nyayPanchayatId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+nyayPanchayat.stateId+"'	title='"+nyayPanchayat.stateName+"'>"+nyayPanchayat.stateName+"</td>"+
		"<td data-zoneid='"+nyayPanchayat.zoneId+"'	title='"+nyayPanchayat.zoneName+"'>"+nyayPanchayat.zoneName+"</td>"+
		"<td data-districtid='"+nyayPanchayat.districtId+"' title='"+nyayPanchayat.districtName+"' >"+nyayPanchayat.districtName+"</td>"+
		"<td data-tehsilid='"+nyayPanchayat.tehsilId+"' title='"+nyayPanchayat.tehsilName+"' >"+nyayPanchayat.tehsilName+"</td>"+
		"<td data-blockid='"+nyayPanchayat.blockId+"' title='"+nyayPanchayat.blockName+"' >"+nyayPanchayat.blockName+"</td>"+
		"<td data-nyaypanchayatid='"+nyayPanchayat.nyayPanchayatId+"' title='"+nyayPanchayat.nyayPanchayatName+"' >"+nyayPanchayat.nyayPanchayatName+"</td>"+
		"<td data-nyaypanchayatcode='"+nyayPanchayat.nyayPanchayatCode+"' title='"+nyayPanchayat.nyayPanchayatCode+"'>"+nyayPanchayat.nyayPanchayatCode+"</td>"+		
		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top' id='btnEditNP'" +
		"onclick=editNyayPanchayatData(&quot;"+nyayPanchayat.stateId+"&quot;,&quot;"+nyayPanchayat.zoneId+"&quot;,&quot;"+nyayPanchayat.districtId+"&quot;,&quot;"+nyayPanchayat.tehsilId+"&quot;,&quot;"+nyayPanchayat.blockId+"&quot;,&quot;"+nyayPanchayat.nyayPanchayatId+"&quot;,&quot;"+escapeHtmlCharacters(nyayPanchayat.nyayPanchayatName)+"&quot;); ><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a style='margin-left:10px;' class='tooltip tooltip-top'  id=deleteNP	onclick=deleteNyayPanchayatData(&quot;"+nyayPanchayat.nyayPanchayatId+"&quot;,&quot;"+escapeHtmlCharacters(nyayPanchayat.nyayPanchayatName)+"&quot;); ><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"+
		"</tr>";
	
	return row;
}
	

//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddNyayPanchayat #statesForZone',
			"Zone" : '#mdlAddNyayPanchayat #selectBox_zonesByState',
			"District" : '#mdlAddNyayPanchayat #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddNyayPanchayat #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddNyayPanchayat #selectBox_blocksByTehsil',
		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
			"State" : '#mdlEditNyayPanchayat #statesForZone',
			"Zone" : '#mdlEditNyayPanchayat #selectBox_zonesByState',
			"District" : '#mdlEditNyayPanchayat #selectBox_districtsByZone',
			"Tehsil" : '#mdlEditNyayPanchayat #selectBox_tehsilsByDistrict',
			"Block" : '#mdlEditNyayPanchayat #selectBox_blocksByTehsil',
		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};


var applyPermissions=function(){
	if (!editNpPermission){
	$("#btnEditNP").remove();
		}
	if (!deleteNpPermission){
	$("#deleteNP").remove();
		}
	}

// smart filter utility--- start here

var initSmartFilter = function(type) {
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){		
		resetFormValidatonForLocFilters("addNyayPanchayatForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addNyayPanchayatForm",allInputEleNamesForNP);
		displaySmartFilters("add")
		fnCollapseMultiselect();
	}else{	
	resetFvForAllInputExceptLoc("editNyayPanchayatForm",['editNyayPanchayatName']);
		displaySmartFilters("edit")
		fnCollapseMultiselect();
	}
}




//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	if($(resetButtonObj).parents("form").attr("id")=="addNyayPanchayatForm"){
		
		resetFormValidatonForLocFilters("addNyayPanchayatForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");
	}else{
		
		resetFormValidatonForLocFilters("addNyayPanchayatForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
	}
	fnCollapseMultiselect();
	
	
}



// reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addNyayPanchayatForm');
	resetFormById('editNyayPanchayatForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}


// smart filter utility--- end here

// perform CRUD

// add Np
var addNyayPanchayat = function(event) {
	$('.outer-loader').show();
	var blockId = $('#mdlAddNyayPanchayat #selectBox_blocksByTehsil').val();
	var panchayatName = $('#mdlAddNyayPanchayat #addNyayPanchayatName').val().trim().replace(/\u00a0/g," ");
	var json = {
		"blockId" : blockId,
		"nyayPanchayatName" : panchayatName,
	};
	
	//update data table without reloading the whole table
	//save&add new
	btnAddMoreNPId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var nyayPanchayat = customAjaxCalling("nyayPanchayat", json, "POST");
	if(nyayPanchayat!=null){
	if(nyayPanchayat.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		var newRow =getNyayPanchayatRow(nyayPanchayat);
		appendNewRow("tblNayaPanchayat",newRow);
		applyPermissions();		
		fnUpdateDTColFilter('#tblNayaPanchayat',[2,4,5],9,[0,8],thLabels);
		setShowAllTagColor(colorClass);
		if(btnAddMoreNPId=="addNyayPanchayatSaveMoreButton"){
			resetFvForAllInputExceptLoc("addNyayPanchayatForm",allInputEleNamesForNP);
			$("#mdlAddNyayPanchayat").find('#errorMessage').hide();			
			fnDisplaySaveAndAddNewElement('#mdlAddNyayPanchayat',panchayatName);
		}else{			
			$('#mdlAddNyayPanchayat').modal("hide");
			setTimeout(function() {   //calls click event after a one second
				 AJS.flag({
				    type: 'success',
				    title: appMessgaes.success,
				    body: '<p>'+ nyayPanchayat.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);			
		}	
		
	}else {
		$(".outer-loader").hide();
		showDiv($('#mdlAddNyayPanchayat #alertdiv'));
		$("#mdlAddNyayPanchayat").find('#successMessage').hide();
		$("#mdlAddNyayPanchayat").find('#okButton').hide();
		$("#mdlAddNyayPanchayat").find('#errorMessage').show();
		$("#mdlAddNyayPanchayat").find('#exception').show();
		$("#mdlAddNyayPanchayat").find('#buttonGroup').show();
		$("#mdlAddNyayPanchayat").find('#exception').text(nyayPanchayat.responseMessage);
	}
	}
	$(".outer-loader").hide();
};



// edit
// get data for edit
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editTehsilId = 0;
var editBlockId = 0;
var editNyayPanchayatId = 0;
var editNyayPanchayatData = function(stateId, zoneId, districtId, tehsilId,
		blockId, nyayPanchayatId, nyayPanchayatName) {

	initSmartFilter("edit");
	var idMap = {
		'#mdlEditNyayPanchayat #statesForZone' : stateId,
		'#mdlEditNyayPanchayat #selectBox_zonesByState' : zoneId,
		'#mdlEditNyayPanchayat #selectBox_districtsByZone' : districtId,
		'#mdlEditNyayPanchayat #selectBox_tehsilsByDistrict' : tehsilId,
		'#mdlEditNyayPanchayat #selectBox_blocksByTehsil' : blockId,
	};
	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
							.prop('selected', true);
				enableMultiSelect(ele_key);
			});
	

	editNyayPanchayatId = nyayPanchayatId;
	$('#mdlEditNyayPanchayat #editNyayPanchayatName').val(nyayPanchayatName);
	$("#mdlEditNyayPanchayat").modal().show();
	fnCollapseMultiselect();
};

// edit Np
var editNyayPanchayat = function() {
	$('.outer-loader').show();
	var blockId = $('.editnyayPanchayat #selectBox_blocksByTehsil').val();
	var nyayPanchayatName = $('#editNyayPanchayatName').val().trim().replace(/\u00a0/g," ");
	var nyayPanchayatId = editNyayPanchayatId;
	var json = {
			"blockId" : blockId,
			"nyayPanchayatName" : nyayPanchayatName,
			"nyayPanchayatId" : nyayPanchayatId
	};


	var nyayPanchayat = customAjaxCalling("nyayPanchayat", json, "PUT");
	if(nyayPanchayat!=null){
	if(nyayPanchayat.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("tblNayaPanchayat",nyayPanchayat.nyayPanchayatId);
		var newRow =getNyayPanchayatRow(nyayPanchayat);
		appendNewRow("tblNayaPanchayat",newRow);
		applyPermissions();		
		fnUpdateDTColFilter('#tblNayaPanchayat',[2,4,5],9,[0,8],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	

		$('#mdlEditNyayPanchayat').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ nyayPanchayat.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
		
	}		
	else {
		$(".outer-loader").hide();
		showDiv($('#mdlEditNyayPanchayat #alertdiv'));
		$("#mdlEditNyayPanchayat").find('#successMessage').hide();
		$("#mdlEditNyayPanchayat").find('#errorMessage').show();
		$("#mdlEditNyayPanchayat").find('#exception').show();
		$("#mdlEditNyayPanchayat").find('#buttonGroup').show();
		$("#mdlEditNyayPanchayat").find('#exception').text(nyayPanchayat.responseMessage);
	}
	enableTooltip();
	}
	$(".outer-loader").hide();

};


// getting data for delete

var deleteNyayPanchayatData = function(nyayPanchayatid, nyayPanchayatname) {
	deleteNyayPanchayatId = nyayPanchayatid;
	deleteNyayPanchayatName = nyayPanchayatname;
	$('#deleteNyayPanchayatMessage').text(
			columnMessages.deleteConfirm.replace("@NAME",nyayPanchayatname) + " ?");
	$("#mdlDelNyayPanchayat").modal().show();
	hideDiv($('#mdlDelNyayPanchayat #alertdiv'));
};



$('#deleteNyayPanchayatButton').click(function() {
	$('.outer-loader').show();
	var nyayPanchayat = customAjaxCalling("nyayPanchayat/"+deleteNyayPanchayatId, null, "DELETE");
	if(nyayPanchayat!=null){
	if(nyayPanchayat.response=="shiksha-200"){
		$(".outer-loader").hide();						
		deleteRow("tblNayaPanchayat",deleteNyayPanchayatId);		
		fnUpdateDTColFilter('#tblNayaPanchayat',[2,4,5],9,[0,8],thLabels);
		setShowAllTagColor(colorClass);
		$('#mdlDelNyayPanchayat').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ nyayPanchayat.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
		
	} else {
		showDiv($('#mdlDelNyayPanchayat #alertdiv'));
		$("#mdlDelNyayPanchayat").find('#errorMessage').show();
		$("#mdlDelNyayPanchayat").find('#exception').text(nyayPanchayat.responseMessage);
		$("#mdlDelNyayPanchayat").find('#buttonGroup').show();
	}
	}
	$(".outer-loader").hide();
});
	



// form validation
$(function() {
		
	thLabels =['#',columnMessages.state ,columnMessages.zone,columnMessages.district,columnMessages.tehsil,columnMessages.block,columnMessages.nyayPanchayat,columnMessages.nyayPanchayatCode,columnMessages.action];
	fnSetDTColFilterPagination('#tblNayaPanchayat',9,[0,8],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tblNayaPanchayat',[2,4,5]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	enableTooltip();
	$( "#addNyayPanchayatName" ).keypress(function() {		
		hideDiv($('#mdlAddNyayPanchayat #alertdiv'),$('#mdlEditNyayPanchayat #alertdiv'));
	})
		$( "#editNyayPanchayatForm" ).keypress(function() {		
		hideDiv($('#mdlEditNyayPanchayat #alertdiv'));
	})
	
	initGloabalVarsNP();
	$("#addNyayPanchayatForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							addNyayPanchayatName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.nyayPanchayat+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addNyayPanchayat(e);

			});
	$("#editNyayPanchayatForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							editNyayPanchayatName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.nyayPanchayat+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
						e.preventDefault();
						editNyayPanchayat();
			});
});
