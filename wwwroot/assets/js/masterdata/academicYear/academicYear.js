var consecutivePreviousYear;
var colorClass;
var thLabels;
var lastFutureAcademicYear;
var editYear={};
editYear.startDate="#frmEditAcademicYear #startDatePicker";
editYear.endDate="#frmEditAcademicYear #endDatePicker";
editYear.form="#frmEditAcademicYear";
editYear.name="#frmEditAcademicYear #editYearName";
editYear.id="#frmEditAcademicYear #editYearId";
editYear.yearType="#frmEditAcademicYear #dropboxEditYearType";
editYear.modal="#mdlEditAcademicYear";
editYear.msg="#frmEditAcademicYear #startDateEndDateValidationMsg";
editYear.msgDiv="#frmEditAcademicYear #startDateEndDateValidationMsgDiv";
editYear.submit="#frmEditAcademicYear #submit";
editYear.alertDiv="#mdlEditAcademicYear #alertDiv"


	var addYear={};
addYear.startDate="#frmAddAcademicYear #startDatePicker";
addYear.endDate="#frmAddAcademicYear #endDatePicker";
addYear.form="#frmAddAcademicYear";
addYear.name="#frmAddAcademicYear #addYearName";
addYear.id="#frmAddAcademicYear #addYearId";
addYear.yearType="#frmAddAcademicYear #dropboxAddYearType";
addYear.modal="#mdlAddAcademicYear";
addYear.msg="#frmAddAcademicYear #startDateEndDateValidationMsg";
addYear.msgDiv="#frmAddAcademicYear #startDateEndDateValidationMsgDiv";
addYear.submit="#frmAddAcademicYear  #submit";
addYear.alertDiv="#mdlAddAcademicYear #alertDiv";

var applyPermissions=function(flagCurrent){
	if (!editAcademicPermission && flagCurrent==3){
		$("#btnEditYear").remove();
	}

}

var showErrorMessage=function(yearObj,message){
	showDiv($(yearObj.alertDiv));
	$(yearObj.modal).find('#successMessage').hide();
	$(yearObj.modal).find('#errorMessage').show();
	$(yearObj.modal).find('#exception').show();
	$(yearObj.modal).find('#buttonGroup').show();
	$(yearObj.modal).find('#exception').text(message);

}


var getYearRow =function(aYearData){
	var yearTypeCol="";
	var editRow="<td><div class='div-tooltip'><a class='tooltip tooltip-top'  id='btnEditYear'" +
	"onclick=fnInitEditModal(&quot;"+aYearData.academicYearId+"&quot;,&quot;"+aYearData.academicYear+"&quot;,&quot;"+aYearData.startDate+"&quot;,&quot;"+aYearData.endDate+"&quot;,&quot;"+aYearData.flagCurrentAY+"&quot;);><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a></td>";

	if (aYearData.flagCurrentAY == 1){
		yearTypeCol='<span class="label label-success text-uppercase">Current Academic Year</span>';

	}else if(aYearData.flagCurrentAY == 2){
		yearTypeCol='<span class="label label-warning text-uppercase">Previous Academic Year</span>';

	}
	else if(aYearData.flagCurrentAY == 0){
		yearTypeCol='<span class="label label-default text-uppercase">Future Academic Year</span>';

	}
	else if(aYearData.flagCurrentAY == 3){
		yearTypeCol='<span class="label label-danger text-uppercase">Past Academic Year</span>';
		editRow="<td></td>";

	}

	var row = "<tr id='"+aYearData.academicYearId+"'>"+
	"<td></td>"+
	"<td title='"+aYearData.academicYear+"'>"+aYearData.academicYear+"</td>"+
	"<td title='"+aYearData.startDate+"'>"+aYearData.startDate+"</td>"+
	"<td title='"+aYearData.endDate+"'>"+aYearData.endDate+"</td>"+
	"<td> "+yearTypeCol+"</td>"+
	"</tr>";
	return row;
}
var initAcademicYearDatePicker=function(datePickerId,formId,field,yearObject){
	var todayDate=new Date();
	$(datePickerId).datepicker({
		allowPastDates: true,
		momentConfig: {
			culture: 'en', // change to specific culture
			format: 'DD-MMM-YYYY' // change for specific format
		}
	})
	$(datePickerId).datepicker('setDate',todayDate);
}

var initAddAcademicYearDatePicker=function(datePickerId,field,yearObject,bAyDate){
	var lastDate =  moment(bAyDate, "DD-MMM-YYYY").add(1, 'days');
	var nextLastDate=moment(bAyDate, "DD-MMM-YYYY").add(365, 'days');
	$(datePickerId).datepicker({
		allowPastDates: false,
		restricted: [{
			from: -Infinity,
			to: bAyDate
		}],
		momentConfig: {
			culture: 'en', // change to specific culture
			format: 'DD-MMM-YYYY' // change for specific format
		}
	})
	if(datePickerId=="#frmAddAcademicYear #startDatePicker"){
		$(datePickerId).datepicker('setDate',lastDate);
	}
	if(datePickerId=="#frmAddAcademicYear #endDatePicker"){
		$(datePickerId).datepicker('setDate',nextLastDate);
	}


}


/*date parsing */
var fnDateParsing=function(data){
	var sArray =data.split('-');
	var monthData =sArray[1];
	var dateData =sArray[0];
	var yearData =sArray[2];
	var dateStringData =Date.parse(monthData+' '+dateData+' ,'+yearData);
	return dateStringData;
}
var setAcademicYearName=function(zYearObj,aStartDate,aEndDate){
	var startDateYear =aStartDate.split('-')[2];
	var endDateYear =aEndDate.split('-')[2].substring(2);
	$(zYearObj.name).val(startDateYear+"-"+endDateYear);

}
var validateAcademicYearName=function(aStartDate,aEndDate){
	var startDateYear =aStartDate.split('-')[2].substring(2);
	var endDateYear =aEndDate.split('-')[2].substring(2);
	if(parseInt(startDateYear)>=parseInt(endDateYear)){
		return false;
	}else if(parseInt(startDateYear)<parseInt(endDateYear)){
		return true;
	}
}
/*validate end date always greater then start date */

var validateEditEndDate =function(e,data){
	var sDateData=fnDateParsing($(data.startDate).datepicker('getFormattedDate'));
	var eDateData=fnDateParsing($(data.endDate).datepicker('getFormattedDate'));
	if (eDateData<sDateData) {
		e.preventDefault();
		$(data.msgDiv).show();
		$(data.submit).prop('disabled',true);
		return false;
	}else{
		$(data.submit).prop('disabled',false);
		$(data.msgDiv).hide();
		return true;
	}
}

var setOptionForEditYearType=function(arg){
	$(arg).multiselect('destroy');
	$(arg).multiselect({
		maxHeight: 180,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',
		onDropdownHide: function() {
			if(arg==editYear.yearType){
				hideDiv($(editYear.alertDiv));
				$(editYear.submit).prop('disabled',false);
			}else if(arg==addYear.yearType){
				hideDiv($(addYear.alertDiv));
				$(addYear.submit).prop('disabled',false);
			}
		} 
	});
	$(arg).multiselect('refresh');
	fnCollapseMultiselect();
}



var bindYearType=function(yearFlag,yearObj){
	if (yearFlag != "") {
		$(yearObj).find('option').each(function(ind, obj) {
			if ($(obj).val() == yearFlag) {
				$(obj).prop('selected', true);
			}
		});
		setOptionForEditYearType(yearObj);
	}
}
var fnInitEditModal=function(yId,yName,yStartDate,yEndDate,flagCurrentAY){
	spinner.showSpinner();
	$(editYear.submit).prop('disabled',true);

	$(editYear.endDate).datepicker('setDate',yEndDate);
	$(editYear.startDate).datepicker('setDate',yStartDate);
	$(editYear.name).val(yName);
	$(editYear.id).val(yId);
	hideDiv($(editYear.alertDiv));
	$(editYear.modal).modal();
	bindYearType(flagCurrentAY,editYear.yearType);
	if(flagCurrentAY==2){
		$(editYear.startDate).datepicker('disable');
		$(editYear.endDate).datepicker('disable');
		$(editYear.startDate).show();
		$(editYear.endDate).show();
	}
	spinner.hideSpinner();
}



var fnEditAcademicYear=function(){
	$(".outer-loader").show();
	var editYearId=$(editYear.id).val();
	var yearStartDate=$(editYear.startDate).datepicker('getFormattedDate');
	var yearEndDate=$(editYear.endDate).datepicker('getFormattedDate');
	var editYearType=$(editYear.yearType).find("option:selected").val();
	var editYname=$(editYear.name).val();
	var jsonData={
			"academicYear":editYname,
			"academicYearId":editYearId,
			"startDate":yearStartDate,
			"endDate":yearEndDate,
			"flagCurrentAY":editYearType,

	};
	var data=customAjaxCalling("academicYear", jsonData, "PUT")

	if(data.response=="shiksha-200"){

		$(".outer-loader").hide();
		deleteRow("ayTable",data.academicYearId);
		var newRow =getYearRow(data);
		appendNewRow("ayTable",newRow);
		applyPermissions(data.flagCurrentAY);
		fnUpdateDTColFilter('#ayTable',[],6,[0,5],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	
		lastFutureAcademicYear=customAjaxCalling("academicYear/futureYear", null, "GET");
		$(editYear.modal).modal("hide");
		setTimeout(function() {   
			 AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+ data.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
		
	}		
	else {
		showErrorMessage(editYear,data.responseMessage);
	}

}

var fnInitAddModal=function(){
	spinner.showSpinner();
	$(addYear.submit).prop('disabled',false);
	hideDiv($(addYear.alertDiv));
	$(addYear.msgDiv).hide();
	resetFormById('frmAddAcademicYear');
	var sDatePicker=  $(addYear.startDate).datepicker('destroy');
	var eDatePicker= $(addYear.endDate).datepicker('destroy');
	$("#frmAddAcademicYear #timeLineStartDiv").append(sDatePicker);
	$("#frmAddAcademicYear #timeLineEndDiv").append(eDatePicker);
	initAddAcademicYearDatePicker(addYear.startDate,'startDate',addYear,lastFutureAcademicYear.endDate);
	initAddAcademicYearDatePicker(addYear.endDate,'endDate',addYear,lastFutureAcademicYear.endDate);
	
	var startDate = $(addYear.startDate).datepicker('getFormattedDate');
	var endDate = $(addYear.endDate).datepicker('getFormattedDate');
	setAcademicYearName(addYear,startDate,endDate);	
	$(addYear.modal).modal();
	spinner.hideSpinner();
}



var fnAddAcademicYearData=function(){
	var yearStartDate=$(addYear.startDate).datepicker('getFormattedDate');
	var yearEndDate=$(addYear.endDate).datepicker('getFormattedDate');
	
	var addYearName=$(addYear.name).val();
	var jsonData={
			"academicYear":addYearName,
			"startDate":yearStartDate,
			"endDate":yearEndDate,
			"flagCurrentAY":0,

	};
	var data=customAjaxCalling("academicYear", jsonData, "POST")

	if(data.response=="shiksha-200"){
		lastFutureAcademicYear=data;
		spinner.hideSpinner();
		var newRow =getYearRow(data);
		appendNewRow("ayTable",newRow);
		applyPermissions(data.flagCurrentAY);
		fnUpdateDTColFilter('#ayTable',[],5,[0,4],thLabels);

		setShowAllTagColor(colorClass);
		displaySuccessMsg();	
		$(addYear.modal).modal("hide");
		setTimeout(function() {   
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ data.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
	

	}		
	else {
		showErrorMessage(addYear,data.responseMessage);
		spinner.hideSpinner();
	}
	spinner.hideSpinner();

}
var fnAddAcademicYear=function(){
	spinner.showSpinner();
	$.wait(50).then(fnAddAcademicYearData);
}







$(function() {
	colorClass = $('#t0').prop('class');
	thLabels =['#','Academic Year ','Start Date  ','End Date ','Status '];
	fnSetDTColFilterPagination('#ayTable',5,[0,4],thLabels);

	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#ayTable',[]);
	});
	fnUnbindDTSortListener();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$(".outer-loader").hide();
	initAcademicYearDatePicker(editYear.startDate,editYear.form,'startDate');
	initAcademicYearDatePicker(editYear.endDate,editYear.form,'endDate');
	setOptionsForMultipleSelect(editYear.yearType);
	initAddAcademicYearDatePicker(addYear.startDate,'startDate',addYear,lastFutureAcademicYear.endDate);
	initAddAcademicYearDatePicker(addYear.endDate,'endDate',addYear,lastFutureAcademicYear.endDate);
	setOptionsForMultipleSelect(addYear.yearType);


	$(editYear.form).formValidation({
		framework : 'bootstrap',
		icon : {
			validating : 'glyphicon glyphicon-refresh'
		},
		excluded : ':hidden',
		fields : {
			startDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},
			endDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},

		}
	}).on('changed.fu.datepicker dateClicked.fu.datepicker',function(e) {
		var startDate = $(editYear.startDate).datepicker('getFormattedDate');
		var endDate = $(editYear.endDate).datepicker('getFormattedDate');
		var isValidDate=validateEditEndDate(e,editYear);
		hideDiv($(editYear.alertDiv));
		setAcademicYearName(editYear,startDate,endDate);	
		if (startDate != "Invalid Date") {
			$(editYear.form).data('formValidation').updateStatus('startDate', 'VALID');

		}
		if (endDate != "Invalid Date") {
			if(isValidDate){
				$(editYear.msgDiv).css('display','none');
				$(editYear.submit).prop('disabled',false);
				$(editYear.form).data('formValidation').updateStatus('endDate', 'VALID');
			}else{
				$(editYear.submit).prop('disabled',true);
				$(editYear.msgDiv).css('display','block');
				$(editYear.form).data('formValidation').updateStatus('endDate', 'INVALID');
			}
		}
	});
	///////////////////// add academic year /////////////////////
	$(addYear.form).formValidation({
		framework : 'bootstrap',
		icon : {
			validating : 'glyphicon glyphicon-refresh'
		},
		excluded : ':hidden',
		fields : {
			startDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},
			endDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},

		}
	}).on('changed.fu.datepicker dateClicked.fu.datepicker',function(e) {
		var startDate = $(addYear.startDate).datepicker('getFormattedDate');
		var endDate = $(addYear.endDate).datepicker('getFormattedDate');
		var isValidDate=validateEditEndDate(e,addYear);
		setAcademicYearName(addYear,startDate,endDate);	
		hideDiv($(addYear.alertDiv));
		if (startDate != "Invalid Date") {
			$(addYear.form).data('formValidation').updateStatus('startDate', 'VALID');

		}
		if (endDate != "Invalid Date") {
			if(isValidDate){
				$(addYear.msgDiv).css('display','none');
				$(addYear.submit).prop('disabled',false);
				$(addYear.form).data('formValidation').updateStatus('endDate', 'VALID');
			}else{
				$(addYear.submit).prop('disabled',true);
				$(addYear.msgDiv).css('display','block');
				$(addYear.form).data('formValidation').updateStatus('endDate', 'INVALID');
			}
		}

	});

	
	
	$(editYear.modal).on("hide.bs.modal", function () {
		resetFormById('frmEditAcademicYear');
		$(editYear.msgDiv).hide();
		$(editYear.startDate).datepicker('enable');
		$(editYear.endDate).datepicker('enable');
		$(editYear.startDate).show();
		$(editYear.endDate).show();
	});

});
