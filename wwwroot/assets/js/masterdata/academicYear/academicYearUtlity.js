var colorClass;
var thLabels;
var editYear={};
editYear.startDate="#frmEditAcademicYear #startDatePicker";
editYear.endDate="#frmEditAcademicYear #endDatePicker";
editYear.startDateDiv="#frmEditAcademicYear #timeLineStartDiv";
editYear.endDateDiv="#frmEditAcademicYear #timeLineEndDiv";
editYear.form="#frmEditAcademicYear";
editYear.formId="frmEditAcademicYear";
editYear.name="#frmEditAcademicYear #editYearName";
editYear.id="#frmEditAcademicYear #editYearId";
editYear.yearType="#frmEditAcademicYear #dropboxEditYearType";
editYear.modal="#mdlEditAcademicYear";
editYear.modalTitle="#mdlEditAcademicYear .modal-title";
editYear.msg="#frmEditAcademicYear #startDateEndDateValidationMsg";
editYear.msgDiv="#frmEditAcademicYear #startDateEndDateValidationMsgDiv";
editYear.submit="#frmEditAcademicYear #submit";
editYear.alertDiv="#mdlEditAcademicYear #alertDiv"


	var applyPermissions=function(){
	if (!editAcademicPermission){
		$("#btnEditCurrentYear").remove();
		$("#btnEditNextYear").remove();
		$("#initUpgrade").remove();
	}

}

var showErrorMessage=function(yearObj,message){
	showDiv($(yearObj.alertDiv));
	$(yearObj.modal).find('#successMessage').hide();
	$(yearObj.modal).find('#errorMessage').show();
	$(yearObj.modal).find('#exception').show();
	$(yearObj.modal).find('#buttonGroup').show();
	$(yearObj.modal).find('#exception').text(message);

}




var initEditAcademicYearDatePicker=function(datePickerId,field,yearObject,bAyDate,restrictionDate){
	$(datePickerId).datepicker({
		allowPastDates: true,
		restricted: [{
			from: -Infinity,
			to: restrictionDate
		}],
		momentConfig: {
			culture: 'en', // change to specific culture
			format: 'DD-MMM-YYYY' // change for specific format
		}
	})
	$(datePickerId).datepicker('setDate',bAyDate);
}

/*date parsing */
var fnDateParsing=function(data){
	var sArray =data.split('-');
	var monthData =sArray[1];
	var dateData =sArray[0];
	var yearData =sArray[2];
	var dateStringData =Date.parse(monthData+' '+dateData+' ,'+yearData);
	return dateStringData;
}
var setAcademicYearName=function(zYearObj,aStartDate,aEndDate){
	var startDateYear =aStartDate.split('-')[2];
	var endDateYear =aEndDate.split('-')[2].substring(2);
	$(zYearObj.name).val(startDateYear+"-"+endDateYear);

}
var validateAcademicYearName=function(aStartDate,aEndDate){
	var startDateYear =aStartDate.split('-')[2].substring(2);
	var endDateYear =aEndDate.split('-')[2].substring(2);
	if(parseInt(startDateYear)>=parseInt(endDateYear)){
		return false;
	}else if(parseInt(startDateYear)<parseInt(endDateYear)){
		return true;
	}
}
/*validate end date always greater then start date */

var validateEditEndDate =function(e,data){
	var sDateData=fnDateParsing($(data.startDate).datepicker('getFormattedDate'));
	var eDateData=fnDateParsing($(data.endDate).datepicker('getFormattedDate'));
	if (eDateData<sDateData) {
		e.preventDefault();
		$(data.msgDiv).show();
		$(data.submit).prop('disabled',true);
		return false;
	}else{
		$(data.submit).prop('disabled',false);
		$(data.msgDiv).hide();
		return true;
	}
}





var fnInitEditAYModal=function(yId,yName,yStartDate,yEndDate,flagCurrentAY,restrictedDate,yearType){
	spinner.showSpinner();
	$(editYear.submit).prop('disabled',true);
	hideDiv($(editYear.alertDiv));
	$(editYear.msgDiv).hide();
	resetFormById(editYear.formId);

	var sDatePicker=  $(editYear.startDate).datepicker('destroy');
	var eDatePicker= $(editYear.endDate).datepicker('destroy');
	$(editYear.startDateDiv).append(sDatePicker);
	$(editYear.endDateDiv).append(eDatePicker);

	initEditAcademicYearDatePicker(editYear.startDate,'startDate',editYear,yStartDate,restrictedDate);
	initEditAcademicYearDatePicker(editYear.endDate,'endDate',editYear,yEndDate,restrictedDate);

	$(editYear.name).val(yName);
	$(editYear.id).val(yId);
	hideDiv($(editYear.alertDiv));
	if(yearType=="current"){
	$(editYear.modalTitle).text("Edit Current Academic Year : "+yName);
	}else{
		$(editYear.modalTitle).text("Edit Next Academic Year : "+yName);	
	}
	$(editYear.modal).modal();
	spinner.hideSpinner();
}



var fnEditAcademicYear=function(){
	spinner.showSpinner();
	var editYearId=$(editYear.id).val();
	var yearStartDate=$(editYear.startDate).datepicker('getFormattedDate');
	var yearEndDate=$(editYear.endDate).datepicker('getFormattedDate');
	var editYname=$(editYear.name).val();
	var jsonData={
			"academicYear":editYname,
			"academicYearId":editYearId,
			"startDate":yearStartDate,
			"endDate":yearEndDate,


	};
	var data=customAjaxCalling("academicYear", jsonData, "PUT")

	if(data.response=="shiksha-200"){

		spinner.hideSpinner();
		displaySuccessMsg();	
		$(editYear.modal).modal("hide");
		setTimeout(function() {   //calls click event after a one sec
		var myFlagEdit = AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+data.responseMessage+'</p>',
			    close :'auto'
			})	
			myFlagEdit.addEventListener('aui-flag-close', function() {
				location.reload();
			});
			
			}, 1000);		
		
	}		
	else {
		showErrorMessage(editYear,data.responseMessage);
	}

}



var ay={
		init:function(){
			$('#initUpgrade').on('click',ay.invokeUpgrade);
			$('#upgradeAy').on('click',ay.upgradeAcademicYear);
		},
		invokeUpgrade : function(){
			hideDiv($('#mdlUpgradeAY #alertdiv'));
			$('#mdlUpgradeAY').modal();
		},
		upgradeAcademicYear : function(){
			spinner.showSpinner();
			var resp =ay.ajaxCall('academicyear/upgrade',null,'GET');
			if(resp=="shiksha-200"){
				$('#mdlUpgradeAY').modal('hide');
				setTimeout(function() {   //calls click event after a one sec
					var myFlag= AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>Information saved.</p>',
					    close :'auto'
					})
					myFlag.addEventListener('aui-flag-close', function() {
						window.location='academicyear';	
					});
					}, 1000);
			

			}else{
				showDiv($('#mdlUpgradeAY #alertdiv'));
				$("#mdlUpgradeAY").find('#errorMessage').show();
				$("#mdlUpgradeAY").find('#exception').show();
				$("#mdlUpgradeAY").find('#exception').text(resp);
				spinner.hideSpinner();
			}

		},
		ajaxCall :function(URL, jsonData, requestType){
			spinner.showSpinner();
			var response=null;
			$.ajax({
				type : requestType,
				url : URL,
				contentType:"application/json",
				async : false,
				data: JSON.stringify(jsonData),
				success : function(data) {
					response=data;
				},
				error : function(data) {
					response =data;
					spinner.hideSpinner();
				}
			});
			return response;
		}

};




$(function() {

	ay.init();

	colorClass = $('#t0').prop('class');
	thLabels =['#','Academic Year ','Start Date  ','End Date ','Status ',' Action '];
	fnSetDTColFilterPagination('#ayTable',6,[0,5],thLabels);

	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#ayTable',[]);
	});
	fnUnbindDTSortListener();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$(".outer-loader").hide();
	applyPermissions();

	$(editYear.form).formValidation({
		framework : 'bootstrap',
		icon : {
			validating : 'glyphicon glyphicon-refresh'
		},
		excluded : ':hidden',
		fields : {
			startDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},
			endDate : {
				validators : {
					notEmpty : {
						message : ' '
					},
				}
			},

		}
	}).on('changed.fu.datepicker dateClicked.fu.datepicker',function(e) {
		var startDate = $(editYear.startDate).datepicker('getFormattedDate');
		var endDate = $(editYear.endDate).datepicker('getFormattedDate');
		var isValidDate=validateEditEndDate(e,editYear);
		hideDiv($(editYear.alertDiv));
		
		if (startDate != "Invalid Date") {
			$(editYear.form).data('formValidation').updateStatus('startDate', 'VALID');

		}
		if (endDate != "Invalid Date") {
			if(isValidDate){
				$(editYear.msgDiv).css('display','none');
				$(editYear.submit).prop('disabled',false);
				$(editYear.form).data('formValidation').updateStatus('endDate', 'VALID');
			}else{
				$(editYear.submit).prop('disabled',true);
				$(editYear.msgDiv).css('display','block');
				$(editYear.form).data('formValidation').updateStatus('endDate', 'INVALID');
			}
		}
	});

	

	//////////////////// Reset from when hidden ///////////////////////////////////
	$(editYear.modal).on("hide.bs.modal", function () {
		resetFormById('frmEditAcademicYear');
		$(editYear.msgDiv).hide();
		$(editYear.startDate).show();
		$(editYear.endDate).show();
	});
	
});