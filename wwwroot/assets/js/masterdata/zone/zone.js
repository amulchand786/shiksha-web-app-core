var  colorClass='';
//global variables
var resetButtonObj;
var allInputEleNamesForZone =[];
var btnAddMoreZoneId;
var deleteZoneId;
var	deleteZoneName;
var elementIdMap;
var thLabels;

//initialize global variables
var initGloabalVarsZone =function(){
	resetButtonObj='';
	allInputEleNamesForZone=['txt_addZoneName'];
	btnAddMoreZoneId ='';
	deleteZoneId='';
}


//smart filter utility--- start here

var initSmartFilter = function(type) {
	

	if (type == "add"){
		/*displaySmartFilters("add")*/

		resetFvForAllInputExceptLoc("form_addZone",allInputEleNamesForZone);
		resetFormValidatonForLoc("form_addZone","stateId");
		
		$('#mdlAddZone #statesForZone').multiselect('destroy')
		/*bindStateToListBox($('#mdlAddZone #statesForZone'));*/
		fnGetStates($('#mdlAddZone #statesForZone'));
		setOptionsForAllMultipleSelect($('#mdlAddZone #statesForZone'))
		fnCollapseMultiselect();
	}else
	{		
		resetFvForAllInputExceptLoc("form_editZone",['editZoneName']);
		//displaySmartFilters("edit")
		$('#mdlEditZone #statesForZone').multiselect('destroy')
		//bindStateToListBox($('#mdlEditZone #statesForZone'));
		fnGetStates($('#mdlEditZone #statesForZone'));
		setOptionsForAllMultipleSelect($('#mdlEditZone #statesForZone'))
		
	}

}

//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
				"State" : '#mdlAddZone #statesForZone',

		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
				"State" : '#mdlEditZone #statesForZone',

		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	;
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};

var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}

//reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('form_addZone');
	resetFormById('form_editZone');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}

//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}



//create a row after save
var getZoneRow =function(zone){
	var row = 
		"<tr id='"+zone.zoneId+"' data-id='"+zone.zoneId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+zone.stateId+"'	title='"+zone.stateName+"'>"+zone.stateName+"</td>"+
		"<td data-zoneid='"+zone.zoneId+"'	title='"+zone.zoneName+"'>"+zone.zoneName+"</td>"+

		"<td data-zonecode='"+zone.zoneCode+"' title='"+zone.zoneCode+"'>"+zone.zoneCode+"</td>"+		

		"<td><div class='div-tooltip'><a class='tooltip tooltip-top' id='btnEditZone'" +
		"onclick=editZone(&quot;"+zone.stateId+"&quot;,&quot;"+zone.zoneId+"&quot;,&quot;"+escapeHtmlCharacters(zone.zoneName)+"&quot;); > <span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a style='margin-left:10px;' class='tooltip tooltip-top' id=btn_deleteZone	onclick=deleteZone(&quot;"+zone.zoneId+"&quot;,&quot;"+escapeHtmlCharacters(zone.zoneName)+"&quot;);><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"+
		"</tr>";

	return row;
}


//permissions for edit and delete zones
var applyPermissions=function(){
	if (!editZonePermission){
	$("#btnEditZone").remove();
		}
	if (!deleteZonePermission){
	$("#btn_deleteZone").remove();
		}
	}

//remove and reinitialize smart filter

//smart filter utility--- end here

//perform Crud

//add Zone
var addZoneData = function(event) {
	$(".outer-loader").show();
	var zoneName = $('#mdlAddZone #txt_addZoneName').val().trim().replace(/\u00a0/g," ");
	var stateId = $("#mdlAddZone #statesForZone option:selected").val();
	var json = {
			"zoneName" : zoneName,
			"stateId" : stateId
	};


	//update datatable without reloading the whole table
	//save&add new
	btnAddMoreZoneId =$(event.target).data('formValidation').getSubmitButton().data('id');

	var zone = customAjaxCalling("zone", json, "POST");
	if(zone!=null){
		if(zone.response=="shiksha-200"){


			var newRow =getZoneRow(zone);
			appendNewRow("tblZone",newRow);
			applyPermissions();
			fnUpdateDTColFilter('#tblZone',[],5,[0,4],thLabels);
			
			setShowAllTagColor(colorClass);

			if(btnAddMoreZoneId=="addZoneSaveMoreButton"){
				$('#alertFailureMessageAddZone').hide();				
				resetFvForAllInputExceptLoc("form_addZone",allInputEleNamesForZone);
				fnDisplaySaveAndAddNewElement('#mdlAddZone',zoneName);
			}else{			
				$('#mdlAddZone').modal("hide");
				$('#alertFailureMessageAddZone').hide();				
				setTimeout(function() {   //calls click event after a one sec
					 AJS.flag({
					    type: 'success',
					    title: validationMsg.sucessAlert,
					    body: '<p>'+ zone.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);
				
				
				
			}		
		}else {
			
			$("#mdlAddZone").find('#successMessage').hide();
			$("#mdlAddZone").find('#okButton').hide();
			$("#mdlAddZone").find('#errorMessage').show();
			$('#alertFailureMessageAddZone').show();
			$("#mdlAddZone").find('#exception').show();
			$("#mdlAddZone").find('#buttonGroup').show();
			$("#mdlAddZone").find('#exception').text(zone.responseMessage);
		}
	}
	$(".outer-loader").hide();

};




//edit
//get data for edit
var editStateId = 0;
var editZoneId = 0;
var editZone = function(stateId, zoneId, zoneName) {
	initSmartFilter("edit");
	var idMap = {
			'#mdlEditZone #statesForZone' : stateId,
	};
	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
					.prop('selected', true);
				enableMultiSelect(ele_key);
			});
	$('#mdlEditZone #editZoneName').val(zoneName);
	editZoneId = zoneId;
	$("#mdlEditZone").modal().show();
	fnCollapseMultiselect();
};

//edit city

var editZoneData = function() {
	$(".outer-loader").show();
	
	var zoneName = $('#editZoneName').val().trim().replace(/\u00a0/g," ");
	var stateId = $(".edit #statesForZone option:selected").val();
	var json = {
			"zoneId" : editZoneId,
			"zoneName" : zoneName,
			"stateId" : stateId
	};


	var zone = customAjaxCalling("zone", json, "PUT");
	if(zone!=null){
		if(zone.response=="shiksha-200"){
			
			deleteRow("tblZone",zone.zoneId);
			var newRow =getZoneRow(zone);
			appendNewRow("tblZone",newRow);
			applyPermissions();
			fnUpdateDTColFilter('#tblZone',[],5,[0,4],thLabels);
		
			setShowAllTagColor(colorClass);

			displaySuccessMsg();	

			$('#mdlEditZone').modal("hide");
			
			 AJS.flag({
			    type: 'success',
			    title: validationMsg.sucessAlert,
			    body: '<p>'+zone.responseMessage+'</p>',
			    close :'auto'
			});
		}		
		else {
		
			showDiv($('#mdlEditZone #alertdiv'));
			$("#mdlEditZone").find('#successMessage').hide();
			$("#mdlEditZone").find('#errorMessage').show();
			$("#mdlEditZone").find('#exception').show();
			$("#mdlEditZone").find('#buttonGroup').show();
			$("#mdlEditZone").find('#exception').text(zone.responseMessage);
		}
	}
	$(".outer-loader").hide();

};



//getting data for delete

var deleteZone = function(zoneId, zoneName) {
	deleteZoneId = zoneId;
	deleteZoneName = zoneName;
	$('#deleteZoneMessage').text(columnMessages.deleteConfirm.replace("@NAME",  deleteZoneName)+" ?");
	$("#mdlDelZone").modal().show();
	hideDiv($('#mdlDelZone #errorMessage'));
};

//delete zone



$(document).on(
		'click',
		'#btn_deleteZoneDelete',
		function() {
			$(".outer-loader").show();
			var zone = customAjaxCalling("zone/" + deleteZoneId,
					null, "DELETE");
			if(zone!=null){
				if(zone.response=="shiksha-200"){
					$(".outer-loader").hide();						
					// Delete Row from data table and refresh the table without refreshing the whole table
					deleteRow("tblZone",deleteZoneId);
					fnUpdateDTColFilter('#tblZone',[],5,[0,4],thLabels);			
					
					setShowAllTagColor(colorClass);

					$('#mdlDelZone').modal("hide");
					AJS.flag({
					    type: 'success',
					    title: validationMsg.sucessAlert,
					    body: '<p>'+zone.responseMessage+'</p>',
					    close :'auto'
					});
					
				} else {
					showDiv($('#mdlDelZone #alertdiv'));
					$("#mdlDelZone").find('#deleteZoneButtonGroup').show();
					$("#mdlDelZone").find('#errorMessage').show();
					
					$("#mdlDelZone").find('#exception').text(zone.responseMessage);
				}
			}
			$(".outer-loader").hide();
		});


//formvalidation

$(document)
.ready(	function() {
				
			$("#txt_addZoneName" ).keypress(function() {
				hideDiv($('#mdlAddZone #errorMessage'));
			})
			$( "#form_editZone" ).keypress(function() {		
				hideDiv($('#mdlEditZone #errorMessage'));
			})

			initGloabalVarsZone();

			thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.zoneCode,columnMessages.action];
			fnSetDTColFilterPagination('#tblZone',5,[0,4],thLabels);

			
			fnMultipleSelAndToogleDTCol('.search_init',function(){
				fnHideColumns('#tblZone',[]);
			});
			colorClass = $('#t0').prop('class');
			$('#tableDiv').show();
			$( ".multiselect-container" ).unbind( "mouseleave");
			$( ".multiselect-container" ).on( "mouseleave", function() {
				$(this).click();
			});
			setOptionsForAllMultipleSelect($("#mdlAddZone  #statesForZone"));
			setOptionsForAllMultipleSelect($("#mdleditZone  #statesForZone"));
			$('#form_addZone')
			.formValidation(
					{ excluded:':disabled',
						framework : 'bootstrap',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {

							stateId : {
								validators : {
									callback : {
										message : '',
										callback : function(
												value) {
											var regx = /^[^'?\"/\\]*$/;
											return regx
											.test(value);
										}
									}
								}
							},

							txt_addZoneName : {
								validators : {
									stringLength: {
										max: 250,
										 message: validationMsg.nameMaxLength
									},
									callback : {
										message :columnMessages.zone+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
										callback : function(value) {
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx
											.test(value);
										}
									}
								}
							},
						}
					}).on('err.form.fv', function(e) {
						e.preventDefault();
					}).on('success.form.fv', function(e) {
						// Prevent form submission
						e.preventDefault();
						// Use Ajax to submit form data
						addZoneData(e);
					});

			$('#form_editZone')
			.formValidation(
					{
						excluded:':disabled',
						framework : 'bootstrap',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {

							stateId : {
								validators : {
									callback : {
										message : ' ',
										callback : function(value) {
											
											var regx = /^[^'?\"/\\]*$/;
											return regx
											.test(value);
										}
									}
								}
							},
							editZoneName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.zone+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
										callback : function(value) {
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx
											.test(value);
										}
									}
								}
							}
						}
					}).on('err.form.fv', function(e) {
						e.preventDefault();
					}).on('success.form.fv', function(e) {
						// Prevent form submission
						e.preventDefault();
						// Use Ajax to submit form data
						editZoneData();
					})
		});