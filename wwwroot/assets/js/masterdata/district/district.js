//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForDist =[];
var allInputEleNamesOfFilter =[];
var btnAddMoreDistId;
var deleteDistId;
var deleteDistrictName;
var elementIdMap;
var selectedIds;
var thLabels;
//initialize global variables
var initGloabalVarsDist =function(){
	resetButtonObj='';
	allInputEleNamesForDist=['addDistrictName'];
	allInputEleNamesOfFilter=['stateId','zoneId'];
	btnAddMoreDistId ='';
	deleteDistId='';
}


//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}


//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
				"State" : '#mdlAddDistrict #statesForZone',
				"Zone" : '#mdlAddDistrict #selectBox_zonesByState',
		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
				"State" : '#mdlEditDistrict #statesForZone',
				"Zone" : '#mdlEditDistrict #selectBox_zonesByState',
		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	;
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
}; 

//smart filter utility--- start here
var initSmartFilter = function(type) {
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){
		
		resetFormValidatonForLocFilters("addDistrictForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addDistrictForm",allInputEleNamesForDist);
		displaySmartFilters("add");
		
		fnCollapseMultiselect();
	}else{		
		
		resetFvForAllInputExceptLoc("editDistrictForm",['editDistrictName']);
		displaySmartFilters("edit")
		fnCollapseMultiselect();
	}
}




//smart filter utility--- end here

//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}
//invoking on click of resetLocation confirmation dialog box
//reset the last child location for form validation..&& reset the locations
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	if($(resetButtonObj).parents("form").attr("id")=="addDistrictForm"){
		
		resetFormValidatonForLocFilters("addDistrictForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");
	}else{
		
		resetFormValidatonForLocFilters("addDistrictForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
	}
	
	fnCollapseMultiselect();
	
}


//reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addDistrictForm');
	resetFormById('editDistrictForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}


//create a row after save
var getDistRow =function(district){
	var row = 
		"<tr id='"+district.districtId+"' data-id='"+district.districtId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+district.stateId+"'	title='"+district.stateName+"'>"+district.stateName+"</td>"+
		"<td data-zoneid='"+district.zoneId+"'	title='"+district.zoneName+"'>"+district.zoneName+"</td>"+
		"<td data-districtid='"+district.districtId+"' title='"+district.districtName+"' >"+district.districtName+"</td>"+
		"<td data-distcode='"+district.districtCode+"' title='"+district.districtCode+"'>"+district.districtCode+"</td>"+		

		"<td><div class='div-tooltip'><a class='tooltip tooltip-top'  id='btnEditDist'" +
		"onclick=editDistrictSetData(&quot;"+district.stateId+"&quot;,&quot;"+district.zoneId+"&quot;,&quot;"+district.districtId+"&quot;,&quot;"+escapeHtmlCharacters(district.districtName)+"&quot;);><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top' id=deleteDist	onclick=deleteDistrictData(&quot;"+district.districtId+"&quot;,&quot;"+escapeHtmlCharacters(district.districtName)+"&quot;); ><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12' ></span></></div></td>"+
		"</tr>";

	return row;
}


var applyPermissions=function(){
	if (!editDistrictPermission){
	$("#btnEditDist").remove();
		}
	if (!deleteDistrictPermission){
	$("#deleteDist").remove();
		}
	}
//perform CRUD

//add District
var addDistrict = function(event) {
	$(".outer-loader").show();
	var zoneId = $('#mdlAddDistrict #selectBox_zonesByState').val();
	var districtName = $('#mdlAddDistrict #addDistrictName').val().trim().replace(/\u00a0/g," ");
	var json = {
			"zoneId" : zoneId,
			"districtId" : 0,
			"districtName" : districtName,
	};

	//update data table without reloading the whole table
	//save&add new
	btnAddMoreDistId =$(event.target).data('formValidation').getSubmitButton().data('id');

	var dist = customAjaxCalling("district", json, "POST");
	if(dist!=null){
		if(dist.response=="shiksha-200"){
			$(".outer-loader").hide();

			var newRow =getDistRow(dist);
			appendNewRow("distTable",newRow);
			applyPermissions();			
			fnUpdateDTColFilter('#distTable',[],6,[0,5],thLabels);
			setShowAllTagColor(colorClass);
			displaySuccessMsg();	
			if(btnAddMoreDistId=="addDistrictSaveMoreButton"){
				$("#mdlAddDistrict").find('#errorMessage').hide();				
				resetFvForAllInputExceptLoc("addDistrictForm",allInputEleNamesForDist);
				fnDisplaySaveAndAddNewElement('#mdlAddDistrict',districtName);


			}else{			
				$('#mdlAddDistrict').modal("hide");
				setTimeout(function() {   //calls click event after a one sec
					 AJS.flag({
					    type: 'success',
					    title: validationMsg.sucessAlert,
					    body: '<p>'+ dist.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);					
			}		
		}else {
			showDiv($('#mdlAddDistrict #alertdiv'));
			$("#mdlAddDistrict").find('#successMessage').hide();
			$("#mdlAddDistrict").find('#okButton').hide();
			$("#mdlAddDistrict").find('#errorMessage').show();
			$("#mdlAddDistrict").find('#exception').show();
			$("#mdlAddDistrict").find('#buttonGroup').show();
			$("#mdlAddDistrict").find('#exception').text(dist.responseMessage);
		}
	}
	$(".outer-loader").hide();
};


//edit
//get data for edit zone
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editDistrictSetData = function(stateId, zoneId, districtId, districtName) {
	initSmartFilter("edit");
	var idMap = {
			'#mdlEditDistrict #statesForZone' : stateId,
			'#mdlEditDistrict #selectBox_zonesByState' : zoneId,
	};

	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
					.prop('selected', true);
				enableMultiSelect(ele_key);
			});

	$('#mdlEditDistrict #editDistrictName').val(districtName);
	editDistrictId = districtId;
	$("#mdlEditDistrict").modal().show();
	
	fnCollapseMultiselect();
};

//edit District
var editDistrict = function() {
	var zoneId = $('.editDist  #selectBox_zonesByState').val();
	var districtName = $('#editDistrictName').val().trim().replace(/\u00a0/g," ");
	

	$(".outer-loader").show();
	var json = {
			"zoneId" : zoneId,
			"districtId" : editDistrictId,
			"districtName" : districtName,
	};


	var dist = customAjaxCalling("district", json, "PUT");
	if(dist!=null){
		if(dist.response=="shiksha-200"){
			$(".outer-loader").hide();
			deleteRow("distTable",dist.districtId);
			var newRow =getDistRow(dist);
			appendNewRow("distTable",newRow);
			applyPermissions();
			
			fnUpdateDTColFilter('#distTable',[],6,[0,5],thLabels);
			setShowAllTagColor(colorClass);
			displaySuccessMsg();	

			$('#mdlEditDistrict').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: validationMsg.sucessAlert,
				    body: '<p>'+ dist.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			
		}		
		else {
			showDiv($('#mdlEditDistrict #alertdiv'));
			$("#mdlEditDistrict").find('#successMessage').hide();
			$("#mdlEditDistrict").find('#errorMessage').show();
			$("#mdlEditDistrict").find('#exception').show();
			$("#mdlEditDistrict").find('#buttonGroup').show();
			$("#mdlEditDistrict").find('#exception').text(dist.responseMessage);
		}
	}
	$(".outer-loader").hide();
};

//get data for delete District
var deleteDistrictData = function(districtid, districtName) {
	
	deleteDistId =districtid;
	deleteDistrictName = districtName;
	$('#deleteDistrictMessage').text(columnMessages.deleteConfirm.replace("@NAME",districtName)+ " ?");
	$("#mdlDelDistrict").modal().show();
	hideDiv($('#mdlDelDistrict #alertdiv'));

};



//delete District
$(document).on('click', '#deleteDistrictButton', function() {
	
	$(".outer-loader").show();

	var dist = customAjaxCalling("district/"+deleteDistId, null, "DELETE");
	if(dist.response=="shiksha-200"){
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("distTable",deleteDistId);
		fnUpdateDTColFilter('#distTable',[],6,[0,5],thLabels);	
		
		setShowAllTagColor(colorClass);

		$('#mdlDelDistrict').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: validationMsg.sucessAlert,
			    body: '<p>'+ dist.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
	} else {
		showDiv($('#mdlDelDistrict #alertdiv'));
		$("#mdlDelDistrict").find('#errorMessage').show();
		$("#mdlDelDistrict").find('#exception').text(dist.responseMessage);
		$("#mdlDelDistrict").find('#buttonGroup').show();
	}
	$(".outer-loader").hide();
});

//form validation

$(function() {
	
	thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.district,columnMessages.districtCode,columnMessages.action];
	fnSetDTColFilterPagination('#distTable',6,[0,5],thLabels);

	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#distTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$( "#addDistrictName" ).keypress(function() {		
		hideDiv($('#mdlAddDistrict #alertdiv'));
	})
	$( "#editDistrictForm" ).keypress(function() {		
		hideDiv($('#mdlEditDistrict #alertdiv'));
	})
	initGloabalVarsDist();


	$("#addDistrictForm").formValidation({		
		excluded:':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {

			addDistrictName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: validationMsg.nameMaxLength
                    },
					callback : {
						message :columnMessages.district+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),

						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);
						}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addDistrict(e);

	});
	$("#editDistrictForm").formValidation({
		excluded:':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			zoneId : {
				validators : {
					stringLength: {
                        max: 250,
                        message: validationMsg.nameMaxLength
                    },
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var options = $('.editDist  #selectBox_zonesByState').val();
					
							return (options != "NONE");
						}
					}
				}
			},
			editDistrictName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: validationMsg.nameMaxLength
                    },
					callback : {
						message :columnMessages.district+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);
						}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		editDistrict();

	});
});
