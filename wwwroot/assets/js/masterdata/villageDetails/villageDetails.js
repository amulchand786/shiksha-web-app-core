var pageContextElements = {
	table : $('#villageDetailsTable'),
	addBtn : $("#addVillageDetails"),
	tableBody : $("#tableBody"),
	eleToggleCollapse		: '[data-toggle="collapse"]',
};
var viewModal = {
	modal : $('#mdlViewVillageDetails')
};
var editModal = {
	editDetailsLink : "#editDetailsLink",
	viewDetailsLink : "#viewDetailsLink",
	editDetailsDiv : ".details-edit-form",
	viewDetailsDiv : ".view-village-details",
	saveBtn : "#editVillageDetailsSaveButton",
	modal : $('#mdlEditVillageDetails'),
	form : $('#editVillageDetailsForm'),
	eleVillageId : $("#editVillageId"),
	eleVillageDetailId : $("#editVillageDetailId"),
	elePradhan : $("#editPradhan"),
	eleAganwari : $("#editAganwari"),
	eleAganwariWorker : $('#editAganwariWorker'),
	eleAshaWorker : $('#editAshaWorker'),
	eleTotalMales : $('#editTotalMales'),
	eleTotalFemales : $('#editTotalFemales'),
	eleTotalPopulation : $('#editTotalPopulation'),
	eleNumberOfHouses : $('#editNumberOfHouses'),
	eleNumberOfFamilies : $('#editNumberOfFamilies'),
	eleNumberOfGovtSchools : $('#editNumberOfGovtSchools'),
	eleNumberOfPvtSchools : $('#editNumberOfPvtSchools'),
	eleNumberOfNgos : $('#editNumberOfNgos'),
	eleNumberOfCooperatives : $('#editNumberOfCooperatives'),
	eleNumberOfSHGs : $('#editNumberOfSHGs'),
	elePecentageLiteracy : $('#editPecentageLiteracy'),
	elePercentageOfPopulationBelowFive : $('#editPercentageOfPopulationBelowFive'),
	elePercentageOfPopulationBetweenFiveToFourteen : $('#editPercentageOfPopulationBetweenFiveToFourteen'),
	elePercentageOfPopulationBetweenFourteenToThirtyFive : $('#editPercentageOfPopulationBetweenFourteenToThirtyFive'),
	elePercentageOfPopulationBetweenThirtyFiveToFifty : $('#editPercentageOfPopulationBetweenThirtyFiveToFifty'),
	elePercentageOfPopulationBetweenFiftyToSeventy : $('#editPercentageOfPopulationBetweenFiftyToSeventy'),
	elePercentageOfPopulationAboveSeventy : $('#editPercentageOfPopulationAboveSeventy'),
	eleGenderRatio : $('#editGenderRatio'),
	eleConnectivityRoad : $('#editConnectivityRoad'),
	eleConnectivityTelephone : $('#editConnectivityTelephone'),
	eleConnectivityMobile : $('#editConnectivityMobile'),
	eleConnectivityInternet : $('#editConnectivityInternet'),
	eleConnectivityPublicTransport : $('#editConnectivityPublicTransport'),
	eleNearestTown : $('#editNearestTown'),
	eleNearestMarket : $('#editNearestMarket'),
	eleVillageReligiousPlaceType : $('#editVillageReligiousPlaceType'),
	eleReligiousCountList : $('#divReligiousCountList'),
	eleAddMoreReligiousPlace : $('#addMoreReligiousPlace'),
	addMoreVillageDetails : $('#addVillgaeDetailsBtn'),
};
var deleteModal = {
	modal : $('#mdlDelVillageDetails'),
};
var religiousTypeCountModal = {
	modal : $('#mdlReligiousTypeCount'),
	eleCount : $("#count"),
	eleCountSave : $("#btnCountReligiousType")
};
var religiousTypeArray = [];
var excludedIds = [];
var editReligiousMapperArray = [];
var jsonDataCall;
var submitActor = null;

var villageDetails = {	
	villageIdForDetails : 0,
	religiousTypeName : "",
	religiousTypeId : 0,
	religoiusPlaces : {},
	deleteVillageReligiousPlaceTypeMapperId : 0,
	init : function() {	
		pageContextElements.addBtn.on('click', this.initAddModal);
		editModal.eleAddMoreReligiousPlace.on('click',this.addMoreReligiousPlace);
		religiousTypeCountModal.eleCountSave.on('click',this.religiousTypeMapper);
		$(editModal.addMoreVillageDetails).on("click",this.addMoreDetailsAction);
		$(document).on("click",'.delete-relegious-details',this.alertForDeleteVillage);
		$(editModal.editDetailsLink+","+editModal.viewDetailsLink).on("click",this.toggleDetailsView);
	},
	toggleDetailsView : function(){
		if($(this).attr("id") == "viewDetailsLink"){
			$(editModal.editDetailsDiv).hide();
			$(editModal.viewDetailsDiv).show();
			$(editModal.editDetailsLink).show();
			$(editModal.viewDetailsLink).hide();
			$(editModal.saveBtn).hide();
		} else {
			$(editModal.editDetailsDiv).show();
			$(editModal.viewDetailsDiv).hide();
			$(editModal.editDetailsLink).hide();
			$(editModal.viewDetailsLink).show();
			$(editModal.saveBtn).show();
		}
	},
	addMoreDetailsAction : function(){
		excludedIds = [];
		if(editModal.eleReligiousCountList.find("tbody tr").length && !editModal.eleReligiousCountList.find("tbody tr:last-child .religious-details").val()){
			return false;
		} else {
			spinner.showSpinner();
			editModal.eleReligiousCountList.find("tbody tr").each(function(){
				excludedIds.push($(this).find(".religious-details").val());
			});
			var religiousTypes = shiksha.invokeAjax("religiousplace/type?exclude=" + excludedIds, null,"GET");
			if(!religiousTypes.length){
				spinner.hideSpinner();
				return false;
			}
			var relId="";
			var row = '<tr data-religiousmapperid><td><select class="form-control religious-details">';
			$.each(religiousTypes,function(index,obj){
				if(index==0){
					relId=obj.religiousPlaceTypeId;
				}
				console.log(relId);
				row+="<option value='"+obj.religiousPlaceTypeId+"'>"+obj.name+"</option>"
			});
				
			row+='</select></td><td><input type="number" name="religiousTypeCountx'+relId+'" class="form-control details-count numberonly" min="1" max="100" value="1"/></td><td><button type="button" class="btn btn-sm btn-danger delete-relegious-details"><span class="glyphicon glyphicon-trash"></span></button></td></tr>';
			editModal.eleReligiousCountList.find("tbody tr").addClass("disabled-row");
			editModal.eleReligiousCountList.find("tbody").append(row);
			spinner.hideSpinner();
		}
	},
	alertForDeleteVillage : function(){
		var villageReligiousPlaceTypeMapperId = $(this).closest("tr").data("religiousmapperid");
		villageDetails.deleteVillageReligiousPlaceTypeMapperId = villageReligiousPlaceTypeMapperId
		if(villageReligiousPlaceTypeMapperId){
			var msg = $("#deleteVillageModalMessage").data("message");
			$("#deleteVillageModalMessage").html(msg.replace('@NAME@',$(this).closest("tr").find("select option:selected").text()));
			$("#deleteCountModal").modal("show");
		} else {
			 $(this).closest("tr").remove();
		}
	},
	deleteVillageDetails: function(){
		spinner.showSpinner();
		var villageDetailsId = $("tr[data-religiousmapperid='"+villageDetails.deleteVillageReligiousPlaceTypeMapperId+"']").data("villageid");
		var religiousTypeId = $("tr[data-religiousmapperid='"+villageDetails.deleteVillageReligiousPlaceTypeMapperId+"']").data("religioustypeid");
		var religiousPlaceRemoveAjaxCall = shiksha.invokeAjax("village/"+ villageDetailsId + "/religiousPlaceType/"+ religiousTypeId, null, "DELETE");
		if (religiousPlaceRemoveAjaxCall != null) {
			if (religiousPlaceRemoveAjaxCall.response == "shiksha-200") {
				$("tr[data-religiousmapperid='"+villageDetails.deleteVillageReligiousPlaceTypeMapperId+"']").remove();				
				AJS.flag({
					type : "success",
					title : appMessgaes.success ,
					body : religiousPlaceRemoveAjaxCall.responseMessage,
					close : 'auto'
				});
			} else {
				// AJS.flag({
				// type : "error",
				// title : appMessgaes.error,
				// body : religiousPlaceRemoveAjaxCall.responseMessage,
				// close :'auto'
				// });
			}
		}
		spinner.hideSpinner();
		$("#deleteCountModal").modal("hide");
	},
	initVillageTableView: function(tableVillageAjax) {
		if (tableVillageAjax.response == "shiksha-200") {
			$("#state-name").text(tableVillageAjax.stateName);
			$("#zone-name").text(tableVillageAjax.zoneName);
			$("#district-name").text(tableVillageAjax.districtName);
			$("#tehsil-name").text(tableVillageAjax.tehsilName);
			$("#block-name").text(tableVillageAjax.blockName);
			$("#nyayPanchayat-name").text(tableVillageAjax.nyayPanchayatName);
			$("#gramPanchayat-name").text(tableVillageAjax.gramPanchayatName);
			$("#revenueVillage-name").text(tableVillageAjax.revenueVillageName);
			$("#village-name").text(tableVillageAjax.villageName);
			$("#village-code").text(tableVillageAjax.villageCode);
		}
	},
	initTable : function(tableAjax,villageData) {
		pageContextElements.tableBody.children().remove();
		var eleKeyMap = detailsColumn;
		if (tableAjax != null) {
			if (tableAjax.response == "shiksha-200") {
				villageDetails.initVillageTableView(villageData);
				delete tableAjax['response'];
				delete tableAjax['responseMessage'];
				delete tableAjax['villageId'];
				delete tableAjax['villageDetailId'];
				$.each(tableAjax, function(key, value) {
					if (key.toString() == 'villageReligiousPlaceTypeMapper' && value) {
						$.each(value, function(k, v) {
							pageContextElements.tableBody.append('<tr><td>'
									+ v.religiousPlaceTypeName + '</td><td>'
									+ v.count + '</td></tr>');
						});
					} else {
						if(key!="responseMessages"){
							var name="";
							switch(value) {
							case 'Yes':
								name=detailsColumn.yes;
								break;
							case 'No':
								name=detailsColumn.no;
								break;
							case 0:
								break;
							default:
								name=value;
							}
						  
							if(key=="connectivityRoad"){
								if(value=='Yes'){
									name=detailsColumn.pakka;
								}else if(value=='No'){
									name=detailsColumn.kacha;
								}
							}
							
							pageContextElements.tableBody.append('<tr id="' + key
									+ '"><td>' + eleKeyMap[key] + '</td><td>'
									+ name + '</td></tr>');
						}
					}
				});
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : tableAjax.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close : 'auto'
			})
		}
	},
	enableMultiSelectEditModal : function(arg) {
		$(arg).multiselect({
							nonSelectedText : 'Add more',
							onChange : function(element, checked) {
								if (checked === true) {
									$(arg).unbind("mouseleave");
									fnCollapseMultiselect;
									religiousTypeName = element.prop('id');
									religiousTypeId = element.val();
									excludedIds.push(religiousTypeId);
									editModal.eleVillageReligiousPlaceType
											.find("#" + religiousTypeName)
											.remove();
									editModal.eleReligiousCountList
											.append('<div class="form-group col-xs-12 ">'
													+ '<label class="control-label" for="'
													+ religiousTypeId
													+ '" ><input type="checkbox" for="'
													+ religiousTypeId
													+ '" onchange="villageDetails.removeRiligiousPlace(\''
													+ religiousTypeId
													+ '\')" checked>'
													+ religiousTypeName
													+ '</label><input class="form-control" type="text" id="'
													+ religiousTypeId
													+ '"></div>');
								}
							}
						});
	},
	initDelete : function(type) {
		deleteModal.modal.modal("show");
	},

	deleteFunction : function() {
		villageDetails.fnDelete(villageDetailId);
	},
	refreshTable : function(table) {
		$(table).bootstrapTable("refresh");
	},
	religiousModal : function() {
		religiousTypeCountModal.modal.modal("show");
		religiousTypeCountModal.eleCount.val("");
	},
	displayReligiousCount : function(mdlId, eleName, eleCount,
			eleReligiousPlaceTypeId) {
		if (!isInfoPanelClosed) {
			$(mdlId).find(addModal.eleCountList).prepend(
					"<li id='" + eleReligiousPlaceTypeId + "'>" + "- "
							+ eleName + "-count" + eleCount + "</li>");
			$(mdlId).find(addModal.eleDivReligiousTypeCount).show();
		}
	},
	religiousTypeMapper : function() {
		var json = {
			"count" : religiousTypeCountModal.eleCount.val(),
			"villageId" : villageIdForDetails,
			"religiousPlaceTypeName" : religiousTypeName,
			"religiousPlaceTypeId" : religiousTypeId
		};
		religiousTypeArray.push(json);
		villageDetails.displayReligiousCount(addModal.modal, religiousTypeName,
				json.count, json.religiousPlaceTypeId);
		religiousTypeCountModal.modal.modal("hide");
	},
	removeRiligiousPlace : function(religiousTypeId) {
		spinner.showSpinner();
		$("#" + religiousTypeId).parent().remove();
		$("#" + religiousTypeId).parent().hide();
		excludedIds = jQuery.grep(excludedIds, function(value) {
			return value != religiousTypeId;
		});
		var religiousPlaceRemoveAjaxCall = shiksha.invokeAjax("village/"
				+ villageIdForDetails + "/religiousPlaceType/"
				+ religiousTypeId, null, "DELETE");
		if (religiousPlaceRemoveAjaxCall != null) {
			if (religiousPlaceRemoveAjaxCall.response == "shiksha-200") {
				console.log("religious place type removed");
				AJS.flag({
					type : "success",
					title : appMessgaes.success ,
					body : religiousPlaceRemoveAjaxCall.responseMessage,
					close : 'auto'
				});
			} else {
				// AJS.flag({
				// type : "error",
				// title : appMessgaes.error,
				// body : religiousPlaceRemoveAjaxCall.responseMessage,
				// close :'auto'
				// });
			}
		}
		spinner.hideSpinner();
	},
	resetForm : function(formId){
		$(formId).find("label.error").remove();
		$(formId).find(".error").removeClass("error");
		$(formId)[0].reset();
	},
	initEdit : function(villageId) {
		$(editModal.editDetailsDiv).hide();
		$(editModal.viewDetailsDiv).show();
		$(editModal.editDetailsLink).show();
		$(editModal.viewDetailsLink).hide();
		 $(editModal.modal).css('overflow', 'hidden');
		$(editModal.saveBtn).hide();
		$(editModal.modal).modal("show");
		 villageDetails.resetForm(editModal.form);
		editModal.form.trigger("reset");
		excludedIds = [];
		editReligiousMapperArray = [];
		editModal.eleReligiousCountList.find("tbody").empty();
		villageIdForDetails = villageId;
		editModal.eleVillageId.val(villageId);
		spinner.showSpinner();
		editModal.eleVillageDetailId.val("");
		villageDetails.religoiusPlaces = shiksha.invokeAjax("religiousplace/type?exclude=" + excludedIds, null,"GET");
		
		var editAjaxCall = shiksha.invokeAjax("village/" + villageId + "/detail", null, "GET");
		var villageTableData=shiksha.invokeAjax("village/" + villageId, null, "GET");
		
		if (editAjaxCall != null) {
			if (editAjaxCall.response == "shiksha-200") {
				editModal.eleVillageId.val(editAjaxCall.villageId);
				editModal.eleVillageDetailId.val(editAjaxCall.villageDetailId);
				editModal.elePradhan.val(editAjaxCall.nameOfPradhan);
				editModal.eleAganwari.val(editAjaxCall.numberOfAnganwaris!=0?editAjaxCall.numberOfAnganwaris:"");
				editModal.eleAganwariWorker.val(editAjaxCall.nameOfAnganwariWorker);
				editModal.eleAshaWorker.val(editAjaxCall.nameOfAshaWorker);
				editModal.eleTotalMales.val(editAjaxCall.totalMales!=0?editAjaxCall.totalMales:"");
				editModal.eleTotalFemales.val(editAjaxCall.totalFemales!=0?editAjaxCall.totalFemales:"");
				editModal.eleTotalPopulation.val(editAjaxCall.totalPopulation!=0?editAjaxCall.totalPopulation:"");
				editModal.eleNumberOfHouses.val(editAjaxCall.numberOfHouses!=0?editAjaxCall.numberOfHouses:"");
				editModal.eleNumberOfFamilies.val(editAjaxCall.numberOfFamilies!=0?editAjaxCall.numberOfFamilies:"");
				editModal.eleNumberOfGovtSchools.val(editAjaxCall.numberOfGovtSchools!=0?editAjaxCall.numberOfGovtSchools:"");
				editModal.eleNumberOfPvtSchools.val(editAjaxCall.numberOfPvtSchools!=0?editAjaxCall.numberOfPvtSchools:"");
				editModal.eleNumberOfNgos.val(editAjaxCall.numberOfNgos!=0?editAjaxCall.numberOfNgos:"");
				editModal.eleNumberOfCooperatives.val(editAjaxCall.numberOfCooperatives!=0?editAjaxCall.numberOfCooperatives:"");
				editModal.eleNumberOfSHGs.val(editAjaxCall.numberOfSHGs!=0?editAjaxCall.numberOfSHGs:"");
				editModal.elePecentageLiteracy.val(editAjaxCall.pecentageLiteracy!=0?editAjaxCall.pecentageLiteracy:"");
				editModal.elePercentageOfPopulationBelowFive.val(editAjaxCall.percentageOfPopulationBelowFive!=0?editAjaxCall.percentageOfPopulationBelowFive:"");
				editModal.elePercentageOfPopulationBetweenFiveToFourteen.val(editAjaxCall.percentageOfPopulationBetweenFiveToFourteen!=0?editAjaxCall.percentageOfPopulationBetweenFiveToFourteen:"");
				editModal.elePercentageOfPopulationBetweenFourteenToThirtyFive.val(editAjaxCall.percentageOfPopulationBetweenFourteenToThirtyFive!=0?editAjaxCall.percentageOfPopulationBetweenFourteenToThirtyFive:"");
				editModal.elePercentageOfPopulationBetweenThirtyFiveToFifty.val(editAjaxCall.percentageOfPopulationBetweenThirtyFiveToFifty!=0?editAjaxCall.percentageOfPopulationBetweenThirtyFiveToFifty:"");
				editModal.elePercentageOfPopulationBetweenFiftyToSeventy.val(editAjaxCall.percentageOfPopulationBetweenFiftyToSeventy!=0?editAjaxCall.percentageOfPopulationBetweenFiftyToSeventy:"");
				editModal.elePercentageOfPopulationAboveSeventy.val(editAjaxCall.percentageOfPopulationAboveSeventy!=0?editAjaxCall.percentageOfPopulationAboveSeventy:"");
				editModal.eleGenderRatio.val(editAjaxCall.genderRatio);
				
				
				
				editModal.eleConnectivityRoad.prop('checked',editAjaxCall.connectivityRoad=="Yes"?true:false);
				editModal.eleConnectivityTelephone.prop('checked',editAjaxCall.connectivityTelephone=="Yes"?true:false);
				editModal.eleConnectivityMobile.prop('checked',editAjaxCall.connectivityMobile=="Yes"?true:false);
				editModal.eleConnectivityInternet.prop('checked',editAjaxCall.connectivityInternet=="Yes"?true:false);
				editModal.eleConnectivityPublicTransport.prop('checked',editAjaxCall.connectivityPublicTransport=="Yes"?true:false);
				editModal.eleNearestTown.val(editAjaxCall.nearestTown);
				editModal.eleNearestMarket.val(editAjaxCall.nearestMarket);
				if(editAjaxCall.villageReligiousPlaceTypeMapper){
					$.each(editAjaxCall.villageReligiousPlaceTypeMapper,function(index, value) {
						
						var row = '<tr data-religioustypeid="'+value.religiousPlaceTypeId+'" data-villageid="'+editAjaxCall.villageId+'" data-religiousmapperid="'+value.villageReligiousPlaceTypeMapperId+'"><td><select class="form-control religious-details edit-religious-details">';
						$.each(villageDetails.religoiusPlaces,function(index,obj){
							if(obj.religiousPlaceTypeId == value.religiousPlaceTypeId){
								row+="<option value='"+obj.religiousPlaceTypeId+"' selected>"+obj.name+"</option>";
							}
						});
						row+='</select></td><td><input type="number" name="religiousTypeCount'+value.religiousPlaceTypeId+'" class="form-control details-count numberonly" min="1" max="100" value="'+value.count+'"/></td><td><button type="button" class="btn btn-sm btn-danger delete-relegious-details"><span class="glyphicon glyphicon-trash"></span></button></td></tr>';
						editModal.eleReligiousCountList.find("tbody").append(row);	
						villageDetails.religoiusPlaces = $.grep(villageDetails.religoiusPlaces,function(ele){
							return ele.religiousPlaceTypeId != value.religiousPlaceTypeId;
						});
					});
				}
				
			} 
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close : 'auto'
			});
		}
		
		villageDetails.initTable(editAjaxCall,villageTableData);
		
		
		spinner.hideSpinner();
	},
	fnUpdate : function() {
		
		var totalMales= editModal.eleTotalMales.val()!=""?editModal.eleTotalMales.val():0;
		var totalFemales=editModal.eleTotalFemales.val()!=""?editModal.eleTotalFemales.val():0;
		var total=totalFemales!=0?((totalMales/totalFemales).toFixed(2).replace(/\.0+$/,''))+" : 1 ": totalMales+" : 0";
		
		var ajaxData = {
			"villageId" : editModal.eleVillageId.val(),
			"villageDetailId" : editModal.eleVillageDetailId.val(),
			"nameOfPradhan" : editModal.elePradhan.val(),
			"numberOfAnganwaris" : editModal.eleAganwari.val(),
			"nameOfAnganwariWorker" : editModal.eleAganwariWorker.val(),
			"nameOfAshaWorker" : editModal.eleAshaWorker.val(),
			"totalMales" : editModal.eleTotalMales.val(),
			"totalFemales" : editModal.eleTotalFemales.val(),
			"totalPopulation" : editModal.eleTotalPopulation.val(),
			"numberOfHouses" : editModal.eleNumberOfHouses.val(),
			"numberOfFamilies" : editModal.eleNumberOfFamilies.val(),
			"numberOfGovtSchools" : editModal.eleNumberOfGovtSchools.val(),
			"numberOfPvtSchools" : editModal.eleNumberOfPvtSchools.val(),
			"numberOfNgos" : editModal.eleNumberOfNgos.val(),
			"numberOfCooperatives" : editModal.eleNumberOfCooperatives.val(),
			"numberOfSHGs" : editModal.eleNumberOfSHGs.val(),
			"pecentageLiteracy" : editModal.elePecentageLiteracy.val(),
			"percentageOfPopulationBelowFive" : editModal.elePercentageOfPopulationBelowFive.val(),
			"percentageOfPopulationBetweenFiveToFourteen" : editModal.elePercentageOfPopulationBetweenFiveToFourteen.val(),
			"percentageOfPopulationBetweenFourteenToThirtyFive" : editModal.elePercentageOfPopulationBetweenFourteenToThirtyFive.val(),
			"percentageOfPopulationBetweenThirtyFiveToFifty" : editModal.elePercentageOfPopulationBetweenThirtyFiveToFifty.val(),
			"percentageOfPopulationBetweenFiftyToSeventy" : editModal.elePercentageOfPopulationBetweenFiftyToSeventy.val(),
			"percentageOfPopulationAboveSeventy" : editModal.elePercentageOfPopulationAboveSeventy.val(),
			"genderRatio" : total,
			"connectivityRoad" : editModal.eleConnectivityRoad.is(':checked'),
			"connectivityTelephone" : editModal.eleConnectivityTelephone.is(':checked'),
			"connectivityMobile" : editModal.eleConnectivityMobile.is(':checked'),
			"connectivityInternet" : editModal.eleConnectivityInternet.is(':checked'),
			"connectivityPublicTransport" : editModal.eleConnectivityPublicTransport.is(':checked'),
			"nearestTown" : editModal.eleNearestTown.val(),
			"nearestMarket" : editModal.eleNearestMarket.val(),
			
		}
		var villageReligiousDetails = []; 
		editModal.eleReligiousCountList.find("tbody").find("tr").each(function(index,row){
			var villageDetailsMapperId = $(this).data('religiousmapperid') ? $(this).data('religiousmapperid') : null;
			var villageId = editModal.eleVillageId.val();
			var count = $(this).find(".details-count").val();
			if(!count){
				$(this).find(".details-count").parent().addClass("has-error");
			}
			var religiousPlaceTypeId = $(this).find(".religious-details").val();
			villageReligiousDetails.push({
				villageReligiousPlaceTypeMapperId : villageDetailsMapperId,
				religiousPlaceTypeId : religiousPlaceTypeId,
				villageId : villageId,
				count : count
			});
		});
		if(editModal.eleReligiousCountList.find("tbody").find("td.has-error").length){
			return false;
		}
		spinner.showSpinner();
		ajaxData['villageReligiousPlaceTypeMapper'] = villageReligiousDetails;
		
		console.log(ajaxData);
		if(!editModal.eleVillageDetailId.val()){
			var addAjaxCall = shiksha.invokeAjax("village/detail", ajaxData, "POST");
			if (addAjaxCall != null) {
				if (addAjaxCall.response == "shiksha-200") {
					$(editModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : addAjaxCall.responseMessage,
						close : 'auto'
					})
					
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		} else {
			var updateAjaxCall = shiksha.invokeAjax("village/detail", ajaxData,"PUT");
			spinner.hideSpinner();
			if (updateAjaxCall != null) {
				if (updateAjaxCall.response == "shiksha-200") {
					$(editModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : updateAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : updateAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close : 'auto'
				}, function() {
					window.location.reload();
				})
			}
		}
	},

	formValidate : function(validateFormName) {
		$(validateFormName)
				.submit(function(e) {
					e.preventDefault();
				})
				.validate(
						{
							ignore : '',
							rules : {

								nameOfPradhan : {
									required : true,
									maxlength : 255,
									noSpace : true
								},
								nameOfAganwari : {
									required : true,
									
								},
								nameOfAganwariWorker : {
									required : true,
									maxlength : 255,
									noSpace : true
								}
							},

							messages : {
								nameOfPradhan : {
									required : detailValidationMsg.detailNameRequired.replace("@NAME",detailsColumn.nameOfPradhan),
									noSpace : detailValidationMsg.detailNoSpace,
									maxlength : detailValidationMsg.detailNameMaxLength.replace("@NAME",detailsColumn.nameOfPradhan),

								},
								nameOfAganwari : {
									required : detailValidationMsg.detailNameRequired.replace("@NAME",detailsColumn.numberOfAnganwaris),
														

								},
								nameOfAganwariWorker : {
									required : detailValidationMsg.detailNameRequired.replace("@NAME",detailsColumn.nameOfAnganwariWorker),
									noSpace : detailValidationMsg.detailNoSpace,
									maxlength : detailValidationMsg.detailNameMaxLength.replace("@NAME",detailsColumn.nameOfAnganwariWorker),
								}
							},
							submitHandler : function() {
								if (validateFormName === editModal.form) {
									villageDetails.fnUpdate();
								}
							}
						});
	},
};

$(document).ready(function() {

	villageDetails.init();

	jQuery.validator.addMethod("noSpace", function(value) {
		return !value.trim().length <= 0;
	}, "");


	 
	villageDetails.formValidate(editModal.form);
	$.validator.messages.min = appMessgaes.minValue;
	$.validator.messages.max =appMessgaes.maxValue;
	$.validator.messages.number= validationMsg.number;

	$("#deleteCountModal").on("shown.bs.modal",function(){
		$(".modal-backdrop:last-child").css({"z-index":"9999"});
	});
	$("#deleteCountModal").on("hidden.bs.modal",function(){
		$("body").addClass("modal-open");
	});
	
	$('.panel').on('hidden.bs.collapse', function () {
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
    });
	
	$('.panel').on('shown.bs.collapse', function () {
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-plus-sign").removeClass("green");
	});
	
	$(document).on("keypress",".numberonly",function (e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && !e.ctrlKey) {
            return false;
        } else {
        	$(this).parent().removeClass("has-error");
        }
    });
	
	$(document).on("input",".maleandfemale",function (e) {
		
		var totalMales= editModal.eleTotalMales.val()!=""?editModal.eleTotalMales.val():0;
		var totalFemales=editModal.eleTotalFemales.val()!=""?editModal.eleTotalFemales.val():0;
		var total=totalFemales!=0?((totalMales/totalFemales).toFixed(2).replace(/\.0+$/,''))+" : 1 ": totalMales+" : 0";
		
		$(editModal.eleGenderRatio).val(total);
		//$(editModal.eleTotalPopulation).val(parseInt(totalMales)+parseInt(totalFemales));
    });
	
	
/*	$(document).on("keypress",".decimalonly",function (e) {
        //if the letter is not digit then display error and don't type anything
		if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57))){
            return false;
        } else {
        	$(this).parent().removeClass("has-error");
        }
    });*/
});
