var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		grade	: "#divSelectGrade",
		section	: "#divSelectSection",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		eleGrade			: "#selectBox_grade",
		eleSection			: "#selectBox_Section",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
		divFilter   		: '#divFilter',
		eleLocType  		: '#divSchoolLocType input:radio[name=locType]',
		filter				: '#locColspFilter',
		panelLink			: '#locationPnlLink',
		form				: '#listDeviceForm',
		eleCollapseFilter	: '#collapseFilter',
		eleToggleCollapse	: '[data-toggle="collapse"]',
		resetModal 			: '#mdlResetLoc',
		resetConfirm 		: '#btnResetLocYes',
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var locationUtility={
		fnInitLocation :function(){
			$(locationPanel.divFilter).css('display','none');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(locationPanel.resetModal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			$(locationPanel.eleGrade).multiselect('destroy').multiselect('refresh');
			$(locationPanel.eleSection).multiselect('destroy').multiselect('refresh');
			$(locationPanel.section).hide();
			$(locationPanel.grade).hide();
			
			if ($(resetButtonObj).parents("form").attr("id") == 'locationFilterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode = $(mapFilter.locTypeSelect).val();
				var aLLevelName =$(mapFilter.locTypeSelect).find("option:selected").data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
			}$(locationPanel.school).val()
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){			
				elementIdMap = {
						"State" 	: '#statesForZone',
						"Zone" 		: '#selectBox_zonesByState',
						"District" 	: '#selectBox_districtsByZone',
						"Tehsil" 	: '#selectBox_tehsilsByDistrict',
						"Block" 	: '#selectBox_blocksByTehsil',
						"City" 		: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" 	: '#selectBox_villagebyRv',
				};
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));				
				locationUtility.reinitializeSmartFilter(elementIdMap);				
				var lLocTypeCode = $(mapFilter.locTypeSelect).val();
				var lLLevelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');				
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));				
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(
						function(ind, ele_key) {
							$(elementIdMap[ele_key]).find("option:selected").prop(
									'selected', false);
							$(elementIdMap[ele_key]).multiselect('destroy');
							enableMultiSelect(elementIdMap[ele_key]);
						});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#btnsRow,#divFilter').show();
				}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});			
			$('#rowDiv1,#divFilter').show();			
			spinner.hideSpinner();
		},
};

var mapFilter = {
		locTypeSelect : "#filterBox_locType",
		villageDiv : "#filter-villagestDiv",
		villageSelect : "#filterBox_village",
		citySelect : "#filterBox_city",
		cityDiv : "#filter-citytDiv",
		filterIcon : "#smart-filter",
		locFilteModal : "#mdlLocationFilter",
		applyBtn : "#locFilterApplyBtn",
		init : function(){
			this.enableMultiSelectForFilters(this.locTypeSelect);
			this.enableMultiSelectForFilters(this.villageSelect);
			this.enableMultiSelectForFilters(this.citySelect);
			$(this.locTypeSelect).on("change",this.locaTypeChange);
			$(this.filterIcon).on("click",this.smartFilterInit);
			
			$("#selectBox_cityByBlock").on("change",function(){
				$(mapFilter.citySelect).val($(this).val());
				$(mapFilter.applyBtn).show();
			});
			$("#selectBox_villagebyRv").on("change",function(){
				$(mapFilter.villageSelect).val($(this).val());
				$(mapFilter.applyBtn).show();
			});
			$(this.villageSelect).on("change",mapperObj.showSchools);
			$(this.citySelect).on("change",mapperObj.showSchools);
			$(mapFilter.applyBtn).on("click",function(){
				mapperObj.showSchools();
			});
		},
		smartFilterInit : function(){
			$(mapFilter.applyBtn).hide();
			mapFilter.setLocations();
			$(mapFilter.locFilteModal).modal("show");
		},
		locaTypeChange : function(){
			if($(this).val() == "R"){
				$(mapFilter.cityDiv).addClass("hide");
				$(mapFilter.villageDiv).removeClass("hide");
			} else {
				$(mapFilter.cityDiv).removeClass("hide");
				$(mapFilter.villageDiv).addClass("hide");
			}
		},
		setLocations : function(){
			$(locationPanel.filter).removeClass('in');
			$(locationPanel.panelLink).text('Select Location');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
			var typeCode =$(this.locTypeSelect).val();	
			var locCode = $(mapFilter.locTypeSelect).val()
			var levelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');
			if(typeCode=='U'){
				showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("U");
				fnCollapseMultiselect();
			}else{
				hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("R");
				fnCollapseMultiselect();						
			}
			$(locationPanel.eleCollapseFilter).css('display','block');
			$(locationPanel.grade).hide();
			$(locationPanel.section).hide();
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
			});	
		}
		
};

var mapperObj = {
		
		schoolListUL : "#schoolsListUL",
		showSchools : function(){
			$(mapFilter.locFilteModal).modal("hide");
			var locType = "villages";
			var locationId = $(mapFilter.locTypeSelect).val() == "R" ? $(mapFilter.villageSelect).val() : $(mapFilter.citySelect).val();
			var ajaxData = {
				locations : [locationId]	
			};
			if($(mapFilter.locTypeSelect).val() == "U"){
				locType = "cities";
			}
			var getAjaxCall = shiksha.invokeAjax("school/"+locType, ajaxData, "POST");
			
			if(getAjaxCall.length){
				if(getAjaxCall[0].response == "shiksha-200"){
					console.log(getAjaxCall);
					$(mapperObj.schoolListUL).empty();
					$.each(getAjaxCall,function(index,school){
					var schoolTemp =' <li class="lms-teacher-school-item" data-schoolid="'+school.id+'"><div class="lms-side-center-far">'
						+'<div class="lms-teacher-school-text">'+school.schoolName+'</div><div class="lms-side-center"><a href="#" class="lms-close-icon">'
						+'<svg><use xlink:href="web-resources/images/teacher.svg#close" href="web-resources/images/teacher.svg#close"></use></svg></a>'
						+'<a href="#" class="lms-expand-icon"><svg><use xlink:href="web-resources/images/teacher.svg#expand" href="web-resources/images/teacher.svg#expand"></use></svg></a></div></div></li>';
						$(mapperObj.schoolListUL).append(schoolTemp);
					});
					
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : getAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		}
}


$(document).ready(function(){
	mapFilter.init();
});