var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		grade	: "#divSelectGrade",
		section	: "#divSelectSection",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		eleGrade			: "#selectBox_grade",
		eleSection			: "#selectBox_Section",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
		divFilter   		: '#divFilter',
		eleLocType  		: '#divSchoolLocType input:radio[name=locType]',
		filter				: '#locColspFilter',
		panelLink			: '#locationPnlLink',
		form				: '#listDeviceForm',
		eleCollapseFilter	: '#collapseFilter',
		eleToggleCollapse	: '[data-toggle="collapse"]',
		resetModal 			: '#mdlResetLoc',
		resetConfirm 		: '#btnResetLocYes',
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var locationUtility={
		fnInitLocation :function(){
			$(locationPanel.divFilter).css('display','none');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(locationPanel.resetModal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			$(locationPanel.eleGrade).multiselect('destroy').multiselect('refresh');
			$(locationPanel.eleSection).multiselect('destroy').multiselect('refresh');
			$(locationPanel.section).hide();
			$(locationPanel.grade).hide();
			
			if ($(resetButtonObj).parents("form").attr("id") == 'locationFilterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode = $(mapFilter.locTypeSelect).val();
				var aLLevelName =$(mapFilter.locTypeSelect).find("option:selected").data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
			}$(locationPanel.school).val()
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){			
				elementIdMap = {
						"State" 	: '#statesForZone',
						"Zone" 		: '#selectBox_zonesByState',
						"District" 	: '#selectBox_districtsByZone',
						"Tehsil" 	: '#selectBox_tehsilsByDistrict',
						"Block" 	: '#selectBox_blocksByTehsil',
						"City" 		: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" 	: '#selectBox_villagebyRv',
				};
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));				
				locationUtility.reinitializeSmartFilter(elementIdMap);				
				var lLocTypeCode = $(mapFilter.locTypeSelect).val();
				var lLLevelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');				
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));				
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(
						function(ind, ele_key) {
							$(elementIdMap[ele_key]).find("option:selected").prop(
									'selected', false);
							$(elementIdMap[ele_key]).multiselect('destroy');
							enableMultiSelect(elementIdMap[ele_key]);
						});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#btnsRow,#divFilter').show();
				}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});			
			$('#rowDiv1,#divFilter').show();			
			spinner.hideSpinner();
		},
};

var mapFilter = {
		locTypeSelect : "#filterBox_locType",
		villageDiv : "#filter-villagestDiv",
		villageSelect : "#filterBox_village",
		citySelect : "#filterBox_city",
		cityDiv : "#filter-citytDiv",
		centerDiv : "#filter-centerDiv",
		centerSelect : "#filterBox_center",
		levelSelect : "#filterBox_level",
		levelDiv : "#filter-levelDiv",
		filterIcon : "#smart-filter",
		locFilteModal : "#mdlLocationFilter",
		applyBtn : "#locFilterApplyBtn",
		tpSelect 		 : "#filterBox_tp",
		searchBtn : ".filter-search-btn",
		init : function(){
			this.enableMultiSelectForFilters(this.locTypeSelect);
			this.enableMultiSelectForFilters(this.villageSelect);
			this.enableMultiSelectForFilters(this.citySelect);
			/*this.enableMultiSelectForFilters(this.centerSelect);
			this.enableMultiSelectForFilters(this.levelSelect);*/
			this.enableMultiSelectForFilters(this.tpSelect);
			
			$(this.locTypeSelect).on("change",this.locaTypeChange);
			$(this.filterIcon).on("click",this.smartFilterInit);
			
			$("#selectBox_cityByBlock").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$("#selectBox_villagebyRv").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$(this.villageSelect).on("change",mapFilter.showCenters);
			$(this.citySelect).on("change",mapFilter.showCenters);
			$(this.centerSelect).on("change",mapFilter.showLevels);
			$(this.levelSelect).on("change",mapperObj.showGroups);
			$(mapFilter.applyBtn).on("click",this.applySmartFilter);
			
			$(this.tpSelect).on("change",this.tpChange);
			$(this.searchBtn).on("click",centerMaperObj.showCenters);
		},
		applySmartFilter :  function(){
			if($(mapFilter.locTypeSelect).val() == "R"){
				var selectedIds = $("#selectBox_villagebyRv").val();
				$(mapFilter.villageSelect).find("option").prop("selected",false);			
				$.each(selectedIds,function(x,val){
					$(mapFilter.villageSelect).find("option[value='"+val+"']").prop("selected",true);
				});
				$(mapFilter.villageSelect).multiselect("rebuild");
			} else {
				var selectedIds = $("#selectBox_cityByBlock").val();
				$(mapFilter.citySelect).find("option").prop("selected",false);
				$.each(selectedIds,function(x,val){
					$(mapFilter.citySelect).find("option[value='"+val+"']").prop("selected",true);
				});
				$(mapFilter.citySelect).multiselect("rebuild");
			}
			$(mapFilter.locFilteModal).modal("hide");
		},
		tpChange : function(){
			$(centerMaperObj.container).hide();
			$(".label-level").show();
			$("#current-level").text($(this).find("option:selected").attr("data-level"));
		},
		smartFilterInit : function(){
			//$(mapFilter.applyBtn).hide();
			mapFilter.setLocations();
			$(mapFilter.locFilteModal).modal("show");
		},
		locaTypeChange : function(){
			if($(this).val() == "R"){
				$(mapFilter.cityDiv).addClass("hide");
				$(mapFilter.villageDiv).removeClass("hide");
				$(mapFilter.villageSelect).val("");
				$(mapFilter.villageSelect).multiselect("rebuild");
			} else {
				$(mapFilter.cityDiv).removeClass("hide");
				$(mapFilter.villageDiv).addClass("hide");
				$(mapFilter.citySelect).val("");
				$(mapFilter.citySelect).multiselect("rebuild");
			}
			$(mapFilter.centerDiv).addClass("hide");
			$(mapFilter.levelDiv).addClass("hide");
			$(mapperObj.subjectListContainer).hide();
		},
		setLocations : function(){
			$(locationPanel.filter).removeClass('in');
			$(locationPanel.panelLink).text(columnMessages.selectLocation);
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
			var typeCode =$(this.locTypeSelect).val();	
			var locCode = $(mapFilter.locTypeSelect).val()
			var levelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');
			if(typeCode=='U'){
				showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("U");
				fnCollapseMultiselect();
			}else{
				hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("R");
				fnCollapseMultiselect();						
			}
			$(locationPanel.eleCollapseFilter).css('display','block');
			$(locationPanel.grade).hide();
			$(locationPanel.section).hide();
		},
		showCenters : function(){
			/*$(mapFilter.centerSelect).empty();
			var locType = "R";
			var locationId = $(mapFilter.locTypeSelect).val() == "R" ? $(mapFilter.villageSelect).val() : $(mapFilter.citySelect).val();
			var ajaxData = {
				locations : [locationId]	
			};
			if($(mapFilter.locTypeSelect).val() == "U"){
				locType = "U";
			}
			var getAjaxCall = shiksha.invokeAjax("center/"+locType, ajaxData, "POST");
			
			if(getAjaxCall.length){
				if(getAjaxCall[0].response == "shiksha-200"){
					$.each(getAjaxCall,function(index,center){
						$(mapFilter.centerSelect).append("<option value='"+center.id+"'>"+center.name+"</option>")
					});
					
				} 
			} 
			$(mapFilter.centerSelect).multiselect("rebuild");
			$(mapFilter.centerDiv).removeClass("hide");
			$(mapFilter.levelDiv).addClass("hide");
			$(mapperObj.subjectListContainer).hide();
			spinner.hideSpinner();*/
		},
		showLevels : function(){
			shikshaPlus.fnGetLevelsByCenter($(mapFilter.levelSelect),0,$(mapFilter.centerSelect).val());
			$(mapFilter.levelSelect).multiselect("rebuild");
			$(mapFilter.levelDiv).removeClass("hide");
			$(mapperObj.subjectListContainer).hide();
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
			});	
		}
		
};

var centerMaperObj = {
		container : "#centerlist-container",
		saveBtn : "#mapSaveBtn",
		table : "#centerMpTbl",
		init : function(){
			$(this.saveBtn).on("click",this.fnSaveMappers);
		},
		showCenters : function(){
			var locationIds = $(mapFilter.locTypeSelect).val() == "R" ? $(mapFilter.villageSelect).val() : $(mapFilter.citySelect).val();
			if(!$(mapFilter.tpSelect).val()){
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : columnMessages.planRequired,
					close :'auto'
				})
				return false;
			} else if(!locationIds || !locationIds.length){
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : columnMessages.locationRequired,
					close :'auto'
				})
				return false;
			}
			$(centerMaperObj.container).show();
			$(centerMaperObj.table).find("tbody").empty();
			if(locationIds.indexOf("multiselect-all") > -1){
				locationIds.splice(0, 1);
			}
			var ajaxData = {
				locations : locationIds,
				locationTypeCode : "R",
				teachingPlanId : $(mapFilter.tpSelect).val()
			};
			if($(mapFilter.locTypeSelect).val() == "U"){
				ajaxData.locationTypeCode = "U";
			}
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/teachingPlan/map", ajaxData, "POST");
			if(getAjaxCall){
				if(getAjaxCall.response == "shiksha-200"){
					$.each(getAjaxCall.centerBatches,function(x,center){
						var centerDisabled=center.center.isTeachingPlanSynced ? "disabled" : "";
						var str = "<tr data-centerid='"+center.center.id+"'><td><input type='checkbox' "+centerDisabled+" class='center-check'/> "+center.center.name+"</td><td>";
							$.each(center.batches,function(y,batch){
								var isMapped = batch.isTeachingPlanMapped ? "checked" : "";
								var isSynced = batch.isTeachingPlanSynced ? "disabled" : "";
								str+='<lable class="section-check"><input type="checkbox" '+isSynced+' class="batch-check-box" data-batch="'+batch.batchId+'" '+isMapped+'/> '+batch.batchName+'</lable>'
							})
							str+="</td></tr>";
							$(centerMaperObj.table).find("tbody").append(str);
					});
					$(".batch-check-box").trigger("change");
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : getAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		fnSaveMappers : function(){
			var ajaxData = {
					centerBatches : []
			};
			$(centerMaperObj.table).find("tbody tr").each(function(z,row){
				var scObj = {
						center : { id : $(this).attr("data-centerid")},
						batches : []
				}
				$(this).find(".batch-check-box").each(function(){
					if($(this).is(":checked")){
						scObj.batches.push({batchId : $(this).attr("data-batch")});
					}
				});
				ajaxData.centerBatches.push(scObj);
			})
			var saveAJAX = shiksha.invokeAjax("shikshaplus/teachingPlan/"+$(mapFilter.tpSelect).val()+"/map", ajaxData, "POST");
			if(saveAJAX){
				if(saveAJAX.response == "shiksha-200"){
					centerMaperObj.showCenters();
					AJS.flag({
						type : "success",
						title :  columnMessages.sucessAlert,
						body : saveAJAX.responseMessage,
						close :'auto'
					})
					
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : saveAJAX.responseMessage,
						close :'auto'
					})
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		}
};

var mapperObj = {
		
		groupListUL : "#groupListUL",
		subjectLi : ".mapper-group",
		subjectListContainer : "#subjectlist-container",
		sectionMapContainer  :"#sectionMapContainer",
		sectionMapContainerMain :"#sectionMapContainerMain",
		mapSaveBtn : "#mapSaveBtn",
		init : function(){
			$(document).on("click",this.subjectLi,this.showGroupBatchMapping);
			$(this.mapSaveBtn).on("click",this.fnSaveMappers);
		},
		showGroupBatchMapping : function(){
			$(mapperObj.sectionMapContainer).empty();
			$(this).siblings().removeClass("active");
			$(this).addClass("active");
			var groupId = $(this).data("groupid");
			var centerId = $(mapFilter.centerSelect).val();
			var levelId = $(mapFilter.levelSelect).val();
			var teachingPlanList = shiksha.invokeAjax("shikshaplus/teachingPlan/level/"+levelId+"/group/"+groupId, null, "GET");
			var ajaxData = {
					"center": {	"id":centerId	},
					"level"	: { "levelId":levelId },
					"group"	: {"groupId":groupId },
					"academicCycle" : {	"academicCycleId" : currentAcId	}
			}
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/teachingPlan/map", ajaxData, "POST");
			console.log(getAjaxCall);
			var mainTemp = $(".map-template").clone();
			$(mainTemp).removeClass("map-template");
			$.each(teachingPlanList,function(index,tplan){
				$(mainTemp).find(".tplan-select").append("<option data-name='"+tplan.name+"' value='"+tplan.id+"'>"+tplan.name+"</option>");
			});
			$.each(getAjaxCall,function(index,mapper){
				var template = $(mainTemp).clone();
				$(template).show();
				$(template).attr("data-batchid",mapper.batch.batchId);
				$(template).attr("data-batchname",mapper.batch.batchName).find(".section-title").text(mapper.batch.batchName);
				$(template).find(".tplan-select").attr("id","tplanSelectBatch"+mapper.batch.batchId);
				$(mapperObj.sectionMapContainer).append($(template));
				$("#tplanSelectBatch"+mapper.batch.batchId).val(mapper.teachingPlanId);
				mapFilter.enableMultiSelectForFilters("#tplanSelectBatch"+mapper.batch.batchId);
			});
			
			$(mapperObj.sectionMapContainerMain).show();
			spinner.hideSpinner();
		},
		showGroups : function(){
			$(mapperObj.sectionMapContainerMain).hide();
			$(mapperObj.groupListUL).empty();
			spinner.showSpinner();
			var centerId = $(mapFilter.centerSelect).val();
			var levelId = $(mapFilter.levelSelect).val();
			var getAjaxcall = shiksha.invokeAjax("group/center/"+centerId+"/level/"+levelId+"/academicCycle/"+currentAcId, null, "GET");
			$.each(getAjaxcall,function(index,group){
				var schoolTemp =' <li class="lms-teacher-school-item mapper-group" data-groupid="'+group.groupId+'"><div class="lms-side-center-far">'
								+'<div class="lms-teacher-school-text">'+group.groupName+'</div><div class="lms-side-center" style="display:none;"><a href="#" class="lms-close-icon">'
				 				+'<svg><use xlink:href="web-resources/images/teacher.svg#close" href="web-resources/images/teacher.svg#close"></use></svg></a>'
				 				+'<a href="#" class="lms-expand-icon"><svg><use xlink:href="web-resources/images/teacher.svg#expand" href="web-resources/images/teacher.svg#expand"></use></svg></a></div></div></li>';
							$(mapperObj.groupListUL).append(schoolTemp);
			});
			$(mapperObj.subjectListContainer).show();
			spinner.hideSpinner();
		},
		fnSaveMappers : function(){
			var ajaxData = [];
			$(mapperObj.sectionMapContainer).find(".section-map-row").each(function(index,row){
				var sectionObj = {
						batch : {
							batchId : $(this).data("batchid"),
							batchName : $(this).data("batchname")
						},
						teachingPlanId  : $(this).find(".tplan-select").val(),
						teachingPlanName  : $(this).find(".tplan-select option:selected").data("name")
				};
				ajaxData.push(sectionObj);
			});
			var centerId = $(mapFilter.centerSelect).val();
			var levelId = $(mapFilter.levelSelect).val();
			var saveAjaxCall = shiksha.invokeAjax("shikshaplus/teachingPlan/center/"+centerId+"/level/"+levelId+"/map", ajaxData, "POST");
			
			if(saveAjaxCall != null){	
				if(saveAjaxCall.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title :  columnMessages.sucessAlert,
						body : saveAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : saveAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		}
		
};


$(document).ready(function(){
	mapFilter.init();
	//mapperObj.init();
	centerMaperObj.init();
	$(".select-all").on("change",function(){
		if($(this).is(":checked")){
			$(".center-check").prop("checked",true);
		} else {
			$(".center-check").prop("checked",false);	
		}
		$(".center-check").trigger("change");
	});
	$(document).on("change",'.center-check',function(){
		if($(".center-check:not(:checked)").length){
			$(".select-all").prop("checked",false);
			
		} else {
			$(".select-all").prop("checked",true);
			
		}	
		if($(this).is(":checked")){
			$(this).closest("tr").find(".batch-check-box").prop("checked",true);
		} else {
			$(this).closest("tr").find(".batch-check-box").prop("checked",false);
		}
	});
	$(document).on("change",".batch-check-box",function(){
		if($(this).closest("tr").find(".batch-check-box:checked").length){
			$(this).closest("tr").find(".center-check").prop("checked",true);
		} else {
			$(this).closest("tr").find(".center-check").prop("checked",false);
		}
		if($(".center-check:not(:checked)").length){
			$(".select-all").prop("checked",false);
			
		} else {
			$(".select-all").prop("checked",true);
			
		}
	})
});