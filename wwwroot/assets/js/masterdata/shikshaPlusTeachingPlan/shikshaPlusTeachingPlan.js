var teachingplanFilterVariables ={
		flag			:0,
		acCycleId		: "",
		centerId		: "",
		groupId			: "",
		levelId			: "",
		batcgId			: ""
};
var startDates=[];
var isPrint = false;
var teachingplanFilter = {
		acCycleSelect 	: "#filterBox_acCycle",
		centersSelect  	: "#filterBox_centers",
		levelSelect 	: "#filterBox_level",
		batchSelect  	: "#filterBox_batch",
		groupSelect		: "#filterBox_group",
		acCycleSelectDiv : "#filter-acCycleDiv",
		centersSelectDiv: "#filter-centersDiv",
		levelSelectDiv 	: "#filter-levelDiv",
		batchSelectDiv	: "#filter-batchDiv",
		groupSelectDiv	: "#filter-groupDiv",
		searchBtn 		: "#filter-search-btn",
		toolbar		 	: "#conteFilterContainer",
		table			: "#teachPlanChapterList",
		teachingPlanId 	: "#teachingPlanId",
		startDate : "#startDate",
		endDate : "#endDate",
		groupLi : ".group-li",
		detailsDownload : "#details-download",
		init : function(){
			/*this.enableMultiSelectForFilters(this.acCycleSelect,"Acamedic Cycle : none",this.accycleChange);
			this.enableMultiSelectForFilters(this.centersSelect,"Centers : none",this.centerChange);
			this.enableMultiSelectForFilters(this.levelSelect,"Level : none",this.levelChange);
			this.enableMultiSelectForFilters(this.batchSelect,"Batch :none",this.batchChange);
			this.enableMultiSelectForFilters(this.groupSelect,"Group : none",this.groupChange);
			$(this.searchBtn).on("click",this.filterData);*/
			$(this.toolbar).show();
			this.filterData();
			$(this.table).on("click",".expand-icon",this.showHideSegments);
			$(this.groupLi).on("click",this.changeGroup);
			$(this.detailsDownload).on("click",createPDF);
			//$("#pdf-download").on("click",createPDF)
		},
		download : function(){
			var teachingPlanId=$("#teachingPlanId").val();
			window.location = baseContextPath +"/shikshaplus/teachingPlan/"+teachingPlanId+"/details/download";
		},
		accycleChange : function(){
			if($(teachingplanFilter.groupSelect).val()){
				teachingplanFilter.filterData();
			}
		},
		showHideSegments : function(){
			if(!isPrint)
				$(this).closest("table").find("tr.tp-activity,tr.tp-day").remove();
			if($(this).closest("tr").hasClass("open") && !isPrint){
				$(this).closest("tr").removeClass("open");
				$(this).closest("table").find("tr").removeClass("open");
			} else {
				if(!isPrint)
					$(this).closest("table").find("tr").removeClass("open");
				$(this).closest("tr").addClass("open");
				teachingplanFilter.showSegments($(this).closest("tr").attr("data-chapterid"));
			}
		},
		showSegments : function(chapterId){
			var lessonplan = customAjaxCalling("shikshaplus/lessonplan/chapter/"+chapterId+"/teachingPlan/"+ $(teachingplanFilter.teachingPlanId).val(),null,"GET");
			if(lessonplan){
				$.each(lessonplan.lessonPlanDays,function(x,dayObj){
					// $("tr[data-chapterid='"+chapterId+"']").last().after("<tr class='tp-day' data-chapterid='"+chapterId+"'><td>Day: "+dayObj.day+"</td><td class='start-date'></td><td class='end-date'></td></tr>");
					 $.each(dayObj.activities,function(y,activity){
						 if(activity.segmentDetails.selectedSegment){
							 $.each(activity.segmentDetails.segments,function(z,obj){
								var actSDate = obj.startDate? obj.startDate : "-";
								var actEDate = obj.endDate ? obj.endDate : "-";
								if(obj.shikshaPlusSegmentId == activity.segmentDetails.selectedSegment){
									$("tr.tp-day").last().find(".start-date").text(actSDate).end().find(".end-date").text(actEDate)
									$("tr[data-chapterid='"+chapterId+"']").last().after("<tr class='tp-activity' data-chapterid='"+chapterId+"'><td class='segment'>"+obj.shikshaPlusSegmentName+" (day "+(x+1)+")</td><td>"+actSDate+"</td><td>"+actEDate+"</td></tr>");
									return false;
								} 
							 });
							 
						 }
					 });
				});
			}
			spinner.hideSpinner();
		},
		centerChange : function(){
			$(teachingplanFilter.searchBtn).hide();
			$(teachingplanFilter.batchSelectDiv).hide();
			$(teachingplanFilter.groupSelectDiv).hide();
			$(teachingplanFilter.table).hide();
			teachingplanFilter.emptySelect(teachingplanFilter.levelSelect);
			teachingplanFilter.emptySelect(teachingplanFilter.batchSelect);
			teachingplanFilter.emptySelect(teachingplanFilter.groupSelect);
			shikshaPlus.fnGetLevelsByCenterinFilter($(teachingplanFilter.levelSelect),0,$(teachingplanFilter.centersSelect).val())
			$(teachingplanFilter.levelSelect).multiselect("rebuild");
			$(teachingplanFilter.levelSelectDiv).show();
		},
		levelChange : function(){
			$(teachingplanFilter.searchBtn).hide();
			$(teachingplanFilter.groupSelectDiv).hide();
			$(teachingplanFilter.table).hide();
			teachingplanFilter.emptySelect(teachingplanFilter.batchSelect);
			teachingplanFilter.emptySelect(teachingplanFilter.groupSelect);
			shikshaPlus.fnGetGroupsByLevelIdInFilter($(teachingplanFilter.groupSelect),0,$(teachingplanFilter.levelSelect).val());
			shikshaPlus.fnGetBatchesByCenterAndLevelInFilter($(teachingplanFilter.batchSelect),0,$(teachingplanFilter.centersSelect).val(),$(teachingplanFilter.levelSelect).val());
			$(teachingplanFilter.batchSelect).multiselect("rebuild");
			$(teachingplanFilter.groupSelect).multiselect("rebuild");
			$(teachingplanFilter.batchSelectDiv).show();
		},
		batchChange : function(){
			$(teachingplanFilter.searchBtn).hide();
			$(teachingplanFilter.groupSelectDiv).show();
		},
		groupChange : function(){
			teachingplanFilter.filterData();
			$(teachingplanFilter.searchBtn).show();
		},
		changeGroup : function(){
			$(teachingplanFilter.groupLi).removeClass("active");
			$(this).addClass("active");
			teachingplanFilter.filterData();
		},

		filterData : function(){
			
			var groupId = $("#group-list").find("li.active").attr("data-groupId")
			var teachingPlanId = $(teachingplanFilter.teachingPlanId).val();  
			var url="shikshaplus/teachingPlan/"+teachingPlanId+"/group/"+groupId+"/details";
			var ajaxCall = shiksha.invokeAjax(url, null, "GET");
			spinner.hideSpinner();
			if(ajaxCall !=null){
				teachingplanFilter.showChaptersData(ajaxCall.shikshaPlusChapters);
			}	
		},
		showChaptersData : function(chapters){
			var str;
			$(teachingplanFilter.table).find("tbody").empty();
			startDates=[];
			var startDate = moment($(teachingplanFilter.startDate).val()).format("DD/MM/YYYY");
			var flag = true;
			$.each(chapters,function(index, value){
				startDates.push(value.startDate);
				if(value.startDate !=null && value.endDate !=null){
					str = "<tr class='tp-chapter' data-chapterid='"+value.chapterId+"'>" 
							+"<td>"
							+'<a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-chapter" data-chapterNumber="'+value.chapterNumber+'">'
							+'<div class="tp-toggle lms-side-center-far"><div class="lms-dropdown-toggle"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#dropdown" href="web-resources/images/content-manager.svg#dropdown"></use></svg></div></div>'
							+'<div><div><div class="log-title-text">'+value.name+'</div></div></div></a>'
							+"</td>" +
							'<td><a data-enddate="'+value.endDate+'" date-today-highlight="false" href="#" id="duration" data-isdetailsexists="'+value.noOfTpDetails+'" data-startDate="'+value.startDate+'" data-chapterNumber="'+value.chapterNumber+'" data-type="date" data-placement="right"  data-title="Start date" class="editable editable-click editable-open start-date-edit" style="">'+value.startDate+'</a></td>'+
							"<td>"+value.endDate+"</td>" +
							"</tr>" ;
					$(teachingplanFilter.table).find("tbody").append(str);
					flag = true;
				}
				else{
					str = "<tr>" +
					"<td>"+value.name+"</td>" +
					'<td><a href="#" id="duration" date-today-highlight="false" data-isdetailsexists="'+value.noOfTpDetails+'"  data-chapterNumber="'+value.chapterNumber+'" data-type="date"  data-placement="right" data-title="Start date" class="editable editable-click editable-open start-date-edit" style=""></a></td>'+
					"<td>---</td>" +
					"</tr>" ;
					$(teachingplanFilter.table).find("tbody").append(str);
					flag = false;
				}
				
				if($('.start-date-edit').length > 1 && flag){
					var date = $($('.start-date-edit')[$('.start-date-edit').length-1]).attr("data-enddate");
					startDate = moment( date ? date : startDate).add(1,"day").format("DD/MM/YYYY");
				}
				
				$('.start-date-edit').last().editable({
					datepicker : {
						startDate 	: startDate,
						endDate		: moment($(teachingplanFilter.endDate).val()).format("DD/MM/YYYY"),
						format		: 'dd/mm/yyyy',
					},
					viewformat : "dd-M-yyyy",
					format:'yyyy-mm-dd',
					clear : false,
					send: 'never',
					validate  : function(value){
						if(!$(".datepicker-days .day.active").length){
							return "Please select date"
						} else if(!value){
							return "Enter Valid date";
						}
					}
			    }).on("save",function(e, params){
			    	teachingplanFilter.startDateParams(params,$(this).attr('data-chapterNumber'),$(this).attr('data-startDate'),$(this).attr('data-isdetailsexists'));
			    });
			});
			
			
		},
		isAlreadyExist: function(value){
			$.each(startDates,function(index,val){
				if(value == val){
					return false;
				}
				else
					return true;
			})
		},
		startDateParams : function(params,chapterNumber,startDate,isDetailsExists){
			if(isDetailsExists==0){
				spinner.showSpinner();
				var data=[];
				$(teachingplanFilter.groupLi).each(function(){
					var obj ={
							group:{
								groupId 	: $(this).attr("data-groupid"),
								groupName : $(this).attr("data-groupname"),						
							},
							startDate 	: params.submitValue,
					};
					
					data.push(obj);
				});
				var teachingPlanId = $(teachingplanFilter.teachingPlanId).val(); 
				var addAjaxCall = shiksha.invokeAjax("shikshaplus/teachingPlan/"+teachingPlanId+"/details", data, "POST");
				spinner.hideSpinner();
				teachingplanFilter.filterData();
				if(addAjaxCall != null){	
					if(addAjaxCall.response == "shiksha-200"){
						AJS.flag({
							type : "success",
							title : appMessgaes.success,
							body : addAjaxCall.responseMessage,
							close :'auto'
						})
					} else if(addAjaxCall.response == "shiksha-800"){
						AJS.flag({
							type : "error",
							title : appMessgaes.error,
							body : validationMsg.noLessonPlan+" : "+addAjaxCall.responseMessages.join(),
							close :'auto'
						});
					} else {
						AJS.flag({
							type : "error",
							title : appMessgaes.error,
							body : addAjaxCall.responseMessage,
							close :'auto'
						});
					}
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.oops,
						body : appMessgaes.serverError,
						close :'auto'
					})
				}
			}
			else{
				teachingplanFilter.fnUpdate(params,chapterNumber);
			}
				
		},
		fnUpdate :function(params,chapterNumber){
			var data;
			$(teachingplanFilter.groupLi).each(function(){
				if($(this).hasClass('active')){
					var group={
							groupId 	: $(this).attr("data-groupid"),
							groupName : $(this).attr("data-groupname"),						
					}
					data={
							group:group,
							startDate	   		: params.submitValue,
							shikshaPlusChapter	: {'chapterNumber' :chapterNumber}
					};
				}
			});
			
			
			
			var teachingPlanId = $(teachingplanFilter.teachingPlanId).val(); 
			var updateAjaxCall = shiksha.invokeAjax("shikshaplus/teachingPlan/"+teachingPlanId+"/details", data, "PUT");
			spinner.hideSpinner();
			teachingplanFilter.filterData();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					})
				} else if(updateAjaxCall.response == "shiksha-800"){
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : validationMsg.noLessonPlan+" : "+updateAjaxCall.responseMessages.join(),
						close :'auto'
					});
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
		},
		emptySelect : function(ele){
			$(ele).empty();
		},
		enableMultiSelectForFilters : function(element,nonSelectedText,onChangeCallback){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				nonSelectedText : nonSelectedText,
				onChange : onChangeCallback
			});	
		},
		fnGetSelectedFiteredValue : function(ele){
			var arr =[];
			var selector = "option" ;
			if($(ele).val()!=null)
				selector = "option:selected";
			$(ele).find(selector).each(function(ind,option){
				if($(option).val()!="multiselect-all"){		
					arr.push(parseInt($(option).val()));
				}
			});
			return arr;
		},
}


function createPDF(){
	isPrint = true;
	$("#teachPlanChapterList tbody").find("tr").removeClass("open");
	$("#teachPlanChapterList").find("tr.tp-activity,tr.tp-day").remove();
	$(".expand-icon").trigger("click");
	var doc = new jsPDF('p', 'pt');
	var name= $("#conteFilterContainer").text().trim()+"_"+$("#group-list li.active").data("groupname");
	var splitTitle = doc.splitTextToSize(name, 500);
	doc.text(40, 30, splitTitle);
	var elem = document.getElementById("teachPlanChapterList");
	var res = doc.autoTableHtmlToJson(elem);
	console.log(doc.getFontList());
	doc.autoTable(res.columns, res.data,{
		startY: splitTitle.length*10+60,
		styles: {overflow: 'linebreak'},
	});

	doc.save(name.substr(0, 100)+".pdf");	
	isPrint = false;

}




$(document).ready(function(){
	$.fn.editable.defaults.mode = 'popup'; 
	teachingplanFilter.init();
	
});