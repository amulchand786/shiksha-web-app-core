var subElements = {};

var selectedIds = [], selectedType = "";
var iscontainsListBox =false; //user at the time of edit modal save for any bool type...

var locationInfoData = {};

var maxLevelType =  "";

var elementIdMap = {
	"State" : '#statesForZone',
	"Zone" : '#selectBox_zonesByState',
	"District" : '#selectBox_districtsByZone',
	"Tehsil" : '#selectBox_tehsilsByDistrict',
	"Block" : '#selectBox_blocksByTehsil',
	"City" : '#selectBox_cityByBlock',
	"Nyaya Panchayat" : '#selectBox_npByBlock',
	"Gram Panchayat" : '#selectBox_gpByNp',
	"Revenue Village" : '#selectBox_revenueVillagebyGp',
	"Village" : '#selectBox_villagebyRv'	
};


var getLocationByTypeAndId = function(id, type){
	var res_location;
	$(locationInfoData[type]).each(function(index, location){
		if(parseInt(location.id) == id){
			res_location = location;
			return false;
		}
	});
	return res_location;
};

var getLocationByName = function(name){
	return locationInfoData[name];
};

var getLocationByTypeAndParentId = function(id, type){
	var keys = Object.keys(locationInfoData);
	var locationMap = {};
	
	$(keys).each(function(keyIndex, key){
		$(locationInfoData[key]).each(function(index, location){
			if(parseInt(location.parentLocationid) == id && location.parentLocationType == type){
				var locType = location.locationType;
				if(locationMap[locType] == null || typeof locationMap[locType] == "undefined"){
					locationMap[locType] = [];
				}
				locationMap[locType].push(location);
			}
		});
	});
	return locationMap;
};

var getAllLocations = function(){	
	return locationInfoData;
};

var setMaxLevelType = function(levelType){	
	maxLevelType = levelType;
};

var getMaxLevelType = function(){
	return maxLevelType;
};

var unSelectAll = function(){
	var allLocations = getAllLocations();
	var keys = Object.keys(allLocations);
	$(keys).each(function(index, key){
		$(allLocations[key]).each(function(index, current){
			$(elementIdMap[key]).find("option[value="+current.id+"]").prop('selected', false);
			
		});
	});
};

var filterParentElements = function(loc_id, loc_type, callBack){
	var result;
	
	while(parseInt(loc_id) != 0){
		var location = getLocationByTypeAndId(parseInt(loc_id), loc_type);
	
		
		var element = elementIdMap[loc_type];
	
		$($(element).children()).each(function(index, current){
			if(parseInt($(current).val()) == location.id){
				$(current).prop('selected', true);
				return false;
			}
		});
		loc_id = location.parentLocationid;
		loc_type = location.parentLocationType;
		if(parseInt(loc_id) == 0){
			result = "Success";
			callBack(result);
		}
	}
};

var getLocationByParent = function(id, type){
	return getLocationByTypeAndParentId(id, type);
};

/*
 * 
 * Validations for filters
 * */
var fnUpdateFvStatus =function(element){
	
	
	if($(element).length > 0 && $(element).val()!=null)
	{
		$('#addSchoolForm').data('formValidation').updateStatus($(element).prop('name'), 'VALID');
	}
	else
		{
		$('#addSchoolForm').data('formValidation').updateStatus($(element).prop('name'), 'INVALID');
		}
	if($(element).length > 0 && $(element).val()!=null)
	{
		$('#editSchoolForm').data('formValidation').updateStatus($(element).prop('name'), 'VALID');
	}
	else
		{
		$('#editSchoolForm').data('formValidation').updateStatus($(element).prop('name'), 'INVALID');
		}

};
var recursiveSelection = function(subLocations){
	var subLocationMap = null;
	$(subLocations).each(function(index, location){
		if(location.locationType == maxLevelType){
			return;
		}
		subLocationMap = getLocationByParent(parseInt(location.id), location.locationType);
		var keys = Object.keys(subLocationMap);
		var key = keys[0];
		$(subLocationMap[key]).each(function(index, current){
			$(elementIdMap[key]).find("option[value="+current.id+"]").prop('selected', true);
			//fnUpdateFvStatus($(elementIdMap[key]));
		});
		return recursiveSelection(subLocationMap[key]);
	});
};

var selectAllSubLocations = function(element, subLocs){

	
	$(element).multiselect('destroy');
	$(subLocs).each(function(index, current){
		$(element).find("option[value="+current.id+"]").prop('selected', true);
	});

	enableMultiSelect(element);
};
var biDirectionalMapping = function(loc_id, loc_type){
	
	if(loc_id != "multiselect-all"){// multiselect-all is input field default value
		//of "Select-all" option provided by bootstrap multiselect
		
		var locations = [];
		var obj = {"id":loc_id, "locationType":loc_type}; 
		locations.push(obj);
		function callBack(result){
			if(result == "Success"){
				recursiveSelection(locations);		
			}
		}
		filterParentElements(loc_id, loc_type, callBack);		
		
	}else{
		$('.outer-loader').show();
		setTimeout(function(){
			var allLocations = getAllLocations();
			var keys = Object.keys(allLocations);
			$(keys).each(function(index, key){
				selectAllSubLocations(elementIdMap[key], allLocations[key]);
			});
			$('.outer-loader').hide();
			return;
		}, 50);
	}
};
var enableMultiSelect = function(arg){
	var dropupFlag =(arg==" #selectBox_villagebyRv")?true:(arg=="#selectBox_cityByBlock")?true:
					(arg==" #selectBox_gpByNp")?true:(arg==" #selectBox_revenueVillagebyGp")?true:false;
	var aMaxHeight;
	if((arg=="#selectBox_villagebyRv")||(arg=="#selectBox_cityByBlock")||(arg==" #selectBox_gpByNp")||(arg==" #selectBox_revenueVillagebyGp")){
		if($(arg).find('option:not(.disabled)').length>4){
			aMaxHeight =150;
		}else{
			aMaxHeight=110;
		}
	}else{
		aMaxHeight=150;
	}
	if((arg==" #selectBox_villagebyRv")||(arg==" #selectBox_cityByBlock")){
		$(arg).multiselect({
			maxHeight: 200,
			includeSelectAllOption: true,
			dropUp :false,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,

			filterPlaceholder: 'Search here...',

			onDropdownHide: function() {

				unSelectAll();
				$(selectedIds).each(function(index, id){
					biDirectionalMapping(id, selectedType);

				});
				$('.outer-loader').show();
				setTimeout(function(){
					var ele_keys = Object.keys(elementIdMap);
					$(ele_keys).each(function(ind, ele_key){
						$(elementIdMap[ele_key]).multiselect('destroy');
						enableMultiSelect(elementIdMap[ele_key]);
						//fnUpdateFvStatus(elementIdMap[ele_key]);
					});
					
					fnCollapseMultiselect();
					$('.outer-loader').hide();
				},100);			
			},onDropdownShow: function() {
				
				var colpseText=$("#locationPnlLink").text();
				if(colpseText=="Hide Location"){
					fnColapseLocationFilter();
				}
			 }
		});	
	}else{
	$(arg).multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		dropUp :false,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,

		filterPlaceholder: 'Search here...',

		onDropdownHide: function() {

			unSelectAll();
			$(selectedIds).each(function(index, id){
				biDirectionalMapping(id, selectedType);

			});
			$('.outer-loader').show();
			setTimeout(function(){
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(function(ind, ele_key){
					$(elementIdMap[ele_key]).multiselect('destroy');
					enableMultiSelect(elementIdMap[ele_key]);
					//fnUpdateFvStatus(elementIdMap[ele_key]);
				});
				
				fnCollapseMultiselect();
				$('.outer-loader').hide();
			},100);			
		},onDropdownShow: function() {
			$(".wizardContentMaxHeight").scrollTop(380);
		 }
	});	
}
};

var displayLocations = function(element, locations){
	$(element).parent().show();
	var aSortedData =fnSortSmartFilterByName(locations);	
	$(aSortedData).each(function(index, location){
		if(location.locationType=="School"){
			if(location.isMember){
				$('<option data-id="'+location.id+'" data-parentid="'+location.parentLocationid+'" data-childtype="'+location.childLocationType+'" data-childids="'+location.childLocationid+'">').val(location.id).text(location.locationName).appendTo(element);
			}
		}else{
			$('<option data-id="'+location.id+'" data-parentid="'+location.parentLocationid+'" data-childtype="'+location.childLocationType+'" data-childids="'+location.childLocationid+'">').val(location.id).text(location.locationName).appendTo(element);
		}
	});
	enableMultiSelect(element);	
};
var displayLocationsUptoLevel = function(levelname){
	var keys = Object.keys(locationInfoData);
	maxLevelType = levelname;
	$(keys).each(function(index, key){
		displayLocations(elementIdMap[key],locationInfoData[key]);
	});
}; 

var displayLocationsOfSurvey = function(locCode,currentElement, divElement) {	
	$("#divFilter").attr("hidden", false);
	$($(divElement).children()).each(function(index, child){
		var selectContainer = $(child).find(".select");
		selectContainer.multiselect('destroy');
		selectContainer.html();
		selectContainer.empty();
		$(child).hide();
	});
	
	
	locationInfoData = sendGETRequest("getAllLocationsByLocTypeCode/"+locCode, "GET");
	if(locationInfoData != null && locationInfoData != ""){
	
		displayLocationsUptoLevel("Village");
	}else{
		$($(divElement).children()).each(function(index, emptyChild){
			var emptyContainer = $(emptyChild).find(".select");
			emptyContainer.multiselect('refresh');	
			$(emptyChild).show();
		});
	}
	$(".outer-loader").hide();
};






$(".select").change(function(){	
	selectedType = "";
	selectedIds = [];
	$(this).multiselect('refresh');
	if($(this).val() != null)
		selectedIds = $(this).val();
	var aLocType =$(this).attr("data-locationName");
	if(aLocType!=null && aLocType!='undefined'){
		selectedType =aLocType;	
	}
	 
});












var fnColapseLocationFilter=function(){
	 $("#collapseFilter").find(".btn-collapse").find("span").removeClass("red");
	 $("#collapseFilter").find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign");
	 $("#collapseFilter").find(".btn-collapse").find("span").addClass("green");
	 $("#collapseFilter").find(".btn-collapse").find("span").addClass("glyphicon-plus-sign");
	 $(' #locationPnlLink').text('Select Location');
	 $("#addLocColspFilter").css("height",0);
	 $("#addLocColspFilter").removeClass("in");
	 $("#editSchoolForm #stepContentsDiv").removeClass("wizardContentMaxHeight");
}
