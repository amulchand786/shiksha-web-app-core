var filterClasses = {
		open : ['open','aui-iconfont-expanded'],
		close : ['','aui-iconfont-collapsed']
};
var contentFilterObj = {
		sourceSelect : "#filterBox_source",
		gradeSelect  : "#filterBox_grade",
		subjectSelect: "#filterBox_subject",
		toolbar		 : "#conteFilterContainer",
		searchTextBox  : "#filter-search",
		filterBoxView : "#filterBox_View",
		filterSearchBtn : "#filter-search-btn",
		filterType	 :'',
		filterSubType:'',
		URL			 :'',
		
		init : function(){
			contentFilterObj.filterType	 ='';
			contentFilterObj.filterSubType	 ='';
			contentFilterObj.URL	 ='';
			
			this.enableMultiSelectForFilters(this.sourceSelect);
			this.enableMultiSelectForFilters(this.gradeSelect);
			this.enableMultiSelectForFilters(this.subjectSelect);
			this.enableMultiSelectForFilters(this.filterBoxView);
			$(this.toolbar).show();
			//$(this.searchTextBox).on("keyup",this.filterDataOnSearchBox);
			$(this.filterSearchBtn).on("click",this.filterData);
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				onChange : contentFilterObj.filterData
			});	
		},
		filterDataOnSearchBox : function(event){
			var searchStr = $(this).val();
			$("tr.shk-nugget").remove();
			$("tr.shk-segment").remove();
			$("tr.shk-chapter").removeClass("open");
			if(searchStr.trim()){
				$("tr.shk-chapter").hide();
				$("tr.shk-chapter[data-chapter-name*='"+searchStr+"' i]").show();
			} else {
				$("tr.shk-chapter").show();
			}
		},
		filterData : function(){
			if($(this.$select).attr("id") === "filetypes")
				return false;
			contentFilterObj.filterSubType = $(contentFilterObj.filterBoxView).val();
			treeViewObj.getChaptersList();
			if($(contentFilterObj.filterBoxView).val() || $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
				$("#warning-msg").show();
			} else {
				$("#warning-msg").hide();
			}
		},
		fnGetSelectedFiteredValue : function(ele){
			var arr =[];
			var selector = "option" ;
			if($(ele).val()!=null)
				selector = "option:selected";
			$(ele).find(selector).each(function(ind,option){
				if($(option).val()!="multiselect-all"){		
					arr.push(parseInt($(option).val()));
				}
			});
			return arr;
		},
}
var treeViewObj = {
		table : "#contentTreeView",
		inputFiles : "#segmentFileinput",
		init:function(){
			$(this.table).find("tbody").empty();
			$(document).on("click",".expand-icon",function(){
				var nodeType = $(this).data("type");
				if($(this).parents("tr").hasClass("open")){
					if(nodeType == 'shk-chapter'){
						$(this).parents("tr").nextUntil("."+nodeType).remove();
					} else {
						$("tr.shk-nugget").find(".move-handle").show();
						treeViewObj.enableNuggetSortable();
						$(this).parents("tr").nextUntil(".shk-chapter,."+nodeType).remove();
					}
				} else {
					if(nodeType == 'shk-chapter'){
						$(this).parents("tr").siblings().removeClass("open");
						$("tr.shk-nugget,tr.shk-segment").remove();
						treeViewObj.getChapterContent($(this).parents("tr").data("chapterid"));
					} else {
						$(this).parents("tr").siblings(".shk-nugget").removeClass("open");
						$("tr.shk-segment").remove();
						treeViewObj.getNuggetContent($(this).parents("tr").data("chapterid"),$(this).parents("tr").data("nuggetid"));
					}
				}
				$(this).parents("tr").toggleClass("open");
				$(this).find(".aui-icon").toggleClass("aui-iconfont-expanded aui-iconfont-collapsed");
			});
			//$(".custom-filter").on("click",this.changeSubFilter);
			
			this.getChaptersList();
		},
		changeSubFilter : function(){
			$(this).addClass("enabled");
			$(this).siblings().removeClass("enabled");
			var subFilterType = $(this).data("filtertype");
			$(treeViewObj.table).find("tbody").empty();
			switch(subFilterType){
				case "total":
				case "totalUpload":
					treeViewObj.showTotalList();
					break;
				case "rejected": 
					treeViewObj.showRejectdList();
					break;
				case "published": 
					treeViewObj.showPublishedList();
					break;
				case "approvals": 
					treeViewObj.showApprovalsList();
					break;
				
				default : 
					break;
			}
		},
		showTotalList : function(){			
			treeViewObj.getChaptersList();
		},
		showRejectdList  :function(){
			contentFilterObj.filterSubType='rejected';
			treeViewObj.getChaptersList();
		},
		showPublishedList : function(){
			contentFilterObj.filterSubType='awaitingApprovals';//TODO-to be change on demand
			treeViewObj.getChaptersList();
		},
		showApprovalsList : function(){
			contentFilterObj.filterSubType='awaitingApprovals';
			treeViewObj.getChaptersList();
		},
		getChaptersList : function(){
			spinner.showSpinner();
			var ajaxData = {
				gradeIds : contentFilterObj.fnGetSelectedFiteredValue(contentFilterObj.gradeSelect),
				subjectIds : contentFilterObj.fnGetSelectedFiteredValue(contentFilterObj.subjectSelect),
				filterSubType:$(contentFilterObj.filterBoxView).val(),
				search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()
			};
			var responseData = shiksha.invokeAjax("shiksha/contentmanagement/chapter", ajaxData, "POST");
			if(responseData.response=="shiksha-200" ){

				$("[data-filtertype='total'] #count").text(responseData.totalChapters);
				$("[data-filtertype='totalUpload'] #count").text(responseData.totalUploaded);
				$("[data-filtertype='approvals'] #count").text(responseData.totalAwaiting);
				$("[data-filtertype='rejected'] #count").text(responseData.totalRejected);	
				//$("[data-filtertype='published'] #count").text(responseData.totalPublished);
				this.showChapters(responseData['chapters'],'close');
			}
			spinner.hideSpinner();
		},
		getChapterContent : function(chapterId){
			spinner.showSpinner();
						
			var ajaxData ={	"chapter":chapterId, "filterSubType":contentFilterObj.filterSubType, search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()};
			var responseData =shiksha.invokeAjax("shiksha/contentmanagement/nugget", ajaxData, "POST");
			console.log(responseData);
			this.showNuggets(chapterId,responseData.shikshaNuggets,'close');
			spinner.hideSpinner();
		},
		getNuggetContent : function(chapterId,nuggetId){
			spinner.showSpinner();
			var ajaxData ={	"chapter":chapterId, "nugget" : nuggetId , "filterSubType":contentFilterObj.filterSubType, search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()};
			var responseData =shiksha.invokeAjax("shiksha/contentmanagement/segment", ajaxData, "POST");
			this.showSegements(chapterId,nuggetId,responseData.segments);
			spinner.hideSpinner();
		},
		showChapters : function(chapters,status){
			$(treeViewObj.table).find("tbody").empty();
			//$(contentFilterObj.searchTextBox).val("");
			if(!chapters || !chapters.length){
				return false;
			}
			$.each(chapters,function(index,chapter){
				var thumbnailUrl = "web-resources/images/thumbnail-placeholder.jpg";
				if(chapter.thumbnailUrl)
					thumbnailUrl = chapter.thumbnailUrl;
				var thumbNail='';
				if(cmPermissions.uploadThumbnail){
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
				}else{
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"></div>';
				}
				
				
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				var otherCount = 0;
				$.each(chapter.nuggets,function(i, nugget){
					if(nugget.totalAwaiting || nugget.totalRejected || nugget.totalUnmapped)
						otherCount++;
				})
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						+'<div class="actions-count">'+otherCount+'</div></div>';
				
				$(treeViewObj.table).find("tbody").append('<tr class="shk-chapter '+filterClasses[status][0]+'" data-chapterid="'+chapter.unitId+'" data-chapter-name="'+chapter.unitName+'">'
					+'<td><a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-chapter">'
					+'<div class="lms-log-status-section lms-side-center-far"><div class="lms-dropdown-toggle" href="#"><svg>'
					+'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#dropdown" href="web-resources/images/content-logs.svg#dropdown"></use></svg></div>'
					//+'<div class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
					//+'<div class="actions-count" title="Awaiting approval">'+chapter.totalAwaiting+'</div></div></div>'
					/*+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
					+statusIcon+"</div>"
					+thumbNail
					+'<div><div><div class="log-title-text">'+chapter.unitName+'</div></div></div>'
					+'</a></td>'
					+'<td class="count">Nug:'+chapter.noOfNuggets+'/Seg:'+chapter.noOfSegments+'</td><td>'
					+'<div class="shk-action-container">'
					+((cmPermissions.addNugget && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim())? '<a class="add-nugget" data-chapterid="'+chapter.unitId+'">Add nugget</a>' : "")
					+'</div></td></tr>');
			});
		},
		showNuggets : function(chapterId,nuggets,status){
			if(!nuggets || !nuggets.length){
				//$(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+chapterId+"']").last().after('<tr class="shk-nugget '+filterClasses[status][0]+'" data-chapterid="'+chapterId+'"><td colspan="3">No nuggets avilable</td></tr>');
				return false;
			}
			$.each(nuggets,function(index,nugget){
				var thumbnailUrl = "web-resources/images/thumbnail-placeholder.jpg";
				if(nugget.thumbnailUrl)
					thumbnailUrl = nugget.thumbnailUrl;
				
				var thumbNail='';
				if(cmPermissions.uploadThumbnail ){
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
				}else{
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"></div>';
				}
				
				var otherCount		= nugget.totalAwaiting + nugget.totalRejected + nugget.totalUnmapped;
								
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						+'<div class="actions-count">'+otherCount+'</div></div>';
								
				var str = '<tr class="shk-nugget '+filterClasses[status][0]+'" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">'
						+'<td><a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-nugget">'
						+'<div class="lms-log-status-section lms-side-center-far"><div class="lms-dropdown-toggle" href="#"><svg>'
						+'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#dropdown" href="web-resources/images/content-logs.svg#dropdown"></use></svg></div>'
						+statusIcon+'</div>'
						/*+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
						+thumbNail
						+'<div><div><div class="log-title-text">'+nugget.name+'</div></div></div>'
						+'</a></td>'
						+'<td class="seg-count">Seg:'+nugget.noOfSegments+'</td><td>'
						+'<div class="shk-action-container">'
						+((cmPermissions.addSegment && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim())? '<a class="add-segment" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">Add segment</a>' : "");
				var actions = "";
					if(true){
					actions+='<div class="dropdown">'
					actions+='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
						+'<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
						+'</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
						+'<li><a href="javascript:void(0)" class="view-nugget" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">View</a></li>';
						(cmPermissions.editNugget)? actions+='<li><a href="javascript:void(0)" class="edit-nugget" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">Edit</a></li>' : "";
						(cmPermissions.delNugget  && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()) ? actions+='<li><a href="javascript:void(0)" class="delete-nugget" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'"  onclick="deleteNugget.initDelete(\''+nugget.nuggetId+'\',\''+nugget.name+'\',\''+chapterId+'\')">Delete</a></li>':"";
						actions+='</ul></div>';
					}
					if(cmPermissions.editNugget && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
						str+=actions+'<span class="glyphicon glyphicon-move move-handle"></span></div></td></tr>';	
					}else{
						str+=actions+'</div></td></tr>';
					}
				
				$(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+chapterId+"']").last().after(str);
			});
			treeViewObj.enableNuggetSortable();
		},
		showSegements : function(chapterId,nuggetId,segments){
			if(!segments || !segments.length){
				return false;
			}
			console.log(segments);
			$.each(segments,function(index,segment){
				var thumbnailUrl = "web-resources/images/thumbnail-placeholder.jpg";
				if(segment.thumbnailUrl)
					thumbnailUrl = segment.thumbnailUrl;
				
				
				var thumbNail='';
				if(cmPermissions.uploadThumbnail){
					thumbNail = '<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
				}else{
					thumbNail = '<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"></div>';
				}
				
				
				
				var statusIcon = '<div title="'+statusMessage.unmappedStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg></div>';
				if(segment.isRejected){
					statusIcon = '<div title="'+statusMessage.rejectedStatus+'" class="log-status-icon log-status--rejected"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#rejected" href="web-resources/images/content-logs.svg#rejected"></use></svg></div>';
				} else if(segment.isApproved){
					statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				} else if(segment.isUploaded){
					statusIcon = '<div title="'+statusMessage.awaitingStatus+'" class="log-status-icon log-status--awaiting"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#awaiting" href="web-resources/images/content-logs.svg#awaiting"></use></svg></div>';
				}
				
				var str = '<tr class="shk-segment" data-nuggetid="'+nuggetId+'" data-segmentid="'+segment.segmentId+'">'
						+'<td><span class="log-title" href="javascript:void(0);" data-type="shk-segment">'
                        +'<div class="lms-log-status-section lms-side-center-far"><div></div>'
                        + statusIcon
                        //+'<div class="log-status-icon log-status--awaiting"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#awaiting" href="web-resources/images/content-logs.svg#awaiting"></use></svg></div>'
                        +'</div>'
                       /* +'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
                        +thumbNail
                        +'<div class="segment-title-container"><div><div class="title-segment">'+segment.segmentName+'('+segment.segmentType.segmentTypeName+')'+'</div><div class="log-version">'+segment.version+'</div></div></div></span>'
						+'</td><td>'
						+'<div class="duration-icon" title="Duration" ><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#durationx" href="web-resources/images/content-logs.svg#durationx"></use></svg></div> <span class="duration-span"></span>'
						+'<a href="#" id="duration'+segment.segmentId+'" data-type="combodate"  data-format="HH:mm:ss" data-viewformat="HH:mm:ss" data-minuteStep="1" data-template="HH : mm : ss" data-pk="1" data-title="Edit duration (hh:mm:ss)" class="editable editable-click editable-open duration-edit" style="">'+segment.duration+'</a>'
						+'</td><td>'
						+'<div class="shk-action-container log-list-actions">'
						+((cmPermissions.previewContent)?'<a class="action-icon download-segment action-icon--play disabled-icon" target="_blank" href="shiksha/segment/'+segment.segmentId+'/download" title="Download content"><div class="lms-play-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/all.svg#download-action" href="web-resources/images/all.svg#download-action"></use></svg></div></a>': '')
						+((cmPermissions.commentSegment || true)?'<a class="action-icon comment-segment disabled-icon" title="View comment" ><div class="comment-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#comments" href="web-resources/images/content-logs.svg#comments"></use></svg></div></span></a>' : '')
						+((cmPermissions.revertContent)?'<a class="action-icon undo-segment disabled-icon" title="Undo segment"><div class="undo-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#undo" href="web-resources/images/content-logs.svg#undo"></use></svg></div></a>' : '')
						+((cmPermissions.approveContent)?'<a class="action-icon approve-segment action-icon--approve disabled-icon" title="Approve segment" data-segmentid="'+segment.segmentId+'"><div class="lms-approve-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#approve" href="web-resources/images/content-manager.svg#approve"></use></svg></div></a>' : '')
						+((cmPermissions.rejectContent)?'<a class="action-icon reject-segment action-icon--reject disabled-icon" title="Reject segment" data-segmentid="'+segment.segmentId+'"><div class="lms-reject-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#reject" href="web-resources/images/content-manager.svg#reject"></use></svg></div></a>' : '');
				var actions = "";
				if(true){
					actions+='<div class="dropdown">'
						+'<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
						+'<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
						+'</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
						+'<li><a href="javascript:void(0)" class="view-segment" data-nuggetid="'+nuggetId+'" data-segmentid="'+segment.segmentId+'">View</a></li>';
						(cmPermissions.editSegment)? actions+='<li><a href="javascript:void(0)" class="edit-segment" data-nuggetid="'+nuggetId+'" data-segmentid="'+segment.segmentId+'">Edit</a></li>' : '';
						(cmPermissions.viewLogs)? actions+='<li><a href="javascript:void(0)" class="view-logs-segment" onclick="logsObj.viewSegmentLogs(\''+segment.segmentId+'\',\''+segment.segmentName+'\')">View logs</a></li>':'';
						(cmPermissions.delSegment  && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim())? actions+='<li><a href="javascript:void(0)" class="delete-segment" onclick="deleteSegment.initDelete(\''+segment.segmentId+'\',\''+segment.segmentName+'\',\''+nuggetId+'\',\''+chapterId+'\')">Delete</a></li>':'';
						actions+='</ul></div>';
				}
				if(cmPermissions.editSegment && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
					str+=actions+'<span class="glyphicon glyphicon-move move-handle" title="Move"></span></div></td></tr>'	
				}else{
					str+=actions+'</div></td></tr>'
				}
				
				$(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+nuggetId+"']").last().after(str);
				var segmentRow = $(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+nuggetId+"'][data-segmentid='"+segment.segmentId+"']");
				if(segment.contentUrl){
					segmentRow.find(".download-segment").removeClass("disabled-icon");
				} 
				if (segment.contentUrl && !segment.isRejected && !segment.isApproved){
					segmentRow.find(".approve-segment,.undo-segment").removeClass("disabled-icon");
				}
				if ((segment.contentUrl && !segment.isRejected) || segment.isApproved){
					segmentRow.find(".reject-segment ").removeClass("disabled-icon");
				}
				if (segment.contentUrl && segment.rejectionComment){
					segmentRow.find(".comment-segment ").removeClass("disabled-icon");
				}
				treeViewObj.enableDurationEditable(segment.segmentId);
			});
			treeViewObj.enableSegmentSortable();
			
			treeViewObj.enableContentDropable();
			
		},
		enableContentDropable : function(){
			
			$(treeViewObj.table).find("tr.shk-segment").droppable({
				  accept: ".li-file",
				  drop: function( event, ui ) {
					  $(this).removeClass("drop-over");
					  var fileName = $(ui.draggable).data("filename");
					  var fileType = $(ui.draggable).data("type");
					  uploadSegment.mapFileToSegment($(this).data("segmentid"),fileName,fileType);
					  $(".ui-draggable-dragging").remove();
				  },
				  over: function( event, ui ) {
					  $(this).addClass("drop-over");
				  },
				  out : function(){
					  $(this).removeClass("drop-over");
				  }
			});
		},
		durationParamenters : function(params){
			console.log(params)
			return params;
		},
		enableDurationEditable : function(segId){
			$('#duration'+segId).editable({
				combodate: {
	                minuteStep: 1
	           },
	           send: 'never',
	           validate  : function(value){
					console.log($(this).text());
					console.log(parseInt(value['_i'].join("")));
					if(parseInt(value['_i'].join("")) <= 1000){
						return "Enter Valid duration";
					}
					
				},
				display : false,
		    }).on("save",function(e, params){
		    	spinner.showSpinner();
		    	var segmentId = $(e.target).parents("tr").data("segmentid");
		    	var updateDurationAjaxCall = shiksha.invokeAjax("shiksha/segment/"+segmentId+"?duration="+params.submitValue, null, "POST");
		    	
		    	if(updateDurationAjaxCall){
					if(updateDurationAjaxCall.response == "shiksha-200"){
						$("tr[data-segmentid='"+segmentId+"']").find(".duration-edit").text(updateDurationAjaxCall.duration);
							AJS.flag({
								type  : "success",
								title : appMessgaes.success,
								body  : updateDurationAjaxCall.responseMessage,
								close : 'auto'
							});					
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : updateDurationAjaxCall.responseMessage,
							close : 'auto'
						});
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
				spinner.hideSpinner();
		    });
		},
		clearFiles : function(){
			$("#segmentFileinput").val("");
			$(".segment-files-list").empty();
			$(".drag-drop-select").show();
			$(".drag-drop-clear").hide();
		},
		enableNuggetSortable : function(){
			$("tbody").sortable('destroy');
			$( "tbody" ).sortable({
				  axis: "y",
				  helper: "clone",
				  items: "> tr.shk-nugget",
				  handle : ".move-handle",
				  forcePlaceholderSize: true,
				  placeholder: "ui-sortable-placeholder",
				  revert: false,
				  stop: function( event, ui ) {
					  var chapterId = $(ui.item[0]).attr("data-chapterid");
					  var ajaxData = [];
					  $("tr.shk-nugget[data-chapterid='"+chapterId+"']").each(function(index,row){
						var tmpObj ={
									nuggetId : $(this).attr("data-nuggetid"),
									sequence : index + 1
						  };
						ajaxData.push(tmpObj);
					  });
					  
					  var updateSequence = shiksha.invokeAjax("shiksha/nugget/swapsequence", ajaxData, "POST");
					  console.log(updateSequence);
					  if(updateSequence){
						  if(updateSequence.response == "shiksha-200"){
							  AJS.flag({
									type  : "success",
									title : "Success",
									body  : "Nugget sequence updated",
									close : 'auto'
								});
						  } else {
							  AJS.flag({
									type  : "error",
									title : "Error",
									body  : updateSequence.responseMessage,
									close : 'auto'
								});
						  }
					  }  
					  spinner.hideSpinner();
					  
				  }
				  
			});
		},
		enableSegmentSortable : function(){
			$("tr.shk-nugget").find(".move-handle").hide();
			$("tbody").sortable('destroy');
			$( "tbody" ).sortable({
				  axis: "y",
				  helper: "clone",
				  items: "> tr.shk-segment",
				  handle : ".move-handle",
				  forcePlaceholderSize: true,
				  placeholder: "ui-sortable-placeholder",
				  revert: false,
				  stop: function( event, ui ) {
					  var nuggetId = $(ui.item[0]).attr("data-nuggetid");
					  var ajaxData = [];
					  $("tr.shk-segment[data-nuggetid='"+nuggetId+"']").each(function(index,row){
						var tmpObj ={
									segmentId : $(this).attr("data-segmentid"),
									sequence : index + 1
						  };
						ajaxData.push(tmpObj);
					  });
					  var updateSequence = shiksha.invokeAjax("shiksha/segment/swapsequence", ajaxData, "POST");
					  console.log(updateSequence);
					  if(updateSequence){
						  if(updateSequence.response == "shiksha-200"){
							  AJS.flag({
									type  : "success",
									title : "Success",
									body  : "Segment sequence updated",
									close : 'auto'
								});
						  } else {
							  AJS.flag({
									type  : "error",
									title : "Error",
									body  : updateSequence.responseMessage,
									close : 'auto'
								});
						  }
					  }  
					  spinner.hideSpinner();
				  }
				  
			});
		}
};

var addSegment = {
	addSegmentBtn 	: ".add-segment",
	addSegmentModal	: "#mdlAddSegment",
	addSegmentForm 	: "#addSegmentForm",
	addSegmetNuggetId  : "#addSegmetNuggetId",
	addSegmentType  : "#addSegmentType",
	addContentType  : "#addContentType",
	addSegmentName 	: "#addSegmentName",
	addDescription	: "#addSegmentDescription",
	addResolution	: "#addResolution",
	init 		 	: function(){	
		$(document).on("click",this.addSegmentBtn,this.showAddSegemntModal);
		setOptionsForMultipleSelect(addSegment.addSegmentType);
		setOptionsForMultipleSelect(addSegment.addContentType);
		$(this.addContentType).on("change",this.fnContentCahnge);
		$(this.addSegmentForm).submit(function(e) {
			e.preventDefault();
		}).validate({
			ignore: '',
			rules: {
				segmentType : {
					required : true
				},
				contentType : {
					required : true
				},
				segmentName: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				},
				description: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				}	,
			},

			messages: {
				segmentType : {
					required : "Please select Segment type"
				},
				contentType : {
					required : "Please select content type"
				},
				segmentName: {
					required: "Please enter segment name",
					noSpace:"Please enter valid segment Name",
					minlength: "The segment name must be more than 2 and less than 50 characters long",
					maxlength: "The segment name must be more than 2 and less than 50 characters long",

				},
				description: {
					required: "Please enter Description",
					noSpace:"Please enter valid segment version",
					minlength: "The segment version must be more than 2 and less than 100 characters long",
					maxlength: "The segment version must be more than 2 and less than 100 characters long",
				}	,
			},
			errorPlacement: function(error, element) {
				$(element).parent().append(error);
			},
			submitHandler : function(){
				addSegment.fnAddSegment();
			}
		});
		
	},
	fnContentCahnge : function(){
		if($(this).find("option:selected").attr("data-name") == 'Flash')
			$("#addResolutionDiv").show();
		else
			$("#addResolutionDiv").hide();
	},
	fnAddSegment : function(){
		var ajaxData = {
				"segmentName"	: $(addSegment.addSegmentName).val().replace(/\s\s+/g, ' ').trim(),
				"description" 	: $(addSegment.addDescription).val(),
				"shikshaNugget" : {
					"nuggetId" 		: $(addSegment.addSegmetNuggetId).val()
				},
				"segmentType"	: {
					"segmentTypeId"	: $(addSegment.addSegmentType).val()
				},
				"contentType" 	: {
						"contentTypeId"	: $(addSegment.addContentType).val()
				}
		}
		
		if($(addSegment.addContentType).find("option:selected").attr("data-name") == 'Flash')
			ajaxData.resolution =  $(addSegment.addResolution+":checked").val();
		
		var addAjaxCall = shiksha.invokeAjax("shiksha/segment", ajaxData, "POST");
		console.log(addAjaxCall);
		if(addAjaxCall != null){
			if(addAjaxCall.response == "shiksha-200"){
				
					$(addSegment.addSegmentModal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
					
					if($("tr.shk-nugget[data-nuggetid='"+addAjaxCall.shikshaNugget.nuggetId+"']").hasClass("open")){
						var thumbNail='';
						if(cmPermissions.uploadThumbnail){
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'; 
						}else{
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"></div>';
						}
						
						var str = '<tr class="shk-segment" data-nuggetid="'+addAjaxCall.shikshaNugget.nuggetId+'" data-segmentid="'+addAjaxCall.segmentId+'">'
							+'<td><span class="log-title" href="javascript:void(0);" data-type="shk-segment">'
		                    +'<div class="lms-log-status-section lms-side-center-far"><div></div><div class="log-status-icon log-status--partial">'
		                    +'<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg></div></div>'
		                    +thumbNail
		                    //+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'
		                    +'<div class="segment-title-container"><div><div class="title-segment">'+addAjaxCall.segmentName+'('+addAjaxCall.segmentType.segmentTypeName+')'+'</div><div class="log-version">'+addAjaxCall.version+'</div></div></div></span>'
							+'</td><td>'
							+'<div class="duration-icon" title="Duration"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#durationx" href="web-resources/images/content-logs.svg#durationx"></use></svg></div> <span class="duration-span"></span>'
							+'<a href="#" id="duration'+addAjaxCall.segmentId+'" data-type="combodate"  data-format="HH:mm:ss" data-viewformat="HH:mm:ss" data-minuteStep="1" data-template="HH : mm : ss" data-pk="1" data-title="Edit duration (hh:mm:ss)" class="editable editable-click editable-open duration-edit" style="">'+addAjaxCall.duration+'</a>'
							+'</td><td>'
							+'<div class="shk-action-container log-list-actions">'
							+((cmPermissions.previewContent)?'<a class="action-icon download-segment action-icon--play disabled-icon" target="_blank" href="shiksha/segment/'+addAjaxCall.segmentId+'/download" title="Download content"><div class="lms-play-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/all.svg#download-action" href="web-resources/images/all.svg#download-action"></use></svg></div></a>' : '')
							+((cmPermissions.commentSegment || true)?'<a class="action-icon comment-segment disabled-icon" title="View comment"><div class="comment-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#comments" href="web-resources/images/content-logs.svg#comments"></use></svg></div></span></a>' : '')
							+((cmPermissions.revertContent || true)?'<a class="action-icon undo-segment disabled-icon" title="Undo segment"><div class="undo-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#undo" href="web-resources/images/content-logs.svg#undo"></use></svg></div></a>' : '')
							+((cmPermissions.approveContent)?'<a class="action-icon approve-segment action-icon--approve disabled-icon" title="Approve segment" data-segmentid="'+addAjaxCall.segmentId+'"><div class="lms-approve-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#approve" href="web-resources/images/content-manager.svg#approve"></use></svg></div></a>' : '')
							+((cmPermissions.rejectContent)? '<a class="action-icon reject-segment action-icon--reject disabled-icon" title="Reject segment" data-segmentid="'+addAjaxCall.segmentId+'"><div class="lms-reject-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#reject" href="web-resources/images/content-manager.svg#reject"></use></svg></div></a>' : '');
							
						var actions = "";
						if(true){
							actions+='<div class="dropdown">'
								+'<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
								+'<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
								+'</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
								+'<li><a href="javascript:void(0)" class="view-segment" data-nuggetid="'+addAjaxCall.shikshaNugget.nuggetId+'" data-segmentid="'+addAjaxCall.segmentId+'">View</a></li>';
								(cmPermissions.editSegment)? actions+= '<li><a href="javascript:void(0)" class="edit-segment" data-nuggetid="'+addAjaxCall.shikshaNugget.nuggetId+'" data-segmentid="'+addAjaxCall.segmentId+'">Edit</a></li>' : '';
								(cmPermissions.viewLogs)? actions+='<li><a href="javascript:void(0)" class="view-logs-segment" onclick="logsObj.viewSegmentLogs(\''+addAjaxCall.segmentId+'\',\''+addAjaxCall.segmentName+'\')">View logs</a></li>':'';
								(cmPermissions.delSegment)? actions+= '<li><a href="javascript:void(0)" class="delete-segment" onclick="deleteSegment.initDelete(\''+addAjaxCall.segmentId+'\',\''+addAjaxCall.segmentname+'\',\''+addAjaxCall.shikshaNugget.nuggetId+'\',\''+addAjaxCall.shikshaNugget.unit.unitId+'\')">Delete</a></li>' : '';
							actions+='</ul></div>';
						}
						if(cmPermissions.editSegment && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
							str+=actions+'<span class="glyphicon glyphicon-move move-handle" title="Move"></span></div></td></tr>'	
						}else{
							str+=actions+'</div></td></tr>'
						}
						$(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+addAjaxCall.shikshaNugget.nuggetId+"']").last().after(str);	

					}
					treeViewObj.enableSegmentSortable();
				$(addSegment.addSegmetNuggetId).val(0);
				resetForm(addSegment.addSegmentForm);
				
				treeViewObj.enableDurationEditable( addAjaxCall.segmentId );
				treeViewObj.enableContentDropable();
				
				var nuggetId = $("tr[data-segmentid='"+addAjaxCall.segmentId+"']").attr("data-nuggetid");
				var chapterId = $("tr.shk-nugget[data-nuggetid='"+addAjaxCall.shikshaNugget.nuggetId+"']").attr("data-chapterid");
				
				updateCountNuggetsAndChapter(chapterId, addAjaxCall.shikshaNugget.nuggetId);		
				
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : addAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type  : "error",
				title : appMessgaes.oops,
				body  : appMessgaes.serverError,
				close : 'auto'
			});
		}
		spinner.hideSpinner();
	},
	showAddSegemntModal : function(){
		$(addSegment.addSegmentType).val("");
		$(addSegment.addContentType).val("");
		shikshaPlus.fnGetSegmentTypes(addSegment.addSegmentType,[],0);
		shikshaPlus.fnGetContentTypes(addSegment.addContentType,0);
		$(addSegment.addSegmentType).multiselect("rebuild");
		$(addSegment.addContentType).multiselect("rebuild");
		resetForm(addSegment.addSegmentForm);
		$(addSegment.addSegmetNuggetId).val($(this).data("nuggetid"));
		$("#addResolutionDiv").hide();
		$("[id='groupDiv']").removeClass('disabled-select');
		$(addSegment.addSegmentModal).modal("show");
	}
};

var editSegment = {
		editSegmentBtn 	: ".edit-segment",
		editSegmentModal	: "#mdlEditSegment",
		editSegmentForm 	: "#editSegmentForm",
		editSegmentNuggetId  : "#editSegmentNuggetId",
		editSegmentType  : "#editSegmentType",
		editContentType  : "#editContentType",
		editSegmentName 	: "#editSegmentName",
		editDescription	: "#editSegmentDescription",
		editResolution:"#editResolution",
		editSegmentId : "#editSegmentId",
		init 		 	: function(){	
			$(document).on("click",this.editSegmentBtn,this.showEditSegemntModal);
			setOptionsForMultipleSelect(editSegment.editSegmentType);
			setOptionsForMultipleSelect(editSegment.editContentType);
			$(this.editContentType).on("change",this.fnContentCahnge);
			$(this.editSegmentForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					segmentType : {
						required : true
					},
					contentType : {
						required : true
					},
					segmentName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					description: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}	,
				},

				messages: {
					segmentType : {
						required : "Please select Segment type"
					},
					contentType : {
						required : "Please select content type"
					},
					segmentName: {
						required: "Please enter segment name",
						noSpace:"Please enter valid segment Name",
						minlength: "The segment name must be more than 2 and less than 50 characters long",
						maxlength: "The segment name must be more than 2 and less than 50 characters long",

					},
					description: {
						required: "Please enter Description",
						noSpace:"Please enter valid segment version",
						minlength: "The segment version must be more than 2 and less than 100 characters long",
						maxlength: "The segment version must be more than 2 and less than 100 characters long",
					}	,
				},
				errorPlacement: function(error, element) {
					$(element).parent().append(error);
				},
				submitHandler : function(){
					editSegment.fnEditSegment();
				}
			});
			
		},
		fnContentCahnge : function(){
			if($(this).find("option:selected").attr("data-name") == 'Flash')
				$("#editResolutionDiv").show();
			else
				$("#editResolutionDiv").hide();
		},
		fnEditSegment : function(){
			var ajaxData = {
					
					"segmentId" 	: $(editSegment.editSegmentId).val(),
					"segmentName"	: $(editSegment.editSegmentName).val().replace(/\s\s+/g, ' ').trim(),
					"description" 	: $(editSegment.editDescription).val(),
					"shikshaNugget" : {
						"nuggetId" 		: $(editSegment.editSegmentNuggetId).val()
					},
					"segmentType"	: {
						"segmentTypeId"	: $(editSegment.editSegmentType).val()
					},
					"contentType" 	: {
							"contentTypeId"	: $(editSegment.editContentType).val()
					}
			}
			if($(editSegment.editContentType).find("option:selected").attr("data-name") == 'Flash')
				ajaxData.resolution =  $(editSegment.editResolution+":checked").val();
			
			var addAjaxCall = shiksha.invokeAjax("shiksha/segment", ajaxData, "PUT");
			console.log(addAjaxCall);
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
						$(editSegment.editSegmentModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
						
					$(treeViewObj.table).find("tbody").find("tr[data-segmentid='"+addAjaxCall.segmentId+"']").find(".title-segment").text(addAjaxCall.segmentName+'('+addAjaxCall.segmentType.segmentTypeName+')');	
					$(addSegment.addSegmetNuggetId).val(0);
					resetForm(addSegment.addSegmentForm);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				});
			}
			spinner.hideSpinner();
		},
		showEditSegemntModal : function(){
			resetForm(addSegment.addSegmentForm);
			var nuggetId = $(this).data("nuggetid");
			var segmentId = $(this).data("segmentid")
			$(editSegment.editSegmentNuggetId).val(nuggetId);
			$(editSegment.editSegmentId).val(segmentId);
			
			var getAjaxCall = shiksha.invokeAjax("shiksha/segment/"+segmentId, null, "GET");
			console.log(getAjaxCall);
			$(editSegment.editSegmentName).val(getAjaxCall.segmentName);
			$(editSegment.editDescription).val(getAjaxCall.description);
			$('input[id=editResolution][value="'+getAjaxCall.resolution+'"]').prop('checked',true);
			shikshaPlus.fnGetSegmentTypes(editSegment.editSegmentType,[],getAjaxCall.segmentType.segmentTypeId);
			shikshaPlus.fnGetContentTypes(editSegment.editContentType,getAjaxCall.contentType.contentTypeId);
			$(editSegment.editSegmentType).multiselect("rebuild");
			$(editSegment.editContentType).multiselect("rebuild");
			var segUrl=getAjaxCall.contentUrl;
			if(!getAjaxCall.isRejected && !getAjaxCall.isApproved){
				$("[id='groupDiv']").removeClass('disabled-select');
				
			} else{
				$("[id='groupDiv']").addClass('disabled-select');
			}
			if(getAjaxCall.contentType.name == "Flash")
				$("#editResolutionDiv").show();
			else 
				$("#editResolutionDiv").hide();
			$(editSegment.editSegmentModal).modal("show");
		}
	};

var deleteSegment  = {
		deleleteSegmentBtn 	: ".delete-segment",
		deleteSegmentModal 	: "#mdlDeleteSegment",
		deleteSegmentConfirmBtn 	: "#deleteSegmentButton",
		deleteSegmentId : "#deleteSegmentId",
		deleteSegmentNuggetId: "#deleteSegmentNuggetId",
		deleteSegmentChapterId: "#deleteSegmentChapterId",
		
		
		init : function(){
			$(this.deleteSegmentConfirmBtn).on("click",this.fnDeleteSegment);
		},
		initDelete : function(segmentId,name,nuggetId,chapterId){
			var msg = $("#deleteSegmentMessage").data("message");
			$("#deleteSegmentMessage").html(msg.replace('@NAME@',name));
			$(deleteSegment.deleteSegmentId).val(segmentId);
			
			$(deleteSegment.deleteSegmentNuggetId).val(nuggetId);
			$(deleteSegment.deleteSegmentChapterId).val(chapterId);
			
			
			$(deleteSegment.deleteSegmentModal).modal("show");
		},
		fnDeleteSegment : function(){
			spinner.showSpinner();
			
			var deleteSegmentId = $(deleteSegment.deleteSegmentId).val();
			
			var deleteAjaxCall = shiksha.invokeAjax("shiksha/segment/"+deleteSegmentId, null, "DELETE");
			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-200"){
					$("tr[data-segmentid='"+deleteSegmentId+"']").remove();
					$(deleteSegment.deleteSegmentModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : deleteAjaxCall.responseMessage,
							close : 'auto'
						});
												
						updateCountNuggetsAndChapter($(deleteSegment.deleteSegmentChapterId).val(),$(deleteSegment.deleteSegmentNuggetId).val())
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};

var approveSegemnt = {
		approveSegmentBtn 	: ".approve-segment",
		rejectSegmentBtn	: ".reject-segment",
		approveModal 		: "#mdlApproveSegment",
		rejectModal 		: "#mdlRejectSegment",
		approveConfirmBtn	: "#approveSegmentConfimBtn",
		rejectConfirmBtn	: "#rejectConrimBtn",
		approveSegmentId 	: "#approveSegmentId",
		rejectSegmentId 	: "#rejectSegmentId",
		rejectSegmentForm 	: "#rejectSegmentForm",
		rejectComment 		: "#rejectComment",
		init : function(){
			$(document).on("click",this.approveSegmentBtn,this.initApprove);
			$(document).on("click",this.rejectSegmentBtn,this.initReject);
			$(document).on("click",this.approveConfirmBtn,this.fnApprove);
			$(this.rejectSegmentForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				rules : {
					rejectComment : {
						required :true
					}
				},
				messages : {
					rejectComment : {
						required : "Please enter comment"
					}
				},
				submitHandler : function(){
					approveSegemnt.fnReject();
				}
			});
		},
		initApprove : function(){
			var name = $(this).closest("tr").find(".segment-title").text();
			var msg = $("#approveSegmentMsg").data("message");
			$("#approveSegmentMsg").html(msg.replace('@NAME@',name));
			$(approveSegemnt.approveSegmentId).val($(this).data("segmentid"));
			$(approveSegemnt.approveModal).modal("show");
		},
		fnApprove : function(){
			var approveSegmentId = $(approveSegemnt.approveSegmentId).val();
			var ajaxData = {
					segmentId : approveSegmentId,
			};
			var approveAjaxCall = shiksha.invokeAjax("shiksha/contentmanagement/segment/approve", ajaxData, "POST");
			if(approveAjaxCall != null){
				if(approveAjaxCall.response == "shiksha-200"){
					$("tr[data-segmentid='"+approveSegmentId+"']").find(".reject-segment,.comment-segment").removeClass("disabled-icon");
					$("tr[data-segmentid='"+approveSegmentId+"']").find(".approve-segment,.undo-segment").addClass("disabled-icon");
					$("tr[data-segmentid='"+approveSegmentId+"']").find(".log-status-icon").removeClass("log-status--awaiting").addClass("log-status--approved").find("use").attr({
						"xlink:href" : "web-resources/images/content-logs.svg#approved",
						"href" : "web-resources/images/content-logs.svg#approved",
						"title" : statusMessage.approvedStatus
					});			
					
					var nuggetId = $("tr[data-segmentid='"+approveSegmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					
					$(approveSegemnt.approveModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : approveAjaxCall.responseMessage,
							close : 'auto'
						});
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : approveAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},
		initReject	: function(){
			resetForm(approveSegemnt.rejectSegmentForm);
			$(approveSegemnt.rejectSegmentId).val($(this).data("segmentid"));
			$(approveSegemnt.rejectModal).modal("show");
			
		},
		fnReject : function(){
			var rejectSegmentId = $(approveSegemnt.rejectSegmentId).val();
			var comment = $(approveSegemnt.rejectComment).val();
			var ajaxData = {
					segmentId : rejectSegmentId,
					rejectionComment : comment
			};
			var rejectAjaxCall = shiksha.invokeAjax("shiksha/contentmanagement/segment/reject", ajaxData, "POST");
			if(rejectAjaxCall != null){
				if(rejectAjaxCall.response == "shiksha-200"){
					$("tr[data-segmentid='"+rejectSegmentId+"']").find(".reject-segment,.approve-segment,.undo-segment").addClass("disabled-icon");
					//$("tr[data-segmentid='"+rejectSegmentId+"']").find(".approve-segment").removeClass("disabled-icon");
					var nuggetId = $("tr[data-segmentid='"+rejectSegmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					$("tr.shk-segment").remove();
					treeViewObj.getNuggetContent(chapterId, nuggetId);
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					$(approveSegemnt.rejectModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : rejectAjaxCall.responseMessage,
							close : 'auto'
						});
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : rejectAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};

var addNugget  = {
	addNuggetBtn 	: ".add-nugget",
	addNuggetModal 	: "#mdlAddNugget",
	addNuggetForm	: "#addNuggetForm",
	addNuggetChapterId : "#addNuggetChapterId",
	eleName			: "#addNuggetName",
	eleDescription	: "#addDescription",
	eleSequence		: "#addNuggetSequence",
	init 			: function(){
		$(document).on("click",this.addNuggetBtn,this.showAddNuggetModal);
		
		$(this.addNuggetForm).submit(function(e) {
			e.preventDefault();
		}).validate({
			ignore: '',
			rules: {
				nuggetName: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				},
				nuggetDescription: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				}
			},

			messages: {
				nuggetName: {
					required: nuggetMessages.nameRequired,
					noSpace: nuggetMessages.nameNoSpace,
					minlength: nuggetMessages.nameMinLength,
					maxlength: nuggetMessages.nameMaxLength,

				},
				nuggetDescription: {
					required: nuggetMessages.descRequired,
					minlength: nuggetMessages.descMinLength,
					maxlength: nuggetMessages.descMaxLength,
					noSpace: nuggetMessages.descNoSpace,
				}
			},
			errorPlacement: function(error, element) {
				$(element).parent().append(error);
			},
			submitHandler : function(){
				addNugget.fnAddNugget();
			}
		});
	},
	fnAddNugget : function(){
		spinner.showSpinner();
		var ajaxData = {
				
				"unit" 			: {
								"unitId" 		: $(addNugget.addNuggetChapterId).val()
				},
				"name"		  	: $(addNugget.eleName).val().replace(/\s\s+/g, ' ').trim(),
				"description" 	: $(addNugget.eleDescription).val()
		}
		var addAjaxCall = shiksha.invokeAjax("shiksha/nugget", ajaxData, "POST");
		
		if(addAjaxCall != null){
			if(addAjaxCall.response == "shiksha-200"){
										
					$(addNugget.addNuggetModal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
					if($("tr.shk-chapter[data-chapterid='"+addAjaxCall.unit.unitId+"']").hasClass("open")){
						var thumbNail='';
						if(cmPermissions.uploadThumbnail){
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'; 
						}else{
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"></div>';
						}	
						var str = '<tr class="shk-nugget '+filterClasses["close"][0]+'" data-chapterid="'+addAjaxCall.unit.unitId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">'
							+'<td><a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-nugget">'
							+'<div class="lms-log-status-section lms-side-center-far"><div class="lms-dropdown-toggle" href="#"><svg>'
							+'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#dropdown" href="web-resources/images/content-logs.svg#dropdown"></use></svg></div>'
							+'<div class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div></div>'
							/*+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
							+thumbNail
							+'<div><div><div class="log-title-text">'+addAjaxCall.name+'</div></div></div>'
							+'</a></td>'
							+'<td class="seg-count">Seg:0</td><td>'
							+'<div class="shk-action-container">'
							+((cmPermissions.addSegment)? '<a class="add-segment" data-chapterid="'+addAjaxCall.unit.unitId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">Add segment</a>' : "");
						var actions = "";
						if(true){
							actions+='<div class="dropdown">'
							actions+='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
							actions+='<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
							actions+='</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
							+'<li><a href="javascript:void(0)" class="view-nugget" data-chapterid="'+addAjaxCall.unit.unitId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">View</a></li>';
							(cmPermissions.editNugget)? actions+='<li><a href="javascript:void(0)" class="edit-nugget" data-chapterid="'+addAjaxCall.unit.unitId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">Edit</a></li>' : "";
							(cmPermissions.delNugget) ? actions+='<li><a href="javascript:void(0)" class="delete-nugget" data-chapterid="'+addAjaxCall.unit.unitId+'" data-nuggetid="'+addAjaxCall.nuggetId+'" onclick="deleteNugget.initDelete(\''+addAjaxCall.nuggetId+'\',\''+addAjaxCall.name+'\',\''+addAjaxCall.unit.unitId+'\')">Delete</a></li>': "";
							actions+='</ul></div>';
						}
						
						if(cmPermissions.editNugget && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
							str+=actions+'<span class="glyphicon glyphicon-move move-handle"></span></div></td></tr>';	
						}else{
							str+=actions+'</div></td></tr>';
						}
					
						var lastNuggetId = $(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+addAjaxCall.unit.unitId+"']").last().attr("data-nuggetid")
						if(!lastNuggetId)
							$(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+addAjaxCall.unit.unitId+"']").last().after(str);
						else 
							$(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+lastNuggetId+"']").last().after(str);
				}
				
				treeViewObj.enableNuggetSortable();
				$(addNugget.addNuggetChapterId).val(0);
				resetForm(addNugget.addNuggetForm);
				
				updateCountNuggetsAndChapter(addAjaxCall.unit.unitId);
								
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : addAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type  : "error",
				title : appMessgaes.oops,
				body  : appMessgaes.serverError,
				close : 'auto'
			})
		}
		spinner.hideSpinner();
	},
	showAddNuggetModal : function(){
		resetForm(addNugget.addNuggetForm);
		$(addNugget.addNuggetChapterId).val($(this).data("chapterid"));
		$(addNugget.addNuggetModal).modal("show");
	}
};

var editNugget  = {
		editNuggetBtn 	: ".edit-nugget",
		editNuggetModal 	: "#mdlEditNugget",
		editNuggetForm	: "#editNuggetForm",
		editNuggetId 	: "#editNuggetId",
		editNuggetName	: "#editNuggetName",
		editDescription	: "#editDescription",
		editNuggetSequence	: "#editNuggetSequence",
		editNuggetChapterId : "#editNuggetChapterId",
		init 			: function(){
			$(document).on("click",this.editNuggetBtn,this.showEditNuggetModal);
			
			$(this.editNuggetForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					nuggetName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					nuggetDescription: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}
				},

				messages: {
					nuggetName: {
						required: nuggetMessages.nameRequired,
						noSpace: nuggetMessages.nameNoSpace,
						minlength: nuggetMessages.nameMinLength,
						maxlength: nuggetMessages.nameMaxLength,

					},
					nuggetDescription: {
						required: nuggetMessages.descriptionRequired,
						minlength: nuggetMessages.descriptionMinLength,
						maxlength: nuggetMessages.descriptionMaxLength,
						noSpace: nuggetMessages.descriptionNoSpace,
					}
				},
				errorPlacement: function(error, element) {
					$(element).parent().append(error);
				},
				submitHandler : function(){
					editNugget.fnEditNugget();
				}
			});
		},
		fnEditNugget : function(){	
			var ajaxData = {
					"unit" 			: {
						"unitId" 		: $(editNugget.editNuggetChapterId).val()
					},
					"nuggetId"		: $(editNugget.editNuggetId).val(),
					"name"		  	: $(editNugget.editNuggetName).val().replace(/\s\s+/g, ' ').trim(),
					"description" 	: $(editNugget.editDescription).val(),
			}
			var editAjaxCall = shiksha.invokeAjax("shiksha/nugget", ajaxData, "PUT");
			
			console.log(editAjaxCall);
			
			if(editAjaxCall != null){
				if(editAjaxCall.response == "shiksha-200"){
						$(editNugget.editNuggetModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : editAjaxCall.responseMessage,
							close : 'auto'
						});
					$("tr[data-nuggetid='"+editAjaxCall.nuggetId+"'].shk-nugget .log-title-text").text(editAjaxCall.name);
					$(editAjaxCall.editNuggeId).val(0);
					resetForm(editNugget.editNuggetForm);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},
		showEditNuggetModal : function(){
			resetForm(editNugget.editNuggetForm);
			var nuggetId = $(this).data("nuggetid");
			var getAjaxCall = shiksha.invokeAjax("shiksha/nugget/"+nuggetId, null, "GET");
			$(editNugget.editNuggetId).val(nuggetId);
			$(editNugget.editNuggetName).val(getAjaxCall.name);
			$(editNugget.editDescription).val(getAjaxCall.description);
			$(editNugget.editNuggetSequence).val(getAjaxCall.sequence);
			$(editNugget.editNuggetChapterId).val($(this).data("chapterid"));
			$(editNugget.editNuggetId).val($(this).data("nuggetid"));
			
			spinner.hideSpinner();
			
			$(editNugget.editNuggetModal).modal("show");
		}
	};

var deleteNugget  = {
		deleteNuggetBtn 	: ".delete-nugget",
		deleteNuggetModal 	: "#mdlDeleteNugget",
		deleteNuggetConfirmBtn 	: "#deleteNuggetButton",
		deleteNuggetId : "#deleteNuggetId",
		deleteNuggetChapterId:"#deleteNuggetChapterId",
		init : function(){
			$(this.deleteNuggetConfirmBtn).on("click",this.fnDeleteNugget);
		},
		initDelete : function(nuggetId,name,chapterId){
			var msg = $("#deleteNuggetMessage").data("message");
			$("#deleteNuggetMessage").html(msg.replace('@NAME',name));
			$(deleteNugget.deleteNuggetId).val(nuggetId);
			$(deleteNugget.deleteNuggetChapterId).val(chapterId);
			$(deleteNugget.deleteNuggetModal).modal("show");
		},
		fnDeleteNugget : function(){
			spinner.showSpinner();
			
			var deleteNuggetId = $(deleteNugget.deleteNuggetId).val();
			
			var deleteAjaxCall = shiksha.invokeAjax("shiksha/nugget/"+deleteNuggetId, null, "DELETE");
			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-200"){
					$(deleteNugget.deleteNuggetModal).modal("hide");
					$("tr[data-nuggetid='"+deleteNuggetId+"']").remove();
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : deleteAjaxCall.responseMessage,
							close : 'auto'
						});
						updateCountNuggetsAndChapter($(deleteNugget.deleteNuggetChapterId).val());			
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};

var uploadSegment = {
	uploadActionBtn : ".upload-segment",
	uploadSegmentModal : "#uploadSegmentModal",
	inputFile : "#segmentFile",
	uploadSegmentIdBox : "#uploadSegemntId",
	uploadForm : "#uploadSegmentForm",
	refreshContentFiels : "#content-refresh",
	filetypes : "#filetypes",
	fileSearch : "#search-files",
	init : function(){
		
		$(document).on("click",this.uploadActionBtn,this.uploadSegmentInit);
		$(this.refreshContentFiels).on("click",this.getUnmappedfiles);
		$("#previewContentSegmentModal").on("hidden.bs.modal",function(){
			$(".content-preview-container").empty();
		});
		contentFilterObj.enableMultiSelectForFilters(this.filetypes);
		$(this.filetypes).on("change",this.getUnmappedfiles);
		$(this.fileSearch).on("keyup",this.searchFiles);
	},
	uploadSegmentInit : function(){
		$(uploadSegment.uploadSegmentIdBox).val($(this).parents("tr").data("segmentid"));
		$(uploadSegment.uploadSegmentModal).modal("show");
	},
	searchFiles : function(){
		var searchStr = $(uploadSegment.fileSearch).val();
		if(searchStr.trim()){
			$(".li-file").hide();
			searchStr = searchStr.toLowerCase();  
			
			$(".li-file").each(function(){
				var fileName = $(this).attr("data-filename");
				fileName = fileName.toLowerCase();
				if(fileName.indexOf(searchStr) > -1)
					$(this).show();
			});
			 
		} else {
			$(".li-file").show();
		}
	},
	submitUploadFile : function(file){
		spinner.showUploadSpinner();
		var formData = new FormData();
		formData.append( 'uploadfile',file );
		
		if(file){
			$.ajax({
		        url: "shiksha/content/upload",
		        type: "POST",
		        data: formData,
		        enctype: 'multipart/form-data',
		        processData: false,
		        contentType: false,
		        cache: false,
		        async : true,
		        success: function (response) {
		        	spinner.hideUploadSpinner();
		        	console.log(response,"successfully uploaded");
		        	if(response.response == "shiksha-200"){
		        		uploadSegment.getUnmappedfiles();
		        		//$(".segment-files-list").prepend("<li class='li-file' data-filename='"+file.name+"'>"+file.name+"</li>");
		        		AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : response.responseMessage,
							close : 'auto'
						});	
		        		$(".li-file").draggable({
		      			  refreshPositions: true,
		      			  revert: true,
		      			  helper: "clone"
		      		});
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : response.responseMessage,
							close : 'auto'
						});
					}
		        },
		        error: function (error) {
		          spinner.hideUploadSpinner();
		          console.log(error);
		          AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
		          });
		        }
		    });
		}else{spinner.hideUploadSpinner();}
		
	
	},
	mapFileToSegment : function(segmentId,fileName,fileType){
		spinner.showSpinner();
		$.ajax({
	        url: "shiksha/segment/"+segmentId+"/map?type="+fileType,
	        type: "POST",
	        contentType:"application/json",
			async : false,
	        data: JSON.stringify({fileName : fileName}),
	        success: function (response) {
	        	spinner.hideSpinner();
	        	console.log("successfully mapped",segmentId);
	        	if(response.response == "shiksha-200"){
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : response.responseMessage,
						close : 'auto'
					});
					var nuggetId = $("tr[data-segmentid='"+segmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					$("tr.shk-segment").remove();
					treeViewObj.getNuggetContent(chapterId, nuggetId);
					
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					if(fileType!=1){
						$(".li-file[data-filename='"+fileName+"']").remove();
					}
					
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : response.responseMessage,
					close : 'auto'
				});
			}
	        },
	        error: function (error) {
	          spinner.hideSpinner();
	          console.log(error);
	          AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
	          });
	        }
	      });
	},
	getUnmappedfiles :function(){
		$(".segment-files-list").empty();
		$(uploadSegment.fileSearch).val("");
		spinner.showSpinner();
		var fileType = $(uploadSegment.filetypes).val();
		var getAjaxCall = shiksha.invokeAjax("shiksha/content/files?type="+fileType, null, "GET");
		if(getAjaxCall.unmappedFiles.length){
			$.each(getAjaxCall.unmappedFiles,function(index,file){
				$(".segment-files-list").prepend("<li class='li-file' data-filename='"+file+"' data-type='"+fileType+"'>"+file+"</li>");
			});
		} else {
			
		}
		spinner.hideSpinner();
		$(".li-file").draggable({
			  refreshPositions: true,
			  revert: true,
			  helper: "clone"
		});
	}
};

var undoSegment = {
		undoActionBtn : ".undo-segment",
		undoSegmentModal : "#undoSegmentModal",
		UndoForm : "#undoSegmentForm",
		undoSegmentId : "#undoSegemntId",
		undoSegemntBtn : "#undoSegemntBtn",
		init : function(){
			$(document).on("click",this.undoActionBtn,this.undoSegmentInit);
			$(document).on("click",this.undoSegemntBtn,this.fnUndoSegmetnContent);
		},
		undoSegmentInit : function(){
			$(undoSegment.undoSegmentId).val($(this).parents("tr").data("segmentid"));
			$(undoSegment.undoSegmentModal).modal("show");
		},
		fnUndoSegmetnContent : function(){
			var undoSegmentId = $(undoSegment.undoSegmentId).val();
			var undoAjaxCall = shiksha.invokeAjax("shiksha/contentmanagement/segment/revert/"+undoSegmentId, null, "GET");
			if(undoAjaxCall != null){
				if(undoAjaxCall.response == "shiksha-200"){
					$(undoSegment.undoSegmentModal).modal("hide");
					var nuggetId = $("tr[data-segmentid='"+undoSegmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					$("tr.shk-segment").remove();
					treeViewObj.getNuggetContent(chapterId, nuggetId);
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					uploadSegment.getUnmappedfiles();
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : undoAjaxCall.responseMessage,
						close : 'auto'
					});
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : undoAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
			
		}
	};
var commentSegment = {
		commnetActionBtn : ".comment-segment",
		commentSegmentModal : "#commentSegmentModal",
		UndoForm : "#commentSegmentForm",
		commentSegemntId : "#commentSegemntId",
		commentList : "#list-comments",
		closeComments : "#close-comments",
		init : function(){
			$(document).on("click",this.commnetActionBtn,this.commentSegmentInit);
			$(this.closeComments).on("click",this.closeSegments);
		},
		commentSegmentInit : function(){
			$(commentSegment.commentList).empty();
			var segmentName = $(this).parents("tr").find(".title-segment").text();
			var duration 	= $(this).parents("tr").find("#duration").text();
			
			$("#comment-main-heading").text(segmentName);
			$("#comment-main-duration").text(duration);
			
			var commentSegemntId = $(this).parents("tr").data("segmentid");
			spinner.showSpinner();
			$("#logs-tab-container").hide();
			$("#comment-container").show();
			
			var commentAjaxCall = shiksha.invokeAjax("shiksha/contentmanagement/segment/comments/"+commentSegemntId, null, "GET");
			if(commentAjaxCall != null){
					$.each(commentAjaxCall,function(index,commentObj){
						var commentTmp = $("#comment-template").clone();
						$(commentTmp).removeAttr("id").show();
						$(commentTmp).find("#commenter-name").text(commentObj.rejectedBy);
						$(commentTmp).find("#comment-text").text(commentObj.rejectionComment);
						$(commentTmp).find("#comment-date").text(commentObj.rejectedDate);
						$(commentTmp).find("#comment-version").text(commentObj.version);
						$(commentSegment.commentList).append(commentTmp);
					});
				
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},
		closeSegments : function(){
			$("#logs-tab-container").show();
			$("#comment-container").hide();
		},
		
	};

var previewSegment = {
		previewActionBtn : ".preview-segment",
		previewSegmentModal : "#previewContentSegmentModal",
		previewForm : "#previewSegmentForm",
		previewSegemntId : "#previewSegemntId",
		init : function(){
			$(document).on("click",this.previewActionBtn,this.previewSegmentInit);
		},
		previewSegmentInit : function(){
			var previewSegemntId = $(this).parents("tr").data("segmentid");
			spinner.showSpinner();
			var getAjaxCall = shiksha.invokeAjax("shiksha/segment/"+previewSegemntId, null, "GET");
			$(".content-preview-container").empty();
			
			if(getAjaxCall != null){
				if(getAjaxCall.response == "shiksha-200"){					
					if(!getAjaxCall.content){
						$(".content-preview-container").append('<video width="320" height="240" controls autobuffer="true" autoplay ><source src="shiksha/contentmanagement/segment/preview/'+previewSegemntId+'" type="video/mp4"></video>')
						/*switch(getAjaxCall.contentType.name){
							case "Flash" :
								
								break;
							case "HTML" :
								
								break;
							case "Image" :
								
								break;
							case "MPEG-4" :
								
								break;
						}*/
						
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : "No content in segment "+getAjaxCall.segmentName,
							close : 'auto'
						});
					}
					
					$(previewSegment.previewSegmentModal).modal("show");
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : getAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
			
			
			
			
			$(previewSegment.previewSegmentModal).modal("show");
		},
	};

var logsObj = {
		
		logRefresh : "#log-refresh",
		logDownload : "#log-download",
		init : function(){
			$(this.logRefresh).on("click",this.refreshLogs);
			$(this.logDownload).on("click",this.downloadLogs);
		},
		refreshLogs : function(){
			spinner.showSpinner();
			var getAjaxCall = shiksha.invokeAjax("shiksha/contentmanagement/logs", null, "GET");
			$("#logs-list").empty();
			
			$.each(getAjaxCall,function(index,log){
				var str = '<li class="log-item lms-log-item"><div class="lms-log-by"><div class="lms-comment-person-icon log-user-icon"><svg> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#user" href="web-resources/images/content-logs.svg#user"></use></svg></div>'
					+'<span>'+log.logType+': '+log.modifiedBy+'</span></div><div class="lms-log-sg">Segment : '+log.segmentName+'</div><div class="lms-side-center lms-log-details">'
					+'<div class="lms-log-details-version">Upload Version: '+log.version+'</div><div>'+log.date+'</div><div>'+log.time+'</div></div>'
					+'<div class="lms-log-status"> Status: '+log.status+' </div></li>';
				$("#logs-list").append(str);
			});
			spinner.hideSpinner();
		},
		
		downloadLogs : function(){
			window.location = baseContextPath +"/shiksha/segment/logs/download";
		},
		
		
		viewSegmentLogs : function(segmentId){
			$("#aui-uid-1").trigger("click");
			spinner.showSpinner();
			var getAjaxCall = shiksha.invokeAjax("shiksha/segment/"+segmentId+"/logs", null, "GET");
			$("#logs-list").empty();
			
			$.each(getAjaxCall,function(index,log){
				var str = '<li class="log-item lms-log-item"><div class="lms-log-by"><div class="lms-comment-person-icon log-user-icon"><svg> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#user" href="web-resources/images/content-logs.svg#user"></use></svg></div>'
					+'<span>'+log.logType+': '+log.modifiedBy+'</span></div><div class="lms-log-sg">Segment : '+log.segmentName+'</div><div class="lms-side-center lms-log-details">'
					+'<div class="lms-log-details-version">Upload Version: '+log.version+'</div><div>'+log.date+'</div><div>'+log.time+'</div></div>'
					+'<div class="lms-log-status"> Status: '+log.status+' </div></li>';
				$("#logs-list").append(str);
			});
			spinner.hideSpinner();
		}
		
};

var viewSegement = {
		viewModal : "#viewSegmentModal",
		viewtAction : ".view-segment",
		init : function(){
			$(document).on("click",this.viewtAction,this.fnViewSegment);
		},
		fnViewSegment : function(){
			var segmentId = $(this).data("segmentid");
			var getAjaxCall = shiksha.invokeAjax("shiksha/segment/"+segmentId, null, "GET");
			$("#seg-type").text(getAjaxCall.segmentType.segmentTypeName);
			$("#seg-con-type").text(getAjaxCall.contentType.name);
			$("#seg-name").text(getAjaxCall.segmentName);
			$("#seg-desc").text(getAjaxCall.description);
			$("#seg-sequ").text(getAjaxCall.sequence);
			var res=getAjaxCall.resolution!=null?getAjaxCall.resolution==1?"SD":"HD":"N/A";
			if(getAjaxCall.contentType.name=="HTML"){
				res="N/A"
			}
			$("#seg-res").text(res);
			$("#seg-res").text(res);
			var content = "N/A";
			if(getAjaxCall.contentUrl)
				content = getAjaxCall.contentUrl
			$("#seg-content").text(content);
			$(viewSegement.viewModal).modal("show");
			spinner.hideSpinner();
		}
};

var viewNugget = {
		viewModal : "#viewNuggetModal",
		viewtAction : ".view-nugget",
		init : function(){
			$(document).on("click",this.viewtAction,this.fnViewNugget);
		},
		fnViewNugget : function(){
			var nuggetId = $(this).data("nuggetid");
			var getAjaxCall = shiksha.invokeAjax("shiksha/nugget/"+nuggetId, null, "GET");
			console.log(getAjaxCall);
			$("#nugget-name").text(getAjaxCall.name);
			$("#nugget-desc").text(getAjaxCall.description);
			$("#nugget-sequ").text(getAjaxCall.sequence);
			$(viewNugget.viewModal).modal("show");
			spinner.hideSpinner();
		}
};

var tumbnailObj = {
		fileInput : ".thumbnail-file",
		init : function(){
			$(document).on("change",this.fileInput,this.uploadThumbnail)
		},
		uploadThumbnail : function(){
			var parentTR = $(this).closest("tr");
			var file = $(this)[0].files[0];
			var fileName = file.name.split(".");
			if(['jpg','JPG','png','PNG','jpeg','JPEG'].indexOf(fileName[fileName.length -1 ]) < 0){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : "Only jpg or png files allowed.",
					close : 'auto'
				});
				return false;
			}else if (this.files[0] && (this.files[0].size > 10485760)) {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : "Image size should be less than 10mb",
					close : 'auto'
				});
				return false;
			}
			
			var type 	= $(this).closest("a").attr("data-type");
			var typeName = "";
			var id 		= 0;
			if(type == "shk-chapter"){
				typeName 	= "c";
				id		= $(this).closest("tr").attr("data-chapterid");
			} else if(type == "shk-nugget"){
				typeName 	= "n";
				id		= $(this).closest("tr").attr("data-nuggetid");
			} else {
				typeName 	= "s";
				id		= $(this).closest("tr").attr("data-segmentid");
			}
			
			spinner.showSpinner();
			var formData = new FormData();
			formData.append( 'uploadfile',file );
			$.ajax({
		        url: "shiksha/thumbnail/upload?type="+typeName+"&id="+id,
		        type: "POST",
		        data: formData,
		        enctype: 'multipart/form-data',
		        processData: false,
		        contentType: false,
		        cache: false,
		        success: function (response) {
		        	spinner.hideSpinner();
		        	if(response.response == "shiksha-200"){
		        		parentTR.find(".lms-content-log-thumbnail").attr("src",response.thumbnailUrl);
		        		AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : response.responseMessage,
							close : 'auto'
						});	
		        		$(".li-file").draggable({
		      			  refreshPositions: true,
		      			  revert: true,
		      			  helper: "clone"
		      		});
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : response.responseMessage,
							close : 'auto'
						});
					}
		        },
		        error: function (error) {
		          spinner.hideSpinner();
		          console.log(error);
		          AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
		          });
		        }
		    });
		}
};

function updateCountNuggetsAndChapter(chapterId,nuggetId){
	if(chapterId){	
		var ajaxData = {
				chapter:chapterId,
				filterSubType:$(contentFilterObj.filterBoxView).val(),
				search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()
		}
		var countChapterData=shiksha.invokeAjax("shiksha/contentmanagement/chapter/count", ajaxData, "POST")
		if(countChapterData.response == "shiksha-200"){
			if(countChapterData.chapters && countChapterData.chapters.length){
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				var otherCount = 0;
				$.each(countChapterData.chapters[0].nuggets,function(i, nugget){
					if(nugget.totalAwaiting || nugget.totalRejected || nugget.totalUnmapped)
						otherCount++;
				})
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						+'<div class="actions-count" title="Awaiting approval">'+otherCount+'</div></div>';
				
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.count').text("Nug:"+countChapterData.chapters[0].noOfNuggets+"/Seg:"+countChapterData.chapters[0].noOfSegments);
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.log-status-icon').replaceWith(statusIcon);
			} else {
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.count').text("Nug:0/Seg:0");
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.log-status-icon').replaceWith(statusIcon);
			}
				
		}	
	}

	if(nuggetId){
		var ajaxData = {
				nugget: nuggetId,
				filterSubType:$(contentFilterObj.filterBoxView).val(),
				search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()
		}
		var countNuggetData=shiksha.invokeAjax("shiksha/contentmanagement/nugget/count", ajaxData, "POST")
		if(countNuggetData.response == "shiksha-200"){
			
			if(countNuggetData.shikshaNuggets && countNuggetData.shikshaNuggets.length){	
				var nugget     = countNuggetData.shikshaNuggets[0]
				var otherCount = nugget.totalAwaiting + nugget.totalRejected + nugget.totalUnmapped;
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'"  class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
					+'<div class="actions-count" title="Awaiting approval">'+otherCount+'</div></div>';
			
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.seg-count').text("Seg:"+nugget.noOfSegments);
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.log-status-icon').replaceWith(statusIcon);
			} else {
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.seg-count').text("Seg:0");
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.log-status-icon').replaceWith(statusIcon);
			}	
		}
	}
	
}

function resetForm(formId){
	$(formId).find("label.error").remove();
	$(formId).find(".error").removeClass("error");
	$(formId)[0].reset();
}

$( document ).ready(function() {
	$.fn.editable.defaults.mode = 'popup'; 
	contentFilterObj.init();
	addSegment.init();
	editSegment.init();
	addNugget.init();
	editNugget.init();
	uploadSegment.init();
	logsObj.init();
	viewSegement.init();
	viewNugget.init();
	
	$(document).on("click",'.lms-content-log-thumbnail-section',function(e){
		e.stopPropagation();
	});
	$("#segmentFileinput").on("change",function(){
		
	uploadSegment.submitUploadFile($(this).get(0).files[0]);				
	
	});
	
	$(".li-file").draggable({
		  revert: true,
		  refreshPositions: true,
		  helper: "clone"
	});
	$("#uploadSegmentForm").submit(function(e) {
		e.preventDefault();
	}).validate({
		rules : {
			segmentFile  : {
				required : true,
				extension :"mp4|mpeg|dat|swf"
				
			}
		},
		messages : {
			segmentFile  : {
				required : "Please select file to upload",
				extension : "Please select proper file"
				
			}
		},
		submitHandler : function(){
			uploadSegment.submitUploadFile($(uploadSegment.uploadSegmentIdBox).val(),$(uploadSegment.inputFile)[0].files[0]);
		}
	});
	treeViewObj.init();	
	deleteSegment.init();
	deleteNugget.init();
	approveSegemnt.init();
	undoSegment.init();
	commentSegment.init();
	previewSegment.init();
	$("tbody").sortable();
	tumbnailObj.init();
	if(cmPermissions.uploadContent){
		uploadSegment.getUnmappedfiles();
	}
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});