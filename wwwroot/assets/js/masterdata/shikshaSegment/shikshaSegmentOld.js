var pageContextElements = {	
		addButton 						: '#addNewSegment',
		table 							: '#segmentTable',
};
var addModal ={
		form 			: '#addSegmentForm',
		modal   		: '#mdlAddSegment',
		saveMoreButton	: 'addSegmentSaveMoreButton',
		saveButton		: 'addSegmentSaveButton',
		eleName		   	: '#addSegmentName',
		eleContentType  : '#addContentType',
		eleSegmentType	: '#addSegmentType',
		eleTime			: '#addTime',
		eleVersion		: '#addVersion',
		eleDesc			: '#addDescription'
};
var editModal ={
			form	 : '#editSegmentForm',
			modal	 : '#mdlEditSegment',
			eleName	 : '#editSegmentName',
			eleId	 : '#editSegmentId',
			eleContentType  : '#editContentType',
			eleSegmentType	: '#editSegmentType',
			eleTime			: '#editTime',
			eleVersion		: '#editVersion',
			eleDesc			: '#editDescription'
			
};
var deleteModal ={
			modal	: '#mdlDeleteSegment',
			confirm : '#deleteSegmentButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var segmentIdForDelete;
var segmentFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(value, row, index, field){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var segment={
		name:'',
		segmentId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',segment.initAddModal);
			$(deleteModal.confirm).on('click',segment.deleteFunction);

		},
		deleteFunction : function(){
			segment.fnDelete(segmentIdForDelete);
		},
		initAddModal :function(){
			segment.resetForm(addModal.form);
			$(addModal.modal).modal("show");
			shikshaPlus.fnGetContentTypes(addModal.eleContentType,0);
			shikshaPlus.fnGetSegmentTypes(addModal.eleSegmentType,0);
			setOptionsForMultipleSelect(addModal.eleContentType);
			setOptionsForMultipleSelect(addModal.eleSegmentType);

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"segmentName"	:$(addModal.eleName).val(),
					"contentTypeId"	:$(addModal.eleContentType).val(),
					"segmentTypeId"	:$(addModal.eleSegmentType).val(),
					"time"			:$(addModal.eleTime).val(),
					"description"	:$(addModal.eleDesc).val(),
					"version"		:$(addModal.eleVersion).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("segment", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					segment.resetForm(addModal.form);
					segment.refreshTable(pageContextElements.table);

					AJS.flag({
						type  : "success",
						title : appMessgaes.saved,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(segmentId){
			segment.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("segment/"+segmentId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.segmentName);
					$(editModal.eleId).val(editAjaxCall.segmentId);
					$(editModal.eleContentType).val(editAjaxCall.contentTypeId);
					$(editModal.eleSegmentType).val(editAjaxCall.segmentTypeId);
					$(editModal.eleTime).val(editAjaxCall.time);
					$(editModal.eleVersion).val(editAjaxCall.version);
					$(editModal.eleDesc).val(editAjaxCall.description);
					shikshaPlus.fnGetContentTypes(editModal.eleContentType,editAjaxCall.contentTypeId);
					shikshaPlus.fnGetSegmentTypes(editModal.eleSegmentType,editAjaxCall.segmentTypeId);
					setOptionsForMultipleSelect(editModal.eleContentType);
					setOptionsForMultipleSelect(editModal.eleSegmentType);
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"segmentId"		:$(editModal.eleId).val(),
					"segmentName"	:$(editModal.eleName).val(),
					"contentTypeId"	:$(editModal.eleContentType).val(),
					"segmentTypeId"	:$(editModal.eleSegmentType).val(),
					"time"			:$(editModal.eleTime).val(),
					"description"	:$(editModal.eleDesc).val(),
					"version"		:$(editModal.eleVersion).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("segment", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					segment.resetForm(editModal.form);
					segment.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: appMessgaes.updated,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: "Error..!",
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(segmentId){
			$(deleteModal.modal).modal("show");
			segmentIdForDelete=segmentId;
		},
		fnDelete :function(segmentId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("segment/"+segmentId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					segment.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.deleted,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					segmentName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					description: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}	,
				},

				messages: {
					segmentName: {
						required: "Please enter segment name",
						noSpace:"Please enter valid segment Name",
						minlength: "The segment name must be more than 2 and less than 50 characters long",
						maxlength: "The segment name must be more than 2 and less than 50 characters long",

					},
					description: {
						required: "Please enter segment version",
						noSpace:"Please enter valid segment version",
						minlength: "The segment version must be more than 2 and less than 100 characters long",
						maxlength: "The segment version must be more than 2 and less than 100 characters long",
					}	,
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						segment.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						segment.fnUpdate();
					}
				}
			});
		},
		segmentActionFormater: function(value, row, index) {
			console.log(value, row, index);
			/*var actionBtns = '<a rel="tooltip" data-tooltip="true" data-original-title="Edit" class="tooltip tooltip-top" onclick="segment.initEdit(\''+row.segmentId+'\')"> <span class="tooltiptext">Edit</span><span class="glyphicon glyphicon-edit font-size12"></span> </a>'
			+ '<a style="margin-left:10px;" class="tooltip tooltip-top" onclick="segment.initDelete(\''+row.segmentId+'\')"><span class="tooltiptext">Delete</span><span class="glyphicon glyphicon-trash font-size12"></span></a>';
*/
			return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
			+ '<li><a onclick="segment.initEdit(\''+ row.segmentId+ '\')" href="javascript:void(0)">Edit</a></li>'
			+ '<li><a onclick="segment.initDelete(\''+ row.segmentId+ '\')" href="javascript:void(0)" class="delLink">Delete</a></li>'
			+ '</ul>';
		},
		segmentNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	segment.formValidate(addModal.form);
	segment.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	segment.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: "#toolbarSegment",
		url : "segment",
		method : "get",
		toolbarAlign :"right",
		search: true,
		sidePagination: "server",
		showToggle: false,
		showColumns: true,
		pagination: true,
		searchAlign: 'left',
		pageSize: 10,
		clickToSelect: false,
		formatShowingRows : segmentFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
	$('.timepicker').datetimepicker({
		format: 'hh:mm a',
	});
});