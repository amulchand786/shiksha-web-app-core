var existedLevels=[];
var mapperId,batchDayId;
var mapperIdForDelete,mapperIdForDayDelete,deletedDayName;
var levelBatchMappingElements={
	tab				: '#tabLevelBatch',
	panel			: '#leveBatchPanel',
	panelMessage	: '#panelMessage',
	levelMasters	: '#levelMasters',
	batchMasters	: '#batchMasters',
	save        	: '#btnLevelBatchSave',
	center			: '#selectBox_centersByLoc',
	table			: '#tblLevelBatch',
	editModal		: '#mdlEditBatchDays',
	viewDaysModal 	: '#mdlViewBatchDays',
	saveEditDetails	: '#editBatchDaysSaveButton',
	cancelEditDetails	: '#editBatchDaysCancelButton',
	cancelMdlButton: '#editBatchDaysMdlCancelButton',
	deleteModal 	: '#mdlDeleteLevelBatchMapping',
	deleteConfirm 	: '#deleteLevelBatchMappingButton',
	deleteDayModal	: '#mdlDeleteBatchDay',
	deleteDayConfirm: '#deleteBatchDayButton',
	viewTable		: "#daysTableInView tbody",
	editTable		: '#daysTableInEdit',
	deleteBtn		: '#btnDelete',
	eleToggleCollapse		: '[data-toggle="collapse"]',
	fnAddLevels 	: function(){
		$(levelBatchMappingElements.levelMasters).empty();
		var	levels = shiksha.invokeAjax("level/list", null, "GET");
		if(levels != null){
			$.each(levels, function(index,obj){
				var div = '<div id="level'+obj.levelId+'" title="">'+
						  '<input type="radio" class="radioLabel" id="'+obj.levelName+'" data-code="'+obj.levelCode+'" value="'+obj.levelId+'" name="levels">'+
						  '<label class="radioLabel" for="'+obj.levelName+'">'+obj.levelName+'</label>'+
						  '</div>';
				$(levelBatchMappingElements.levelMasters).append(div);
			})
		}
		$.each(existedLevels,function(index,object){
			$("#level"+object).remove();
		})
		if($(levelBatchMappingElements.levelMasters).is(':empty')){
			$(levelBatchMappingElements.panel).hide();
			$(levelBatchMappingElements.panelMessage).show();
		}
		
	},
	fnAddBatches : function(){
		$(levelBatchMappingElements.batchMasters).empty();
		var	batches = shiksha.invokeAjax("batch/list", null, "GET");
		if(batches != null){
			$.each(batches, function(index,obj){
				var div = '<div id="'+obj.batchId+'" title="" class="row">'+
						  '<input type="checkbox" class="radioLabel" id="batch'+obj.batchId+'" data-code="'+obj.batchCode+'" value="'+obj.batchId+'" name="batches">'+
						  '<label class="radioLabel" for="batch'+obj.batchId+'">'+obj.batchName+'</label>'+
						  '</div>';
				$(levelBatchMappingElements.batchMasters).append(div);
			})
			
		}
	},
	fnShowLevelBatchMappingTable : function(){
		var centerId=$(levelBatchMappingElements.center).val();
		$("#tblLevelBatch").bootstrapTable({
			//url : 'centerconfig/center/'+centerId,
			method : "get",
			toolbarAlign :"right",
			search: false,
			sidePagination: "client",
			showToggle: false,
			showColumns: false,
			pagination: true,
			searchAlign: 'left',
			pageSize: 20,
			clickToSelect: true,
		});
		$("#tblLevelBatch").bootstrapTable("refresh",{url : 'centerconfig/center/'+centerId});
	},
};
var batchDays=[];
var batchId,day,startTime,endTime,data;
var batch=[];
var batchIds=[];
var days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
var checkboxes={
	days		: '#daysTable [name="days"]',
	levels		: '#levelMasters [name="levels"]',
	batches		: '#batchMasters [name="batches"]',
	startTime	: '.startTime',
	endTime		: '.endTime',
};
var levelBatchMapping ={		
	centerLevelBatchMapperId:'',
	init:function(){
		
		$(levelBatchMappingElements.panelMessage).hide();
		$(levelBatchMappingElements.tab).on('click',levelBatchMapping.initTab);
		$(levelBatchMappingElements.save).on('click',levelBatchMapping.initSaveLevelBatch);	
		$(levelBatchMappingElements.saveEditDetails).on('click',levelBatchMapping.fnUpdateBatchDays);
		$(levelBatchMappingElements.cancelMdlButton).on('click',levelBatchMapping.fnCancel);
		$(levelBatchMappingElements.cancelEditDetails).on('click',levelBatchMapping.fnCancel);
		$(levelBatchMappingElements.deleteConfirm).on('click',levelBatchMapping.fnDelete);
		$(levelBatchMappingElements.deleteDayConfirm).on('click',levelBatchMapping.fnDeleteDay);
	},
	initTab:function(){
		existedLevels=[];
		$(levelBatchMappingElements.table).bootstrapTable("refresh");
		levelBatchMappingElements.fnAddLevels();
		levelBatchMappingElements.fnAddBatches();
		levelBatchMapping.batchReset();
		spinner.hideSpinner();
		if(!$(checkboxes.days).is(':checked')){
			$.each($(checkboxes.days),function(){
				$(this).parent().siblings().find(checkboxes.startTime).prop('disabled',true);
				$(this).parent().siblings().find(checkboxes.endTime).prop('disabled',true);
			})
		}
	},
	daysCheckboxChecked:function(element){
		if(element.is(':checked')){
			element.parent().siblings().find(checkboxes.startTime).prop('disabled',false);
			element.parent().siblings().find(checkboxes.endTime).prop('disabled',false);
			element.parent().siblings().find(checkboxes.startTime).val('08:00 am');
			element.parent().siblings().find(checkboxes.endTime).val('09:00 am');
		}
		else{
			element.parent().siblings().find(checkboxes.startTime).prop('disabled',true);
			element.parent().siblings().find(checkboxes.endTime).prop('disabled',true);
			element.parent().siblings().find(checkboxes.startTime).val('');
			element.parent().siblings().find(checkboxes.endTime).val('');
		}
		
	},
	daysCheckboxCheckedInEdit:function(element){
		if(element.is(':checked')){
			element.parent().siblings().find(checkboxes.startTime).prop('disabled',false);
			element.parent().siblings().find(checkboxes.endTime).prop('disabled',false);
			element.parent().siblings().find(checkboxes.startTime).val('08:00 am');
			element.parent().siblings().find(checkboxes.endTime).val('09:00 am');
		}
		else{
			element.parent().siblings().find(checkboxes.startTime).prop('disabled',true);
			element.parent().siblings().find(checkboxes.endTime).prop('disabled',true);
			element.parent().siblings().find(checkboxes.startTime).val('');
			element.parent().siblings().find(checkboxes.endTime).val('');
		}
	},
	initSaveLevelBatch:function(){
		batchDays=[];
		batch=[];
		batchIds=[];
		var count=-1;
		if ($(checkboxes.levels).is(':checked')){
			if ($(checkboxes.batches).is(':checked')){
				$.each($(checkboxes.batches+':checked'),function(){
					batchId=$(this).val();
					batchIds.push(batchId);
				})
				if($(checkboxes.days).is(':checked')){
					count=$(checkboxes.days+":checked").length;
					$.each($(checkboxes.days+":checked"), function(){
						if(($(this).parent().siblings().find(checkboxes.startTime).val())!='' && ($(this).parent().siblings().find(checkboxes.endTime).val())!=''){
							count--;
						}
					})
					
				}
				else{
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : messages.batchDays,
						close : 'auto'
					})
				}
			}
			else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : messages.batch,
					close : 'auto'
				})
			}
		}
		else{
			AJS.flag({
				type  : "error",
				title : appMessgaes.oops,
				body  : messages.level,
				close : 'auto'
			})
		}
		if(count==0)
		{levelBatchMapping.fnSaveLevelBatch();}
		else if(count!= -1){
			AJS.flag({
				type  : "error",
				title : appMessgaes.oops,
				body  : messages.timings,
				close : 'auto'
			})
		}
	},
	fnSaveLevelBatch:function(){
		$.each($(checkboxes.days+":checked"), function(){            
			day=$(this).val();
            startTime =$(this).parent().siblings().find(checkboxes.startTime).val();
            endTime =$(this).parent().siblings().find(checkboxes.endTime).val();
            var daysObject={
            		'day':day,
            		'startTime':startTime,
            		'endTime':endTime
            };
            batchDays.push(daysObject);
        });
		$.each(batchIds, function(index,val){ 
	        var batchObject={
				'batchId'		:val,
				'batchDays' :batchDays
			};
	        batch.push(batchObject);
		});
		data={
				"level" : {'levelId':$(checkboxes.levels+':checked').val()},
				"center": {'id':$(levelBatchMappingElements.center).val()},
				"batch" : batch
		};
		var ajaxCall =shiksha.invokeAjax("centerconfig",data, "POST");
		spinner.hideSpinner();
		if(ajaxCall !=null){
			if(ajaxCall[0].response == "shiksha-200"){
				levelBatchMapping.batchReset();
				$("#level"+data.level.levelId).remove();
				$(levelBatchMappingElements.table).bootstrapTable("refresh");
				
				AJS.flag({
					type  : "success",
					title : messages.sucessAlert,
					body  : ajaxCall[0].responseMessage,
					close : 'auto'
				})
			}
		}
	},
	batchReset:function(){
		$.each($(checkboxes.batches+":checked"), function(){  
			var batchId=$(this).attr('id');
			$('#'+batchId).removeAttr('checked');
		})
		$.each($(checkboxes.days+":checked"), function(){  
			var dayId=$(this).attr('id');
			$('#'+dayId).removeAttr('checked');
		})
		if(!$(checkboxes.days).is(':checked')){
			$.each($(checkboxes.days),function(){
				$(this).parent().siblings().find(checkboxes.startTime).prop('disabled',true);
				$(this).parent().siblings().find(checkboxes.endTime).prop('disabled',true);
			})
		}
		$.each($(".timepicker"), function(){  
			$(this).val('');
		})
	},
	initViewDays:function(centerLevelBatchMapperId){
		$(levelBatchMappingElements.viewDaysModal).modal('show');
		var ajaxCall =shiksha.invokeAjax("centerconfig/days/"+centerLevelBatchMapperId,null, "GET");
		spinner.hideSpinner();
		if(ajaxCall !=null){
			$(levelBatchMappingElements.viewTable).empty();
			$.each(ajaxCall,function(index,object){
				$(levelBatchMappingElements.viewTable).append('<tr><td>'+object.day+'</td><td>'+object.startTime+'</td><td>'+object.endTime+'</td></tr>');	
			});
		}
	},
	initEdit:function(centerLevelBatchMapperId){
		levelBatchMapping.centerLevelBatchMapperId =centerLevelBatchMapperId;
		$.each(days,function(ind,obj){
			$('input[name="'+obj+'"').prop('checked',false).prop('disabled',false);
			$.each($(levelBatchMappingElements.editTable+' [name="'+obj+'"]'),function(){
				$(this).parent().siblings().find(checkboxes.startTime).prop('disabled',true).val('');
				$(this).parent().siblings().find(checkboxes.endTime).prop('disabled',true).val('');
				$(this).parent().siblings().find('.tooltip').hide();
			})
		})
		mapperId=centerLevelBatchMapperId;
		$(levelBatchMappingElements.editModal).modal('show');
		var ajaxCall =shiksha.invokeAjax("centerconfig/days/"+levelBatchMapping.centerLevelBatchMapperId,null, "GET");
		spinner.hideSpinner();
		if(ajaxCall !=null){
			$.each(ajaxCall,function(index,object){
				$.each(days,function(ind,obj){
					if(object.day == obj){
						$('input[name="'+object.day+'"').prop('checked',true);
						$('input[name="'+object.day+'"').prop('disabled',true);
						$('#edit'+object.day+'StartTime').prop('disabled',false);
						$('#edit'+object.day+'StartTime').attr('batchDayId',object.id);
						$('#edit'+object.day+'EndTime').prop('disabled',false);
						$('#edit'+object.day+'StartTime').val(object.startTime);
						$('#edit'+object.day+'EndTime').val(object.endTime);
						$('#btnDelete'+object.day).attr('value',object.id);
						$('#btnDelete'+object.day).show();
					}
				})
			})
		}
		
	},
	fnCancel:function(){
		var daysSelected=false;
		$.each($("#daysTableInEdit input[type='checkbox']:disabled"), function(){    
			daysSelected=true;

		});
		if(daysSelected){
			$(levelBatchMappingElements.editModal).modal('hide');
		} else{
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : messages.batchDays,
				close : 'auto'
			})
		}

	},
	fnUpdateBatchDays:function(){
		var batchDays=[];
		var flag=true;
		var daysSelected=false;
		$.each($("#daysTableInEdit input[type='checkbox']:checked"), function(){    
			daysSelected=true;
			day=$(this).val();
            startTime =$(this).parent().siblings().find(checkboxes.startTime).val();
            endTime =$(this).parent().siblings().find(checkboxes.endTime).val();
            batchDayId=$(this).parent().siblings().find(checkboxes.startTime).attr('batchDayId');
            if(startTime == '' || endTime ==''){
            	flag=false;
            }
            var daysObjectInEdit={
            		'id' :batchDayId,
            		'day':day,
            		'startTime':startTime,
            		'endTime':endTime
            };
            batchDays.push(daysObjectInEdit);
        })
        if(daysSelected){
        	if(flag){
            	var ajaxCall =shiksha.invokeAjax("centerconfig/"+levelBatchMapping.centerLevelBatchMapperId,batchDays, "PUT");
        		spinner.hideSpinner();
        		if(ajaxCall !=null){
        			if(ajaxCall.response =="shiksha-200"){
        			AJS.flag({
        				type  : "success",
        				title : messages.sucessAlert,
        				body  : ajaxCall.responseMessage,
        				close : 'auto'
        			})
        			$(levelBatchMappingElements.editModal).modal('hide');
        			}
        			else{
        				AJS.flag({
        					type  : "error",
        					title : appMessgaes.error,
        					body  : ajaxCall.responseMessage,
        					close : 'auto'
        				})
        			}
        		}
            }
            else{
            	AJS.flag({
    				type  : "error",
    				title : appMessgaes.error,
    				body  : messages.timings,
    				close : 'auto'
    			})
            }
        }
        else{
        	AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : messages.batchDays,
				close : 'auto'
			})
        }
        
		
	},
	
	initDelete:function(centerLevelBatchMapperId){
		$(levelBatchMappingElements.deleteModal).modal('show');
		mapperIdForDelete =centerLevelBatchMapperId;
		
	},
	fnDelete:function(){
		var ajaxCall =shiksha.invokeAjax("centerconfig/"+mapperIdForDelete,null, "DELETE");
		spinner.hideSpinner();
		existedLevels=[];
		if(ajaxCall !=null){
			if(ajaxCall.response == "shiksha-200"){
				$(levelBatchMappingElements.table).bootstrapTable("refresh");
				console.log(existedLevels);
				levelBatchMappingElements.fnAddLevels();
				levelBatchMappingElements.fnAddBatches();
				$(levelBatchMappingElements.panel).show();
				$(levelBatchMappingElements.panelMessage).hide();
				$(levelBatchMappingElements.deleteModal).modal('hide');
				levelBatchMapping.batchReset();
				AJS.flag({
					type  : "success",
					title : messages.sucessAlert,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				})
			}else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				})
			}
		}
	},
	initDeleteDays:function(element){
		if($("#daysTableInEdit .radioLabel.disabled:checked").length == 1){
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : "Atleast one day required",
				close : 'auto'
			})
			return false;
		}
		mapperIdForDayDelete=element.attr('value');
		deletedDayName=element.attr('dayName');
		$(levelBatchMappingElements.deleteDayModal).modal('show');
	},
	fnDeleteDay:function(){
		var ajaxCall =shiksha.invokeAjax("centerconfig/day/"+mapperIdForDayDelete,null, "DELETE");
		spinner.hideSpinner();
		if(ajaxCall !=null){
			if(ajaxCall.response == "shiksha-200"){
				$(levelBatchMappingElements.deleteDayModal).modal('hide');
				$.each(days,function(ind,obj){
					if(deletedDayName == obj){
						$('input[name="'+deletedDayName+'"').prop('checked',false);
						$('input[name="'+deletedDayName+'"').prop('disabled',false);
						$('#edit'+deletedDayName+'StartTime').prop('disabled',true);
						$('#edit'+deletedDayName+'EndTime').prop('disabled',true);
						$('#edit'+deletedDayName+'StartTime').val('');
						$('#edit'+deletedDayName+'EndTime').val('');
						$('#edit'+deletedDayName+'StartTime').removeAttr('batchDayId');
						$('#btnDelete'+deletedDayName).hide();
					}
				})
				AJS.flag({
					type  : "success",
					title : messages.sucessAlert,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				})
			}else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				})
			}
		}
	},
	batchNameActionFormatter:function(index,row){
		return row.batch[0].batchName;
	},
	centerLevelBatchActionFormater : function(value,row){
		existedLevels=[];
		existedLevels.push(row.level.levelId);
		$.each(existedLevels,function(index,object){
			$("#level"+object).remove();
		})
		if($(levelBatchMappingElements.levelMasters).is(':empty')){
			$(levelBatchMappingElements.panel).hide();
			$(levelBatchMappingElements.panelMessage).show();
		}
		return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
				+ '<li><a onclick="levelBatchMapping.initViewDays('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)">'+messages.viewDays+'</a></li>'
				+ '<li><a onclick="levelBatchMapping.initEdit('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>'
				+ '<li><a onclick="levelBatchMapping.initDelete('+row.centerLevelBatchMapperId+')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>'
				+ '</ul>';
	},
	
};
var actionFormater=function(){
	return {
		classes:"dropdown dropdown-td"
	}
};
$( document ).ready(function() {
	levelBatchMapping.init();
	$(checkboxes.days).on('click',function(){
		levelBatchMapping.daysCheckboxChecked($(this));
	});
	$(levelBatchMappingElements.editTable+" [type='checkbox']").on('click',function(){
		levelBatchMapping.daysCheckboxCheckedInEdit($(this));
	});
	$.each(days,function(ind,obj){
		$(levelBatchMappingElements.deleteBtn+obj).on('click',function(){
			levelBatchMapping.initDeleteDays($(this));
		})
	});
	$('.timepicker').datetimepicker({
		format:"hh:mm a" 
	});
	$('.endTime').on('dp.change',function(){
		var id=$(this).attr('id').replace('End','Start');
		var start = new Date("May 9, 2017 " + $('#'+id).val());
		var end=new Date("May 9, 2017 " + $(this).val());
		if(end<=start)
			{$(this).val('');}
	})
	$('.startTime').on('dp.change',function(){
		var id=$(this).attr('id').replace('Start','End');
		 $('#'+id).val('');
	})
	
	
});