var elements={
	tab				: '#tabLevelBatchGroupInstructor',
	instructors		: '#instuctorsByCenters',
	levels 			: '#levelsByCenter',
	batches			: '#batchesByCenterLevel',
	groups 			: '#groupsByLevel',
	save			: '#btnLevBatchGroupInstrSave',
	batchDiv		: '#batchDiv',
	groupDiv		: '#groupDiv',
	table			: '#tblLevelBatchGroupInstructor',
	deleteModal 	: '#mdlDeleteInstructorLevelBatchMapping',
	deleteConfirm 	: '#deleteInstructorLevelBatchMappingButton',
};
var panelElements={
		instructors :'#instuctorsByCenters [name="instructors"]',
		levels		:'#levelsByCenter [name="instructorLevels"]',
		batches		:'#batchesByCenterLevel [name="instructorBatches"]',
		groups		:'#groupsByLevel [name="instructorGroups"]',
};
var instructorId,levelId,batchId,groupId;
var batchIds=[];
var groupIds=[];
var centerId,instructorIdForDelete,groupIdForDelete,batchIdForDelete,levelIdForDelete;
var centerLevelBatchGroupInstructor={
		centerLevelBatchMapperId:'',
		init:function(){
			$(elements.tab).on('click',centerLevelBatchGroupInstructor.fnInit);
			$(elements.save).on('click',centerLevelBatchGroupInstructor.initSave);
			$(elements.deleteConfirm).on('click',centerLevelBatchGroupInstructor.fnDelete);
			$(elements.batchDiv).hide();
			$(elements.groupDiv).hide();
		},
		fnInit:function(){
			centerLevelBatchGroupInstructor.fnBindInstructors();
			centerLevelBatchGroupInstructor.fnBindLevels();
			centerLevelBatchGroupInstructor.fnBindTableData();
			centerLevelBatchGroupInstructor.batchReset();
		},
		fnBindInstructors:function(){
			$(elements.instructors).empty();
			centerId=$(levelBatchMappingElements.center).val();
			var	instructors = shiksha.invokeAjax("user/center/"+centerId+"?role=IST", null, "GET");
			spinner.hideSpinner();
			if(instructors != null){
				$.each(instructors, function(index,obj){
					var div = '<div id="instructorDiv'+obj.id+'" title="">'+
							  '<input type="radio" class="radioLabel" id="instructor'+obj.id+'" data-code="'+obj.sapCode+'" value="'+obj.id+'" name="instructors">'+
							  '<label class="radioLabel" for="instructor'+obj.id+'">'+obj.userName+'</label>'+
							  '</div>';
					$(elements.instructors).append(div);
				});
			}
		},
	fnBindLevels:function(){
		$(elements.levels).empty();
		var	levels = shiksha.invokeAjax("level/list", null, "GET");
		spinner.hideSpinner();
		if(levels != null){
			$.each(levels, function(index,obj){
				var div = '<div id="levelDiv'+obj.levelId+'" title="">'+
						  '<input type="radio" class="radioLabel" id="instructorLevel'+obj.levelId+'" data-code="'+obj.levelCode+'" value="'+obj.levelId+'" name="instructorLevels">'+
						  '<label class="radioLabel" for="instructorLevel'+obj.levelId+'">'+obj.levelName+'</label>'+
						  '</div>';
				$(elements.levels).append(div);
			})
		}
		$('input:radio[name=instructorLevels]').change(function() {
			levelId=$(elements.levels+' [name="instructorLevels"]:checked').val();
			$(elements.batchDiv).show();
			$(elements.groupDiv).show();
			centerLevelBatchGroupInstructor.fnBindGroups(levelId);
			centerLevelBatchGroupInstructor.fnBindBatches(levelId);
		})
	},
	fnBindGroups:function(levelId){
		$(elements.groups).empty();
		var	groups = shiksha.invokeAjax("group/level/"+levelId, null, "GET");
		spinner.hideSpinner();
		if(groups != null){
			$.each(groups, function(index,obj){
				var div = '<div id="groupDiv'+obj.groupId+'" title="">'+
						  '<input type="checkbox" class="radioLabel" id="instructorGroup'+obj.groupId+'" data-code="'+obj.groupCode+'" value="'+obj.groupId+'" name="instructorGroups">'+
						  '<label class="radioLabel" for="instructorGroup'+obj.groupId+'">'+obj.groupName+'</label>'+
						  '</div>';
				$(elements.groups).append(div);
			})
		}
	},
	fnBindBatches:function(levelId){
		centerId=$(levelBatchMappingElements.center).val();
		$(elements.batches).empty();
		var	batches = shiksha.invokeAjax("batch/center/"+centerId+"/level/"+levelId, null, "GET");
		spinner.hideSpinner();
		if(batches != null){
			$.each(batches, function(index,obj){
				var div = '<div id="batchDiv'+obj.batchId+'" title="">'+
						  '<input type="checkbox" class="radioLabel" id="instructorBatch'+obj.batchId+'" data-code="'+obj.batchCode+'" value="'+obj.batchId+'" name="instructorBatches">'+
						  '<label class="radioLabel" for="instructorBatch'+obj.batchId+'">'+obj.batchName+'</label>'+
						  '</div>';
				$(elements.batches).append(div);
			})
		}
	},
	batchReset:function(){
		if ($(panelElements.instructors).is(':checked')){
			var id=$('#instuctorsByCenters [name="instructors"]:checked').attr('id');
			$('#'+id).removeAttr('checked');
		}
		if ($(panelElements.levels).is(':checked')){
			var id=$('#levelsByCenter [name="instructorLevels"]:checked').attr('id');
			$('#'+id).removeAttr('checked');
		}
		$.each($("input[name='instructorBatches']:checked"), function(){  
			var id=$(this).attr('id');
			$('#'+id).removeAttr('checked');
		})
		$.each($("input[name='instructorGroups']:checked"), function(){  
			var id=$(this).attr('id');
			$('#'+id).removeAttr('checked');
		})
		$(elements.batchDiv).hide();
		$(elements.groupDiv).hide();
	},
	fnSave:function(){
		
		batchIds=[];
		groupIds=[];
		if ($(panelElements.instructors).is(':checked')){
			if ($(panelElements.levels).is(':checked')){
				if ($(panelElements.batches).is(':checked')){
					if ($(panelElements.groups).is(':checked')){
						instructorId =$(panelElements.instructors+':checked').val();
						levelId	=$(panelElements.levels+':checked').val();
						$.each($("input[name='instructorBatches']:checked"), function(){
							batchId=$(this).val();
							batchIds.push({'batchId':batchId});
						})
						$.each($("input[name='instructorGroups']:checked"), function(){
							groupId=$(this).val();
							groupIds.push({'groupId':groupId});
						})
						var data={
							center : {'id':centerId},
							instructor:{'userId':instructorId},
							level	:{'levelId' :levelId},
							'groups' :groupIds,
							'batches':batchIds
						};
						console.log(data);
						spinner.showSpinner();
						var ajaxCall =shiksha.invokeAjax("centerconfig/instructor/",data, "POST");
						spinner.hideSpinner();
						if(ajaxCall !=null){
							if(ajaxCall.response =='shiksha-200'){
								centerLevelBatchGroupInstructor.batchReset();
								centerLevelBatchGroupInstructor.fnBindTableData();
								
								$(elements.table).bootstrapTable("refresh");
								AJS.flag({
									type  : "success",
									title : appMessgaes.success,
									body  : ajaxCall.responseMessage,
									close : 'auto'
								})
							}else if(ajaxCall.response == "shiksha-800"){
								centerLevelBatchGroupInstructor.batchReset();
								centerLevelBatchGroupInstructor.fnBindTableData();
								
								$(elements.table).bootstrapTable("refresh");
								if(ajaxCall.responseMessages.length>0){
									var names="";
									$.each(ajaxCall.responseMessages, function(index,obj){
										var num=index+1;
										names=names+"<ul>"+num+". "+obj+"</ul>"
									});
									AJS.flag({
										type  : "warning",
										title : appMessgaes.warning,
										body  : names,
										close : 'auto'
									});
								}	
							}
							else{
								AJS.flag({
									type  : "Error",
									title : appMessgaes.error,
									body  : ajaxCall.responseMessage,
									close : 'auto'
								})
							}
						}
					}
					else{
						AJS.flag({
							type  : "error",
							title : appMessgaes.oops,
							body  : messages.group,
							close : 'auto'
						})
					}
				}
				else{
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : messages.batch,
						close : 'auto'
					})
				}
			}
			else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : messages.level,
					close : 'auto'
				})
			}
		}
		else{
			AJS.flag({
				type  : "error",
				title : appMessgaes.oops,
				body  : messages.instructor,
				close : 'auto'
			})
		}
	},
	fnBindTableData:function(){
		centerId=$(levelBatchMappingElements.center).val();
		$(elements.table).bootstrapTable({
			url : 'centerconfig/instructor/'+centerId,
			method : "get",
			toolbarAlign :"right",
			search: false,
			sidePagination: "client",
			showToggle: false,
			showColumns: false,
			pagination: true,
			searchAlign: 'left',
			pageSize: 20,
			clickToSelect: true,
		});
		$(elements.table).bootstrapTable("refresh",{url : 'centerconfig/instructor/'+centerId});
	},
	initSave :function(){
		var flag=1;
		if(flag){
			$.each($(elements.table+' tbody tr'),function(){
				if($(this).hasClass('selected')){
					centerLevelBatchGroupInstructor.fnUpdate();
				}
				else{
					flag=0;
				}
			})
		}
		if(flag==0)
		{
			centerLevelBatchGroupInstructor.fnSave();
			
		}
		
	},
	initEdit:function(mapperId){
		
	},
	initDelete:function(mapperId){
		$(elements.deleteModal).modal('show');
		centerLevelBatchGroupInstructor.centerLevelBatchMapperId=mapperId;
		
	},
	fnDelete:function(){
		var ajaxCall =shiksha.invokeAjax("centerconfig/instructor/"+centerLevelBatchGroupInstructor.centerLevelBatchMapperId,null, "DELETE");
		spinner.hideSpinner();
		if(ajaxCall !=null){
			if(ajaxCall.response == "shiksha-200"){
				$(elements.deleteModal).modal('hide');
				$(elements.table).bootstrapTable("refresh");
				AJS.flag({
					type  : "success",
					title : appMessgaes.success,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				})
			}
		}
	},
	showSelectedInstrucorMapping:function(row,$element){
		console.log(row,$element);
		centerLevelBatchGroupInstructor.batchReset();
		$.each($(panelElements.instructors),function(){
			if($(this).val() == $element.instructorId){
				$(this).prop('checked',true);
			}
		});
		$.each($(panelElements.levels),function(){
			if($(this).val() == $element.levelId){
				$(this).prop('checked',true);
			}
		});
		$.each($(panelElements.batches),function(){
			if($(this).val() == $element.instructorId){
				$(this).prop('checked',true);
			}
		});
		$.each($(panelElements.groups),function(){
			if($(this).val() == $element.instructorId){
				$(this).prop('checked',true);
			}
		});
	},
	actionFormatter:function(value,row,index){
		console.log("..........."+value,row,index);
		return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
		+ '<li><a onclick="centerLevelBatchGroupInstructor.initEdit('+row.centerLevelBatchGroupInstructorId+')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>'
		+ '<li><a onclick="centerLevelBatchGroupInstructor.initDelete('+row.centerLevelBatchGroupInstructorId+')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>'
		+ '</ul>';
	},
	batchNameActionFormatter:function(index,row,value){
		return row.batches[0].batchName;
	},
	levelNameActionFormatter:function(index,row,value){
		return row.levels[0].levelName;
	}
};
var actionFormater=function(){
	return {
		classes:"dropdown dropdown-td"
	}
};
$( document ).ready(function() {
	/*$(elements.table).on('check.bs.table', function (row,$element) {
		centerLevelBatchGroupInstructor.showSelectedInstrucorMapping(row,$element);
	});
	$(elements.table).on('uncheck.bs.table', function () {
		centerLevelBatchGroupInstructor.batchReset();
	});*/
	centerLevelBatchGroupInstructor.init();
	
});