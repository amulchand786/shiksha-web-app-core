var centerConfigurationPageContext = {	
	city 	:"#divSelectCity",
	block 	:"#divSelectBlock",
	NP 		:"#divSelectNP",
	GP 		:"#divSelectGP",
	RV 		:"#divSelectRV",
	village :"#divSelectVillage",
	school			 :"#divSelectSchool",
	elementSchool 	 :"#selectBox_centersByLoc",
	grade 			 :"#selectBox_grades",
	resetLocDivCity  :"#divResetLocCity",
	resetLocDivVillage :"#aSelectedSchooldivResetLocVillage",
};	
var locationElements ={
		filter : '#locColspFilter',
		panel  : '#locationPnlLink',
		tabPanel: '#tabPane',
};
var lObj;
var elementIdMap;
var reinitializeSmartFilter =function(eleMap){
		var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
			$(eleMap[ele_key]).find("option:selected").prop('selected', false);
			$(eleMap[ele_key]).find("option[value]").remove();
			$(eleMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(eleMap[ele_key]);
	});	
}

var displaySmartFilters =function(lCode,lLevelName){
		$(".outer-loader").show();
		elementIdMap = {
			"State" 	: '#statesForZone',
			"Zone" 		: '#selectBox_zonesByState',
			"District"  : '#selectBox_districtsByZone',
			"Tehsil" 	: '#selectBox_tehsilsByDistrict',
			"Block"  	: '#selectBox_blocksByTehsil',
			"City" 		: '#selectBox_cityByBlock',
			"Nyaya Panchayat" : '#selectBox_npByBlock',
			"Gram Panchayat"  : '#selectBox_gpByNp',
			"Revenue Village" : '#selectBox_revenueVillagebyGp',
			"Village" : '#selectBox_villagebyRv',
			"Center"  : '#selectBox_centersByLoc'
		};
		
		displayLocationsOfSurveys(lCode,lLevelName,$('#divFilter'));
		
		var ele_keys = Object.keys(elementIdMap);
		$(ele_keys).each(function(ind, ele_key){
			$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
			$(elementIdMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(elementIdMap[ele_key]);
		});	
		setTimeout(function(){
			$(".outer-loader").hide();
			$('#rowDiv1,#divFilter').show();
			}, 100);	
};
var positionFilterDiv =function(type){
		if(type=="R"){
			$(centerConfigurationPageContext.NP).insertAfter( $(centerConfigurationPageContext.block));
			$(centerConfigurationPageContext.RV).insertAfter( $(centerConfigurationPageContext.GP));
			$(centerConfigurationPageContext.school).insertAfter( $(centerConfigurationPageContext.village));
		}else{
			$(centerConfigurationPageContext.school).insertAfter( $(centerConfigurationPageContext.city));
		}
}

$('#divCenterLocType input:radio[name=locType]').change(function() {
		$(locationElements.filter).removeClass('in');
		$(locationElements.panel).text(centerColumn.selectLocation);
		$(locationElements.panel).append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
		$(locationElements.tabPanel).css('display','none');
		lObj =this;
		var typeCode =$(this).data('code');		
		$('.outer-loader').show();		
		reinitializeSmartFilter(elementIdMap);	
		$("#hrDiv").show();
		if(typeCode.toLowerCase() =='U'.toLowerCase()){
			showDiv($(centerConfigurationPageContext.city),$(centerConfigurationPageContext.resetLocDivCity));
			hideDiv($(centerConfigurationPageContext.block),$(centerConfigurationPageContext.NP),$(centerConfigurationPageContext.GP),$(centerConfigurationPageContext.RV),$(centerConfigurationPageContext.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("U");
		}else{
			hideDiv($(centerConfigurationPageContext.city),$(centerConfigurationPageContext.resetLocDivCity));
			showDiv($(centerConfigurationPageContext.block),$(centerConfigurationPageContext.NP),$(centerConfigurationPageContext.GP),$(centerConfigurationPageContext.RV),$(centerConfigurationPageContext.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("R");
		}
		
		fnCollapseMultiselect();
		
		$('.outer-loader').hide();
		return;
});

var resetLocAndSchool =function(){	
	$("#mdlResetLoc").modal().show();
	
}

var doResetLocation =function(){
	$(locationElements.filter).removeClass('in');
	$(locationElements.panel).text(centerColumn.selectLocation);
	$(locationElements.panel).append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	$('#tabLevelBatchGroupInstructor').parent().removeClass('active');
	$('#tabLevelBatch').parent().addClass('active');

	reinitializeSmartFilter(elementIdMap);
	var aLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
	var aLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
	displaySmartFilters(aLocTypeCode,aLLevelName);
	$(locationElements.tabPanel).hide();
	fnCollapseMultiselect();
	
}
	