var subElements = {};
var nextChildParentIds = [];
var parentIdMap;
var selectedIds = [], selectedType = "";
var iscontainsListBox =false; //user at the time of edit modal save for any bool type...

var locationInfoData = {};

var maxLevelType =  "";

var elementIdMap = {
	"State" : '#statesForZone',
	"Zone" : '#selectBox_zonesByState',
	"District" : '#selectBox_districtsByZone',
	"Tehsil" : '#selectBox_tehsilsByDistrict',
	"Block" : '#selectBox_blocksByTehsil',
	"City" : '#selectBox_cityByBlock',
	"Nyaya Panchayat" : '#selectBox_npByBlock',
	"Gram Panchayat" : '#selectBox_gpByNp',
	"Revenue Village" : '#selectBox_revenueVillagebyGp',
	"Village" : '#selectBox_villagebyRv',
	"Center" : '#selectBox_centersByLoc'
};


var getLocationByTypeAndId = function(id, type){
	var res_location;
	$(locationInfoData[type]).each(function(index, location){
		if(parseInt(location.id) == id){
			res_location = location;
			return false;
		}
	});
	return res_location;
};

var getLocationByName = function(name){
	return locationInfoData[name];
};

var getLocationByTypeAndParentId = function(id, type){
	var keys = Object.keys(locationInfoData);
	var locationMap = {};
	
	$(keys).each(function(keyIndex, key){
		$(locationInfoData[key]).each(function(index, location){
			if(parseInt(location.parentLocationid) == id && location.parentLocationType == type){
				var locType = location.locationType;
				if(locationMap[locType] == null || typeof locationMap[locType] == "undefined"){
					locationMap[locType] = [];
				}
				locationMap[locType].push(location);
			}
		});
	});
	return locationMap;
};

var getAllLocations = function(){	
	return locationInfoData;
};

var setMaxLevelType = function(levelType){	
	maxLevelType = levelType;
};

var getMaxLevelType = function(){
	return maxLevelType;
};


var unSelectAll = function(){
	var allLocations = getAllLocations();
	var keys = Object.keys(allLocations);
	$(keys).each(function(index, key){
		$(allLocations[key]).each(function(index, current){
			$(elementIdMap[key]).find("option[value="+current.id+"]").prop('selected', false);
		});
	});
};


var getLocationByParent = function(id, type){
	return getLocationByTypeAndParentId(id, type);
};

var disableChild =function(parent,child){		
	parentIdMap={};
	$(elementIdMap[parent]).find('option').each(function(){
		if(!$(this).is(":disabled")){				
			var parentVal =$(this).val();
			parentVal="P"+parentVal;
			parentIdMap[parentVal]=1;				
		};
	});

	//child
	$(elementIdMap[child]).find('option').each(function(index,ele){
		var parentIdKey="P"+$(ele).attr("data-parentid");
		if(parentIdMap[parentIdKey]!=1){	
			$(this).prop('disabled', true);
			$(this).addClass("disabled");
		}
	});		
}


var recursiveSelection = function(subLocations){
	var subLocationMap = null;
	$(subLocations).each(function(index, location){
		if(location.locationType == maxLevelType){
			return;
		}
		subLocationMap = getLocationByParent(parseInt(location.id), location.locationType);
		var keys = Object.keys(subLocationMap);
		var key = keys[0];
		if(keys.length!=0)
			disableChild(location.locationType,keys);		
		
		
		return recursiveSelection(subLocationMap[key]);
	});
};


var filterParentElements = function(loc_id, loc_type, callBack){
	var result;
	while(parseInt(loc_id) != 0){
		var location = getLocationByTypeAndId(parseInt(loc_id), loc_type);
		var element = elementIdMap[location.locationType];
		$($(element).children()).each(function(index, current){
			if(parseInt($(current).val()) == location.id){
				$(current).prop('selected', true);
				
				}else{									
					$(current).prop('disabled', true);					
					$(current).addClass("disabled");
				}
		});
		loc_id = location.parentLocationid;
		loc_type = location.parentLocationType;
		if(parseInt(loc_id) == 0){
			result = "Success";
			callBack(result);
		}
	}
};



var fnRomoveDisabledOptions =function(){
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key) {
		enableMultiSelect(elementIdMap[ele_key]);
		$(elementIdMap[ele_key]).find('.disabled').remove();
	});
}


var selectAllSubLocations = function(element, subLocs){
	$(element).multiselect('destroy');
	$(subLocs).each(function(index, current){
		$(element).find("option[value="+current.id+"]").prop('selected', true);			
		
	});
	enableMultiSelect(element);
};

var biDirectionalMapping = function(loc_id, loc_type){
	if(loc_id != "multiselect-all"){// multiselect-all is input field default value
		//of "Select-all" option provided by bootstrap multiselect
		var locations = [];
		var obj = {"id":loc_id, "locationType":loc_type}; 
		locations.push(obj);
		function callBack(result){
			if(result == "Success"){
				recursiveSelection(locations);		
			}
		}
		filterParentElements(loc_id, loc_type, callBack);		
		
	}else{
		$('.outer-loader').show();
		setTimeout(function(){
			var allLocations = getAllLocations();
			var keys = Object.keys(allLocations);
			$(keys).each(function(index, key){
				selectAllSubLocations(elementIdMap[key], allLocations[key]);
			});
			$('.outer-loader').hide();
			return;
		}, 50);
	}
};
var enableMultiSelect = function(arg){
	$(arg).multiselect({
		maxHeight: 180,
		//includeSelectAllOption: false,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',
		onChange: function() {
				unSelectAll();
				if(typeof selectedIds != "string"){
					$(selectedIds).each(function(index, id){
						biDirectionalMapping(id, selectedType);
					});
				}
				else{
						biDirectionalMapping(selectedIds,selectedType);
				}
				
				fnRomoveDisabledOptions();
				
				$('.outer-loader').show();
				setTimeout(function(){
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(function(ind, ele_key){
					$(elementIdMap[ele_key]).multiselect('destroy');
					enableMultiSelect(elementIdMap[ele_key]);
					$(elementIdMap[ele_key]).find("option").each(function() {						
													
					});
				});
				fnCollapseMultiselect();
				$('.outer-loader').hide();
			},100);
			if(arg=='#selectBox_centersByLoc'){
				
				$('#pnlMappingElements').addClass('in')
				$('#tabPnlConfigHeading').text('Hide school configuration');
				levelBatchMappingElements.fnShowLevelBatchMappingTable();
				levelBatchMappingElements.fnAddLevels();
				levelBatchMappingElements.fnAddBatches();
				$('#tabPane').css('display','block');
				if(!$('#daysTable [name="days"]').is(':checked')){
					$.each($("#daysTable [name='days']"),function(){
						$(this).parent().siblings().find('.startTime').prop('disabled',true);
						$(this).parent().siblings().find('.endTime').prop('disabled',true);
					})
				}
				$("#levelBatchGroupInstructorMapping").removeClass("active in");
				$("#levelBatchMapper").addClass("active in");
			}
		},	 	
	});	
};


var displayLocations = function(element, locations){
	$(element).parent().show();
	var aSortedData =fnSortSmartFilterByName(locations);	
	$(aSortedData).each(function(index, location){
		if(location.locationType=="School"){
			if(location.isMember){
				$('<option data-id="'+location.id+'" data-parentid="'+location.parentLocationid+'" data-childtype="'+location.childLocationType+'" data-childids="'+location.childLocationid+'">').val(location.id).text(location.locationName).appendTo(element);
			}
		}else{
			$('<option data-id="'+location.id+'" data-parentid="'+location.parentLocationid+'" data-childtype="'+location.childLocationType+'" data-childids="'+location.childLocationid+'">').val(location.id).text(location.locationName).appendTo(element);
		}
	});
	enableMultiSelect(element);	
};

var displayLocationsUptoLevel = function(levelname){
	var keys = Object.keys(locationInfoData);
	maxLevelType = levelname;
	$(keys).each(function(index, key){
		displayLocations(elementIdMap[key],locationInfoData[key]);
	});
}; 

var displayLocationsOfSurveys = function(alCode,alLevelName, divElement) {		
		$("#divFilter").attr("hidden", false);
		$($(divElement).children()).each(function(index, child){
			var selectContainer = $(child).find(".select");
			selectContainer.multiselect('destroy');
			selectContainer.html();
			selectContainer.empty();
			$(child).hide();
		});
		locationInfoData = sendGETRequest("location/"+alCode+"/center", "GET");
		
		if(locationInfoData != null && locationInfoData != ""){
			displayLocationsUptoLevel("Center");
		}else{
			$($(divElement).children()).each(function(index, emptyChild){
				var emptyContainer = $(emptyChild).find(".select");
				emptyContainer.multiselect('refresh');	
				$(emptyChild).show();
			});
		}
		$(".outer-loader").hide();
};




$(".select").change(function(){	
	selectedType = "";
	selectedIds = [];
	$(this).multiselect('refresh');
	if($(this).val() != null)
		selectedIds = $(this).val();
	selectedType = $(this).attr("data-locationName");
});



