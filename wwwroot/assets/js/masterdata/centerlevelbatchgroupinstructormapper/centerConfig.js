var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		grade	: "#divSelectGrade",
		section	: "#divSelectSection",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		eleGrade			: "#selectBox_grade",
		eleSection			: "#selectBox_Section",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
		divFilter   		: '#divFilter',
		eleLocType  		: '#divSchoolLocType input:radio[name=locType]',
		filter				: '#locColspFilter',
		panelLink			: '#locationPnlLink',
		form				: '#listDeviceForm',
		eleCollapseFilter	: '#collapseFilter',
		eleToggleCollapse	: '[data-toggle="collapse"]',
		resetModal 			: '#mdlResetLoc',
		resetConfirm 		: '#btnResetLocYes',
};
var days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
var checkboxes={
		days		: '#daysTable [name="days"]',
		levels		: '#levelMasters [name="levels"]',
		batches		: '#batchMasters [name="batches"]',
		startTime	: '.startTime',
		endTime		: '.endTime',
	};
var mapperId,batchDayId;
var mapperIdForDelete,mapperIdForDayDelete,deletedDayName;
var batchDays=[];
var batchId,day,startTime,endTime,data;
var batch=[];
var batchIds=[];
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var locationUtility={
		fnInitLocation :function(){
			$(locationPanel.divFilter).css('display','none');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(locationPanel.resetModal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			$(locationPanel.eleGrade).multiselect('destroy').multiselect('refresh');
			$(locationPanel.eleSection).multiselect('destroy').multiselect('refresh');
			$(locationPanel.section).hide();
			$(locationPanel.grade).hide();
			
			if ($(resetButtonObj).parents("form").attr("id") == 'locationFilterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode = $(mapFilter.locTypeSelect).val();
				var aLLevelName =$(mapFilter.locTypeSelect).find("option:selected").data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
			}$(locationPanel.school).val()
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){			
				elementIdMap = {
						"State" 	: '#statesForZone',
						"Zone" 		: '#selectBox_zonesByState',
						"District" 	: '#selectBox_districtsByZone',
						"Tehsil" 	: '#selectBox_tehsilsByDistrict',
						"Block" 	: '#selectBox_blocksByTehsil',
						"City" 		: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" 	: '#selectBox_villagebyRv',
				};
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));				
				locationUtility.reinitializeSmartFilter(elementIdMap);				
				var lLocTypeCode = $(mapFilter.locTypeSelect).val();
				var lLLevelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');				
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));				
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(
						function(ind, ele_key) {
							$(elementIdMap[ele_key]).find("option:selected").prop(
									'selected', false);
							$(elementIdMap[ele_key]).multiselect('destroy');
							enableMultiSelect(elementIdMap[ele_key]);
						});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#btnsRow,#divFilter').show();
				}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});			
			$('#rowDiv1,#divFilter').show();			
			spinner.hideSpinner();
		},
};

var mapFilter = {
		locTypeSelect : "#filterBox_locType",
		villageDiv : "#filter-villagestDiv",
		villageSelect : "#filterBox_village",
		citySelect : "#filterBox_city",
		cityDiv : "#filter-citytDiv",
		centerDiv : "#filter-centerDiv",
		centerSelect : "#filterBox_center",
		levelSelect : "#filterBox_level",
		levelDiv : "#filter-levelDiv",
		filterIcon : "#smart-filter",
		locFilteModal : "#mdlLocationFilter",
		applyBtn : "#locFilterApplyBtn",
		init : function(){
			this.enableMultiSelectForFilters(this.locTypeSelect);
			this.enableMultiSelectForFilters(this.villageSelect);
			this.enableMultiSelectForFilters(this.citySelect);
			this.enableMultiSelectForFilters(this.centerSelect);
			this.enableMultiSelectForFilters(this.levelSelect);
			
			$(this.locTypeSelect).on("change",this.locaTypeChange);
			$(this.filterIcon).on("click",this.smartFilterInit);
			
			$("#selectBox_cityByBlock").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$("#selectBox_villagebyRv").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$(this.villageSelect).on("change",mapperObj.showCenters);
			$(this.citySelect).on("change",mapperObj.showCenters);
			$(mapFilter.applyBtn).on("click",function(){
				if($(mapFilter.locTypeSelect).val() == "R"){
					$(mapFilter.villageSelect).val($("#selectBox_villagebyRv").val());
					$(mapFilter.villageSelect).multiselect("rebuild");
				} else {
					$(mapFilter.citySelect).val($("#selectBox_cityByBlock").val());
					$(mapFilter.citySelect).multiselect("rebuild");
				}
				$(mapFilter.locFilteModal).modal("hide");
				mapperObj.showCenters();
			});
		},
		smartFilterInit : function(){
			$(mapFilter.applyBtn).hide();
			mapFilter.setLocations();
			$(mapFilter.locFilteModal).modal("show");
		},
		locaTypeChange : function(){
			if($(this).val() == "R"){
				$(mapFilter.cityDiv).addClass("hide");
				$(mapFilter.villageDiv).removeClass("hide");
				$(mapFilter.villageSelect).val("");
				$(mapFilter.villageSelect).multiselect("rebuild");
			} else {
				$(mapFilter.cityDiv).removeClass("hide");
				$(mapFilter.villageDiv).addClass("hide");
				$(mapFilter.citySelect).val("");
				$(mapFilter.citySelect).multiselect("rebuild");
			}
			$(mapFilter.centerDiv).addClass("hide");
			$(mapFilter.levelDiv).addClass("hide");
			$(mapperObj.subjectListContainer).hide();
		},
		setLocations : function(){
			$(locationPanel.filter).removeClass('in');
			$(locationPanel.panelLink).text(columnMessages.selectLocation);
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
			var typeCode =$(this.locTypeSelect).val();	
			var locCode = $(mapFilter.locTypeSelect).val()
			var levelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');
			if(typeCode=='U'){
				showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("U");
				fnCollapseMultiselect();
			}else{
				hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("R");
				fnCollapseMultiselect();						
			}
			$(locationPanel.eleCollapseFilter).css('display','block');
			$(locationPanel.grade).hide();
			$(locationPanel.section).hide();
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
			});	
		}
		
};
var currentCenterId = 0;
var mapperObj = {
		centerListUL : "#centerListUL",
		centerLi : ".mapper-center",
		centerListContainer : "#centerlist-container",
		sectionMapContainer  :"#sectionMapContainer",
		sectionMapContainerMain :"#sectionMapContainerMain",
		mapSaveBtn : "#mapSaveBtn",
		levelBatchTabLi : "#tabLevelBatch",
		academicCycleId : "#academicCycleId",
		acMapDiv: ".level-batch-mapper-container",
		acMapTableDiv : ".level-batch-table-container",
		levelBatchSave : "#levelBatchSave",
		acMapTable : "#tblLevelBatch",
		editModal		: '#mdlEditBatchDays',
		viewDaysModal 	: '#mdlViewBatchDays',
		saveEditDetails	: '#editBatchDaysSaveButton',
		cancelEditDetails	: '#editBatchDaysCancelButton',
		cancelMdlButton: '#editBatchDaysMdlCancelButton',
		deleteModal 	: '#mdlDeleteLevelBatchMapping',
		deleteConfirm 	: '#deleteLevelBatchMappingButton',
		deleteDayModal	: '#mdlDeleteBatchDay',
		deleteDayConfirm: '#deleteBatchDayButton',
		viewTable		: "#daysTableInView tbody",
		editTable		: '#daysTableInEdit',
		deleteBtn		: '#btnDelete',
		centerLevelBatchMapperId:'',
		launchModal : "#mdlLaunchLevelBatchMapping",
		closeModal : "#mdlCloseLevelBatchMapping",
		launchConfirm : "#launchLevelBatchMappingButton",
		closeConfirm : "#closeLevelBatchMappingButton",
		launchMapperId 	: "#launchMapperId",
		closeMapperId 	: "#closeMapperId",
		init : function(){
			$(document).on("click",this.centerLi,this.showSelectedCenter);
			$(this.mapSaveBtn).on("click",this.fnSaveMappers);
			$(this.levelBatchTabLi).on("click",this.showLevelBatchMapping);
			mapFilter.enableMultiSelectForFilters(mapperObj.academicCycleId);
			$(document).on("change",".day-check",this.enableDisableDayTimes);
			$(this.levelBatchSave).on("click",this.fnLevelBatchSave);
			$(this.deleteDayConfirm).on('click',this.fnDeleteDay);
			$(this.saveEditDetails).on('click',this.fnUpdateBatchDays);
			$(this.cancelMdlButton).on('click',this.fnCancel);
			$(this.cancelEditDetails).on('click',this.fnCancel);
			$(this.deleteConfirm).on('click',this.fnDelete);
			$(this.launchConfirm).on('click',this.fnLaunchBatch);
			$(this.closeConfirm).on('click',this.fnCloseBatch);
			$(mapperObj.academicCycleId).on("change",this.acChange)
		},
		enableDisableDayTimes: function(){
			if($(this).is(":checked")){
				$(this).closest("tr").find(".timepicker").prop("disabled", false);
				$(this).closest("tr").find(".startTime").val('08:00 am').end().find(".endTime").val("09:00 am");
			} else {
				$(this).closest("tr").find(".timepicker").prop("disabled", true).val("");
			}
		},
		showSelectedCenter : function(){
			currentCenterId = $(this).attr("data-centerid");
			$(mapperObj.sectionMapContainer).empty();
			$(this).siblings().removeClass("active");
			$(this).addClass("active");
			var mainTemp = $(".map-template").clone();
			$(mainTemp).removeClass("map-template");
			$(mapperObj.sectionMapContainerMain).show();
			$(mapperObj.levelBatchTabLi).trigger("click");
		},
		acChange : function(){
			if($(this).find("option:selected").attr("data-iscurrent") == "true"){
				mapperObj.showLevelBatchMappingWithAC();
			} else {
				$(mapperObj.acMapDiv).empty();
				$(mapperObj.levelBatchSave).hide();
				$(mapperObj.acMapDiv).append("<div style='text-align:center;'><span>"+messages.cycleCompleted+"</span></div>");
				$("#tblLevelBatch").bootstrapTable("refresh",{url : 'centerconfig/center/'+currentCenterId+'/academicCycle/'+$(this).val() });
			}
			spinner.hideSpinner();
		},
		showLevelBatchMapping : function(){
			shikshaPlus.fnGetAcademicCyclesUptoCurrent(mapperObj.academicCycleId,[0]);
			//$(mapperObj.academicCycleId).find("option").removeAttr("selected");
			$(mapperObj.academicCycleId).multiselect("rebuild");
			mapperObj.showLevelBatchMappingWithAC();
			
		},
		showLevelBatchMappingWithAC : function(){
			var academicCycleId 	= $(mapperObj.academicCycleId).val();
			var centerDetails 	= shiksha.invokeAjax("centerconfig/academicCycle/"+academicCycleId+"/center/"+currentCenterId, null, "POST");
			$("#tblLevelBatch").bootstrapTable("refresh",{url : 'centerconfig/center/'+currentCenterId+'/academicCycle/'+academicCycleId });
			console.log(centerDetails);
			$(mapperObj.acMapDiv).empty();
			if(centerDetails.response != "shiksha-200" || !centerDetails.levels.length){
				$(mapperObj.levelBatchSave).hide();
				$(mapperObj.acMapDiv).append("<div style='text-align:center;'><span>"+messages.noUnMappedData+"</span></div>");
				return false;
			} else if(!configPermissions.add){
				$(mapperObj.levelBatchSave).hide();
				$(mapperObj.acMapDiv).append("<div style='text-align:center;'><span>"+messages.addPermissionDenied+"</span></div>");
				return false;
			}
			$(mapperObj.levelBatchSave).show();	
			/*var	responseData = shiksha.invokeAjax("batch/list", null, "GET");
			var batches = "";
			if(responseData != null){
				batches += '<option value="NONE" selected disabled hidden>None Selected</option>';
				$.each(responseData, function(index,obj){
					batches +='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'">'+obj.batchName+'</option>';
				})
			}*/
			
			if(centerDetails.response == "shiksha-200"){
				if(!centerDetails.levels){
					$(mapperObj.levelBatchSave).hide();
					$(mapperObj.acMapDiv).append("<div style='text-align:center;'><span>"+messages.noUnMappedData+"</span></div>");
					return false;
				}
				$.each(centerDetails.levels,function(index,level){
					if(!level.batches.length){
						return true;
					}
					var levelMapperDOM = $(".level-mapper-template").clone().removeClass("level-mapper-template").show().attr({"data-levelid":level.levelId, "data-levelname": level.levelName });
					var bactchesId = "levelBatches"+level.levelId;
					var levelDateId = "levelDate"+level.levelId;
					levelMapperDOM.find("#levelTitle").text(level.levelName);
					levelMapperDOM.find(".level-batch").attr("id",bactchesId);
					levelMapperDOM.find(".level-date").attr("id",levelDateId);
					
					var batches = "";
					batches += '<option value="NONE" selected disabled hidden>None Selected</option>';
					$.each(level.batches, function(index,obj){
						batches +='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'">'+obj.batchName+'</option>';
					})
					
					levelMapperDOM.find(".level-batch").append(batches);
					$(mapperObj.acMapDiv).append(levelMapperDOM);
					mapFilter.enableMultiSelectForFilters("#"+bactchesId)
					$("#"+levelDateId).datepicker({
						format: 'dd-M-yyyy',
						todayHighlight: true,
						autoclose: true,
						startDate : new Date($(mapperObj.academicCycleId).find("option:selected").attr("data-start-date")),
					    endDate:new Date($(mapperObj.academicCycleId).find("option:selected").attr("data-end-date"))
					});
					
					
					$("[data-levelid='"+level.levelId+"'] .timepicker").each(function(){
						$(this).attr("id",$(this).attr("id")+level.levelId)
					});
					$("[data-levelid='"+level.levelId+"'] .input-group-addon").each(function(){
						$(this).attr("for",$(this).attr("for")+level.levelId)
					});
					
				});
				$('.timepicker').datetimepicker({
					format:"hh:mm a" 
				});
			}
		},
		showCenters : function(){
			$(mapperObj.sectionMapContainerMain).hide();
			$(mapperObj.centerListUL).empty();
			var locType = "R";
			var locationId = $(mapFilter.locTypeSelect).val() == "R" ? $(mapFilter.villageSelect).val() : $(mapFilter.citySelect).val();
			var ajaxData = {
				locations : [locationId]	
			};
			if($(mapFilter.locTypeSelect).val() == "U"){
				locType = "U";
			}
			var getAjaxCall = shiksha.invokeAjax("center/"+locType, ajaxData, "POST");
			
			$.each(getAjaxCall,function(index,center){
				var centerTemp =' <li class="lms-teacher-school-item mapper-center" data-centerid="'+center.id+'"><div class="lms-side-center-far">'
								+'<div class="lms-teacher-school-text">'+center.name+'</div><div class="lms-side-center" style="display:none;"><a href="#" class="lms-close-icon">'
				 				+'<svg><use xlink:href="web-resources/images/teacher.svg#close" href="web-resources/images/teacher.svg#close"></use></svg></a>'
				 				+'<a href="#" class="lms-expand-icon"><svg><use xlink:href="web-resources/images/teacher.svg#expand" href="web-resources/images/teacher.svg#expand"></use></svg></a></div></div></li>';
				$(mapperObj.centerListUL).append(centerTemp);
			});
			$(mapperObj.centerListContainer).show();
			spinner.hideSpinner();
		},
		fnLevelBatchSave : function(){
			var ajaxData = [];
			var flag = true;
			var academicCycleId 	= $(mapperObj.academicCycleId).val();
			$(mapperObj.acMapDiv).find(".level-mapper").each(function(){
				var levelName = $(this).attr("data-levelname");
				var obj = {
						level : {
							levelId : $(this).attr("data-levelid")
						},
						center : {
							id : currentCenterId
						},
						academicCycle : {
							academicCycleId : academicCycleId
						},
						startDate   : $(this).find(".level-date").val(),
						batch : [
							{
								batchId 	: $(this).find(".level-batch").val(),
								batchDays 	: []	
							}
						]
				};
				if(!obj.batch[0].batchId){
					/*AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : levelName+" : "+messages.batch,
						close : 'auto'
					});
					flag = false;
					return false;*/
				} else{
					
						if(!obj.startDate){
						AJS.flag({
							type  : "error",
							title : appMessgaes.oops,
							body  : levelName+" : "+messages.startDate,
							close : 'auto'
						});
						flag = false;
						return false;
					} else {
						if($(this).find(".day-check:checked").length){
							$(this).find(".day-check:checked").each(function(){
								if(!$(this).closest("tr").find(".startTime").val() || !$(this).closest("tr").find(".endTime").val()){
									AJS.flag({
										type  : "error",
										title : appMessgaes.oops,
										body  : levelName+" : "+messages.timings,
										close : 'auto'
									});
									flag = false;
									return false;
								} else {
									obj.batch[0].batchDays.push({
										day 			: $(this).val(),
										startTime		: $(this).closest("tr").find(".startTime").val(),
										endTime		: $(this).closest("tr").find(".endTime").val(),
									});
								}
							});
							
						} else {
							AJS.flag({
								type  : "error",
								title : appMessgaes.oops,
								body  : levelName+" : "+messages.batchDays,
								close : 'auto'
							});
							flag = false;
							return false;
						}
						
					}
					if(!flag)
						return false;
					ajaxData.push(obj);
				}
				
			});	
			if(!flag)
				return false;
			if(!ajaxData.length){
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : messages.batch,
					close : 'auto'
				});
				return false;
			}
			console.log(ajaxData);
			var centerId = $(mapFilter.centerSelect).val();
			var levelId = $(mapFilter.levelSelect).val();
			
			var saveAjaxCall = shiksha.invokeAjax("centerconfig", ajaxData, "POST");
			
			if(saveAjaxCall != null){	
				if(saveAjaxCall[0].response == "shiksha-200"){
					mapperObj.showLevelBatchMappingWithAC();
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : saveAjaxCall[0].responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : saveAjaxCall[0].responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		initViewDays:function(centerLevelBatchMapperId){
			$(mapperObj.viewDaysModal).modal('show');
			var ajaxCall =shiksha.invokeAjax("centerconfig/days/"+centerLevelBatchMapperId,null, "GET");
			spinner.hideSpinner();
			if(ajaxCall.batchSectionDays !=null){
				$(mapperObj.viewTable).empty();
				$.each(ajaxCall.batchSectionDays,function(index,object){
					$(mapperObj.viewTable).append('<tr><td>'+centerConfigDays[object.day]+'</td><td>'+object.startTime+'</td><td>'+object.endTime+'</td></tr>');	
				});
			}
		},
		initEdit:function(centerLevelBatchMapperId){
			mapperObj.centerLevelBatchMapperId =centerLevelBatchMapperId;
			$.each(days,function(ind,obj){
				$('input[name="'+obj+'"').prop('checked',false).prop('disabled',false);
				$.each($(mapperObj.editTable+' [name="'+obj+'"]'),function(){
					$(this).parent().siblings().find(checkboxes.startTime).prop('disabled',true).val('');
					$(this).parent().siblings().find(checkboxes.endTime).prop('disabled',true).val('');
					$(this).parent().siblings().find('.tooltip').hide();
				})
			})
			mapperId=centerLevelBatchMapperId;
			$(mapperObj.editModal).modal('show');
			var ajaxCall =shiksha.invokeAjax("centerconfig/days/"+mapperObj.centerLevelBatchMapperId,null, "GET");
			$("#editStartDate").datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate : new Date($(mapperObj.academicCycleId).find("option:selected").attr("data-start-date")),
			    endDate:new Date($(mapperObj.academicCycleId).find("option:selected").attr("data-end-date")),
				
			});
			$("#editStartDate").datepicker("setDate",ajaxCall.startDate)
			spinner.hideSpinner();
			if(ajaxCall.batchSectionDays !=null){
				$.each(ajaxCall.batchSectionDays,function(index,object){
					$.each(days,function(ind,obj){
						if(object.day == obj){
							$('input[name="'+object.day+'"').prop('checked',true);
							$('input[name="'+object.day+'"').prop('disabled',true);
							$('#edit'+object.day+'StartTime').prop('disabled',false);
							$('#edit'+object.day+'StartTime').attr('batchDayId',object.id);
							$('#edit'+object.day+'EndTime').prop('disabled',false);
							$('#edit'+object.day+'StartTime').val(object.startTime);
							$('#edit'+object.day+'EndTime').val(object.endTime);
							$('#btnDelete'+object.day).attr('value',object.id);
							$('#btnDelete'+object.day).show();
						}
					})
				})
			}
			
		},
		fnUpdateBatchDays:function(){
			var ajaxData={
					startDate : $("#editStartDate").val(),
					batchDays : []
			};
			var flag=true;
			var daysSelected=false;
			if(!$("#editStartDate").val()){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : messages.startDate,
					close : 'auto'
				})
				return false;
			}
			$.each($("#daysTableInEdit input[type='checkbox']:checked"), function(){    
				daysSelected=true;
				day=$(this).val();
	            startTime =$(this).parent().siblings().find(checkboxes.startTime).val();
	            endTime =$(this).parent().siblings().find(checkboxes.endTime).val();
	            batchDayId=$(this).parent().siblings().find(checkboxes.startTime).attr('batchDayId');
	            if(startTime == '' || endTime ==''){
	            	flag=false;
	            }
	            
	            var daysObjectInEdit={
	            		'id' :batchDayId,
	            		'day':day,
	            		'startTime':startTime,
	            		'endTime':endTime
	            };
	            ajaxData.batchDays.push(daysObjectInEdit);
	        });
	        if(daysSelected){
	        	if(flag){
	            	var ajaxCall =shiksha.invokeAjax("centerconfig/"+mapperObj.centerLevelBatchMapperId,ajaxData, "PUT");
	        		spinner.hideSpinner();
	        		if(ajaxCall !=null){
	        			if(ajaxCall.response =="shiksha-200"){
	        			AJS.flag({
	        				type  : "success",
	        				title : messages.sucessAlert,
	        				body  : ajaxCall.responseMessage,
	        				close : 'auto'
	        			})
	        			$(mapperObj.editModal).modal('hide');
	        			setTimeout(function(){
	        				mapperObj.showLevelBatchMappingWithAC();
	        				spinner.hideSpinner();
	        			}, 500);
	        			}
	        			else{
	        				AJS.flag({
	        					type  : "error",
	        					title : appMessgaes.error,
	        					body  : ajaxCall.responseMessage,
	        					close : 'auto'
	        				})
	        			}
	        		}
	            }
	            else{
	            	AJS.flag({
	    				type  : "error",
	    				title : appMessgaes.error,
	    				body  : messages.timings,
	    				close : 'auto'
	    			})
	            }
	        }
	        else{
	        	AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : messages.batchDays,
					close : 'auto'
				})
	        }
	        
			
		},
		fnCancel:function(){
			$(mapperObj.editModal).modal('hide');
			/*var daysSelected=false;
			$.each($("#daysTableInEdit input[type='checkbox']:disabled"), function(){    
				daysSelected=true;

			});
			if(daysSelected){
				$(mapperObj.editModal).modal('hide');
			} else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : messages.batchDays,
					close : 'auto'
				})
			}*/

		},
		initDelete:function(centerLevelBatchMapperId){
			$(mapperObj.deleteModal).modal('show');
			mapperIdForDelete =centerLevelBatchMapperId;
			
		},
		fnDelete:function(){
			var ajaxCall =shiksha.invokeAjax("centerconfig/"+mapperIdForDelete,null, "DELETE");
			spinner.hideSpinner();
			existedLevels=[];
			if(ajaxCall !=null){
				if(ajaxCall.response == "shiksha-200"){
					$(mapperObj.deleteModal).modal('hide');
					mapperObj.showLevelBatchMappingWithAC();
					AJS.flag({
						type  : "success",
						title : messages.sucessAlert,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}else{
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
		},
		initDeleteDays:function(element){
			if($("#daysTableInEdit .radioLabel:disabled:checked").length == 1){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : messages.batchDayRequired,
					close : 'auto'
				})
				return false;
			}
			mapperIdForDayDelete=element.attr('value');
			deletedDayName=element.attr('dayName');
			$(mapperObj.deleteDayModal).modal('show');
		},
		fnDeleteDay:function(){
			var ajaxCall =shiksha.invokeAjax("centerconfig/day/"+mapperIdForDayDelete,null, "DELETE");
			spinner.hideSpinner();
			if(ajaxCall !=null){
				if(ajaxCall.response == "shiksha-200"){
					$(mapperObj.deleteDayModal).modal('hide');
					$.each(days,function(ind,obj){
						if(deletedDayName == obj){
							$('input[name="'+deletedDayName+'"').prop('checked',false);
							$('input[name="'+deletedDayName+'"').prop('disabled',false);
							$('#edit'+deletedDayName+'StartTime').prop('disabled',true);
							$('#edit'+deletedDayName+'EndTime').prop('disabled',true);
							$('#edit'+deletedDayName+'StartTime').val('');
							$('#edit'+deletedDayName+'EndTime').val('');
							$('#edit'+deletedDayName+'StartTime').removeAttr('batchDayId');
							$('#btnDelete'+deletedDayName).hide();
						}
					})
					AJS.flag({
						type  : "success",
						title : messages.sucessAlert,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}else{
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
		},
		daysCheckboxCheckedInEdit:function(element){
			if(element.is(':checked')){
				element.parent().siblings().find(checkboxes.startTime).prop('disabled',false);
				element.parent().siblings().find(checkboxes.endTime).prop('disabled',false);
				element.parent().siblings().find(checkboxes.startTime).val('08:00 am');
				element.parent().siblings().find(checkboxes.endTime).val('09:00 am');
			}
			else{
				element.parent().siblings().find(checkboxes.startTime).prop('disabled',true);
				element.parent().siblings().find(checkboxes.endTime).prop('disabled',true);
				element.parent().siblings().find(checkboxes.startTime).val('');
				element.parent().siblings().find(checkboxes.endTime).val('');
			}
		},
		centerLevelBatchActionFormater : function(value,row){
			if($(mapperObj.academicCycleId).find("option:selected").attr("data-iscurrent") != "true"){
				return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
				+ '<li><a onclick="mapperObj.initViewDays('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)">'+messages.viewDays+'</a></li>'
				+ '</ul>';
			}			
			var str = (configPermissions.edit)? '<li><a onclick="mapperObj.initLaunch('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)">'+appMessgaes.launch+'</a></li>' : "";
					str += (configPermissions.edit)? '<li><a onclick="mapperObj.initEdit('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>' : "";
					str += (configPermissions.del)? '<li><a onclick="mapperObj.initDelete('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>': "";
			if(row.status){
				if(row.status.statusName == "In progress" && configPermissions.edit){
					str = '<li><a onclick="mapperObj.initClose('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)">'+appMessgaes.close+'</a></li>';
				} else if(row.status.statusName == "Completed"){
					str = "";
				}
			}
			
			return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
					+ '<li><a onclick="mapperObj.initViewDays('+row.centerLevelBatchAcademicCycleMapperId+')" href="javascript:void(0)">'+messages.viewDays+'</a></li>'
					+ str
					+ '</ul>';
		},
		batchNameActionFormatter:function(index,row){
			return row.batch[0].batchName;
		},
		statusFormater : function(index,row){
			var  status="<span class='label label-default text-uppercase'> Pending </span>";
			if(row.status)
				switch(row.status.statusName) {
			    case "In progress":
			    	status="<span class='label label-warning text-uppercase'> In progress </span>";
			        break;
			     case "Completed":
			    	status="<span class='label label-success text-uppercase'> Completed </span>";
			        break;
			    default: 
			    	
			 }
			  
			return status;
		},
		initLaunch : function(mapperId){
			$(mapperObj.launchMapperId).val(mapperId);
			$(mapperObj.launchModal).modal("show");
		},
		fnLaunchBatch : function(){
			spinner.showSpinner();
			var mapperId = $(mapperObj.launchMapperId).val();
			var ajaxCall =shiksha.invokeAjax("centerconfig/launch/"+mapperId,null, "POST");
			if(ajaxCall){	
				if(ajaxCall.response == "shiksha-200"){
					mapperObj.showLevelBatchMappingWithAC();
					$(mapperObj.launchModal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : ajaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : ajaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		initClose : function(mapperId){
			$(mapperObj.closeMapperId).val(mapperId);
			$(mapperObj.closeModal).modal("show");
		},
		fnCloseBatch : function(){
			spinner.showSpinner();
			var mapperId = $(mapperObj.closeMapperId).val();
			var ajaxCall =shiksha.invokeAjax("centerconfig/close/"+mapperId,null, "POST");
			if(ajaxCall){	
				if(ajaxCall.response == "shiksha-200"){
					mapperObj.showLevelBatchMappingWithAC();
					$(mapperObj.closeModal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : ajaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : ajaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
			
		}
		
};

var instMapper = {
		academicCycleId : "#instAcademicCycleId",
		acMapDiv : ".inst-level-batch-mapper-container",
		instLevelBatchSave : "#instLevelBatchSave",
		instructorMappingTabLi : "#tabLevelBatchGroupInstructor",
		table : "#tblLevelBatchGroupInstructor",
		deleteModal : "#mdlDeleteInstructorLevelBatchMapping",
		deleteConfirmBtn : "#deleteInstructorLevelBatchMappingButton",
		centerLevelBatchMapperId : "",
		instructorId:"",
		init : function(){
			$(this.instructorMappingTabLi).on("click",this.showInstructorMapping);
			mapFilter.enableMultiSelectForFilters(instMapper.academicCycleId);
			$(this.instLevelBatchSave).on("click",this.fnSaveInstructorMapping);
			$(document).on("change",".inst-level",this.levelChange);
			$(instMapper.academicCycleId).on("change",this.acChange);
			$(this.deleteConfirmBtn).on("click",this.fnDelete);
		},
		showInstructorMapping: function(){
			shikshaPlus.fnGetAcademicCyclesUptoCurrent(instMapper.academicCycleId,[0]);
			//$(mapperObj.academicCycleId).find("option").removeAttr("selected");
			$(instMapper.academicCycleId).multiselect("rebuild");
			instMapper.showInstructorMappingWithAC();
		},
		acChange : function(){
			if($(this).find("option:selected").attr("data-iscurrent") == "true"){
				instMapper.showInstructorMappingWithAC();
			} else {
				$(instMapper.acMapDiv).empty();
				$(instMapper.instLevelBatchSave).hide();
				$(instMapper.acMapDiv).append("<div style='text-align:center;'><span>"+messages.cycleCompleted+"</span></div>");
				$(instMapper.table).bootstrapTable("refresh",{url : 'centerconfig/instructor/academicCycle/'+$(this).val()+'/center/'+currentCenterId});
			}
			
		},
		levelChange : function(){
			var acId = $(instMapper.academicCycleId).val();
			var levelId = $(this).val();
			var batchList = $(this).closest(".inst-level-mapper").find("#batchesByCenterLevel").empty();
			var groupList = $(this).closest(".inst-level-mapper").find("#groupsByLevel").empty();
			$(this).closest(".inst-level-mapper").find("#batchGroupContainer").show();
			var instId=$(this).closest(".inst-level-mapper").data("inst-id");
			var	batches = shiksha.invokeAjax("batch/center/"+currentCenterId+"/level/"+levelId+"/academicCycle/"+acId, null, "GET");
			if(batches){
				$.each(batches, function(index,obj){
					var div = '<div id="batchDiv'+obj.batchId+'" title="">'+
							  '<input type="checkbox" class="radioLabel batch-radio" id="instructorBatch'+obj.batchId+instId+'" data-code="'+obj.batchCode+'" value="'+obj.batchId+'" name="instructorBatches">'+
							  '<label class="radioLabel" for="instructorBatch'+obj.batchId+instId+'">'+obj.batchName+'</label>'+
							  '</div>';
					batchList.append(div);
				})
			}
			
			var	groups = shiksha.invokeAjax("group/level/"+levelId, null, "GET");
			
			if(groups){
				$.each(groups, function(index,obj){
					var div = '<div id="groupDiv'+obj.groupId+'" title="">'+
							  '<input type="checkbox" class="radioLabel group-radio" id="instructorGroup'+obj.groupId+instId+'" data-code="'+obj.groupCode+'" value="'+obj.groupId+'" name="instructorGroups">'+
							  '<label class="radioLabel" for="instructorGroup'+obj.groupId+instId+'">'+obj.groupName+'</label>'+
							  '</div>';
					$(groupList).append(div);
				})
			}
			
			spinner.hideSpinner();
		} ,
		showInstructorMappingWithAC : function(){
			var acId = $(instMapper.academicCycleId).val();
			
			var	instructors = shiksha.invokeAjax("/centerconfig/instructor/center/"+currentCenterId+"/academicCycle/"+acId, null, "GET");

			$(instMapper.acMapDiv).empty();
			$(instMapper.table).bootstrapTable("refresh",{url : 'centerconfig/instructor/academicCycle/'+acId+'/center/'+currentCenterId});
			if(!instructors.length){
				$(instMapper.acMapDiv).append("<div style='text-align:center;'><span>"+messages.noInstructorMapped+"</span></div>");
				$(instMapper.instLevelBatchSave).hide();
				return false;
			} else if(!configPermissions.instAdd){
				$(instMapper.instLevelBatchSave).hide();
				$(instMapper.acMapDiv).append("<div style='text-align:center;'><span>"+messages.noUnMappedData+"</span></div>");
				return false;
			}
			$(instMapper.instLevelBatchSave).show();
			$.each(instructors, function(index,obj){
				
				var levelsStr = "";
				if(obj.levels.length){
					levelsStr += '<option value="NONE" selected disabled hidden>None Selected</option>';
					$.each(obj.levels, function(index,levelObj){
						levelsStr +='<option value="'+levelObj.levelId+'" data-id="'+levelObj.levelId+'"data-name="'+levelObj.levelName+'">'+levelObj.levelName+'</option>';
					})
				} else {
					return true;
				}
				
				var mapDom = $(".inst-level-mapper-template").clone().removeClass("inst-level-mapper-template").show().attr({"data-inst-id": obj.instructor.userId, "data-inst-name" : obj.instructor.name, "data-code"  : obj.sapCod});
				var levelSelectId = "instMaplevels"+obj.instructor.userId;
				mapDom.find("#instTitle").text(obj.instructor.name);
				mapDom.find(".inst-level").append(levelsStr);
				mapDom.find(".inst-level").attr("id",levelSelectId);
												
				$(instMapper.acMapDiv).append(mapDom);
				mapFilter.enableMultiSelectForFilters("#"+levelSelectId);
								
			});
			spinner.hideSpinner();
			
		},
		fnSaveInstructorMapping : function(){
			var ajaxData = [];
			var centerId = currentCenterId;
			var academicCycleId = $(instMapper.academicCycleId).val();
			var flag = true;
			$(instMapper.acMapDiv).find(".inst-level-mapper").each(function(){
				var instName = $(this).attr("data-inst-name");
				var instId =  $(this).attr("data-inst-id");
				var levelSelect = "#instMaplevels"+instId;
				var levelId = $(this).find(levelSelect).val();
				var obj={
						center : {'id':centerId},
						instructor:{'userId':$(this).attr("data-inst-id")},
						level	:{'levelId' : levelId},
						academicCycle : {
							academicCycleId : academicCycleId
						},
						'groups' : [],
						'batches':[]
					};
				if(levelId){
					/*AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : instName+" :  "+messageslevel,
						close : 'auto'
					});
					flag = false;
					return false;*/
				
				if($(this).find(".batch-radio:checked").length){
					$(this).find(".batch-radio:checked").each(function(){
						obj.batches.push({ batchId : $(this).val()})
					});
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : instName+" : "+messages.batch,
						close : 'auto'
					});
					flag = false;
					return false;
				}
				
				if($(this).find(".group-radio:checked").length){
					$(this).find(".group-radio:checked").each(function(){
						obj.groups.push({ groupId : $(this).val() })
					});
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : instName+" : "+messages.group,
						close : 'auto'
					});
					flag = false;
					return false;
				}
				
				ajaxData.push(obj);	
				}
			});
			console.log(ajaxData);
			if(!flag)
				return false;
			if(!ajaxData.length){
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : messages.instructorRequired,
					close : 'auto'
				});
				return false;
			}
			spinner.showSpinner();
			var ajaxCall =shiksha.invokeAjax("centerconfig/instructor/",ajaxData, "POST");
			spinner.hideSpinner();
			if(ajaxCall !=null){
				if(ajaxCall.response =='shiksha-200'){
					instMapper.showInstructorMappingWithAC();
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}else if(ajaxCall.response == "shiksha-800"){
					instMapper.showInstructorMappingWithAC();
					if(ajaxCall.responseMessages.length>0){
						var names="";
						$.each(ajaxCall.responseMessages, function(index,obj){
							var num=index+1;
							names=names+"<ul>"+num+". "+obj+"</ul>"
						});
						AJS.flag({
							type  : "warning",
							title : appMessgaes.warning,
							body  : names,
							close : 'auto'
						});
					}	
				}
				else{
					AJS.flag({
						type  : "Error",
						title : appMessgaes.error,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}
			}
			
		},
		initDelete:function(instructorId,mapperId){
			$(instMapper.deleteModal).modal('show');
			instMapper.centerLevelBatchMapperId=mapperId;
			instMapper.instructorId=instructorId;
		},
		fnDelete:function(){
			var ajaxCall =shiksha.invokeAjax("centerconfig/instructor/"+instMapper.instructorId+"/centerLevelBatchAcademicCycleMapper/"+instMapper.centerLevelBatchMapperId,null, "DELETE");
			spinner.hideSpinner();
			if(ajaxCall !=null){
				if(ajaxCall.response == "shiksha-200"){
					$(instMapper.deleteModal).modal('hide');
					instMapper.showInstructorMappingWithAC();
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}else if(ajaxCall.response == "shiksha-800"){
					$(instMapper.deleteModal).modal('hide');
					instMapper.showInstructorMappingWithAC();
					AJS.flag({
						type: 'warning',
					    title : appMessgaes.warning,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					})
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
		},
		actionFormatter:function(value,row,index){
			if($(instMapper.academicCycleId).find("option:selected").attr("data-iscurrent") != "true" || !configPermissions.instDelete){
				return "-";
			}
			console.log("..........."+value,row,index);
			return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
			//+ '<li><a onclick="instMapper.initEdit('+row.centerLevelBatchGroupInstructorId+')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>'
			+ '<li><a onclick="instMapper.initDelete('+row.instructor.userId+','+row.centerLevelBatchAcademicCycleId+')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>'
			+ '</ul>';
		}
};
var actionFormater=function(){
	return {
		classes:"dropdown dropdown-td"
	}
};
$(document).ready(function(){
	mapFilter.init();
	mapperObj.init();
	instMapper.init();
	$("#tblLevelBatch").bootstrapTable({
		//url : 'centerconfig/center/'+centerId,
		method : "post",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: true,
	});
	$(instMapper.table).bootstrapTable({
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: true,
	});
	
	$.each(days,function(ind,obj){
		$(mapperObj.deleteBtn+obj).on('click',function(){
			mapperObj.initDeleteDays($(this));
		})
	});
	/*$("#editStartDate").datepicker({
		format: 'dd-M-yyyy',
		todayHighlight: true,
		autoclose: true,
		
	});*/
	$(mapperObj.editTable+" [type='checkbox']").on('change',function(){
		mapperObj.daysCheckboxCheckedInEdit($(this));
	});
	$(document).on("change",".select-all-days",function(){
		if($(this).is(":checked")){
			$(this).closest("table").find("tbody .day-check").prop("checked",true).trigger("change");
		} else {
			$(this).closest("table").find("tbody .day-check").prop("checked",false).trigger("change");
		}
	});
	$(document).on("change",".day-check",function(){
		if($(this).closest("tbody").find(".day-check:not(:checked)").length){
			$(this).closest("table").find(".select-all-days").prop("checked",false);
		} else {
			$(this).closest("table").find(".select-all-days").prop("checked",true);
		}
	});
});