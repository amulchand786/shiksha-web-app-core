var $table;
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		grade	: "#divSelectGrade",
		section	: "#divSelectSection",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		eleGrade			: "#selectBox_grade",
		eleSection			: "#selectBox_Section",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
		divFilter   		: '#divFilter',
		eleLocType  		: '#divSchoolLocType input:radio[name=locType]',
		filter				: '#locColspFilter',
		panelLink			: '#locationPnlLink',
		form				: '#listDeviceForm',
		eleCollapseFilter	: '#collapseFilter',
		eleToggleCollapse	: '[data-toggle="collapse"]',
		resetModal 			: '#mdlResetLoc',
		resetConfirm 		: '#btnResetLocYes',
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var locationUtility={
		fnInitLocation :function(){
			$(locationPanel.divFilter).css('display','none');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(locationPanel.resetModal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},

		doResetLocation : function() {
			$(locationPanel.eleGrade).multiselect('destroy').multiselect('refresh');
			$(locationPanel.eleSection).multiselect('destroy').multiselect('refresh');
			$(locationPanel.section).hide();
			$(locationPanel.grade).hide();

			enableMultiSelect($("#selectBox_Section"));
			enableMultiSelect($("#selectBox_Grade"));
			if ($(resetButtonObj).parents("form").attr("id") == 'mapDeviceForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divSchoolLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divSchoolLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
			}$(locationPanel.school).val()
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){			
			elementIdMap = {
					"State" 	: '#statesForZone',
					"Zone" 		: '#selectBox_zonesByState',
					"District" 	: '#selectBox_districtsByZone',
					"Tehsil" 	: '#selectBox_tehsilsByDistrict',
					"Block" 	: '#selectBox_blocksByTehsil',
					"City" 		: '#selectBox_cityByBlock',
					"Nyaya Panchayat" 	: '#selectBox_npByBlock',
					"Gram Panchayat" 	: '#selectBox_gpByNp',
					"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
					"Village" 	: '#selectBox_villagebyRv',
					"School" 	: '#selectBox_schoolsByLoc'
			};
			displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));				
			locationUtility.reinitializeSmartFilter(elementIdMap);				
			var lLocTypeCode =$('#divSchoolLocType input:radio[name=locType]:checked').data('code');
			var lLLevelName =$('#divSchoolLocType input:radio[name=locType]:checked').data('levelname');				
			displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));				
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(
					function(ind, ele_key) {
						$(elementIdMap[ele_key]).find("option:selected").prop(
								'selected', false);
						$(elementIdMap[ele_key]).multiselect('destroy');
						enableMultiSelect(elementIdMap[ele_key]);
					});
			setTimeout(function() {
				$(".outer-loader").hide();
				$('#rowDiv1,#btnsRow,#divFilter').show();
			}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});			
			$('#rowDiv1,#divFilter').show();			
			spinner.hideSpinner();
		},
};
var device = {	

		table : "#listDevices",
		tableContainer : "#tableContainer",

		init:function(){
			$(locationPanel.resetConfirm).on('click',locationUtility.doResetLocation);
			$(locationPanel.elementSchool).on('change',this.showGrades);
			$(locationPanel.eleGrade).on('change',this.showSections);
			$(locationPanel.eleLocType).change(function() {				
				$(locationPanel.filter).removeClass('in');
				$(locationPanel.panelLink).text('Select Location');
				$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
				$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
				var typeCode =$(this).data('code');				
				if(typeCode.toLowerCase() =='U'.toLowerCase()){
					showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("U");
					fnCollapseMultiselect();
				}else{
					hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("R");
					fnCollapseMultiselect();						
				}
				$(locationPanel.eleCollapseFilter).css('display','block');
				$(locationPanel.grade).hide();
				$(locationPanel.section).hide();					
			});
		},
		
		bindGrades : function(bindToElementId,selectedSchoolId,selectedId){
			var grades = customAjaxCalling("grade/list/school/"+ selectedSchoolId+"/device/"+mapDevice.deviceId,null,"GET");
			$select	=bindToElementId;
			$select.html('');
			if(grades!=null || grades!=""){
				$.each(grades,function(index,obj){
					$select.append('<option value="' + obj.gradeId + '" data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '">' +obj.gradeName+ '</option>');
				});
			}
		},
		showGrades : function(){
			device.bindGrades($(locationPanel.eleGrade),$(locationPanel.elementSchool).val());
			setOptionsForMultipleSelect(locationPanel.eleGrade);
			$(locationPanel.eleGrade).multiselect("rebuild");
			$(locationPanel.grade).show();

		},
		showSections : function(){
			device.bindSourceBySchoolAndGradeToListBox($(locationPanel.eleSection),$(locationPanel.elementSchool).val(),$(locationPanel.eleGrade).val());
			setOptionsForMultipleSelect(locationPanel.eleSection);
			$(locationPanel.eleSection).multiselect("rebuild");
			$(locationPanel.section).show();

		},
		bindSourceBySchoolAndGradeToListBox : function(bindToElementId,selectedSchoolId,selectedGradeId){	
			$select	=bindToElementId;
			$select.html('');
			
			var sections = customAjaxCalling("section/school/"+selectedSchoolId+"/grade/"+selectedGradeId+"/device/"+mapDevice.deviceId, null,"GET");
			if(sections!=null || sections!=""){
				$.each(sections,function(index,obj){
					//if(!$("#tblMappedSections tbody tr[data-gradeid='"+selectedGradeId+"'][data-sectionid='"+obj.sectionId+"']").length)
						$select.append('<option value="' + obj.sectionId + '" data-id="' + obj.sectionId + '"data-gradename="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
				});
			}
			
		},
		filterDevicesData : function(){
			$(device.tableContainer).show();
			$(device.table).bootstrapTable("refresh");
		},
		fiterQueryParmams : function(params){
			var schoolId = $(locationPanel.elementSchool).val() ? $(locationPanel.elementSchool).val() : 0;
			var gradeId  = $(locationPanel.eleGrade).val() ? $(locationPanel.eleGrade).val() : 0;
			var sectionId = $(locationPanel.eleSection).find("option:selected").data("id") ? $(locationPanel.eleSection).find("option:selected").data("id") : 0;
			var paramsTemp = {};
			paramsTemp.school = schoolId;
			paramsTemp.grade  = gradeId;
			paramsTemp.section = sectionId;
			return paramsTemp;
		},

		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},


		formValidate : function(validateFormName){
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				rules: {
					deviceType : {
						required : true
					},
					macId : {
						required : true,
						macid	 : true,
					},

					imeiId : {
						required : true
					},


					startDate:{
						required : true
					},
					endDate:{
						required : true
					},
					make : {
						required : true
					},
					model : {
						required : true
					},
					os : {
						required : true
					},
					serialNumber : {
						required : true
					},
					assetNumber : {
						required : true
					},
					desktopAppVersion : {
						required : true
					},
					tabletContentAppVersion : {
						required : true
					},
					tabletAssessmentAppVersion : {
						required : true
					}

				},
				messages: {
					deviceType : {
						required : "Please select device type"
					},
					macId : {
						required : "Please enter MAC Address",
						macid	 : "Please enter valid MAC Address(Format xx-xx-xx-xx-xx-xx or xx:xx:xx:xx:xx:xx and x must be (a-f)(A-F)(0-9))"
					},
					imeiId : {
						required : "Please enter IMEI Number"
					},


					startDate:{
						required : "Please select start date"
					},
					endDate:{
						required : "Please select end date"
					},
					make : {
						required : "Please enter Made by"
					},
					model : {
						required : "Please enter model"
					},
					os : {
						required : "Please enter OS"
					},
					serialNumber : {
						required : "Please enter Serial number"
					},
					assetNumber : {
						required : "Please enter Asset number"
					},
					desktopAppVersion : {
						required : "Please enter Desktop version"
					},
					tabletContentAppVersion : {
						required : "Please enter Content app version"
					},
					tabletAssessmentAppVersion : {
						required : "Please enter Assessment version"
					}
				},
				errorPlacement: function(error, element) {
					if($(element).hasClass("select")){
						$(element).parent().append(error);
					}else  if($(element).hasClass("datepicker")){
						$(element).parent().parent().append(error);
					} else {
						$(element).after(error);
					}
				},
				submitHandler : function(){
					if(validateFormName === addDevice.addForm){
						addDevice.fnAdd(submitActor.id);
					}
					if(validateFormName === editDevice.editForm){
						editDevice.fnEdit();
					}
				}
			});
		},
		actionFormater : function(value, row, index){


			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"]){
					action+='<li><a class="edit-device-btn" data-deviceid="'+row.deviceId+'" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
					action+='<li><a class="map-device-btn"  data-islocmapped="'+row.isLocationMapped+'" data-locmapperid="'+row.locationMapperId+'" data-deviceid="'+row.deviceId+'" href="javascript:void(0)">Map school</a></li>';
				}
				if(permission["delete"]){

					row.deviceLocationMappers.length ? action+='<li><a class="unmap-device-btn" data-deviceid="'+row.deviceId+'" href="javascript:void(0)" >Unmap</a></li>' : "";
					action+='<li><a class="delete-device-btn"  data-deviceid="'+row.deviceId+'" href="javascript:void(0)" >'+appMessgaes.delet+'</a></li>';
				}
				if(permission["emailLicense"]){
					action+='<li><a class="key-device-btn" data-deviceid="'+row.deviceId+'" href="javascript:void(0)">'+validationMsg.emailLicenceKey+'</a></li>';	
					action+='<li><a class="view-license-btn" data-devicetype="'+row.deviceType+'  "data-imeiid="'+row.imeiId+'  "data-macid="'+row.macId+'  "data-encodedmacid="'+row.encodedMacId+' "data-deviceid="'+row.deviceId+'" href="javascript:void(0)">'+validationMsg.viewLicense+'</a></li>';	
					
				}				
				action+='</ul>';
			}
			return action;
		},
		cellFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
};
var addDevice = {
		addDeviceBtn : "#addDeviceBtn",
		addModal : "#mdlAddDevice",
		addForm : "#addDeviceForm",
		addDeviceType : "#addDeviceType",
		addMacId : "#addMacId",
		addIMEIId : "#addIMEIId",
		addMake : "#addMake",
		addModel : "#addModel", 
		addOS : "#addOS",
		addSerialNo : "#addSerialNo",
		addAssetNo : "#addAssetNo",
		addDesktopVersion : "#addDesktopVersion",
		addContentVersion : "#addContentVersion",
		addAssessmentVersion : "#addAssessmentVersion",
		addMackIdDiv : "#addMackIdDiv",
		addIMEIDiv : "#addIMEIDiv",
		addMoreBtn : "#addDeviceSaveMoreButton",

		eleStartDate   	: '#addStartDate',
		eleEndDate	   	: '#addEndDate',



		init : function(){
			device.formValidate(this.addForm);	
			$(this.addDeviceBtn).on("click",this.initAdd);
			$(this.addDeviceType).on("change",function(){
				if($(this).val() == "DESKTOP"){
					$(addDevice.addMackIdDiv).show();
					$(addDevice.addIMEIDiv).hide();
				} else {
					$(addDevice.addMackIdDiv).hide();
					$(addDevice.addIMEIDiv).show();
				}
			});
		},
		initAdd : function(){
			fnInitSaveAndAddNewList();
			device.resetForm(addDevice.addForm);
			$(addDevice.addModal).modal("show");
			var start = new Date(); 
			var end = new Date();
			$('.datepicker').datepicker("remove");
			$('.datepicker').datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start
			});
			$(addDevice.eleStartDate).datepicker({
				startDate : start,
				endDate   : end
			}).on('hide', function(){				
				$(addDevice.eleEndDate).val("");
				$(addDevice.eleEndDate).datepicker('setStartDate', $(this).val());

			});
		},
		fnAdd : function(submitBtnId){
			var ajaxData = {
					deviceType 	: $(addDevice.addDeviceType).val(),
					macId 		: $(addDevice.addMacId).val(),
					imeiId		: $(addDevice.addIMEIId).val(),
					make		: $(addDevice.addMake).val(),
					model 		: $(addDevice.addModel).val(),
					os 			: $(addDevice.addOS).val(),
					serialNumber: $(addDevice.addSerialNo).val(),
					assetNumber : $(addDevice.addAssetNo).val(),
					desktopAppVersion 			: $(addDevice.addDesktopVersion).val(),
					tabletContentAppVersion 	: $(addDevice.addContentVersion).val(),
					tabletAssessmentAppVersion 	: $(addDevice.addAssessmentVersion).val(),
					startDate:$(addDevice.eleStartDate).val(),
					endDate:$(addDevice.eleEndDate).val(),

			};
			spinner.showSpinner();
			var addAjaxCall = shiksha.invokeAjax("device/", ajaxData, "POST");

			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					$(device.table).bootstrapTable("refresh");
					if(submitBtnId == "addDeviceSaveButton"){
						$(addDevice.addModal).modal("hide");
						AJS.flag({
							type : "success",
							title : appMessgaes.success,
							body : addAjaxCall.responseMessage,
							close :'auto'
						})	
					} else {
						$(addDevice.addForm)[0].reset();
						$(addDevice.addModal).find("#divSaveAndAddNewMessage").show();
						var name = addAjaxCall.macId ? addAjaxCall.macId : addAjaxCall.imeiId;
						fnDisplaySaveAndAddNewElementAui(addDevice.addModal,name);
					}
					device.refreshTable(device.table);
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

			spinner.hideSpinner();
		}

};


var editDevice = {
		editDeviceBtn : ".edit-device-btn",
		editModal : "#mdlEditDevice",
		editForm : "#editDeviceForm",
		editDeviceType : "#editDeviceType",
		editMacId : "#editMacId",
		editIMEIId : "#editIMEIId",
		editMake : "#editMake",
		editModel: "#editModel",
		editOS : "#editOS",
		editSerialNo : "#editSerialNo",
		editAssetNo : "#editAssetNo",
		editDesktopVersion : "#editDesktopVersion",
		editContentVersion : "#editContentVersion",
		editAssessmentVersion : "#editAssessmentVersion",
		editMackIdDiv : "#editMackIdDiv",
		editIMEIDiv : "#editIMEIDiv",
		editDeviceId : "#editDeviceId",

		eleStartDate   	: '#editStartDate',
		eleEndDate	   	: '#editEndDate',



		init : function(){
			device.formValidate(this.editForm);	
			$(document).on("click",this.editDeviceBtn,this.initEdit);

			$(this.editDeviceType).on("change",function(){
				if($(this).val() == "DESKTOP"){
					$(editDevice.editMackIdDiv).show();
					$(editDevice.editIMEIDiv).hide();
					$(editDevice.editIMEIId).val("");
				} else {
					$(editDevice.editMackIdDiv).hide();
					$(editDevice.editIMEIDiv).show();
					$(editDevice.editMacId).val("");
				}
			});
		},
		initEdit : function(){
			$(editDevice.editForm)[0].reset();

			var start = new Date(); 
			var end = new Date();
			$('.datepicker').datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start
			});
			$(editDevice.eleStartDate).datepicker({
				startDate : start,
				endDate   : end
			}).on('hide', function(){				
				$(editDevice.eleEndDate).val("");
				$(editDevice.eleEndDate).datepicker('setStartDate', $(this).val());

			});


			var deviceId = $(this).data("deviceid");
			var getAjaxCall = shiksha.invokeAjax("device/"+deviceId, null, "GET");

			$(editDevice.editDeviceId).val(getAjaxCall.deviceId);
			$(editDevice.editDeviceType).val(getAjaxCall.deviceType);
			$(editDevice.editMacId).val(getAjaxCall.macId);
			$(editDevice.editIMEIId).val(getAjaxCall.imeiId);
			$(editDevice.editMake).val(getAjaxCall.make);
			$(editDevice.editModel).val(getAjaxCall.model);
			$(editDevice.editOS).val(getAjaxCall.os);
			$(editDevice.editSerialNo).val(getAjaxCall.serialNumber);
			$(editDevice.editAssetNo).val(getAjaxCall.assetNumber);
			$(editDevice.editDesktopVersion).val(getAjaxCall.desktopAppVersion);
			$(editDevice.editContentVersion).val(getAjaxCall.tabletContentAppVersion);
			$(editDevice.editAssessmentVersion).val(getAjaxCall.tabletAssessmentAppVersion);

			$(editDevice.eleStartDate).val(getAjaxCall.startDate);
			$(editDevice.eleEndDate).val(getAjaxCall.endDate);

			if(getAjaxCall.deviceType == "DESKTOP"){
				$(editDevice.editMackIdDiv).show();
				$(editDevice.editIMEIDiv).hide();
			} else {
				$(editDevice.editMackIdDiv).hide();
				$(editDevice.editIMEIDiv).show();
			}


			spinner.hideSpinner();
			$(editDevice.editModal).modal("show");
		},
		fnEdit : function(){
			var ajaxData = {

					deviceId 	: $(editDevice.editDeviceId).val(),
					deviceType 	: $(editDevice.editDeviceType).val(),
					macId 		: $(editDevice.editMacId).val(),
					imeiId		: $(editDevice.editIMEIId).val(),
					make		: $(editDevice.editMake).val(),
					model 		: $(editDevice.editModel).val(),
					os 			: $(editDevice.editOS).val(),
					serialNumber: $(editDevice.editSerialNo).val(),
					assetNumber : $(editDevice.editAssetNo).val(),
					desktopAppVersion 			: $(editDevice.editDesktopVersion).val(),
					tabletContentAppVersion 	: $(editDevice.editContentVersion).val(),
					tabletAssessmentAppVersion 	: $(editDevice.editAssessmentVersion).val(),
					startDate:$(editDevice.eleStartDate).val(),
					endDate:$(editDevice.eleEndDate).val(),


			};
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("device/", ajaxData, "PUT");

			if(editAjaxCall != null){
				if(editAjaxCall.response == "shiksha-200"){
					$(device.table).bootstrapTable("refresh");
					$(editDevice.editModal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : editAjaxCall.responseMessage,
						close :'auto'
					})				
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : editAjaxCall.responseMessage,
						close :'auto'
					});
				}
				device.refreshTable(device.table);
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

			spinner.hideSpinner();


		}

};

var mapDevice = {
		mapDeviceBtn : ".map-device-btn",
		mapModal : "#mdlMapDevice",
		mapForm : "#mapDeviceForm",
		mapDeviceId : "#mapDeviceId",
		unMapSectionBtn : ".unmap-device",

		deviceId:'',
		isLocationMapped:false,
		locationMapperId:0,
		unampSectionBtn : null,

		init : function(){	
			$(document).on("click",this.mapDeviceBtn,this.initMap);
			$(document).on("click",this.unMapSectionBtn,this.initUnampSection);
			$(this.mapForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					schoolId : {
						required : true
					},
					gradeId : {
						required : true
					},
					sectionId : {
						required : true
					}
				},
				messages: {
					schoolId : {
						required : "Please select School"
					},
					gradeId : {
						required : "Please selet Grade"
					},
					sectionId : {
						required : "Please select Section"
					}
				},
				errorPlacement: function(error, element) {
					if($(element).hasClass("select")){
						$(element).parent().append(error);
					} else {
						$(element).after(error);
					}
				},
				submitHandler : function(){
					mapDevice.fnMap();
				}
			});
		},
		initMap : function(){
			spinner.showSpinner();

			$(editDevice.editForm)[0].reset();
			device.resetForm(mapDevice.mapForm);
			mapDevice.deviceId = $(this).data("deviceid");
			mapDevice.isLocationMapped =$(this).data("islocmapped");
			mapDevice.locationMapperId =$(this).data("locmapperid");


			var getAjaxCall = shiksha.invokeAjax("device/"+mapDevice.deviceId, null, "GET");
			if(getAjaxCall != null){
				if(getAjaxCall.response == "shiksha-200"){
					$(mapDevice.mapModal).modal("show");
					spinner.showSpinner();
					locationUtility.doResetLocation();
					$("#tblMappedSections tbody").empty();
					$(locationPanel.divFilter).css('display','none');
					$.each($(locationPanel.eleLocType),function(i,ele){$(ele).prop('checked',false);});
					if(getAjaxCall.deviceLocationMappers.length){
						//var locations = shiksha.invokeAjax("device/location/"+mapDevice.locationMapperId, null, "GET");
						mapDevice.fnEditLocation(getAjaxCall);
						mapDevice.showMappedSections(getAjaxCall);
					}
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : getAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		fnMap : function(){
			var ajaxData = {

					locationMapperId:mapDevice.locationMapperId,
					deviceId 	: mapDevice.deviceId,
					school : {
						id :  parseInt($(locationPanel.elementSchool).val()),
					},
					deviceLocationMappers : [{
						gradeId : parseInt($(locationPanel.eleGrade).val()),
						sectionIds : []
					}]
			};
			$.each($(locationPanel.eleSection).val(),function(x,section){
				if(section != "multiselect-all"){
					ajaxData.deviceLocationMappers[0]['sectionIds'].push(section);
				}
			});
			spinner.showSpinner();
			var mapAjaxCall = shiksha.invokeAjax("device/location", ajaxData, "POST");

			if(mapAjaxCall != null){
				if(mapAjaxCall.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : mapAjaxCall.responseMessage,
						close :'auto'
					});
					mapDevice.showMappedSections(mapAjaxCall)
					$(locationPanel.elementSchool).trigger('change');
					$(locationPanel.section).hide();
					$(device.table).bootstrapTable("refresh");
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : mapAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

			spinner.hideSpinner();	
		},

		fnEditLocation : function(deviceLoc){
			spinner.showSpinner();
			console.log(deviceLoc);
			var school 	=deviceLoc.school;
			var grade 	=deviceLoc.grade;
			var section	=deviceLoc.section;
			$(locationPanel.divFilter).css('display','block');
			var idMap = {
					'#statesForZone'				:school.stateId,
					'#selectBox_zonesByState'		:school.zoneId,
					'#selectBox_districtsByZone'	:school.districtId,
					'#selectBox_tehsilsByDistrict'	:school.tehsilId,
					'#selectBox_blocksByTehsil'		:school.blockId,
					'#selectBox_cityByBlock'		:school.cityId,
					'#selectBox_npByBlock'			:school.nyayPanchayatId,
					'#selectBox_gpByNp'				:school.gramPanchayatId,
					'#selectBox_revenueVillagebyGp'	:school.revenueVillageId,
					'#selectBox_villagebyRv'		:school.villageId,
					'#selectBox_schoolsByLoc'		:school.id
			};

			$.each($(locationPanel.eleLocType),function(i,ele){
				if($(ele).data('code')==school.schoolLocationTypeCode){
					$(ele).trigger('change');
					$(ele).prop('checked',true);
				}
			});
			var ele_keys = Object.keys(idMap);
			$(ele_keys).each(function(ind, ele_key){
				$(ele_key).multiselect('destroy');
				if(idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value="+idMap[ele_key]+"]").prop('selected', true);		
				enableMultiSelect(ele_key);
			});

			device.showGrades();
			/*$(locationPanel.eleGrade).val(grade.gradeId);
			$(locationPanel.eleGrade).trigger('change');
			device.showSections();
			$(locationPanel.eleSection).val(section.sectionId);
			$(locationPanel.eleSection).trigger('change');*/

			spinner.hideSpinner();
		},
		showMappedSections : function(deviceDetails){
			$("#tblMappedSections tbody").empty();
			if(deviceDetails.deviceLocationMappers.length){
				$.each(deviceDetails.deviceLocationMappers, function(x, grade){
					$.each(grade.sections, function(y, section){
						$("#tblMappedSections tbody").append('<tr data-schoolid="'+deviceDetails.school.id+'" data-gradeid="'+grade.grade.gradeId+'" data-sectionid="'+section.sectionId+'"><td>'+grade.grade.gradeName+'</td><td>'+section.sectionName+'</td><td><div class=""><a class="tooltip tooltip-top unmap-device"><span class="tooltiptext">Unmap</span><span class="glyphicon glyphicon-remove font-size12"></span></a></div></td><tr>');
					})
				})
			}
			
		},
		initUnampSection : function(){
			mapDevice.unampSectionBtn = $(this);
			$("#mdlWarningUnamp").modal("show");
		},
		fnUnampSection :function(){
			var schoolId 	= mapDevice.unampSectionBtn.closest("tr").attr("data-schoolid");
			var gradeId  	= mapDevice.unampSectionBtn.closest("tr").attr("data-gradeid");
			var sectionId 	= mapDevice.unampSectionBtn.closest("tr").attr("data-sectionid");
			
			spinner.showSpinner();
			var unMapAjaxCall = shiksha.invokeAjax("/device/"+mapDevice.deviceId+"/location/school/"+schoolId+"/grade/"+gradeId+"/section/"+sectionId+"/unmap", null, "PUT");

			if(unMapAjaxCall != null){
				if(unMapAjaxCall.response == "shiksha-200"){
					$("#mdlWarningUnamp").modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : unMapAjaxCall.responseMessage,
						close :'auto'
					});
					mapDevice.showMappedSections(unMapAjaxCall);
					$(locationPanel.elementSchool).trigger('change');
					$(locationPanel.section).hide();
					$(device.table).bootstrapTable("refresh");
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : unMapAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

			spinner.hideSpinner();	
		}
};

var deleteDevice = {
		deleteDeviceBtn : ".delete-device-btn",
		deleteModal : "#mdlDeleteDevice",
		deleteDeviceId : "#deleteDeviceId",
		deleteConfirmBtn : "#deleteDeleteButton",
		init : function(){
			$(document).on("click",this.deleteDeviceBtn,this.initDelete);
			$(document).on("click",this.deleteConfirmBtn,this.fnDelete);
		},
		initDelete :function(){
			var deviceId = $(this).data("deviceid");
			$(deleteDevice.deleteDeviceId).val(deviceId);
			$(deleteDevice.deleteModal).modal("show");																																																					
		},
		fnDelete : function(){
			var deviceId =  $(deleteDevice.deleteDeviceId).val();

			var deleteAjaxCall = shiksha.invokeAjax("device/"+deviceId, null, "DELETE");

			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-200"){
					$(device.table).bootstrapTable("refresh");
					$(deleteDevice.deleteModal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					})				
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

			spinner.hideSpinner();
		}
};

var unmapDevice = {
		unmapDeviceBtn : ".unmap-device-btn",
		unmapModal : "#mdlUnmapDevice",
		unmapDeviceMapperId : "#unmapDeviceMapperId",
		unmapConfirmBtn : "#unmapDeviceButton",
		init : function(){
			$(document).on("click",this.unmapDeviceBtn,this.initUnmap);
			$(document).on("click",this.unmapConfirmBtn,this.fnUnmap);
		},
		initUnmap :function(){
			var mapperId = $(this).data("deviceid");
			$(unmapDevice.unmapDeviceMapperId).val(mapperId);
			$(unmapDevice.unmapModal).modal("show");																																																					
		},
		fnUnmap : function(){
			var deviceId =  $(unmapDevice.unmapDeviceMapperId).val();

			var unampAjaxCall = shiksha.invokeAjax("/device/"+deviceId+"/location/unmap", null, "PUT");

			if(unampAjaxCall != null){
				if(unampAjaxCall.response == "shiksha-200"){
					$(device.table).bootstrapTable("refresh");
					$(unmapDevice.unmapModal).modal("hide");
					AJS.flag({
						type : "success",
						title : "Success",
						body : unampAjaxCall.responseMessage,
						close :'auto'
					})				
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : unampAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

			spinner.hideSpinner();
		}
};

var viewLicense={

		licenceViewBtn 	: ".view-license-btn",
		modal 		: "#mdlViewDeviceLicence",
		macIdTxt            :"#macId",
		imeiIdText      :"#imeiId",
		encodedMacIdText    :"#encodedMacId",
		init : function(){
			
			$(document).on("click",this.licenceViewBtn,this.initViewLicence);
		},
		initViewLicence :function(){
			$(viewLicense.macIdTxt).text("");
			$(viewLicense.imeiIdText).text("");
			$(viewLicense.encodedMacIdText).val("");
			var deviceType=$(this).data("devicetype").trim();
			if(deviceType=="DESKTOP"){
				$('#tabletDiv').hide()
				$('#desktopDiv').show();
				
			$(viewLicense.macIdTxt).text($(this).data("macid"));
		
			}else if(deviceType=="TABLET"){
				$('#desktopDiv').hide()
				$('#tabletDiv').show()
				$(viewLicense.imeiIdText).text($(this).data("imeiid"));
			}
			$(viewLicense.encodedMacIdText).val($(this).data("encodedmacid"));
            $(viewLicense.modal).modal("show");																																																					
		},
}
 var emailLicense={
		licenceDeviceBtn 	: ".key-device-btn",
		licenceModal 		: "#mdlDeviceLicence",
		licenceDeviceId 	: "#licenceDeviceId",
		btnSend				: "#btnSend",
		copyLicenceButton   : "#copyLicenceButton",
		
		
		
		eleRole		:"#selectBox_role",
		eleUser		:"#selectBox_user",
		eleEmail	:"#email",
		roleDiv		:"#roleDiv",
		userDiv		:"#userDiv",
		emailDiv	:"#emailDiv",
		
		user		: '',
		device		: '',

		init : function(){
			$(document).on("click",this.licenceDeviceBtn,this.initEmailLicence);
			$(emailLicense.eleRole).on('change',this.showUsers);
			$(emailLicense.eleUser).on('change',this.showEmail);
			$(emailLicense.btnSend).on('click',this.sendEmail);
		    emailLicense.user ='';
			emailLicense.device ='';

		},
		initEmailLicence :function(){
			
			emailLicense.device =$(this).data("deviceid");						
			emailLicense.showRoles();
			$(emailLicense.licenceModal).modal("show");																																																					
		},
		showRoles : function(){
			emailLicense.fnGetRoles($(emailLicense.eleRole));
			setOptionsForMultipleSelect(emailLicense.eleRole);
			$(emailLicense.eleRole).multiselect("rebuild");
			$(emailLicense.userDiv).hide();
			$(emailLicense.emailDiv).hide();
			$(emailLicense.roleDiv).show();
			$(emailLicense.btnSend).prop("disabled",true);

		},
		showUsers : function(){
			var flag =emailLicense.fnGetUsers($(emailLicense.eleUser),$(emailLicense.eleRole).val());
			if(!flag){
				setOptionsForMultipleSelect(emailLicense.eleUser);
				$(emailLicense.eleUser).multiselect("rebuild");
				$(emailLicense.emailDiv).hide();
				$(emailLicense.userDiv).show();					
			}
			$(emailLicense.btnSend).prop("disabled",true);

		},
		showEmail : function(){
			$(emailLicense.eleEmail).val('');
			$(emailLicense.emailDiv).show();
			var email =$(this).find('option:selected').data('email');
			if(email.lastIndexOf('@')!=-1){
				$(emailLicense.eleEmail).val(email);
				emailLicense.user =$(this).find('option:selected').val();
				$(emailLicense.btnSend).prop("disabled",false);
			}else{
				$(emailLicense.btnSend).prop("disabled",true);
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : validationMsg.emailRequired,
					close :'auto'
				});
			}
		},
		
		sendEmail : function(){
			spinner.showSpinner();
			var sendResopnse =shiksha.invokeAjax("device/license/"+emailLicense.device+"/user/"+emailLicense.user, null, "POST");
			if(sendResopnse != null){
				if(sendResopnse.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title : "Success",
						body : sendResopnse.responseMessage,
						close :'auto'
					})	;
					$(emailLicense.licenceModal).modal("hide");
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : sendResopnse.responseMessage,
						close :'auto'
					});
				}
				spinner.hideSpinner();
			}
		},
		
		
		
		fnGetRoles : function(elementId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("role/list",null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.roleId+'" data-id="'+object.roleId+'"data-name="'+object.roleName+'">'+object.roleName+'</option>';
					$(elementId).append(row);
				})


			}
			spinner.hideSpinner();
		},
	    fnGetUsers : function(elementId,roleId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("user/role/"+roleId,null, "GET");
			
			if(responseData!=null?responseData.length==0:false){
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : validationMsg.noUserForRole,
					close :'auto'
				});
				$(emailLicense.emailDiv).hide();
				$(emailLicense.userDiv).hide();
				spinner.hideSpinner();
				return false;
			}
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.id+'" data-id="'+object.id+' " data-email="'+object.email+' "data-name="'+object.userName+'">'+object.userName+'</option>';
					$(elementId).append(row);
				})
			}
			spinner.hideSpinner();
		},


}
var submitActor = null;
var $submitActors = $(addDevice.addForm).find('button[type=submit]');

$( document ).ready(function() {
	device.init();
	addDevice.init();
	editDevice.init();
	deleteDevice.init();
	mapDevice.init();
	emailLicense.init();
	unmapDevice.init();
	viewLicense.init();
	$(device.table).bootstrapTable({
		toolbar: "#deviceToolBar",
		url : "device/page",
		method : "post",
		search: true,
		sidePagination: "server",
		showToggle: false,
		showColumns: true,
		pagination: true,
		pageSize: 10,
		clickToSelect: false,
		//queryParamsType :'limit',
		//queryParams : device.fiterQueryParmams
	});
	$("#deviceToolBar").show();
	$submitActors.click(function() {
		submitActor = this;
	});
	$(document).on("keypress",".numberonly",function (e) {
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && !e.ctrlKey && (e.which < 96 || e.which > 105)) {
			return false;
		}
	});



	$(addDevice.eleStartDate).on('changeDate', function(){
		$(addDevice.eleEndDate).datepicker('setStartDate', $(this).val());
	});

	jQuery.validator.addMethod("macid", function(value) { 
		return /^([0-9A-Fa-f]{2}[\.:-]){5}([0-9a-fA-F]{2})$/.test(value); 
	}, "");

	$('#collapseFilter').on('hidden.bs.collapse', function () {
	    $('#collapseFilter #locationPnlLink').text("Select Location");
	    $("#collapseFilter #collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green");
		$("#collapseFilter #collapseSpanSign").addClass("glyphicon-plus-sign").addClass("green");
    });
	
     $('#collapseFilter').on('shown.bs.collapse', function () {
		$('#collapseFilter #locationPnlLink').text("Hide Location");
       $("#collapseFilter #collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green ");
       $("#collapseFilter #collapseSpanSign").addClass("glyphicon-minus-sign").addClass("red");
    	
});
});