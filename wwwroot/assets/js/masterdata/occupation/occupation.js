var pageContextElements = {	
		eleAddButton 	: '#addNewOccupationName',
		table 			: '#occupationTable',
		toolbar			: "#toolbarOccupation"
};
var addModal ={
	form 			: '#addOccupationForm',
	modal   		: '#mdlAddOccupation',
	saveMoreButton	: 'addOccupationSaveMoreButton',
	saveButton		: 'addOccupationSaveButton',
	eleOccupation   : '#addOccupationName',
	message			: "#divSaveAndAddNewMessage"
};
var editModal ={
		form	 : '#editOccupationForm',
		modal	 : '#mdlEditOccupation',
		eleOccupation: '#editOccupation',
		eleOccupationId: '#occupationId',
};
var deleteModal ={
		modal	: '#mdlDeleteOccupation',
		confirm : '#deleteOccupationButton',
		message	: "#deleteOccupationMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var occupationIdForDelete;
var occupationFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var occupation={
		name:'',
		occupationId:0,

		init:function(){

			$(pageContextElements.eleAddButton).on('click',occupation.initAddModal);
			$(deleteModal.confirm).on('click',occupation.deleteFunction);

		},
		deleteFunction : function(){
			occupation.fnDelete(occupationIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			occupation.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleOccupation).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("occupation", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : occupationMessages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					occupation.resetForm(addModal.form);
					occupation.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(occupationId){
			occupation.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("occupation/"+occupationId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleOccupation).val(editAjaxCall.name);
					$(editModal.eleOccupationId).val(editAjaxCall.occupationId);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"			:$(editModal.eleOccupation).val(),
					"occupationId"	:$(editModal.eleOccupationId).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("occupation", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					occupation.resetForm(editModal.form);
					occupation.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: occupationMessages.sucessAlert,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(occupationId,name){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			occupationIdForDelete=occupationId;
		},
		fnDelete :function(occupationId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("occupation/"+occupationId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					occupation.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : occupationMessages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					name: {
						required : true,
						noSpace	 :true,
						minlength : 2,
						maxlength : 50,
					},
				
				},

				messages: {
					name: {
						required : occupationMessages.nameRequired,
						noSpace  :occupationMessages.nameNoSpace,
						minlength : occupationMessages.nameMinLength,
						maxlength : occupationMessages.nameMaxLength,
					},
					
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						occupation.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						occupation.fnUpdate();
					}
				}
			});
		},
		occupationActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="occupation.initEdit(\''+row.occupationId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="occupation.initDelete(\''+row.occupationId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		occupationNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	occupation.formValidate(addModal.form);
	occupation.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	occupation.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar			: pageContextElements.toolbar,
		url 			: "occupation",
		method 			: "get",
		toolbarAlign 	: "right",
		search			: false,
		sidePagination	: "client",
		showToggle		: false,
		showColumns		: false,
		pagination		: true,
		searchAlign		: 'left',
		pageSize		: 20,
		clickToSelect	: false,
		formatShowingRows : occupationFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});