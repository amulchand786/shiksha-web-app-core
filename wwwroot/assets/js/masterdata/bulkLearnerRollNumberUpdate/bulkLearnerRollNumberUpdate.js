var pageContextElements = {
		divFilter   :'#divFilter',
		eleLocType  : '#divCenterLocType input:radio[name=locType]',
		filter		: '#locColspFilter',
		panelLink	: '#locationPnlLink',
		form		: '#listCenterForm',
		eleCollapseFilter		:'#collapseFilter',
		eleToggleCollapse		: '[data-toggle="collapse"]',
		clearBtn	 : '#btnResetLocation',
		levelDiv	 : '#divSelectLevel',
		batchDiv	 : '#divSelectBatch',
		yearDiv      :'#divSelectYear',
		academicYearDiv : '#divSelectAcademicYear',
		learnerDiv  : '#viewLearnerDiv',
		learnerBtn	: '#viewLearnerBtn',
		tableDiv	: '#rowDiv2',
		table		: '#tableDiv',
		bootstrapTable :'#bulkRollNoUpdateTable',
		updateBtn	: '#updateRollNumberBtn',

};
var selectBox ={
		center		: '#selectBox_centersByLoc',
		level		: '#selectBox_Level',
		batch		: '#selectBox_Batch',
		academicYear: '#selectBox_AcademicYear',
		year		: '#selectBox_year'
};
var reset = {
		modal : '#mdlResetLoc',
		confirm : '#btnResetLocYes',
};
var updateElements = {
		table 		: '#bulkRollNoUpdateTable',
		centerId	: '',
		levelId		: '',
		batchId		: '',
		groupId		: '',
		ayCycleId	: '',
		allLearners	: '',
};
var learnerData ={};
var learnersList=[];
var learnersListForUpdate=[];
var updatedLearnerData;
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		level 				: "#selectBox_Level",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var $table;
var locationUtility={
		fnInitLocation :function(){
			$(pageContextElements.divFilter).css('display','none');
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
		},
		resetLocation :function(obj){	
			$(reset.modal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},

		doResetLocation : function() {

			$(selectBox.level).multiselect('destroy').multiselect('rebuild');
			$(selectBox.batch).multiselect('destroy').multiselect('rebuild');
			$(selectBox.academicYear).multiselect('destroy').multiselect('rebuild');
			$(pageContextElements.levelDiv).hide();
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
			enableMultiSelect($(selectBox.batch));
			enableMultiSelect($(selectBox.level));
			deleteAllRow("studentTable");
			if ($(resetButtonObj).parents("form").attr("id") == 'listCenterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);

			}
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},

		displaySmartFilters : function(lCode,lvlName){

			elementIdMap = {
					"State" 	: '#statesForZone',
					"Zone" 		: '#selectBox_zonesByState',
					"District" 	: '#selectBox_districtsByZone',
					"Tehsil" 	: '#selectBox_tehsilsByDistrict',
					"Block" 	: '#selectBox_blocksByTehsil',
					"City" 		: '#selectBox_cityByBlock',
					"Nyaya Panchayat" 	: '#selectBox_npByBlock',
					"Gram Panchayat" 	: '#selectBox_gpByNp',
					"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
					"Village" : '#selectBox_villagebyRv',
					"Center" : '#selectBox_centersByLoc'
			};

			displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));

			locationUtility.reinitializeSmartFilter(elementIdMap);

			var lLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
			var lLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');

			displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));

			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(
					function(ind, ele_key) {
						$(elementIdMap[ele_key]).find("option:selected").prop(
								'selected', false);
						$(elementIdMap[ele_key]).multiselect('destroy');
						enableMultiSelect(elementIdMap[ele_key]);
					});
			setTimeout(function() {
				$(".outer-loader").hide();
				$('#rowDiv1,#divFilter').show();
			}, 100);
			//

			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});

			$('#rowDiv1,#divFilter').show();

			spinner.hideSpinner();
		},

};
var learnerRollNumberUpdate = {

		queryParmams : function(params){
			params['center'] = {"id":$(selectBox.level).val()};
			params['level'] ={"levelId": $(selectBox.center).val()};
			params['batch'] = {"batchId":$(selectBox.batch).val()};
			params['academicCycle'] = {"academicCycleId":$(selectBox.academicYear).val()};
			return params;
		},
};
var bulkLearnerRollNumberUpdate = {
		mapperData:{
			center		 : '',
			level 		 : '',
			batch		 : '',
			academicCycle: ''
		},
		fnInit:function(){
			$(reset.confirm).on('click',locationUtility.doResetLocation);
			$(selectBox.center).on('change',bulkLearnerRollNumberUpdate.showLevels);
			$(selectBox.level).on('change',bulkLearnerRollNumberUpdate.showBatches);
			$(selectBox.batch).on('change',bulkLearnerRollNumberUpdate.showAcademicCycles);
			$(pageContextElements.learnerBtn).on('click',bulkLearnerRollNumberUpdate.showTable);
			$(pageContextElements.updateBtn).on('click',bulkLearnerRollNumberUpdate.initUpdate)
			$(pageContextElements.eleLocType).change(function() {
				$(pageContextElements.filter).removeClass('in');
				$(pageContextElements.panelLink).text(centerColumn.SelectLocation);
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
				var typeCode =$(this).data('code');
				$(pageContextElements.levelDiv).hide();
				$(pageContextElements.batchDiv).hide();
				$(pageContextElements.yearDiv).hide();
				$(pageContextElements.academicYearDiv).hide();
				$(pageContextElements.tableDiv).hide();
				$(pageContextElements.learnerDiv).hide();
				if(typeCode.toLowerCase() =='U'.toLowerCase()){
					showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("U");
					fnCollapseMultiselect();
				}else{
					hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("R");
					fnCollapseMultiselect();

				}
				$(pageContextElements.eleCollapseFilter).css('display','');
			});
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		showLevels : function(){
			shikshaPlus.fnGetLevelsByCenter(selectBox.level,0,$(selectBox.center).val());
			setOptionsForMultipleSelect(selectBox.level);
			$(selectBox.level).multiselect("rebuild");
			$(pageContextElements.levelDiv).show();
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();

		},
		showBatches : function(){
			shikshaPlus.fnGetBatchesByCenterAndLevel(selectBox.batch,0,$(selectBox.center).val(),$(selectBox.level).val());
			setOptionsForMultipleSelect(selectBox.batch);
			$(selectBox.batch).multiselect("rebuild");
			$(pageContextElements.batchDiv).show();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();

		},
		showAcademicCycles : function(){
			shikshaPlus.fnGetCurrentYear(selectBox.year,0);
			setOptionsForMultipleSelect(selectBox.year);
			$(selectBox.year).multiselect("rebuild");

			shikshaPlus.fnGetCurrentACYear(selectBox.academicYear,0);
			setOptionsForMultipleSelect(selectBox.academicYear);
			$(selectBox.academicYear).multiselect("rebuild");
			$(pageContextElements.yearDiv).show();
			$(pageContextElements.academicYearDiv).show();
			$(pageContextElements.learnerDiv).show();
		},
		enableEditable : function(){
			$('.roll-number-edit').editable({
				type: 'text',
				step: 'any',
				placement: 'right',
				validate: function(value) {
					if(value.indexOf(".") != -1){
						return centerColumn.validRoll;
					}
					if($.isNumeric(value) == '') {
						return centerColumn.validRoll;
					}if(value<='0'){
						return centerColumn.rollNonZero;
					}
				}
			}).on("save",function(e, params){
			});
		},
		rollNumber :function(index,row){
			var rollnumber;
			if(row.rollNumber !=null){
				rollnumber = '<a href="#" data-learnerId="'+row.learnerId+'" data-rollNumber="'+row.rollNumber+'" data-learnerName="'+row.name+'" '
				+'data-title="Enter roll" class="editable editable-click roll-number-edit">'+row.rollNumber+'</a>';
			}
			else{
				rollnumber = '<a href="#" data-learnerId="'+row.learnerId+'" data-rollNumber="'+row.rollNumber+'" data-learnerName="'+row.name+'" '
				+'data-title="Enter roll" class="editable editable-click roll-number-edit"></a>';
			}
			return rollnumber;
		},
		showTable :function(){
			bulkLearnerRollNumberUpdate.mapperData={
					center		 : {"id":$(selectBox.center).val()},
					level 		 : {"levelId": $(selectBox.level).val()},
					batch		 : {"batchId":$(selectBox.batch).val()},
					academicCycle: {"academicCycleId":$(selectBox.academicYear).val()}
			}
			var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=rollNumber",bulkLearnerRollNumberUpdate.mapperData, "POST");
			if(ajaxCall.response == 'shiksha-200'){
				$(pageContextElements.bootstrapTable).bootstrapTable('load',ajaxCall.learners);			
				$(pageContextElements.tableDiv).show();
				bulkLearnerRollNumberUpdate.enableEditable();
			}else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				});
			}

			spinner.hideSpinner();
		},
		initUpdate : function(){
			updateElements.centerId = $(selectBox.center).val();
			updateElements.levelId = $(selectBox.level).val();
			updateElements.batchId = $(selectBox.batch).val();
			updateElements.ayCycleId = $(selectBox.academicYear).val();
			learnersList=[];
			learnersListForUpdate=[];
			$.each($(updateElements.table+" tbody tr"),function(){
				learnerData={};
				learnerData.id=$(this).find('a').attr('data-learnerId');
				learnerData.name=$(this).find('a').attr('data-learnerName');
				learnerData.prevRoll=$(this).find('a').attr('data-rollNumber');
				learnerData.newRoll=$(this).find('a').text();
				if(learnerData.newRoll == 'Empty'){
					learnerData.newRoll ='null';
				}
				learnersList.push(learnerData);
			})
			$.each(learnersList,function(index,object){
				if(object.prevRoll != object.newRoll){
					updatedLearnerData={};
					updatedLearnerData.learnerId=object.id;
					updatedLearnerData.name=object.name;
					updatedLearnerData.rollNumber=object.newRoll;
					learnersListForUpdate.push(updatedLearnerData);
				}
			})
			var length=learnersListForUpdate.length;
			$.each(learnersList,function(index,object){
				$.each(learnersListForUpdate,function(ind,obj){
					if((obj.learnerId != object.id) && (parseInt(obj.rollNumber) == parseInt(object.newRoll))){
						length--;
					}
				})
			})
			if(learnersListForUpdate.length>length){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : centerColumn.duplicateRollNumber,
					close : 'auto'
				});


			} else{
				if(learnersListForUpdate.length!=0){
					bulkLearnerRollNumberUpdate.fnUpdate();
				}else{
					AJS.flag({
						type  : "success",
						title : centerColumn.sucessAlert,
						body  : centerColumn.bulkRollNumbersUpdated,
						close : 'auto'
					});
					setTimeout(function(){
						bulkLearnerRollNumberUpdate.showTable();
					}, 700)
				}
			}
			spinner.hideSpinner();
		},
		fnUpdate : function(){
			var data={
					center 			: {'id':updateElements.centerId},
					level	 		: {'levelId':updateElements.levelId},
					batch	 		: {'batchId':updateElements.batchId},
					academicCycle 	: {'academicCycleId':updateElements.ayCycleId},
					learners		: learnersListForUpdate,
			};
			var updateAjaxCall= shiksha.invokeAjax("learner/?type=bulkUpdateRollNumber&op=updateRollNumber",data,'POST');
			spinner.hideSpinner();
			if(updateAjaxCall.response == 'shiksha-200')
			{
				AJS.flag({
					type  : "success",
					title : centerColumn.sucessAlert,
					body  : updateAjaxCall.responseMessage,
					close : 'auto'
				});
				setTimeout(function(){
					var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=rollNumber",bulkLearnerRollNumberUpdate.mapperData, "POST");
					spinner.hideSpinner();
					$(pageContextElements.bootstrapTable).bootstrapTable('load',ajaxCall.learners);	
					bulkLearnerRollNumberUpdate.enableEditable();
				}, 1000);

			}
			else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : updateAjaxCall.responseMessage,
					close : 'auto'
				});
				var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=rollNumber",bulkLearnerRollNumberUpdate.mapperData, "POST");
				spinner.hideSpinner();
				$(pageContextElements.bootstrapTable).bootstrapTable('load',ajaxCall.learners);	
				bulkLearnerRollNumberUpdate.enableEditable();
			}
		},
};
$( document ).ready(function() {
	$.fn.editable.defaults.mode = 'inline';
	bulkLearnerRollNumberUpdate.fnInit();
});