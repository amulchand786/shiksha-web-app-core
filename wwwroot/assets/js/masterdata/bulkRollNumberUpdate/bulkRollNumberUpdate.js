//global variables
var  colorClass='';

var schoolId;
var mappedGrades;
var mappedSchool;
var sectionId;
var lObj;
var elementIdMap;
var bindToElementId;
var jsonData;
var resetButtonObj;
var thLabels;
//global selectDivIds for smart filter
var allInputEleNamesForStudent = [ 'addStudentId', 'addStudentName',
                                   'addFatherName', 'addMotherName', 'addAge', 'selectBox_forAddCast',
                                   'addSelectBox_forGender', 'startDate', 'selectBox_forReligion',
                                   'addAddress' ];
var schoolGradePageContext = {};
	schoolGradePageContext.city = "#divSelectCity";
	schoolGradePageContext.block = "#divSelectBlock";
	schoolGradePageContext.NP = "#divSelectNP";
	schoolGradePageContext.GP = "#divSelectGP";
	schoolGradePageContext.RV = "#divSelectRV";
	schoolGradePageContext.village = "#divSelectVillage";
	schoolGradePageContext.school = "#divSelectSchool";
	
	schoolGradePageContext.schoolLocationType = "#selectBox_schoolLocationType";
	schoolGradePageContext.elementSchool = "#selectBox_schoolsByLoc";
	schoolGradePageContext.grade = "#selectBox_grades";
	
	schoolGradePageContext.resetLocDivCity = "#divResetLocCity";
	schoolGradePageContext.resetLocDivVillage = "#divResetLocVillage";




//Smart filter //
	
	//hide grade,section,upload file div
	var fnHideGrSecFile =function(){
		$('#divSelectGrade').hide();
		$('#divSelectSection').hide();
		$('#divSelectAcademicYear').hide();
		$('#editStudentButton').hide();
		$('#rowDiv2').hide();
	}

	//remove and reinitialize smart filter
	var reinitializeSmartFilter = function(eleMap) {
		var ele_keys = Object.keys(eleMap);
		$(ele_keys).each(function(ind, ele_key) {
			$(eleMap[ele_key]).find("option:selected").prop('selected', false);
			$(eleMap[ele_key]).find("option[value]").remove();
			$(eleMap[ele_key]).multiselect('destroy');
			enableMultiSelect(eleMap[ele_key]);
		});
	}


	//displaying smart-filters
	var displaySmartFilters =function(lCode,lLevelName) {
		$(".outer-loader").show();

		elementIdMap = {
				"State" : '#statesForZone',
				"Zone" : '#selectBox_zonesByState',
				"District" : '#selectBox_districtsByZone',
				"Tehsil" : '#selectBox_tehsilsByDistrict',
				"Block" : '#selectBox_blocksByTehsil',
				"City" : '#selectBox_cityByBlock',
				"Nyaya Panchayat" : '#selectBox_npByBlock',
				"Gram Panchayat" : '#selectBox_gpByNp',
				"Revenue Village" : '#selectBox_revenueVillagebyGp',
				"Village" : '#selectBox_villagebyRv',
				"School" : '#selectBox_schoolsByLoc'
		};
		displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));

		var ele_keys = Object.keys(elementIdMap);
		$(ele_keys).each(
				function(ind, ele_key) {
					$(elementIdMap[ele_key]).find("option:selected").prop(
							'selected', false);
					$(elementIdMap[ele_key]).multiselect('destroy');
					enableMultiSelect(elementIdMap[ele_key]);
				});
		setTimeout(function() {
			$(".outer-loader").hide();
			$('#rowDiv1,#divFilter').show();
		}, 100);
	};

	var positionFilterDiv = function(type) {
		if (type == "R") {
			$(schoolGradePageContext.NP).insertAfter(
					$(schoolGradePageContext.block));
			$(schoolGradePageContext.RV).insertAfter($(schoolGradePageContext.GP));
			$(schoolGradePageContext.school).insertAfter(
					$(schoolGradePageContext.village));
		} else {
			$(schoolGradePageContext.school).insertAfter(
					$(schoolGradePageContext.city));
		}
	}
	
//school location type on change...get locations...
$('#divSchoolLocType input:radio[name=locType]').change(function() {

	fnHideGrSecFile();
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");



	$('#tabPane').css('display','none');

	lObj =this;
	var typeCode =$(this).data('code');		
	$('.outer-loader').show();		
	reinitializeSmartFilter(elementIdMap);	
	$("#hrDiv").show();
	if(typeCode.toLowerCase() =='U'.toLowerCase()){
		showDiv($(schoolGradePageContext.city),$(schoolGradePageContext.resetLocDivCity));
		hideDiv($(schoolGradePageContext.block),$(schoolGradePageContext.NP),$(schoolGradePageContext.GP),$(schoolGradePageContext.RV),$(schoolGradePageContext.village));
		displaySmartFilters($(this).data('code'),$(this).data('levelname'));
		positionFilterDiv("U");
		fnCollapseMultiselect();
	}else{
		hideDiv($(schoolGradePageContext.city),$(schoolGradePageContext.resetLocDivCity));
		showDiv($(schoolGradePageContext.block),$(schoolGradePageContext.NP),$(schoolGradePageContext.GP),$(schoolGradePageContext.RV),$(schoolGradePageContext.village));
		displaySmartFilters($(this).data('code'),$(this).data('levelname'));
		positionFilterDiv("R");
		fnCollapseMultiselect();
	}
	$('.outer-loader').hide();
	return;
});






//custum start @Mukteshwar################################################

var getGradeList = function() {
	$('#rowDiv2').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
	var schoolId = $("#selectBox_schoolsByLoc").val();
	var bintToElementId = $('#selectBox_grade');
	$('.outer-loader').show();
	
	bindGradesBySchoolToListBox(bintToElementId, schoolId, 0);
	setOptionsForMultipleSelect('#selectBox_grade');
	if($('#selectBox_grade').children('option').length==0){
		var schoolName=$('#selectBox_schoolsByLoc').find('option:selected').text();
		$('#msgText').text("School "+schoolName+" is not mapped to any Grade");
		$('#mdlError').modal();
	}else{
		$('#divSelectGrade').show();
	}
	$('.outer-loader').hide();

	fnCollapseMultiselect();

}	
var getSectionList = function(){	
	$('#rowDiv2').hide();//hideTable
	$('#divSelectSection').hide();
	$('#divSelectAcademicYear').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
	$('#divSelectAcademicYear').hide();//hide academic year dropdown
	$('#selectBox_Section').find("option:selected").prop('selected', false);
	$('#selectBox_Section').multiselect('refresh').multiselect('destroy');
	//$('#selectBox_Section')



	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	bindToElementId = $('#selectBox_Section');

	$('.outer-loader').show();

	bindSourceBySchoolAndGradeToListBox(bindToElementId,schoolId,gradeId,0);
	setOptionsForMultipleSelect('#selectBox_Section');
	$('.outer-loader').hide();
	$('#divSelectSection').show();

	fnCollapseMultiselect();

}

var getAcademicYearList = function(){
	$('#rowDiv2').hide();//hideTable
	$('#divSelectAcademicYear').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
	$('#addStudentBtn').prop('disabled', true);//Hide AddButton
	$("#bulkUploadButton").prop('disabled',true);//Hide BulkUpload Button
	$('#selectBox_AcademicYear').find("option:selected").prop('selected', false);
	$('#selectBox_AcademicYear').multiselect('refresh').multiselect('destroy');
	
	
	deleteAllRow('bulkRollNoUpdateTable');
	
	bindToElementId = $('#selectBox_AcademicYear');
	var currentYear = customAjaxCalling("academicYear/currentAcademicYear", null,"GET");
	bindAcademicYearToLustBix(bindToElementId,currentYear.academicYearId);
	setOptionsForMultipleSelect('#selectBox_AcademicYear');
	if($('#selectBox_AcademicYear').children('option').length!=0){
		$('#editStudentButton').show();
	}

	$('.outer-loader').hide();
	$('#divSelectAcademicYear').show();



}


function isNumeric(value) {
	if (value == null || !value.toString().match(/^[0-9]*[1-9][0-9]*$/)) {
		return false;
	}	
	return true;
}

var createStudentRow = function(student,rownumber) {
	var rollNumber = student.rollNumber==null?'N/A':student.rollNumber;
	
	var row = "<tr id='"+ student.studentId	+ "' data-id='"+ student.studentId+ + "' data-rollNumber='"+ rollNumber+ "'>"
	+ "<td>"+rownumber+"</td>"
	/*+ "<td  id='"+ student.studentId	+ "' data-rollNumber='"+student.rollNumber+ "'title='"+ student.rollNumber+ "'></td>"*/
	+ "<td data-studentName='"+student.name+"' title='"+ student.name + "' >" + student.name+"</td>"
	+ "<td data-rollno='"+rollNumber+"' title='"+rollNumber+ "' >" +rollNumber+"</td>"
	+'<td><span style="display:none">'+rollNumber+'</span><input oninput="fnRemoveError('+ student.studentId+');" type="number"  min="'+1+'"  max="'+999999999+'" class="form-control rollnumber_input" step="1"  data-rollNumber="'+rollNumber+'" data-id="'+ student.studentId+'" data-name="'+ student.name	+'" id="'+ student.studentId	+ '"  name="studentRollNo" value="'+rollNumber+'" /></td>'
	+ "<td data-fatherName='"+student.fatherName+ "'	title='"+ student.fatherName+ "'>"+ student.fatherName+ "</td>"
	+ "<td data-motherName='"+student.motherName+ "'title='"+ student.motherName+ "'>"+ student.motherName+"</td>"
	+"</tr>";
	return row;
}

var fnGetStudents= function() {
	$('.outer-loader').show();
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId = parseInt($("#selectBox_Section").find('option:selected').data('id'));
	var academicYearId = parseInt($("#selectBox_AcademicYear").find('option:selected').data('id'));
	var json = {
			"schoolId" : schoolId,
			"gradeId" : gradeId,
			"sectionId" : sectionId,
			"academicYearId":academicYearId
	};

	$('.outer-loader').show();

	var result = customAjaxCalling("studentlistSortByRollNumber", json, "POST");
	if(result.length==0){
		$('#msgText').text("There are no students available for the selected school, grade, section and academic year.");
		$('#mdlError').modal();
	}
	deleteAllRow('bulkRollNoUpdateTable');
	
	$.each(result, function(index, obj) {
		var rowCount=index+1;
		var row = createStudentRow(obj,rowCount);
		appendNewRow('bulkRollNoUpdateTable', row);
	});
	
	
	$('#rowDiv2').show();
	
	$('.outer-loader').hide();
	fnUpdateStudentDt('#bulkRollNoUpdateTable',[2],6,[0,1,2,3,4,5],thLabels);
	
	var rows= $("#bulkRollNoUpdateTable").dataTable().fnGetNodes().length;
	if(rows==0){
		$('#updateRollNumberBtn').prop("disabled",true);
	}
	return;
}

var viewOrEditStudent = function(){
	$('#addStudentButton').show();
	$('#editStudentButton').show();
	$('#linkToUploadTeacher').show();
	$('#addStudentBtn').prop('disabled', false);
}


var gradeAndSectionHide = function(){	
	$('#rowDiv2').hide();
	$('#divSelectGrade').hide();
	$('#addStudentButton').hide();
	$('#editStudentButton').hide();
}


var removeActionButton=function(buttonId){
	$("#bulkRollNoUpdateTable tr").each(function(){
		$(this).find("#"+buttonId).remove();
	});
}



function unique(list) {
	  var result = [];
	  $.each(list, function(i, e) {
	    if ($.inArray(e, result) == -1)
	    	result.push(e);
	  });
	  return result;
	}

var destroyReInitializeMultiselect = function(stringId){
	$("#"+stringId).multiselect('destroy');
	$("#"+stringId).multiselect('refresh');
}
var updateAllEditedRollNumber=function(){
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId = parseInt($("#selectBox_Section").find('option:selected').data('id'));
	var academicYearId = parseInt($("#selectBox_AcademicYear").find('option:selected').data('id'));
	var studentTableRows = $("#bulkRollNoUpdateTable").dataTable().fnGetNodes();
	
	var studentList=[];
	var allStudentList=[];
	var testBeforeAjax=[];
	
	if(studentTableRows.length>0){
		$.each(studentTableRows,function(i,row){
			
			var oldRollNumber=parseInt($(row).find('input').data('rollnumber'));
			var studentId=parseInt($(row).find('input').data('id'));
			var studentName=$(row).find('input').data('name');
			var rollNumber=parseInt($(row).find('input').val());

			var studentData = {};
			studentData.studentId=studentId;
			studentData.studentName=studentName;
			studentData.rollNumber=rollNumber;
			studentData.oldRollNumber=oldRollNumber;

			allStudentList.push(studentData);

			if(oldRollNumber!=rollNumber){
				testBeforeAjax.push(validateRollNumber(studentId,rollNumber));
				studentList.push(studentData);
			}
		});

	}
	
	jsonData={
			"schoolId" : schoolId,
			"gradeId" : gradeId,
			"sectionId" : sectionId,
			"academicYearId":academicYearId,
			"studentList":studentList
	}
	//---------
	if($.inArray(false, testBeforeAjax) && studentList.length>0){
		var result = customAjaxCalling("student/updateStudentRollNumber", jsonData, "PUT");
		if(result!=null){
			var rollNumberList=[]
			if(result.response=="shiksha-800"){
				
				deleteAllRow('bulkRollNoUpdateTable');
				
				fnGetStudents();
				
				
				$.each(result.studentList,function(index,value){
					if(result.studentList[index].isRollNumberUpdated==0){
						rollNumberList.push(result.studentList[index].rollNumber);
						var sId = result.studentList[index].studentId;
						$('#'+sId).find('input').css('color','red');
						$('#'+sId).addClass("has-error");
						$('#'+sId).find('input').val(result.studentList[index].rollNumber);
					}
				});
				if(rollNumberList!=' '){
					//$('#msgText').text("Partially Updated, Roll No.   "+unique(rollNumberList) + "  already exist." );
					//$('#mdlError').modal();
					AJS.flag({
					    type: 'warning',
					    title: 'Warning!',
					    body: "<p>Partially Updated, Roll No.   "+unique(rollNumberList) + "  already exist.</p>",
					    close :'auto'
					})
				}
				
			}else{
				deleteAllRow('bulkRollNoUpdateTable');
				fnGetStudents();
				setTimeout(function() {   
					 AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>Information updated</p>',
					    close :'auto'
					})
					}, 1000);
							
				$('.outer-loader').hide();
			}

		}
		
		$('.outer-loader').hide();
		
	}else{
		 AJS.flag({
			    type: 'info',
			    title: 'Information!',
			    body: "<p>Please edit Student's Roll Number first.  </p>",
			    close :'auto'
			})
	}
	$('.outer-loader').hide();
	
}
$( "#bulkRollNoUpdateTable" ).keypress(function() {		
	$('#bulkRollNoUpdateTable').find('input').css('color','black');
	
	 $(this).parent().removeClass("has-error");
	 
})

var fnRemoveError=function(studentId) {		
	$('#'+studentId).find('input').css('color','black');
	
	$('#'+studentId).removeClass("has-error");
	 
};
function validateRollNumber(studentId,rollNumber){
	
	var result;
	 var re = /^\d*[1-9]\d*$/;
	    result =  re.test(rollNumber);
	    if(!result) {
	    	$('#'+studentId).addClass('has-error');
	    	}
	   return result;
}


//reset only location when click on reset button icon
var resetLocAndSchool = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;


}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation = function() {
	$('#addStudentBtn').prop('disabled', true);
	$("#bulkUploadButton").prop('disabled',true);
	$("#selectBox_grade").multiselect('destroy').multiselect('refresh');
	$("#selectBox_Section").multiselect('destroy').multiselect('refresh');
	$("#selectBox_AcademicYear").multiselect('destroy').multiselect('refresh');
	$("#divSelectGrade").hide();
	$("#divSelectSection").hide();
	$("#divSelectAcademicYear").hide();
	$('#editStudentButton').hide();
	$("#rowDiv2").hide();
	enableMultiSelect($("#selectBox_Section"));
	enableMultiSelect($("#selectBox_grade"));
	deleteAllRow("bulkRollNoUpdateTable");
	if ($(resetButtonObj).parents("form").attr("id") == "listSudentForm") {
		reinitializeSmartFilter(elementIdMap);
		var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
		var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
		displaySmartFilters(aLocTypeCode,aLLevelName);
		
	}
	fnCollapseMultiselect();
}






//Data table fot student table








$(function() {

	thLabels =['#','Student Name  ','Roll No  ','Roll No  ','Father Name ','Mother Name '];
	fnDTForStudetTable('#bulkRollNoUpdateTable',6,[0,1,2,3,4,5],thLabels);
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#bulkRollNoUpdateTable',[2]);
	});
	$(window).bind("pageshow", function() {
		var form = $('form'); 
		form[0].reset();
	});
	
	
	fnColSpanIfDTEmpty('bulkRollNoUpdateTable',6);
	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
	});  
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
	});

	colorClass = $('#t0').prop('class');





	$('#rowDiv1').show();
	// init bootstrap tooltip
	$('[data-toggle="tooltip"]').tooltip();



});





