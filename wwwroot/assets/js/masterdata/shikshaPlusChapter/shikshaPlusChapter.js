var pageContextElements = {
		table:$('#shikshaPlusChapterTable'),
		addBtn:$("#addShikshaPlusChapter"),
};
var addModal={
		form : $('#addShikshaPlusChapterForm'),
		modal : $('#mdlAddShikshaPlusChapter'),
		eleChapterNo : $('#addShikshaPlusChapterNo'),
		eleName :$("#addShikshaPlusChapterName"),
		eleDescription :$("#addShikshaPlusChapterDescription"),
		eleLevel	:$("#addLevel"),
		eleGroup	:$("#addGroup"),
		eleGroupDiv	:$("#groupDiv"),
		eleDuration	:$("#addDuration"),
};

var editModal={
		form : $('#editShikshaPlusChapterForm'),
		modal : $('#mdlEditShikshaPlusChapter'),
		eleId : $('#editShikshaPlusChapterId'),
		eleChapterNo : $('#editShikshaPlusChapterNo'),
		eleName :$("#editShikshaPlusChapterName"),
		eleDescription :$('#editShikshaPlusChapterDescription'),
		eleLevel	:$("#editLevel"),
		eleGroup	:$("#editGroup"),
		eleGroupDiv	:$("#groupDiv"),
		eleDuration	:$("#editDuration"),
};
var deleteModal={
		modal : $('#mdlDelShikshaPlusChapter'),
		confirm : $('#btnDelShikshaPlusChapter'),
};
var self;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');

var chapterFilterObj = {
	toolBar : "#toolbarShikshaPlusChapter",
	levelSelect : "#filterBox_levels",
	groupSelect : "#filterBox_groups",
	searchBox : "#filter-search",
	searchBtn : "#filter-search-btn",
	init : function(){
		shikshaPlus.fnGetLevelsForChapterFilter(this.levelSelect,0);
		shikshaPlus.fnGetGroupsForChapterFilter(this.groupSelect,0);
		this.enableMultiSelectForFilters(this.levelSelect);
		this.enableMultiSelectForFilters(this.groupSelect);
		$(this.searchBtn).on("click",this.filterData);
		$(this.toolBar).show();
	},
	fiterQueryParmams : function(params){
		params['levelIds'] = chapterFilterObj.fnGetSelectedFiteredValue(chapterFilterObj.levelSelect);
		params['groupIds'] = chapterFilterObj.fnGetSelectedFiteredValue(chapterFilterObj.groupSelect);
		params['search'] = $(chapterFilterObj.searchBox).val();
		return params;
	},
	fnGetSelectedFiteredValue : function(ele){
		var arr =[];
		var selector = "option" ;
		if($(ele).val()!=null)
			selector = "option:selected";
		$(ele).find(selector).each(function(ind,option){
			if($(option).val()!="multiselect-all"){		
				arr.push(parseInt($(option).val()));
			}
		});
		return arr;
	},
	filterData : function(){
		$table.bootstrapTable("refresh");
	},
	formatShowingRows : function(pageFrom,pageTo,totalRows){
		return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
	},
	actionFormater : function(){
		return {
			classes:"dropdown dropdown-td"
		}
	},
	enableMultiSelectForFilters : function(element){
		$(element).multiselect({
			maxHeight: 325,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,	
			filterPlaceholder: 'Search here...',
			onChange : chapterFilterObj.filterData
		});	
	},
};

var chapterIdDelete;
var shikshaPlusChapter ={
		levelId:0,
		groupId:0,
		chpaterId:0,
		init:function(){
			self =shikshaPlusChapter;
			pageContextElements.addBtn.on('click',self.initAddModal);
			deleteModal.confirm.on('click',shikshaPlusChapter.deleteFunction);
			$(addModal.eleLevel).on('change',shikshaPlusChapter.showHideGroups);
			$(editModal.eleLevel).on('change',shikshaPlusChapter.changeLevels);
		},

		refreshAddModalForm: function(){
			addModal.form.resetForm();
		},

		initAddModal :function(type){
			fnInitSaveAndAddNewList();
			shikshaPlusChapter.resetFormById("#addShikshaPlusChapterForm");	
			addModal.modal.modal("show");
			shikshaPlus.fnGetLevels(addModal.eleLevel,self.levelId);
			setOptionsForMultipleSelect(addModal.eleLevel); 
			$(addModal.eleLevel).multiselect("refresh");
			$(addModal.eleGroupDiv).hide();
		},
		showHideGroups: function(){
			var levId=$(addModal.eleLevel).val();
			$(addModal.eleGroup).multiselect("destroy");
			shikshaPlus.fnGetGroupsByLevelId(addModal.eleGroup,self.groupId,levId);
			setOptionsForMultipleSelect(addModal.eleGroup);
			$(addModal.eleGroup).multiselect("rebuild");
			$(addModal.eleGroupDiv).show();
		},
		changeLevels: function(){
			var levId=$(editModal.eleLevel).val();
			shikshaPlus.fnGetGroupsByLevelId(editModal.eleGroup,self.groupId,levId);
			//setOptionsForMultipleSelect(editModal.eleGroup);
			$(editModal.eleGroup).multiselect("rebuild");
		},
		deleteFunction : function(){
			shikshaPlusChapter.fnDelete(chapterIdDelete);
		},
		resetForm : function(){
			addModal.form.trigger("reset");
		},
		resetFormById : function(formId) {
			$(formId).find('#alertdiv').hide();
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var levelObj = {levelId : $(addModal.eleLevel).val()};
			
			var groupObj = {groupId : $(addModal.eleGroup).val()};
				
			var ajaxData={
					"chapterNumber": addModal.eleChapterNo.val(),
					"name":	addModal.eleName.val(),
					"description":addModal.eleDescription.val(),
					"level"	:levelObj,
					"group"	:groupObj,
					/*"duration"	:addModal.eleDuration.val(),*/
			};
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/chapter", ajaxData, "POST");

			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == "addShikshaPlusChapterSaveButton"){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type : "success",
							title : shikshaPlusChapterlMessages.sucessAlert,
							body : addAjaxCall.responseMessage,
							close :'auto'
						})
					}
					if(submitBtnId == "addShikshaPlusChapterSaveMoreButton"){
						$(addModal.modal).modal("show");
						$(addModal.eleGroupDiv).hide();
						$(addModal.modal).find("#divSaveAndAddNewMessage").show();
						fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					}
					shikshaPlusChapter.resetForm(addModal.form);
					$(addModal.eleLevel).multiselect("refresh");
					$(addModal.eleGroup).multiselect("refresh");
					shikshaPlusChapter.refreshTable(pageContextElements.table);
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(chapterId){
            shikshaPlusChapter.resetFormById("#editShikshaPlusChapterForm");	
			$(editModal.modal).modal("show");
			$(editModal.eleLevel).multiselect('destroy');
			$(editModal.eleGroup).multiselect('destroy');
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/chapter/"+chapterId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					editModal.eleId.val(editAjaxCall.chapterId);
					editModal.eleChapterNo.val(editAjaxCall.chapterNumber);
					editModal.eleName.val(editAjaxCall.name);
					editModal.eleDescription.val(editAjaxCall.description);
					/*editModal.eleDuration.val(editAjaxCall.duration);*/
					shikshaPlus.fnGetLevels(editModal.eleLevel,editAjaxCall.level.levelId);
					shikshaPlus.fnGetGroupsByLevelId(editModal.eleGroup,editAjaxCall.group.groupId,editAjaxCall.level.levelId);
					setOptionsForMultipleSelect(editModal.eleLevel);
					setOptionsForMultipleSelect(editModal.eleGroup);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var levelObj = {levelId : editModal.eleLevel.val()};
			var groupObj = {groupId : editModal.eleGroup.val()};
			var ajaxData={
					"chapterId":editModal.eleId.val(),
					"chapterNumber":editModal.eleChapterNo.val(),
					"name"			:editModal.eleName.val(),
					"description":editModal.eleDescription.val(),
					"level"	: levelObj,
					"group"	: groupObj,
					/*"duration"	:editModal.eleDuration.val(),*/
			}

			var updateAjaxCall = shiksha.invokeAjax("shikshaplus/chapter", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					shikshaPlusChapter.resetForm(editModal.form);
					shikshaPlusChapter.refreshTable(pageContextElements.table);
					AJS.flag({
						type : "success",
						title : shikshaPlusChapterlMessages.sucessAlert	,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				},function(){
					window.location.reload();
				})
			}
		},

		initDelete : function(chapterId,name){
			var msg = $("#deleteShikshaPlusChapterMessage").data("message");
			$("#deleteShikshaPlusChapterMessage").html(msg.replace('@NAME@',name));
			deleteModal.modal.modal("show");
			chapterIdDelete=chapterId;
		},
		fnDelete :function(chapterId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/chapter/"+chapterId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					shikshaPlusChapter.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : shikshaPlusChapterlMessages.sucessAlert,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					level:{
						required: function(){
							var option = $(addModal.eleLevel).val();
							return (option != 0);
						},
					},
					group: {
						required: true,
					},
					chapterNumber: {
						required: true,
						noSpace : true,
						digits : true,
						min: 1,
						max: 1000
					},
					name: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace: true
					},
					/*duration:{
						required: true,
					},*/
					description: {
						required: true,
						minlength: 1,
						maxlength: 5000,
						noSpace: true
					}
				},

				messages: {
					level:{
						required: shikshaPlusChapterlMessages.levelRequired,
					},
					group: {
						required: shikshaPlusChapterlMessages.groupRequired,
					},
					chapterNumber: {
						required:  shikshaPlusChapterlMessages.chapterNumberRequired,
						noSpace:   shikshaPlusChapterlMessages.chapterNumberNoSpace,
						digits:    shikshaPlusChapterlMessages.chapterNumberDigits,
						min  : shikshaPlusChapterlMessages.chapterMinNumber,
						 
					},
					name: {
						required: shikshaPlusChapterlMessages.chapterNameRequired,
						noSpace: shikshaPlusChapterlMessages.chapterNameNoSpace,
						minlength: shikshaPlusChapterlMessages.chapterNameMinLength,
						maxlength: shikshaPlusChapterlMessages.chapterNameMaxLength,

					},
					/*duration:{
						required: shikshaPlusChapterlMessages.durationRequired,
					},*/
					description: {
						required: shikshaPlusChapterlMessages.descriptionRequired,
						noSpace: shikshaPlusChapterlMessages.descriptionNoSpace,
						minlength: shikshaPlusChapterlMessages.descriptionMinLength,
						maxlength: shikshaPlusChapterlMessages.descriptionMaxLength,

					}
				},
				errorPlacement: function(error, element) {
				    if($(element).hasClass("select")){
				    	$(element).parent().append(error);
				    } else {
				    	$(element).after(error);
				    }
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						shikshaPlusChapter.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						shikshaPlusChapter.fnUpdate();
					}
				}
			});
		},
		shikshaPlusChapterActionFormater: function(value, row, index) {						
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="shikshaPlusChapter.initEdit(\''+row.chapterId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="shikshaPlusChapter.initDelete(\''+row.chapterId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		shikshaPlusChapterNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	shikshaPlusChapter.init();
	chapterFilterObj.init();
	$table = pageContextElements.table.bootstrapTable({
		toolbar: "#toolbarShikshaPlusChapter",
		url : "shikshaplus/chapter/filter/page",
		method : "post",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: true,
		pagination: true,
		pageSize: 20,
		formatShowingRows : chapterFilterObj.formatShowingRows,
		clickToSelect: false,
		queryParamsType :'limit',
		queryParams : chapterFilterObj.fiterQueryParmams
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");

	shikshaPlusChapter.formValidate(addModal.form);
	shikshaPlusChapter.formValidate(editModal.form);
	
	$submitActors.click(function() {
		submitActor = this;
	});
	jQuery(addModal.eleChapterNo).on('keydown', function(e) {
		console.log(e.which);
		  if((e.which != 8) && (e.which != 46) && (e.which >57) && !(e.which <48))
		  {
				  e.preventDefault(); 
		  }
		});
	jQuery(editModal.eleChapterNo).on('keydown', function(e) {		
		  if((e.which != 8) && (e.which != 46) && !(e.which <48) && (e.which >57)){
			  e.preventDefault(); 
		  }
		});
	$.validator.messages.max =appMessgaes.maxValue;
});
