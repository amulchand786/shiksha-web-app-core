var pageContextElements = {	
		addButton 						: '#addNewHelp',
		table 							: '#helpTable',
		toolbar							: "#toolbarHelp"
};
var addModal ={
		form 			: '#addHelpForm',
		modal   		: '#mdlAddHelp',
		saveMoreButton	: 'addHelpSaveMoreButton',
		saveButton		: 'addHelpSaveButton',
		eleType			: '#addType',
		eleDescription		: '#addDescription',
		message		    : "#divSaveAndAddNewMessage"
};
var editModal ={
			form	 : '#editHelpForm',
			modal	 : '#mdlEditHelp',
			eleId	 : '#helpId',
			eleType		: '#editType',
			eleDescription		: '#editDescription'
			
};
var deleteModal ={
			modal	: '#mdlDeleteHelp',
			confirm : '#deleteHelpButton',
			message	: "#deleteHelpMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var helpIdForDelete;
var helpFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var help={
		name:'',
		helpId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',help.initAddModal);
			$(deleteModal.confirm).on('click',help.deleteFunction);

		},
		deleteFunction : function(){
			help.fnDelete(helpIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			help.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"type" :$(addModal.eleType).val(),
					"description"   : $(addModal.eleDescription).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("help", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : helpMessages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					help.resetForm(addModal.form);
					help.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.type);	

				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(helpId){
			help.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("help/"+helpId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleType).val(editAjaxCall.type);
					$(editModal.eleDescription).val(editAjaxCall.description);
					$(editModal.eleId).val(editAjaxCall.helpId);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"helpId"		:$(editModal.eleId).val(),
					"type" 			:$(editModal.eleType).val(),
					"description"   : $(editModal.eleDescription).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("help", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					help.resetForm(editModal.form);
					help.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title : helpMessages.sucessAlert,
						body  : updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(helpId,type){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',type));
			$(deleteModal.modal).modal("show");
			helpIdForDelete=helpId;
		},
		fnDelete :function(helpId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("help/"+helpId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					help.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : helpMessages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					type: {
						required: true,
						noSpace:true,
						minlength: 1,
						maxlength: 255,
						
					},
					description: {
						required: true,
						noSpace:true,
						minlength: 1,
						maxlength: 1500,
						
					},
						
				},

				messages: {
					type: {
						required : helpMessages.typeRequired,
						noSpace  : helpMessages.typeNoSpace,
						minlength: helpMessages.typeMinLength,
						maxlength: helpMessages.typeMaxLength,

					},
					description: {
						required : helpMessages.descriptionRequired,
						noSpace  : helpMessages.descriptionNoSpace,
						minlength: helpMessages.descriptionMinLength,
						maxlength: helpMessages.descriptionMaxLength,
						
					},
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						help.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						help.fnUpdate();
					}
				}
			});
		},
		helpActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="help.initEdit(\''+ row.helpId+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="help.initDelete(\''+ row.helpId+ '\',\''+ row.type+ '\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		helpNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	help.formValidate(addModal.form);
	help.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	help.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar,
		url : "help/list",
		method : "post",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: false,
		queryParamsType :'limit',
		//formatShowingRows : helpFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});