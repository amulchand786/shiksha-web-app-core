//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForCity =[];
var allInputEleNamesOfFilter =[];

var btnAddMoreCityId;
var deleteCityId;
var deleteCityName;
var thLabels;
var elementIdMap;
var selectedIds;


//initialize global variables
var initGloabalVarsCity =function(){
	resetButtonObj='';
	allInputEleNamesForCity=['addCityName'];
	allInputEleNamesOfFilter=['stateId','zoneId','districtId','tehsilId'];
	btnAddMoreCityId ='';
	deleteCityId='';
}



//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}


//create a row after save
var getCityRow =function(city){
	var row = 
		"<tr id='"+city.cityId+"' data-id='"+city.cityId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+city.stateId+"'	title='"+city.stateName+"'>"+city.stateName+"</td>"+
		"<td data-zoneid='"+city.zoneId+"'	title='"+city.zoneName+"'>"+city.zoneName+"</td>"+
		"<td data-districtid='"+city.districtId+"' title='"+city.districtName+"' >"+city.districtName+"</td>"+
		"<td data-tehsilid='"+city.tehsilId+"' title='"+city.tehsilName+"' >"+city.tehsilName+"</td>"+
		"<td data-citytid='"+city.cityId+"' title='"+city.cityName+"' >"+city.cityName+"</td>"+
		"<td data-citycode='"+city.cityCode+"' title='"+city.cityCode+"'>"+city.cityCode+"</td>"+		

		"<td><div class='div-tooltip' ><a class='tooltip tooltip-top'	  id='btnEditCity'" +
		"onclick=editCityData(&quot;"+city.stateId+"&quot;,&quot;"+city.zoneId+"&quot;,&quot;"+city.districtId+"&quot;,&quot;"+city.tehsilId+"&quot;,&quot;"+city.cityId+"&quot;,&quot;"+escapeHtmlCharacters(city.cityName)+"&quot;);   ><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a style='margin-left:10px;' class='tooltip tooltip-top' id=deleteCity	onclick=deleteCityData(&quot;"+city.cityId+"&quot;,&quot;"+escapeHtmlCharacters(city.cityName)+"&quot;); ><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	
	return row;
}

//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddCity #statesForZone',
			"Zone" : '#mdlAddCity #selectBox_zonesByState',
			"District" : '#mdlAddCity #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddCity #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddSchool #selectBox_blocksByTehsil',
		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
			"State" : '#mdlEditCity #statesForZone',
			"Zone" : '#mdlEditCity #selectBox_zonesByState',
			"District" : '#mdlEditCity #selectBox_districtsByZone',
			"Tehsil" : '#mdlEditCity #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddSchool #selectBox_blocksByTehsil',
		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};


var applyPermissions=function(){
	if (!editCityPermission){
	$("#btnEditCity").remove();
		}
	if (!deleteCityPermission){
	$("#deleteCity").remove();
		}
	}

// smart filter utility--- start here

var initSmartFilter = function(type) {
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){
		
		resetFormValidatonForLocFilters("addCityForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addCityForm",allInputEleNamesForCity);
		displaySmartFilters("add");
		fnCollapseMultiselect();
	}
	else
	{		
		
	resetFvForAllInputExceptLoc("editCityForm",['editCityName']);
	displaySmartFilters("edit")
	fnCollapseMultiselect();
	}
}



//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	if($(resetButtonObj).parents("form").attr("id")=="addCityForm"){
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");
	
		resetFormValidatonForLocFilters("addCityForm",allInputEleNamesOfFilter);
	}else{
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
		
		resetFormValidatonForLocFilters("addCityForm",allInputEleNamesOfFilter);
	}
	fnCollapseMultiselect();
	
	
}


// reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addCityForm');
	resetFormById('editCityForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}


// smart filter utility--- end here

// perform CRUD

// add City

var addCity = function(event) {
	$('.outer-loader').show();
	var tehsilId = $('#mdlAddCity #selectBox_tehsilsByDistrict').val();
	var cityName = $('#mdlAddCity #addCityName').val().trim().replace(/\u00a0/g," ");
	var json = {
		"tehsilId" : tehsilId,
		"cityName" : cityName,
	};
	
	
	
	//update data table without reloading the whole table
	//save&add new
	btnAddMoreCityId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var city = customAjaxCalling("city", json, "POST");
	if(city!=null){
	if(city.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		var newRow =getCityRow(city);
		appendNewRow("tblCity",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#tblCity',[2,4],8,[0,7],thLabels);
		setShowAllTagColor(colorClass);

		if(btnAddMoreCityId=="addCitySaveMoreButton"){
			$("#mdlAddCity").find('#errorMessage').hide();

		resetFvForAllInputExceptLoc("addCityForm",allInputEleNamesForCity);
			fnDisplaySaveAndAddNewElement('#mdlAddCity',cityName);
		}else{			
			$('#mdlAddCity').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: validationMsg.sucessAlert,
				    body: '<p>'+ city.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
					
		}		
	}else {
		showDiv($('#mdlAddCity #alertdiv'));
		$("#mdlAddCity").find('#successMessage').hide();
		$("#mdlAddCity").find('#okButton').hide();
		$("#mdlAddCity").find('#errorMessage').show();
		$("#mdlAddCity").find('#exception').show();
		$("#mdlAddCity").find('#buttonGroup').show();
		$("#mdlAddCity").find('#exception').text(city.responseMessage);
	}
	}
	$(".outer-loader").hide();
};




// edit
// get data for edit
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editTehsilId = 0;
var editCityId = 0;
var editCityData = function(stateId, zoneId, districtId, tehsilId, cityId,
		cityName) {
	$('.editResetFilterDivForCity').hide();
	initSmartFilter("edit");
	var idMap = {
		'#mdlEditCity #statesForZone' : stateId,
		'#mdlEditCity #selectBox_zonesByState' : zoneId,
		'#mdlEditCity #selectBox_districtsByZone' : districtId,
		'#mdlEditCity #selectBox_tehsilsByDistrict' : tehsilId,
	};

	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
							.prop('selected', true);
				enableMultiSelect(ele_key);
			});

	$('#mdlEditCity #editCityName').val(cityName);
	editCityId = cityId;
	$("#mdlEditCity").modal().show();
	fnCollapseMultiselect();
};

// edit city

var editCity = function() {
	$('.outer-loader').show();
	var tehsilId = $('.editCity #selectBox_tehsilsByDistrict').val();
	var cityId = editCityId;
	var cityName = $('#editCityName').val().trim().replace(/\u00a0/g," ");
	var json = {
			"tehsilId" : tehsilId,
			"cityId" : cityId,
			"cityName" : cityName,
	};

	var city = customAjaxCalling("city", json, "PUT");
	if(city!=null){
	if(city.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("tblCity",city.cityId);
		var newRow =getCityRow(city);
		appendNewRow("tblCity",newRow);
		applyPermissions();
		fnUpdateDTColFilter('#tblCity',[2,4],8,[0,7],thLabels);		
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	

		$('#mdlEditCity').modal("hide");
		
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: validationMsg.sucessAlert,
			    body: '<p>'+ city.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
		
	}else {
		showDiv($('#mdlEditCity #alertdiv'));
		$("#mdlEditCity").find('#successMessage').hide();
		$("#mdlEditCity").find('#errorMessage').show();
		$("#mdlEditCity").find('#exception').show();
		$("#mdlEditCity").find('#buttonGroup').show();
		$("#mdlEditCity").find('#exception').text(city.responseMessage);
	}
	}
	$(".outer-loader").hide();
};
	

// getting data for delete
var deleteCityData = function(cityId, cityName) {
	deleteCityName = cityName;
	deleteCityId = cityId;
	$('#deleteCityMessage').text(columnMessages.deleteConfirm.replace("@NAME", cityName) + " ?");
	$("#mdlDelCity").modal().show();
	hideDiv($('#mdlDelCity #alertdiv'));

};

// delete city
$('#deleteCityButton').click(function() {

	$('.outer-loader').show();
	

	var city = customAjaxCalling("city/"+deleteCityId, null, "DELETE");
	if(city!=null){
	if(city.response=="shiksha-200"){
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("tblCity",deleteCityId);
		fnUpdateDTColFilter('#tblCity',[2,4],8,[0,7],thLabels);			
		setShowAllTagColor(colorClass);
		$('#mdlDelCity').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: validationMsg.sucessAlert,
			    body: '<p>'+ city.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
			
	} else {
		showDiv($('#mdlDelCity #alertdiv'));
		$("#mdlDelCity").find('#errorMessage').show();
		$("#mdlDelCity").find('#exception').text(city.responseMessage);
		$("#mdlDelCity").find('#buttonGroup').show();
	}
	}
	$(".outer-loader").hide();
	enableTooltip();
});


// form validation

$(function() {
		
	thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.district,columnMessages.tehsil,columnMessages.city,columnMessages.cityCode,columnMessages.action];
	fnSetDTColFilterPagination('#tblCity',8,[0,7],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tblCity',[2,4]);
	});
	colorClass = $('#t0').prop('class');
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$( "#addCityName" ).keypress(function() {		
		hideDiv($('#mdlAddCity #alertdiv'));
	})
		$( "#editCityForm" ).keypress(function() {		
		hideDiv($('#mdlEditCity #alertdiv'));
	})
	initGloabalVarsCity();
	$("#addCityForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
						
							addCityName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.city+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                           callback : function(value) {
                                        	   var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addCity(e);

			});

	$("#editCityForm")
			.formValidation(
					{ 
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							editCityName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.city+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				editCity();

			});
});
