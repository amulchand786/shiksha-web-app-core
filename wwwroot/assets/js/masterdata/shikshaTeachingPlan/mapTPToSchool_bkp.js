var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		grade	: "#divSelectGrade",
		section	: "#divSelectSection",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		eleGrade			: "#selectBox_grade",
		eleSection			: "#selectBox_Section",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
		divFilter   		: '#divFilter',
		eleLocType  		: '#divSchoolLocType input:radio[name=locType]',
		filter				: '#locColspFilter',
		panelLink			: '#locationPnlLink',
		form				: '#listDeviceForm',
		eleCollapseFilter	: '#collapseFilter',
		eleToggleCollapse	: '[data-toggle="collapse"]',
		resetModal 			: '#mdlResetLoc',
		resetConfirm 		: '#btnResetLocYes',
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var locationUtility={
		fnInitLocation :function(){
			$(locationPanel.divFilter).css('display','none');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(locationPanel.resetModal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			$(locationPanel.eleGrade).multiselect('destroy').multiselect('refresh');
			$(locationPanel.eleSection).multiselect('destroy').multiselect('refresh');
			$(locationPanel.section).hide();
			$(locationPanel.grade).hide();
			
			if ($(resetButtonObj).parents("form").attr("id") == 'locationFilterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode = $(mapFilter.locTypeSelect).val();
				var aLLevelName =$(mapFilter.locTypeSelect).find("option:selected").data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
			}$(locationPanel.school).val()
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){			
				elementIdMap = {
						"State" 	: '#statesForZone',
						"Zone" 		: '#selectBox_zonesByState',
						"District" 	: '#selectBox_districtsByZone',
						"Tehsil" 	: '#selectBox_tehsilsByDistrict',
						"Block" 	: '#selectBox_blocksByTehsil',
						"City" 		: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" 	: '#selectBox_villagebyRv',
				};
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));				
				locationUtility.reinitializeSmartFilter(elementIdMap);				
				var lLocTypeCode = $(mapFilter.locTypeSelect).val();
				var lLLevelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');				
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));				
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(
						function(ind, ele_key) {
							$(elementIdMap[ele_key]).find("option:selected").prop(
									'selected', false);
							$(elementIdMap[ele_key]).multiselect('destroy');
							enableMultiSelect(elementIdMap[ele_key]);
						});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#btnsRow,#divFilter').show();
				}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});			
			$('#rowDiv1,#divFilter').show();			
			spinner.hideSpinner();
		},
};

var mapFilter = {
		locTypeSelect : "#filterBox_locType",
		villageDiv : "#filter-villagestDiv",
		villageSelect : "#filterBox_village",
		citySelect : "#filterBox_city",
		cityDiv : "#filter-citytDiv",
		schoolDiv : "#filter-schoolDiv",
		schoolSelect : "#filterBox_school",
		gradeSelect : "#filterBox_grade",
		gradeDiv : "#filter-gradeDiv",
		filterIcon : "#smart-filter",
		locFilteModal : "#mdlLocationFilter",
		applyBtn : "#locFilterApplyBtn",
		init : function(){
			this.enableMultiSelectForFilters(this.locTypeSelect);
			this.enableMultiSelectForFilters(this.villageSelect);
			this.enableMultiSelectForFilters(this.citySelect);
			this.enableMultiSelectForFilters(this.schoolSelect);
			this.enableMultiSelectForFilters(this.gradeSelect);
			
			$(this.locTypeSelect).on("change",this.locaTypeChange);
			$(this.filterIcon).on("click",this.smartFilterInit);
			
			$("#selectBox_cityByBlock").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$("#selectBox_villagebyRv").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$(this.villageSelect).on("change",mapFilter.showSchools);
			$(this.citySelect).on("change",mapFilter.showSchools);
			$(this.schoolSelect).on("change",mapFilter.showGrades);
			$(this.gradeSelect).on("change",mapperObj.showSubjects);
			$(mapFilter.applyBtn).on("click",function(){
				if($(mapFilter.locTypeSelect).val() == "R"){
					$(mapFilter.villageSelect).val($("#selectBox_villagebyRv").val());
					$(mapFilter.villageSelect).multiselect("rebuild");
				} else {
					$(mapFilter.citySelect).val($("#selectBox_cityByBlock").val());
					$(mapFilter.citySelect).multiselect("rebuild");
				}
				$(mapFilter.schoolDiv).addClass("hide");
				$(mapFilter.gradeDiv).addClass("hide");
				$(mapperObj.subjectListContainer).hide();
				$(mapFilter.locFilteModal).modal("hide");
				mapFilter.showSchools();
			});
		},
		smartFilterInit : function(){
			$(mapFilter.applyBtn).hide();
			mapFilter.setLocations();
			$(mapFilter.locFilteModal).modal("show");
		},
		locaTypeChange : function(){
			if($(this).val() == "R"){
				$(mapFilter.cityDiv).addClass("hide");
				$(mapFilter.villageDiv).removeClass("hide");
				$(mapFilter.villageSelect).val("");
				$(mapFilter.villageSelect).multiselect("rebuild");
			} else {
				$(mapFilter.cityDiv).removeClass("hide");
				$(mapFilter.villageDiv).addClass("hide");
				$(mapFilter.citySelect).val("");
				$(mapFilter.citySelect).multiselect("rebuild");
			}
			$(mapFilter.schoolDiv).addClass("hide");
			$(mapFilter.gradeDiv).addClass("hide");
			$(mapperObj.subjectListContainer).hide();
		},
		setLocations : function(){
			$(locationPanel.filter).removeClass('in');
			$(locationPanel.panelLink).text('Select Location');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
			var typeCode =$(this.locTypeSelect).val();	
			var locCode = $(mapFilter.locTypeSelect).val()
			var levelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');
			if(typeCode=='U'){
				showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("U");
				fnCollapseMultiselect();
			}else{
				hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("R");
				fnCollapseMultiselect();						
			}
			$(locationPanel.eleCollapseFilter).css('display','block');
			$(locationPanel.grade).hide();
			$(locationPanel.section).hide();
		},
		showSchools : function(){
			$(mapFilter.schoolSelect).empty();
			var locType = "R";
			var locationId = $(mapFilter.locTypeSelect).val() == "R" ? $(mapFilter.villageSelect).val() : $(mapFilter.citySelect).val();
			var ajaxData = {
				locations : [locationId]	
			};
			if($(mapFilter.locTypeSelect).val() == "U"){
				locType = "U";
			}
			var getAjaxCall = shiksha.invokeAjax("school/"+locType, ajaxData, "POST");
			
			if(getAjaxCall.length){
				if(getAjaxCall[0].response == "shiksha-200"){
					$.each(getAjaxCall,function(index,school){
						$(mapFilter.schoolSelect).append("<option value='"+school.id+"'>"+school.schoolName+"</option>")
					});
					
				} 
			} 
			$(mapFilter.schoolSelect).multiselect("rebuild");
			$(mapFilter.schoolDiv).removeClass("hide");
			$(mapFilter.gradeDiv).addClass("hide");
			$(mapperObj.subjectListContainer).hide();
			spinner.hideSpinner();
		},
		showGrades : function(){
			bindGradesBySchoolToListBox($(mapFilter.gradeSelect),$(mapFilter.schoolSelect).val(),0);
			$(mapFilter.gradeSelect).multiselect("rebuild");
			$(mapFilter.gradeDiv).removeClass("hide");
			$(mapperObj.subjectListContainer).hide();
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
			});	
		}
		
};

var mapperObj = {
		
		subjectListUL : "#subjectListUL",
		subjectLi : ".mapper-subject",
		subjectListContainer : "#subjectlist-container",
		sectionMapContainer  :"#sectionMapContainer",
		sectionMapContainerMain :"#sectionMapContainerMain",
		mapSaveBtn : "#mapSaveBtn",
		init : function(){
			$(document).on("click",this.subjectLi,this.showSubjectSectionMapping);
			$(this.mapSaveBtn).on("click",this.fnSaveMappers);
		},
		showSubjectSectionMapping : function(){
			$(mapperObj.sectionMapContainer).empty();
			$(this).siblings().removeClass("active");
			$(this).addClass("active");
			var subjectId = $(this).data("subjectid");
			var schoolId = $(mapFilter.schoolSelect).val();
			var gradeId = $(mapFilter.gradeSelect).val();
			var teachingPlanList = shiksha.invokeAjax("shiksha/teachingPlan/grade/"+gradeId+"/subject/"+subjectId, null, "GET");
			var ajaxData = {
					"school":{
						"id":schoolId
						},
						"grade":{
						"gradeId":gradeId
						},
						"subject":{
						"subjectId":subjectId
						}
			}
			var getAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/map", ajaxData, "POST");
			console.log(getAjaxCall);
			var mainTemp = $(".map-template").clone();
			$(mainTemp).removeClass("map-template");
			$.each(teachingPlanList,function(index,tplan){
				$(mainTemp).find(".tplan-select").append("<option data-name='"+tplan.name+"' value='"+tplan.id+"'>"+tplan.name+"</option>");
			});
			$.each(getAjaxCall,function(index,mapper){
				var template = $(mainTemp).clone();
				$(template).show();
				$(template).attr("data-sectionid",mapper.section.sectionId);
				$(template).attr("data-sectionname",mapper.sectionName).find(".section-title").text(mapper.section.sectionName);
				$(template).find(".tplan-select").attr("id","tplanSelectSection"+mapper.section.sectionId);
				$(mapperObj.sectionMapContainer).append($(template));
				$("#tplanSelectSection"+mapper.section.sectionId).val(mapper.shikshaTeachingPlanId);
				mapFilter.enableMultiSelectForFilters("#tplanSelectSection"+mapper.section.sectionId);
			});
			
			$(mapperObj.sectionMapContainerMain).show();
			spinner.hideSpinner();
		},
		showSubjects : function(){
			$(mapperObj.sectionMapContainerMain).hide();
			$(mapperObj.subjectListUL).empty();
			spinner.showSpinner();
			var schoolId = $(mapFilter.schoolSelect).val();
			var gradeId = $(mapFilter.gradeSelect).val();
			var getAjaxcall = shiksha.invokeAjax("subject/school/"+schoolId+"/grade/"+gradeId+"/", null, "GET");
			$.each(getAjaxcall,function(index,subject){
				var schoolTemp =' <li class="lms-teacher-school-item mapper-subject" data-subjectid="'+subject.subjectId+'"><div class="lms-side-center-far">'
								+'<div class="lms-teacher-school-text">'+subject.subjectName+'</div><div class="lms-side-center" style="display:none;"><a href="#" class="lms-close-icon">'
				 				+'<svg><use xlink:href="web-resources/images/teacher.svg#close" href="web-resources/images/teacher.svg#close"></use></svg></a>'
				 				+'<a href="#" class="lms-expand-icon"><svg><use xlink:href="web-resources/images/teacher.svg#expand" href="web-resources/images/teacher.svg#expand"></use></svg></a></div></div></li>';
							$(mapperObj.subjectListUL).append(schoolTemp);
			});
			$(mapperObj.subjectListContainer).show();
			spinner.hideSpinner();
		},
		fnSaveMappers : function(){
			var ajaxData = [];
			$(mapperObj.sectionMapContainer).find(".section-map-row").each(function(index,row){
				var sectionObj = {
						section : {
							sectionId : $(this).data("sectionid"),
							sectionName : $(this).data("sectionname")
						},
						shikshaTeachingPlanId  : $(this).find(".tplan-select").val(),
						shikshaTeachingPlanName  : $(this).find(".tplan-select option:selected").data("name")
				};
				ajaxData.push(sectionObj);
			});
			var schoolId = $(mapFilter.schoolSelect).val();
			var gradeId = $(mapFilter.gradeSelect).val();
			var saveAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/school/"+schoolId+"/grade/"+gradeId+"/map", ajaxData, "POST");
			
			if(saveAjaxCall != null){	
				if(saveAjaxCall.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title : "Success!",
						body : saveAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : saveAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		}
		
};


$(document).ready(function(){
	mapFilter.init();
	mapperObj.init();
});