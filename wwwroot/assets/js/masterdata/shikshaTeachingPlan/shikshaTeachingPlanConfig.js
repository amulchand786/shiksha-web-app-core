var $table;
var submitActor = null;
var teachingPlanFilterObj = {
		toolBar : "#toolbarTeachingPlan",
		acYear : "#acYear",
		gradeSelect : "#filterBox_grade",		
		init : function(){
			this.enableMultiSelectForFilters(this.gradeSelect);
			
			this.enableMultiSelectForFilters(this.acYear);
			$(this.toolBar).show();
			
		},
		fiterQueryParmams : function(params){
			params['acdemicYearId'] = $(teachingPlanFilterObj.acYear).val();
			params['gradeIds'] = teachingPlanFilterObj.fnGetSelectedFiteredValue(teachingPlanFilterObj.gradeSelect);
		
			return params;
		},
		fnGetSelectedFiteredValue : function(ele){
			var arr =[];
			var selector = "option" ;
			if($(ele).val()!=null)
				selector = "option:selected";
			$(ele).find(selector).each(function(ind,option){
				if($(option).val()!="multiselect-all"){		
					arr.push(parseInt($(option).val()));
				}
			});
			return arr;
		},
		filterData : function(){
			$table.bootstrapTable("refresh");
		},
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				onChange : teachingPlanFilterObj.filterData
			});	
		},
	};

var teachingPlanConfig = {
	table : "#teachingPlanTable",
	addForm : "#addTeachingPlanForm",
	addModal : "#mdlAddTeachingPlan",
	addBtn : "#addTeachingPlan",
	addGrade : "#addGrade",
	
	addName : "#addTeachingPlanName",
	
	
	editForm : "#editTeachingPlanForm",
	editModal : "#mdlEditTeachingPlan",
	editGrade : "#editGrade",
	
	editName : "#editTeachingPlanName",
	
	editTeachingPlanId  : "#editTeachingPlanId",
	
	deleteModal : "#mdlDelTeachingPlan",
	deleteConfirmBtn : "#btnDelTeachingPlan",
	deleteTeachingPlanId : "#deleteTeachingPlanId",
	
	init : function(){
		shikshaPlus.fnGetGrades(this.addGrade,0);
		shikshaPlus.fnGetGrades(this.editGrade,0);
		setOptionsForMultipleSelect(this.addGrade);
		setOptionsForMultipleSelect(this.editGrade);
		
		$(this.addBtn).on("click",this.initAdd);
		$(this.deleteConfirmBtn).on("click",this.fnDelete);
		
		this.formValidate(this.addForm);
		this.formValidate(this.editForm);
		
	},

	formValidate : function(validateFormName){
		$(validateFormName).submit(function(e) {
			e.preventDefault();
		}).validate({
			ignore: '',
			rules: {
				grade:{
					required: true,
				},
				
				name: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace: true
				},
			},

			messages: {
				grade:{
					required: "Please select Grade",
				},
				
				name: {
					required: "Please enter teaching plan name",
					noSpace: "Please enter teaching plan name",
					minlength: "Teaching plan name must be 2 characters and not more then 50",
					maxlength: "Teaching plan name must be 2 characters and not more then 50",

				},
			},
			errorPlacement: function(error, element) {
				//$(element).parent("div").addClass("has-error");
			    if($(element).hasClass("select")){
			    	$(element).parent().append(error);
			    } else {
			    	$(element).after(error);
			    }
			},
			submitHandler : function(){
				if(validateFormName === teachingPlanConfig.addForm){
					teachingPlanConfig.fnAdd(submitActor.id);
				}
				if(validateFormName === teachingPlanConfig.editForm){
					teachingPlanConfig.fnEdit();
				}
			}
		});
	},
	initAdd : function(){
		fnInitSaveAndAddNewList();
		teachingPlanConfig.resetFormById(teachingPlanConfig.addForm);
		$(teachingPlanConfig.addGrade).multiselect("refresh");
		$(teachingPlanConfig.addModal).modal("show");
	},
	fnAdd : function(submitBtnId){
		spinner.showSpinner();			
		var ajaxData={
				grade : {
					gradeId : $(teachingPlanConfig.addGrade).val()
				},
				name  : $(teachingPlanConfig.addName).val()
		};
		var addAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan", ajaxData, "POST");

		if(addAjaxCall != null){
			if(addAjaxCall.response == "shiksha-200"){
				if(submitBtnId == "addTeachingPlanSaveButton"){
					$(teachingPlanConfig.addModal).modal("hide");
					AJS.flag({
						type : "success",
						title : "Success!",
						body : addAjaxCall.responseMessage,
						close :'auto'
					})
				}
				window.location.href =baseContextPath+"/shiksha/teachingPlan/"+addAjaxCall.id+"/details/view";
			}else if(addAjaxCall.response == "shiksha-800"){
				spinner.hideSpinner();
				AJS.flag({
					type : "error",
					title : "Error..!",
					body : addAjaxCall.responseMessages,
					close :'manual'
				});
			}else {
				spinner.hideSpinner();
				AJS.flag({
					type : "error",
					title : "Error..!",
					body : addAjaxCall.responseMessage,
					close :'auto'
				});
			}
		} else {
			spinner.hideSpinner();
			AJS.flag({
				type : "error",
				title : "Oops..!",
				body : appMessgaes.serverError,
				close :'auto'
			})
		}
		
	},
	initEdit : function(teachingPlanId){
		teachingPlanConfig.resetFormById(teachingPlanConfig.editForm);
		spinner.showSpinner();
		var getAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/"+teachingPlanId,null, "GET");
		spinner.hideSpinner();
		if(getAjaxCall != null){	
			if(getAjaxCall.response == "shiksha-200"){
				$(teachingPlanConfig.editTeachingPlanId).val(getAjaxCall.id);
				$(teachingPlanConfig.editName).val(getAjaxCall.name);
				$(teachingPlanConfig.editGrade).val(getAjaxCall.grade.gradeId);
				$(teachingPlanConfig.editGrade).multiselect("rebuild");
				$(teachingPlanConfig.editModal).modal("show");
				
			} else {
				AJS.flag({
					type  : "error",
					title : "Error..!",
					body  : getAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type 	: "error",
				title 	: "Oops..!",
				body 	: appMessgaes.serverError,
				close	: 'auto'
			})
		}
	},
	fnEdit : function(){
		spinner.showSpinner();			
		var ajaxData={
				grade : {
					gradeId : $(teachingPlanConfig.editGrade).val()
				},
				name  : $(teachingPlanConfig.editName).val(),
				id :$(teachingPlanConfig.editTeachingPlanId).val(),
		};
		var editAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan", ajaxData, "PUT");

		if(editAjaxCall != null){
			if(editAjaxCall.response == "shiksha-200"){
					$(teachingPlanConfig.editModal).modal("hide");
					AJS.flag({
						type : "success",
						title : "Success!",
						body : editAjaxCall.responseMessage,
						close :'auto'
					});
					window.location.href =baseContextPath+"/shiksha/teachingPlan/"+editAjaxCall.id+"/details/view";
			} else {
				spinner.hideSpinner();
				AJS.flag({
					type : "error",
					title : "Error..!",
					body : editAjaxCall.responseMessage,
					close :'auto'
				});
			}
		} else {
			spinner.hideSpinner();
			AJS.flag({
				type : "error",
				title : "Oops..!",
				body : appMessgaes.serverError,
				close :'auto'
			})
		}
	},
	
	initViewDetails:function(tpId,name){
		window.location.href =baseContextPath+"/shiksha/teachingPlan/"+tpId+"/details/view";
	},
	
	initDelete : function(teachingplanId,name){
		$(teachingPlanConfig.deleteTeachingPlanId).val(teachingplanId);
		var msg = $("#deleteTeachingPlanMessage").data("message");
		$("#deleteTeachingPlanMessage").html(msg.replace('@NAME@',name));
		$(teachingPlanConfig.deleteModal).modal("show");
	},
	fnDelete : function(){
		spinner.showSpinner();
		var deleteTeachingPlanId = $(teachingPlanConfig.deleteTeachingPlanId).val();
		var deleteAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/"+deleteTeachingPlanId,null, "DELETE");
		spinner.hideSpinner();
		if(deleteAjaxCall != null){	
			if(deleteAjaxCall.response == "shiksha-200"){
				teachingPlanConfig.refreshTable(teachingPlanConfig.table);
				$(teachingPlanConfig.deleteModal).modal("hide");
				AJS.flag({
					type : "success",
					title : "Success!",
					body : deleteAjaxCall.responseMessage,
					close :'auto'
				})
			} else {
				AJS.flag({
					type : "error",
					title : "Error..!",
					body : deleteAjaxCall.responseMessage,
					close :'auto'
				});
			}
		} else {
			AJS.flag({
				type : "error",
				title : "Oops..!",
				body : appMessgaes.serverError,
				close :'auto'
			})
		}
	},
	resetFormById : function(formId) {
		$(formId).find('#alertdiv').hide();
		$(formId).find("label.error").remove();
		$(formId).find(".error").removeClass("error");
		$(formId)[0].reset();
	},
	resetForm : function(form){
		$(form).trigger("reset");
	},
	refreshTable: function(table){
		$(table).bootstrapTable("refresh");
	},
	actionFormater: function(value, row, index) {
		
		var action = "";
		if(row.academicYear.flagCurrentAY==1){
			//if(permission["edit"] || permission["delete"]){
			action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
			if(tpPermissions.editTechPlan){
				action+='<li><a onclick="teachingPlanConfig.initEdit(\''+row.id+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
			}
			if(tpPermissions.delTechPlan){
				action+='<li><a onclick="teachingPlanConfig.initDelete(\''+row.id+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
			}
			action+='<li><a onclick="teachingPlanConfig.initViewDetails(\''+row.id+'\',\''+row.name+'\')" href="javascript:void(0)" class="detailsLink">Details</a></li>';
			action+='</ul>';
			//}			
		}
		return action;
	},
};
var $submitActors = $(teachingPlanConfig.addForm).find('button[type=submit]');
$(document).ready(function(){
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
	
	teachingPlanFilterObj.init();
	teachingPlanConfig.init();
	
	$table = $(teachingPlanConfig.table).bootstrapTable({
		toolbar: teachingPlanFilterObj.toolbar,
		url : "shiksha/teachingPlan/page",
		method : "post",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: true,
		pagination: true,
		pageSize: 50,
		formatShowingRows : teachingPlanFilterObj.formatShowingRows,
		clickToSelect: false,
		queryParamsType :'limit',
		queryParams : teachingPlanFilterObj.fiterQueryParmams
	});
	$submitActors.click(function() {
		submitActor = this;
	});
});