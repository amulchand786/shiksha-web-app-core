var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		grade	: "#divSelectGrade",
		section	: "#divSelectSection",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		eleGrade			: "#selectBox_grade",
		eleSection			: "#selectBox_Section",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
		divFilter   		: '#divFilter',
		eleLocType  		: '#divSchoolLocType input:radio[name=locType]',
		filter				: '#locColspFilter',
		panelLink			: '#locationPnlLink',
		form				: '#listDeviceForm',
		eleCollapseFilter	: '#collapseFilter',
		eleToggleCollapse	: '[data-toggle="collapse"]',
		resetModal 			: '#mdlResetLoc',
		resetConfirm 		: '#btnResetLocYes',
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var locationUtility={
		fnInitLocation :function(){
			$(locationPanel.divFilter).css('display','none');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(locationPanel.resetModal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			$(locationPanel.eleGrade).multiselect('destroy').multiselect('refresh');
			$(locationPanel.eleSection).multiselect('destroy').multiselect('refresh');
			$(locationPanel.section).hide();
			$(locationPanel.grade).hide();
			
			if ($(resetButtonObj).parents("form").attr("id") == 'locationFilterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode = $(mapFilter.locTypeSelect).val();
				var aLLevelName =$(mapFilter.locTypeSelect).find("option:selected").data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
			}$(locationPanel.school).val()
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){			
				elementIdMap = {
						"State" 	: '#statesForZone',
						"Zone" 		: '#selectBox_zonesByState',
						"District" 	: '#selectBox_districtsByZone',
						"Tehsil" 	: '#selectBox_tehsilsByDistrict',
						"Block" 	: '#selectBox_blocksByTehsil',
						"City" 		: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" 	: '#selectBox_villagebyRv',
				};
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));				
				locationUtility.reinitializeSmartFilter(elementIdMap);				
				var lLocTypeCode = $(mapFilter.locTypeSelect).val();
				var lLLevelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');				
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));				
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(
						function(ind, ele_key) {
							$(elementIdMap[ele_key]).find("option:selected").prop(
									'selected', false);
							$(elementIdMap[ele_key]).multiselect('destroy');
							enableMultiSelect(elementIdMap[ele_key]);
						});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#btnsRow,#divFilter').show();
				}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});			
			$('#rowDiv1,#divFilter').show();			
			spinner.hideSpinner();
		},
};

var mapFilter = {
		locTypeSelect : "#filterBox_locType",
		villageDiv : "#filter-villagestDiv",
		villageSelect : "#filterBox_village",
		citySelect : "#filterBox_city",
		cityDiv : "#filter-citytDiv",
		schoolDiv : "#filter-schoolDiv",
		schoolSelect : "#filterBox_school",
		gradeSelect : "#filterBox_grade",
		gradeDiv : "#filter-gradeDiv",
		filterIcon : "#smart-filter",
		locFilteModal : "#mdlLocationFilter",
		applyBtn : "#locFilterApplyBtn",
		tpSelect 		 : "#filterBox_tp",
		searchBtn : ".filter-search-btn",
		init : function(){
			this.enableMultiSelectForFilters(this.locTypeSelect);
			this.enableMultiSelectForFilters(this.villageSelect);
			this.enableMultiSelectForFilters(this.citySelect);
			/*this.enableMultiSelectForFilters(this.schoolSelect);
			this.enableMultiSelectForFilters(this.gradeSelect);*/
			this.enableMultiSelectForFilters(this.tpSelect);
			
			
			$(this.locTypeSelect).on("change",this.locaTypeChange);
			$(this.filterIcon).on("click",this.smartFilterInit);
			
			$("#selectBox_cityByBlock").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$("#selectBox_villagebyRv").on("change",function(){
				$(mapFilter.applyBtn).show();
			});
			$(this.villageSelect).on("change",mapFilter.showSchools);
			$(this.citySelect).on("change",mapFilter.showSchools);
			/*$(this.schoolSelect).on("change",mapFilter.showGrades);
			$(this.gradeSelect).on("change",mapperObj.showSubjects);*/
			$(mapFilter.applyBtn).on("click",this.applySmartFilter);
			
			$(this.tpSelect).on("change",this.tpChange);
			$(this.searchBtn).on("click",schoolMaperObj.showSchools);
		},
		applySmartFilter :  function(){
			if($(mapFilter.locTypeSelect).val() == "R"){
				var selectedIds = $("#selectBox_villagebyRv").val();
				$(mapFilter.villageSelect).find("option").prop("selected",false);			
				$.each(selectedIds,function(x,val){
					$(mapFilter.villageSelect).find("option[value='"+val+"']").prop("selected",true);
				});
				$(mapFilter.villageSelect).multiselect("rebuild");
			} else {
				var selectedIds = $("#selectBox_cityByBlock").val();
				$(mapFilter.citySelect).find("option").prop("selected",false);
				$.each(selectedIds,function(x,val){
					$(mapFilter.citySelect).find("option[value='"+val+"']").prop("selected",true);
				});
				$(mapFilter.citySelect).multiselect("rebuild");
			}
			$(mapFilter.locFilteModal).modal("hide");
		},
		tpChange : function(){
			$(schoolMaperObj.container).hide();
			$(".label-grade").show();
			$("#current-grade").text($(this).find("option:selected").attr("data-grade"));
		},
		smartFilterInit : function(){
			//$(mapFilter.applyBtn).hide();
			mapFilter.setLocations();
			$(mapFilter.locFilteModal).modal("show");
		},
		locaTypeChange : function(){
			if($(this).val() == "R"){
				$(mapFilter.cityDiv).addClass("hide");
				$(mapFilter.villageDiv).removeClass("hide");
				$(mapFilter.villageSelect).val("");
				$(mapFilter.villageSelect).multiselect("rebuild");
			} else {
				$(mapFilter.cityDiv).removeClass("hide");
				$(mapFilter.villageDiv).addClass("hide");
				$(mapFilter.citySelect).val("");
				$(mapFilter.citySelect).multiselect("rebuild");
			}
			$(mapFilter.schoolDiv).addClass("hide");
			$(mapFilter.gradeDiv).addClass("hide");
			$(mapperObj.subjectListContainer).hide();
		},
		setLocations : function(){
			$(locationPanel.filter).removeClass('in');
			$(locationPanel.panelLink).text('Select Location');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
			var typeCode =$(this.locTypeSelect).val();	
			var locCode = $(mapFilter.locTypeSelect).val()
			var levelName = $(mapFilter.locTypeSelect).find("option:selected").data('levelname');
			if(typeCode=='U'){
				showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("U");
				fnCollapseMultiselect();
			}else{
				hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
				showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
				locationUtility.displaySmartFilters(locCode,levelName);
				positionFilterDiv("R");
				fnCollapseMultiselect();						
			}
			$(locationPanel.eleCollapseFilter).css('display','block');
			$(locationPanel.grade).hide();
			$(locationPanel.section).hide();
		},
		showSchools : function(){
			/*$(mapFilter.schoolSelect).empty();
			var locType = "R";
			var locationId = $(mapFilter.locTypeSelect).val() == "R" ? $(mapFilter.villageSelect).val() : $(mapFilter.citySelect).val();
			var ajaxData = {
				locations : [locationId]	
			};
			if($(mapFilter.locTypeSelect).val() == "U"){
				locType = "U";
			}
			var getAjaxCall = shiksha.invokeAjax("school/"+locType, ajaxData, "POST");
			
			if(getAjaxCall.length){
				if(getAjaxCall[0].response == "shiksha-200"){
					$.each(getAjaxCall,function(index,school){
						$(mapFilter.schoolSelect).append("<option value='"+school.id+"'>"+school.schoolName+"</option>")
					});
					
				} 
			} 
			$(mapFilter.schoolSelect).multiselect("rebuild");
			$(mapFilter.schoolDiv).removeClass("hide");
			$(mapFilter.gradeDiv).addClass("hide");
			$(mapperObj.subjectListContainer).hide();
			spinner.hideSpinner();*/
		},
		showGrades : function(){
			bindGradesBySchoolToListBox($(mapFilter.gradeSelect),$(mapFilter.schoolSelect).val(),0);
			$(mapFilter.gradeSelect).multiselect("rebuild");
			$(mapFilter.gradeDiv).removeClass("hide");
			$(mapperObj.subjectListContainer).hide();
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
			});	
		}
		
};

var schoolMaperObj = {
		container : "#schoollist-container",
		saveBtn : "#mapSaveBtn",
		table : "#shcoolMpTbl",
		init : function(){
			$(this.saveBtn).on("click",this.fnSaveMappers);
		},
		showSchools : function(){
			var locationIds = $(mapFilter.locTypeSelect).val() == "R" ? $(mapFilter.villageSelect).val() : $(mapFilter.citySelect).val();
			if(!$(mapFilter.tpSelect).val()){
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : "Please select Teaching plan",
					close :'auto'
				})
				return false;
			} else if(!locationIds || !locationIds.length){
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : "Please select at least one village/city",
					close :'auto'
				})
				return false;
			}
			$(schoolMaperObj.container).show();
			$(schoolMaperObj.table).find("tbody").empty();
			if(locationIds.indexOf("multiselect-all") > -1){
				locationIds.splice(0, 1);
			}
			var ajaxData = {
				locations : locationIds,
				locationTypeCode : "R",
				shikshaTeachingPlanId : $(mapFilter.tpSelect).val()
			};
			if($(mapFilter.locTypeSelect).val() == "U"){
				ajaxData.locationTypeCode = "U";
			}
			var getAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/map", ajaxData, "POST");
			if(getAjaxCall){
				if(getAjaxCall.response == "shiksha-200"){
					$.each(getAjaxCall.schoolSections,function(x,school){
						var schoolDisabled=school.school.isTeachingPlanSynced ? "disabled" : "";
						var str = "<tr data-schoolid='"+school.school.id+"'><td><input type='checkbox' "+schoolDisabled+ " class='school-check'/> "+school.school.schoolName+"</td><td>";
							$.each(school.sections,function(y,section){
								var isMapped = section.isTeachingPlanMapped ? "checked" : "";
								var isSynced = section.isTeachingPlanSynced ? "disabled" : "";
								str+='<lable class="section-check"><input type="checkbox" class="section-check-box" data-section="'+section.sectionId+'" '+isMapped+' '+isSynced+'/> '+section.sectionName+'</lable>'
							})
							str+="</td></tr>";
							$(schoolMaperObj.table).find("tbody").append(str);
					});
					//$(".school-check").prop(schoolDisabled);
					$(".section-check-box").trigger("change");
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : getAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		fnSaveMappers : function(){
			var ajaxData = {
					schoolSections : []
			};
			$(schoolMaperObj.table).find("tbody tr").each(function(z,row){
				var scObj = {
						school : { id : $(this).attr("data-schoolid")},
						sections : []
				}
				$(this).find(".section-check-box").each(function(){
					if($(this).is(":checked")){
						scObj.sections.push({sectionId : $(this).attr("data-section")});
					}
				});
				ajaxData.schoolSections.push(scObj);
			})
			var saveAJAX = shiksha.invokeAjax("shiksha/teachingPlan/"+$(mapFilter.tpSelect).val()+"/map", ajaxData, "POST");
			if(saveAJAX){
				if(saveAJAX.response == "shiksha-200"){
					schoolMaperObj.showSchools();
					AJS.flag({
						type : "success",
						title : "Success..!",
						body : saveAJAX.responseMessage,
						close :'auto'
					})
					
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : saveAJAX.responseMessage,
						close :'auto'
					})
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		}
};


var mapperObj = {
		
		subjectListUL : "#subjectListUL",
		subjectLi : ".mapper-subject",
		subjectListContainer : "#subjectlist-container",
		sectionMapContainer  :"#sectionMapContainer",
		sectionMapContainerMain :"#sectionMapContainerMain",
		mapSaveBtn : "#mapSaveBtn",
		init : function(){
			$(document).on("click",this.subjectLi,this.showSubjectSectionMapping);
			$(this.mapSaveBtn).on("click",this.fnSaveMappers);
		},
		showSubjectSectionMapping : function(){
			$(mapperObj.sectionMapContainer).empty();
			$(this).siblings().removeClass("active");
			$(this).addClass("active");
			var subjectId = $(this).data("subjectid");
			var schoolId = $(mapFilter.schoolSelect).val();
			var gradeId = $(mapFilter.gradeSelect).val();
			var teachingPlanList = shiksha.invokeAjax("shiksha/teachingPlan/grade/"+gradeId+"/subject/"+subjectId, null, "GET");
			var ajaxData = {
					"school":{
						"id":schoolId
						},
						"grade":{
						"gradeId":gradeId
						},
						"subject":{
						"subjectId":subjectId
						}
			}
			var getAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/map", ajaxData, "POST");
			console.log(getAjaxCall);
			var mainTemp = $(".map-template").clone();
			$(mainTemp).removeClass("map-template");
			$.each(teachingPlanList,function(index,tplan){
				$(mainTemp).find(".tplan-select").append("<option data-name='"+tplan.name+"' value='"+tplan.id+"'>"+tplan.name+"</option>");
			});
			$.each(getAjaxCall,function(index,mapper){
				var template = $(mainTemp).clone();
				$(template).show();
				$(template).attr("data-sectionid",mapper.section.sectionId);
				$(template).attr("data-sectionname",mapper.sectionName).find(".section-title").text(mapper.section.sectionName);
				$(template).find(".tplan-select").attr("id","tplanSelectSection"+mapper.section.sectionId);
				$(mapperObj.sectionMapContainer).append($(template));
				$("#tplanSelectSection"+mapper.section.sectionId).val(mapper.shikshaTeachingPlanId);
				mapFilter.enableMultiSelectForFilters("#tplanSelectSection"+mapper.section.sectionId);
			});
			
			$(mapperObj.sectionMapContainerMain).show();
			spinner.hideSpinner();
		},
		showSubjects : function(){
			$(mapperObj.sectionMapContainerMain).hide();
			$(mapperObj.subjectListUL).empty();
			spinner.showSpinner();
			var schoolId = $(mapFilter.schoolSelect).val();
			var gradeId = $(mapFilter.gradeSelect).val();
			var getAjaxcall = shiksha.invokeAjax("subject/school/"+schoolId+"/grade/"+gradeId+"/", null, "GET");
			$.each(getAjaxcall,function(index,subject){
				var schoolTemp =' <li class="lms-teacher-school-item mapper-subject" data-subjectid="'+subject.subjectId+'"><div class="lms-side-center-far">'
								+'<div class="lms-teacher-school-text">'+subject.subjectName+'</div><div class="lms-side-center" style="display:none;"><a href="#" class="lms-close-icon">'
				 				+'<svg><use xlink:href="web-resources/images/teacher.svg#close" href="web-resources/images/teacher.svg#close"></use></svg></a>'
				 				+'<a href="#" class="lms-expand-icon"><svg><use xlink:href="web-resources/images/teacher.svg#expand" href="web-resources/images/teacher.svg#expand"></use></svg></a></div></div></li>';
							$(mapperObj.subjectListUL).append(schoolTemp);
			});
			$(mapperObj.subjectListContainer).show();
			spinner.hideSpinner();
		},
		fnSaveMappers : function(){
			var ajaxData = [];
			$(mapperObj.sectionMapContainer).find(".section-map-row").each(function(index,row){
				var sectionObj = {
						section : {
							sectionId : $(this).data("sectionid"),
							sectionName : $(this).data("sectionname")
						},
						shikshaTeachingPlanId  : $(this).find(".tplan-select").val(),
						shikshaTeachingPlanName  : $(this).find(".tplan-select option:selected").data("name")
				};
				ajaxData.push(sectionObj);
			});
			var schoolId = $(mapFilter.schoolSelect).val();
			var gradeId = $(mapFilter.gradeSelect).val();
			var saveAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/school/"+schoolId+"/grade/"+gradeId+"/map", ajaxData, "POST");
			
			if(saveAjaxCall != null){	
				if(saveAjaxCall.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title : "Success!",
						body : saveAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : saveAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Oops..!",
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		}
		
};


$(document).ready(function(){
	mapFilter.init();
	//mapperObj.init();
	schoolMaperObj.init();
	$(".select-all").on("change",function(){
		if($(this).is(":checked")){
			$(".school-check").prop("checked",true);
		} else {
			$(".school-check").prop("checked",false);	
		}
		$(".school-check").trigger("change");
	});
	$(document).on("change",'.school-check',function(){
		if($(".school-check:not(:checked)").length){
			$(".select-all").prop("checked",false);
			
		} else {
			$(".select-all").prop("checked",true);
			
		}	
		if($(this).is(":checked")){
			$(this).closest("tr").find(".section-check-box").prop("checked",true);
		} else {
			$(this).closest("tr").find(".section-check-box").prop("checked",false);
		}
	});
	$(document).on("change",".section-check-box",function(){
		if($(this).closest("tr").find(".section-check-box:checked").length){
			$(this).closest("tr").find(".school-check").prop("checked",true);
		} else {
			$(this).closest("tr").find(".school-check").prop("checked",false);
		}
		if($(".school-check:not(:checked)").length){
			$(".select-all").prop("checked",false);
			
		} else {
			$(".select-all").prop("checked",true);
			
		}
	});
});