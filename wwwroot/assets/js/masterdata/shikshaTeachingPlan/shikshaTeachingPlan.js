var teachingplanFilterVariables ={
		flag			:0,
		acYearId		: "",
		schoolId		: "",
		subjectId		: "",
		gradeId			: "",
		sectionId		: ""
};
var startDates=[];
var isPrint = false;
var teachingplanFilter = {
		acYearSelect 	: "#filterBox_acYear",
		schoolsSelect  	: "#filterBox_schools",
		gradeSelect 	: "#filterBox_grade",
		sectionSelect  	: "#filterBox_section",
		subjectSelect	: "#filterBox_subject",
		acYearSelectDiv : "#filter-acYearDiv",
		schoolsSelectDiv: "#filter-schoolsDiv",
		gradeSelectDiv 	: "#filter-gradeDiv",
		sectionSelectDiv: "#filter-sectionDiv",
		subjectSelectDiv: "#filter-subjectDiv",
		searchBtn 		: "#filter-search-btn",
		toolbar		 	: "#conteFilterContainer",
		table			: "#teachPlanChapterList",
		teachingPlanId : "#teachingPlanId",
		startDate : "#startDate",
		endDate : "#endDate",
		subjectLi : ".subject-li",
		detailsDownload : "#details-download",
		init : function(){
			/*this.enableMultiSelectForFilters(this.acYearSelect,"Acamedic year : none",this.acyearChange);
			this.enableMultiSelectForFilters(this.schoolsSelect,"Schools : none",this.schoolChange);
			this.enableMultiSelectForFilters(this.gradeSelect,"Grade : none",this.gradeChange);
			this.enableMultiSelectForFilters(this.sectionSelect,"Section :none",this.sectionChange);
			this.enableMultiSelectForFilters(this.subjectSelect,"Subject : none",this.subjectChange);
			$(this.searchBtn).on("click",this.filterData);*/
			$(this.toolbar).show();
			this.filterData();
			$(this.table).on("click",".expand-icon",this.showHideSegments);
			$(this.subjectLi).on("click",this.changeSubject);
			$(this.detailsDownload).on("click",createPDF);
			//$("#pdf-download").on("click",createPDF)
		},
		download : function(){
			var teachingPlanId=$("#teachingPlanId").val();
			window.location = baseContextPath +"/shiksha/teachingPlan/"+teachingPlanId+"/details/download";
		},
		acyearChange : function(){
			if($(teachingplanFilter.subjectSelect).val()){
				teachingplanFilter.filterData();
			}
		},
		showHideSegments : function(){
			if(!isPrint)
				$(this).closest("table").find("tr.tp-activity,tr.tp-day").remove();
			if($(this).closest("tr").hasClass("open") && !isPrint){
				$(this).closest("tr").removeClass("open");
				$(this).closest("table").find("tr").removeClass("open");
			} else {
				if(!isPrint)
					$(this).closest("table").find("tr").removeClass("open");
				$(this).closest("tr").addClass("open");
				teachingplanFilter.showSegments($(this).closest("tr").attr("data-chapterid"));
			}
		},
		showSegments : function(chapterId){
			var lessonplan = customAjaxCalling("shiksha/lessonplan/unit/"+chapterId+"/teachingPlan/"+$("#teachingPlanId").val(),null,"GET");
			if(lessonplan){
				$.each(lessonplan.lessonPlanDays,function(x,dayObj){
					 //$("tr[data-chapterid='"+chapterId+"']").last().after("<tr class='tp-day' data-chapterid='"+chapterId+"'><td>Day: "+dayObj.day+"</td><td class='start-date'></td><td class='end-date'></td></tr>");
					 $.each(dayObj.activities,function(y,activity){
						 if(activity.segmentDetails.selectedSegment){
							 $.each(activity.segmentDetails.segments,function(z,obj){
								var actSDate = obj.startDate? obj.startDate : "-";
								var actEDate = obj.endDate ? obj.endDate : "-";
								if(obj.segmentId == activity.segmentDetails.selectedSegment){
									$("tr.tp-day").last().find(".start-date").text(actSDate).end().find(".end-date").text(actEDate);
									$("tr[data-chapterid='"+chapterId+"']").last().after("<tr class='tp-activity' data-chapterid='"+chapterId+"'><td class='segment'>"+obj.segmentName+" (day "+(x+1)+") </td><td>"+actSDate+"</td><td>"+actEDate+"</td></tr>");
									return false;
								} 
							 });
							 
						 }
					 });
				});
			}
			spinner.hideSpinner();
		},
		schoolChange : function(){
			$(teachingplanFilter.searchBtn).hide();
			$(teachingplanFilter.sectionSelectDiv).hide();
			$(teachingplanFilter.subjectSelectDiv).hide();
			$(teachingplanFilter.table).hide();
			teachingplanFilter.emptySelect(teachingplanFilter.gradeSelect);
			teachingplanFilter.emptySelect(teachingplanFilter.sectionSelect);
			teachingplanFilter.emptySelect(teachingplanFilter.subjectSelect);
			bindGradesBySchoolToListBox($(teachingplanFilter.gradeSelect),$(teachingplanFilter.schoolsSelect).val(),0)
			$(teachingplanFilter.gradeSelect).multiselect("rebuild");
			$(teachingplanFilter.gradeSelectDiv).show();
		},
		gradeChange : function(){
			$(teachingplanFilter.searchBtn).hide();
			$(teachingplanFilter.subjectSelectDiv).hide();
			$(teachingplanFilter.table).hide();
			teachingplanFilter.emptySelect(teachingplanFilter.sectionSelect);
			teachingplanFilter.emptySelect(teachingplanFilter.subjectSelect);
			bindSubjectByGradeToListBox($(teachingplanFilter.subjectSelect),$(teachingplanFilter.gradeSelect).val())
			bindSourceBySchoolAndGradeToListBox($(teachingplanFilter.sectionSelect),$(teachingplanFilter.schoolsSelect).val(),$(teachingplanFilter.gradeSelect).val());
			$(teachingplanFilter.sectionSelect).multiselect("rebuild");
			$(teachingplanFilter.subjectSelect).multiselect("rebuild");
			$(teachingplanFilter.sectionSelectDiv).show();
		},
		sectionChange : function(){
			$(teachingplanFilter.searchBtn).hide();
			$(teachingplanFilter.subjectSelectDiv).show();
		},
		subjectChange : function(){
			teachingplanFilter.filterData();
			$(teachingplanFilter.searchBtn).show();
		},
		changeSubject : function(){
			$(teachingplanFilter.subjectLi).removeClass("active");
			$(this).addClass("active");
			teachingplanFilter.filterData();
		},
		filterData : function(){
			$(teachingplanFilter.table).show();
			var teachingPlanId = $(teachingplanFilter.teachingPlanId).val();  
			var subjectId = $("#subject-list").find("li.active").attr("data-subjectId")
			var url= "shiksha/teachingPlan/"+teachingPlanId+"/subject/"+subjectId+"/details";
			var ajaxCall = shiksha.invokeAjax(url, null, "GET");
			spinner.hideSpinner();
			if(ajaxCall !=null){
				teachingplanFilter.showChaptersData(ajaxCall.units);
			}		
		},
		showChaptersData : function(chapters){
			var str;
			$(teachingplanFilter.table).find("tbody").empty();
			startDates=[];
			var startDate = moment($(teachingplanFilter.startDate).val()).format("DD/MM/YYYY");
			var flag = true;
			$.each(chapters,function(index, value){
				startDates.push(value.startDate);
				if(value.startDate !=null && value.endDate !=null){
					str = "<tr class='tp-chapter' data-chapterid='"+value.unitId+"'>" +
						"<td>"
						+'<a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-chapter">'
						+'<div class="tp-toggle lms-side-center-far"><div class="lms-dropdown-toggle"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#dropdown" href="web-resources/images/content-manager.svg#dropdown"></use></svg></div></div>'
						+'<div><div><div class="log-title-text">'+value.unitName+'</div></div></div></a>'
						+"</td>" +
						'<td><a data-enddate="'+value.endDate+'"  data-date-today-highlight="false" href="#" id="duration" data-isdetailsexists="'+value.noOfTpDetails+'" data-startDate="'+value.startDate+'" data-unitNumber="'+value.unitNumber+'" data-type="date" data-placement="right"  data-title="Start date" class="editable editable-click editable-open start-date-edit" style="">'+value.startDate+'</a></td>'+
						"<td>"+value.endDate+"</td>" +
						"</tr>" ;
					$(teachingplanFilter.table).find("tbody").append(str);
					flag = true;
				}
				else{
					str = "<tr>" +
					"<td>"+value.unitName+"</td>" +
					'<td><a href="#" id="duration" data-date-today-highlight="false" data-isdetailsexists="'+value.noOfTpDetails+'" data-unitNumber="'+value.unitNumber+'" data-type="date" data-placement="right"  data-title="Start date" class="editable editable-click editable-open start-date-edit" style=""></a></td>'+
					"<td>---</td>" +
					"</tr>" ;
					$(teachingplanFilter.table).find("tbody").append(str);
					flag = false;
				}
				
				if($('.start-date-edit').length > 1 && flag){
					var date = $($('.start-date-edit')[$('.start-date-edit').length-1]).attr("data-enddate");
					startDate = moment( date ? date : startDate).add(1,"day").format("DD/MM/YYYY");
				}
				
				
				$('.start-date-edit').last().editable({
					datepicker : {
						startDate 	: startDate,
						endDate		: moment($(teachingplanFilter.endDate).val()).format("DD/MM/YYYY"),
						format		: 'dd/mm/yyyy',
						
					},
					viewformat : "dd-M-yyyy",
					format:'yyyy-mm-dd',
					clear : false,
					send: 'never',
					validate  : function(value){
						console.log(value);
						if(!$(".datepicker-days .day.active").length){
							return "Please select date"
						} else if(!value){
							return "Enter Valid date";
						}
						
					}
			    }).on("save",function(e, params){
			    	console.log(params.submitValue,$(this).attr('data-unitNumber'),$(this).attr('data-startDate'));
			    	teachingplanFilter.startDateParams(params,$(this).attr('data-unitNumber'),$(this).attr('data-startDate'),$(this).attr('data-isDetailsExists'));
			    });
				
			});
			
			
		},
		isAlreadyExist: function(value){
			$.each(startDates,function(index,val){
				if(value == val){
					return false;
				}
				else
					return true;
			})
		},
		startDateParams : function(params,unitNumber,startDate,isDetailsExists){
			if(isDetailsExists==0){
				spinner.showSpinner();
				var data=[];
				$(teachingplanFilter.subjectLi).each(function(){
					var obj ={
							subject:{
								subjectId 	: $(this).attr("data-subjectid"),
								subjectName : $(this).attr("data-subjectname"),						
							},
							startDate 	: params.submitValue,
					};
					
					data.push(obj);
				});
				var teachingPlanId = $(teachingplanFilter.teachingPlanId).val(); 
				var addAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/"+teachingPlanId+"/details", data, "POST");
				spinner.hideSpinner();
				teachingplanFilter.filterData();
				if(addAjaxCall != null){	
					if(addAjaxCall.response == "shiksha-200"){
						AJS.flag({
							type : "success",
							title : "Success",
							body : addAjaxCall.responseMessage,
							close :'auto'
						})
					} else if(addAjaxCall.response == "shiksha-800"){
						AJS.flag({
							type : "error",
							title : "Error",
							body : "The following chapters have no lesson plan : "+addAjaxCall.responseMessages.join(),
							close :'auto'
						});
					} else {
						AJS.flag({
							type : "error",
							title : "Error",
							body : addAjaxCall.responseMessage,
							close :'auto'
						});
					}
				} else {
					AJS.flag({
						type : "error",
						title : "Error",
						body : appMessgaes.serverError,
						close :'auto'
					})
				}
			}
			else{
				teachingplanFilter.fnUpdate(params,unitNumber);
			}
				
		},
		fnUpdate :function(params,unitNumber){
			/*var data={
					startDate	   	: params.submitValue,
					unit			: {'unitNumber' :unitNumber}
			};*/
			
			var data;
			$(teachingplanFilter.subjectLi).each(function(){
				if($(this).hasClass('active')){
					var subject={
							subjectId 	: $(this).attr("data-subjectid"),
							subjectName : $(this).attr("data-subjectname"),						
					}
					data={
							subject:subject,
							startDate	   	: params.submitValue,
							unit			: {'unitNumber' :unitNumber}
					};
				}
			});
			var teachingPlanId = $(teachingplanFilter.teachingPlanId).val(); 
			var updateAjaxCall = shiksha.invokeAjax("shiksha/teachingPlan/"+teachingPlanId+"/details", data, "PUT");
			spinner.hideSpinner();
			teachingplanFilter.filterData();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title : "Success",
						body : updateAjaxCall.responseMessage,
						close :'auto'
					})
				} else if(updateAjaxCall.response == "shiksha-800"){
					AJS.flag({
						type : "error",
						title : "Error",
						body : "The following chapters have no lesson plan : "+updateAjaxCall.responseMessages.join(),
						close :'auto'
					});
				} else {
					AJS.flag({
						type : "error",
						title : "Error",
						body : updateAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : "Error",
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
		},
		emptySelect : function(ele){
			$(ele).empty();
		},
		enableMultiSelectForFilters : function(element,nonSelectedText,onChangeCallback){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				nonSelectedText : nonSelectedText,
				onChange : onChangeCallback
			});	
		},
		fnGetSelectedFiteredValue : function(ele){
			var arr =[];
			var selector = "option" ;
			if($(ele).val()!=null)
				selector = "option:selected";
			$(ele).find(selector).each(function(ind,option){
				if($(option).val()!="multiselect-all"){		
					arr.push(parseInt($(option).val()));
				}
			});
			return arr;
		},
}

function createPDF(){
	isPrint = true;
	$("#teachPlanChapterList tbody").find("tr").removeClass("open");
	$("#teachPlanChapterList").find("tr.tp-activity,tr.tp-day").remove();
	$(".expand-icon").trigger("click");
	var doc = new jsPDF('p', 'pt');
	var name=$("#conteFilterContainer").text().trim()+" : "+$("#subject-list li.active").data("subjectname");
	var splitTitle = doc.splitTextToSize(name, 500);
	doc.text(40, 30, splitTitle);
	
	var elem = document.getElementById("teachPlanChapterList");
	var res = doc.autoTableHtmlToJson(elem);
	doc.autoTable(res.columns, res.data,{
		startY: splitTitle.length*10+60,
		styles: {overflow: 'linebreak'},
	});
	doc.save(name.substr(0, 100)+".pdf");
	isPrint = false;
}
	

$(document).ready(function(){
	$.fn.editable.defaults.mode = 'popup'; 
	teachingplanFilter.init();
	
});