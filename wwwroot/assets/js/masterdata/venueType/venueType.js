var pageContextElements = {	
	addButton 					: '#addNewVenueType',
	table 						: '#venueTypeTable',
	toolbar						: '#toolbarVenueType',
};
var addModal = {
		form 			: '#addVenueTypeForm',
		modal   		: '#mdlAddVenueType',
		saveMoreButton	: 'addVenueTypeSaveMoreButton',
		saveButton		: 'addVenueTypeSaveButton',
		eleName		   	: '#addVenueTypeName',	
};
var editModal ={
		form	 : '#editVenueTypeForm',
		modal	 : '#mdlEditVenueType',
		eleName	 : '#editVenueTypeName',
		eleId	 : '#venueTypeId',
		
};
var deleteModal ={
		modal	: '#mdlDeleteVenueType',
		confirm : '#deleteVenueTypeButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');
var venueId;
var venueTypeFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){ 
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var venueType={
		name:'',
		venueTypeId:0,
		init:function(){
			$(pageContextElements.addButton).on('click',venueType.initAddModal);
			$(deleteModal.confirm).on('click',venueType.deleteFunction);
		},
		initAddModal :function(){
			venueType.resetForm(addModal.form);
			fnInitSaveAndAddNewList();
			$(addModal.modal).modal("show");
			
		},
		resetForm : function(formId){
			$(formId).find('#alertdiv').hide();
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		deleteFunction : function(){
			venueType.fnDelete(venueId);
		},		
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleName).val(),
					}
			var addAjaxCall = shiksha.invokeAjax("venue/type", ajaxData, "POST");
			spinner.hideSpinner();
			if(addAjaxCall != null){
					if(addAjaxCall.response == "shiksha-200"){
						if(submitBtnId == addModal.saveButton){ 
							$(addModal.modal).modal("hide");
							$(addModal.modal).find("#divSaveAndAddNewMessage").hide();
							AJS.flag({
								type  : "success",
								title :  appMessgaes.success,
								body  : addAjaxCall.responseMessage,
								close : 'auto'
							});
						}
						if(submitBtnId == addModal.saveMoreButton){ 
							$(addModal.modal).modal("show");
							$(addModal.modal).find("#divSaveAndAddNewMessage").show();
							fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
						}
						venueType.resetForm(addModal.form);
						venueType.refreshTable(pageContextElements.table);
					}
					else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
				} 
			else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'		
					})
			}
		},
		
		initEdit: function(VenueTypeId){
			venueType.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("venue/type/"+VenueTypeId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
					if(editAjaxCall.response == "shiksha-200"){
						$(editModal.eleName).val(editAjaxCall.name); 
						$(editModal.eleId).val(editAjaxCall.venueTypeId); 
						
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : editAjaxCall.responseMessage,
							close : 'auto'
						})
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
			
		},
		
		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"        : $(editModal.eleName).val(),    
					"venueTypeId" : $(editModal.eleId).val(),  
					}
			var updateAjaxCall = shiksha.invokeAjax("venue/type", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
					if(updateAjaxCall.response == "shiksha-200"){
						$(editModal.modal).modal("hide");
						venueType.resetForm(editModal.form);
						venueType.refreshTable(pageContextElements.table);
						AJS.flag({
							type  : "success",
							title : venuetypeMessages.sucessAlert,
							body  : updateAjaxCall.responseMessage,
							close :'auto'
						})
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : updateAjaxCall.responseMessage,
							close : 'auto'
						})
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
		},
		initDelete : function(venueTypeId,name){
			var msg = $("#deleteVenueTypeMessage").data("message");
			$("#deleteVenueTypeMessage").html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			venueId=venueTypeId;
			
		},
		fnDelete :function(venueTypeId){
			spinner.hideSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("venue/type/"+venueTypeId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
					if(deleteAjaxCall.response == "shiksha-200"){
						venueType.refreshTable(pageContextElements.table);
						$(deleteModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : venuetypeMessages.sucessAlert,
							body  : deleteAjaxCall.responseMessage,
							close : 'auto'
						})
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : deleteAjaxCall.responseMessage,
							close : 'auto'
						});
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
			
		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
				}).validate({
					ignore: '',
			        rules: {
			        	venueTypeName: {
			                required: true,
			                minlength: 1,
							maxlength: 255,
			                noSpace:true,
			                accept:/^[^'?\"/\\]*$/,
			            }			            
			        },
			        messages: {
			        	venueTypeName: {
			                required: venuetypeMessages.nameRequired,
			                noSpace:venuetypeMessages.nameNoSpace,
			                minlength: venuetypeMessages.nameMinLength,
			                maxlength: venuetypeMessages.nameMaxLength,
			                accept:venuetypeMessages.nameWithSpecialChar,

			            }
			        },
					submitHandler : function(){
						if(validateFormName === addModal.form){
							venueType.fnAdd(submitActor.id);
						}
						if(validateFormName === editModal.form){
							venueType.fnUpdate();
						}
					}
		});
		},
		venueTypeActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="venueType.initEdit(\''+ row.venueTypeId+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="venueType.initDelete(\''+ row.venueTypeId+ '\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		
};
//trim whitespace 
$(':input').change(function() {
	$(this).val($(this).val().trim());
});
$( document ).ready(function() {
	
	venueType.formValidate(addModal.form);
	venueType.formValidate(editModal.form);
	
	$submitActors.click(function() {
        submitActor = this;
    });
	venueType.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar,
		url : "venue/type",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: false,
		formatShowingRows : venueTypeFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});