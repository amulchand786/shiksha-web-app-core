var pageContextElements = {	
		addButton 					 : '#addNewSegmentType',
		table 						 : '#segmentTypeTable',
		toolbar						 : "#toolbarSegmentType",
};
var addModal = {
	form	: '#addSegmentTypeForm'	,
	modal	: '#mdlAddSegmentType',
	saveMoreBtn	: 'addSegmentTypeSaveMoreButton',
	saveBtn		: 'addSegmentTypeSaveButton',
	eleName		: '#addSegmentTypeName',
	eleCheckbox	: '#subSegmentTypeCheckBox',
	eleSelected : '#selectedSubSegmentType',
	eleSegmentType : '#addParentSegmentTypeName',
	eleActivityCheckbox	: '#isActivityCheckBox',
	message		: "#divSaveAndAddNewMessage"
};
var editModal = {
		form	: '#editSegmentTypeForm',
		modal	: '#mdlEditSegmentType',
		eleName	: '#editSegmentTypeName',
		eleId	: '#editSegmentTypeId',
		eleCheckbox : '#subSegmentTypeCheckBoxInEdit',
		eleSelected	: '#selectedSubSegmentTypeInEdit',
		eleSegmentType : '#editParentSegmentTypeName',
		eleActivityCheckbox	: '#isActivityEditCheckBox',
};
var deleteModal = {
		modal	: '#mdlDeleteSegmentType',
		confirm	: '#deleteSegmentTypeButton',
		message	: "#deleteSegmentTypeMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var segmentId;
var segmentTypeFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var segmentType={
		name:'',
		segmentTypeId:0,
		self : segmentType,
		init:function(){

			$(pageContextElements.addButton).on('click',segmentType.initAddModal);
			$(deleteModal.confirm).on('click',segmentType.deleteFunction);
			$(addModal.eleCheckbox).on('change',segmentType.showHideParentSegmentTypes);
			$(editModal.eleCheckbox).on('change',segmentType.showHideParentSegmentTypesInEdit);

		},
		deleteFunction : function(){
			segmentType.fnDelete(segmentId);
		},
		initAddModal :function(){
			segmentType.resetForm(addModal.form);
			$(addModal.modal).modal("show");
			$(addModal.eleSelected).hide();
			fnInitSaveAndAddNewList();
			shikshaPlus.fnGetSegmentTypes(addModal.eleSegmentType,0,[]);
			segmentType.fnSetOptionsForMultiSelect(addModal.eleSegmentType);
			$(addModal.eleSegmentType).multiselect("rebuild");
		},
		resetForm : function(formId){
			$(formId).find('#alertdiv').hide();
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		showHideParentSegmentTypes: function(){
			if($(addModal.eleCheckbox).is(':checked')){
				shikshaPlus.fnGetSegmentTypes(addModal.eleSegmentType,0,[]);
				segmentType.fnSetOptionsForMultiSelect(addModal.eleSegmentType);
				$(addModal.eleSegmentType).multiselect("rebuild");
				$(addModal.eleSelected).show();
			}else{
				$(addModal.eleSelected).hide();
				$(addModal.eleSegmentType).val("");
			}
		},
		showHideParentSegmentTypesInEdit: function(){
			if($(editModal.eleCheckbox).is(':checked')){
				$(editModal.eleSelected).show();
			}else{
				$(editModal.eleSelected).hide();
				$(editModal.eleSegmentType).val("");
			}
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var parentSegmentTypes =$(addModal.eleSegmentType).val();
			var ajaxData={
					"segmentTypeName" : $(addModal.eleName).val(),
					"parentSegmentTypeId" : parentSegmentTypes,
					"isActivity": $(addModal.eleActivityCheckbox).is(':checked')?1:0,
			}
			var addAjaxCall = shiksha.invokeAjax("segment/type", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveBtn){
						
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						})
					}
					if(submitBtnId == addModal.saveMoreBtn){
						$(addModal.modal).modal("show");
						
						$(addModal.eleSelected).hide();
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						})
					}
					segmentType.resetForm(addModal.form);
					segmentType.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					$(addModal.eleSegmentType).multiselect("rebuild");
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.segmentTypeName);

				} else {
					AJS.flag({
						type : "error",
						title :  appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(segmentTypeId){
			segmentType.resetForm(editModal.form);
			$(editModal.eleSelected).hide();
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("segment/type/"+segmentTypeId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.segmentTypeName);
					$(editModal.eleId).val(editAjaxCall.segmentTypeId);
					$(editModal.eleActivityCheckbox).attr('checked', editAjaxCall.isActivity==1?true:false);
					if(editAjaxCall.parentSegmentTypeId != null){
						$(editModal.eleCheckbox).prop('checked',true);
						$('.modal-body').find('input[type=checkbox]').addClass('checked');
						$(editModal.eleSelected).show();
					}
					shikshaPlus.fnGetSegmentTypes(editModal.eleSegmentType,editAjaxCall.segmentTypeId,editAjaxCall.parentSegmentTypeId);
					segmentType.fnSetOptionsForMultiSelect(editModal.eleSegmentType);
					$(editModal.eleSegmentType).multiselect("rebuild");
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : editAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"segmentTypeName"	:$(editModal.eleName).val(),
					"segmentTypeId" :$(editModal.eleId).val(),
					"parentSegmentTypeId" :$(editModal.eleSegmentType).val(),
					"isActivity": $(editModal.eleActivityCheckbox).is(':checked')?1:0,
			}
			var updateAjaxCall = shiksha.invokeAjax("segment/type", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					segmentType.resetForm(editModal.form );
					segmentType.refreshTable(pageContextElements.table);
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : updateAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'})
			}
		},
		
		initDelete : function(segmentTypeId,name){
			
			$(deleteModal.modal).modal("show");
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',name));
			
			segmentId=segmentTypeId;
		},
		fnDelete :function(segmentTypeId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("segment/type/"+segmentTypeId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					segmentType.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body  : deleteAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
		},
		
		
		fnSetOptionsForMultiSelect : function(arg){	
			$(arg).multiselect({
				maxHeight: 200,
				includeSelectAllOption: false,
				enableFiltering: false,
				enableCaseInsensitiveFiltering: false,
				includeFilterClearBtn: true,
				
			});	
			fnCollapseMultiselect();
		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					segmentTypeName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					parentSegmentTypeName :{
						required: function(element){
							return $(element).parents(".form-group").is(":visible");
						},
					}
				},

				messages: {
					segmentTypeName: {
						required: messages.nameRequired,
						noSpace:messages.noSpace,
						minlength: messages.minLength,
						maxlength: messages.maxLength,
					},
					parentSegmentTypeName: {
						required: messages.subNameRequired,
						
					}
				},
				errorPlacement: function(error, element) {
				    if($(element).hasClass("select")){
				    	$(element).parent().append(error);
				    } else {
				    	$(error).insertAfter(element);
				    }
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						segmentType.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						segmentType.fnUpdate();
					}
				}
			});
		},
		activityActionFormater: function(value, row) {
			var action = ""; 
			if(row.isActivity==1 ){
				action='<span class="label label-success text-uppercase">True</span>';
				
			}else{
				action='<span class="label label-default text-uppercase">False</span>';
			}
			return action;
		},
		segmentTypeActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="segmentType.initEdit(\''+row.segmentTypeId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="segmentType.initDelete(\''+row.segmentTypeId+'\',\''+row.segmentTypeName+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
};

$( document ).ready(function() {

	segmentType.formValidate(addModal.form);
	segmentType.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	segmentType.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar ,
		url : "segment/type?exclude=",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: false,
		formatShowingRows : segmentTypeFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});