var pageContextElements = {
	addButton 	: '#addNewLevel',
	table 		: '#levelTable',
	toolbar		: "#toolbarLevel"
};
var addModal = {
	form 			: '#addLevelForm',
	modal 			: '#mdlAddLevel',
	saveMoreButton 	: 'addLevelSaveMoreButton',
	saveButton 		: 'addLevelSaveButton',
	eleName 		: '#addLevelName',
	eleSeqNumber 	: '#addSequenceNumber',
	eleCutOffPercentage:'#addCutOffPercentage'
};
var editModal = {
	form 		: '#editLevelForm',
	modal 		: '#mdlEditLevel',
	eleName 	: '#editLevelName',
	eleId 		: '#levelId',
	eleSeqNumber : '#editSequenceNumber',
	eleCutOffPercentage:'#editCutOffPercentage'

};
var deleteModal = {
	modal : '#mdlDeleteLevel',
	confirm : '#deleteLevelButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');
var levelIdForDelete;
var levelFilterObj = {
	
	formatShowingRows : function(pageFrom, pageTo, totalRows) {
		return 'Showing ' + pageFrom + ' to ' + pageTo + ' of ' + totalRows
				+ ' rows';
	},
	actionFormater : function() {
		return {
			classes : "dropdown dropdown-td"
		}
	}
};
var level = {
	name : '',
	levelId : 0,

	init : function() {

		$(pageContextElements.addButton).on('click', level.initAddModal);
		$(deleteModal.confirm).on('click', level.deleteFunction);
	},
	deleteFunction : function() {
		level.fnDelete(levelIdForDelete);
	},
	initAddModal : function() {
		fnInitSaveAndAddNewList();
		level.resetForm(addModal.form);
		$(addModal.modal).modal("show");

	},
	resetForm : function(formId) {
		$(formId).find('#alertdiv').hide();
		$(formId).find("label.error").remove();
		$(formId).find(".error").removeClass("error");
		$(formId)[0].reset();
	},
	refreshTable : function(table) {
		$(table).bootstrapTable("refresh");
	},

	fnAdd : function(submitBtnId) {
		spinner.showSpinner();
		var ajaxData = {
			"levelName" : $(addModal.eleName).val(),
			"sequenceNumber" : $(addModal.eleSeqNumber).val(),
			"cutOffPercentage" : $(addModal.eleCutOffPercentage).val(),
		}
		var addAjaxCall = shiksha.invokeAjax("level", ajaxData, "POST");

		if (addAjaxCall != null) {
			if (addAjaxCall.response == "shiksha-200") {
				if (submitBtnId == addModal.saveButton) {
					$(addModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : levelMessages.sucessAlert,
						body : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
				if (submitBtnId == addModal.saveMoreButton) {
					$(addModal.modal).modal("show");
					$(addModal.modal).find("#divSaveAndAddNewMessage").show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.levelName);
				}
				level.resetForm(addModal.form);
				level.refreshTable(pageContextElements.table);

				
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : addAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close : 'auto'
			})
		}
		spinner.hideSpinner();
	},

	initEdit : function(levelId) {
		level.resetForm(editModal.form);
		$(editModal.modal).modal("show");
		spinner.showSpinner();
		var editAjaxCall = shiksha.invokeAjax("level/" + levelId, null, "GET");
		spinner.hideSpinner();
		if (editAjaxCall != null) {
			if (editAjaxCall.response == "shiksha-200") {
				$(editModal.eleName).val(editAjaxCall.levelName);
				$(editModal.eleId).val(editAjaxCall.levelId);
				$(editModal.eleSeqNumber).val(editAjaxCall.sequenceNumber);
				$(editModal.eleCutOffPercentage).val(editAjaxCall.cutOffPercentage);
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : editAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close : 'auto'
			})
		}
	},

	fnUpdate : function() {
		spinner.showSpinner();
		var ajaxData = {
			"levelName" : $(editModal.eleName).val(),
			"levelId" : $(editModal.eleId).val(),
			"sequenceNumber" : $(editModal.eleSeqNumber).val(),
			"cutOffPercentage" : $(editModal.eleCutOffPercentage).val(),
		}

		var updateAjaxCall = shiksha.invokeAjax("level", ajaxData, "PUT");
		spinner.hideSpinner();
		if (updateAjaxCall != null) {
			if (updateAjaxCall.response == "shiksha-200") {
				$(editModal.modal).modal("hide");
				level.resetForm(editModal.form);
				level.refreshTable(pageContextElements.table);
				AJS.flag({
					type : "success",
					title : levelMessages.sucessAlert,
					body : updateAjaxCall.responseMessage,
					close : 'auto'
				})
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : updateAjaxCall.responseMessage,
					close : 'auto'
				})
			}
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close : 'auto'
			})
		}
	},
	initDelete : function(levelId,name) {
		var msg = $("#deleteLevelMessage").data("message");
		$("#deleteLevelMessage").html(msg.replace('@NAME@',name));
		$(deleteModal.modal).modal("show");
		levelIdForDelete = levelId;
	},
	fnDelete : function(levelId) {
		spinner.showSpinner();
		var deleteAjaxCall = shiksha.invokeAjax("level/" + levelId, null,
				"DELETE");
		spinner.hideSpinner();
		if (deleteAjaxCall != null) {
			if (deleteAjaxCall.response == "shiksha-200") {
				level.refreshTable(pageContextElements.table);
				$(deleteModal.modal).modal("hide");
				AJS.flag({
					type : "success",
					title :levelMessages.sucessAlert,
					body : deleteAjaxCall.responseMessage,
					close : 'auto'
				})
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : deleteAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close : 'auto'
			})
		}

	},
	formValidate : function(validateFormName) {
		$(validateFormName)
				.submit(function(e) {
					e.preventDefault();
				})
				.validate(
						{
							ignore : '',
							rules : {
								levelName : {
									required : true,
									minlength : 1,
									maxlength : 255,
									noSpace : true,
									accept:/^[^'?\"/\\]*$/,
								},
								sequenceNumber : {
									required : true,
									noSpace : true,
									digits : true,
									max: 20,
									min:1
								},
								cutOffPercentage : {
									required : true,
									noSpace : true,
									decimalNumber : true,
									cutoffmax:100,
									min:1
								}

							},

							messages : {
								levelName : {
									required: 	levelMessages.nameRequired,
									noSpace:	levelMessages.nameNoSpace,
									minlength: 	levelMessages.nameMinLength,
									maxlength: 	levelMessages.nameMaxLength,		
									accept: levelMessages.accept
								},
								sequenceNumber : {
									required : levelMessages.sequenceNumberRequired,
									noSpace : levelMessages.sequenceNumberNoSpace,
									digits : levelMessages.sequenceNumberDigits,
									max: 	levelMessages.sequenceMaxLength,
									min:levelMessages.minNumber
								},
								cutOffPercentage : {
									required : levelMessages.cutOffPercentageRequired,
									noSpace : levelMessages.cutOffPercentageNoSpace,
									cutoffmax: 	levelMessages.cutOffPercentageMaxLength,
									decimalNumber : levelMessages.cutOffPercentageDigits,
									min:   levelMessages.cutOffPercentageMinLength
								}
							},
							submitHandler : function() {
								if (validateFormName === addModal.form) {
									level.fnAdd(submitActor.id);
								}
								if (validateFormName === editModal.form) {
									level.fnUpdate();
								}
							}
						});
	},
	levelActionFormater : function(value, row) {
		var action = ""; 
		if(permission["edit"] || permission["delete"]){
			action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
			if(permission["edit"])
				action+='<li><a onclick="level.initEdit(\''+ row.levelId+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
			if(permission["delete"])
				action+='<li><a onclick="level.initDelete(\''+ row.levelId+ '\',\''+row.levelName+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
			action+='</ul>';
		}
		return action;
	},
	levelNumber : function(value, row, index) {
		return index + 1;
	},
};

$(document).ready(function() {

	level.formValidate(addModal.form);
	level.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	level.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar : pageContextElements.toolbar,
		url : "level",
		method : "get",
		search : false,
		sidePagination : "server",
		showToggle : false,
		showColumns : false,
		pagination : true,
		searchAlign : 'left',
		pageSize : 20,
		clickToSelect : false,
		queryParamsType :'limit',
		formatShowingRows : levelFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) {
		return !value.trim().length <= 0;
	}, "");
	
	jQuery.validator.addMethod("cutoffmax", function(value) {
		return 101 > parseInt(value);
	}, "");
	
	jQuery.validator.addMethod("decimalNumber", function(value) {
		return $.isNumeric(value);
	}, "");
	
	jQuery(addModal.eleSeqNumber).on('keydown', function(e) {
		  if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && !e.ctrlKey && (e.which < 96 || e.which > 105)) {
				  e.preventDefault(); 
		  }
	});
	
	jQuery(editModal.eleSeqNumber).on('keydown', function(e) {		
		if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && !e.ctrlKey && (e.which < 96 || e.which > 105)) {
			  e.preventDefault(); 
		  }
	});
});