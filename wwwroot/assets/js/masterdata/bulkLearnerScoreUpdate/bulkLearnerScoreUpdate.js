var pageContextElements = {
		divFilter   :'#divFilter',
		eleLocType  : '#divCenterLocType input:radio[name=locType]',
		filter		: '#locColspFilter',
		panelLink	: '#locationPnlLink',
		form		: '#listCenterForm',
		eleCollapseFilter		:'#collapseFilter',
		eleToggleCollapse		: '[data-toggle="collapse"]',
		clearBtn	 : '#btnResetLocation',
		levelDiv	 : '#divSelectLevel',
		batchDiv	 : '#divSelectBatch',
		groupDiv	 : '#divSelectGroup',
		yearDiv                 :'#divSelectYear',
		academicYearDiv : '#divSelectAcademicYear',
		learnerDiv  : '#viewLearnerDiv',
		learnerBtn	: '#viewLearnerBtn',
		tableDiv	: '#rowDiv2',
		table		: '#tableDiv',
		update		: '#updateScorerBtn',
		
};
var maxMarks = 0;
var updateElements = {
		bulkLearnerRollNumberUpdateId:'',
		table 		: '#bulkScoreUpdateTable',
		centerId	: '',
		levelId		: '',
		batchId		: '',
		groupId		: '',
		ayCycleId	: '',
		allLearners	: '',
};
var learnerData ={};
var learnersList=[];
var learnersListForUpdate=[];
var updatedLearnerData;
var selectBox ={
	center		: '#selectBox_centersByLoc',
	level		: '#selectBox_Level',
	batch		: '#selectBox_Batch',
	group		: '#selectBox_Group',
	academicYear: '#selectBox_AcademicYear',
	year		: '#selectBox_year'
};
var reset = {
	modal : '#mdlResetLoc',
	confirm : '#btnResetLocYes',
};
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		level 				: "#selectBox_Level",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter(
				$(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter(
				$(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter(
				$(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var $table;
var locationUtility={
		fnInitLocation :function(){
			$(pageContextElements.divFilter).css('display','none');
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(reset.modal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			
			$(selectBox.level).multiselect('destroy').multiselect('rebuild');
			$(selectBox.batch).multiselect('destroy').multiselect('rebuild');
			$(selectBox.academicYear).multiselect('destroy').multiselect('rebuild');
			$(pageContextElements.levelDiv).hide();
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.groupDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
			enableMultiSelect($("#selectBox_Section"));
			enableMultiSelect($("#selectBox_Level"));
			deleteAllRow("studentTable");
			if ($(resetButtonObj).parents("form").attr("id") == 'listCenterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
				
			}
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){
				elementIdMap = {
						"State" 			: '#statesForZone',
						"Zone" 				: '#selectBox_zonesByState',
						"District" 			: '#selectBox_districtsByZone',
						"Tehsil" 			: '#selectBox_tehsilsByDistrict',
						"Block" 			: '#selectBox_blocksByTehsil',
						"City" 				: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" 			: '#selectBox_villagebyRv',
						"Center" 			: '#selectBox_centersByLoc'
				};
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var lLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var lLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(function(ind, ele_key){
					$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
					$(elementIdMap[ele_key]).multiselect('destroy');
					enableMultiSelect(elementIdMap[ele_key]);
				});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#divFilter').show();
				}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});
			
			$('#rowDiv1,#divFilter').show();
			
			spinner.hideSpinner();
		},
		
};
var learnerScoreUpdate = {
		queryParmams : function(params){
			params['center'] = {"id":$(selectBox.center).val()};
			params['level'] ={"levelId": $(selectBox.level).val()};
			params['batch'] = {"batchId":$(selectBox.batch).val()};
			params['academicCycle'] = {"academicCycleId":$(selectBox.academicYear).val()};
			return params;
		},
};
var bulkLearnerScoreUpdate = {
		fnInit:function(){
			$(reset.confirm).on('click',locationUtility.doResetLocation);
			$(selectBox.center).on('change',bulkLearnerScoreUpdate.showLevels);
			$(selectBox.level).on('change',bulkLearnerScoreUpdate.showBatches);
			$(selectBox.batch).on('change',bulkLearnerScoreUpdate.showGroups);
			$(selectBox.group).on('change',bulkLearnerScoreUpdate.showAcademicCycles);
			$(pageContextElements.learnerBtn).on('click',bulkLearnerScoreUpdate.showTable);
			$(pageContextElements.update).on('click',bulkLearnerScoreUpdate.fnUpdate);
			$(pageContextElements.eleLocType).change(function() {
				$(pageContextElements.filter).removeClass('in');
				$(pageContextElements.panelLink).text(centerColumn.selectLocation);
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
				var typeCode =$(this).data('code');
				$(pageContextElements.levelDiv).hide();
				$(pageContextElements.batchDiv).hide();
				$(pageContextElements.groupDiv).hide();
				$(pageContextElements.yearDiv).hide();
				$(pageContextElements.academicYearDiv).hide();
				$(pageContextElements.tableDiv).hide();
				$(pageContextElements.learnerDiv).hide();
				if(typeCode.toLowerCase() =='U'.toLowerCase()){
					showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("U");
					fnCollapseMultiselect();
				}else{
					hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("R");
					fnCollapseMultiselect();
				}
				$(pageContextElements.eleCollapseFilter).css('display','');
			});
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		showLevels : function(){
			shikshaPlus.fnGetLevelsByCenter(selectBox.level,0,$(selectBox.center).val());
			setOptionsForMultipleSelect(selectBox.level);
			$(selectBox.level).multiselect("rebuild");
			$(pageContextElements.levelDiv).show();
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.groupDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
		},
		showBatches : function(){
			shikshaPlus.fnGetBatchesByCenterAndLevel(selectBox.batch,0,$(selectBox.center).val(),$(selectBox.level).val());
			setOptionsForMultipleSelect(selectBox.batch);
			$(selectBox.batch).multiselect("rebuild");
			$(pageContextElements.batchDiv).show();
			$(pageContextElements.groupDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
		},
		showGroups : function(){
			shikshaPlus.fnGetGroupsByLevelId(selectBox.group,0,$(selectBox.level).val());
			setOptionsForMultipleSelect(selectBox.group);
			$(selectBox.group).multiselect("rebuild");
			$(pageContextElements.groupDiv).show();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
		},
		showAcademicCycles : function(){
			shikshaPlus.fnGetCurrentYear(selectBox.year,0);
			setOptionsForMultipleSelect(selectBox.year);
			$(selectBox.year).multiselect("rebuild");
			
			shikshaPlus.fnGetCurrentACYear(selectBox.academicYear,0);
			setOptionsForMultipleSelect(selectBox.academicYear);
			$(selectBox.academicYear).multiselect("rebuild");
			$(pageContextElements.academicYearDiv).show();
			$(pageContextElements.yearDiv).show();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).show();
		},
		scoreActionFormater :function(index,row){
			console.log(row);
			return '<a href="#" data-type="text" data-learnerId="'+row.learnerId+'" data-score="'+row.score+'" data-learnerName="'+row.name+'" '
					+'data-placement="right" data-title="'+ centerColumn.enterScore+'" class="editable editable-click score-edit">'+row.score+'</a>';
		},
		showTable :function(){
			var data={
					'center':{"id":$(selectBox.center).val()},
					'level' :{"levelId": $(selectBox.level).val()},
					'batch':{"batchId":$(selectBox.batch).val()},
					'group':{"groupId":$(selectBox.group).val()},
					'academicCycle': {"academicCycleId":$(selectBox.academicYear).val()}
			}
			var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=score",data, "POST");
			spinner.hideSpinner();
			maxMarks = 0;
			if(ajaxCall.response == 'shiksha-200'){
				maxMarks = ajaxCall.maxMarks;
			if(maxMarks)
				$("[data-field='action']").text((centerColumn.scoreHeader+" / "+maxMarks));
			else
				$("[data-field='action']").text(centerColumn.scoreHeader);
				
			$(updateElements.table).bootstrapTable('load',ajaxCall.learners);
			$(pageContextElements.tableDiv).show();
			$('.score-edit').editable({
				 type: 'number',
				 step: 'any',
			     placement: 'right',
			     success : function(response, newValue) {
			         
			     },
			     validate: function(value) {
			    	 var regx = /^\d*(\.\d{1})?\d{0,1}$/;
			    	 if(!regx.test(value)){
			    		 return centerColumn.validScore;
			    	 } 
			    	 if($.isNumeric(value) == '') {
			    		 return centerColumn.validScore;
			    	 }
			    	 if(value<'0'){
			    		 return centerColumn.positiveScore;
			    	 }
			    	 if(maxMarks==0){
			    		 return centerColumn.updateMaxMarksInfo;
			    	 }else if(value>maxMarks){
			    		 return centerColumn.under100.replace("#MAXMARKS",maxMarks);
			    	 }
			     }
		    }).on("save",function(e, params){
		    });
			}else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				});
			}
		},
		fnUpdate : function(){
			updateElements.centerId = $(selectBox.center).val();
			updateElements.levelId = $(selectBox.level).val();
			updateElements.batchId = $(selectBox.batch).val();
			updateElements.groupId = $(selectBox.group).val();
			updateElements.ayCycleId = $(selectBox.academicYear).val();
			learnersList=[];
			learnersListForUpdate=[];
			$.each($(updateElements.table+" tbody tr"),function(){
				learnerData={};
				learnerData.id=$(this).find('a').attr('data-learnerId');
				learnerData.name=$(this).find('a').attr('data-learnerName');
				learnerData.prevScore=$(this).find('a').attr('data-score');
				learnerData.newScore=$(this).find('a').text();
				learnersList.push(learnerData);
			})
			$.each(learnersList,function(index,object){
				if(object.prevScore != object.newScore){
					updatedLearnerData={};
					updatedLearnerData.learnerId=object.id;
					updatedLearnerData.name=object.name;
					updatedLearnerData.score=object.newScore;
					learnersListForUpdate.push(updatedLearnerData);
				}
			})
			var data={
				center 			: {'id':updateElements.centerId},
				level	 		: {'levelId':updateElements.levelId},
				batch	 		: {'batchId':updateElements.batchId},
				group	 		: {'groupId':updateElements.groupId},
				academicCycle 	: {'academicCycleId':updateElements.ayCycleId},
				"maxMarks" : maxMarks,
				learners		: learnersListForUpdate,
			};
			if(learnersListForUpdate.length==0){
				AJS.flag({
					type  : "success",
					title : centerColumn.sucessAlert,
					body  : centerColumn.bulkScoreUpdated,
					close : 'auto'
				});
				setTimeout(function(){
					bulkLearnerScoreUpdate.showTable();
				}, 500);
				
			}else{
				var ajaxCall= shiksha.invokeAjax("learner/?type=bulkUpdateScore&op=updateScore",data,'POST');
				spinner.hideSpinner();
				if(ajaxCall.response == 'shiksha-200')
				{
					AJS.flag({
						type  : "success",
						title : centerColumn.sucessAlert,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					});
					
					setTimeout(function(){
						bulkLearnerScoreUpdate.showTable();
					}, 500);
				}
				else{
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : ajaxCall.responseMessage,
						close : 'auto'
					});
				}
			}
		},
		resetForm : function(formId) {
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		updateMaxScore : function(){
			spinner.showSpinner();
			var data={
					'center':{"id":$(selectBox.center).val()},
					'level' :{"levelId": $(selectBox.level).val()},
					'batch':{"batchId":$(selectBox.batch).val()},
					'group':{"groupId":$(selectBox.group).val()},
					'academicCycle': {"academicCycleId":$(selectBox.academicYear).val()},
					"maxMarks" : $("#updateMaxScore").val()
					
			}
			var ajaxCall= shiksha.invokeAjax("learner/?type=bulkUpdateScore&op=updateMaxMarks",data,'POST');
			spinner.hideSpinner();
			
			if(ajaxCall.response == 'shiksha-200') {
				maxMarks = ajaxCall.maxMarks;
				if(maxMarks)
					$("[data-field='action']").text((centerColumn.scoreHeader+" / "+maxMarks));
				else
					$("[data-field='action']").text(centerColumn.scoreHeader);
				
				$("#mdlUpdateMaxScore").modal("hide");
				AJS.flag({
					type  : "success",
					title : centerColumn.sucessAlert,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				});
				
				
			} else{
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				});
			}
		}
};
$( document ).ready(function() {
	$.fn.editable.defaults.mode = 'popup';
	bulkLearnerScoreUpdate.fnInit();
	$.validator.messages.noDecimal=centerColumn.onlyNumber;
	$("#updateMaxScoreBtn").on("click",function(){
		bulkLearnerScoreUpdate.resetForm("#updateMaxScoreForm");
		spinner.showSpinner();
		var data={
				'center':{"id":$(selectBox.center).val()},
				'level' :{"levelId": $(selectBox.level).val()},
				'batch':{"batchId":$(selectBox.batch).val()},
				'group':{"groupId":$(selectBox.group).val()},
				'academicCycle': {"academicCycleId":$(selectBox.academicYear).val()},
		}
		var ajaxCall= shiksha.invokeAjax("learner/?type=bulkUpdateScore&op=maxMarks",data,'POST');
		spinner.hideSpinner();
		if(ajaxCall.response == 'shiksha-200'){
			$("#updateMaxScore").val(ajaxCall.maxMarks);
			$("#mdlUpdateMaxScore").modal("show");
		}else{
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : ajaxCall.responseMessage,
				close : 'auto'
			});
		}
		
	});

	$.validator.addMethod("noDecimal", function(value, element) {
		var reg = /^[0-9]*$/;
		 return reg.test(value)
         
	}, $.validator.messages.noDecimal);
	
	 
	$("#updateMaxScoreForm").submit(function(e) {
		e.preventDefault();
	}).validate({
		rules : {
			updateMaxScore : {
				required : true,
				min : 1,
				max:1000,
				noDecimal:true
			},
		},
		messages : {
			updateMaxScore : {
				required : centerColumn.maxMarksRequired,
				min : centerColumn.maxMarksNonZero,
				max : centerColumn.maxScoreMaxValue
			}
		},
		submitHandler : function(){
			bulkLearnerScoreUpdate.updateMaxScore();
		}
	});
});