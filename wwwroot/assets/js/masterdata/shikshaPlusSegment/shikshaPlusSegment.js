//var chapters=[{chapterId:1,chapterName:"Chapter 1",nuggets:[{nuggetId:1,nuggetName:"Nugget 1",segments:[{segmentId:1,segmentName:"Segment 1",duration:"",minuits:"3",seconds:"20",isPublished:!0},{segmentId:2,segmentName:"Segment 2",duration:"2:40",minuits:"2",seconds:"40",isPublished:!0},{segmentId:3,segmentName:"Segment 3",duration:"4:30",minuits:"4",seconds:"30",isPublished:!0}]},{nuggetId:2,nuggetName:"Nugget 2",segments:[{segmentId:4,segmentName:"Segment 4",duration:"3:20",minuits:"3",seconds:"20",isPublished:!0},{segmentId:5,segmentName:"Segment 5",duration:"2:40",minuits:"2",seconds:"40",isPublished:!1}]}]},{chapterId:2,chapterName:"Chapter 2",nuggets:[{nuggetId:3,nuggetName:"Nugget 3",segments:[{segmentId:6,segmentName:"Segment 6",duration:"3:20",minuits:"3",seconds:"20",isPublished:!0},{segmentId:7,segmentName:"Segment 7",duration:"2:40",minuits:"2",seconds:"40",isPublished:!1},{segmentId:8,segmentName:"Segment 8",duration:"4:30",minuits:"4",seconds:"30",isPublished:!0}]},{nuggetId:4,nuggetName:"Nugget 4",segments:[{segmentId:9,segmentName:"Segment 9",duration:"3:20",minuits:"3",seconds:"20"},{segmentId:10,segmentName:"Segment 10",duration:"2:40",minuits:"2",seconds:"40"}]}]}];
var filterClasses = {
		open : ['open','aui-iconfont-expanded'],
		close : ['','aui-iconfont-collapsed']
};
var contentFilterObj = {
		sourceSelect : "#filterBox_source",
		levelSelect  : "#filterBox_level",
		groupSelect: "#filterBox_group",
		toolbar		 : "#conteFilterContainer",
		searchTextBox  : "#filter-search",
		filterBoxView : "#filterBox_View",
		filterSearchBtn : "#filter-search-btn",
		filterType	 :'',
		filterSubType:'',
		URL			 :'',
		
		init : function(){
			contentFilterObj.filterType	 ='';
			contentFilterObj.filterSubType	 ='';
			contentFilterObj.URL	 ='';
			
			this.enableMultiSelectForFilters(this.sourceSelect);
			this.enableMultiSelectForFilters(this.levelSelect);
			this.enableMultiSelectForFilters(this.groupSelect);
			this.enableMultiSelectForFilters(this.filterBoxView);
			$(this.toolbar).show();
			//$(this.searchTextBox).on("keyup",this.filterDataOnSearchBox);
			$(this.filterSearchBtn).on("click",this.filterData);
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				onChange : contentFilterObj.filterData
			});	
		},
		filterDataOnSearchBox : function(event){
			var searchStr = $(this).val();
			$("tr.shk-nugget").remove();
			$("tr.shk-segment").remove();
			$("tr.shk-chapter").removeClass("open");
			if(searchStr.trim()){
				$("tr.shk-chapter").hide();
				$("tr.shk-chapter[data-chapter-name*='"+searchStr+"' i]").show();
			} else {
				$("tr.shk-chapter").show();
			}
		},
		filterData : function(){
			if($(this.$select).attr("id") === "filetypes")
				return false;
			contentFilterObj.filterSubType = $(contentFilterObj.filterBoxView).val();
			treeViewObj.getChaptersList();
			if($(contentFilterObj.filterBoxView).val() || $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
				$("#warning-msg").show();
			} else {
				$("#warning-msg").hide();
			}
		},
		fnGetSelectedFiteredValue : function(ele){
			var arr =[];
			var selector = "option" ;
			if($(ele).val()!=null)
				selector = "option:selected";
			$(ele).find(selector).each(function(ind,option){
				if($(option).val()!="multiselect-all"){		
					arr.push(parseInt($(option).val()));
				}
			});
			return arr;
		},
}
var treeViewObj = {
		table : "#contentTreeView",
		inputFiles : "#segmentFileinput",
		init:function(){
			$(this.table).find("tbody").empty();
			$(document).on("click",".expand-icon",function(){
				var nodeType = $(this).data("type");
				if($(this).parents("tr").hasClass("open")){
					if(nodeType == 'shk-chapter'){
						$(this).parents("tr").nextUntil("."+nodeType).remove();
					} else {
						$("tr.shk-nugget").find(".move-handle").show();
						treeViewObj.enableNuggetSortable();
						$(this).parents("tr").nextUntil(".shk-chapter,."+nodeType).remove();
					}
				} else {
					if(nodeType == 'shk-chapter'){
						$(this).parents("tr").siblings().removeClass("open");
						$("tr.shk-nugget,tr.shk-segment").remove();
						treeViewObj.getChapterContent($(this).parents("tr").data("chapterid"));
					} else {
						$(this).parents("tr").siblings(".shk-nugget").removeClass("open");
						$("tr.shk-segment").remove();
						treeViewObj.getNuggetContent($(this).parents("tr").data("chapterid"),$(this).parents("tr").data("nuggetid"));
					}
				}
				$(this).parents("tr").toggleClass("open");
				$(this).find(".aui-icon").toggleClass("aui-iconfont-expanded aui-iconfont-collapsed");
			});
			$(".custom-filter").on("click",this.changeSubFilter);
			
			this.getChaptersList();
		},
		changeSubFilter : function(){
			$(this).addClass("enabled");
			$(this).siblings().removeClass("enabled");
			var subFilterType = $(this).data("filtertype");
			$(treeViewObj.table).find("tbody").empty();
			switch(subFilterType){
				case "total":
				case "totalUpload":
					treeViewObj.showTotalList();
					break;
				case "rejected": 
					treeViewObj.showRejectdList();
					break;
				case "published": 
					treeViewObj.showPublishedList();
					break;
				case "approvals": 
					treeViewObj.showApprovalsList();
					break;
				
				default : 
					break;
			}
		},
		showTotalList : function(){			
			treeViewObj.getChaptersList();
		},
		showRejectdList  :function(){
			
			contentFilterObj.filterSubType='rejected';
			
			treeViewObj.getChaptersList();
			
			/*this.showChapters(chapters,'open');
			$.each(chapters,function(index,chapter){
				treeViewObj.showNuggets(chapter.chapterId,chapter['nuggets'],'open');
				$.each(chapter['nuggets'],function(index,nugget){
					treeViewObj.showSegements(chapter.chapterId,nugget.nuggetId,nugget['segments']);
				});
			});*/
		},
		showPublishedList : function(){
			contentFilterObj.filterSubType='awaitingApprovals';//TODO-to be change on demand
			treeViewObj.getChaptersList();
		},
		showApprovalsList : function(){
			contentFilterObj.filterSubType='awaitingApprovals';
			treeViewObj.getChaptersList();
		},
		getChaptersList : function(){
			spinner.showSpinner();
			var ajaxData = {
				levelIds : contentFilterObj.fnGetSelectedFiteredValue(contentFilterObj.levelSelect),
				groupIds : contentFilterObj.fnGetSelectedFiteredValue(contentFilterObj.groupSelect),
				filterSubType:$(contentFilterObj.filterBoxView).val(),
				search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()
			};
			var responseData = shiksha.invokeAjax("shikshaplus/contentmanagement/chapter", ajaxData, "POST");
			if(responseData.response=="shiksha-200" ){

				$("[data-filtertype='total'] #count").text(responseData.totalChapters);
				$("[data-filtertype='totalUpload'] #count").text(responseData.totalUploaded);
				$("[data-filtertype='approvals'] #count").text(responseData.totalAwaiting);
				$("[data-filtertype='rejected'] #count").text(responseData.totalRejected);	
				//$("[data-filtertype='published'] #count").text(responseData.totalPublished);
				this.showChapters(responseData['chapters'],'close');
			}
			spinner.hideSpinner();
		},
		getChapterContent : function(chapterId){
			spinner.showSpinner();
						
			var ajaxData ={	"chapter":chapterId, "filterSubType":contentFilterObj.filterSubType, search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()	};
			var responseData =shiksha.invokeAjax("shikshaplus/contentmanagement/nugget", ajaxData, "POST");
			console.log(responseData);
			this.showNuggets(chapterId,responseData.shikshaNuggets,'close');
			spinner.hideSpinner();
		},
		getNuggetContent : function(chapterId,nuggetId){
			spinner.showSpinner();
			var ajaxData ={	"chapter":chapterId, "nugget" : nuggetId , "filterSubType":contentFilterObj.filterSubType, search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()};
			var responseData =shiksha.invokeAjax("shikshaplus/contentmanagement/segment", ajaxData, "POST");
			this.showSegements(chapterId,nuggetId,responseData.segments);
			spinner.hideSpinner();
		},
		showChapters : function(chapters,status){
			$(treeViewObj.table).find("tbody").empty();
			//$(contentFilterObj.searchTextBox).val("");
			if(!chapters || !chapters.length){
				return false;
			}
			$.each(chapters,function(index,chapter){
				var thumbnailUrl = "web-resources/images/thumbnail-placeholder.jpg";
				if(chapter.thumbnailUrl)
					thumbnailUrl = chapter.thumbnailUrl;
				
				var thumbNail='';
				if(cmPermissions.uploadThumbnail){
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" accept=".jpg,.png" class="thumbnail-file"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
				}else{
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"></div>';
				}
				
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				var otherCount = 0;
				$.each(chapter.nuggets,function(i, nugget){
					if(nugget.totalAwaiting || nugget.totalRejected || nugget.totalUnmapped)
						otherCount++;
				})
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						+'<div class="actions-count">'+otherCount+'</div></div>';
				
				$(treeViewObj.table).find("tbody").append('<tr class="shk-chapter '+filterClasses[status][0]+'" data-chapterid="'+chapter.chapterId+'" data-chapter-name="'+chapter.name+'">'
						+'<td><a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-chapter">'
						+'<div class="lms-log-status-section lms-side-center-far"><div class="lms-dropdown-toggle" href="#"><svg>'
						+'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#dropdown" href="web-resources/images/content-logs.svg#dropdown"></use></svg></div>'
						
						//+'<div class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						//+'<div class="actions-count" title="'+contentLabels.awaitingApproval+'">'+chapter.totalAwaiting+'</div></div></div>'
						
						+statusIcon+"</div>"					
						+thumbNail
						+'<div><div><div class="log-title-text">'+chapter.name+'</div></div></div>'
						+'</a></td>'
						+'<td class="count">Nug:'+chapter.noOfNuggets+'/Seg:'+chapter.noOfSegments+'</td><td>'
						+'<div class="shk-action-container log-list-actions">'
						+((cmPermissions.addNugget && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim())? '<a class="add-nugget" data-chapterid="'+chapter.chapterId+'">'+contentLabels.addNuggetBtn+'</a>' : "")
						+'</div></td></tr>');
				});
		},
		showNuggets : function(chapterId,nuggets,status){
			if(!nuggets || !nuggets.length){
				//$(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+chapterId+"']").last().after('<tr class="shk-nugget '+filterClasses[status][0]+'" data-chapterid="'+chapterId+'"><td colspan="3">No nuggets avilable</td></tr>');
				return false;
			}
			$.each(nuggets,function(index,nugget){
				var thumbnailUrl = "web-resources/images/thumbnail-placeholder.jpg";
				if(nugget.thumbnailUrl)
					thumbnailUrl = nugget.thumbnailUrl;
				
				
				var thumbNail='';
				if(cmPermissions.uploadThumbnail){
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" accept=".jpg,.png"  class="thumbnail-file"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
				}else{
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"></div>';
				}
				
				var otherCount		= nugget.totalAwaiting + nugget.totalRejected + nugget.totalUnmapped;
				
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						+'<div class="actions-count">'+otherCount+'</div></div>';
				
				var str = '<tr class="shk-nugget '+filterClasses[status][0]+'" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">'
						+'<td><a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-nugget">'
						+'<div class="lms-log-status-section lms-side-center-far"><div class="lms-dropdown-toggle" href="#"><svg>'
						+'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#dropdown" href="web-resources/images/content-logs.svg#dropdown"></use></svg></div>'
						
						//+'<div class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						//+'<div class="actions-count" title="'+contentLabels.awaitingApproval+'">'+nugget.totalAwaiting+'</div></div></div>'
						/*+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" accept=".jpg,.png"  class="thumbnail-file"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
						
						+statusIcon+"</div>"
						+thumbNail
						+'<div><div><div class="log-title-text">'+nugget.name+'</div></div></div>'
						+'</a></td>'
						+'<td class="seg-count">Seg:'+nugget.noOfSegments+'</td><td>'
						+'<div class="shk-action-container log-list-actions">'
						+((cmPermissions.addSegment && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim())? '<a class="add-segment" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">'+contentLabels.addSegmentBtn+'</a>' : "");

						var actions = "";
						if(true){
						actions+='<div class="dropdown">'
						actions+='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
							+'<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
							+'</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
							+'<li><a href="javascript:void(0)" class="view-nugget" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">'+contentLabels.viewBtn+'</a></li>';
						
							(cmPermissions.editNugget)? actions+='<li><a href="javascript:void(0)" class="edit-nugget" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'">'+contentLabels.editBtn+'</a></li>' : "";
							(cmPermissions.delNugget  && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()) ? actions+='<li><a href="javascript:void(0)" class="delete-nugget" data-chapterid="'+chapterId+'" data-nuggetid="'+nugget.nuggetId+'"  onclick="deleteNugget.initDelete(\''+nugget.nuggetId+'\',\''+nugget.name+'\',\''+chapterId+'\')">'+contentLabels.deleteBtn+'</a></li>':"";
							actions+='</ul></div>';
						}
						if(cmPermissions.editNugget && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
							str+=actions+'<span class="glyphicon glyphicon-move move-handle" title="'+contentLabels.moveBtn+'"></span></div></td></tr>';	
						}else{
							str+=actions+'</div></td></tr>';
						}
						
						
				$(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+chapterId+"']").last().after(str);
			});
			treeViewObj.enableNuggetSortable();
			
		},
		showSegements : function(chapterId,nuggetId,segments){
			if(!segments || !segments.length){
				return false;
			}
			$.each(segments,function(index,segment){
				var thumbnailUrl = "web-resources/images/thumbnail-placeholder.jpg";
				if(segment.thumbnailUrl)
					thumbnailUrl = segment.thumbnailUrl;
				
				
				var thumbNail='';
				if(cmPermissions.uploadThumbnail){
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" accept=".jpg,.png" class="thumbnail-file"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
				}else{
					thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"></div>';
				}
				
				
				
				
				var statusIcon = '<div title="'+statusMessage.unmappedStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg></div>';
				if(segment.isRejected){
					statusIcon = '<div title="'+statusMessage.rejectedStatus+'" class="log-status-icon log-status--rejected"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#rejected" href="web-resources/images/content-logs.svg#rejected"></use></svg></div>';
				} else if(segment.isApproved){
					statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				} else if(segment.isUploaded){
					statusIcon = '<div title="'+statusMessage.awaitingStatus+'" class="log-status-icon log-status--awaiting"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#awaiting" href="web-resources/images/content-logs.svg#awaiting"></use></svg></div>';
				}
				var str = '<tr class="shk-segment" data-nuggetid="'+nuggetId+'" data-segmentid="'+segment.shikshaPlusSegmentId+'">'
						+'<td><span class="log-title" href="javascript:void(0);" data-type="shk-segment">'
		                +'<div class="lms-log-status-section lms-side-center-far"><div></div>'
		              //  +'<div class="log-status-icon log-status--awaiting"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#awaiting" href="web-resources/images/content-logs.svg#awaiting"></use></svg></div>'
		                + statusIcon
		                +'</div>'
		                /*+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="'+thumbnailUrl+'"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" accept=".jpg,.png" class="thumbnail-file"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
		                +thumbNail
		                +'<div class="segment-title-container"><div><div class="title-segment">'+segment.shikshaPlusSegmentName+'('+segment.segmentType.segmentTypeName+')'+'</div><div class="log-version">'+segment.version+'</div></div></div></span>'
						+'</td><td>'
						+'<div class="duration-icon" title="'+contentLabels.durationBtn+'"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#durationx" href="web-resources/images/content-logs.svg#durationx"></use></svg></div> <span class="duration-span"></span>'
						+'<a href="#" id="duration'+segment.shikshaPlusSegmentId+'" data-type="combodate"  data-format="HH:mm:ss" data-viewformat="HH:mm:ss" data-minuteStep="1" data-template="HH : mm : ss" data-pk="1" data-title="'+contentLabels.editDuration+' (hh:mm:ss)" class="editable editable-click editable-open duration-edit" style="">'+segment.duration+'</a>'
						+'</td><td>'
						+'<div class="shk-action-container log-list-actions">'
						+((cmPermissions.previewContent)?'<a class="action-icon download-segment action-icon--play disabled-icon" target="_blank" href="shikshaplus/segment/'+segment.shikshaPlusSegmentId+'/download" title="'+contentLabels.downloadBtn+'"><div class="lms-play-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/all.svg#download-action" href="web-resources/images/all.svg#download-action"></use></svg></div></a>': '')
						+((cmPermissions.commentSegment || true)?'<a class="action-icon comment-segment disabled-icon" title="'+contentLabels.commentBtn+'"><div class="comment-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#comments" href="web-resources/images/content-logs.svg#comments"></use></svg></div></span></a>' : '')
						+((cmPermissions.revertContent)?'<a class="action-icon undo-segment disabled-icon" title="'+contentLabels.undoBtn+'"><div class="undo-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#undo" href="web-resources/images/content-logs.svg#undo"></use></svg></div></a>' : '')
						+((cmPermissions.approveContent)?'<a class="action-icon approve-segment action-icon--approve disabled-icon" title="'+contentLabels.approveBtn+'" data-segmentid="'+segment.shikshaPlusSegmentId+'"><div class="lms-approve-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#approve" href="web-resources/images/content-manager.svg#approve"></use></svg></div></a>' : '')
						+((cmPermissions.rejectContent)?'<a class="action-icon reject-segment action-icon--reject disabled-icon" title="'+contentLabels.rejectBtn+'" data-segmentid="'+segment.shikshaPlusSegmentId+'"><div class="lms-reject-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#reject" href="web-resources/images/content-manager.svg#reject"></use></svg></div></a>' : '');
						
				
				var actions = "";
				if(true){
					actions+='<div class="dropdown">'
						+'<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
						+'<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
						+'</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
						+'<li><a href="javascript:void(0)" class="view-segment" data-nuggetid="'+nuggetId+'" data-segmentid="'+segment.shikshaPlusSegmentId+'">'+contentLabels.viewBtn+'</a></li>';
						(cmPermissions.editSegment)? actions+='<li><a href="javascript:void(0)" class="edit-segment" data-nuggetid="'+nuggetId+'" data-segmentid="'+segment.shikshaPlusSegmentId+'">'+contentLabels.editBtn+'</a></li>' : '';
						(cmPermissions.viewLogs)? actions+='<li><a href="javascript:void(0)" class="view-logs-segment" onclick="logsObj.viewSegmentLogs(\''+segment.shikshaPlusSegmentId+'\',\''+segment.shikshaPlusSegmentName+'\')">'+contentLabels.viewLogs+'</a></li>':'';
						(cmPermissions.delSegment  && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim())? actions+='<li><a href="javascript:void(0)" class="delete-segment" onclick="deleteSegment.initDelete(\''+segment.shikshaPlusSegmentId+'\',\''+segment.shikshaPlusSegmentName+'\',\''+nuggetId+'\',\''+chapterId+'\')">'+contentLabels.deleteBtn+'</a></li>':'';
						actions+='</ul></div>';
				}
				
				if(cmPermissions.editSegment && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
					str+=actions+'<span class="glyphicon glyphicon-move move-handle" title="'+contentLabels.moveBtn+'"></span></div></td></tr>';	
				}else{
					str+=actions+'</div></td></tr>';
				}
				
				$(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+nuggetId+"']").last().after(str);
				var segmentRow = $(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+nuggetId+"'][data-segmentid='"+segment.shikshaPlusSegmentId+"']");
				if(segment.contentUrl){
					segmentRow.find(".download-segment").removeClass("disabled-icon");
				} 
				if (segment.contentUrl && !segment.isRejected && !segment.isApproved){
					segmentRow.find(".undo-segment,.approve-segment").removeClass("disabled-icon");
				}
				if ((segment.contentUrl && !segment.isRejected) || segment.isApproved){
					segmentRow.find(".reject-segment ").removeClass("disabled-icon");
				}
				if (segment.contentUrl && segment.rejectionComment){
					segmentRow.find(".comment-segment ").removeClass("disabled-icon");
				}
				treeViewObj.enableDurationEditable(segment.shikshaPlusSegmentId);
			});
			treeViewObj.enableSegmentSortable();
			
			treeViewObj.enableContentDropable();
		},
		enableContentDropable : function(){
			$(treeViewObj.table).find("tr.shk-segment").droppable({
				  accept: ".li-file",
				  drop: function( event, ui ) {
					  $(this).removeClass("drop-over");
					  var fileName = $(ui.draggable).data("filename");
					  var fileType = $(ui.draggable).data("type");
					  uploadSegment.mapFileToSegment($(this).data("segmentid"),fileName,fileType);
					  $(".ui-draggable-dragging").remove();
				  },
				  over: function( event, ui ) {
					  $(this).addClass("drop-over");
				  },
				  out : function(){
					  $(this).removeClass("drop-over");
				  }
			});
		},
		durationParamenters : function(params){
			console.log(params)
			return params;
		},
		enableDurationEditable : function(segId){
			$('#duration'+segId).editable({
				combodate: {
	                minuteStep: 1
	           },
	           send: 'never',
	           validate  : function(value){
					console.log($(this).text());
					console.log(parseInt(value['_i'].join("")));
					if(parseInt(value['_i'].join("")) <= 1000){
						return "Enter Valid duration";
					}
					
				},
				display : false,
		    }).on("save",function(e, params){
		    	spinner.showSpinner();
		    	var segmentId = $(e.target).parents("tr").data("segmentid");
		    	var updateDurationAjaxCall = shiksha.invokeAjax("shikshaplus/segment/"+segmentId+"?duration="+params.submitValue, null, "POST");
		    	
		    	if(updateDurationAjaxCall){
					if(updateDurationAjaxCall.response == "shiksha-200"){
						$("tr[data-segmentid='"+segmentId+"']").find(".duration-edit").text(updateDurationAjaxCall.duration);
							AJS.flag({
								type  : "success",
								title : appMessgaes.success,
								body  : updateDurationAjaxCall.responseMessage,
								close : 'auto'
							});					
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : updateDurationAjaxCall.responseMessage,
							close : 'auto'
						});
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
				spinner.hideSpinner();
		    });
		},
		clearFiles : function(){
			$("#segmentFileinput").val("");
			$(".segment-files-list").empty();
			$(".drag-drop-select").show();
			$(".drag-drop-clear").hide();
		},
		enableNuggetSortable : function(){
			$("tbody").sortable('destroy');
			$( "tbody" ).sortable({
				  axis: "y",
				  helper: "clone",
				  items: "> tr.shk-nugget",
				  handle : ".move-handle",
				  forcePlaceholderSize: true,
				  placeholder: "ui-sortable-placeholder",
				  revert: false,
				  stop: function( event, ui ) {
					  var chapterId = $(ui.item[0]).attr("data-chapterid");
					  var ajaxData = [];
					  $("tr.shk-nugget[data-chapterid='"+chapterId+"']").each(function(index,row){
						var tmpObj ={
									nuggetId : $(this).attr("data-nuggetid"),
									sequence : index + 1
						  };
						ajaxData.push(tmpObj);
					  });
					  
					  var updateSequence = shiksha.invokeAjax("shikshaplus/nugget/swapsequence", ajaxData, "POST");
					  console.log(updateSequence);
					  if(updateSequence){
						  if(updateSequence.response == "shiksha-200"){
							  AJS.flag({
									type  : "success",
									title :  appMessgaes.success,
									body  : updateSequence.responseMessage,
									close : 'auto'
								});
						  } else {
							  AJS.flag({
									type  : "error",
									title : appMessgaes.error,
									body  : updateSequence.responseMessage,
									close : 'auto'
								});
						  }
					  }  
					  spinner.hideSpinner();
					  
				  }
				  
			});
		},
		enableSegmentSortable : function(){
			$("tr.shk-nugget").find(".move-handle").hide();
			$("tbody").sortable('destroy');
			$( "tbody" ).sortable({
				  axis: "y",
				  helper: "clone",
				  items: "> tr.shk-segment",
				  handle : ".move-handle",
				  forcePlaceholderSize: true,
				  placeholder: "ui-sortable-placeholder",
				  revert: false,
				  stop: function( event, ui ) {
					  var nuggetId = $(ui.item[0]).attr("data-nuggetid");
					  var ajaxData = [];
					  $("tr.shk-segment[data-nuggetid='"+nuggetId+"']").each(function(index,row){
						var tmpObj ={
								shikshaPlusSegmentId : $(this).attr("data-segmentid"),
									sequence : index + 1
						  };
						ajaxData.push(tmpObj);
					  });
					  var updateSequence = shiksha.invokeAjax("shikshaplus/segment/swapsequence", ajaxData, "POST");
					  console.log(updateSequence);
					  if(updateSequence){
						  if(updateSequence.response == "shiksha-200"){
							  AJS.flag({
									type  : "success",
									title : "Success",
									body  : updateSequence.responseMessage,
									close : 'auto'
								});
						  } else {
							  AJS.flag({
									type  : "error",
									title : appMessgaes.error,
									body  : updateSequence.responseMessage,
									close : 'auto'
								});
						  }
					  }  
					  spinner.hideSpinner();
				  }
				  
			});
		}
};

var addSegment = {
	addSegmentBtn 	: ".add-segment",
	addSegmentModal	: "#mdlAddSegment",
	addSegmentForm 	: "#addSegmentForm",
	addSegmetNuggetId  : "#addSegmetNuggetId",
	addSegmentType  : "#addSegmentType",
	addContentType  : "#addContentType",
	addSegmentName 	: "#addSegmentName",
	addDescription	: "#addSegmentDescription",
	addResolution	: "#addResolution",
	init 		 	: function(){	
		$(document).on("click",this.addSegmentBtn,this.showAddSegemntModal);
		setOptionsForMultipleSelect(addSegment.addSegmentType);
		setOptionsForMultipleSelect(addSegment.addContentType);
		$(this.addContentType).on("change",this.fnContentCahnge);
		$(this.addSegmentForm).submit(function(e) {
			e.preventDefault();
		}).validate({
			ignore: '',
			rules: {
				segmentType : {
					required : true
				},
				contentType : {
					required : true
				},
				segmentName: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				},
				description: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				}	,
			},

			messages: {
				segmentType : {
					required : segmentMessages.segmentTypeRequired
				},
				contentType : {
					required : segmentMessages.contentTypeRequired
				},
				segmentName: {
					required: segmentMessages.nameRequired,
					noSpace:  segmentMessages.nameNoSpace,
					minlength: segmentMessages.nameMinLength,
					maxlength: segmentMessages.nameMaxLength

				},
				description: {
					required: segmentMessages.descRequired,
					noSpace:segmentMessages.descNoSpace,
					minlength: segmentMessages.descMinLength,
					maxlength: segmentMessages.descMaxLength
				},
			},
			errorPlacement: function(error, element) {
				$(element).parent().append(error);
			},
			submitHandler : function(){
				addSegment.fnAddSegment();
			}
		});
		
	},
	fnContentCahnge : function(){
		if($(this).find("option:selected").attr("data-name") == 'Flash')
			$("#addResolutionDiv").show();
		else
			$("#addResolutionDiv").hide();
	},
	fnAddSegment : function(){
		var ajaxData = {
				"shikshaPlusSegmentName"	: $(addSegment.addSegmentName).val().replace(/\s\s+/g, ' ').trim(),
				"description" 	: $(addSegment.addDescription).val(),
				"shikshaPlusNugget" : {
					"nuggetId" 		: $(addSegment.addSegmetNuggetId).val()
				},
				"segmentType"	: {
					"segmentTypeId"	: $(addSegment.addSegmentType).val()
				},
				"contentType" 	: {
						"contentTypeId"	: $(addSegment.addContentType).val()
				}
		}
		
		if($(addSegment.addContentType).find("option:selected").attr("data-name") == 'Flash')
			ajaxData.resolution =  $(addSegment.addResolution+":checked").val();
		
		var addAjaxCall = shiksha.invokeAjax("shikshaplus/segment", ajaxData, "POST");
		console.log(addAjaxCall);
		if(addAjaxCall != null){
			if(addAjaxCall.response == "shiksha-200"){
					$(addSegment.addSegmentModal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
					
					if($("tr.shk-nugget[data-nuggetid='"+addAjaxCall.shikshaPlusNugget.nuggetId+"']").hasClass("open")){
						var thumbNail='';
						if(cmPermissions.uploadThumbnail){
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
						}else{
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"></div>';
						}
						var str = '<tr class="shk-segment" data-nuggetid="'+addAjaxCall.shikshaPlusNugget.nuggetId+'" data-segmentid="'+addAjaxCall.shikshaPlusSegmentId+'">'
							+'<td><span class="log-title" href="javascript:void(0);" data-type="shk-segment">'
			                +'<div class="lms-log-status-section lms-side-center-far"><div></div><div class="log-status-icon log-status--partial">'
			                +'<svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg></div></div>'
			                /*+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
			                +thumbNail
			                +'<div class="segment-title-container"><div><div class="title-segment">'+addAjaxCall.shikshaPlusSegmentName+'('+addAjaxCall.segmentType.segmentTypeName+')'+'</div><div class="log-version">'+addAjaxCall.version+'</div></div></div></span>'
							+'</td><td>'
							+'<div class="duration-icon" title="'+contentLabels.durationBtn+'"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#durationx" href="web-resources/images/content-logs.svg#durationx"></use></svg></div> <span class="duration-span"></span>'
							+'<a href="#" id="duration'+addAjaxCall.shikshaPlusSegmentId+'" data-type="combodate"  data-format="HH:mm:ss" data-viewformat="HH:mm:ss" data-minuteStep="1" data-template="HH : mm : ss" data-pk="1" data-title="'+contentLabels.editDuration+' (hh:mm:ss)" class="editable editable-click editable-open duration-edit" style="">00:00:00</a>'
							+'</td><td>'
							+'<div class="shk-action-container log-list-actions">'
							+((cmPermissions.previewContent)?'<a class="action-icon download-segment action-icon--play disabled-icon" target="_blank" href="shikshaplus/segment/'+addAjaxCall.shikshaPlusSegmentId+'/download" title="'+contentLabels.downloadBtn+'"><div class="lms-play-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/all.svg#download-action" href="web-resources/images/all.svg#download-action"></use></svg></div></a>' : '')
							+((cmPermissions.commentSegment || true)?'<a class="action-icon comment-segment disabled-icon" title="'+contentLabels.commentBtn+'"><div class="comment-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#comments" href="web-resources/images/content-logs.svg#comments"></use></svg></div></span></a>' : '')
							+((cmPermissions.revertContent || true)?'<a class="action-icon undo-segment disabled-icon" title="'+contentLabels.undoBtn+'"><div class="undo-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#undo" href="web-resources/images/content-logs.svg#undo"></use></svg></div></a>' : '')
							+((cmPermissions.approveContent)?'<a class="action-icon approve-segment action-icon--approve disabled-icon" title="'+contentLabels.approveBtn+'" data-segmentid="'+addAjaxCall.shikshaPlusSegmentId+'"><div class="lms-approve-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#approve" href="web-resources/images/content-manager.svg#approve"></use></svg></div></a>' : '')
							+((cmPermissions.rejectContent)? '<a class="action-icon reject-segment action-icon--reject disabled-icon"  title="'+contentLabels.rejectBtn+'"   data-segmentid="'+addAjaxCall.shikshaPlusSegmentId+'"><div class="lms-reject-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-manager.svg#reject" href="web-resources/images/content-manager.svg#reject"></use></svg></div></a>' : '');
							
							var actions = "";
							if(cmPermissions.editNugget || cmPermissions.delNugget){
								actions+='<div class="dropdown">'
									+'<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
									+'<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
									+'</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
									+'<li><a href="javascript:void(0)" class="view-segment" data-nuggetid="'+addAjaxCall.shikshaPlusNugget.nuggetId+'" data-segmentid="'+addAjaxCall.shikshaPlusSegmentId+'">'+contentLabels.viewBtn+'</a></li>';
								(cmPermissions.editSegment)? actions+= '<li><a href="javascript:void(0)" class="edit-segment" data-nuggetid="'+addAjaxCall.shikshaPlusNugget.nuggetId+'" data-segmentid="'+addAjaxCall.shikshaPlusSegmentId+'">'+contentLabels.editBtn+'</a></li>' : '';
								(cmPermissions.viewLogs)? actions+='<li><a href="javascript:void(0)" class="view-logs-segment" onclick="logsObj.viewSegmentLogs(\''+addAjaxCall.shikshaPlusSegmentId+'\',\''+addAjaxCall.shikshaPlusSegmentName+'\')">'+contentLabels.viewLogs+'</a></li>':'';
								(cmPermissions.delSegment)? actions+= '<li><a href="javascript:void(0)" class="delete-segment" onclick="deleteSegment.initDelete(\''+addAjaxCall.shikshaPlusSegmentId+'\',\''+addAjaxCall.shikshaPlusSegmentName+'\',\''+addAjaxCall.shikshaPlusNugget.nuggetId+'\',\''+addAjaxCall.shikshaPlusNugget.shikshaPlusChapter.chapterId+'\')">'+contentLabels.deleteBtn+'</a></li>' : '';
								actions+='</ul></div>';
							}
							if(cmPermissions.editSegment && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
								str+=actions+'<span class="glyphicon glyphicon-move move-handle" title="'+contentLabels.moveBtn+'"></span></div></td></tr>';	
							}else{
								str+=actions+'</div></td></tr>';
							}
							
						$(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+addAjaxCall.shikshaPlusNugget.nuggetId+"']").last().after(str);	
					}
					treeViewObj.enableSegmentSortable();
				$(addSegment.addSegmetNuggetId).val(0);
				resetForm(addSegment.addSegmentForm);
				treeViewObj.enableDurationEditable(addAjaxCall.shikshaPlusSegmentId);
				treeViewObj.enableContentDropable();
				
				var nuggetId = $("tr[data-segmentid='"+addAjaxCall.shikshaPlusSegmentId+"']").attr("data-nuggetid");
				var chapterId = $("tr.shk-nugget[data-nuggetid='"+addAjaxCall.shikshaPlusNugget.nuggetId+"']").attr("data-chapterid");
				
				updateCountNuggetsAndChapter(chapterId, addAjaxCall.shikshaPlusNugget.nuggetId);
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : addAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type  : "error",
				title : appMessgaes.oops,
				body  : appMessgaes.serverError,
				close : 'auto'
			});
		}
		spinner.hideSpinner();
	},
	showAddSegemntModal : function(){
		$(addSegment.addSegmentType).val("");
		$(addSegment.addContentType).val("");
		shikshaPlus.fnGetSegmentTypes(addSegment.addSegmentType,[],0);
		shikshaPlus.fnGetContentTypes(addSegment.addContentType,0);
		$(addSegment.addSegmentType).multiselect("rebuild");
		$(addSegment.addContentType).multiselect("rebuild");
		resetForm(addSegment.addSegmentForm);
		$(addSegment.addSegmetNuggetId).val($(this).data("nuggetid"));
		$("#addResolutionDiv").hide();
		$("[id='groupDiv']").removeClass('disabled-select');
		$(addSegment.addSegmentModal).modal("show");
	}
};

var editSegment = {
		editSegmentBtn 	: ".edit-segment",
		editSegmentModal	: "#mdlEditSegment",
		editSegmentForm 	: "#editSegmentForm",
		editSegmentNuggetId  : "#editSegmentNuggetId",
		editSegmentType  : "#editSegmentType",
		editContentType  : "#editContentType",
		editSegmentName 	: "#editSegmentName",
		editDescription	: "#editSegmentDescription",
		editSegmentId : "#editSegmentId",
		editResolution:"#editResolution",
		init 		 	: function(){	
			$(document).on("click",this.editSegmentBtn,this.showEditSegemntModal);
			setOptionsForMultipleSelect(editSegment.editSegmentType);
			setOptionsForMultipleSelect(editSegment.editContentType);
			$(this.editContentType).on("change",this.fnContentCahnge);
			$(this.editSegmentForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					segmentType : {
						required : true
					},
					contentType : {
						required : true
					},
					segmentName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					description: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}	,
				},

				messages: {
					segmentType : {
						required : segmentMessages.segmentTypeRequired
					},
					contentType : {
						required : segmentMessages.contentTypeRequired
					},
					segmentName: {
						required: segmentMessages.nameRequired,
						noSpace:  segmentMessages.nameNoSpace,
						minlength: segmentMessages.nameMinLength,
						maxlength: segmentMessages.nameMaxLength

					},
					description: {
						required: segmentMessages.descRequired,
						noSpace:segmentMessages.descNoSpace,
						minlength: segmentMessages.descMinLength,
						maxlength: segmentMessages.descMaxLength
					},
				},
				errorPlacement: function(error, element) {
					$(element).parent().append(error);
				},
				submitHandler : function(){
					editSegment.fnEditSegment();
				}
			});
			
		},
		fnContentCahnge : function(){
			if($(this).find("option:selected").attr("data-name") == 'Flash'){
				$("#editResolutionDiv").show();
				$('input[id=editResolution][value="2"]').prop('checked',true);
			}else
				$("#editResolutionDiv").hide();
		},
		fnEditSegment : function(){
			var ajaxData = {
					
					"shikshaPlusSegmentId" 	: $(editSegment.editSegmentId).val(),
					"shikshaPlusSegmentName"	: $(editSegment.editSegmentName).val().replace(/\s\s+/g, ' ').trim(),
					"description" 	: $(editSegment.editDescription).val(),
					"shikshaPlusNugget" : {
						"nuggetId" 		: $(editSegment.editSegmentNuggetId).val()
					},
					"segmentType"	: {
						"segmentTypeId"	: $(editSegment.editSegmentType).val()
					},
					"contentType" 	: {
							"contentTypeId"	: $(editSegment.editContentType).val()
					}
			}
			
			if($(editSegment.editContentType).find("option:selected").attr("data-name") == 'Flash')
				ajaxData.resolution =  $(editSegment.editResolution+":checked").val();			
			
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/segment", ajaxData, "PUT");
			console.log(addAjaxCall);
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
						$(editSegment.editSegmentModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
						$(treeViewObj.table).find("tbody").find("tr[data-segmentid='"+addAjaxCall.shikshaPlusSegmentId+"']").find(".title-segment").text(addAjaxCall.shikshaPlusSegmentName+'('+addAjaxCall.segmentType.segmentTypeName+')');							
					$(addSegment.addSegmetNuggetId).val(0);
					resetForm(addSegment.addSegmentForm);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				});
			}
			spinner.hideSpinner();
		},
		showEditSegemntModal : function(){
			resetForm(addSegment.addSegmentForm);
			var nuggetId = $(this).data("nuggetid");
			var segmentId = $(this).data("segmentid")
			$(editSegment.editSegmentNuggetId).val(nuggetId);
			$(editSegment.editSegmentId).val(segmentId);
			
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/segment/"+segmentId, null, "GET");
			console.log(getAjaxCall);
			$(editSegment.editSegmentName).val(getAjaxCall.shikshaPlusSegmentName);
			$(editSegment.editDescription).val(getAjaxCall.description);
		//	$(editSegment.editResolution).val(getAjaxCall.resolution);
			$('input[id=editResolution][value="'+getAjaxCall.resolution+'"]').prop('checked',true);
			
			shikshaPlus.fnGetSegmentTypes(editSegment.editSegmentType,[],getAjaxCall.segmentType.segmentTypeId);
			shikshaPlus.fnGetContentTypes(editSegment.editContentType,getAjaxCall.contentType.contentTypeId);
			$(editSegment.editSegmentType).multiselect("rebuild");
			$(editSegment.editContentType).multiselect("rebuild");
			var segUrl=getAjaxCall.contentUrl;
			if(!getAjaxCall.isRejected && !getAjaxCall.isApproved){
				$("[id='groupDiv']").removeClass('disabled-select');
				
			} else{
				$("[id='groupDiv']").addClass('disabled-select');
			}
			
			if(getAjaxCall.contentType.name == "Flash")
				$("#editResolutionDiv").show();
			else 
				$("#editResolutionDiv").hide();
			
			$(editSegment.editSegmentModal).modal("show");
		}
	};

var approveSegemnt = {
		approveSegmentBtn 	: ".approve-segment",
		rejectSegmentBtn	: ".reject-segment",
		approveModal 		: "#mdlApproveSegment",
		rejectModal 		: "#mdlRejectSegment",
		approveConfirmBtn	: "#approveSegmentConfimBtn",
		rejectConfirmBtn	: "#rejectConrimBtn",
		approveSegmentId 	: "#approveSegmentId",
		rejectSegmentId 	: "#rejectSegmentId",
		rejectSegmentForm 	: "#rejectSegmentForm",
		rejectComment 		: "#rejectComment",
		init : function(){
			$(document).on("click",this.approveSegmentBtn,this.initApprove);
			$(document).on("click",this.rejectSegmentBtn,this.initReject);
			$(document).on("click",this.approveConfirmBtn,this.fnApprove);
			$(this.rejectSegmentForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				rules : {
					rejectComment : {
						required :true
					}
				},
				messages : {
					rejectComment : {
						required : segmentMessages.commentRequired
					}
				},
				submitHandler : function(){
					approveSegemnt.fnReject();
				}
			});
		},
		initApprove : function(){
			var name = $(this).closest("tr").find(".segment-title").text();
			var msg = $("#approveSegmentMsg").data("message");
			$("#approveSegmentMsg").html(msg.replace('@NAME',name));
			$(approveSegemnt.approveSegmentId).val($(this).data("segmentid"));
			$(approveSegemnt.approveModal).modal("show");
		},
		fnApprove : function(){
			var approveSegmentId = $(approveSegemnt.approveSegmentId).val();
			var ajaxData = {
					shikshaPlusSegmentId : approveSegmentId,
			};
			var approveAjaxCall = shiksha.invokeAjax("shikshaplus/contentmanagement/segment/approve", ajaxData, "POST");
			if(approveAjaxCall != null){
				if(approveAjaxCall.response == "shiksha-200"){
					$("tr[data-segmentid='"+approveSegmentId+"']").find(".reject-segment,.comment-segment").removeClass("disabled-icon");
					$("tr[data-segmentid='"+approveSegmentId+"']").find(".approve-segment,.undo-segment").addClass("disabled-icon");
					$("tr[data-segmentid='"+approveSegmentId+"']").find(".log-status-icon").removeClass("log-status--awaiting").addClass("log-status--approved").find("use").attr({
						"xlink:href" : "web-resources/images/content-logs.svg#approved",
						"href" : "web-resources/images/content-logs.svg#approved",
						"title" : statusMessage.approvedStatus
					});
					
					var nuggetId = $("tr[data-segmentid='"+approveSegmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					
					$(approveSegemnt.approveModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : approveAjaxCall.responseMessage,
							close : 'auto'
						});
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : approveAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},
		initReject	: function(){
			resetForm(approveSegemnt.rejectSegmentForm);
			$(approveSegemnt.rejectSegmentId).val($(this).data("segmentid"));
			$(approveSegemnt.rejectModal).modal("show");
			
		},
		fnReject : function(){
			var rejectSegmentId = $(approveSegemnt.rejectSegmentId).val();
			var comment = $(approveSegemnt.rejectComment).val();
			var ajaxData = {
					shikshaPlusSegmentId : rejectSegmentId,
					rejectionComment : comment
			};
			var rejectAjaxCall = shiksha.invokeAjax("shikshaplus/contentmanagement/segment/reject", ajaxData, "POST");
			if(rejectAjaxCall != null){
				if(rejectAjaxCall.response == "shiksha-200"){
					var nuggetId = $("tr[data-segmentid='"+rejectSegmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					$("tr.shk-segment").remove();
					treeViewObj.getNuggetContent(chapterId, nuggetId);
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					$(approveSegemnt.rejectModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : rejectAjaxCall.responseMessage,
							close : 'auto'
						});
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : rejectAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};

var deleteSegment  = {
		deleleteSegmentBtn 	: ".delete-segment",
		deleteSegmentModal 	: "#mdlDeleteSegment",
		deleteSegmentConfirmBtn 	: "#deleteSegmentButton",
		deleteSegmentId : "#deleteSegmentId",
		
		
		deleteSegmentNuggetId: "#deleteSegmentNuggetId",
		deleteSegmentChapterId: "#deleteSegmentChapterId",
		
		init : function(){
			$(this.deleteSegmentConfirmBtn).on("click",this.fnDeleteSegment);
		},
		initDelete : function(segmentId,name,nuggetId,chapterId){
			var msg = $("#deleteSegmentMessage").data("message");
			$("#deleteSegmentMessage").html(msg.replace('@NAME',name));
			$(deleteSegment.deleteSegmentId).val(segmentId);
			
			
			$(deleteSegment.deleteSegmentNuggetId).val(nuggetId);
			$(deleteSegment.deleteSegmentChapterId).val(chapterId);
			
			$(deleteSegment.deleteSegmentModal).modal("show");
		},
		fnDeleteSegment : function(){
			spinner.showSpinner();
			
			var deleteSegmentId = $(deleteSegment.deleteSegmentId).val();
			
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/segment/"+deleteSegmentId, null, "DELETE");
			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-200"){
					$("tr[data-segmentid='"+deleteSegmentId+"']").remove();
					$(deleteSegment.deleteSegmentModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : deleteAjaxCall.responseMessage,
							close : 'auto'
						});
							
						updateCountNuggetsAndChapter($(deleteSegment.deleteSegmentChapterId).val(),$(deleteSegment.deleteSegmentNuggetId).val())
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};

var addNugget  = {
	addNuggetBtn 	: ".add-nugget",
	addNuggetModal 	: "#mdlAddNugget",
	addNuggetForm	: "#addNuggetForm",
	addNuggetChapterId : "#addNuggetChapterId",
	eleName			: "#addNuggetName",
	eleDescription	: "#addDescription",
	eleSequence		: "#addNuggetSequence",
	init 			: function(){
		$(document).on("click",this.addNuggetBtn,this.showAddNuggetModal);
		
		$(this.addNuggetForm).submit(function(e) {
			e.preventDefault();
		}).validate({
			ignore: '',
			rules: {
				nuggetName: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				},
				nuggetDescription: {
					required: true,
					minlength: 1,
					maxlength: 255,
					noSpace:true,
				}
			},

			messages: {
				nuggetName: {
					required: nuggetMessages.nameRequired,
					noSpace: nuggetMessages.nameNoSpace,
					minlength: nuggetMessages.nameMinLength,
					maxlength: nuggetMessages.nameMaxLength,

				},
				nuggetDescription: {
					required: nuggetMessages.descRequired,
					minlength: nuggetMessages.descMinLength,
					maxlength: nuggetMessages.descMaxLength,
					noSpace: nuggetMessages.descNoSpace,
				}
			},
			errorPlacement: function(error, element) {
				$(element).parent().append(error);
			},
			submitHandler : function(){
				addNugget.fnAddNugget();
			}
		});
	},
	fnAddNugget : function(){
		spinner.showSpinner();
		var ajaxData = {
				
				"shikshaPlusChapter" : {
								"chapterId" : $(addNugget.addNuggetChapterId).val()
				},
				"name"		  	: $(addNugget.eleName).val().replace(/\s\s+/g, ' ').trim(),
				"description" 	: $(addNugget.eleDescription).val(),
		}
		var addAjaxCall = shiksha.invokeAjax("shikshaplus/nugget", ajaxData, "POST");
		
		if(addAjaxCall != null){
			if(addAjaxCall.response == "shiksha-200"){
					$(addNugget.addNuggetModal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
					
					if($("tr.shk-chapter[data-chapterid='"+addAjaxCall.shikshaPlusChapter.chapterId+"']").hasClass("open")){
					
						var thumbNail='';
						if(cmPermissions.uploadThumbnail){
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>';
						}else{
							thumbNail ='<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"></div>';
						}	
						var str = '<tr class="shk-nugget '+filterClasses["close"][0]+'" data-chapterid="'+addAjaxCall.shikshaPlusChapter.chapterId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">'
							+'<td><a class="expand-icon log-title" href="javascript:void(0);" data-type="shk-nugget">'
							+'<div class="lms-log-status-section lms-side-center-far"><div class="lms-dropdown-toggle" href="#"><svg>'
							+'<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#dropdown" href="web-resources/images/content-logs.svg#dropdown"></use></svg></div>'
							+'<div class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div></div>'
							/*+'<div class="lms-content-log-thumbnail-section"><img class="lms-content-log-thumbnail" src="web-resources/images/thumbnail-placeholder.jpg"><label class="lms-content-log-thumbnail-upload lms-side-center-center"><input type="file" class="thumbnail-file" accept=".jpg,.png"><div class="upload-icon"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#uploadx" href="web-resources/images/content-logs.svg#uploadx"></use></svg></div></label></div>'*/
							+thumbNail
							+'<div><div><div class="log-title-text">'+addAjaxCall.name+'</div></div></div>'
							+'</a></td>'
							+'<td class="seg-count">Seg:0</td><td>'
							+'<div class="shk-action-container log-list-actions">'
							+((cmPermissions.addSegment)? '<a class="add-segment" data-chapterid="'+addAjaxCall.shikshaPlusChapter.chapterId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">'+contentLabels.addSegmentBtn+'</a>' : "");
							
						var actions = "";
						if(cmPermissions.editNugget || cmPermissions.delNugget){
							actions+='<div class="dropdown">'
							actions+='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#">'
							actions+='<span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> '
							actions+='</a><ul id="contextMenu" class="dropdown-menu" role="menu">'
							actions+='<li><a href="javascript:void(0)" class="view-nugget" data-chapterid="'+addAjaxCall.shikshaPlusChapter.chapterId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">'+contentLabels.viewBtn+'</a></li>';
							
							(cmPermissions.editNugget)? actions+='<li><a href="javascript:void(0)" class="edit-nugget" data-chapterid="'+addAjaxCall.shikshaPlusChapter.chapterId+'" data-nuggetid="'+addAjaxCall.nuggetId+'">'+contentLabels.editBtn+'</a></li>' : "";
							(cmPermissions.delNugget) ? actions+='<li><a href="javascript:void(0)" class="delete-nugget" data-chapterid="'+addAjaxCall.shikshaPlusChapter.chapterId+'" data-nuggetid="'+addAjaxCall.nuggetId+'" onclick="deleteNugget.initDelete(\''+addAjaxCall.nuggetId+'\',\''+addAjaxCall.name+'\',\''+addAjaxCall.shikshaPlusChapter.chapterId+'\')">'+contentLabels.deleteBtn+'</a></li>': "";
							actions+='</ul></div>';
						}
						if(cmPermissions.editNugget && !$(contentFilterObj.filterBoxView).val() && !$(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()){
							str+=actions+'<span class="glyphicon glyphicon-move move-handle" title="'+contentLabels.moveBtn+'"></span></div></td></tr>';	
						}else{
							str+=actions+'</div></td></tr>';
						}
					
					
						var lastNuggetId = $(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+addAjaxCall.shikshaPlusChapter.chapterId+"']").last().attr("data-nuggetid")
						if(!lastNuggetId)
							$(treeViewObj.table).find("tbody").find("tr[data-chapterid='"+addAjaxCall.shikshaPlusChapter.chapterId+"']").last().after(str);
						else
							$(treeViewObj.table).find("tbody").find("tr[data-nuggetid='"+lastNuggetId+"']").last().after(str);
				}
				treeViewObj.enableNuggetSortable();
				$(addNugget.addNuggetChapterId).val(0);
				resetForm(addNugget.addNuggetForm);
				
				updateCountNuggetsAndChapter(addAjaxCall.shikshaPlusChapter.chapterId);				
				
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : addAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		} else {
			AJS.flag({
				type  : "error",
				title : appMessgaes.oops,
				body  : appMessgaes.serverError,
				close : 'auto'
			})
		}
		spinner.hideSpinner();
	},
	showAddNuggetModal : function(){
		resetForm(addNugget.addNuggetForm);
		$(addNugget.addNuggetChapterId).val($(this).data("chapterid"));
		$(addNugget.addNuggetModal).modal("show");
	}
};

var editNugget  = {
		editNuggetBtn 	: ".edit-nugget",
		editNuggetModal 	: "#mdlEditNugget",
		editNuggetForm	: "#editNuggetForm",
		editNuggetId 	: "#editNuggetId",
		editNuggetName	: "#editNuggetName",
		editDescription	: "#editDescription",
		editNuggetSequence	: "#editNuggetSequence",
		editNuggetChapterId : "#editNuggetChapterId",
		init 			: function(){
			$(document).on("click",this.editNuggetBtn,this.showEditNuggetModal);
			
			$(this.editNuggetForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					nuggetName: {
						required: true,
						noSpace:true,
						minlength: 1,
						maxlength: 255,
						
					},
					nuggetDescription: {
						required: true,
						noSpace:true,
						minlength: 1,
						maxlength: 255,
						
					}
				},

				messages: {
					nuggetName: {
						required: nuggetMessages.nameRequired,
						noSpace: nuggetMessages.nameNoSpace,
						minlength: nuggetMessages.nameMinLength,
						maxlength: nuggetMessages.nameMaxLength,

					},
					nuggetDescription: {
						required: nuggetMessages.descRequired,
						minlength: nuggetMessages.descMinLength,
						maxlength: nuggetMessages.descMaxLength,
						noSpace: nuggetMessages.descNoSpace,
					}
				},
				errorPlacement: function(error, element) {
					$(element).parent().append(error);
				},
				submitHandler : function(){
					editNugget.fnEditNugget();
				}
			});
		},
		fnEditNugget : function(){
			var ajaxData = {
					"shikshaPlusChapter" 			: {
						"chapterId" 		: $(editNugget.editNuggetChapterId).val()
					},
					"nuggetId"		: $(editNugget.editNuggetId).val(),
					"name"		  	: $(editNugget.editNuggetName).val().replace(/\s\s+/g, ' ').trim(),
					"description" 	: $(editNugget.editDescription).val(),
			}
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/nugget", ajaxData, "PUT");
			
			console.log(editAjaxCall);
			
			if(editAjaxCall != null){
				if(editAjaxCall.response == "shiksha-200"){
						$(editNugget.editNuggetModal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : editAjaxCall.responseMessage,
							close : 'auto'
						});
					$("tr[data-nuggetid='"+editAjaxCall.nuggetId+"'].shk-nugget .log-title-text").text(editAjaxCall.name);
					$(editAjaxCall.editNuggeId).val(0);
					resetForm(editNugget.editNuggetForm);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},
		showEditNuggetModal : function(){
			resetForm(editNugget.editNuggetForm);
			var nuggetId = $(this).data("nuggetid");
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/nugget/"+nuggetId, null, "GET");
			$(editNugget.editNuggetId).val(nuggetId);
			$(editNugget.editNuggetName).val(getAjaxCall.name);
			$(editNugget.editDescription).val(getAjaxCall.description);
			$(editNugget.editNuggetSequence).val(getAjaxCall.sequence);
			$(editNugget.editNuggetChapterId).val($(this).data("chapterid"));
			$(editNugget.editNuggetId).val($(this).data("nuggetid"));
			
			spinner.hideSpinner();
			
			$(editNugget.editNuggetModal).modal("show");
		}
	};

var deleteNugget  = {
		deleteNuggetBtn 	: ".delete-nugget",
		deleteNuggetModal 	: "#mdlDeleteNugget",
		deleteNuggetConfirmBtn 	: "#deleteNuggetButton",
		deleteNuggetId : "#deleteNuggetId",
		
		deleteNuggetChapterId:"#deleteNuggetChapterId",
		
		init : function(){
			$(this.deleteNuggetConfirmBtn).on("click",this.fnDeleteNugget);
		},
		initDelete : function(nuggetId,name,chapterId){
			var msg = $("#deleteNuggetMessage").data("message");
			$("#deleteNuggetMessage").html(msg.replace('@NAME',name));
			$(deleteNugget.deleteNuggetId).val(nuggetId);
			
			$(deleteNugget.deleteNuggetChapterId).val(chapterId);
			$(deleteNugget.deleteNuggetModal).modal("show");
		},
		fnDeleteNugget : function(){
			spinner.showSpinner();
			
			var deleteNuggetId = $(deleteNugget.deleteNuggetId).val();
			
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/nugget/"+deleteNuggetId, null, "DELETE");
			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-200"){
					$(deleteNugget.deleteNuggetModal).modal("hide");
					$("tr[data-nuggetid='"+deleteNuggetId+"']").remove();
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : deleteAjaxCall.responseMessage,
							close : 'auto'
						});
						updateCountNuggetsAndChapter($(deleteNugget.deleteNuggetChapterId).val());							
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};

var uploadSegment = {
	uploadActionBtn : ".upload-segment",
	uploadSegmentModal : "#uploadSegmentModal",
	inputFile : "#segmentFile",
	uploadSegmentIdBox : "#uploadSegemntId",
	uploadForm : "#uploadSegmentForm",
	refreshContentFiels : "#content-refresh",
	filetypes : "#filetypes",
	fileSearch : "#search-files",
	init : function(){
		
		$(document).on("click",this.uploadActionBtn,this.uploadSegmentInit);
		$(this.refreshContentFiels).on("click",this.getUnmappedfiles);
		$("#previewContentSegmentModal").on("hidden.bs.modal",function(){
			$(".content-preview-container").empty();
		});
		contentFilterObj.enableMultiSelectForFilters(this.filetypes);
		$(this.filetypes).on("change",this.getUnmappedfiles);
		$(this.fileSearch).on("keyup",this.searchFiles);
	},
	uploadSegmentInit : function(){
		$(uploadSegment.uploadSegmentIdBox).val($(this).parents("tr").data("segmentid"));
		$(uploadSegment.uploadSegmentModal).modal("show");
	},
	searchFiles : function(){
		var searchStr = $(this).val();
		if(searchStr.trim()){
			$(".li-file").hide();
			searchStr = searchStr.toLowerCase(); 
			$(".li-file").each(function(){
				var fileName = $(this).attr("data-filename");
				fileName = fileName.toLowerCase();
				if(fileName.indexOf(searchStr) > -1)
					$(this).show();
			});

		} else {
			$(".li-file").show();
		}
	},
	submitUploadFile : function(file){
		var formData = new FormData();
		formData.append( 'uploadfile',file );
		spinner.showUploadSpinner();
		
		if(file){
			$.ajax({
				url: "shikshaplus/content/upload",
				type: "POST",
				data: formData,
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				async : true,
				success: function (response) {
					spinner.hideUploadSpinner();
					console.log("successfully uploaded");
					if(response.response == "shiksha-200"){
						uploadSegment.getUnmappedfiles();
						//$(".segment-files-list").prepend("<li class='li-file' data-filename='"+file.name+"'>"+file.name+"</li>");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : response.responseMessage,
							close : 'auto'
						});	
						$(".li-file").draggable({
							refreshPositions: true,
							revert: true,
							helper: "clone"
						});
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : response.responseMessage,
							close : 'auto'
						});
					}
				},
				error: function (error) {
					spinner.hideUploadSpinner();
					console.log(error);
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					});
				}
			});
		}else{spinner.hideUploadSpinner();}
	},
	mapFileToSegment : function(segmentId,fileName,fileType){
		spinner.showSpinner();
		$.ajax({
			url: "shikshaplus/segment/"+segmentId+"/map?type="+fileType,
	        type: "POST",
	        contentType:"application/json",
			async : false,
	        data: JSON.stringify({fileName : fileName}),
	        success: function (response) {
	        	spinner.hideSpinner();
	        	console.log("successfully mapped",segmentId);
	        	if(response.response == "shiksha-200"){
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : response.responseMessage,
						close : 'auto'
					});
					var nuggetId = $("tr[data-segmentid='"+segmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					$("tr.shk-segment").remove();
					treeViewObj.getNuggetContent(chapterId, nuggetId);
					//$("tr.shk-segment[data-segmentid='"+segmentId+"']").find(".undo-segment,.download-segment,.reject-segment,.approve-segment").removeClass("disabled-icon");
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					if(fileType!=1){
						$(".li-file[data-filename='"+fileName+"']").remove();
					}
					
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : response.responseMessage,
					close : 'auto'
				});
			}
	        },
	        error: function (error) {
	          spinner.hideSpinner();
	          console.log(error);
	          AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
	          });
	        }
	      });
	},
	getUnmappedfiles :function(){
		$(".segment-files-list").empty();
		$(uploadSegment.fileSearch).val("");
		spinner.showSpinner();
		var fileType = $(uploadSegment.filetypes).val();
		var getAjaxCall = shiksha.invokeAjax("shikshaplus/content/files?type="+fileType, null, "GET");
		if(getAjaxCall.unmappedFiles.length){
			$.each(getAjaxCall.unmappedFiles,function(index,file){
				$(".segment-files-list").prepend("<li class='li-file' data-filename='"+file+"' data-type='"+fileType+"'>"+file+"</li>");
			});
		} else {
			
		}
		spinner.hideSpinner();
		$(".li-file").draggable({
			  refreshPositions: true,
			  revert: true,
			  helper: "clone"
		});
	}
}

var undoSegment = {
		undoActionBtn : ".undo-segment",
		undoSegmentModal : "#undoSegmentModal",
		UndoForm : "#undoSegmentForm",
		undoSegmentId : "#undoSegemntId",
		undoSegemntBtn : "#undoSegemntBtn",
		init : function(){
			$(document).on("click",this.undoActionBtn,this.undoSegmentInit);
			$(document).on("click",this.undoSegemntBtn,this.fnUndoSegmetnContent);
		},
		undoSegmentInit : function(){
			$(undoSegment.undoSegmentId).val($(this).parents("tr").data("segmentid"));
			$(undoSegment.undoSegmentModal).modal("show");
		},
		fnUndoSegmetnContent : function(){
			var undoSegmentId = $(undoSegment.undoSegmentId).val();
			var undoAjaxCall = shiksha.invokeAjax("shikshaplus/contentmanagement/segment/revert/"+undoSegmentId, null, "GET");
			if(undoAjaxCall != null){
				if(undoAjaxCall.response == "shiksha-200"){
					$(undoSegment.undoSegmentModal).modal("hide");
					var nuggetId = $("tr[data-segmentid='"+undoSegmentId+"']").attr("data-nuggetid");
					var chapterId = $("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").attr("data-chapterid");
					$("tr.shk-segment").remove();
					treeViewObj.getNuggetContent(chapterId, nuggetId);
					updateCountNuggetsAndChapter(chapterId, nuggetId);
					uploadSegment.getUnmappedfiles();
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : undoAjaxCall.responseMessage,
						close : 'auto'
					});
											
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : undoAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
			
		}
	};
var commentSegment = {
		commnetActionBtn : ".comment-segment",
		commentSegmentModal : "#commentSegmentModal",
		UndoForm : "#commentSegmentForm",
		commentSegemntId : "#commentSegemntId",
		commentList : "#list-comments",
		closeComments : "#close-comments",
		init : function(){
			$(document).on("click",this.commnetActionBtn,this.commentSegmentInit);
			$(this.closeComments).on("click",this.closeSegments);
		},
		commentSegmentInit : function(){
			$(commentSegment.commentList).empty();
			var segmentName = $(this).parents("tr").find(".title-segment").text();
			var duration 	= $(this).parents("tr").find("#duration").text();
			$("#comment-main-heading").text(segmentName);
			$("#comment-main-duration").text(duration);
			var commentSegemntId = $(this).parents("tr").data("segmentid");
			spinner.showSpinner();
			$("#logs-tab-container").hide();
			$("#comment-container").show();
			var commentAjaxCall = shiksha.invokeAjax("shikshaplus/contentmanagement/segment/comments/"+commentSegemntId, null, "GET");
			if(commentAjaxCall != null){
					$.each(commentAjaxCall,function(index,commentObj){
						var commentTmp = $("#comment-template").clone();
						$(commentTmp).removeAttr("id").show();
						$(commentTmp).find("#commenter-name").text(commentObj.rejectedBy);
						$(commentTmp).find("#comment-text").text(commentObj.rejectionComment);
						$(commentTmp).find("#comment-date").text(commentObj.rejectedDate);
						$(commentTmp).find("#comment-version").text(commentObj.version);
						$(commentSegment.commentList).append(commentTmp);
					});
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},
		closeSegments : function(){
			$("#logs-tab-container").show();
			$("#comment-container").hide();
		},
	};

var previewSegment = {
		previewActionBtn : ".preview-segment",
		previewSegmentModal : "#previewContentSegmentModal",
		previewForm : "#previewSegmentForm",
		previewSegemntId : "#previewSegemntId",
		init : function(){
			$(document).on("click",this.previewActionBtn,this.previewSegmentInit);
		},
		previewSegmentInit : function(){
			$(previewSegment.previewSegemntId).val($(this).parents("tr").data("segmentid"));
			$(previewSegment.previewSegmentModal).modal("show");
		},
	};

var logsObj = {
		
		logRefresh : "#log-refresh",
		logDownload : "#log-download",
		init : function(){
			$(this.logRefresh).on("click",this.refreshLogs);
			$(this.logDownload).on("click",this.downloadLogs);
		},
		refreshLogs : function(){
			spinner.showSpinner();
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/contentmanagement/logs", null, "GET");
			$("#logs-list").empty();
			
			$.each(getAjaxCall,function(index,log){
				var str = '<li class="log-item lms-log-item"><div class="lms-log-by">'
					+'<div class="lms-comment-person-icon log-user-icon"><svg> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#user" href="web-resources/images/content-logs.svg#user"></use></svg></div>'
					+'<span>'+log.logType+': '+log.modifiedBy+'</span></div><div class="lms-log-sg">Segment : '+log.segmentName+'</div><div class="lms-side-center lms-log-details">'
					+'<div class="lms-log-details-version">'+contentLabels.uploadVersion+log.version+'</div><div>'+log.date+'</div><div>'+log.time+'</div></div>'
					+'<div class="lms-log-status"> '+contentLabels.status+log.status+' </div></li>';
				$("#logs-list").append(str);
			});
			spinner.hideSpinner();
		},
		

		downloadLogs : function(){
			window.location = baseContextPath +"/shikshaplus/segment/logs/download";
		},
		
		viewSegmentLogs : function(segmentId){
			spinner.showSpinner();
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/segment/"+segmentId+"/logs", null, "GET");
			$("#logs-list").empty();
			$.each(getAjaxCall,function(index,log){
				var str = '<li class="log-item lms-log-item"><div class="lms-log-by">'
					+'<div class="lms-comment-person-icon log-user-icon"><svg> <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#user" href="web-resources/images/content-logs.svg#user"></use></svg></div>'
					+'<span>'+log.logType+': '+log.modifiedBy+'</span></div><div class="lms-log-sg">Segment : '+log.segmentName+'</div><div class="lms-side-center lms-log-details">'
					+'<div class="lms-log-details-version">'+contentLabels.uploadVersion+log.version+'</div><div>'+log.date+'</div><div>'+log.time+'</div></div>'
					+'<div class="lms-log-status"> '+contentLabels.status+log.status+' </div></li>';
				$("#logs-list").append(str);
			});
			$("#aui-uid-1").trigger("click");
			spinner.hideSpinner();
			
		}
		
};

var viewSegement = {
		viewModal : "#viewSegmentModal",
		commentAction : ".view-segment",
		init : function(){
			$(document).on("click",this.commentAction,this.fnViewSegment);
		},
		fnViewSegment : function(){
			var segmentId = $(this).data("segmentid");
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/segment/"+segmentId, null, "GET");
			$("#seg-type").text(getAjaxCall.segmentType.segmentTypeName);
			$("#seg-con-type").text(getAjaxCall.contentType.name);
			$("#seg-name").text(getAjaxCall.shikshaPlusSegmentName);
			$("#seg-desc").text(getAjaxCall.description);
			$("#seg-sequ").text(getAjaxCall.sequence);
			var res=getAjaxCall.resolution!=null?getAjaxCall.resolution==1?segmentMessages.sd:segmentMessages.hd:"N/A";
			if(getAjaxCall.contentType.name=="HTML"){
				res="N/A"
			}
			$("#seg-res").text(res);
			var content = "N/A";
			if(getAjaxCall.contentUrl)
				content = getAjaxCall.contentUrl
			$("#seg-content").text(content);
			
			$(viewSegement.viewModal).modal("show");
			spinner.hideSpinner();
		}
};

var viewNugget = {
		viewModal : "#viewNuggetModal",
		viewtAction : ".view-nugget",
		init : function(){
			$(document).on("click",this.viewtAction,this.fnViewNugget);
		},
		fnViewNugget : function(){
			var nuggetId = $(this).data("nuggetid");
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/nugget/"+nuggetId, null, "GET");
			console.log(getAjaxCall);
			$("#nugget-name").text(getAjaxCall.name);
			$("#nugget-desc").text(getAjaxCall.description);
			$("#nugget-sequ").text(getAjaxCall.sequence);
			$(viewNugget.viewModal).modal("show");
			spinner.hideSpinner();
		}
};

var tumbnailObj = {
		fileInput : ".thumbnail-file",
		init : function(){
			$(document).on("change",this.fileInput,this.uploadThumbnail)
		},
		uploadThumbnail : function(){
			var parentTR = $(this).closest("tr");
			var file = $(this)[0].files[0];
			var fileName = file.name.split(".");
			if(['jpg','JPG','png','PNG','jpeg','JPEG'].indexOf(fileName[fileName.length -1 ]) < 0){
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : segmentMessages.unsupportedImageFile,
					close : 'auto'
				});
				return false;
			}else if (this.files[0] && (this.files[0].size > 10485760)) {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : segmentMessages.unsupportedFileSize10MB,
					close : 'auto'
				});
				return false;
			}
			
			var type 	= $(this).closest("a").attr("data-type");
			var typeName = "";
			var id 		= 0;
			if(type == "shk-chapter"){
				typeName 	= "c";
				id		= $(this).closest("tr").attr("data-chapterid");
			} else if(type == "shk-nugget"){
				typeName 	= "n";
				id		= $(this).closest("tr").attr("data-nuggetid");
			} else {
				typeName 	= "s";
				id		= $(this).closest("tr").attr("data-segmentid");
			}
			
			spinner.showSpinner();
			var formData = new FormData();
			formData.append( 'uploadfile',file );
			$.ajax({
		        url: "shikshaplus/thumbnail/upload?type="+typeName+"&id="+id,
		        type: "POST",
		        data: formData,
		        enctype: 'multipart/form-data',
		        processData: false,
		        contentType: false,
		        cache: false,
		        success: function (response) {
		        	spinner.hideSpinner();
		        	if(response.response == "shiksha-200"){
		        		parentTR.find(".lms-content-log-thumbnail").attr("src",response.thumbnailUrl);
		        		AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : response.responseMessage,
							close : 'auto'
						});	
		        		$(".li-file").draggable({
		      			  refreshPositions: true,
		      			  revert: true,
		      			  helper: "clone"
		      		});
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : response.responseMessage,
							close : 'auto'
						});
					}
		        },
		        error: function (error) {
		          spinner.hideSpinner();
		          console.log(error);
		          AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
		          });
		        }
		    });
		}
};

function updateCountNuggetsAndChapter(chapterId, nuggetId){
	if(chapterId){	
		var ajaxData = {
				chapter:chapterId,
				filterSubType:$(contentFilterObj.filterBoxView).val(),
				search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()
		}
		var countChapterData=shiksha.invokeAjax("shikshaplus/contentmanagement/chapter/count", ajaxData, "POST")
		if(countChapterData.response == "shiksha-200"){
			if(countChapterData.chapters && countChapterData.chapters.length){
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				var otherCount = 0;
				$.each(countChapterData.chapters[0].nuggets,function(i, nugget){
					if(nugget.totalAwaiting || nugget.totalRejected || nugget.totalUnmapped)
						otherCount++;
				})
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
						+'<div class="actions-count" title="Awaiting approval">'+otherCount+'</div></div>';
				
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.count').text("Nug:"+countChapterData.chapters[0].noOfNuggets+"/Seg:"+countChapterData.chapters[0].noOfSegments);
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.log-status-icon').replaceWith(statusIcon);
			} else {
				var statusIcon = '<div class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.count').text("Nug:0/Seg:0");
				$(treeViewObj.table).find("tbody").find("tr.shk-chapter[data-chapterid='"+chapterId+"']").find('.log-status-icon').replaceWith(statusIcon);
			}
				
		}	
	}

	if(nuggetId){
		var ajaxData = {
				nugget: nuggetId,
				filterSubType:$(contentFilterObj.filterBoxView).val(),
				search : $(contentFilterObj.searchTextBox).val().replace(/\s\s+/g, ' ').trim()
		}
		var countNuggetData=shiksha.invokeAjax("shikshaplus/contentmanagement/nugget/count", ajaxData, "POST")
		if(countNuggetData.response == "shiksha-200"){
			
			if(countNuggetData.shikshaNuggets && countNuggetData.shikshaNuggets.length){	
				var nugget     = countNuggetData.shikshaNuggets[0]
				var otherCount = nugget.totalAwaiting + nugget.totalRejected + nugget.totalUnmapped;
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				
				if(otherCount)
					statusIcon = '<div title="'+statusMessage.partialStatus+'" class="log-status-icon log-status--partial"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#partially-approved" href="web-resources/images/content-logs.svg#partially-approved"></use></svg>'
					+'<div class="actions-count" title="Awaiting approval">'+otherCount+'</div></div>';
			
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.seg-count').text("Seg:"+nugget.noOfSegments);
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.log-status-icon').replaceWith(statusIcon);
			} else {
				var statusIcon = '<div title="'+statusMessage.approvedStatus+'" class="log-status-icon log-status--approved"><svg><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="web-resources/images/content-logs.svg#approved" href="web-resources/images/content-logs.svg#approved"></use></svg></div>';
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.seg-count').text("Seg:0");
				$(treeViewObj.table).find("tbody").find("tr.shk-nugget[data-nuggetid='"+nuggetId+"']").find('.log-status-icon').replaceWith(statusIcon);
			}
			
				
		}
	}
	
}

function resetForm(formId){
	$(formId).find("label.error").remove();
	$(formId).find(".error").removeClass("error");
	$(formId)[0].reset();
}

$( document ).ready(function() {
	
	$.fn.editable.defaults.mode = 'popup'; 
	contentFilterObj.init();
	addSegment.init();
	editSegment.init();
	addNugget.init();
	editNugget.init();
	uploadSegment.init();
	logsObj.init();
	viewSegement.init();
	viewNugget.init();
	$(document).on("click",'.lms-content-log-thumbnail-section',function(e){
		e.stopPropagation();
	});
	$("#segmentFileinput").on("change",function(){
			uploadSegment.submitUploadFile($(this).get(0).files[0]);				
	});
	
	$(".li-file").draggable({
		  refreshPositions: true,
		  revert: true,
		  helper: "clone"
	});
	
	$("#uploadSegmentForm").submit(function(e) {
		e.preventDefault();
	}).validate({
		rules : {
			segmentFile  : {
				required : true,
				extension :"mp4|mpeg|dat|swf"
				}
		},
		messages : {
			segmentFile  : {
				required : segmentMessages.fileRequired,
				extension : segmentMessages.incorrectFile,
				
			}
		},
		submitHandler : function(){
			uploadSegment.submitUploadFile($(uploadSegment.uploadSegmentIdBox).val(),$(uploadSegment.inputFile)[0].files[0]);
		}
	});
	treeViewObj.init();	
	deleteSegment.init();
	deleteNugget.init();
	approveSegemnt.init();
	undoSegment.init();
	commentSegment.init();
	previewSegment.init();
	if(cmPermissions.uploadContent){
		uploadSegment.getUnmappedfiles();
	}
	tumbnailObj.init();
	$("tbody").sortable();
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
	
});