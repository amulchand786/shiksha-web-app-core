var pageContextElements = {	
		addButton 						: '#addNewShikshaPlusSegment',
		table 							: '#shikshaPlusSegmentTable',
		toolbar							: "#toolbarShikshaPlusSegment"
};
var addModal ={
		form 			: '#addShikshaPlusSegmentForm',
		modal   		: '#mdlAddShikshaPlusSegment',
		saveMoreButton	: 'addShikshaPlusSegmentSaveMoreButton',
		saveButton		: 'addShikshaPlusSegmentSaveButton',
		eleName		   	: '#addSegmentName',
		eleDescription	: '#addDescription',
		eleSequence		: '#addNuggetSequence',
		eleSegmentType	: '#addSegmentType',
		eleContentType	: '#addContentType',
		eleNugget		: '#addNugget'
		
};
var editModal ={
			form	 : '#editShikshaPlusSegmentForm',
			modal	 : '#mdlEditShikshaPlusSegment',
			eleName	 : '#editShikshaPlusSegmentName',
			eleId	 : '#segmentId',
			eleDescription	: '#editDescription',
			eleSequence		: '#editShikshaPlusSegmentSequence',
			eleSource		: '#editSource',
			eleLevel		: '#editLevel',
			eleGroup		: '#editGroup',
			eleChapter		: '#editChapter',
			eleChapterDiv	: '#editChapterDiv',
			eleSegmentDiv	: '#SegmentDiv',
};
var deleteModal ={
			modal	: '#mdlDeleteShikshaPlusSegment',
			confirm : '#deleteShikshaPlusSegmentButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var ShikshaPlusSegmentIdForDelete;

var shikshaPlusSegmentFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var shikshaPlusSegment={
		name:'',
		shikshaPlusSegmentId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',shikshaPlusSegment.initAddModal);
			$(deleteModal.confirm).on('click',shikshaPlusSegment.deleteFunction);
		},
		deleteFunction : function(){
			shikshaPlusSegment.fnDelete(ShikshaPlusSegmentIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			shikshaPlusSegment.resetForm(addModal.form);
			shikshaPlus.fnGetSegmentTypesForShikshaPlusSegment(addModal.eleSegmentType,0);
			setOptionsForMultipleSelect(addModal.eleSegmentType);
			$(addModal.eleSegmentType).multiselect("refresh");
			shikshaPlus.fnGetContentTypes(addModal.eleContentType,0);
			setOptionsForMultipleSelect(addModal.eleContentType);
			$(addModal.eleContentType).multiselect("refresh");
			shikshaPlus.fnGetNuggets(addModal.eleNugget,0);
			setOptionsForMultipleSelect(addModal.eleNugget);
			$(addModal.eleNugget).multiselect("refresh");
			$(addModal.modal).modal("show");
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		showHideGroups: function(){
			shikshaPlus.fnGetGroupsByLevelId(addModal.eleGroup,0,$(addModal.eleLevel).val());
			setOptionsForMultipleSelect(addModal.eleGroup);
			$(addModal.eleGroup).multiselect("rebuild");
			$(addModal.eleGroupDiv).show();
		},
		showHideChapters: function(){
			shikshaPlus.fnGetChapters(addModal.eleChapter,0,$(addModal.eleLevel).val(),$(addModal.eleGroup).val());
			setOptionsForMultipleSelect(addModal.eleChapter);
			$(addModal.eleChapter).multiselect("rebuild");
			$(addModal.eleChapterDiv).show();
		},
		showHideSegment: function(){
			$(addModal.eleSegmentDiv).show();
		},
		showHideGroupsInEdit: function(){
			shikshaPlus.fnGetGroupsByLevelId(editModal.eleGroup,0,$(editModal.eleLevel).val());
			setOptionsForMultipleSelect(editModal.eleGroup);
			$(editModal.eleGroup).multiselect("rebuild");
			shikshaPlus.fnGetChapters(editModal.eleChapter,0,0,0);
			setOptionsForMultipleSelect(editModal.eleGroup);
			$(editModal.eleChapter).multiselect("rebuild");
		},
		showHideChaptersInEdit: function(){
			shikshaPlus.fnGetChapters(editModal.eleChapter,0,$(editModal.eleLevel).val(),$(editModal.eleGroup).val());
			setOptionsForMultipleSelect(editModal.eleChapter);
			$(editModal.eleChapter).multiselect("rebuild");
			
		},
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"shikshaPlusSegmentName"	  : $(addModal.eleName).val(),
					"description" 				  : $(addModal.eleDescription).val(),
					"shikshaPlusNuggetSequence"	  : $(addModal.eleSequence).val(),
					"shikshaPlusNugget"		  : {"nuggetId": $(addModal.eleNugget).val()},
					"segmentType" 			 : {"segmentTypeId": $(addModal.eleSegmentType).val()},
					"contentType" 			 : {"contentTypeId": $(addModal.eleContentType).val()},
					
			}
			console.log(ajaxData);
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/segment", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : "Success!",
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					shikshaPlusSegment.resetForm(addModal.form);
					$(addModal.eleSegmentType).multiselect("rebuild");
					$(addModal.eleContentType).multiselect("rebuild");
					shikshaPlusSegment.refreshTable(pageContextElements.table);
					$(addModal.modal).find("#divSaveAndAddNewMessage").show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(shikshaPlusSegmentId){
			shikshaPlusSegment.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/segment/"+shikshaPlusSegmentId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.name);
					$(editModal.eleId).val(editAjaxCall.SegmentId);
					$(editModal.eleDescription).val(editAjaxCall.description);
					$(editModal.eleSequence).val(editAjaxCall.sequence);
					shikshaPlus.fnGetSources(editModal.eleSource,editAjaxCall.shikshaPlusSource.sourceId);
					setOptionsForMultipleSelect(editModal.eleSource);
					shikshaPlus.fnGetLevels(editModal.eleLevel,editAjaxCall.level.levelId);
					setOptionsForMultipleSelect(editModal.eleLevel);
					shikshaPlus.fnGetGroupsByLevelId(editModal.eleGroup,editAjaxCall.group.groupId,$(editModal.eleLevel).val());
					setOptionsForMultipleSelect(editModal.eleGroup);
					shikshaPlus.fnGetChapters(editModal.eleChapter,editAjaxCall.shikshaPlusChapter.chapterId,$(editModal.eleLevel).val(),$(editModal.eleGroup).val());
					setOptionsForMultipleSelect(editModal.eleChapter);
					$(editModal.eleSource).multiselect("rebuild");
					$(editModal.eleLevel).multiselect("rebuild");
					$(editModal.eleGroup).multiselect("rebuild");
					$(editModal.eleChapter).multiselect("rebuild");
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"		:$(editModal.eleName).val(),
					"SegmentId"	:$(editModal.eleId).val(),
					"sequence"	  : $(editModal.eleSequence).val(),
					"description" : $(editModal.eleDescription).val(),
					"shikshaPlusSource" : { "sourceId": $(editModal.eleSource).val() },
					"shikshaPlusChapter" : {"chapterId": $(editModal.eleChapter).val()},
					"level" 			 : {"levelId": $(editModal.eleLevel).val()},
					"group" 			 : {"groupId": $(editModal.eleGroup).val()},
			}

			var updateAjaxCall = shiksha.invokeAjax("shikshaplus/segment", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					shikshaPlusSegment.resetForm(editModal.form);
					shikshaPlusSegment.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: "Success!",
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: "Error..!",
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(shikshaPlusSegmentId,name){
			var msg = $("#deleteShikshaPlusSegmentMessage").data("message");
			$("#deleteShikshaPlusSegmentMessage").html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			ShikshaPlusSegmentIdForDelete=shikshaPlusSegmentId;
		},
		fnDelete :function(shikshaPlusSegmentId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/segment/"+shikshaPlusSegmentId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					shikshaPlusSegment.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : "Success!",
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					source:{
						required:true,
					},
					level:{
						required:true,
					},
					group:{
						required:true,
					},
					chapter:{
						required:true,
					},
					shikshaPlusSegmentName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					shikshaPlusSegmentSequence:{
						required: true,
						noSpace:true,
					},
					shikshaPlusSegmentDescription: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}
				},

				messages: {
					/*source:{
						required:segmentMessages.source,
					},
					level:{
						required:SegmentMessages.level,
					},
					group:{
						required:SegmentMessages.group,
					},
					chapter:{
						required:segmentMessages.chapter,
					},
					shikshaPlusSegmentName: {
						required: segmentMessages.nameRequired,
						noSpace: segmentMessages.nameNoSpace,
						minlength: segmentMessages.nameMinLength,
						maxlength: segmentMessages.nameMaxLength,

					},
					shikshaPlusSegmentSequence:{
						required: segmentMessages.sequenceRequiered,
						noSpace:segmentMessages.sequenceRequiered,
					},
					shikshaPlusSegmentDescription: {
						required: segmentMessages.descriptionRequired,
						minlength: segmentMessages.descriptionMinLength,
						maxlength: segmentMessages.descriptionMaxLength,
						noSpace: segmentMessages.descriptionNoSpace,
					}*/
				},
				errorPlacement: function(error, element) {
					$(element).parent().append(error);
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						shikshaPlusSegment.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						shikshaPlusSegment.fnUpdate();
					}
				}
			});
		},
		shikshaPlusSegmentActionFormater: function(value, row) {
			
			return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
			+'<li><a onclick="shikshaPlusSegment.initEdit(\''+row.segmentId+'\')" href="javascript:void(0)">Edit</a></li>'
			+'<li><a onclick="shikshaPlusSegment.initDelete(\''+row.segmentId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">Delete</a></li>'
			+'</ul>';
		},
		shikshaPlusSegmentNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	shikshaPlusSegment.formValidate(addModal.form);
	shikshaPlusSegment.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	shikshaPlusSegment.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar ,
		url : "shikshaplus/segment",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		formatShowingRows : shikshaPlusSegmentFilterObj.formatShowingRows,
		clickToSelect: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});