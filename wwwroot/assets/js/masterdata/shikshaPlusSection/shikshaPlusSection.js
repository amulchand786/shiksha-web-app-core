var pageContextElements = {	
		addButton 						: '#addNewShikshaPlusSection',
		table 							: '#shikshaPlusSectionTable',
};
var addModal ={
		form 			: '#addShikshaPlusSectionForm',
		modal   		: '#mdlAddShikshaPlusSection',
		saveMoreButton	: '#addShikshaPlusSectionSaveMoreButton',
		saveButton		: '#addShikshaPlusSectionSaveButton',
		eleName		   	: '#addShikshaPlusSectionName',
		
};
var editModal ={
			form	 : '#editShikshaPlusSectionForm',
			modal	 : '#mdlEditShikshaPlusSection',
			eleName	 : '#editShikshaPlusSectionName',
			eleId	 : '#shikshaPlusSectionId',
			
			
};
var deleteModal ={
			modal	: '#mdlDeleteShikshaPlusSection',
			confirm : '#deleteShikshaPlusSectionButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var ShikshaPlusSectionIdForDelete;

var shikshaPlusSectionFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(value, row, index, field){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var shikshaPlusSection={
		name:'',
		shikshaPlusSectionId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',shikshaPlusSection.initAddModal);
			$(deleteModal.confirm).on('click',shikshaPlusSection.deleteFunction);

		},
		deleteFunction : function(){
			shikshaPlusSection.fnDelete(ShikshaPlusSectionIdForDelete);
		},
		initAddModal :function(){
			shikshaPlusSection.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"sectionName":$(addModal.eleName).val(),
					
			}
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/section", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == "addShikshaPlusSectionSaveButton"){
						$(addModal.modal).modal("hide");
					}
					if(submitBtnId == "addShikshaPlusSectionSaveMoreButton"){
						$(addModal.modal).modal("show");
					}
					shikshaPlusSection.resetForm(addModal.form);
					shikshaPlusSection.refreshTable(pageContextElements.table);

					AJS.flag({
						type  : "success",
						title : appMessgaes.saved,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(shikshaPlusSectionId){
			shikshaPlusSection.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/section/"+shikshaPlusSectionId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.sectionName);
					$(editModal.eleId).val(editAjaxCall.sectionId);
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"sectionName":$(editModal.eleName).val(),
					"sectionId":$(editModal.eleId).val(),
					
			}

			var updateAjaxCall = shiksha.invokeAjax("shikshaplus/section", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					shikshaPlusSection.resetForm(editModal.form);
					shikshaPlusSection.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: appMessgaes.updated,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: "Error..!",
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(shikshaPlusSectionId){
			$(deleteModal.modal).modal("show");
			ShikshaPlusSectionIdForDelete=shikshaPlusSectionId;
		},
		fnDelete :function(shikshaPlusSectionId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/section/"+shikshaPlusSectionId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					shikshaPlusSection.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.deleted,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					shikshaPlusSectionName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}
				},

				messages: {
					shikshaPlusSectionName: {
						required: "Please enter Section name",
						noSpace:"Please enter valid Section Name",
						minlength: "The Section name must be more than 2 and less than 50 characters long",
						maxlength: "The Section name must be more than 2 and less than 50 characters long",

					}
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						shikshaPlusSection.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						shikshaPlusSection.fnUpdate();
					}
				}
			});
		},
		shikshaPlusSectionActionFormater: function(value, row, index) {
			console.log(value, row, index);
			/*
			var actionBtns = '<a rel="tooltip" data-tooltip="true" data-original-title="Edit" class="tooltip tooltip-top" onclick="shikshaPlusSection.initEdit(\''+row.SectionId+'\')"> <span class="tooltiptext">Edit</span><span class="glyphicon glyphicon-edit font-size12"></span> </a>'
			+ '<a style="margin-left:10px;" class="tooltip tooltip-top" onclick="shikshaPlusSection.initDelete(\''+row.SectionId+'\')"><span class="tooltiptext">Delete</span><span class="glyphicon glyphicon-trash font-size12"></span></a>';
*/
			return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
			+'<li><a onclick="shikshaPlusSection.initEdit(\''+row.sectionId+'\')" href="javascript:void(0)">Edit</a></li>'
			+'<li><a onclick="shikshaPlusSection.initDelete(\''+row.sectionId+'\')" href="javascript:void(0)" class="delLink">Delete</a></li>'
			+'</ul>';
		},
		shikshaPlusSectionNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	shikshaPlusSection.formValidate(addModal.form);
	shikshaPlusSection.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	shikshaPlusSection.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: "#toolbarShikshaPlusSection",
		url : "shikshaplus/section",
		method : "get",
		toolbarAlign :"right",
		search: true,
		sidePagination: "server",
		showToggle: false,
		showColumns: true,
		pagination: true,
		searchAlign: 'left',
		pageSize: 50,
		formatShowingRows : shikshaPlusSectionFilterObj.formatShowingRows,
		clickToSelect: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});