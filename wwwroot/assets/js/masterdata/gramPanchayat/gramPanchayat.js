//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForGP =[];
var allInputEleNamesOfFilter =[]; 
var btnAddMoreGPId;
var deleteGPId;
var elementIdMap;
var selectedIds;
var editPanchayatId;
var deleteGramPanchayatId;
var deletePanchayatName;
var thLabels;
//initialize global variables
var initGloabalVarsGP =function(){
	resetButtonObj='';
	allInputEleNamesForGP=['addGramPanchayatName'];
	allInputEleNamesOfFilter=['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId'];
	btnAddMoreGPId ='';
	deleteGPId='';
	elementIdMap={};
	selectedIds=[];
	editPanchayatId=0;
	deleteGramPanchayatId=0;
	deletePanchayatName='';	
}


//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}


//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddGramPanchayat #statesForZone',
			"Zone" : '#mdlAddGramPanchayat #selectBox_zonesByState',
			"District" : '#mdlAddGramPanchayat #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddGramPanchayat #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddGramPanchayat #selectBox_blocksByTehsil',
			"Nyaya Panchayat" : '#mdlAddGramPanchayat #selectBox_npByBlock',
		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
			"State" : '#mdlEditGramPanchayat #statesForZone',
			"Zone" : '#mdlEditGramPanchayat #selectBox_zonesByState',
			"District" : '#mdlEditGramPanchayat #selectBox_districtsByZone',
			"Tehsil" : '#mdlEditGramPanchayat #selectBox_tehsilsByDistrict',
			"Block" : '#mdlEditGramPanchayat #selectBox_blocksByTehsil',
			"Nyaya Panchayat" : '#mdlEditGramPanchayat #selectBox_npByBlock',
		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};


//create a row after save
var getGramPanchayatRow =function(gramPanchayat){
	var row = 
		"<tr id='"+gramPanchayat.gramPanchayatId+"' data-id='"+gramPanchayat.gramPanchayatId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+gramPanchayat.stateId+"'	title='"+gramPanchayat.stateName+"'>"+gramPanchayat.stateName+"</td>"+
		"<td data-zoneid='"+gramPanchayat.zoneId+"'	title='"+gramPanchayat.zoneName+"'>"+gramPanchayat.zoneName+"</td>"+
		"<td data-districtid='"+gramPanchayat.districtId+"' title='"+gramPanchayat.districtName+"' >"+gramPanchayat.districtName+"</td>"+
		"<td data-districtid='"+gramPanchayat.tehsilId+"' title='"+gramPanchayat.tehsilName+"' >"+gramPanchayat.tehsilName+"</td>"+
		"<td data-districtid='"+gramPanchayat.blockId+"' title='"+gramPanchayat.blockName+"' >"+gramPanchayat.blockName+"</td>"+
		"<td data-districtid='"+gramPanchayat.nyayPanchaytId+"' title='"+gramPanchayat.nyayPanchayatName+"' >"+gramPanchayat.nyayPanchayatName+"</td>"+
		"<td data-districtid='"+gramPanchayat.gramPanchayatId+"' title='"+gramPanchayat.gramPanchaytName+"' >"+gramPanchayat.gramPanchayatName+"</td>"+
		"<td data-distcode='"+gramPanchayat.gramPanchayatCode+"' title='"+gramPanchayat.gramPanchayatCode+"'>"+gramPanchayat.gramPanchayatCode+"</td>"+		

		"<td><div class='div-tooltip'><a class='tooltip tooltip-top'	  id='btnEditGramPanchayat'" +
		"onclick=editGramPanchayatData(&quot;"+gramPanchayat.stateId+"&quot;,&quot;"+gramPanchayat.zoneId+"&quot;,&quot;"+gramPanchayat.districtId+"&quot;,&quot;"+gramPanchayat.tehsilId+"&quot;,&quot;"+gramPanchayat.blockId+"&quot;,&quot;"+gramPanchayat.nyayPanchayatId+"&quot;,&quot;"+gramPanchayat.gramPanchayatId+"&quot;,&quot;"+escapeHtmlCharacters(gramPanchayat.gramPanchayatName)+"&quot;);  ><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top'	 id=deleteGramPanchayat	onclick=deletePanchayatData(&quot;"+gramPanchayat.gramPanchayatId+"&quot;,&quot;"+escapeHtmlCharacters(gramPanchayat.gramPanchayatName)+"&quot;);  ><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	
	return row;
}


var applyPermissions=function(){
	if (!editGPPermission){
	$("#btnEditGramPanchayat").remove();
		}
	if (!deleteGPPermission){
	$("#deleteGramPanchayat").remove();
		}
	}

// smart filter utility--- start here

var initSmartFilter = function(type) {
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){		
		resetFormValidatonForLocFilters("addPanchayatForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addPanchayatForm",allInputEleNamesForGP);
		displaySmartFilters("add")
		fnCollapseMultiselect();
	}
	else{	
		resetFvForAllInputExceptLoc("editPanchayatForm",['editPanchayatName']);
		displaySmartFilters("edit")
		fnCollapseMultiselect();
	}
}



//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	if($(resetButtonObj).parents("form").attr("id")=="addPanchayatForm"){		
		resetFormValidatonForLocFilters("addPanchayatForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");
	}else{		
		resetFormValidatonForLocFilters("addPanchayatForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
	}
	fnCollapseMultiselect();

}



// reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addPanchayatForm');
	resetFormById('editPanchayatForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}

// smart filter utility--- end here

// perform CRUD

// add GP
var addPanchayat = function(event) {
	$('.outer-loader').show();
	var nyayPanchayatId = $('#mdlAddGramPanchayat #selectBox_npByBlock').val();
	var panchayatName = $('#mdlAddGramPanchayat #addGramPanchayatName').val().trim().replace(/\u00a0/g," ");
	var json = {
		"nyayPanchayatId" : nyayPanchayatId,
		"gramPanchayatName" : panchayatName,
	};
	
	

	//update data table without reloading the whole table
	//save&add new
	btnAddMoreGPId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var gramPanchayat = customAjaxCalling("gramPanchayat", json, "POST");
	if(gramPanchayat!=null){
	if(gramPanchayat.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		var newRow =getGramPanchayatRow(gramPanchayat);
		appendNewRow("tblPanchayat",newRow);
		applyPermissions();		
		fnUpdateDTColFilter('#tblPanchayat',[2,4,5,6],10,[0,9],thLabels);
		setShowAllTagColor(colorClass);
		if(btnAddMoreGPId=="addGramPanchayatSaveMoreButton"){
			$("#mdlAddGramPanchayat").find('#errorMessage').hide();
			resetFvForAllInputExceptLoc("addPanchayatForm",allInputEleNamesForGP);
			fnDisplaySaveAndAddNewElement('#mdlAddGramPanchayat',panchayatName);		
		}else{			
			$('#mdlAddGramPanchayat').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: appMessgaes.success,
				    body: '<p>'+ gramPanchayat.responseMessage+'</p>',
				    close :'auto'
				 })
				}, 1000);
			
		}		
	}else {
		showDiv($('#mdlAddGramPanchayat #alertdiv'));
		$("#mdlAddGramPanchayat").find('#successMessage').hide();
		$("#mdlAddGramPanchayat").find('#okButton').hide();
		$("#mdlAddGramPanchayat").find('#errorMessage').show();
		$("#mdlAddGramPanchayat").find('#exception').show();
		$("#mdlAddGramPanchayat").find('#buttonGroup').show();
		$("#mdlAddGramPanchayat").find('#exception').text(gramPanchayat.responseMessage);
	}	
	}
	$(".outer-loader").hide();
	
};





// edit
// get data for edit
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editTehsilId = 0;
var editBlockId = 0;
var editNyayPanchayatId = 0;
var editGramPanchayatId = 0;
var editGramPanchayatData = function() {
	
	initSmartFilter("edit");
	var idMap = {
		'#mdlEditGramPanchayat #statesForZone' : arguments[0],
		'#mdlEditGramPanchayat #selectBox_zonesByState' : arguments[1],
		'#mdlEditGramPanchayat #selectBox_districtsByZone' : arguments[2],
		'#mdlEditGramPanchayat #selectBox_tehsilsByDistrict' : arguments[3],
		'#mdlEditGramPanchayat #selectBox_blocksByTehsil' : arguments[4],
		'#mdlEditGramPanchayat #selectBox_npByBlock' : arguments[5],
	};

	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
							.prop('selected', true);
				enableMultiSelect(ele_key);
			});

	editPanchayatId = arguments[6];

	$('#mdlEditGramPanchayat #editGramPanchayatName').val(arguments[7]);
	$("#editPanchayatSaveButton").prop("disabled",false);
	$("#editPanchayatSaveButton").removeClass("disabled");
	$("#mdlEditGramPanchayat").modal();
	
	fnCollapseMultiselect();
};

// edit Gp
var editPanchayat = function() {
	$('.outer-loader').show();
	var nyayPanchayatId = $('.editGramPanchayat #selectBox_npByBlock').val();
	var gramPanchayatName = $('#editGramPanchayatName').val().trim().replace(/\u00a0/g," ");
	var gramPanchayatId = editPanchayatId;
	var json = {
			"nyayPanchayatId" : nyayPanchayatId,
			"gramPanchayatName" : gramPanchayatName,
			"gramPanchayatId" : gramPanchayatId
	};


	var gramPanchayat = customAjaxCalling("gramPanchayat", json, "PUT");
	if(gramPanchayat!=null){
	if(gramPanchayat.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("tblPanchayat",gramPanchayat.gramPanchayatId);
		var newRow =getGramPanchayatRow(gramPanchayat);
		appendNewRow("tblPanchayat",newRow);
		applyPermissions();		
		fnUpdateDTColFilter('#tblPanchayat',[2,4,5,6],10,[0,9],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	

		$('#mdlEditGramPanchayat').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ gramPanchayat.responseMessage+'</p>',
			    close :'auto'
			 })
			}, 1000);
		
	}		
	else {
		showDiv($('#mdlEditGramPanchayat #alertdiv'));
		$("#mdlEditGramPanchayat").find('#successMessage').hide();
		$("#mdlEditGramPanchayat").find('#errorMessage').show();
		$("#mdlEditGramPanchayat").find('#exception').show();
		$("#mdlEditGramPanchayat").find('#buttonGroup').show();
		$("#mdlEditGramPanchayat").find('#exception').text(gramPanchayat.responseMessage);
	}
	}
	$(".outer-loader").hide();

	};


// getting data for delete

var deletePanchayatData = function(panchayatid, panchayatname) {
	deleteGramPanchayatId = panchayatid;
	deletePanchayatName = panchayatname;
	$('#deletePanchayatMessage').text(columnMessages.deleteConfirm.replace("@NAME",panchayatname) + " ?");
	$("#mdlDelGramPanchayat").modal().show();
	hideDiv($('#mdlDelGramPanchayat #alertdiv'));

};

// delete Gp

$('#deletePanchayatButton').click(function() {
	$('.outer-loader').show();

	var gramPanchayat = customAjaxCalling("gramPanchayat/"+deleteGramPanchayatId, null, "DELETE");
	if(gramPanchayat!=null){
	if(gramPanchayat.response=="shiksha-200"){
		$(".outer-loader").hide();						

		deleteRow("tblPanchayat",deleteGramPanchayatId);		
		fnUpdateDTColFilter('#tblPanchayat',[2,4,5,6],10,[0,9],thLabels);
		setShowAllTagColor(colorClass);
		$('#mdlDelGramPanchayat').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ gramPanchayat.responseMessage+'</p>',
			    close :'auto'
			 })
			}, 1000);
		
	} else {
		showDiv($('#mdlDelGramPanchayat #alertdiv'));
		$("#mdlDelGramPanchayat").find('#errorMessage').show();
		$("#mdlDelGramPanchayat").find('#exception').text(gramPanchayat.responseMessage);
		$("#mdlDelGramPanchayat").find('#buttonGroup').show();
	}
	}
	$(".outer-loader").hide();
});


// form validation

$(function() {
		
	thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.district,columnMessages.tehsil,columnMessages.block, columnMessages.nyayPanchayat,columnMessages.gramPanchayat,columnMessages.gramPanchayatCode,columnMessages.action];
	fnSetDTColFilterPagination('#tblPanchayat',10,[0,9],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tblPanchayat',[2,4,5,6]);
	});
	$("#tableDiv").show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$( "#addGramPanchayatName" ).keypress(function() {		
		hideDiv($('#mdlAddGramPanchayat #alertdiv'),$('#mdlEditGramPanchayat #alertdiv'));
	})
		$( "#editPanchayatForm" ).keypress(function() {		
		hideDiv($('#mdlEditGramPanchayat #alertdiv'));
	})
	colorClass = $('#t0').prop('class');
	initGloabalVarsGP();
	$("#addPanchayatForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							addGramPanchayatName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.gramPanchayat+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addPanchayat(e);

			});
	$("#editPanchayatForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							editGramPanchayatName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.gramPanchayat+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}

						}

					}).on('success.form.fv', function(e) {
				e.preventDefault();
				editPanchayat();

			});
});
