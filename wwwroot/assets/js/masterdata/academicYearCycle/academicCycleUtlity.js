var pageContextElements = {
	upgrade : '#initUpgrade',
};
var currentACPanelElements={
	heading    : '#currentAC',
	table  	   : '#tbl_currentAY',
	startDate  : '#startDate',
	endDate    : '#endDate',
	editBtn	   : '#btnEditCurrentYear',
	
};
var nextACPanelElements={
		heading    : '#nextAC',
		table      : '#tbl_nextAY',
		startDate  : '#nextStartDate',
		endDate    : '#nextEndDate',
		editBtn	   : '#btnEditNextYear'
};
var upgrade = {
		modal   : '#mdlUpgradeAY',
		table	: '#mdlUpgradeAY #tbl_currentAY',
		currentHeading    : '#currentACInModal',
		currentStartDate  : '#currentACStartDateInModal',
		currentEndDate    : '#currentACEndDateInModal',
		nextHeading : '#nextACInModal',
		nextStartDate  : '#nextACStartDateInModal',
		nextEndDate    : '#nextACEndDateInModal',
		confirm		   : '#upgradeAC',
};
var editYear ={
		modal	   : '#mdlEditAcademicYear',
		modalTitle : '#modal-title',
		startDate  : '#editStartDate',
		endDate	   : '#editEndDate',
		aCName	   : '#editAcademicCycleName'
};
var nextAC,previousAC,currentAC,nextFutureAC;
var academicCycle ={
	getCurrentAC :function(){
		currentAC = shiksha.invokeAjax("academicCycle/?type=current",null, "GET");
		spinner.hideSpinner();
		$(currentACPanelElements.heading).find('b').text(currentAC.name);
		$(currentACPanelElements.startDate).text(currentAC.startDate);
		$(currentACPanelElements.endDate).text(currentAC.endDate);
	},
	getNextAC :function(){
		nextAC = shiksha.invokeAjax("academicCycle/?type=future",null, "GET");
		if(nextAC.response =="shiksha-404"){
			$(pageContextElements.upgrade).prop("disabled",true);
			$(nextACPanelElements.editBtn).prop("disabled",true);
		}
		spinner.hideSpinner();
		$(nextACPanelElements.heading).find('b').text(nextAC.name);
		$(nextACPanelElements.startDate).text(nextAC.startDate);
		$(nextACPanelElements.endDate).text(nextAC.endDate);
	},
	getPreviousAC :function(){
		previousAC = shiksha.invokeAjax("academicCycle/?type=previous",null, "GET");
		spinner.hideSpinner();		
	},
	getnextFutureAC :function(){
		nextFutureAC = shiksha.invokeAjax("academicCycle/?type=nextFuture",null, "GET");
		spinner.hideSpinner();
	},
	init : function(){
		$(pageContextElements.upgrade).on('click',academicCycle.showUpgradeModal);
		$(currentACPanelElements.editBtn).on('click',academicCycle.editCurrentAC);
		$(nextACPanelElements.editBtn).on('click',academicCycle.editNextAC);
		$(upgrade.confirm).on('click',academicCycle.upgradeAC)
	},
	showUpgradeModal : function(){
		$(upgrade.currentHeading).empty();
		$(upgrade.nextHeading).empty();
		$(upgrade.currentHeading).append("<b>"+acText.currentAC+nextAC.name+"</b>");
		$(upgrade.currentStartDate).text(nextAC.startDate);
		$(upgrade.currentEndDate).text(nextAC.endDate);
		var futureAc=nextFutureAC.name!=null?nextFutureAC.name:"";
		$(upgrade.nextHeading).append("<b>"+acText.nextAC+futureAc+"</b>");
		$(upgrade.nextStartDate).text(nextFutureAC.startDate);
		$(upgrade.nextEndDate).text(nextFutureAC.endDate);
		$(upgrade.modal).modal('show');
	},
	editCurrentAC :function(){
		$(editYear.modal).modal('show');
		$(editYear.modalTitle).text(acText.editCurrentAC+currentAC.name);
		$(editYear.startDate).datepicker('setDate', currentAC.startDate);
		$(editYear.endDate).datepicker('setDate', currentAC.endDate);
		$(editYear.aCName).val(currentAC.name);
		$(editYear.aCName).attr('aCId',currentAC.academicCycleId);
	},
	editNextAC : function(){
		$(editYear.modal).modal('show');
		$(editYear.modalTitle).text(acText.editNextAC+nextAC.name);
		$(editYear.startDate).datepicker('setDate', nextAC.startDate);
		$(editYear.endDate).datepicker('setDate', nextAC.endDate);
		$(editYear.aCName).val(nextAC.name);
		$(editYear.aCName).attr('aCId',nextAC.academicCycleId);
		$(editYear.startDate).datepicker('setDate', nextAC.startDate);
	},
	upgradeAC : function(){
		var upgradeAC = shiksha.invokeAjax("academicCycle/upgrade",null, "GET");
		spinner.hideSpinner();
		if(upgradeAC.response == "shiksha-200"){
			$(upgrade.modal).modal('hide');
			AJS.flag({
				type  : "success",
				title : appMessgaes.success,
				body  : upgradeAC.responseMessage,
				close : 'auto'
			});
			setTimeout(function(){
				window.location.reload();
			}, 100);
			
		}else{

			AJS.flag({
				type 	: "error",
				title 	: appMessgaes.error,
				body 	: upgradeAC.responseMessage,
				close 	: 'auto'
			})
		
		}		
	},
	fnUpdate:function(){
		spinner.showSpinner();
		var ajaxData={
				"name":$(editYear.aCName).val(),
				"academicCycleId":$(editYear.aCName).attr('aCId'),
				"startDate":$(editYear.startDate).val(),
				"endDate" :$(editYear.endDate).val(),
		}
		var updateAjaxCall = shiksha.invokeAjax("academicCycle", ajaxData, "PUT");
		spinner.hideSpinner();
		if(updateAjaxCall != null){	
			if(updateAjaxCall.response == "shiksha-200"){
				$(editYear.modal).modal('hide');
				academicCycle.getCurrentAC();
				academicCycle.getNextAC();
				AJS.flag({
					type 	: "success",
					title 	: appMessgaes.success,
					body 	:updateAjaxCall.responseMessage,
					close 	: 'auto'
				})
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.error,
					body 	: updateAjaxCall.responseMessage,
					close 	: 'auto'
				})
			}
		} else {
			AJS.flag({
				type 	: "error",
				title 	: appMessgaes.oops,
				body 	: appMessgaes.serverError,
				close 	: 'auto'
			})
		}
	},
	formValidate: function(validateFormName) {
		$(validateFormName).submit(function(e) {
			e.preventDefault();
		}).validate({
			ignore: '',
			rules: {
				startDate: {
					required: true,
				},
				endDate: {
					required: true,
				}
			},

			messages: {
				startDate: {
					required: academicCycleMessages.startDateRequired
				},
				endDate: {
					required: academicCycleMessages.endDateRequired
				}
			},
			submitHandler : function(){
				academicCycle.fnUpdate();
			}
		});
	},
};
$( document ).ready(function() {
	academicCycle.getCurrentAC();
	academicCycle.getNextAC();
	academicCycle.getPreviousAC();
	academicCycle.getnextFutureAC();
	academicCycle.init();
	academicCycle.formValidate('#frmEditAcademicYear');
	var start = new Date(); 
	var end = new Date();
	$('.datepicker').datepicker({
		format: 'dd-M-yyyy',
		ignoreReadonly: true,
		autoclose: true,
		
	});
	$(editYear.startDate).datepicker({
		
	}).on('changeDate', function(){
		$(editYear.endDate).datepicker('setStartDate', $(this).val());
	}); 
	$(editYear.endDate).datepicker({
		
	}).on('changeDate', function(e){
		$(editYear.startDate).datepicker('setEndDate', $(this).val());
		if($(editYear.startDate).val() <= $(editYear.endDate).val())
			e.preventDefault();
	}); 
});