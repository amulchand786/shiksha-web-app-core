var pageContextElements = {	
		addButton 						: '#addNewAcademicYearCycle',
		table 							: '#academicYearCycleTable',
};
var addModal ={
		form 			: '#addAcademicYearCycleForm',
		modal   		: '#mdlAddAcademicYearCycle',
		saveMoreButton	: 'addAcademicYearCycleSaveMoreButton',
		saveButton		: 'addAcademicYearCycleSaveButton',
		eleName		   	: '#addAcademicYearCycleName',
		eleSequence		: '#addAcademicYearCycleSequence',
		eleStratDate	: '#addStartDate',
		eleEndDate		: '#addEndDate',
		eleFlagCurrentAC: '#addFlagCurretAY',
		message			: "#divSaveAndAddNewMessage",
		afterCycle		: "#addAfterCycle"
};
var editModal ={
			form	 : '#editAcademicYearCycleForm',
			modal	 : '#mdlEditAcademicYearCycle',
			eleName	 : '#editAcademicYearCycleName',
			eleId	 : '#academicYearCycleId',
			eleSequence		: '#editAcademicYearCycleSequence',
			eleStratDate	: '#editStartDate',
			eleEndDate		: '#editEndDate',
			eleFlagCurrentAC: '#editFlagCurretAY',
};
var deleteModal ={
			modal	: '#mdlDeleteAcademicYearCycle',
			confirm : '#deleteAcademicYearCycleButton',
			message	: "#deleteAcademicYearCycleMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var start = new Date(); 
var academicYearCycleFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var academicYearCycle={
		name:'',
		academicYearCycleId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',academicYearCycle.initAddModal);
			$(deleteModal.confirm).on('click',academicYearCycle.deleteFunction);

		},
		deleteFunction : function(){
			academicYearCycle.fnDelete(religiousPlaceId);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			academicYearCycle.resetForm(addModal.form);
			var start = new Date();
			$(addModal.form).trigger("reset");
			$(addModal.modal).modal("show");
			 academicYearCycle.enableDatePicker();
			$(addModal.eleStratDate).datepicker({
				startDate : start,
			});
			academicYearCycle.fnGetAferAcademicCycle();
			
			
		},
		fnGetAferAcademicCycle:function(){
			var afterCycle = shiksha.invokeAjax("academiccycle", null, "GET");
			shk.fnEmptyAllSelectBox(addModal.afterCycle);
			if(afterCycle != null){
				$.each(afterCycle,function(index,value){
					$(addModal.afterCycle).append("<option id="+value.academicCycleId+">"+value.name +" : "+value.sequence+"</option>");
				});
			}	else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : afterCycle.serverError,
					close : 'auto'
				})
			}
			
			destroyAndRefreshMultiselect($(addModal.afterCycle));
			spinner.hideSpinner();

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleName).val(),
					"startDate":$(addModal.eleStratDate).val(),
					"endDate" :$(addModal.eleEndDate).val(),
					"sequence" :$(addModal.eleSequence).val(),
					"flagCurrentAC":$(addModal.eleFlagCurrentAC+":checked").val(),
					"prevAcademicCycleId": $(addModal.afterCycle).find( "option:selected" ).prop("id")
			}
			var addAjaxCall = shiksha.invokeAjax("academicCycle", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : academicCycleMessages.successAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
						academicYearCycle.refreshTable(pageContextElements.table);
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
						academicYearCycle.resetForm(addModal.form);
						academicYearCycle.refreshTable(pageContextElements.table);
						academicYearCycle.enableDatePicker();
						$(addModal.eleStratDate).datepicker({
								startDate : start,
							});
						
						academicYearCycle.fnGetAferAcademicCycle();
						
					}
				
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);

				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(academicCycleId){
			academicYearCycle.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("academicCycle/"+academicCycleId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.name);
					$(editModal.eleId).val(editAjaxCall.academicCycleId);
					$(editModal.eleSequence).val(editAjaxCall.sequence);
					$(editModal.eleStratDate).val(editAjaxCall.startDate);
					$(editModal.eleEndDate).datepicker('setDate', editAjaxCall.endDate);
					$(editModal.eleFlagCurrentAC+"[value='"+editAjaxCall.flagCurrentAC+"']").prop("checked",true);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name":$(editModal.eleName).val(),
					"academicCycleId":$(editModal.eleId).val(),
					"startDate":$(editModal.eleStratDate).val(),
					"endDate" :$(editModal.eleEndDate).val(),
					"sequence" :$(editModal.eleSequence).val(),
					"flagCurrentAC":$(editModal.eleFlagCurrentAC+":checked").val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("academicCycle", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					academicYearCycle.resetForm(editModal.form);
					academicYearCycle.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: academicCycleMessages.sucessAlert,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
					
					if(updateAjaxCall.flagCurrentAC){
						setTimeout(function(){
							location.reload();
						}, 100);
						
					}
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(academicCycleId,name){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			religiousPlaceId=academicCycleId;
		},
		fnDelete :function(academicCycleId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("academicCycle/"+academicCycleId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					academicYearCycle.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : academicCycleMessages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					academicYearCycleName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
						accept:/^[^'?\"/\\]*$/,
					},
					/*sequence: {
						required: true,
						noSpace:true,
					},*/
					startDate: {
						required: true,
					},
					endDate: {
						required: true,
					}
				},

				messages: {
					academicYearCycleName: {
						required: academicCycleMessages.nameRequired,
						noSpace: academicCycleMessages.nameNoSpace,
						minlength:  academicCycleMessages.nameMinLength,
						maxlength: academicCycleMessages.nameMaxLength,
						accept: academicCycleMessages.nameAccept
					},
					/*sequence: {
						required: academicCycleMessages.sequenceRequired,
						noSpace :academicCycleMessages.sequenceNoSpace,
						digits  :academicCycleMessages.digits,
					},*/
					startDate: {
						required: academicCycleMessages.startDateRequired
					},
					endDate: {
						required: academicCycleMessages.endDateRequired
					}
				},
				errorPlacement : function(error, element){
					if ($(element).hasClass("datepicker")) {
					     error.insertAfter($(element).parent());
				    } else {
				    	error.insertAfter(element);
				    }
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						academicYearCycle.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						academicYearCycle.fnUpdate();
					}
				}
			});
		},
		academicYearCycleActionFormater: function(value, row) {
			if(row.isEditable){
				var action = ""; 
				if(permission["edit"] || permission["delete"]){
					action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
					if(permission["edit"])
						action+='<li><a onclick="academicYearCycle.initEdit(\''+ row.academicCycleId+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
					if(permission["delete"])
						action+='<li><a onclick="academicYearCycle.initDelete(\''+ row.academicCycleId+ '\',\''+ row.name+ '\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
					action+='</ul>';
				}
				return action;
			
			} 

		},
		academicYearCycleNumber: function(value, row, index) {
			return index+1;
		},
		enableDatePicker : function(){
			$('.datepicker').datepicker("remove");
			$('.datepicker').datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start
			}).on("hide",function(){
				$(this).trigger("blur");
			});
		}
};

$( document ).ready(function() {

	academicYearCycle.formValidate(addModal.form);
	academicYearCycle.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	academicYearCycle.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: "#toolbarAcademicYearCycle",
		url : "academicCycle",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: false,
		formatShowingRows : academicYearCycleFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");	
	var start = new Date(); 
	var end = new Date();
	$('.datepicker').datepicker({
		format: 'dd-M-yyyy',
		todayHighlight: true,
		autoclose: true,
		startDate:start
	});
	
	$(addModal.eleStratDate).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		$(addModal.eleEndDate).datepicker('setStartDate', $(this).val());
	}); 
	$(addModal.eleEndDate).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(e){
		$(addModal.eleStratDate).datepicker('setEndDate', $(this).val());
		if($(addModal.eleStratDate).val() < $(addModal.eleEndDate).val())
			e.preventDefault();
	});
	$(editModal.eleStratDate).datepicker({
	}).on('changeDate', function(){
		$(editModal.eleEndDate).datepicker('setStartDate', $(this).val());
	}); 
	$(editModal.eleEndDate).datepicker({
	}).on('changeDate', function(e){
		//$(editModal.eleStratDate).datepicker('setEndDate', $(this).val());
		if($(editModal.eleStratDate).val() < $(editModal.eleEndDate).val())
			e.preventDefault();
	});
});