//global variables
var editCenterForm="editCenterForm";

var editSelectedLocType;
var editSelectedLocations;


var allInputEditEleNamesOfCityFilter =[];
var allInputEditEleNamesOfVillageFilter =[];
var allInputEditEleNamesOfCityFilterCenter=[];

//to addcampaign data
var selectedEditCenters;
var isAssessmentShown;
var lObjEdit;
var selectedIds;
var selectedEditIds;
var elementIdMap;
var resetButtonObj;
var selectedChkBox;
var noOfCenters;
var cityName;
var villageName;
var centerIdList;
var usercenters;
var selectedCentersList;
var removeCenterRowId;
var isSchoolDeleted;
var $container;
var userCenterMdlElement={
		eleLocType						 : '#mdlEditCenterForUser input:radio[name=locType]',
		eleAddLocColspFilter	 		 : '#mdlEditCenterForUser #addLocColspFilter',
		eleLocationPnlLink		 		 : '#mdlEditCenterForUser #locationPnlLink',
		eleToggleCollapse		 		 : '[data-toggle="collapse"]',
		eleCollapseFilter		  		 : '#mdlEditCenterForUser #collapseFilter',
		eleLocTypeChecked		 		 : '#mdlEditCenterForUser input:radio[name=locType]:checked',
		eleTabPane               		 : '#mdlEditCenterForUser #tabPane',
		centerWizard     				 : '#editUserCenterWizard',
		centerwizardBtnNext				 : '#editUserCenterWizard #btnNext',
		centerwizardDivFilter			 : '#editUserCenterWizard #divFilter',
		centerWizardTable          	     : 'editCenterWizardTable',
		locationStepWiz      		     : '#editUserCenterWizard #locationStepWiz',
		chkboxCenterSelectAll			 : '#chkboxEditCenterSelectAll',
		editUserCenterTable              : 'editUserCenterTable',
};




var editUserCenterMdlDivs = {	
		city 					: "#mdlEditCenterForUser #divSelectCity",
		block 					: "#mdlEditCenterForUser #divSelectBlock",
		NP 						: "#mdlEditCenterForUser #divSelectNP",
		GP 						: "#mdlEditCenterForUser #divSelectGP",
		RV 						: "#mdlEditCenterForUser #divSelectRV",
		village				    : "#mdlEditCenterForUser #divSelectVillage",
		schoolDiv 				: "#mdlEditCenterForUser #divSchool",
		schoolElement 			: "#mdlEditCenterForUser #selectBox_schoolByLocAndType",
		centerLocationType 		: "#mdlEditCenterForUser #selectBox_centerLocationType",
		schoolType 				: "#mdlEditCenterForUser #selectBox_schoolType",
		resetLocDivCity 		: "#mdlEditCenterForUser #divResetLocCity",
		resetLocDivVillage 		: "#mdlEditCenterForUser #divResetLocVillage",
		stepContentsDiv         : "#editCenterForm #stepContentsDiv",
		
		
};
//initialize global vars	
var initEditGlobalValues =function(){
	editSelectedLocType ='';
	editSelectedLocations =[];
	
	isEditCenterShown=false;
	selectedEditCenters =[];
	allInputEditEleNamesOfCityFilter =['stateId','zoneId','districtId','tehsilId','cityId'];
	allInputEditEleNamesOfVillageFilter =['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
	allInputEditEleNamesOfCityFilterCenter =['blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
}	
	

//remove and reinitialize smart filter
var reinitializeSmartEditFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
};

//displaying smart-filters
var displaySmartEditFilters =function(type,lCode,lvlName){
	$(".outer-loader").show();
	if(type=="add"){
		elementIdMap = {
				"State"				 : '#mdlEditCenterForUser #statesForZone',
				"Zone"				 : '#mdlEditCenterForUser #selectBox_zonesByState',
				"District" 			 : '#mdlEditCenterForUser #selectBox_districtsByZone',
				"Tehsil" 			 : '#mdlEditCenterForUser #selectBox_tehsilsByDistrict',
				"Block" 			 : '#mdlEditCenterForUser #selectBox_blocksByTehsil',
				"City"				 : '#mdlEditCenterForUser #selectBox_cityByBlock',
				"Nyaya Panchayat" 	 : '#mdlEditCenterForUser #selectBox_npByBlock',
				"Gram Panchayat"	 : '#mdlEditCenterForUser #selectBox_gpByNp',
				"Revenue Village"	 : '#mdlEditCenterForUser #selectBox_revenueVillagebyGp',
				"Village"			 : '#mdlEditCenterForUser #selectBox_villagebyRv'				
		};
		displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));
	}
		
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
		}, 100);
};

//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionEditFilterDiv =function(type){
	if(type=="add"){		
		$(editUserCenterMdlDivs.NP).insertAfter( $(editUserCenterMdlDivs.block));
		$(editUserCenterMdlDivs.RV).insertAfter( $(editUserCenterMdlDivs.GP));
	}
};
/////////////////////////
//smart filter starts here//

/////////////////////////
$(userCenterMdlElement.eleLocType).change(function() {
	$(userCenterMdlElement.eleAddLocColspFilter).removeClass('in');
	$(userCenterMdlElement.eleLocationPnlLink).text(columnMessages.selectLocation);
	$(userCenterMdlElement.eleTabPane).css('display','none');
	$(userCenterMdlElement.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$(userCenterMdlElement.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	
	lObjEdit =this;
	
	selectedIds=[];
	selectedEditIds=[];
	var typeCode =$(this).data('code');		
	$('.outer-loader').show();	
	
	editSelectedLocType =typeCode;
	reinitializeSmartEditFilter(elementIdMap);	
	resetFormValidatonForLocFilters(editCenterForm,allInputEditEleNamesOfCityFilter);
	resetFormValidatonForLocFilters(editCenterForm,allInputEditEleNamesOfVillageFilter);
	if(typeCode.toLowerCase() =='U'.toLowerCase()){
		fnUpdateFVElementStatus(editCenterForm,allInputEditEleNamesOfCityFilterCenter,'VALID');
		showDiv($(editUserCenterMdlDivs.city),$(editUserCenterMdlDivs.resetLocDivCity));
		hideDiv($(editUserCenterMdlDivs.block),$(editUserCenterMdlDivs.NP),$(editUserCenterMdlDivs.GP),$(editUserCenterMdlDivs.RV),$(editUserCenterMdlDivs.village),$(editUserCenterMdlDivs.resetLocDivVillage));
		displaySmartEditFilters("add",$(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
	}else{
	$("#"+editCenterForm).data('formValidation').updateStatus('cityId', 'VALID');
		hideDiv($(editUserCenterMdlDivs.city),$(editUserCenterMdlDivs.resetLocDivCity));
		showDiv($(editUserCenterMdlDivs.block),$(editUserCenterMdlDivs.NP),$(editUserCenterMdlDivs.GP),$(editUserCenterMdlDivs.RV),$(editUserCenterMdlDivs.village),$(editUserCenterMdlDivs.resetLocDivVillage));
		displaySmartEditFilters("add",$(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
		positionEditFilterDiv("add");
	}
});





//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
}

//invoking on click of resetEditLocation confirmation dialog box
var doResetLocation =function(){
	selectedEditIds=[];
	selectedIds=[];

	$("#mdlResetLoc").modal("hide");
	$('.outer-loader').show();
	if($(resetButtonObj).parents("form").attr("id")==editCenterForm){
		resetFormValidatonForLocFilters(editCenterForm,allInputEditEleNamesOfCityFilter);
		resetFormValidatonForLocFilters(editCenterForm,allInputEditEleNamesOfVillageFilter);
		reinitializeSmartEditFilter(elementIdMap);
		var aLocTypeCode =$(userCenterMdlElement.eleLocTypeChecked).data('code');
		var aLLevelName =$(userCenterMdlElement.eleLocTypeChecked).data('levelname');
		displaySmartEditFilters("add",aLocTypeCode,aLLevelName);
		if(aLocTypeCode!="" && aLocTypeCode!=null){
			if(aLocTypeCode.toLowerCase() =='U'.toLowerCase()){	
				fnUpdateFVElementStatus(editCenterForm,allInputEditEleNamesOfCityFilterCenter,'VALID');
			}else{
				 $("#"+editCenterForm).data('formValidation').updateStatus('cityId', 'VALID');
			}
		}
	}
	fnCollapseMultiselect();
	
};

//get all centertype and location and bind to respective list box at add center
var getEditCenterTypeAndLoc =function(){
	$(userCenterMdlElement.centerWizard).removeClass('wizardMaxHeight');
	$(editUserCenterMdlDivs.stepContentsDiv).removeClass("wizardContentMaxHeight");
	reinitializeSmartEditFilter(elementIdMap);
	
	
	$(userCenterMdlElement.centerwizardDivFilter).hide();
	deleteAllRow(userCenterMdlElement.centerWizardTable	);
	$(userCenterMdlElement.locationStepWiz).trigger('click');
	$(userCenterMdlElement.chkboxCenterSelectAll	).prop('checked',false);
	$(userCenterMdlElement.eleLocType).each(function(i,ele){$(ele).prop('checked',false)});
	resetFormById(editCenterForm);
	 
	setOptionsForAllMultipleSelect($(editUserCenterMdlDivs.centerLocationType));
	
	 
	$(editUserCenterMdlDivs.centerLocationType).multiselect('refresh');
	selectedChkBox =[];
	$(userCenterMdlElement.centerwizardBtnNext).prop('disabled',false);
	
}
//reset form
var resetEditLocatonAndForm =function(type){
	$('#divFilter').hide();
	if(type=="add"){
		resetFormById(editCenterForm);	
	}
	reinitializeSmartEditFilter(elementIdMap);
	getEditCenterTypeAndLoc();
};



/////////////////////////
//smart filter ends here//
/////////////////////////

//after add center modal changed to wizard based
//get selected locations for center
var fnGetSelectedEditLocations =function(){
	if (editSelectedLocType.toLowerCase() == 'U'.toLowerCase()) {
		if ($(elementIdMap.City).find('option:selected').length > 0) {
			
			editSelectedLocations = [];
			$(elementIdMap.City).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						editSelectedLocations.push(parseInt($(option).val()));
					}
				});
			});
		}
	} else {
		if ($(elementIdMap.Village).find('option:selected').length > 0) {
			
			editSelectedLocations = [];
			$(elementIdMap.Village).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						editSelectedLocations.push(parseInt($(option).val()));
					}
				});
			});
		}
	}// else
};


//get addedd center from center table
var fnGetSelectedEditCenters =function(){	
	selectedEditCenters = [];
	var abDtRows = $("#"+userCenterMdlElement.editUserCenterTable).dataTable().fnGetNodes();
	$.each(abDtRows,function(i,row){		
		selectedEditCenters.push(parseInt($(row).prop('id')));	
	});
};

//bind row to the add center table in wizard
var fnBindEditCenterToTable =function(dCenter){
	deleteAllRow(userCenterMdlElement.centerWizardTable);
	selectedChkBox =[];
	$.each(dCenter,	function(index, obj) {
		var row; var cityName; var villageName ; var revenueVillageName;
		var gramPanchayatName; var nyayPanchayatName; 	var blockName;
		var tehsilName; var districtName; 	var zoneName; var stateName;
		if(obj.city==null){
			 cityName ="N/A";
			 villageName =obj.village.villageName;
			 revenueVillageName=obj.village.revenueVillage.revenueVillageName;
			 gramPanchayatName=obj.village.revenueVillage.gramPanchayat.gramPanchayatName;
			 nyayPanchayatName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.nyayPanchayatName;
			 blockName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.block.blockName;
			 tehsilName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.tehsilName;
			 districtName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.districtName;
			 zoneName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.zone.zoneName;
			 stateName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.zone.state.stateName;
			 row = "<tr id='"+ obj.id+ "' data-id='"	+obj.id+ "' data-center='"	+obj.name+ "' data-statename='"+stateName+"'" +
			"data-zonename='" + zoneName+"' data-distname='"+ districtName+"'" +
			"data-tehsilname='"+ tehsilName+"' data-blockname='"+ blockName+"'" +
			"data-cityname='"+ cityName+"' data-nyaypanchayatname='"+ nyayPanchayatName+"'" +
			"data-grampanchayatname='"+gramPanchayatName+"'" +
			"data-revenuevillagename='" + revenueVillageName+"'" +
			"data-villagename='"+ villageName+"'>"+											
			"<td data-id='"+ obj.id+"'><center><input class='chkbox' type='checkbox' onchange='fnChkUnchkEditUser("+obj.id+")'  id='chkboxEditCenterWizard"+obj.id+"'></input></center></td>"+
			"<td title='"+ obj.name+ "'>"+ obj.name+ "</td>"+
			"</tr>";
			fnAppendNewRow(userCenterMdlElement.centerWizardTable, row);
	}else{
		 cityName =obj.city.cityName;
		 villageName ="N/A";
		 tehsilName=obj.city.tehsil.tehsilName;
		 districtName=obj.city.tehsil.district.districtName;
		 zoneName=obj.city.tehsil.district.zone.zoneName;
		 stateName=obj.city.tehsil.district.zone.state.stateName;
		 row = "<tr id='"+ obj.id+ "' data-id='"	+obj.id+ "' data-center='"	+obj.name+ "' data-statename='"+stateName+"'" +
			"data-zonename='" + zoneName+"' data-distname='"+ districtName+"'" +
			"data-tehsilname='"+ tehsilName+"' data-blockname='"+ blockName+"'" +
			"data-cityname='"+ cityName+"' data-nyaypanchayatname='"+ nyayPanchayatName+"'" +
			"data-grampanchayatname='"+gramPanchayatName+"'" +
			"data-revenuevillagename='" + revenueVillageName+"'" +
			"data-villagename='"+ villageName+"'>"+											
			"<td data-id='"+ obj.id+"'><center><input class='chkbox' type='checkbox' onchange='fnChkUnchkEditUser("+obj.id+")'  id='chkboxEditCenterWizard"+obj.id+"'></input></center></td>"+
			"<td title='"+ obj.name+ "'>"+ obj.name+ "</td>"+
			"</tr>";
			fnAppendNewRow(userCenterMdlElement.centerWizardTable, row);
	}
			
		
	});
	
	fnDTColFilterForEditCenterWizard("#"+userCenterMdlElement.centerWizardTable	,2,[0],false);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns(userCenterMdlElement.centerWizardTable	,[]);
	});
}
//get school for campaign and bind to the table for adding to campaign
var fnBindCenterForEditUser =function(e){
	fnGetSelectedEditLocations();
	fnGetSelectedEditCenters();
		var rData ={
			"locations":editSelectedLocations,
			"centerIds":selectedEditCenters,
	};
	var centerVms = customAjaxCalling("center/user/"+editSelectedLocType,rData,"POST");
	if (centerVms.length != 0) {
		noOfCenters=0;
		noOfCenters =centerVms.length;
		fnBindEditCenterToTable(centerVms);		
	}else{
		e.preventDefault();
		$('#mdlErrorForSchool #msgTextForSchool').text('');
		$('#mdlErrorForSchool #msgTextForSchool').text(validationMsg.noMoreCenter);
		$('#mdlErrorForSchool').modal().show();		
	}
};





//disable the centers if it has been added to table
var disableEditCenterFromListBox =function(){

	selectedEditCenters =[];
	centerIdList=[];
	var i=0;
	$('#'+userCenterMdlElement.editUserCenterTable+' >tbody>tr').each(function(i,row){		
		selectedEditCenters.push( parseInt(row.getAttribute('data-id')));
	});
	$(editUserCenterMdlDivs.schoolElement).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				centerIdList.push( parseInt($(option).data('id')));
			}
		});
	});	
	$.grep(centerIdList, function(el) {
		if ($.inArray(el, selectedEditCenters) != -1){
			var option = $(editUserCenterMdlDivs.schoolElement).find('option[value="' +el+ '"]');
			option.prop('disabled', true);
			option.parent('label').parent('a').parent('li').remove();
		}
		i++;
	});
	$(editUserCenterMdlDivs.schoolElement).multiselect('refresh');	
}

var createNewEditCenterRow =function(selectedCenters){
	if($('#'+userCenterMdlElement.editUserCenterTable+' >tbody>tr').length==1){
		$('#'+userCenterMdlElement.editUserCenterTable+' tbody').find(' tr').remove();
	}	
	selectedCentersList =[];
	$('#'+userCenterMdlElement.editUserCenterTable+' >tbody>tr').each(function(i,row){		
		selectedCentersList.push( parseInt(row.getAttribute('data-id')));
	});
	$.each(selectedCenters,function(index,obj){
	    if ($.inArray(obj.id, selectedCentersList) > -1){
			
		}else{
			var row ="<tr id='"+obj.id+"'data-oldSchool='false' data-id='"+obj.id+"'>"+
			"<td></td>"+
			"<td title='"+obj.state+"'>"+obj.state+"</td>"+
			//"<td title='"+obj.zone+"'>"+obj.zone+"</td>"+
			"<td title='"+obj.dist+"'>"+obj.dist+"</td>"+
			"<td title='"+obj.city+"'>"+obj.city+"</td>"+
			"<td title='"+obj.village+"'>"+obj.village+"</td>"+
			"<td title='"+obj.name+"'>"+obj.name+"</td>"+
			"<td><div class='div-tooltip'><a class='tooltip tooltip-top' id='btnRemoveSchool'" +
			"onclick=removeEditUserCenter(&quot;"+obj.id+"&quot;,&quot;"+escapeHtmlCharacters(obj.name)+"&quot;,&quot;"+false+"&quot;);><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"+
			"</tr>";
		appendNewRow(userCenterMdlElement.editUserCenterTable,row);
		 
		}
	});		
};
//add selected center to the table
var editUserCenter =function(){
	
	usercenters =[];
	$(editUserCenterMdlDivs.schoolElement).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				selectedEditCenters = {};
				selectedEditCenters.id =$(this).data('id');
				selectedEditCenters.name =$(this).text();
				selectedEditCenters.state =$(this).data('statename');
				selectedEditCenters.zone =$(this).data('zonename');
				selectedEditCenters.dist =$(this).data('distname');
				selectedEditCenters.city =$(this).data('cityname');
				selectedEditCenters.village =$(this).data('villagename');
				usercenters.push(selectedEditCenters);
			}			
		});
	});
	createNewEditCenterRow(usercenters);
	$('#mdlEditCenterForUser').modal('hide');
};


var oldEditCenterData;
//remove center from table

var removeEditUserCenter =function(rowId,centerName,isOldCenter){
	removeCenterRowId=rowId;
	oldEditCenterData=isOldCenter;
	$('#mdlRemoveCenter #msgText').text('');	
	$('#mdlRemoveCenter #msgText').text((validationMsg.center_deleteConfirm).replace("@NAME",centerName)+ "?");
	$('#mdlRemoveCenter').modal().show();
	}

$('#mdlRemoveCenter .btnRemoveCenter').on('click',function(){
	var userId=$(editUserMdlElements.userId).val();
	isSchoolDeleted=true;
	if(oldEditCenterData=='true'){
	var userSchoolMappingData=customAjaxCalling("user/"+userId+"/center/"+removeCenterRowId, null, "DELETE");
	if(userSchoolMappingData.response=="shiksha-200"){
		isSchoolDeleted=true;
	deleteRow(userCenterMdlElement.editUserCenterTable,removeCenterRowId);
	$('#mdlRemoveCenter').modal('hide');
	setTimeout(function() {   //calls click event after a one sec
		 AJS.flag({
		    type: 'success',
		    title: appMessgaes.success,
		    body: '<p>'+userSchoolMappingData.responseMessage+'</p>',
		    close :'auto'
		})
		}, 1000);
	
	}else{
		$('#mdlRemoveCenter').modal('hide');
        setTimeout(function() {   //calls click event after a one sec
		 AJS.flag({
		    type: 'error',
		    title:appMessgaes.failure,
		    body: '<p>'+userSchoolMappingData.responseMessage+'</p>',
		    close :'auto'
		})
		}, 1000);
	
	
	}
	}else{
		deleteRow(userCenterMdlElement.editUserCenterTable,removeCenterRowId);
		$('#mdlRemoveCenter').modal('hide');
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+validationMsg.centerDeleted+'</p>',
			    close :'auto'
			})
			}, 1000);
		
	}
});

//get the selected values and push to respective array for assessment
var getEditUserSelectedData =function(elementObj,collector){
	$(elementObj).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				collector.push( parseInt($(option).val()));
			}
		});
	});
};

var fnChkUnchkEditUser =function(id){	 	 
	 var idx = $.inArray(id, selectedChkBox);	
	 if (idx == -1) {
		 selectedChkBox.push(id);
		 selectedChkBox.length==noOfCenters? $(userCenterMdlElement.chkboxCenterSelectAll).prop('checked',true): $(userCenterMdlElement.chkboxCenterSelectAll	).prop('checked',false);
	 } else {
		 selectedChkBox.splice(idx, 1);
		 $(userCenterMdlElement.chkboxCenterSelectAll	).prop('checked',false)
	 }
};

//on click of select all in table header...
var fnInvokeEditUserSelectAll =function(){
	var isChecked =$(userCenterMdlElement.chkboxCenterSelectAll	).is(':checked');
	var rows = $("#"+userCenterMdlElement.centerWizardTable).dataTable().fnGetNodes();
	if(isChecked){
	
		fnDoSelectAllEditCenter(isChecked);
	}else{
		selectedChkBox =[];
		$.each(rows,function(i,row){
			
			var id="#chkboxEditCenterWizard"+($(row).prop('id').toString());
			$(id).prop('checked',false);
		});	
	}
}

//add selected schools to the table
var fnAddCenterEditUser = function() {
	usercenters = [];

	
	var rows = $("#"+userCenterMdlElement.centerWizardTable).dataTable().fnGetNodes();
	$.each(rows,function(i,row){		
		var id =$(row).data('id');
		 var idxp = $.inArray(id, selectedChkBox);	
		 if (idxp!= -1) {
			selectedEditCenters = {};
			selectedEditCenters.id = $(row).data('id');
			selectedEditCenters.name = $(row).data('center');
			selectedEditCenters.state = $(row)	.data('statename');
			selectedEditCenters.zone = $(row).data('zonename');
			selectedEditCenters.dist = $(row).data('distname');
			selectedEditCenters.tehsil = $(row).data('tehsilname');
			selectedEditCenters.block = $(row).data('blockname');
			selectedEditCenters.city = $(row).data('cityname');
			selectedEditCenters.nyaypanchayat = $(row).data('nyaypanchayatname');
			selectedEditCenters.grampanchayat = $(row).data('grampanchayatname');
			selectedEditCenters.revenuevillage = $(row).data('revenuevillagename');
			selectedEditCenters.village = $(row).data(	'villagename');
			usercenters.push(selectedEditCenters);
		}
	});
	createNewEditCenterRow(usercenters);
	$('#editUserSaveBtn').prop('disabled',false);
	$('#mdlEditCenterForUser').modal('hide');
}

$(function() {
	setDataTablePagination("#"+userCenterMdlElement.editUserCenterTable);
   initEditGlobalValues();
   fnColSpanIfDTEmpty(userCenterMdlElement.editUserCenterTable,8);
	
    
	$(userCenterMdlElement.eleAddLocColspFilter).on('show.bs.collapse', function(){
		$(userCenterMdlElement.eleLocationPnlLink).text(columnMessages.hideLocation);
	});  
	$(userCenterMdlElement.eleAddLocColspFilter).on('hide.bs.collapse', function(){
		$(userCenterMdlElement.eleLocationPnlLink).text(columnMessages.selectLocation);
	});
	//for add center wizard of user
	$("#" + editCenterForm).formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			schoolType: {
                validators: {
                    choice: {
                        min: 1,
                        max: 2,
                        message: ' '
                    }
                }
            },
            locType: {
                validators: {
                    notEmpty: {
                        message: ' '
                    }
                }
            }
		}
	});
	$(userCenterMdlElement.centerWizard).wizard().on('actionclicked.fu.wizard',function(e, data) {
		var fv = $('#editCenterForm').data('formValidation'), // FormValidation
		// instance
		step = data.step; // Current step
		// The current step container
		$container = $('#editCenterForm').find('.step-pane[data-step="' + step + '"]');

		// Validate the container
		fv.validateContainer($container);

		var isValidStep = fv.isValidContainer($container);		
		if (isValidStep === false || isValidStep === null) {
			// Do not jump to the target panel
			e.preventDefault();
		}
		if (step == 1 && isValidStep) {
			fnBindCenterForEditUser(e);
			// for handling dynamic height #673
			$(userCenterMdlElement.centerWizard).addClass('wizardMaxHeight');
			
			
		}if (step == 2 && isValidStep && data.direction!='previous') {
			var isOneCenterSelected =false;
			
			var aaDtRows = $("#"+userCenterMdlElement.centerWizardTable	).dataTable().fnGetNodes();
			$.each(aaDtRows,function(i,row){			
				if($(row).find('.chkbox').is(':checked') === true){
					isOneCenterSelected =true;
				}
			});
			if(!isOneCenterSelected){
				e.preventDefault();
				$('#mdlErrorForSchool #msgTextForSchool').text('');
				$('#mdlErrorForSchool #msgTextForSchool').text(validationMsg.centerRequired);
				$('#mdlErrorForSchool').modal().show();
			}
		}else if (step == 2 && isValidStep && data.direction=='previous') {
			$(userCenterMdlElement.centerWizard).removeClass('wizardMaxHeight');
		}
	})
	// Triggered when clicking the Complete button
	.on('finished.fu.wizard',function() {
		$(userCenterMdlElement.centerwizardBtnNext).prop('disabled',true);
		
		var fv = $('#editCenterForm').data('formValidation'),
		step = $('#editCenterForm').wizard('selectedItem').step, 
		$container = $('#editCenterForm').find('.step-pane[data-step="' + step + '"]');

		// Validate the last step container
		fv.validateContainer($container);

		var isValidStep = fv.isValidContainer($container);
		if (isValidStep === true) {			
			fnAddCenterEditUser();
			$(userCenterMdlElement.centerWizard).removeClass('wizardMaxHeight');
			 fnEnableEditSaveBtn();
		}
	});

});
	