//global variables
var addSchoolForm="addSchoolForm";
var selectedLocType;
var selectedLocations;
var selectedSchooltypes;
var isSchoolShown;
var allInputEleNamesOfCityFilter =[];
var allInputEleNamesOfVillageFilter =[];
var allInputEleNamesOfCityFilterSchool=[];
var lObj;
var selectedIds;
var elementIdMap;
var selectedChkBox;
var selectedSchoolsInTable;
//to addcampaign data
var selectedSchools;
var resetButtonObj;
//global selectDivIds
var addSchoolMdlSelectDivs = {};	
addSchoolMdlSelectDivs.city 	="#mdlAddSchoolForUser #divSelectCity";
addSchoolMdlSelectDivs.block 	="#mdlAddSchoolForUser #divSelectBlock";
addSchoolMdlSelectDivs.NP 	="#mdlAddSchoolForUser #divSelectNP";
addSchoolMdlSelectDivs.GP 	="#mdlAddSchoolForUser #divSelectGP";
addSchoolMdlSelectDivs.RV 	="#mdlAddSchoolForUser #divSelectRV";
addSchoolMdlSelectDivs.village ="#mdlAddSchoolForUser #divSelectVillage";
addSchoolMdlSelectDivs.schoolTypeDiv ="#mdlAddSchoolForUser #divSchoolType";
addSchoolMdlSelectDivs.schoolDiv ="#mdlAddSchoolForUser #divSchool";
addSchoolMdlSelectDivs.schoolElement ="#mdlAddSchoolForUser #selectBox_schoolByLocAndType";
addSchoolMdlSelectDivs.schoolLocationType ="#mdlAddSchoolForUser #selectBox_schoolLocationType";
addSchoolMdlSelectDivs.schoolType ="#mdlAddSchoolForUser #selectBox_schoolType";
addSchoolMdlSelectDivs.resetLocDivCity ="#mdlAddSchoolForUser #divResetLocCity";
addSchoolMdlSelectDivs.resetLocDivVillage ="#mdlAddSchoolForUser #divResetLocVillage";

//initialize global vars	
var initGlobalValues =function(){
	selectedLocType ='';
	selectedLocations =[];
	selectedSchooltypes =[];
	isSchoolShown=false;
	selectedSchools =[];
	allInputEleNamesOfCityFilter =['stateId','zoneId','districtId','tehsilId','cityId'];
	allInputEleNamesOfVillageFilter =['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
	allInputEleNamesOfCityFilterSchool =['blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
};	

/////////////////////////
//smart filter starts here//
/////////////////////////

//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
}

//displaying smart-filters
var displaySmartFilters =function(type,lCode,lvlName){
	$(".outer-loader").show();
	if(type=="add"){
		elementIdMap = {
				"State" : '#mdlAddSchoolForUser #statesForZone',
				"Zone" : '#mdlAddSchoolForUser #selectBox_zonesByState',
				"District" : '#mdlAddSchoolForUser #selectBox_districtsByZone',
				"Tehsil" : '#mdlAddSchoolForUser #selectBox_tehsilsByDistrict',
				"Block" : '#mdlAddSchoolForUser #selectBox_blocksByTehsil',
				"City" : '#mdlAddSchoolForUser #selectBox_cityByBlock',
				"Nyaya Panchayat" : '#mdlAddSchoolForUser #selectBox_npByBlock',
				"Gram Panchayat" : '#mdlAddSchoolForUser #selectBox_gpByNp',
				"Revenue Village" : '#mdlAddSchoolForUser #selectBox_revenueVillagebyGp',
				"Village" : '#mdlAddSchoolForUser #selectBox_villagebyRv'				
		};
		displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));
	}

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
		}, 100);
};

//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionFilterDiv =function(type){
	if(type=="add"){		
		$(addSchoolMdlSelectDivs.NP).insertAfter( $(addSchoolMdlSelectDivs.block));
		$(addSchoolMdlSelectDivs.RV).insertAfter( $(addSchoolMdlSelectDivs.GP));
	}
};
//show or hide div as per school location type
$('#mdlAddSchoolForUser input:radio[name=locType]').change(function() {
	$('#mdlAddSchoolForUser #addLocColspFilter').removeClass('in');
	$('#mdlAddSchoolForUser #locationPnlLink').text('Select Location');
	$('#mdlAddSchoolForUser #tabPane').css('display','none');
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	$('#schoolType').find('input[type=checkbox]:checked').removeAttr('checked');
	$("#"+addSchoolForm).data('formValidation').updateStatus('schoolType', 'VALID');
	lObj =this;
	selectedIds=[];//shk-369
	var typeCode =$(this).data('code');		
	$('.outer-loader').show();	
	//reset multiselct
	$(addSchoolMdlSelectDivs.schoolElement).multiselect('destroy');
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	
	selectedLocType =typeCode;
	reinitializeSmartFilter(elementIdMap);	
	resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfCityFilter);
	resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfVillageFilter);
	if(typeCode.toLowerCase() =='U'.toLowerCase()){
		fnUpdateFVElementStatus(addSchoolForm,allInputEleNamesOfCityFilterSchool,'VALID');
		showDiv($(addSchoolMdlSelectDivs.city),$(addSchoolMdlSelectDivs.resetLocDivCity));
		hideDiv($(addSchoolMdlSelectDivs.block),$(addSchoolMdlSelectDivs.NP),$(addSchoolMdlSelectDivs.GP),$(addSchoolMdlSelectDivs.RV),$(addSchoolMdlSelectDivs.village),$(addSchoolMdlSelectDivs.resetLocDivVillage));
		displaySmartFilters("add",$(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
	}else{
		$("#"+addSchoolForm).data('formValidation').updateStatus('cityId', 'VALID');
		hideDiv($(addSchoolMdlSelectDivs.city),$(addSchoolMdlSelectDivs.resetLocDivCity));
		showDiv($(addSchoolMdlSelectDivs.block),$(addSchoolMdlSelectDivs.NP),$(addSchoolMdlSelectDivs.GP),$(addSchoolMdlSelectDivs.RV),$(addSchoolMdlSelectDivs.village),$(addSchoolMdlSelectDivs.resetLocDivVillage));
		displaySmartFilters("add",$(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
		positionFilterDiv("add");
	}
});





//reset only location when click on reset button icon
var resetAddLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
};

//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];
	$("#mdlResetLoc").modal("hide");
	$('.outer-loader').show();
	if($(resetButtonObj).parents("form").attr("id")==addSchoolForm){
		resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfCityFilter);
		resetFormValidatonForLocFilters(addSchoolForm,allInputEleNamesOfVillageFilter);
		reinitializeSmartFilter(elementIdMap);
		var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
		var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
		displaySmartFilters("add",aLocTypeCode,aLLevelName);
		if(aLocTypeCode!="" && aLocTypeCode!=null){
			if(aLocTypeCode.toLowerCase() =='U'.toLowerCase()){
				fnUpdateFVElementStatus(addSchoolForm,allInputEleNamesOfCityFilterSchool,'VALID');
			}else{
				$("#"+addSchoolForm).data('formValidation').updateStatus('cityId', 'VALID');
			}
		}
	}
	fnCollapseMultiselect();
};

//IsInternal field Hid & show
var getSelected =function()
{
	var selected = $('#addUserType').val();
	if(selected || selected== "true")
	{	$('#addIuserSAPCode').val('');
	$('#internalSapCodeDiv').show();
	}
	else{$('#internalSapCodeDiv').hide();}
}

var fnInitSchoolLocationTypeMulSelect = function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 100,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onDropdownHide: function() {
				fnGetSmartFilter();
			}
		});
	});
}
//get all schooltype and location and bind to respective list box at add school
var getSchoolTypeAndLoc = function() {
	$(addSchoolMdlSelectDivs.schoolType).multiselect('destroy');
	$('#divFilter').hide();
	$('#locationStepWiz').trigger('click'); //reset wizard
	$('#chkboxSelectAll').prop('checked',false);
	$('input:radio[name=locType]').each(function(i,ele){$(ele).prop('checked',false)});
	resetFormById(addSchoolForm);
	bindSchoolTypeForCampaignToListBox($(addSchoolMdlSelectDivs.schoolType), 0);
	getAllSchoolTypeLocation();
	fnInitSchoolLocationTypeMulSelect($(addSchoolMdlSelectDivs.schoolLocationType));
	$(addSchoolMdlSelectDivs.schoolType).multiselect('refresh');
	$(addSchoolMdlSelectDivs.schoolLocationType).multiselect('refresh');
	selectedChkBox =[];
	$('#addUserSchoolWizard #btnNext').prop('disabled',false);
	
}
//reset form
var resetLocatonAndForm =function(type){
	$('#divFilter').hide();
	if(type=="add"){
		resetFormById(addSchoolForm);	
	}
	reinitializeSmartFilter(elementIdMap);
	getSchoolTypeAndLoc();
}


/////////////////////////
//smart filter ends here//
/////////////////////////


//get addedd schools from school table
var fnGetSelectedSchools =function(){	
	selectedSchoolsInTable = [];
	var abDtRows = $("#addUserModal #userSchoolTable").dataTable().fnGetNodes();
	$.each(abDtRows,function(i,row){		
		selectedSchoolsInTable.push(parseInt($(row).prop('id')));	
	});
}





//get selected locations for schools
var fnGetSelectedLocations =function(){
	if (selectedLocType.toLowerCase() == 'U'.toLowerCase()) {
		if ($(elementIdMap.City).find('option:selected').length > 0) {
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations = [];
			$(elementIdMap.City).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all")
						selectedLocations.push(parseInt($(option).val()));
				});
			});
		}
	} else {
		if ($(elementIdMap.Village).find('option:selected').length > 0) {
			showDiv($(addSchoolMdlSelectDivs.schoolTypeDiv));
			selectedLocations = [];
			$(elementIdMap.Village).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all")
						selectedLocations.push(parseInt($(option).val()));
				});
			});
		}
	}// else
}

//get selected schoolType
var fnGetSelectedSchoolType =function(){
	selectedSchooltypes = [];
	$('#schoolType input:checked').each(function() {
		selectedSchooltypes.push($(this).data('id'));
	});
}
//get addedd schools from school table
var fnGetSelectedSchools =function(){	
	selectedSchools = [];
	if ($('#addUserModal #userSchoolTable >tbody>tr').length == 1) {
		$("#addUserModal #userSchoolTable tbody").find(' tr').remove();
	}
	var abDtRows = $("#addUserModal #userSchoolTable").dataTable().fnGetNodes();
	$.each(abDtRows,function(i,row){		
		selectedSchools.push(parseInt($(row).prop('id')));	
	});
}

//get school for campaign and bind to the table for adding to campaign
var fnBindSchoolForCampaign =function(e){
	fnGetSelectedLocations();
	fnGetSelectedSchoolType();
	fnGetSelectedSchools();
	var rData ={
			"locations":selectedLocations,
			"schoolTypes":selectedSchooltypes,
			"schoolIds":selectedSchools,
	};
	var schoolVm = customAjaxCalling("school/user/"+selectedLocType,rData,"POST");
	if (schoolVm.length != 0) {
		noOfSchools =schoolVm.length;
		fnBindSchoolToTable(schoolVm);		
	}else{
		e.preventDefault();
		$('#mdlErrorForSchool #msgTextForSchool').text('');
		$('#mdlErrorForSchool #msgTextForSchool').text("There are no schools of the above selections.");
		$('#mdlErrorForSchool').modal().show();		
	}
}

//bind row to the add school table in wizard
var fnBindSchoolToTable =function(dSchool){
	deleteAllRow('addSchoolWizardTable');
	selectedChkBox =[];
	$.each(dSchool,	function(index, obj) {
		if(obj.isMember){
		cityName =(obj.cityName==null)?"N/A":obj.cityName ;
		villageName =(obj.villageName==null)?"N/A":obj.villageName;
		var row = "<tr id='"+ obj.id+ "' data-id='"	+obj.id+ "' data-school='"	+obj.schoolName+ "' data-statename='"+obj.stateName+"'" +
		"data-zonename='" + obj.zoneName+"' data-distname='"+ obj.districtName+"'" +
		"data-tehsilname='"+ obj.tehsilName+"' data-blockname='"+ obj.blockName+"'" +
		"data-cityname='"+ cityName+"' data-nyaypanchayatname='"+ obj.nyayPanchayatName+"'" +
		"data-grampanchayatname='"+obj.gramPanchayatName+"'" +
		"data-revenuevillagename='" + obj.revenueVillageName+"'" +
		"data-villagename='"+ villageName+"'>"+					
		"<td data-id='"+ obj.id+"'><center><input class='chkbox' type='checkbox' onchange='fnChkUnchk("+obj.id+")'  id='chkboxAddSchoolWizard"+obj.id+"'></input></center></td>"+
		"<td title='"+ obj.schoolName+ "'>"+ obj.schoolName+ "</td>"+
		"</tr>";
		fnAppendNewRow("addSchoolWizardTable", row);
	}
	});	
	fnDTColFilterForAddSchoolWizard('#addSchoolWizardTable',2,[0],false);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#addUserModal #addSchoolWizardTable',[]);
	});
}

//add selected schools to the table
var addSchool =function(event){
	console.log("add school to user.....");
	userSchools =[];
	$(addSchoolMdlSelectDivs.schoolElement).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				selectedSchools =new Object();
				selectedSchools.id =$(this).data('id');
				selectedSchools.name =$(this).text();
				selectedSchools.state =$(this).data('statename');
				selectedSchools.zone =$(this).data('zonename');
				selectedSchools.dist =$(this).data('distname');
				selectedSchools.city =$(this).data('cityname');
				selectedSchools.village =$(this).data('villagename');
				userSchools.push(selectedSchools);
			}			
		});
	});
	createNewSchoolRow(userSchools);
	$('#mdlAddSchoolForUser').modal('hide');
}

var createNewSchoolRow = function(selectedSchools) {
	if ($('#addUserModal #userSchoolTable >tbody>tr').length == 1) {
		$("#addUserModal #userSchoolTable tbody").find(' tr').remove();
	}
	$.each(selectedSchools,	function(index, obj) {
		var row = "<tr id='"+ obj.id+ "' data-id='"	+obj.id+ "'>"+
		"<td data-id='"+ obj.id+"'><center><input class='chkboxDelSchool' type='checkbox' onchange='fnChkUnchkDelSchool("+obj.id+")'  id='chkboxDelSchool"+obj.id+"'></input></center></td>"+ 
		"<td title='"+ obj.state+ "'>"+ obj.state+ "</td>"+
		"<td title='"+ obj.dist+ "'>"+ obj.dist+"</td>"+
		"<td title='"+ obj.city+ "'>"+ obj.city+ "</td>"+
		"<td title='"+ obj.village+ "'>"+ obj.village+ "</td>"+
		"<td title='"+ obj.name+ "'>"+ obj.name+ "</td>"+
		"<td><div  class='div-tooltip'><a  class='tooltip tooltip-top'	'id='btnRemoveSchool'"+
		"onclick=removeSchool(&quot;"+ obj.id+ "&quot;,&quot;"+ escapeHtmlCharacters(obj.name)+ "&quot;);> <span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"+
		"</tr>";
		fnAppendNewRow("userSchoolTable", row);
	});
}

//remove school from table
var removeSchool =function(rowId,schoolName){
	removeSchoolRowId=rowId;
	$('#mdlRemoveSchool #msgText').text('');	
	$('#mdlRemoveSchool #msgText').text("Do you want to remove school "+schoolName+ "?");
	$('#mdlRemoveSchool').modal().show();
}
$('#mdlRemoveSchool #btnRemoveSchool').on('click',function(){
	console.log("remove school clicked...");
	deleteRow("userSchoolTable",removeSchoolRowId);
	$('#mdlRemoveSchool').modal('hide');
});

//get the selected values and push to respective array for assessment
var getSelectedData =function(elementObj,collector){
	$(elementObj).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				collector.push( parseInt($(option).val()));
			}
		});
	});
};

var fnChkUnchk =function(id){	 	 
	var idx = $.inArray(id, selectedChkBox);	
	if (idx == -1) {
		selectedChkBox.push(id);
		selectedChkBox.length==noOfSchools? $('#chkboxSelectAll').prop('checked',true): $('#chkboxSelectAll').prop('checked',false);
	} else {
		selectedChkBox.splice(idx, 1);
		$('#chkboxSelectAll').prop('checked',false);
	}
};

//on click of select all in table header for adding schools to school wizard...
var fnInvokeSelectAll =function(){
	var isChecked =$('#chkboxSelectAll').is(':checked');
	var rows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
	if(isChecked==true){
		fnDoSelectAllAddSchool(isChecked);
	}else{
		selectedChkBox =[];
		$.each(rows,function(i,row){
			//var id="#chkbox"+($(row).data('id').toString());
			var id="#chkboxAddSchoolWizard"+($(row).prop('id').toString());
			$(id).prop('checked',false);
		});	
	}		
};

//add selected schools to the table
var fnAddSchool = function() {
	userSchools = [];
	var rows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
	$.each(rows,function(i,row){
		
			 

		if($(row).find('.chkbox').prop('checked')){
			selectedSchools = {};
			selectedSchools.id = $(row).data('id')
			selectedSchools.name = $(row).data('school');
			selectedSchools.state = $(row)	.data('statename');
			selectedSchools.zone = $(row).data('zonename');
			selectedSchools.dist = $(row).data('distname');
			selectedSchools.tehsil = $(row).data('tehsilname');
			selectedSchools.block = $(row).data('blockname');
			selectedSchools.city = $(row).data('cityname');
			selectedSchools.nyaypanchayat = $(row).data('nyaypanchayatname');
			selectedSchools.grampanchayat = $(row).data('grampanchayatname');
			selectedSchools.revenuevillage = $(row).data('revenuevillagename');
			selectedSchools.village = $(row).data(	'villagename');
			userSchools.push(selectedSchools);
		}
	});
	createNewSchoolRow(userSchools);
	$('#mdlAddSchoolForUser').modal('hide');
}

$(function() {
	$('#addSchoolSaveButton').prop('disabled',true);
	setDataTablePagination("#userSchoolTable");
	initGlobalValues();
	fnColSpanIfDTEmpty('userSchoolTable',8);
	$('#mdlAddSchoolForUser #addLocColspFilter').on('show.bs.collapse', function(){
		$('#mdlAddSchoolForUser #locationPnlLink').text('Hide Location');
	});  
	$('#mdlAddSchoolForUser #addLocColspFilter').on('hide.bs.collapse', function(){
		$('#mdlAddSchoolForUser #locationPnlLink').text('Select Location');
	});
	//for add school wizard of campaign
	$("#" + addSchoolForm).formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			schoolType: {
				validators: {
					choice: {
						min: 1,
						max: 2,
						message: ' '
					}
				}
			},
			locType: {
				validators: {
					notEmpty: {
						message: ' '
					}
				}
			}
		}
	});
	
	$('#addUserWizard').wizard().on('actionclicked.fu.wizard',function(e, data) {
		
		step = data.step // Current step
		
		$container = $('#addUserForm').find('.step-pane[data-step="' + step + '"]');
		if (step == 2) {
			hideDiv($('#divInfo'));
		}
		if (step == 3) {
			$(addUserMdlElements.roles).each(function(index,ele){
				$(ele).find("option:selected").each(function(ind,option){
					if($(option).val()==9){
						showDiv($('#divInfo'));
					}			
				});
			});
		}
	})
	
	$('#addUserSchoolWizard').wizard().on('actionclicked.fu.wizard',function(e, data) {
		var fv = $('#addSchoolForm').data('formValidation'), // FormValidation
		// instance
		step = data.step, // Current step
		// The current step container
		$container = $('#addSchoolForm').find('.step-pane[data-step="' + step + '"]');

		// Validate the container
		fv.validateContainer($container);

		var isValidStep = fv.isValidContainer($container);		
		if (isValidStep === false || isValidStep === null) {
			// Do not jump to the target panel
			e.preventDefault();
		}
		if (step == 1 && isValidStep) {
			fnBindSchoolForCampaign(e);
		}
		if (step == 2 && isValidStep && data.direction!='previous') {
			var isOneSchoolSelected =false;
			var aaDtRows = $("#addSchoolWizardTable").dataTable().fnGetNodes();
			$.each(aaDtRows,function(i,row){			
				if($(row).find('.chkbox').is(':checked') === true){
					isOneSchoolSelected =true;
				}
			});
			if(!isOneSchoolSelected){
				e.preventDefault();
				$('#mdlErrorForSchool #msgText').text('');
				$('#mdlErrorForSchool #msgText').text("Please select at least one school.");
				$('#mdlErrorForSchool').modal().show();
			}
		}
		
		//if not selected any school, notification...
	})
//	Triggered when clicking the Complete button
	.on('finished.fu.wizard',function(e) {
		$('#addUserSchoolWizard #btnNext').prop('disabled',true);

		var fv = $('#addSchoolForm').data('formValidation'),
		step = $('#addSchoolForm').wizard('selectedItem').step, 
		$container = $('#addSchoolForm').find('.step-pane[data-step="' + step + '"]');

		// Validate the last step container
		fv.validateContainer($container);

		var isValidStep = fv.isValidContainer($container);
		if (isValidStep === true) {			
			fnAddSchool();
		}
	});
});
$('#mdlResetLoc').on('hidden.bs.modal', function () {
	$("body").addClass("modal-open");

});