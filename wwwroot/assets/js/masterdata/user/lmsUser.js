//global variables
var schoolGradeFormId;
var addRoleData;
var addFormId;
var editFormId;
var gradeIdList;
var sectionIdList;
var addSectionsIds;
var editSectionsIds;
var editMapperId;
var schoolId;
var mappedGrades;
var mappedSchool;
var deletedGradeId;
var deletedSchoolId;
var deleteSchoolGradeId;
var addButtonId;
var isSchoolDeleted = false;
var editedRowId;
var isAbleTOCreateUser;
var isMappedToschool;
var listTableName;
var sectionObj;
var isRoleTeacher;
var isInternalType;
var selectedSchool;
var requestData;
var thLabels;
var $ele;
var selectedSchoolsOfuser;
var clickedObjEvent;
var isSchoolAdded;
var addRoleId;
var isEdited;
var selectedRoleId;

//global selectDivIds for smart filter
var userPageContext = {
		city 					: "#divSelectCity",
		block 					: "#divSelectBlock",
		NP 						: "#divSelectNP",
		GP 						: "#divSelectGP",
		RV 						: "#divSelectRV",
		village 				: "#divSelectVillage",
		school 					: "#divSelectSchool",
        schoolLocationType 		: "#selectBox_schoolLocationType",
		elementSchool 			: "#selectBox_schoolsByLoc",
		grade 					: "#selectBox_grades",
        resetLocDivCity 		: "#divResetLocCity",
		resetLocDivVillage 		: "#divResetLocVillage",
};
//edit user wizard
var editUserMdlElements = {

		userId 						: "#editUserModal #editUserid",
		userName		 			: "#editUserModal  #editUserName",
		userLogin 					: "#editUserModal #editLoginName",
		isInternal 					: "#editUserModal #editUserType",
		sapCode 					: "#editUserModal #editIuserSAPCode",
		roles			 			: "#editUserModal #selectBox_editRoleType",
		designation 				: "#editUserModal #editDesignation",
		reportingToDesignaion 		: "#editUserModal #selectBox_editReportingToDesignaion",
		reportingToName 			: "#editUserModal #selectBox_editReportingToName",
		gender 						: "#editUserModal #editGender",
		email 						: "#editUserModal #editEmail",
		phone 						: "#editUserModal #editPhone",
		emergencyContact 			: "#editUserModal #editEmergencyNo",
		bloodGroup 					: "#editUserModal #editBloodGroup",
		workAddress 				: "#editUserModal #editWorkAddress",
		localAddress 				: "#editUserModal #editLocalAddress",
		permanentAddress 			: "#editUserModal #editPermanentAddress",
		highestQualification 		: "#editUserModal #editHighestQualification",
		hqYearOfPassing 			: "#editUserModal #editHQyearOfPassing",
		professionalQualification 	: "#editUserModal #editProfessionalQualification",
		pqYearOfPassing 			: "#editUserModal #editPQyearOfPassing",
		experience 					: "#editUserModal #editExperienceInYears",
		trainedTeacherType 			: "#editUserModal #trainedTeacherTypeEdit",
};

//initialise global variables
var initGlblVariables = function() {

	isAbleTOCreateUser = false;
	isMappedToschool = false;
	isRoleTeacher = false;
	isInternalType = false;
	schoolGradeFormId = "listUserForm";
	addFormId = "addSchoolGradeSecForm";
	editFormId = "editSchoolGradeSecForm";
	gradeIdList = [];
	schoolId = "";
	mappedGrades = [];
	mappedSchool = "";
	deletedGradeId = "";
	deletedSchoolId = "";
	deleteSchoolGradeId = "";
	listTableName ="schoolGradeTable";
	editedRowId = "";
	addSectionsIds = [];
	editSectionsIds = [];
	editMapperId = "";
	addButtonId = "";
	//get all section on page load
	sectionObj = customAjaxCalling("section/list", null, "GET");
	sectionIdList = [];
	$.each(sectionObj, function(index, obj) {
		sectionIdList.push(obj.sectionId);
	});
};

//create table



var createUserRow = function(user) {
	
	var row = '';
	var cityName="N/A"; 
	var villageName="N/A" ; 
	var districtName="N/A"; 	
	var stateName="N/A";
	
	if (user.centers!=null?user.centers.length > 0:false) {
		var isLoginUser="";
		if(user.loggedUser==true || user.loggedUser=='true'){
			isLoginUser="Yes";
		}else{
			isLoginUser="NO";
		}
		$.each(user.centers, function(index, center) {
			if(center.city==null){

				villageName =center.village.villageName;
				districtName=center.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.districtName;
				stateName=center.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.zone.state.stateName;	
			} else{
				cityName =center.city.cityName;
				districtName=center.city.tehsil.district.districtName;
				stateName=center.city.tehsil.district.zone.state.stateName;
			}
			
			row = "<tr id='" + user.userId + "_" + isLoginUser+ "_" + user.userLogin + "'data-name='" + user.userName + "'data-loggedUserRoleSequence='" + logedUserSequenceNumber + "' data-ownUserRoleSequence='" + user.ownSequenceNumber + "' data-id='" + user.userId + "'>" +
			"<td></td>" +
			"<td data-userName='" + user.userName + "'	title='" + user.userName + "'>" + user.userName + "</td>" +
			"<td data-userName='" + user.userLogin + "'	title='" + user.userLogin + "'>" + user.userLogin + "</td>" +
			"<td data-userName='" + user.roleName + "'	title='" + user.roleName + "'>" + user.roleName + "</td>" +
			"<td data-stateName='" + stateName + "'	title='" + stateName + "'>" + stateName + "</td>" +
			"<td data-districtName='" + districtName + "'	title='" + districtName + "'>" + districtName + "</td>" +
			"<td data-userName='" + cityName + "'	title='" + cityName + "'>" + cityName + "</td>" +
			"<td data-villageName='" + villageName + "'	title='" + villageName + "'>" + villageName + "</td>" +
			"<td data-schoolName='" + center.name + "'	title='" + center.name + "'>" + center.name + "</td>" +
			"<td><div class='btn-group'><button type='button'  class='btn btn-link' style='display: none;' id='btnEditUser'" +
			"onclick=editUserModal(&quot;" + user.id + "&quot;,&quot;" + escapeHtmlCharacters(user.userName) + "&quot;,&quot;" + user.schoolId + "&quot;);><span class='glyphicon glyphicon-edit font-size12'></span>"+appMessgaes.edit+"</button><button type='button' class='btn btn-link' style='display: none;' data-toggle='modal'		data-target='#mdlDelUser' id=deleteUser" + user.id + "'	onclick=deleteUserData(&quot;" + user.id + "&quot;,&quot;" + escapeHtmlCharacters(user.userName) + "&quot;);><span class='glyphicon glyphicon-remove font-size12'></span>"+appMessgaes.delet+"</button></div></td>" +
			"</tr>";
			appendNewRow('userTable', row);

		})
	} else {

		row = row +
		"<tr id='" + user.userId + "_" + isLoginUser + "_" + user.userLogin +"'data-name='" + user.userName + "' data-loggedUserRoleSequence='" + logedUserSequenceNumber + "' data-ownUserRoleSequence='" + user.ownSequenceNumber + "' data-id='" + user.userId + "'>" +
		"<td></td>" +
		"<td data-userName='" + user.userName + "'	title='" + user.userName + "'>" + user.userName + "</td>" +
		"<td data-userName='" + user.userLogin + "'	title='" + user.userLogin + "'>" + user.userLogin + "</td>" +
		"<td data-userName='" + user.roleName + "'	title='" + user.roleName + "'>" + user.roleName + "</td>" +
		"<td data-stateName='N/A'	title='N/A'>N/A</td>" +
		"<td data-stateName='N/A'	title='N/A'>N/A</td>" +
		"<td data-stateName='N/A'	title='N/A'>N/A</td>" +
		"<td data-stateName='N/A'	title='N/A'>N/A</td>" +
		"<td data-stateName='N/A'	title='N/A'>N/A</td>" +
		"<td><div class='btn-group'><button type='button'  class='btn btn-link' style='display: none;' id='btnEditUser'" +
		"onclick=editUserModal(&quot;" + user.id + "&quot;,&quot;" + escapeHtmlCharacters(user.userName) + "&quot;,&quot;" + user.schoolId + "&quot;);><span class='glyphicon glyphicon-edit font-size12'></span>"+appMessgaes.edit+"</button><button type='button' class='btn btn-link' style='display: none;' data-toggle='modal'		data-target='#mdlDelUser' id=deleteUser" + user.id + "'	onclick=deleteUserData(&quot;" + user.id + "&quot;,&quot;" + escapeHtmlCharacters(user.userName) + "&quot;);><span class='glyphicon glyphicon-remove font-size12'></span>"+appMessgaes.delet+"</button></div></td>" +
		"</tr>";
		appendNewRow('userTable', row);
	}
};
var populateDataForRole327 = function(roleId) {
	var userList = customAjaxCalling("getUserListByRoleId/" + roleId, null, "GET");
	deleteAllRow("userTable");
	$.each(userList, function(index, obj) {
		var row = createUserRow(obj);
		appendNewRow("userTable", row);
	});


};
var mappingData = function() {
	window.location = baseContextPath + "/schoolconfiguration";
};


var resetLocatonAndForm = function(type) {
	$("#divFilter").hide();
	if (type == "add") {
		resetFormById(addSchoolForm);
	}
	reinitializeSmartFilter(elementIdMap);
	getSchoolTypeAndLoc();
	deleteAllRow("userTable");
};

var applyPermissions = function(btnEditUser, btnDeleteUser) {

	if (!editUserPermission) {
		$(btnEditUser).remove();

	}
	if (!deleteUserPermission) {
		$(btnDeleteUser).remove();
	}

};

var disableModifyUserControlsOnRoleSequenceBasis = function() {

	$(".group").each(function(index, groupRowItem) {
		var userDetailRow = $(groupRowItem).next("tr");
		var ownuserrolesequence = $(userDetailRow).data("ownuserrolesequence");
		var editButton = $(groupRowItem).find(".action-edit");
		var deleteButton = $(groupRowItem).find(".action-delete");

		if (logedUserSequenceNumber >= ownuserrolesequence && logedUserSequenceNumber != 1) {

			$(editButton).remove();
			$(deleteButton).remove();

		}

	});
};
//get all the users on basis of role and display
var getUsers = function() {
	$("#columnTableId").hide();
	enableTooltip();
	$(".outer-loader").show();
	setTimeout(function() {


		var roleId = $("#selectBox_roleType").val();


		var allUserData = customAjaxCalling("user/list/" + roleId, null, "GET");
		
		deleteAllRow("userTable");
		$.each(allUserData, function(index, obj) {
			createUserRow(obj);
		});
		 thLabels = ['#', columnMessages.userName,columnMessages.loginName,columnMessages.role,columnMessages.state,columnMessages.district,columnMessages.city,columnMessages.village,columnMessages.center,columnMessages.action];
		
		rowGroupingDataTable('#userTable', 10, [0, 9], 2, thLabels);

		fnMultipleSelAndToogleDTCol('.search_init', function() {
			fnHideColumns('#userTable', [2]);
		});

		showDiv($('#tblDiv'));
		$('#columnTableId').show();
		$('.outer-loader').hide();
		applyPermissions(".action-edit", ".action-delete");
		disableModifyUserControlsOnRoleSequenceBasis();
		$("#userTable #h2").removeClass("sorting_asc");
		//logic to disable
		return;
	}, 300);
};



//super admin can change any user Password
var userIdForResetPassword = 0;
var resetPasswordData = function(userDataId) {
	userIdForResetPassword = userDataId;
	resetFormById("resetPasswordUserForm");
	$("#mdlResetUserPassword").modal().show();

};
var resetUserPassword = function() {
	var newpassword = $('#newpassword').val();

	var json = {
			"newPassword": newpassword,
			"userId": userIdForResetPassword
	}
	var result = customAjaxCalling("resetUserPassword", json, "POST");
	if (result.response == "shiksha-200") {		

		$("#mdlResetUserPassword").modal("hide");

		setTimeout(function() {   //calls click event after a one sec
			AJS.flag({
				type: 'success',
				 title: appMessgaes.success,
				body: '<p>'+result.responseMessage+'</p>',
				close :'auto'
			})
		}, 1000);

	} else {

		$('#errorPwdMsg').show().text(result.responseMessage);

	}
};

//delete user section //
var deleteUserId = 0;
var deleteUserData = function(userId) {
	
	deleteUserId = userId.split('^')[0];
	
	$('#delUserMsgTxt').text(
			columnMessages.deleteConfirm +" "+ userId.split('^')[1] + " ?");
	$('#mdlDelUser').modal();
	hideDiv($('#mdlDelUser #alertdiv'));
};
var deleteData = function(id) {
	isAbleTOCreateUser = (parseInt(logedUserSequenceNumber) < 50) ? (parseInt(logedUserSequenceNumber) < 10) ? true : false : false;

	if (isAbleTOCreateUser === true) {
		deleteUserData(id);

	} else {
		$('#editUserSchoolConfirmationModal #msgText').text(validationMsg.youHaveNoPreviligesToCreateUser);
		$('#editUserSchoolConfirmationModal').modal();
	}
};

var deleteUser = function() {
	var userData = {
			"id": deleteUserId
	}
	var responseData = customAjaxCalling("user", userData, "DELETE");
	if (responseData.response == "shiksha-200") {
		$(".outer-loader").hide();

		deleteRow('userTable', deleteUserId);
		getUsers();
		$('#mdlDelUser').modal('hide');
		setTimeout(function() {   //calls click event after a one sec
			AJS.flag({
				type: 'success',
				title: appMessgaes.success,
				body: '<p>'+responseData.responseMessage+'</p>',
				close :'auto'
			})
		}, 1000);

		$('#notify').fadeIn(3000);
	} else {
		$(".outer-loader").hide();
		showDiv($('#mdlDelUser #alertdiv'));
		$("#mdlDelUser").find('#successMessage').hide();
		$("#mdlDelUser").find('#okButton').hide();
		$("#mdlDelUser").find('#errorMessage').show();
		$("#mdlDelUser").find('#exception').show();
		$("#mdlDelUser").find('#buttonGroup').show();
		$("#mdlDelUser").find('#exception').text(responseData.responseMessage);

	}
	$(window).scrollTop(0);
};

//edit user section //
var fnBindReportingToDesignaionName = function(element, designaionNameList, type) {
	$(element).multiselect('destroy');
	if (designaionNameList != null) {
		$ele = $(element);
		$ele.html('');
		$.each(designaionNameList, function(index, obj) {
			$ele.append('<option  value="' + obj + '">' + escapeHtmlCharacters(obj) + '</option>');
		});
	}
	setOptionsForMultipleSelect(element);
};

var fnBindReportingToName = function(element, designation, opType) {
	if (designation != null && designation != "") {
		var designationNames = customAjaxCalling("getAllUserByDesignation/" + designation, null, "GET");
		fnBindReportingToDesignaionName(element, designationNames, opType);
	}
}


var setMultiSelectForAddDesignaion = function(ele) {
	$(ele).multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		//	dropUp : true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		numberDisplayed: 2,
		filterPlaceholder: 'Search here...',
		disableIfEmpty: true,
		onDropdownHide: function() {
			var designation = $(ele).find("option:selected").val();
			fnBindReportingToName("#selectBox_addReportingToName", designation, "add");
			if ($('#selectBox_addReportingToDesignaion').find("option:selected").val() != undefined) {
				$("#addReportingToNameDiv").show();
			} else {
				$("#addReportingToNameDiv").hide();
			}
		}, onDropdownShow: function() {
			$("#addUserForm").scrollTop(425);
		}
	});
};
var setMultiSelectForEditDesignaion = function(ele) {
	$(ele).multiselect({
		maxHeight: 220,
		includeSelectAllOption: true,
		//	dropUp : true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		numberDisplayed: 2,
		filterPlaceholder: 'Search here...',
		disableIfEmpty: true,
		onDropdownHide: function() {
			var designation = $(ele).find("option:selected").val();
			fnBindReportingToName("#selectBox_editReportingToName", designation, "edit");
			if ($('#selectBox_editReportingToDesignaion').find("option:selected").val() != undefined) {
				$("#editReportingToNameDiv").show();
			} else {
				$("#editReportingToNameDiv").hide();
			}
		}, onDropdownShow: function() {
			$("#editUserForm").css("position","relative");

			$("#editUserForm").scrollTop(550);
		}
	});
};
var fnBindReportingToDesignaion = function(element, designaionList, type) {
	$(element).multiselect('destroy');
	if (designaionList != null) {
		$ele = $(element);
		$ele.html('');
		$.each(designaionList, function(index, obj) {
			//if(obj!="")
			$ele.append('<option  value="' + obj + '">' + escapeHtmlCharacters(obj) + '</option>');

		});

		if (type == "add") {
			setMultiSelectForAddDesignaion("#selectBox_addReportingToDesignaion");
		} else {
			setMultiSelectForEditDesignaion("#selectBox_editReportingToDesignaion");
		}
		fnCollapseMultiselect();
	}
};
var fnappendNewRow = function(tableName, aRow) {
	var oaTable = $(tableName).DataTable();
	oaTable.page();
	oaTable.row.add($(aRow)).draw(false);
	var currentRows = oaTable.data().toArray(); // current table data
	//Issue-SHK-964
	var newRow = currentRows.pop();
	var info = $(tableName).DataTable().page.info();
	var startRow = info.start;
	currentRows.splice(startRow, 0, newRow);
	oaTable.clear();
	oaTable.rows.add(currentRows);
	oaTable.draw(false);

};


var fnUserCreateCenterRow = function(selectedCenters) {
	if(selectedCenters.length>0){
		$.each(selectedCenters, function(index, obj) {

			var row; 
			var cityName="N/A"; 
			var villageName="N/A" ; 
			var districtName="N/A"; 
			var stateName="N/A";
			if(obj.city==null){

				villageName =obj.village.villageName;

				districtName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.districtName;

				stateName=obj.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.zone.state.stateName;


			}else{
				cityName =obj.city.cityName;


				districtName=obj.city.tehsil.district.districtName;

				stateName=obj.city.tehsil.district.zone.state.stateName;

			}
			row = "<tr id='" + obj.id + "'data-oldCenter='true' data-id='" + obj.id + "'>" +
			"<td></td>" +
			"<td title='" + stateName + "'>" + stateName + "</td>" +
			//"<td title='"+obj.zone+"'>"+obj.zoneName+"</td>"+
			"<td title='" + districtName + "'>" + districtName + "</td>" +
			"<td title='" + cityName + "'>" + cityName + "</td>" +
			"<td title='" + villageName + "'>" + villageName + "</td>" +
			"<td title='" + obj.name + "'>" + obj.name + "</td>" +
			"<td><div class='div-tooltip'><a class='tooltip tooltip-top' id='btnRemoveCenter'" +
			"onclick=removeEditUserCenter(&quot;" + obj.id + "&quot;,&quot;" + escapeHtmlCharacters(obj.name) + "&quot;,&quot;" + true + "&quot;);><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>" +
			"</tr>";
			fnappendNewRow("#editUserModal #editUserCenterTable", row);
		});
	}
};


var fnBindUserType = function(element) {
	var userTypeList = ["Internal User", "External User"];
	$(element).multiselect('destroy');
	if (userTypeList.length) {
		$ele = $(element);
		$ele.html('');
		$.each(userTypeList, function(index, obj) {
			if (obj == "Internal User") {
				$ele.append('<option  value="true">' + escapeHtmlCharacters(obj) + '</option>');
			} else {
				$ele.append('<option  value="false">' + escapeHtmlCharacters(obj) + '</option>');
			}

		});
	}
	setOptionsForMultipleSelect(element);
};

var checkIsTeacherAdd = function() {
	$('#selectBox_addRoleType').each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				if ($(option).data('name') == "Teacher") {
					isRoleTeacher = true;
				} else {
					isRoleTeacher = false;
				}
			}
		});
	});
	if ($('#selectBox_addRoleType').find('option:selected').length < 1) {
		isRoleTeacher = false;
	}
};
var hideAndShowIstrainedDiv = function(element) {
	if (isRoleTeacher  || isRoleTeacher == "true") {
		$(element).show();
	} else {
		$(element).hide();
	}
};

var addUserMdlElements = {};
addUserMdlElements.roles = "#addUserModal #selectBox_addRoleType";

var hideEmailStar = function() {
	$("#addEmailSpan").hide();
	$("#editEmailSpan").hide();
};
var showEmailStar = function() {
	$("#addEmailSpan").show();
	$("#editEmailSpan").show();
};

var checkEmailValidation = function(formId, element, value) {
	showEmailStar();
	var roleDataIds = [];
	$(element).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				roleDataIds.push($(this).data('id'));
			}
		});
	});
	if (roleDataIds.length > 1) {
		if (value == "") {
			$("#" + formId).data('formValidation').updateStatus('email', 'NOT_VALIDATED');
			return false;
		} else {
			$("#" + formId).data('formValidation').updateStatus('email', 'VALID');
			return true;
		}
	} else if (roleDataIds.length == 1) {
		if (roleDataIds[0] == 9) {
			$("#" + formId).data('formValidation').updateStatus('email', 'VALID');
			hideEmailStar();
			return true;
		} else {
			if (value == "") {
				$("#" + formId).data('formValidation').updateStatus('email', 'NOT_VALIDATED');
				return false;
			} else {
				$("#" + formId).data('formValidation').updateStatus('email', 'VALID');
				return true;
			}
		}
	} else {
		$("#" + formId).data('formValidation').updateStatus('email', 'NOT_VALIDATED');
		return false;
	}
};
//set multiselect options for role select box in add user screen
var setMultiSelectForAddUserRole = function(ele) {
	checkIsTeacherAdd();
	hideAndShowIstrainedDiv("#trainedTeacherTypeDiv");
	$(ele).multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		//	dropUp : true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		numberDisplayed: 2,
		filterPlaceholder: 'Search here...',
		disableIfEmpty: true,
		onDropdownHide: function() {
			$(addUserMdlElements.roles).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all") {
						if ($(option).data('ismappedschool')) {
							isMappedToschool = true;
						} else{
							isMappedToschool = false;
						}
					}
				});
			});
			if ($(addUserMdlElements.roles).find('option:selected').length < 1) {
				isMappedToschool = false;
				$("#addEmailDiv").hide();
			} else {
				$("#addEmailDiv").show();
			}
			checkIsTeacherAdd();
			hideAndShowIstrainedDiv("#trainedTeacherTypeDiv");

			var emailId = $("#addEmail").val();
			checkEmailValidation("addUserForm", "#selectBox_addRoleType", emailId);


		}, onDropdownShow: function() {
			$("#addUserForm").scrollTop(425);
		}
	});
};
/**/

var checkIsMappedToSchool = function() {
	$('#selectBox_editRoleType').each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				if ($(option).data('ismappedschool')) {
					isMappedToschool = true;
				} else {isMappedToschool = false}
			}
		});
	});
	if ($('#selectBox_editRoleType').find('option:selected').length < 1) {
		isMappedToschool = false;
	}
};

var checkIsTeacher = function() {
	$('#selectBox_editRoleType').each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				if ($(option).data('name') == "Teacher") {
					isRoleTeacher = true;
				} else isRoleTeacher = false;
			}
		});
	});
	if ($('#selectBox_editRoleType').find('option:selected').length < 1) {
		isRoleTeacher = false;
	}
};

var setMultiSelectForEditUserRole = function(ele) {
	checkIsMappedToSchool();
	checkIsTeacher();
	hideAndShowIstrainedDiv("#trainedTeacherTypeDivEdit");
	$(ele).multiselect({
		maxHeight: 220,
		includeSelectAllOption: true,
		//dropUp : true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		numberDisplayed: 2,
		filterPlaceholder: 'Search here...',
		disableIfEmpty: true,

		onDropdownHide: function() {
			checkIsMappedToSchool();
			checkIsTeacher();
			hideAndShowIstrainedDiv("#trainedTeacherTypeDivEdit");

			var emailId = $("#editEmail").val();
			checkEmailValidation("editUserForm", "#selectBox_editRoleType", emailId);

		}, onDropdownShow: function() {
			$("#editUserForm").scrollTop(425);
		}
	});

};

var fnBindRolesToListBox = function(element, editRoleData, type) {
	$(element).multiselect('destroy');

	if (editRoleData != null) {
		$ele = $(element);

		$ele.html('');
		$.each(editRoleData, function(index, obj) {
			if (obj.isSelected) {
				$ele.append('<option selected value="' + obj.roleId + '" data-id="' + obj.roleId + '" data-ismappedschool="' + obj.ismappedschool + '" data-sequenceNumber="' + obj.sequenceNumber + '"data-name="' + obj.roleName + '">' + escapeHtmlCharacters(obj.roleName)+' (' +obj.roleCode+ ') </option>');
			} else {
				$ele.append('<option  value="' + obj.roleId + '" data-id="' + obj.roleId + '" data-ismappedschool="' + obj.ismappedschool + '" data-sequenceNumber="' + obj.sequenceNumber + '"data-name="' + obj.roleName + '">' + escapeHtmlCharacters(obj.roleName) +' (' +obj.roleCode+ ') </option>');

			}
		});

		if (type == "add") {
			setMultiSelectForAddUserRole(addUserMdlElements.roles);

		} else {
			setMultiSelectForEditUserRole(editUserMdlElements.roles);
		}

		fnCollapseMultiselect();
	}
};
var fnDisableEditSaveBtn=function(){
	$('#editUserSaveBtn').prop('disabled',true);
	$(".edit-user-submit").prop('disabled',true);
}
var editData = function(aEditUserId) {
	var addDesignaionList = customAjaxCalling("getAllDesignation", null, 'GET');
	var getEditUserData = customAjaxCalling("getUserDataByuserId/" + aEditUserId, null, "GET");
	resetFormById('editUserForm');
	$("#editReportingToNameDiv").hide();
	fnBindReportingToDesignaion("#selectBox_editReportingToDesignaion", addDesignaionList, "edit");
	setOptionsForMultipleSelectWithoutSearchUser("#editUserType","#editUserForm",500);
	$(editUserMdlElements.userId).val(getEditUserData.id);
	$(editUserMdlElements.userName).val(getEditUserData.userName);
	$(editUserMdlElements.userLogin).val(getEditUserData.userLogin);
	$(editUserMdlElements.email).val(getEditUserData.email);
	$(editUserMdlElements.phone).val(getEditUserData.phone);
	$(editUserMdlElements.emergencyContact).val(getEditUserData.emergencyContact);
	$(editUserMdlElements.localAddress).val(getEditUserData.localAddress);
	$(editUserMdlElements.permanentAddress).val(getEditUserData.permanentAddress);
	$(editUserMdlElements.workAddress).val(getEditUserData.workAddress);
	$(editUserMdlElements.designation).val(getEditUserData.designation);
	$(editUserMdlElements.highestQualification).val(getEditUserData.highestQualification);
	$(editUserMdlElements.professionalQualification).val(getEditUserData.professionalQualification);
	$(editUserMdlElements.experience).val(getEditUserData.experience);
	$(editUserMdlElements.trainedTeacherType).val(getEditUserData.trainedTeacherType);
	$(editUserMdlElements.isInternal).val(getEditUserData.isInternal);
	$(editUserMdlElements.hqYearOfPassing).val(getEditUserData.hqYearOfPassing);

	$(editUserMdlElements.pqYearOfPassing).val(getEditUserData.pqYearOfPassing);
	$(editUserMdlElements.bloodGroup).val(getEditUserData.bloodGroup);

	$(editUserMdlElements.gender).val(getEditUserData.gender);
	$(editUserMdlElements.sapCode).val(getEditUserData.sapCode);
	
	fnUserCreateCenterRow(getEditUserData.centers);	
	


	$('#editBloodGroup').find('option').each(function(ind, obj) {
		if ($(obj).val() == getEditUserData.bloodGroup) {
			$(obj).prop('selected', true);
		}
	});
	$('#editBloodGroup').multiselect('refresh');
	if (getEditUserData.gender != "") {
		$('#editGender option[value=' + getEditUserData.gender + ']').attr(
				"selected", "selected");
	}
	fnBindReportingToName("#selectBox_editReportingToName", getEditUserData.reportingToDesignaion, "edit");
	fnBindUserType("#editUserType");
	if (getEditUserData.reportingToDesignaion != "") {
		$('#selectBox_editReportingToDesignaion option[value="' + getEditUserData.reportingToDesignaion + '"]').prop('selected', 'selected').change();
	}
	if (getEditUserData.reportingToName != "") {
		$('#selectBox_editReportingToName option[value="' + getEditUserData.reportingToName + '"]').prop('selected', 'selected').change();
	}

	if (getEditUserData.reportingToDesignaion != "") {
		$("#editReportingToNameDiv").show();
	} else {
		$("#editReportingToNameDiv").hide();
	}
	var isInternal = getEditUserData.isInternal;


	if (!isInternal) {
		$('#editUserType option[value=' + isInternal + ']').attr("selected", "selected");
	} else {
		$('#editUserType option[value=' + isInternal + ']').attr("selected", "selected");
	}
	$('#editUserType').multiselect("refresh");
	if (isInternal || isInternal == "true") {
		$('#editIuserSAPCodeDiv').show();
	} else {
		$('#editIuserSAPCodeDiv').hide();
	}

	fnBindRolesToListBox(editUserMdlElements.roles, getEditUserData.roleVms, "edit");
	setOptionsForMultipleSelect('#editGender');
	$('#editUserSaveBtn').prop('disabled', true);
	$("#editUserModal #success-msg").hide();
	$('#editUserModal').modal();
	checkEmailValidation("editUserForm", "#selectBox_editRoleType", getEditUserData.email);

	fnDisableEditSaveBtn();
}




/*var fnCreateNewSchoolRow = function(selectedSchools) {
	$.each(selectedSchools, function(index, obj) {

		var row = "<tr id='" + obj.id + "'data-oldSchool='false' data-id='" + obj.id + "'>" +
		"<td></td>" +
		"<td title='" + obj.state + "'>" + obj.stateName + "</td>" +
		//"<td title='"+obj.zone+"'>"+obj.zoneName+"</td>"+
		"<td title='" + obj.dist + "'>" + obj.districtName + "</td>" +
		"<td title='" + obj.city + "'>" + obj.cityName + "</td>" +
		"<td title='" + obj.village + "'>" + obj.villageName + "</td>" +
		"<td title='" + obj.name + "'>" + obj.schoolName + "</td>" +
		"<td><div class='div-tooltip'><a class='tooltip tooltip-top' id='btnRemoveSchool'" +
		"onclick=removeSchool(&quot;" + obj.id + "&quot;,&quot;" + escapeHtmlCharacters(obj.schoolName) + "&quot;,&quot;" + false + "&quot;);><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>" +
		"</tr>";
		fnappendNewRow("#editUserModal #editUserSchoolTable", row);
		//}

	});
};
*/

var customUserRedirect = function(redirectUrl, userId, userValue, roleId, roleValue) {
	var form = $('<form action="' + redirectUrl + '" method="post">' +
			'<input type="hidden" name="' + userId + '" value="' + userValue + '"></input>' +
			'<input type="hidden" name="' + roleId + '" value="' + roleValue + '"></input>' + '</form>');
	$('body').append(form);
	$(form).submit();
};

var fnRemoveActivePan=function(){
	$('#editpersonaldetailTabPanli').removeClass('active');
	$('#editpersonaldetail-tab').removeClass('active');

	$('#editqualificationdetailTabPanli').removeClass('active');
	$('#editqualificationdetail-tab').removeClass('active');

	$('#editcenterdetailTabPanli').removeClass('active');
	$('#editcenterdetail-tab').removeClass('active');

	$('#edituserdetail-tab').removeClass('active');
	$('#edituserdetailTabPanli').removeClass('active');
}
var fnActivatePanById=function(tabId,tabPanelId){
	$(tabId).addClass('active');
	$(tabPanelId).addClass('active');
	$(tabId).trigger('click');
}

var fnActivateNextPan=function(currentTabId) {
	if(currentTabId=="edituserdetailTabPanli"){
		fnActivatePanById("#editpersonaldetail-tab","#editpersonaldetailTabPanli");
	} else if(currentTabId=="editpersonaldetailTabPanli"){
		fnActivatePanById("#editqualificationdetail-tab","#editqualificationdetailTabPanli");
	}else if(currentTabId=="editqualificationdetailTabPanli"){
		fnActivatePanById("#editcenterdetail-tab","#editcenterdetailTabPanli");
	}else if(currentTabId=="editcenterdetailTabPanli"){
		fnActivatePanById("#edituserdetail-tab","#edituserdetailTabPanli");
	}else{
		fnActivatePanById("#edituserdetail-tab","#edituserdetailTabPanli");
	}
}

var saveEditUserData = function() {
	setMultiSelectForEditUserRole(editUserMdlElements.roles);
	/*selectedSchoolsOfuser = [];
	var userSchoolTableRows = $("#editUserSchoolTable").dataTable().fnGetNodes();
	$.each(userSchoolTableRows, function(i, row) {
		var sAId = $(row).prop('id'); //		
		selectedSchoolsOfuser.push(parseInt(sAId));
	});*/
	selectedCentersOfuser = [];
	var userSchoolTableRows = $("#editUserCenterTable").dataTable().fnGetNodes();
	$.each(userSchoolTableRows, function(i, row) {
		var sAId = $(row).prop('id'); //		
		selectedCentersOfuser.push(parseInt(sAId));
	});
	var roleIds = [];
	$(editUserMdlElements.roles).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				roleIds.push($(this).data('id'));
			}
		});
	});


	var userId = $(editUserMdlElements.userId).val();
	var userName = $(editUserMdlElements.userName).val();
	var userLogin = $(editUserMdlElements.userLogin).val();
	var email = $(editUserMdlElements.email).val();
	var phone = $(editUserMdlElements.phone).val();
	var emergencyContact = $(editUserMdlElements.emergencyContact).val();
	var localAddress = $(editUserMdlElements.localAddress).val();
	var permanentAddress = $(editUserMdlElements.permanentAddress).val();
	var workAddress = $(editUserMdlElements.workAddress).val();
	var designation = $(editUserMdlElements.designation).val();
	var highestQualification = $(editUserMdlElements.highestQualification).val();
	var professionalQualification = $(editUserMdlElements.professionalQualification).val();
	var experience = $(editUserMdlElements.experience).val();
	var trainedTeacherType = $(editUserMdlElements.trainedTeacherType).val();
	var isUserInternal = $(editUserMdlElements.isInternal).val();
	var hqYearOfPassing = $(editUserMdlElements.hqYearOfPassing).val();
	var reportingToDesignaion = $(editUserMdlElements.reportingToDesignaion).find("option:selected").val();
	var pqYearOfPassing = $(editUserMdlElements.pqYearOfPassing).val();
	var bloodGroup = $(editUserMdlElements.bloodGroup).val();
	var reportingToName = $(editUserMdlElements.reportingToName).find("option:selected").val();
	var gender = $(editUserMdlElements.gender).val();
	var sapCode = $(editUserMdlElements.sapCode).val();


	var userData = {
			"id": userId,
			//"schoolIdList": selectedSchoolsOfuser,
			"centerIds":selectedCentersOfuser,
			"roleIds": roleIds,
			"userName": userName,
			"email": email,
			"phone": phone,
			"userLogin": userLogin,
			"emergencyContact": emergencyContact,
			"localAddress": localAddress,
			"permanentAddress": permanentAddress,
			"workAddress": workAddress,
			"designation": designation,
			"highestQualification": highestQualification,
			"professionalQualification": professionalQualification,
			"experience": experience,
			"isTrained": 1,
			"trainedTeacherType": trainedTeacherType,
			"isInternal": isUserInternal,
			"sapCode": sapCode,
			"hqYearOfPassing": hqYearOfPassing,
			"reportingToDesignaion": (reportingToDesignaion != undefined ? reportingToDesignaion : ""),
			"pqYearOfPassing": pqYearOfPassing,
			"bloodGroup": bloodGroup,
			"reportingToName": (reportingToName != undefined ? reportingToName : ""),
			"gender": gender
	};



	var responseMessage = customAjaxCalling("user", userData, "PUT");
	if (responseMessage.response == "shiksha-200") {

		if (responseMessage.isTeacher && responseMessage.isSchoolMapped ) {
			$("#editUserSchoolConMappingfirmationModal").modal();
		} else {

			$(".outer-loader").hide();
			var currentTabId=   $('#editUserModal .nav-tabs >.active').attr('id');
			setTimeout(function() {   //calls click event after a one sec
				AJS.flag({
					type: 'success',
					title: appMessgaes.success,
					body: '<p>'+responseMessage.responseMessage+'</p>',
					close :'auto'
				})
			}, 1000);

			fnRemoveActivePan();
			fnActivateNextPan(currentTabId);

		}
	} else {
		$(".outer-loader").hide();
		 AJS.flag({
			    type: 'error',
			    title: appMessgaes.success,
			    body: '<p>'+responseMessage.responseMessage+'</p>',
			    close :'auto'
			})
	}
	$(window).scrollTop(0);
	fnDisableEditSaveBtn();
};

var editUserData = function(e) {

	clickedObjEvent = e;

	isSchoolAdded = ($('#editUserSchoolTable').dataTable().fnGetData().length == 0) ? 'no' : 'yes';

	saveEditUserData();
};
//Add school later
$('#btnEditOkDoLater').on('click', function() {
	saveEditUserData();
});


var loadUserListPage = function() {
	window.location = 'users';
};
var reloadScreenAfterEdit = function() {
	var editUserRoleId = $('#selectBox_roleType').find('option:selected').val();
	editUserRoleId = editUserRoleId != "NONE" ? editUserRoleId : 0;
	if (editUserRoleId != 0) {
		customRedirect('users', 'roleId', editUserRoleId);
	} else {
		loadUserListPage();
	}
};
$('#trainedTeacherTypeEdit').find('option').each(function(ind, obj) {
	var trainedTeacherType = $(editUserMdlElements.trainedTeacherType).val();
	if ($(obj).val() == trainedTeacherType) {
		$(obj).prop('selected', true);
	}
});
var getUsersAfterEdit = function(roleId) {
	enableTooltip();
	$('.outer-loader').show();


	var allUserData = customAjaxCalling("user/list/" + roleId, null, "GET");

	deleteAllRow("userTable");
	$.each(allUserData, function(index, obj) {
		createUserRow(obj);
	});
	
	 thLabels = ['#', columnMessages.userName,columnMessages.loginName,columnMessages.role,columnMessages.state,columnMessages.district,columnMessages.city,columnMessages.village,columnMessages.center,columnMessages.action];
		
	rowGroupingDataTable('#userTable', 10, [0, 9], 2, thLabels);

	fnMultipleSelAndToogleDTCol('.search_init', function() {
		fnHideColumns('#userTable', [2]);
	});
	showDiv($('#tblDiv'));

	$('.outer-loader').hide();

	if (roleId != null && roleId != 0) {
		$('#columnTableId').css('display', 'block');
	}
	applyPermissions(".action-edit", ".action-delete");
	disableModifyUserControlsOnRoleSequenceBasis();

	//logic to disable
	return;

};

//redirect to add user screen
var invokeAddUserPage = function() {

	var isSaveBtnDisabled=$("#addUserbtnSave").is(":disabled");
	$(".add-user-submit").prop('disabled',isSaveBtnDisabled);
	var roleId = $('#selectBox_roleType').find('option:selected').val();
	isAbleTOCreateUser = (parseInt(logedUserSequenceNumber) < 50) ? (parseInt(logedUserSequenceNumber) < 10) ? true : false : false;
	roleId = $('#selectBox_roleType').val();
	var addRoleData = customAjaxCalling("getAllFilteredRole", null, 'GET');

	fnBindUserType("#addUserType");
	$('#internalSapCodeDiv').show();
	if (isAbleTOCreateUser === true) {
		var addDesignaionList = customAjaxCalling("getAllDesignation", null, 'GET');
		fnBindRolesToListBox(addUserMdlElements.roles, addRoleData, "add");
		fnBindReportingToDesignaion("#selectBox_addReportingToDesignaion", addDesignaionList, "add");
		$("#addReportingToNameDiv").hide();
		if (roleId != null) {


			$('#addUserModal').modal().show();
		} else {
			$('#addUserModal').modal().show();

		}

	} else {
		$('#mdlError #msgText').text("Sorry. You have no previliges to create user. Please contact admin.");
		$('#mdlError').modal().show();
	}
};

//save add user data
var saveAddUserData = function() {

	selectedSchoolsOfuser = [];
	var userSchoolTableRows = $("#userSchoolTable").dataTable().fnGetNodes();


	if (userSchoolTableRows.length != 0) {
		$.each(userSchoolTableRows, function(i, row) {
			var sAId = $(row).prop('id'); //		
			selectedSchoolsOfuser.push(parseInt(sAId));
		});
	}
	var roleIds = [];
	$(addUserMdlElements.roles).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				roleIds.push($(this).data('id'));
			}
		});
	});




	var userName = $('#addUserName').val();
	var email = $('#addEmail').val();
	var password = $('#addUserPassword').val();
	var userLogin = $('#addUserLoginName').val();
	var phone = $('#addPhone').val();
	var emergencyContact = $('#addEmergencyNo').val();
	var localAddress = $('#addLocalAddress').val();
	var permanentAddress = $('#addPermanentAddress').val();
	var workAddress = $('#addWorkAddress').val();
	var designation = $('#addDesignation').val();
	var highestQualification = $('#addHighestQualification').val();
	var professionalQualification = $('#addProfessionalQualification').val();
	var experience = $('#addExperienceInYears').val();
	var trainedTeacherType = $('#trainedTeacherType').val();

	var isInternal = $('#addUserType').val();
	var hqYearOfPassing = $('#addHQyearOfPassing').val();
	var reportingToDesignaion = $('#selectBox_addReportingToDesignaion').find("option:selected").val();
	var pqYearOfPassing = $('#addPQyearOfPassing').val();
	var bloodGroup = $('#addBloodGroup').val();
	var reportingToName = $('#selectBox_addReportingToName').find("option:selected").val();
	var gender = $('#addGender').val();

	var sapCode = $('#addIuserSAPCode').val();

	addRoleId = $('#selectBox_roleType').find('option:selected').val();
	addRoleId = addRoleId != "NONE" ? addRoleId : 0;
	var userData = {
			"roleId": addRoleId,
			"schoolIdList": selectedSchoolsOfuser,
			"roleIds": roleIds,
			"userName": userName,
			"email": email,
			"password": password,
			"userLogin": userLogin,
			"phone": phone,
			"emergencyContact": emergencyContact,
			"localAddress": localAddress,
			"permanentAddress": permanentAddress,
			"workAddress": workAddress,
			"designation": designation,
			"highestQualification": highestQualification,
			"professionalQualification": professionalQualification,
			"experience": experience,
			"isTrained": 1,
			"trainedTeacherType": trainedTeacherType,
			"isInternal": isInternal,
			"sapCode": sapCode,			
			"hqYearOfPassing": hqYearOfPassing,
			"reportingToDesignaion": (reportingToDesignaion != undefined ? reportingToDesignaion : ""),
			"pqYearOfPassing": pqYearOfPassing,
			"bloodGroup": bloodGroup,
			"reportingToName": (reportingToName != undefined ? reportingToName : ""),
			"gender": gender
	}
	var responseMessage = customAjaxCalling("user", userData, "POST");
	if (responseMessage.response == "shiksha-200") {
		$("#addUserModal").modal("hide");
		// event = event || window.event //For IE
		setTimeout(function() {   //calls click event after a one sec
			AJS.flag({
				type: 'success',
				title: appMessgaes.success,
				body: '<p>'+ responseMessage.responseMessage+'</p>',
				close :'auto'
			})
		}, 1000);

		editData(responseMessage.id);

		$('#editpersonaldetailTabPanli').addClass('active');
		$('#editpersonaldetail-tab').addClass('active');
		$('#editpersonaldetail-tab').trigger('click');

		$('#editqualificationdetailTabPanli').removeClass('active');
		$('#editqualificationdetail-tab').removeClass('active');

		$('#editcenterdetailTabPanli').removeClass('active');
		$('#editcenterdetail-tab').removeClass('active');

		$('#edituserdetail-tab').removeClass('active');
		$('#edituserdetailTabPanli').removeClass('active');
		////////////////////////
		$("#editUserModal").modal();



		$("#editUserName").focus();

	} else {
		$(".outer-loader").hide();
		AJS.flag({
			type  : "error",
			title : appMessgaes.oops,
			body  : responseMessage.responseMessage,
			close : 'auto'
		})
		
		
	}
	$(window).scrollTop(0);
}

$('#btnOkDoLater').on('click', function() {
	saveAddUserData();
});


//add user section //


//page elements selectors

setOptionsForMultipleSelect("#addBloodGroup");


var addUserData = function(e) {
	clickedObjEvent = e;
	saveAddUserData();
}



//link to uploadTeacher on select teacher

$("#selectBox_roleType").change(function() {
	var id = $(this).val();
	if (id == 9) {
		$("#linkToUploadTeacher").show();
	} else {
		$("#linkToUploadTeacher").hide();
	}

	if (id != null) {
		$("#columnTableId").show();
	} else {
		$("#columnTableId").hide();
	}

});



$("#addUserType").change(function() {
	var isInternalTypeVal = $('#addUserType').find('option:selected').val();
	if ( isInternalTypeVal == "true") {
		$("#internalSapCodeDiv").show();
	} else {
		$("#internalSapCodeDiv").hide();
	}
});

$("#editUserType").change(function() {
	var isInternalTypeVal = $('#editUserType').find('option:selected').val();
	if (isInternalTypeVal == "true") {
		$("#editIuserSAPCodeDiv").show();
	} else {
		$("#editIuserSAPCodeDiv").hide();
	}
});


var setOptionsForAllMultipleSchoolSelect = function() {
	$(arguments).each(function(index, arg) {
		$(arg).multiselect({
			maxHeight: 200,
			includeSelectAllOption: true,
			dropUp: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
		});
	});
};
var fnIsUniqueLogin = function(e, loginName) {
	var loginData = customAjaxCalling("user/loginName/" + loginName + "/0", null, "GET");
	if (loginData !== true) {
		$('#addUserLoginNameDiv').addClass('has-error');
		$('#addUserLoginNameDiv').removeClass('has-success');
		e.preventDefault();
		$('#mdlErrorUniqueness #msgText').text(" ");
		$('#mdlErrorUniqueness #msgText').text("User login name already exist. (Please specify other)");
		$('#mdlErrorUniqueness').modal().show();
		return false;
	} else {
		$('#addUserLoginNameDiv').removeClass('has-error');
		$('#addUserLoginNameDiv').addClass('has-success');


		return true;
	}
};
$("#btnDownNext").on("click", function() {
	$("#btnNext").trigger("click");


});
$("#btnPrevious").on("click", function() {
	$('#btnDownNext').html('Next <span class="glyphicon glyphicon-arrow-right">');
});



var fnIsValidLoginName = function(loginName) {
	var loginData = customAjaxCalling("user/loginName/" + loginName + "/0", null, "GET");
	if (loginData !== true) {
		return false;
	} 
	return true;
};
var fnIsUniqueEmail = function(e, emailId) {
	var emailData = customAjaxCalling("user/email/" + emailId + "/0", null, "GET");
	if (emailData !== true) {
		e.preventDefault();
		$('#mdlErrorUniqueness #msgText').text(" ");
		$('#mdlErrorUniqueness #msgText').text("User email already exist. (Please specify other)");
		$('#mdlErrorUniqueness').modal().show();
		return false;
	} else {
		return true;
	}
};



var fnAppendNewRow = function(tableName, newRow) {

	var table = $("#" + tableName).DataTable();
	table.row.add($(newRow)).draw(false);
}

$('#addUserModal').on('hidden.bs.modal', function() {
	$('#addUserForm').formValidation('resetForm', true);
	resetFormById('addUserForm');
	deleteAllRow('userSchoolTable');
	fnColSpanIfDTEmpty('userSchoolTable', 8);

	$('#personaldetailTabPanli').removeClass('active');
	$('#personaldetail-tab').removeClass('active');

	$('#qualificationdetailTabPanli').removeClass('active');
	$('#qualificationdetail-tab').removeClass('active');

	$('#schooldetailTabPanli').removeClass('active');
	$('#schooldetail-tab').removeClass('active');

	$('#userdetail-tab').addClass('active');
	$('#userdetailTabPanli').addClass('active');
});
$('#editUserModal').on('hidden.bs.modal', function() {

	resetFormById('editUserForm');
	deleteAllRow('editUserCenterTable');
	fnColSpanIfDTEmpty('editUserCenterTable', 8);

	$('#editpersonaldetailTabPanli').removeClass('active');
	$('#editpersonaldetail-tab').removeClass('active');

	$('#editqualificationdetailTabPanli').removeClass('active');
	$('#editqualificationdetail-tab').removeClass('active');

	$('#editcenterdetailTabPanli').removeClass('active');
	$('#editcenterdetail-tab').removeClass('active');

	$('#edituserdetail-tab').addClass('active');
	$('#edituserdetailTabPanli').addClass('active');
	if (isSchoolDeleted || isSchoolDeleted == "true") {
		location.reload();

	}
});
var fnEnableEditSaveBtn=function(){
	$('#editUserSaveBtn').prop('disabled',false);
	$('#editUserSaveBtn').removeClass('disabled');
	$(".edit-user-submit").prop('disabled',false);
}


$('#editUserModal .nav-tabs >.active').attr('id');

$(function() {



	var event = window.event;
	initGlblVariables();
	setMultiSelectForAddUserRole(addUserMdlElements.roles);
	setMultiSelectForEditUserRole('#selectBox_editRoleType');



	setOptionsForMultipleSelectWithoutSearchUser('#trainedTeacherType',"#addUserForm",500);
	setOptionsForMultipleSelectWithoutSearchUser('#trainedTeacherTypeEdit',"#editUserForm",500);
	setOptionsForMultipleSelectWithoutSearchUser('#addUserType',"#addUserForm",500);
	setOptionsForMultipleSelectForUserRole("#selectBox_roleType");
	setOptionsForMultipleSelect($("#selectBox_addRoleType"));

	setOptionsForMultipleSelect($(userPageContext.schoolLocationType));
	setOptionsForMultipleSelectWithScrol("#genderType","#editUserForm",500);
	setOptionsForMultipleSelectWithoutSearchUser("#editUserType","#editUserForm",500);

	setOptionsForMultipleSelectWithoutSearchUser("#addGender","#addUserForm",500);
	setOptionsForMultipleSelectWithScrol("#editBloodGroup","#editUserForm",500);
	setMultiSelectForEditDesignaion("#selectBox_editReportingToDesignaion");
	setMultiSelectForAddDesignaion("#selectBox_addReportingToDesignaion");
	fnCollapseMultiselect();

	var isInValidForm = false;
	//init bootstrap tooltip
	$('[data-toggle="tooltip"]').tooltip();
	$("#addUserForm").formValidation({
		framework: 'bootstrap',
		icon: {
			validating: 'glyphicon glyphicon-refresh'
		},
		excluded: ':disabled',
		fields: {
			name: {
				validators: {
					notEmpty: {
						message: validationMsg.nameRequired
					},
					stringLength: {
						max: 250,
						message: validationMsg.nameLength
					},
					regexp: {
						regexp: /^[^'?\"/\\]*$/,
						message: validationMsg.nameConsist
					}
				}
			},
			loginName: {
				validators: {
					stringLength: {
						max: 250,
						message: validationMsg.loginNamelength
					},
					callback: {
						message: validationMsg.loginName,
						callback: function(value) {
							var regx=/^[^'?\"/\\\*\\{\}\>\<\,\=\^\!\|\%\\[\]\\(\)\~\`\:;&]*$/;
							if(value.indexOf(" ")!=-1){
								return false
							}else if (/\ {2,}/g.test(value)){
								return false;
							}else{
								return regx.test(value);
							}
						}
					},
					remote: {
						message: validationMsg.loginNameExist,
						url: 'user/isUserLoginValid',
						data: function(validator, $field, value) {
							return {
								userLogin: validator.getFieldElements('loginName').val(),
								userId: 0
							};
						},
						type: 'POST'
					}

				}
			},
			userRole: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options = $('#selectBox_addRoleType').val();
							return (options != null);
						}
					}
				}
			},
			genderType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options = $('#addGender').val();
							return (options != "");
						}
					}
				}
			},
			userType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options = $('#addUserType').val();
							return (options != "");
						}
					}
				}
			},
			trainedTeacherType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var roleDataIds = [];
							$('#selectBox_addRoleType').each(function(index, ele) {
								$(ele).find("option:selected").each(function(ind, option) {
									if ($(option).val() != "multiselect-all") {
										roleDataIds.push($(this).data('id'));
									}
								});
							});
							var isTeacherSelected=false;
							var options = $('#addUserForm #trainedTeacherType').val();
							if ($.inArray(9, roleDataIds) > -1)
							{
								isTeacherSelected=true; 
							}
							if(isTeacherSelected && options==null){
								return false
							}
							return true


						}
					}
				}
			},


			password: {
				validators: {
					callback: {
						message: validationMsg.password,
						callback: function(value) {
							var regex = new RegExp(/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[~`@!#\$\^%&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]).*$/);
							return regex.test(value);
						}
					}
				}
			},
			confirmPassword: {
				validators: {
					notEmpty: {
						message: ' '
					},
					identical: {
						field: 'password',
						message: ' '
					}
				}
			},
			email: {
				validators: {

					callback: {
						message: ' ',
						callback: function(value, validator, $field) {
							showEmailStar();
							var roleDataIds = [];
							$('#selectBox_addRoleType').each(function(index, ele) {
								$(ele).find("option:selected").each(function(ind, option) {
									if ($(option).val() != "multiselect-all") {
										roleDataIds.push($(this).data('id'));
									}
								});
							});
							if (roleDataIds.length > 1) {
								if (value == "") {
									return false;
								} else {
									return true;
								}
							} else if (roleDataIds.length == 1) {
								if (roleDataIds[0] == 9) {
									hideEmailStar();
									return true;
								} else {
									if (value == "") {
										return false;
									} else {
										return true;
									}
								}
							} else {
								return false;
							}
						}
					},

					remote: {
						message: validationMsg.emailExist,
						url: 'user/isUserEmailValid',
						data: function(validator, $field, value) {
							return {
								email: validator.getFieldElements('email').val(),
								userId: 0
							};
						},
						type: 'POST'
					},
					emailAddress: {
						message: validationMsg.emailInvalid
					},
					regexp: {
						regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message: validationMsg.emailInvalid
					}
				}
			},
			addPhone: {
				validators: {
					callback: {


						message: validationMsg.phoneNumber,
						callback: function(value) {

							if ($('#addPhone').val() == null || $('#addPhone').val() == "") {
								return true;
							}
							var regx = /^(?!0{10})[0789]\d{9}$/;
							return regx.test(value);
						}
					}
				}
			},
			addEmergencyNo: {
				validators: {
					callback: {
						message: validationMsg.emergencyNumber,
						callback: function(value) {
							if ($('#addEmergencyNo').val() == null || $('#addEmergencyNo').val() == "") {
								return true;
							}else{
								if(value.length>13){
									return false;

								}else{
									var regx=/^\d+$/;
									return regx.test(value);
								}
							}


						}
					}
				}
			},
			addHQyearOfPassing: {
				validators: {
					callback: {
						message: validationMsg.year,
						callback: function(value) {
							if ($('#addHQyearOfPassing').val() == null || $('#addHQyearOfPassing').val() == "") {
								return true;
							}
							if ($('#addHQyearOfPassing').val() < 1000) {
								return false;
							}
							var regex = new RegExp(/^\d{4}$/);
							return regex.test(value);

						}
					}
				}
			},
			addPQyearOfPassing: {
				validators: {
					callback: {
						message:  validationMsg.year,
						callback: function(value) {
							if ($('#addPQyearOfPassing').val() == null || $('#addPQyearOfPassing').val() == "") {
								return true;
							}
							if ($('#addPQyearOfPassing').val() < 1000) {
								return false;
							}
							var regex = new RegExp(/^\d{4}$/);
							return regex.test(value);

						}
					}
				}
			},
			addExperienceInYears: {
				validators: {
					callback: {
						message: validationMsg.yearsInNumber,
						callback: function(value) {
							var experienceInYears = parseFloat(value);
							if ($('#addExperienceInYears').val() == null || $('#addExperienceInYears').val() == "") {
								return true;
							} else {
								if(experienceInYears>80){
									return false
								}else{
									var regx = /^\s*(?=.*[0-9])\d*(?:\.\d{1,2})?\s*$/;

									return regx.test(value);
								}
							}
						}

					}
				}
			}
		}

	}).on('err.field.fv', function(e, data) {
		// data.fv --> The FormValidation instance

		// Get the first invalid field
		var $invalidFields = data.fv.getInvalidFields().eq(0);

		// Get the tab that contains the first invalid field
		var $tabPane = $invalidFields.parents('.tab-pane'),
		invalidTabId = $tabPane.attr('id');

		// If the tab is not active
		if (!$tabPane.hasClass('active')) {
			// Then activate it
			$tabPane.parents('.tab-content')
			.find('.tab-pane')
			.each(function(index, tab) {
				var tabId = $(tab).attr('id'),
				$li = $('a[href="#' + tabId + '"][data-toggle="tab"]').parent();

				if (tabId === invalidTabId) {
					// activate the tab pane
					$(tab).addClass('active');
					// and the associated <li> element
					$li.addClass('active');
				} else {
					$(tab).removeClass('active');
					$li.removeClass('active');
				}
			});

			// Focus on the field
			$invalidFields.focus();
			isInValidForm = true;

		}
	}).on('err.validator.fv', function(e, data) {
		data.element
		.data('fv.messages')
		.find('.help-block[data-fv-for="' + data.field + '"]').hide()
		.filter('[data-fv-validator="' + data.validator + '"]').show();
		var isSaveBtnDisabled=$("#addUserbtnSave").is(":disabled");
		$(".add-user-submit").prop('disabled',isSaveBtnDisabled);

	}).on('success.validator.fv', function() {
		var isSaveBtnDisabled=$("#addUserbtnSave").is(":disabled");
		$(".add-user-submit").prop('disabled',isSaveBtnDisabled);
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addUserData(e);
	});



	if (isEdited || isEdited == 'true') {
		getUsersAfterEdit(parseInt(selectedRoleId));
		isEdited = false;
		selectedRoleId = 0;
	}


	$("#editUserForm").formValidation({
		framework: 'bootstrap',
		icon: {
			validating: 'glyphicon glyphicon-refresh'
		},
		excluded: ':disabled',
		fields: {
			name: {
				validators: {
					stringLength: {
						max: 250,
						message: validationMsg.nameLength
					},
					notEmpty: {
						message: validationMsg.nameRequired
					},
					regexp: {
						regexp: /^[^'?\"/\\]*$/,
						message: validationMsg.nameConsist
					}
				}
			},
			userRole: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options = $('#selectBox_editRoleType').val();
							return (options != null);
						}
					}
				}
			},
			/*genderType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options = $('#editGender').val();
							return (options != null);
						}
					}
				}
			},*/
			userType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options = $('#editUserType').val();
							return (options != null);
						}
					}
				}
			}, 
			trainedTeacherType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var roleDataIds = [];
							$('#selectBox_editRoleType').each(function(index, ele) {
								$(ele).find("option:selected").each(function(ind, option) {
									if ($(option).val() != "multiselect-all") {
										roleDataIds.push($(this).data('id'));
									}
								});
							});
							var isTeacherSelected=false;
							var options = $('#trainedTeacherTypeEdit').val();
							if ($.inArray(9, roleDataIds) > -1)
							{
								isTeacherSelected=true; 
							}
							if(isTeacherSelected && options==null){
								return false
							}
							return true;

						}
					}
				}
			},
			email: {
				validators: {
					callback: {
						message: ' ',
						callback: function(value, validator, $field) {
							var roleDataIds = [];
							showEmailStar();
							$('#selectBox_editRoleType').each(function(index, ele) {
								$(ele).find("option:selected").each(function(ind, option) {
									if ($(option).val() != "multiselect-all") {
										roleDataIds.push($(this).data('id'));
									}
								});
							});
							if (roleDataIds.length > 1) {
								if (value == "") {
									return false;
								} else {
									return true;
								}
							} else if (roleDataIds.length == 1) {
								if (roleDataIds[0] == 9) {
									hideEmailStar();
									return true;
								} else {
									if (value == "") {
										return false;
									} else {
										return true;
									}
								}
							} else {
								return false;
							}
						}
					},
					remote: {
						message: validationMsg.emailExist,
						url: 'user/isUserEmailValid',
						data: function(validator, $field, value) {
							var editUserDataId = $(editUserMdlElements.userId).val();
							return {
								email: validator.getFieldElements('email').val(),
								userId: editUserDataId
							};
						},
						type: 'POST'
					},
					emailAddress: {
						message: validationMsg.emailInvalid
					},
					regexp: {
						regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message:  validationMsg.emailInvalid
					}
				}

			},
			editPhone: {
				validators: {
					callback: {

						message: validationMsg.phoneNumber,
						callback: function(value) {
							if ($('#editPhone').val() == null || $('#editPhone').val() == "") {
								return true;
							}
							var regx = /^(?!0{10})[0789]\d{9}$/;
							return regx.test(value);
						}
					}
				}
			},
			editEmergencyNo: {
				validators: {
					callback: {
						message: validationMsg.emergencyNumber,
						callback: function(value) {
							var editEmergencyNo = parseFloat(value);
							if ($('#editEmergencyNo').val() == null || $('#editEmergencyNo').val() == "") {
								return true;
							}else{
								if(editEmergencyNo>9999999999999){
									return false;

								}else{
									var regx=/^\d+$/;
									return regx.test(value);
								}
							}

						}
					}
				}
			},
			editHQyearOfPassing: {
				validators: {
					callback: {
						message: validationMsg.year,
						callback: function(value) {
							if ($('#editHQyearOfPassing').val() == null || $('#editHQyearOfPassing').val() == "") {
								return true;
							}
							if ($('#editHQyearOfPassing').val() < 1000) {
								return false;
							}
							var regex = new RegExp(/^\d{4}$/);
							return regex.test(value);

						}
					}
				}
			},
			editPQyearOfPassing: {
				validators: {
					callback: {
						message: validationMsg.year,
						callback: function(value) {
							if ($('#editPQyearOfPassing').val() == null || $('#editPQyearOfPassing').val() == "") {
								return true;
							}
							if ($('#editPQyearOfPassing').val() < 1000) {
								return false;
							}

							var regex = new RegExp(/^\d{4}$/);
							return regex.test(value);

						}
					}
				}
			},
			editExperienceInYears: {
				validators: {
					callback: {
						message: validationMsg.yearsInNumber,
						callback: function(value) {
							var experienceInYears = parseFloat(value);
							if ($('#editExperienceInYears').val() == null || $('#editExperienceInYears').val() == "") {
								return true;
							} else {
								if(experienceInYears>80){
									return false
								}else{
									var regx = /^\s*(?=.*[0-9])\d*(?:\.\d{1,2})?\s*$/;

									return regx.test(value);
								}
							}
						}

					}
				}
			}
		}

	}).on('err.field.fv', function(e, data) {
		// data.fv --> The FormValidation instance

		// Get the first invalid field
		var $invalidFields = data.fv.getInvalidFields().eq(0);

		// Get the tab that contains the first invalid field
		var $tabPane = $invalidFields.parents('.tab-pane'),
		invalidTabId = $tabPane.attr('id');

		// If the tab is not active
		if (!$tabPane.hasClass('active')) {
			// Then activate it
			$tabPane.parents('.tab-content')
			.find('.tab-pane')
			.each(function(index, tab) {
				var tabId = $(tab).attr('id'),
				$li = $('a[href="#' + tabId + '"][data-toggle="tab"]').parent();

				if (tabId === invalidTabId) {
					// activate the tab pane
					$(tab).addClass('active');
					// and the associated <li> element
					$li.addClass('active');
				} else {
					$(tab).removeClass('active');
					$li.removeClass('active');
				}
			});

			// Focus on the field
			$invalidFields.focus();
			isInValidForm = true;

		}
	})
	.on('err.validator.fv', function(e, data) {
		data.element
		.data('fv.messages')
		.find('.help-block[data-fv-for="' + data.field + '"]').hide()
		.filter('[data-fv-validator="' + data.validator + '"]').show();
		var isSaveBtnDisabled=$("#editUserSaveBtn").is(":disabled");
		$(".edit-user-submit").prop('disabled',isSaveBtnDisabled);

	}).on('success.validator.fv', function() {
		var isSaveBtnDisabled=$("#editUserSaveBtn").is(":disabled");
		$(".edit-user-submit").prop('disabled',isSaveBtnDisabled);
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		editUserData(e)
	});



	////////////////////////////////////////////////
	///////////
	$("#resetPasswordUserForm")
	.formValidation({
		feedbackIcons: {
			validating: 'glyphicon glyphicon-refresh'
		},
		fields: {
			newpassword: {
				validators: {
					callback: {
						message: validationMsg.password,
						callback: function(value) {
							// var regex = new

							var regex = new RegExp(
							/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@~`!#\$\^%&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]).*$/);
							return regex.test(value);
						}
					}
				}
			},
			confirmPassword: {
				validators: {
					notEmpty: {
						message: ' '
					},
					identical: {
						field: 'newpassword',
						message: validationMsg.confirmPassword,
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		resetUserPassword();

	});

	$('#mdlResetUserPassword').on('shown.bs.modal', function() {
		$('#resetPasswordUserForm').data('formValidation').resetForm(true);
	});
	$('#mdlResetUserPassword').on('hide.bs.modal', function() {
		$('#resetPasswordUserForm').data('formValidation').resetForm(true);
	});

	$('#addUserModal').on('hide.bs.modal', function() {
		$('#addUserForm').data('formValidation').resetForm(true);
	});

	$("#mdlEditSchoolForUser").on("hidden.bs.modal", function() {
		setTimeout(function() {
			$("body").addClass("modal-open");
		}, 150);
	});

	$(".add-user-submit").click(function(){
		$("#addUserbtnSave").trigger("click");
	})
	$(".edit-user-submit").click(function(){
		$("#editUserSaveBtn").trigger("click");
	})
	$('input:not([type="checkbox"])').change(function() {
		$(this).val($(this).val().trim());
	});
});