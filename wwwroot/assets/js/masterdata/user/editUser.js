//global variables
var editSchoolForm = "editSchoolForm";
var addAssessmentFormId = 'addAssessmentForCampaignForm';
var editSelectedLocType;
var editSelectedLocations;
var editSelectedSchooltypes;
var isEditSchoolShown;
var allInputEditEleNamesOfCityFilter = [];
var allInputEditEleNamesOfVillageFilter = [];
var allInputEditEleNamesOfCityFilterSchool = [];

// to addcampaign data
var selectedEditSchools;
var isAssessmentShown;
var lObjEdit;
var selectedIds;
var selectedEditIds;
var elementIdMap;
var resetButtonObj;
var selectedChkBox;
var noOfSchools;
var cityName;
var villageName;
var schoolIdList;
var userSchools;
var selectedSchoolsList;
var removeSchoolRowId;
var isSchoolDeleted;
var $container;
// global selectDivIds
var editSchoolMdlSelectDivs = {};
editSchoolMdlSelectDivs.city = "#mdlEditSchoolForUser #divSelectCity";
editSchoolMdlSelectDivs.block = "#mdlEditSchoolForUser #divSelectBlock";
editSchoolMdlSelectDivs.NP = "#mdlEditSchoolForUser #divSelectNP";
editSchoolMdlSelectDivs.GP = "#mdlEditSchoolForUser #divSelectGP";
editSchoolMdlSelectDivs.RV = "#mdlEditSchoolForUser #divSelectRV";
editSchoolMdlSelectDivs.village = "#mdlEditSchoolForUser #divSelectVillage";
editSchoolMdlSelectDivs.schoolTypeDiv = "#mdlEditSchoolForUser #divSchoolType";
editSchoolMdlSelectDivs.schoolDiv = "#mdlEditSchoolForUser #divSchool";
editSchoolMdlSelectDivs.schoolElement = "#mdlEditSchoolForUser #selectBox_schoolByLocAndType";
editSchoolMdlSelectDivs.schoolLocationType = "#mdlEditSchoolForUser #selectBox_schoolLocationType";
editSchoolMdlSelectDivs.schoolType = "#mdlEditSchoolForUser #selectBox_schoolType";
editSchoolMdlSelectDivs.resetLocDivCity = "#mdlEditSchoolForUser #divResetLocCity";
editSchoolMdlSelectDivs.resetLocDivVillage = "#mdlEditSchoolForUser #divResetLocVillage";

// initialize global vars
var initEditGlobalValues = function() {
	editSelectedLocType = '';
	editSelectedLocations = [];
	editSelectedSchooltypes = [];
	isAssessmentShown = false;
	isEditSchoolShown = false;
	selectedEditSchools = [];
	allInputEditEleNamesOfCityFilter = [ 'stateId', 'zoneId', 'districtId',
			'tehsilId', 'cityId' ];
	allInputEditEleNamesOfVillageFilter = [ 'stateId', 'zoneId', 'districtId',
			'tehsilId', 'blockId', 'nyayPanchayatId', 'panchayatId',
			'revenueVillageId', 'villageId' ];
	allInputEditEleNamesOfCityFilterSchool = [ 'blockId', 'nyayPanchayatId',
			'panchayatId', 'revenueVillageId', 'villageId' ];
}

// remove and reinitialize smart filter
var reinitializeSmartEditFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
};

// displaying smart-filters
var displaySmartEditFilters = function(type, lCode, lvlName) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlEditSchoolForUser #statesForZone',
			"Zone" : '#mdlEditSchoolForUser #selectBox_zonesByState',
			"District" : '#mdlEditSchoolForUser #selectBox_districtsByZone',
			"Tehsil" : '#mdlEditSchoolForUser #selectBox_tehsilsByDistrict',
			"Block" : '#mdlEditSchoolForUser #selectBox_blocksByTehsil',
			"City" : '#mdlEditSchoolForUser #selectBox_cityByBlock',
			"Nyaya Panchayat" : '#mdlEditSchoolForUser #selectBox_npByBlock',
			"Gram Panchayat" : '#mdlEditSchoolForUser #selectBox_gpByNp',
			"Revenue Village" : '#mdlEditSchoolForUser #selectBox_revenueVillagebyGp',
			"Village" : '#mdlEditSchoolForUser #selectBox_villagebyRv'
		};
		displayLocationsOfSurvey(lCode, lvlName, $('#divFilter'));
	}

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};

// arrange filter div based on location type
// if rural, hiding city and moving elements to upper
var positionEditFilterDiv = function(type) {
	if (type == "add") {
		$(editSchoolMdlSelectDivs.NP).insertAfter(
				$(editSchoolMdlSelectDivs.block));
		$(editSchoolMdlSelectDivs.RV)
				.insertAfter($(editSchoolMdlSelectDivs.GP));
	}
};
// ///////////////////////
// smart filter starts here//
// ///////////////////////
$('#mdlEditSchoolForUser input:radio[name=locType]')
		.change(
				function() {
					$('#mdlEditSchoolForUser #addLocColspFilter').removeClass('in');
					$('#mdlEditSchoolForUser #locationPnlLink').text( 'Select Location');
					$('#mdlEditSchoolForUser #tabPane').css('display', 'none');
					$('[data-toggle="collapse"]').find(".btn-collapse").find( "span").removeClass("glyphicon-minus-sign").removeClass("red");
					$('[data-toggle="collapse"]').find(".btn-collapse").find( "span").addClass("glyphicon-plus-sign").addClass( "green");
					$('#schoolType').find('input[type=checkbox]:checked') .removeAttr('checked');
					$("#" + editSchoolForm).data('formValidation') .updateStatus('schoolType', 'VALID');
					lObjEdit = this;

					selectedIds = [];
					selectedEditIds = [];// shk-369
					var typeCode = $(this).data('code');
					$('.outer-loader').show();
					// reset multiselct
					$(editSchoolMdlSelectDivs.schoolElement).multiselect(
							'destroy');
					$(editSchoolMdlSelectDivs.schoolType)
							.multiselect('destroy');
					editSelectedLocType = typeCode;
					reinitializeSmartEditFilter(elementIdMap);
					resetFormValidatonForLocFilters(editSchoolForm,
							allInputEditEleNamesOfCityFilter);
					resetFormValidatonForLocFilters(editSchoolForm,
							allInputEditEleNamesOfVillageFilter);
					if (typeCode.toLowerCase() == 'U'.toLowerCase()) {
						fnUpdateFVElementStatus(editSchoolForm,
								allInputEditEleNamesOfCityFilterSchool, 'VALID');
						showDiv($(editSchoolMdlSelectDivs.city),
								$(editSchoolMdlSelectDivs.resetLocDivCity));
						hideDiv($(editSchoolMdlSelectDivs.block),
								$(editSchoolMdlSelectDivs.NP),
								$(editSchoolMdlSelectDivs.GP),
								$(editSchoolMdlSelectDivs.RV),
								$(editSchoolMdlSelectDivs.village),
								$(editSchoolMdlSelectDivs.resetLocDivVillage));
						displaySmartEditFilters("add", $(this).data('code'), $(
								this).data('levelname'));
						fnCollapseMultiselect();
					} else {
						$("#" + editSchoolForm).data('formValidation')
								.updateStatus('cityId', 'VALID');
						hideDiv($(editSchoolMdlSelectDivs.city),
								$(editSchoolMdlSelectDivs.resetLocDivCity));
						showDiv($(editSchoolMdlSelectDivs.block),
								$(editSchoolMdlSelectDivs.NP),
								$(editSchoolMdlSelectDivs.GP),
								$(editSchoolMdlSelectDivs.RV),
								$(editSchoolMdlSelectDivs.village),
								$(editSchoolMdlSelectDivs.resetLocDivVillage));
						displaySmartEditFilters("add", $(this).data('code'), $(
								this).data('levelname'));
						fnCollapseMultiselect();
						positionEditFilterDiv("add");
					}
				});

// reset only location when click on reset button icon
var resetLocation = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;
}

// invoking on click of resetEditLocation confirmation dialog box
var doResetLocation = function() {
	selectedEditIds = [];
	selectedIds = [];
	hideDiv($("#divSchoolType"), $("#divSchool"));
	$("#mdlResetLoc").modal("hide");
	$('.outer-loader').show();
	if ($(resetButtonObj).parents("form").attr("id") == editSchoolForm) {
		resetFormValidatonForLocFilters(editSchoolForm,
				allInputEditEleNamesOfCityFilter);
		resetFormValidatonForLocFilters(editSchoolForm,
				allInputEditEleNamesOfVillageFilter);
		reinitializeSmartEditFilter(elementIdMap);
		var aLocTypeCode = $('input:radio[name=locType]:checked').data('code');
		var aLLevelName = $('input:radio[name=locType]:checked').data(
				'levelname');
		displaySmartEditFilters("add", aLocTypeCode, aLLevelName);
		if (aLocTypeCode != "" && aLocTypeCode != null) {
			if (aLocTypeCode.toLowerCase() == 'U'.toLowerCase()) {
				fnUpdateFVElementStatus(editSchoolForm,
						allInputEditEleNamesOfCityFilterSchool, 'VALID');
			} else {
				$("#" + editSchoolForm).data('formValidation').updateStatus(
						'cityId', 'VALID');
			}
		}
	}
	fnCollapseMultiselect();

};

// get all schooltype and location and bind to respective list box at add school
var getEditSchoolTypeAndLoc = function() {
	$("#editUserSchoolWizard").removeClass('wizardMaxHeight');
	$("#editSchoolForm #stepContentsDiv").removeClass("wizardContentMaxHeight");
	reinitializeSmartEditFilter(elementIdMap);
	$(editSchoolMdlSelectDivs.schoolType).multiselect('destroy');

	$('#editUserSchoolWizard #divFilter').hide();
	deleteAllRow("editSchoolWizardTable");
	$('#editUserSchoolWizard #locationStepWiz').trigger('click');
	$('#chkboxEditSelectAll').prop('checked', false);
	$('input:radio[name=locType]').each(function(i, ele) {
		$(ele).prop('checked', false)
	});
	resetFormById(editSchoolForm);
	bindSchoolTypeForCampaignToListBox($(editSchoolMdlSelectDivs.schoolType), 0);
	getAllSchoolTypeLocation();
	setOptionsForAllMultipleSelect($(editSchoolMdlSelectDivs.schoolLocationType));

	$(editSchoolMdlSelectDivs.schoolType).multiselect('refresh');
	$(editSchoolMdlSelectDivs.schoolLocationType).multiselect('refresh');
	selectedChkBox = [];
	$('#editUserSchoolWizard #btnNext').prop('disabled', false);

}
// reset form
var resetEditLocatonAndForm = function(type) {
	$('#divFilter').hide();
	if (type == "add") {
		resetFormById(editSchoolForm);
	}
	reinitializeSmartEditFilter(elementIdMap);
	getEditSchoolTypeAndLoc();
};

// ///////////////////////
// smart filter ends here//
// ///////////////////////

// after add school modal changed to wizard based
// get selected locations for schools
var fnGetSelectedEditLocations = function() {
	if (editSelectedLocType.toLowerCase() == 'U'.toLowerCase()) {
		if ($(elementIdMap.City).find('option:selected').length > 0) {
			showDiv($(editSchoolMdlSelectDivs.schoolTypeDiv));
			editSelectedLocations = [];
			$(elementIdMap.City).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all") {
						editSelectedLocations.push(parseInt($(option).val()));
					}
				});
			});
		}
	} else {
		if ($(elementIdMap.Village).find('option:selected').length > 0) {
			showDiv($(editSchoolMdlSelectDivs.schoolTypeDiv));
			editSelectedLocations = [];
			$(elementIdMap.Village).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all") {
						editSelectedLocations.push(parseInt($(option).val()));
					}
				});
			});
		}
	}// else
};
// get selected schoolType
var fnGetSelectedEditSchoolType = function() {
	editSelectedSchooltypes = [];
	$('#schoolType input:checked').each(function() {
		editSelectedSchooltypes.push($(this).data('id'));
	});
};

// get addedd schools from school table
var fnGetSelectedEditSchools = function() {
	selectedEditSchools = [];
	var abDtRows = $("#editUserSchoolTable").dataTable().fnGetNodes();
	$.each(abDtRows, function(i, row) {
		selectedEditSchools.push(parseInt($(row).prop('id')));
	});
};

// bind row to the add school table in wizard
var fnBindEditSchoolToTable = function(dSchool) {
	deleteAllRow('editSchoolWizardTable');
	selectedChkBox = [];
	$
			.each(
					dSchool,
					function(index, obj) {
						if (obj.isMember) {
							cityName = (obj.cityName == null) ? "N/A"
									: obj.cityName;
							villageName = (obj.villageName == null) ? "N/A"
									: obj.villageName;
							var row = "<tr id='"
									+ obj.id
									+ "' data-id='"
									+ obj.id
									+ "' data-school='"
									+ obj.schoolName
									+ "' data-statename='"
									+ obj.stateName
									+ "'"
									+ "data-zonename='"
									+ obj.zoneName
									+ "' data-distname='"
									+ obj.districtName
									+ "'"
									+ "data-tehsilname='"
									+ obj.tehsilName
									+ "' data-blockname='"
									+ obj.blockName
									+ "'"
									+ "data-cityname='"
									+ cityName
									+ "' data-nyaypanchayatname='"
									+ obj.nyayPanchayatName
									+ "'"
									+ "data-grampanchayatname='"
									+ obj.gramPanchayatName
									+ "'"
									+ "data-revenuevillagename='"
									+ obj.revenueVillageName
									+ "'"
									+ "data-villagename='"
									+ villageName
									+ "'>"
									+ "<td data-id='"
									+ obj.id
									+ "'><center><input class='chkbox' type='checkbox' onchange='fnChkUnchkEditUser("
									+ obj.id + ")'  id='chkboxEditSchoolWizard"
									+ obj.id + "'></input></center></td>"
									+ "<td title='" + obj.schoolName + "'>"
									+ obj.schoolName + "</td>" + "</tr>";
							fnAppendNewRow("editSchoolWizardTable", row);
						}
					});

	fnDTColFilterForEditSchoolWizard('#editSchoolWizardTable', 2, [ 0 ], false);
	fnMultipleSelAndToogleDTCol('.search_init', function() {
		fnHideColumns('#editSchoolWizardTable', []);
	});
}
// get school for campaign and bind to the table for adding to campaign
var fnBindSchoolForEditUser = function(e) {
	fnGetSelectedEditLocations();
	fnGetSelectedEditSchoolType();
	fnGetSelectedEditSchools();
	var rData = {
		"locations" : editSelectedLocations,
		"schoolTypes" : editSelectedSchooltypes,
		"schoolIds" : selectedEditSchools,
	};
	var schoolVm = customAjaxCalling("school/user/" + editSelectedLocType,
			rData, "POST");
	if (schoolVm.length != 0) {
		noOfSchools = 0;
		noOfSchools = schoolVm.length;
		fnBindEditSchoolToTable(schoolVm);
	} else {
		e.preventDefault();
		$('#mdlErrorForSchool #msgTextForSchool').text('');
		$('#mdlErrorForSchool #msgTextForSchool').text(
				"There are no schools of the above selections.");
		$('#mdlErrorForSchool').modal().show();
	}
};

// disable the schools if it has been added to table
var disableEditSchoolFromListBox = function() {

	selectedEditSchools = [];
	schoolIdList = [];
	var i = 0;
	$('#editUserSchoolTable >tbody>tr').each(function(i, row) {
		selectedEditSchools.push(parseInt(row.getAttribute('data-id')));
	});
	$(editSchoolMdlSelectDivs.schoolElement).each(function(index, ele) {
		$(ele).find("option").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				schoolIdList.push(parseInt($(option).data('id')));
			}
		});
	});
	$.grep(schoolIdList, function(el) {
		if ($.inArray(el, selectedEditSchools) != -1) {
			var option = $(editSchoolMdlSelectDivs.schoolElement).find(
					'option[value="' + el + '"]');
			option.prop('disabled', true);
			option.parent('label').parent('a').parent('li').remove();
		}
		i++;
	});
	$(editSchoolMdlSelectDivs.schoolElement).multiselect('refresh');
}

var createNewEditSchoolRow = function(selectedSchools) {
	if ($('#editUserSchoolTable >tbody>tr').length == 1) {
		$("#editUserSchoolTable tbody").find(' tr').remove();
	}
	selectedSchoolsList = [];
	$('#editUserSchoolTable >tbody>tr').each(function(i, row) {
		selectedSchoolsList.push(parseInt(row.getAttribute('data-id')));
	});
	$
			.each(
					selectedSchools,
					function(index, obj) {
						if ($.inArray(obj.id, selectedSchoolsList) > -1) {

						} else {
							var row = "<tr id='"
									+ obj.id
									+ "'data-oldSchool='false' data-id='"
									+ obj.id
									+ "'>"
									+ "<td></td>"
									+ "<td title='"
									+ obj.state
									+ "'>"
									+ obj.state
									+ "</td>"
									+
									// "<td
									// title='"+obj.zone+"'>"+obj.zone+"</td>"+
									"<td title='"
									+ obj.dist
									+ "'>"
									+ obj.dist
									+ "</td>"
									+ "<td title='"
									+ obj.city
									+ "'>"
									+ obj.city
									+ "</td>"
									+ "<td title='"
									+ obj.village
									+ "'>"
									+ obj.village
									+ "</td>"
									+ "<td title='"
									+ obj.name
									+ "'>"
									+ obj.name
									+ "</td>"
									+ "<td><div class='div-tooltip'><a class='tooltip tooltip-top' id='btnRemoveSchool'"
									+ "onclick=removeEditUserSchool(&quot;"
									+ obj.id
									+ "&quot;,&quot;"
									+ escapeHtmlCharacters(obj.name)
									+ "&quot;,&quot;"
									+ false
									+ "&quot;);><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"
									+ "</tr>";
							appendNewRow("editUserSchoolTable", row);

						}
					});
};
// add selected schools to the table
var editUserSchool = function() {

	userSchools = [];
	$(editSchoolMdlSelectDivs.schoolElement).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				selectedEditSchools = {};
				selectedEditSchools.id = $(this).data('id');
				selectedEditSchools.name = $(this).text();
				selectedEditSchools.state = $(this).data('statename');
				selectedEditSchools.zone = $(this).data('zonename');
				selectedEditSchools.dist = $(this).data('distname');
				selectedEditSchools.city = $(this).data('cityname');
				selectedEditSchools.village = $(this).data('villagename');
				userSchools.push(selectedEditSchools);
			}
		});
	});
	createNewEditSchoolRow(userSchools);
	$('#mdlEditSchoolForUser').modal('hide');
};

var oldEditSchoolData;
// remove school from table

var removeEditUserSchool = function(rowId, schoolName, isOldSchool) {
	removeSchoolRowId = rowId;
	oldEditSchoolData = isOldSchool;
	$('#mdlRemoveSchool #msgText').text('');
	$('#mdlRemoveSchool #msgText').text(
			"Do you want to remove school " + schoolName + "?");
	$('#mdlRemoveSchool').modal().show();
}
$('#mdlRemoveSchool .btnRemoveSchool').on(
		'click',
		function() {
			var userId = $(editUserMdlElements.userId).val();
			isSchoolDeleted = true;
			if (oldEditSchoolData == 'true') {
				var userSchoolMappingData = customAjaxCalling("user/" + userId
						+ "/school/" + removeSchoolRowId, null, "DELETE");
				if (userSchoolMappingData.response == "shiksha-200") {
					isSchoolDeleted = true;
					deleteRow("editUserSchoolTable", removeSchoolRowId);
					$('#mdlRemoveSchool').modal('hide');
					setTimeout(function() { // calls click event after a one sec
						AJS.flag({
							type : 'success',
							title : 'Success!',
							body : '<p>School deleted</p>',
							close : 'auto'
						})
					}, 1000);

				} else {
					$('#mdlRemoveSchool').modal('hide');
					 setTimeout(function() {   //calls click event after a one sec
						 AJS.flag({
						    type: 'error',
						    title:'Error!',
						    body: '<p>'+userSchoolMappingData.responseMessage+'</p>',
						    close :'auto'
						})
						}, 1000);
				}
			} else {
				deleteRow("editUserSchoolTable", removeSchoolRowId);
				$('#mdlRemoveSchool').modal('hide');
				setTimeout(function() { // calls click event after a one sec
					AJS.flag({
						type : 'success',
						title : 'Success!',
						body : '<p>School deleted</p>',
						close : 'auto'
					})
				}, 1000);

			}
		});

// get the selected values and push to respective array for assessment
var getEditUserSelectedData = function(elementObj, collector) {
	$(elementObj).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all") {
				collector.push(parseInt($(option).val()));
			}
		});
	});
};

var fnChkUnchkEditUser = function(id) {
	var idx = $.inArray(id, selectedChkBox);
	if (idx == -1) {
		selectedChkBox.push(id);
		selectedChkBox.length == noOfSchools ? $('#chkboxEditSelectAll').prop(
				'checked', true) : $('#chkboxEditSelectAll').prop('checked',
				false);
	} else {
		selectedChkBox.splice(idx, 1);
		$('#chkboxEditSelectAll').prop('checked', false)
	}
};

// on click of select all in table header...
var fnInvokeEditUserSelectAll = function() {
	var isChecked = $('#chkboxEditSelectAll').is(':checked');
	var rows = $("#editSchoolWizardTable").dataTable().fnGetNodes();
	if (isChecked) {

		fnDoSelectAllEditSchool(isChecked);
	} else {
		selectedChkBox = [];
		$.each(rows,
				function(i, row) {

					var id = "#chkboxEditSchoolWizard"
							+ ($(row).prop('id').toString());
					$(id).prop('checked', false);
				});
	}
}

// add selected schools to the table
var fnAddSchoolEditUser = function() {
	userSchools = [];

	var rows = $("#editSchoolWizardTable").dataTable().fnGetNodes();
	$.each(rows, function(i, row) {
		var id = $(row).data('id');
		var idxp = $.inArray(id, selectedChkBox);
		if (idxp != -1) {
			selectedEditSchools = {};
			selectedEditSchools.id = $(row).data('id');
			selectedEditSchools.name = $(row).data('school');
			selectedEditSchools.state = $(row).data('statename');
			selectedEditSchools.zone = $(row).data('zonename');
			selectedEditSchools.dist = $(row).data('distname');
			selectedEditSchools.tehsil = $(row).data('tehsilname');
			selectedEditSchools.block = $(row).data('blockname');
			selectedEditSchools.city = $(row).data('cityname');
			selectedEditSchools.nyaypanchayat = $(row)
					.data('nyaypanchayatname');
			selectedEditSchools.grampanchayat = $(row)
					.data('grampanchayatname');
			selectedEditSchools.revenuevillage = $(row).data(
					'revenuevillagename');
			selectedEditSchools.village = $(row).data('villagename');
			userSchools.push(selectedEditSchools);
		}
	});
	createNewEditSchoolRow(userSchools);
	$('#editUserSaveBtn').prop('disabled', false);
	$('#mdlEditSchoolForUser').modal('hide');
}

$(function() {
	setDataTablePagination("#editUserSchoolTable");
	initEditGlobalValues();
	fnColSpanIfDTEmpty('editUserSchoolTable', 8);

	$('#mdlEditSchoolForUser #addLocColspFilter').on(
			'show.bs.collapse',
			function() {
				$('#mdlEditSchoolForUser #locationPnlLink').text(
						'Hide Location');
			});
	$('#mdlEditSchoolForUser #addLocColspFilter').on(
			'hide.bs.collapse',
			function() {
				$('#mdlEditSchoolForUser #locationPnlLink').text(
						'Select Location');
			});
	// for add school wizard of campaign
	$("#" + editSchoolForm).formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			schoolType : {
				validators : {
					choice : {
						min : 1,
						max : 2,
						message : ' '
					}
				}
			},
			locType : {
				validators : {
					notEmpty : {
						message : ' '
					}
				}
			}
		}
	});
	$('#editUserSchoolWizard')
			.wizard()
			.on(
					'actionclicked.fu.wizard',
					function(e, data) {
						var fv = $('#editSchoolForm').data('formValidation'), // FormValidation
						// instance
						step = data.step; // Current step
						// The current step container
						$container = $('#editSchoolForm').find(
								'.step-pane[data-step="' + step + '"]');

						// Validate the container
						fv.validateContainer($container);

						var isValidStep = fv.isValidContainer($container);
						if (isValidStep === false || isValidStep === null) {
							// Do not jump to the target panel
							e.preventDefault();
						}
						if (step == 1 && isValidStep) {
							fnBindSchoolForEditUser(e);
							// for handling dynamic height #673
							$("#editUserSchoolWizard").addClass(
									'wizardMaxHeight');

						}
						if (step == 2 && isValidStep
								&& data.direction != 'previous') {
							var isOneSchoolSelected = false;

							var aaDtRows = $("#editSchoolWizardTable")
									.dataTable().fnGetNodes();
							$.each(aaDtRows,
									function(i, row) {
										if ($(row).find('.chkbox').is(
												':checked') === true) {
											isOneSchoolSelected = true;
										}
									});
							if (!isOneSchoolSelected) {
								e.preventDefault();
								$('#mdlErrorForSchool #msgTextForSchool').text(
										'');
								$('#mdlErrorForSchool #msgTextForSchool').text(
										"Please select at least one school.");
								$('#mdlErrorForSchool').modal().show();
							}
						} else if (step == 2 && isValidStep
								&& data.direction == 'previous') {
							$("#editUserSchoolWizard").removeClass(
									'wizardMaxHeight');
						}
					})
			// Triggered when clicking the Complete button
			.on(
					'finished.fu.wizard',
					function() {
						$('#editUserSchoolWizard #btnNext').prop('disabled',
								true);

						var fv = $('#editSchoolForm').data('formValidation'), step = $(
								'#editSchoolForm').wizard('selectedItem').step, $container = $(
								'#editSchoolForm').find(
								'.step-pane[data-step="' + step + '"]');

						// Validate the last step container
						fv.validateContainer($container);

						var isValidStep = fv.isValidContainer($container);
						if (isValidStep === true) {
							fnAddSchoolEditUser();
							$("#editUserSchoolWizard").removeClass(
									'wizardMaxHeight');
							fnEnableEditSaveBtn();
						}
					});
	
	
	
	$('#collapseFilter').on('hidden.bs.collapse', function () {
	    $('#collapseFilter #locationPnlLink').text(selectLocation);
	    $("#collapseFilter #collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green");
		$("#collapseFilter #collapseSpanSign").addClass("glyphicon-plus-sign").addClass("green");
    });
	
$('#collapseFilter').on('shown.bs.collapse', function () {
		$('#collapseFilter #locationPnlLink').text(hideLocation);
       $("#collapseFilter #collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green ");
       $("#collapseFilter #collapseSpanSign").addClass("glyphicon-minus-sign").addClass("red");
    	
});

});
