var shiksha = {
		form		:"#shiksha-login-form",
		errorMsg	:'#errorMessage',
		exception	:'#exception',
		userName	:'input#userName',
		password	:'input#password',
		formLink	:'#shk-form-link'
};
var lms = {
		form		:"#lms-login-form",
		errorMsg	:'#errorMessage',
		exception	:'#exception',
		userName	:'#lmsUserName',
		password	:'#lmsPassword',
		formLink	:'#lms-form-link'
};
var errorflag;
var shikshaAjax ={
		invokeAjax :function(URL, jsonData, requestType) {
				var responseData=null;
				$('#outer-loader').show();
				$.ajax({
					type : requestType,
					url : URL,
					contentType:"application/json",
					async : false,
					data: JSON.stringify(jsonData),
					success : function(data) {
						if ($(".icon-close").length > 0 && URL.toLowerCase().indexOf("notification")==-1) {
							$('.icon-close').trigger('click');
						}	
						responseData=data;
					},
					error : function(data) {
						responseData =data;
					}
				});
				return responseData;
		},
	};
var loginResponse = {};
var loginObj = {
		appType : "",
		roleType : "",
		loginContainer : "#loginContainer",
		appTypeContainer : "#appTypeContainer",
		roleTypeContainer : "#roleTypeContainer",
		roleList : [],
		init : function(){
			$(document).on("click",".apptype-action",this.appTypeChange);
			$(document).on("change",".role-radio",this.roleChange);
		},
		appTypeChange :function(){
			loginObj.appType = $(this).data("appid");
			var appName = loginObj.appType == 1 ? "Shiksha plus" : "Shiksha elementary";
			$("#appName").text(appName);
			$("[data-app='"+loginObj.appType+"']").show();
			var roles = loginObj.appType == "1" ? loginResponse.shikshaPlusRoles : loginResponse.shikshaRoles;
			if(roles.length > 1){
				$(".role-container").empty();
				$.each(roles,function(index,role){
					var roleName=role.roleName;
					$(".role-container").append('<label class="lms-input-radio-group"  id="'+role.roleCode+'"><input class="role-radio" data-roleid="'+role.roleId+'" type="radio" name="role"><span class="lms-chk-icon"></span><span>'+roleName+'</span></label>');
				});
				$(loginObj.appTypeContainer).hide();
				loginObj.showAppRole(loginObj.appType);
				
			} else if(roles.length){
				loginObj.roleType = roles[0]['roleId'];
				loginObj.doLogin();
			} else {
				AJS.flag({
					type : "error",
					title : "Error..!",
					body : "Sorry. You have no roles.Please contact administrator.",
					close :'auto'
				});
			}
		},
		showAppRole:function(apType){
			if(apType==0){
				$("[id='S']").hide();
				$("[id='IST']").hide();
				$("[id='T']").show();
				$("[id='FS']").show();
			}else if(apType==1){
				$("[id='T']").hide();
				$("[id='FS']").hide();
				$("[id='IST']").show();
				$("[id='S']").show();
			}
			$(loginObj.roleTypeContainer).show();
		},
		roleChange : function(){
			loginObj.roleType = $(this).data("roleid");
			loginObj.doLogin();
		},
		doLogin : function(){
			var data = {
					appType : loginObj.appType,
					role : loginObj.roleType
			};
			$('#outer-loader').show();
			var message = shikshaAjax.invokeAjax("app/navigate", data, "POST");
			window.location= message.redirectUrl;
		}
	
};

var loginShiksha = function() {
	$('#outer-loader').show();
	
	$(shiksha.form).find(shiksha.errorMsg).hide();
	$(shiksha.form).find(shiksha.exception).text("");

	var userLogin = $(shiksha.userName).val();
	var password = $(shiksha.password).val();
	
	if(userLogin=="" && password=="" ){
		$('#outer-loader').hide();
		$(shiksha.form).find(shiksha.errorMsg).show();
		$(shiksha.form).find(shiksha.exception).text("Username and password field should not be blank");
		return false;
	}
	if(userLogin==""){
		$('#outer-loader').hide();
		$(shiksha.form).find(shiksha.errorMsg).show();
		$(shiksha.form).find(shiksha.exception).text("Username field should not be blank");
		return false;
	}
	if(password=="" ){
		$('#outer-loader').hide();
		$(shiksha.form).find(shiksha.errorMsg).show();
		$(shiksha.form).find(shiksha.exception).text("Password field should not be blank");
		return false;
	}
	
	var json = {
			"userLogin" : userLogin,
			"password" : password,
	};
	var message = shikshaAjax.invokeAjax("login", json, "POST");
	if(message!==null && message!=='undefined' && message!==""){ //condition for handle null
		if (message.response=="shiksha-200"){
			loginResponse = message;
			$(shiksha.form).find(shiksha.errorMsg).hide();
			$(shiksha.form).find(shiksha.exception).text("");
			loginObj.roleList = message.roles ;
			console.log(message);
			/*if(message.roleCode=="LDU"){
				if(message.isDashboardPermission){
					window.location= 'app/dashboard';
				}else{
					window.location= 'unAuthorizedAccess';
				}
			} else*/ if(message.isMultiAppPermissions){
				//get appType
				/*if(message.roles.length > 1){
					$.each(message.roles,function(index,role){
						var roleName=role.roleName;
						$(".role-container").append('<label class="lms-input-radio-group"  id="'+role.roleCode+'"><div class="role-button"><input class="role-radio" data-roleid="'+role.roleId+'" type="radio" name="role"><span class="lms-chk-icon"></div><div class="role-text"></span><span>'+roleName+'</span></div></label>');
					});
				} else {
					loginObj.roleType = message.roles[0]['roleId'];
				}*/
				$(loginObj.loginContainer).hide();
				$(loginObj.appTypeContainer).show();
			} else if(message.shikshaMultiRoles || message.shikshaPlusMultiRoles){
				//get role
				loginObj.appType = message.appType;
				var appName = loginObj.appType == "1" ? "Shiksha plus" : "Shiksha elementary";
				$("#appName").text(appName);
				var roles = loginObj.appType == "1" ?  message.shikshaPlusRoles : message.shikshaRoles;
				
				if(!roles.length){
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : "Sorry. You have no roles.Please contact administrator.",
						close :'auto'
					});
					return false;
				}
				
				$(".role-container").empty();
				$.each(roles,function(index,role){
					var roleName=role.roleName;
					$(".role-container").append('<label class="lms-input-radio-group"  id="'+role.roleCode+'"><input class="role-radio" data-roleid="'+role.roleId+'" type="radio" name="role"><span class="lms-chk-icon"></span><span>'+roleName+'</span></label>');
				});
				$(loginObj.loginContainer).hide();
				$(loginObj.roleTypeContainer).show();
				$(".role-container[data-app='"+loginObj.appType+"']").show();
			}else {
				loginObj.appType = message.appType;
					var roles = loginObj.appType == "1" ? message.shikshaPlusRoles : message.shikshaRoles;
				if(roles.length){
					loginObj.roleType = roles[0]['roleId'];
					loginObj.doLogin()
				} else {
					AJS.flag({
						type : "error",
						title : "Error..!",
						body : "Sorry. You have no roles.Please contact administrator.",
						close :'auto'
					});

				}
			}
		}else if(message.response=="shiksha-400"){
			loginResponse = {};
			$('#outer-loader').hide();
			$(shiksha.password).val("");
			$(shiksha.form).find(shiksha.errorMsg).show();
			$(shiksha.form).find(shiksha.exception).text("Credentials do not match. In case you have forgotten your password, please contact the Administrator for helping with password reset");

		}
	}
	$(".outer-loader").hide();
};




var loginLms = function() {
	$(".outer-loader").show();
	
	$(lms.form).find(lms.errorMsg).hide();
	$(lms.form).find(lms.exception).text("");

	var userLogin = $(lms.userName).val();
	var password = $(lms.password).val();


	if(userLogin=="" && password=="" ){
		$(".outer-loader").hide();
		$(lms.form).find(lms.errorMsg).show();
		$(lms.form).find(lms.exception).text("Username and password field should not be blank");
		return false;
	}
	if(userLogin==""){
		$(".outer-loader").hide();
		$(lms.form).find(lms.errorMsg).show();
		$(lms.form).find(lms.exception).text("Username field should not be blank");
		return false;
	}
	if(password=="" ){
		$(".outer-loader").hide();
		$(lms.form).find(lms.errorMsg).show();
		$(lms.form).find(lms.exception).text("Password field should not be blank");
		return false;
	}
	$('.outer-loader').show();
	var json = {
			"userLogin" : userLogin,
			"password" : password
	};
	var message = shikshaAjax.invokeAjax("login", json, "POST");
	if(message!==null && message!=='undefined' && message!==""){ //condition for handle null
		if (message.response=="shiksha-200"){

			$(lms.form).find(lms.errorMsg).hide();
			$(lms.form).find(lms.exception).hide();
			if(message.roleCode=="LDU"){
				if(message.isDashboardPermission){
				window.location= 'dashboard';
				}else{
					window.location= 'unAuthorizedAccess';
				}
			}else{
				//window.location= 'home';
				
				
				
			}
		}else if(message.response=="shiksha-400"){
			$(lms.password).val("");
			$(lms.form).find(lms.errorMsg).show();
			$(lms.form).find(lms.exception).text("Credentials do not match. In case you have forgotten your password, please contact the Administrator for helping with password reset");

		}
	}
	$(".outer-loader").hide();
};



$(function() {
	$('#outer-loader').hide();
	
	 $(shiksha.formLink).click(function(e) {
			$(shiksha.form).delay(100).fadeIn(100);
			$(lms.form).find(lms.errorMsg).hide();
			$(lms.form).find(lms.exception).text("");
	 		$(lms.form).fadeOut(100);
			$(lms.formLink).removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
		$(lms.formLink).click(function(e) {
			$(lms.form).delay(100).fadeIn(100);
			$(shiksha.form).find(shiksha.errorMsg).hide();
			$(shiksha.form).find(shiksha.exception).text("");
	 		$(shiksha.form).fadeOut(100);
			$(shiksha.formLink).removeClass('active');
			$(this).addClass('active');
			e.preventDefault();
		});
	
	$(shiksha.form).formValidation({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		loginShiksha();

	});
	
	
	$(lms.form).formValidation({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		loginLms();

	});
	loginObj.init();
});