
var isSelectAllClicked;
var selectedChkBox;
//based on filter value and checked rows value, select-all checkbox will mark
var fnMarkSelectUnselectByEditFilterValue =function(){
	var filterVal =$('#editCenterWizardTable_filter').find('input').val();
	
	var isAllSelect ;
	if(filterVal.length<=0){
		isAllSelect =fnIsAllSelectEdit();

	}else if(isSelectAllClicked ===true){
		isAllSelect =fnIsAllSelectEdit();
	}else{
		isAllSelect =fnIsAllSelectEdit();
	}
	
	if(!isAllSelect){
		$('#chkboxEditCenterSelectAll').prop('checked',false);
	}else{
		$('#chkboxEditCenterSelectAll').prop('checked',true);
	}
} 

var fnDTColFilterForEditCenterWizard =function(tableId,noOfCols,excludeCol,isXScroll){	
	$(tableId).on('order.dt',enableTooltip)
				.on( 'search.dt',enableTooltip )
				.on( 'page.dt',enableTooltip)
				.DataTable({
		"pagingType": "numbers",		
		"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": true,
		 "bDestroy": true,
		 "bFilter": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,
		 'targets': 0,
         'searchable': false,
          "fnDrawCallback": function () {
        	 fnMarkSelectUnselectByEditFilterValue();
	        },
         
         
	        "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
		    "aaSorting": [],
		    bSortCellsTop:true,
	});
	
	fnApplyColumnFilter(tableId,noOfCols,[0],['','School ']);
}

//select all header functionality for campaign center table
var fnDoSelectAllEditCenter =function(status){	

	var rows = $("#editCenterWizardTable").DataTable().rows({ 'search': 'applied' }).nodes();
	
	if(status){
		selectedChkBox =[];
		$.each(rows,function(i,row){		
			
			selectedChkBox.push($(row).data('id'));
		});
		
	}else if(selectedChkBox.length==0){
		$.each(rows,function(){			
			$('.chkbox').prop('checked',false);			
		});	
	}
}
// Handle click on "Select all" control
$('#chkboxEditCenterSelectAll').on('click', function(){
	
	isSelectAllClicked =true; 
	var isChecked =$('#chkboxEditCenterSelectAll').is(':checked');
	
	var dRows = $("#editCenterWizardTable").DataTable().rows({ 'search': 'applied' }).nodes();
	 $('input[type="checkbox"]', dRows).prop('checked', this.checked);
	 
	fnDoSelectAllEditCenter(isChecked);
});








$('#editCenterWizardTable tbody').on('change', 'input[type="checkbox"]', function(){
    // If checkbox is not checked
    if(!this.checked){
       var el = $('#chkboxEditCenterSelectAll').get(0);
       // If "Select all" control is checked and has 'indeterminate' property
       if(el && el.checked && ('indeterminate' in el)){
          // Set visual state of "Select all" control 
          // as 'indeterminate'
          el.indeterminate = true;
       }
    }
 });





var fnIsAllSelectEdit =function(){
	
	var aDataRows =$("#editCenterWizardTable").DataTable().rows({ 'search': 'applied' }).nodes();
	var fooAllSelect =false ;
	$.each(aDataRows,function(i,row){		
		 if($(row).find('.chkbox').prop('checked')){
			 fooAllSelect =true;
		}else{
			fooAllSelect =false;
			return false;
		}
	});
	return fooAllSelect;
}