
var uploadStudentPageContext = new Object();
	uploadStudentPageContext.city = "#divSelectCity";
	uploadStudentPageContext.block = "#divSelectBlock";
	uploadStudentPageContext.NP = "#divSelectNP";
	uploadStudentPageContext.GP = "#divSelectGP";
	uploadStudentPageContext.RV = "#divSelectRV";
	uploadStudentPageContext.village = "#divSelectVillage";
	uploadStudentPageContext.school = "#divSelectSchool";
	
	uploadStudentPageContext.schoolLocationType = "#selectBox_schoolLocationType";
	uploadStudentPageContext.elementSchool = "#selectBox_schoolsByLoc";
	uploadStudentPageContext.grade = "#selectBox_grades";
	
	uploadStudentPageContext.resetLocDivCity = "#divResetLocCity";
	uploadStudentPageContext.resetLocDivVillage = "#divResetLocVillage";
	
	
	
	
//show or hide locations filter
//after UI chanaged......
$('input:radio[name=locType]').change(function() {		
		lObj =this;
		fnHideGrSecFile();
		$('#locColspFilter').removeClass('in');
		$('#locationPnlLink').text('View location hierarchy');
		var typeCode =$(this).data('code');
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
		
		reinitializeSmartFilter(elementIdMap);
		$("#hrDiv").show();
		if (typeCode.toLowerCase() == 'U'.toLowerCase()) {
			showDiv($(uploadStudentPageContext.city),$(uploadStudentPageContext.resetLocDivCity));
			hideDiv($(uploadStudentPageContext.block),$(uploadStudentPageContext.NP),$(uploadStudentPageContext.GP),
					$(uploadStudentPageContext.RV),$(uploadStudentPageContext.village),$(uploadStudentPageContext.resetLocDivVillage));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			fnCollapseMultiselect();
		} else {
			hideDiv($(uploadStudentPageContext.city),$(uploadStudentPageContext.resetLocDivCity));
			showDiv($(uploadStudentPageContext.block),$(uploadStudentPageContext.NP),$(uploadStudentPageContext.GP),
					$(uploadStudentPageContext.RV),$(uploadStudentPageContext.village),$(uploadStudentPageContext.resetLocDivVillage));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			fnCollapseMultiselect();
		}
		$('.outer-loader').hide();
	});



var positionFilterDiv = function(type) {
	if (type == "R") {
		$(uploadStudentPageContext.NP).insertAfter($(uploadStudentPageContext.block));
		$(uploadStudentPageContext.RV).insertAfter($(uploadStudentPageContext.GP));
		$(uploadStudentPageContext.school).insertAfter($(uploadStudentPageContext.village));
	} else {
		$(uploadStudentPageContext.school).insertAfter($(uploadStudentPageContext.city));
	}
}
// displaying smart-filters
var displaySmartFilters = function(lCode,lLevelName) {
	$(".outer-loader").show();
	elementIdMap = {
		"State" : '#statesForZone',
		"Zone" : '#selectBox_zonesByState',
		"District" : '#selectBox_districtsByZone',
		"Tehsil" : '#selectBox_tehsilsByDistrict',
		"Block" : '#selectBox_blocksByTehsil',
		"City" : '#selectBox_cityByBlock',
		"Nyaya Panchayat" : '#selectBox_npByBlock',
		"Gram Panchayat" : '#selectBox_gpByNp',
		"Revenue Village" : '#selectBox_revenueVillagebyGp',
		"Village" : '#selectBox_villagebyRv',
		"School" : '#selectBox_schoolsByLoc'
	};
	displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');
		enableMultiSelect(elementIdMap[ele_key]);
	});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};

// reset only location when click on reset button icon
var resetLocAndSchool = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;
}
// invoking on click of resetLocation confirmation dialog box
var doResetLocation = function() {
	hideDiv($("#divSelectGrade"),$("#divSelectSection"));
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('View location hierarchy');
	selectedIds=[];//it is for smart filter utility..SHK-369
	if ($(resetButtonObj).parents("form").attr("id") == "bulkUploadStudentForm") {
		reinitializeSmartFilter(elementIdMap);
		var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
		var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
		displaySmartFilters(aLocTypeCode,aLLevelName);
		fnHideGrSecFile();
	}
	fnCollapseMultiselect();
}

// remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}
///////////////////////////////////////////////////////////////////

var getGradeList = function() {
	$('.outer-loader').show();
	$('#divSelectGrade').hide();
	var aSchoolId = $("#selectBox_schoolsByLoc").val();
	var bindToEleId = $('#selectBox_grade');
	var aGrades = customAjaxCalling("gradelist/"+aSchoolId,null,"GET");
	if(aGrades.length>0){
		fnBindGradesToListBox(bindToEleId,aGrades);
		fnSetMultipleSelect('#selectBox_grade');
		$('#divSelectGrade').show();
	}else{
		fnHideGrSecFile();
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no grades for the selected school.");
		$('#mdlError').modal().show();
	}
	fnCollapseMultiselect();
	$('.outer-loader').hide();
	return;
}
var fnBindGradesToListBox =function(bEleId,gData){
	if(gData!=null || gData!=""){
		$select	=bEleId;
		$select.html('');
		$.each(gData,function(index,obj){
			$select.append('<option data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '">' +obj.gradeName+ '</option>');
		});
	}
}


var getSectionList = function(){
	$('.outer-loader').show();
	$('#divSelectSection').hide();
	$('#uploadFile').hide();
	$('#selectBox_Section').find("option:selected").prop('selected', false);
	$('#selectBox_Section').multiselect('refresh');
	$('#selectBox_Section').multiselect('destroy');
	 
	
	var aoSchoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var aoGradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	
	aBindToEleId = $('#selectBox_Section');
	var aRequestData ={
			"schoolId": aoSchoolId,
			"gradeId": aoGradeId
	}
	var aSections = customAjaxCalling("sectionlist", aRequestData,"POST");
	if(aSections.length>0){
		fnBindSectionsToListBox(aBindToEleId,aSections);
		fnSetMultipleSelect('#selectBox_Section');
		$('#divSelectSection').show();
	}else{
		$('#divSelectSection').hide();
		$('#uploadFile').hide();
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no sections for the selected school and grade.");
		$('#mdlError').modal().show();
	}
	fnCollapseMultiselect();
	$('.outer-loader').hide();
	return;
}
var fnBindSectionsToListBox =function(cEleId,secData){
	if(secData!=null || secData!=""){
		$select	=cEleId;
		$select.html('');
		$.each(secData,function(index,obj){
			$select.append('<option data-id="' + obj.sectionId + '"data-gradename="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
		});
	}
}








var gradeAndSectionHide = function(){
	$('#divSelectGrade').hide();
	$('#divSelectSection').hide();
}

var applyPermissions=function(){
	if (!editStudentPermission){
		$("#btnEditUser").remove();
	}
	if (!deleteStudentPermission){
		$("#deleteStudent").remove();
	}

}


$("#uploadBtn").click(function(){
	var filename=	$('#exampleInputFile').val();
	var file_size = $('#exampleInputFile')[0].files[0].size;
	/* Check input file extension. If different from “.xls” or “.xlsx” display error message */
	var fileExtensionName = filename.split('.').pop();
	if (fileExtensionName!="xlsx") {
		$('#errorMsg').show();
		$(".alert").fadeOut(5000);
		return false;
	}else if(file_size>3072000) {
		$('#alertIdForClientValidate,#alertIdForClientValidate .alert').show();
		return false;
	} 
	if(filename.length>0){
		$(".outer-loader").show(); 
		return true;
	}
});
$('#cancelButton').click(function () {
	$("#exampleInputFile").val('');
});

//hide grade,section,upload file div
var fnHideGrSecFile =function(){
	$('#divSelectGrade').hide();
	$('#divSelectSection').hide();
	$('#uploadFile').hide();
}


var getSelected = function() {
	var selectedanswer = document.getElementById("selectBox_Section").selectedIndex;
	if (selectedanswer != null) {
		$("#uploadFile").show();
	}
};


//@Debendra
//customize the multiple select box
var fnSetMultipleSelect = function(ele){	
	$(ele).multiselect({
		maxHeight: 100,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,	
		filterPlaceholder: 'Search here...',
		onChange : function(event) {
			if (ele == '#selectBox_Section') {
				$('#uploadFile').show();
			}
		}
	});	
}













$(function() {	
	$(window).bind("pageshow", function() {
	    var form = $('form'); 
	    form[0].reset();
	});
	fnSetMultipleSelect($('#selectBox_grade'));
	$('#rowDiv1').show();	
	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide location hierarchy');
	});  
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('View location hierarchy');
	});
	$(".uploadStudent").fadeOut(3000);
	
});
