//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForVillage =[];
var allInputEleNamesOfFilter =[];
var btnAddMoreVillageId;
var deleteVillageId;
var elementIdMap={};
var selectedIds=[];
var deleteVillageName='';
var thLabels;
var villageIdForVillageDetails=0;

//initialize global variables
var initGloabalVarsVillage =function(){
	resetButtonObj='';
	allInputEleNamesForVillage=['addVillageName'];
	allInputEleNamesOfFilter=['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId','panchayatId','revenueVillageId'];
	btnAddMoreVillageId ='';
	deleteVillageId='';
}

//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}


//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddVillage #statesForZone',
			"Zone" : '#mdlAddVillage #selectBox_zonesByState',
			"District" : '#mdlAddVillage #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddVillage #selectBox_tehsilsByDistrict',
			"Block" : '#mdlAddVillage #selectBox_blocksByTehsil',
			"Nyaya Panchayat" : '#mdlAddVillage #selectBox_npByBlock',
			"Gram Panchayat" : '#mdlAddVillage #selectBox_gpByNp',
			"Revenue Village" : '#mdlAddVillage #selectBox_revenueVillagebyGp',

		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
			"State" : '#mdlEditVillage #statesForZone',
			"Zone" : '#mdlEditVillage #selectBox_zonesByState',
			"District" : '#mdlEditVillage #selectBox_districtsByZone',
			"Tehsil" : '#mdlEditVillage #selectBox_tehsilsByDistrict',
			"Block" : '#mdlEditVillage #selectBox_blocksByTehsil',
			"Nyaya Panchayat" : '#mdlEditVillage #selectBox_npByBlock',
			"Gram Panchayat" : '#mdlEditVillage #selectBox_gpByNp',
			"Revenue Village" : '#mdlEditVillage #selectBox_revenueVillagebyGp',
		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};


// smart filter utility--- start here

var initSmartFilter = function(type) {
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){		
		resetFormValidatonForLocFilters("addVillageForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addVillageForm",allInputEleNamesForVillage);
		displaySmartFilters("add")
		fnCollapseMultiselect();
	}else
	{		
		resetFormValidatonForLoc("editVillageForm","villageId");
	resetFvForAllInputExceptLoc("editVillageForm",['editVillageName']);
	displaySmartFilters("edit")
	fnCollapseMultiselect();
	}
}


//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	if($(resetButtonObj).parents("form").attr("id")=="addVillageForm"){
		resetFormValidatonForLocFilters("addVillageForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");
	}else{
		resetFormValidatonForLocFilters("addVillageForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
	}
	fnCollapseMultiselect();

}


// reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addVillageForm');
	resetFormById('editVillageForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}


//create a row after save
var getVillageRow =function(village){
	var row = 
		"<tr id='"+village.villageId+"' data-id='"+village.villageId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+village.stateId+"'	title='"+village.stateName+"'>"+village.stateName+"</td>"+
		"<td data-zoneid='"+village.zoneId+"'	title='"+village.zoneName+"'>"+village.zoneName+"</td>"+
		"<td data-districtid='"+village.districtId+"' title='"+village.districtName+"' >"+village.districtName+"</td>"+
		"<td data-tehsilid='"+village.tehsilId+"' title='"+village.districtName+"' >"+village.districtName+"</td>"+
		
		"<td data-blockid='"+village.blockId+"' title='"+village.blockName+"' >"+village.blockName+"</td>"+
		"<td data-nyaypanchayatid='"+village.nyayPanchayatId+"' title='"+village.nyayPanchayatName+"' >"+village.nyayPanchayatName+"</td>"+
		"<td data-grampanchayatid='"+village.gramPanchayatId+"' title='"+village.gramPanchayatName+"' >"+village.gramPanchayatName+"</td>"+
		"<td data-revenuevillageid='"+village.revenueVillageId+"' title='"+village.revenueVillageName+"' >"+village.revenueVillageName+"</td>"+
		"<td data-villageid='"+village.villageId+"' title='"+village.villageName+"' >"+village.villageName+"</td>"+
		"<td data-villagecode='"+village.villageCode+"' title='"+village.villageCode+"'>"+village.villageCode+"</td>"+		

		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top'  id='btnEditVillage'" +
		"onclick=editVillageData(&quot;"+village.stateId+"&quot;,&quot;"+village.zoneId+"&quot;,&quot;"+village.districtId+"&quot;,&quot;"+village.tehsilId+"&quot;,&quot;"+village.blockId+"&quot;,&quot;"+village.nyayPanchayatId+"&quot;,&quot;"+village.gramPanchayatId+"&quot;,&quot;"+village.revenueVillageId+"&quot;,&quot;"+village.villageId+"&quot;,&quot;"+escapeHtmlCharacters(village.villageName)+"&quot;); 	><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a>" +
				"<a style='margin-left: 10px;' class='tooltip tooltip-top' onclick=\"villageDetails.initEdit(\'"+village.villageId+"\');\"><span class='tooltiptext'>"+columnMessages.viewVillageDetails+"</span><span class='glyphicon glyphicon glyphicon-eye-open font-size12'></span></a>" +
								"<a   style='margin-left:10px;' class='tooltip tooltip-top'	 id=deleteVillage	onclick=deleteVillageData(&quot;"+village.villageId+"&quot;,&quot;"+escapeHtmlCharacters(village.villageName)+"&quot;); ><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	
	return row;
}


var applyPermissions=function(){
	if (!editVillagePermission){
	$("#btnEditVillage").remove();
		}
	if (!deleteVillagePermission){
	$("#deleteVillage").remove();
		}
	}

// smart filter utility--- end here

// perform CRUD

// add Village
var addVillage = function(event) {
	$('.outer-loader').show();
	var revenueVillageId = $('#mdlAddVillage #selectBox_revenueVillagebyGp')
			.val();
	var villageName = $('#mdlAddVillage #addVillageName').val().trim().replace(/\u00a0/g," ");
	var json = {
		"revenueVillageId" : revenueVillageId,
		"villageName" : villageName,
	};
	
	
	//update data table without reloading the whole table
	//save&add new
	btnAddMoreVillageId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var village = customAjaxCalling("village", json, "POST");
	villageIdForVillageDetails=village.villageId;
	if(village!=null){
	if(village.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		var newRow =getVillageRow(village);
		appendNewRow("tblVillage",newRow);		
		applyPermissions();
		
		fnUpdateDTColFilter('#tblVillage',[2,4,5,6,7,8],12,[0,11],thLabels);
		setShowAllTagColor(colorClass);
				
		if(btnAddMoreVillageId=="addVillageSaveMoreButton"){
			$("#mdlAddVillage").find('#errorMessage').hide();			
			resetFvForAllInputExceptLoc("addVillageForm",allInputEleNamesForVillage);
			fnDisplaySaveAndAddNewElement('#mdlAddVillage',villageName);		
		}else{			
			$('#mdlAddVillage').modal("hide");
			//villageDetails.initAddModal();
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: appMessgaes.success,
				    body: '<p>'+ village.responseMessage+'</p>',
				    close :'auto'
				 })
				}, 1000);
			//villageDetails.initAddModal(villageIdForVillageDetails);
			villageDetails.initEdit(villageIdForVillageDetails);
		}		
	}else {
		$(".outer-loader").hide();
		showDiv($('#mdlAddVillage #alertdiv'));
		$("#mdlAddVillage").find('#successMessage').hide();
		$("#mdlAddVillage").find('#okButton').hide();
		$("#mdlAddVillage").find('#errorMessage').show();
		$("#mdlAddVillage").find('#exception').show();
		$("#mdlAddVillage").find('#buttonGroup').show();
		$("#mdlAddVillage").find('#exception').text(village.responseMessage);
		$('#notify').fadeIn(3000);
		}
	}
	$(".outer-loader").hide();
};



// edit
// get data for edit
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editTehsilId = 0;
var editBlockId = 0;
var editNyayPanchayatId = 0;
var editGramPanchayatId = 0;
var editRevenueVillageId = 0;
var editVillageId = 0;
var editVillageData = function() {

	var stateId = arguments[0];
	var zoneId = arguments[1];
	var districtId = arguments[2];	
	var tehsilId =arguments[3];
	var blockId=arguments[4];
	var nyayPanchayatId=arguments[5];
	var gramPanchayatId=arguments[6];
	var revenueVillageId=arguments[7];
	var villageId=arguments[8];
	var villageName = arguments[9];
	
	initSmartFilter("edit");
	var idMap = {
		'#mdlEditVillage #statesForZone' : stateId,
		'#mdlEditVillage #selectBox_zonesByState' : zoneId,
		'#mdlEditVillage #selectBox_districtsByZone' : districtId,
		'#mdlEditVillage #selectBox_tehsilsByDistrict' : tehsilId,
		'#mdlEditVillage #selectBox_blocksByTehsil' : blockId,
		'#mdlEditVillage #selectBox_npByBlock' : nyayPanchayatId,
		'#mdlEditVillage #selectBox_gpByNp' : gramPanchayatId,
		'#mdlEditVillage #selectBox_revenueVillagebyGp' : revenueVillageId,

	};

	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
							.prop('selected', true);
				enableMultiSelect(ele_key);
			});

	editVillageId = villageId;

	$('#mdlEditVillage #editVillageName').val(villageName);
	$("#mdlEditVillage").modal().show();
	fnCollapseMultiselect();

};

// //edit Village
var editVillage = function() {
	var revenueVillageId = $('.editVillage #selectBox_revenueVillagebyGp')
	.val();
	var villageName = $('#editVillageName').val().trim().replace(/\u00a0/g," ");
	var villageId = editVillageId;
	$('.outer-loader').show();
	var json = {
			"revenueVillageId" : revenueVillageId,
			"villageName" : villageName,
			"villageId" : villageId
	};

	var village = customAjaxCalling("village", json, "PUT");
	if(village!=null){
	if(village.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("tblVillage",village.villageId);
		var newRow =getVillageRow(village);
		appendNewRow("tblVillage",newRow);		
		applyPermissions();		
		fnUpdateDTColFilter('#tblVillage',[2,4,5,6,7,8],12,[0,11],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	

		$('#mdlEditVillage').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ village.responseMessage+'</p>',
			    close :'auto'
			 })
			}, 1000);	
		
	}		
	else {
		$(".outer-loader").hide();
		showDiv($('#mdlEditVillage #alertdiv'));
		$("#mdlEditVillage").find('#successMessage').hide();
		$("#mdlEditVillage").find('#errorMessage').show();
		$("#mdlEditVillage").find('#exception').show();
		$("#mdlEditVillage").find('#buttonGroup').show();
		$("#mdlEditVillage").find('#exception').text(village.responseMessage);

	}
	}
	$(".outer-loader").hide();
};
	

// get data for delete

var deleteVillageData = function(villageId, villageName) {
	deleteVillageId = villageId;
	deleteVillageName = villageName;
	$('#deleteVillageMessage').text( columnMessages.deleteConfirm .replace("@NAME" , villageName) + " ?");
	$("#mdlDelVillage").modal().show();
	hideDiv($('#mdlDelVillage #alertdiv'));

};

// delete Village

$('#deleteVillageButton').click(function() {
	$('.outer-loader').show();
	var village = customAjaxCalling("village/"+deleteVillageId, null, "DELETE");
	if(village!=null){
	if(village.response=="shiksha-200"){
		$(".outer-loader").hide();						

		deleteRow("tblVillage",deleteVillageId);				
		fnUpdateDTColFilter('#tblVillage',[2,4,5,6,7,8],12,[0,11],thLabels);
		setShowAllTagColor(colorClass);
		
		$('#mdlDelVillage').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ village.responseMessage+'</p>',
			    close :'auto'
			 })
			}, 1000);	
		
	} else {
		showDiv($('#mdlDelVillage #alertdiv'));
		$("#mdlDelVillage").find('#errorMessage').show();
		$("#mdlDelVillage").find('#exception').text(village.responseMessage);
		$("#mdlDelVillage").find('#buttonGroup').show();
	}
	}
	$(".outer-loader").hide();
	$(window).scrollTop(0);
	
});



// form validation

$(function() {
	$('#mdlResetLoc').on('hidden.bs.modal', function () {
		$("body").addClass("modal-open");
		
		});
	
	$(window).scrollTop(0);
	
	
	thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.district,columnMessages.tehsil,columnMessages.block, columnMessages.nyayPanchayat,columnMessages.gramPanchayat,columnMessages.revenueVillage,columnMessages.name,columnMessages.code,columnMessages.action];
	fnSetDTColFilterPagination('#tblVillage',12,[0,11],thLabels);		
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tblVillage',[2,4,5,6,7,8]);
	});
	fnUnbindDTSortListener();
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	
	$('#tblVillage').dataTable();
	
	
	$( "#addVillageName" ).keypress(function() {		
		hideDiv($('#mdlAddVillage #alertdiv'),$('#mdlAddVillage #alertdiv'));
	})
		$( "#editVillageForm" ).keypress(function() {		
		hideDiv($('#mdlEditVillage #alertdiv'));
	})
	initGloabalVarsVillage();
	$("#addVillageForm")
			.formValidation(
					{	excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
						
							addVillageName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.village+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addVillage(e);

			});
	$("#editVillageForm")
			.formValidation(
					{	excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							editVillageName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.village+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                                        callback : function(value) {
                                        	var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}

						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				editVillage();

			});
	
});
