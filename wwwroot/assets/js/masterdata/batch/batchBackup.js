var pageContextElements = {	
		addButton 		: '#addBatch',
		table 			: '#batchTable',
};
var addModal ={
		form 			: '#addBatchForm',
		modal   		: '#mdlAddBatch',
		saveMoreButton	: '#addBatchSaveMoreButton',
		saveButton		: '#addBatchSaveButton',
		eleName		   	: '#addBatchName',
		eleSize		   	: '#addBatchSize',
		eleCode		   	: '#addBatchCode',
		eleStartDate   	: '#addStartDate',
		eleEndDate	   	: '#addEndDate',
		eleNoDays	   	: '#addDays',
		eleNoMonths	   	: '#addMonths',
		eleDescription  : '#addDescription',
		eleWeeks		: '#addDay',
		eleStartTime	: '#addStartTime',
		eleEndTime		: '#addEndTime',
		sameTime		: '#forAllDaysCheckBox',
		checkbox		: '.checkbox',
		
};
var editModal ={
			form	 : '#editBatchForm',
			modal	 : '#mdlEditBatch',
			eleName	 : '#editBatchName',
			eleId	 : '#batchId',
			eleSize		   	: '#editBatchSize',
			eleCode		   	: '#editBatchCode',
			eleStartDate   	: '#editStartDate',
			eleEndDate	   	: '#editEndDate',
			eleNoDays	   	: '#editDays',
			eleNoMonths	   	: '#editMonths',
			eleDescription  : '#editDescription',
			eleWeeks		: '#editDay',
			eleStartTime	: '#editStartTime',
			eleEndTime		: '#editEndTime',
			
};
var deleteModal ={
			modal	: '#mdlDeleteBatch',
			confirm : '#deleteBatchButton',
};
var launchModal ={
		modal : '#mdlLaunchBatch',
		confirm : '#launchBatchButton',
};
var daysForCheckbox = ['#monCheckbox','#tueCheckbox','#wedCheckbox','#thrCheckbox','#friCheckbox','#satCheckbox','#sunCheckbox'];
var daysStartTimes = ['monStartTime','tueStartTime','wedStartTime','thrStartTime','friStartTime','satStartTime','sunStartTime'];
var daysEndTimes = ['monEndTime','tueEndTime','wedEndTime','thrEndTime','friEndTime','satEndTime','sunEndTime'];
var daysForCheckboxInEdit = ['#monCheckboxInEdit','#tueCheckboxInEdit','#wedCheckboxInEdit','#thrCheckboxInEdit','#friCheckboxInEdit','#satCheckboxInEdit','#sunCheckboxInEdit'];
var daysStartTimesInEdit = ['monStartTimeInEdit','tueStartTimeInEdit','wedStartTimeInEdit','thrStartTimeInEdit','friStartTimeInEdit','satStartTimeInEdit','sunStartTimeInEdit'];
var daysEndTimesInEdit = ['monEndTimeInEdit','tueEndTimeInEdit','wedEndTimeInEdit','thrEndTimeInEdit','friEndTimeInEdit','satEndTimeInEdit','sunEndTimeInEdit'];
var selectedDaysInEdit=[];
var selectedDays=[];
var daysTimes = {
		monStartTime : '#monStartTime',
		monEndTime : '#monEndTime',
}
var numberOfDays;
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var batchIdForDelete,batchIdForLaunch;
var batchFilterObj = {
		
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var batch={
		name:'',
		batchId:0,
		init:function(){
			setOptionsForMultipleSelect(addModal.eleWeeks);
			$(pageContextElements.addButton).on('click',batch.initAddModal);
			$(deleteModal.confirm).on('click',batch.deleteFunction);
			$(launchModal.confirm).on('click',batch.launchFunction);
			$("#forAllDaysCheckBox").on('change',batch.sameTimeForAllDays);
			$(".checkboxLabel").on('change',batch.checkboxAltered);
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
			$('#addDaysColspFilter').collapse('hide');
			$('#addDaysColspFilter').removeClass('in');	

		},
		deleteFunction : function(){
			batch.fnDelete(batchIdForDelete);
		},
		launchFunction : function(){
			batch.fnLaunch(batchIdForLaunch);
		},
		initAddModal :function(){
			batch.resetForm(addModal.form);
			fnInitSaveAndAddNewList();
			$(addModal.modal).modal("show");
			$(addModal.eleWeeks).multiselect("refresh");
			selectedDays=[];

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
			$('#addDaysColspFilter').collapse('hide');
			$('#addDaysColspFilter').removeClass('in');	
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		checkboxAltered:function(){
			
			if($(addModal.sameTime).is(":checked")){
				var startTime = $(daysTimes.monStartTime).val();
				var endTime = $(daysTimes.monEndTime).val();
				if($(this).is(":checked")){
					var id=$(this).attr('id');
					 $.each(daysStartTimes,function(ind,valu){
						 $('#'+id).parent().siblings().each(function (idx, ele){
							 $(ele).children().find('input[id="'+valu+'"]').val(startTime);
						 })
					 })
				 }
			}
		},
		sameTimeForAllDays: function(){
			if($(addModal.sameTime).is(":checked"))
			{
				var startTime = $(daysTimes.monStartTime).val();
				var endTime = $(daysTimes.monEndTime).val();
				$.each(daysForCheckbox,function(ind,value){
					 if($(value).is(":checked")){
						 $.each(daysStartTimes,function(ind,valu){
							 $(value).siblings().each(function (idx, ele){
								 $(ele).find('input[id="'+valu+'"]').val(startTime);
							 })
						 })
					 }
				 })
				 $.each(daysForCheckbox,function(ind,value){
					 if($(value).is(":checked")){
						 $.each(daysEndTimes,function(ind,valu){
							 $(value).siblings().each(function (idx, ele){
								 $(ele).find('input[id="'+valu+'"]').val(endTime);
							 })
						 })
					 }
				 })
			}
			else
			{
				$.each(daysForCheckbox,function(ind,value){
					 if($(value).is(":checked")){
						 $.each(daysStartTimes,function(ind,valu){
							 $(value).siblings().each(function (idx, ele){
								 $(ele).find('input[id="'+valu+'"]').val("");
							 })
						 })
					 }
				 })
				 $.each(daysForCheckbox,function(ind,value){
					 if($(value).is(":checked")){
						 $.each(daysEndTimes,function(ind,valu){
							 $(value).siblings().each(function (idx, ele){
								 $(ele).find('input[id="'+valu+'"]').val("");
							 })
						 })
					 }
				 })
			}
		},
		addDaysInEdit: function(daysList)
		{
			$.each(daysList,function(index,value){
				$.each(daysForCheckboxInEdit,function(ind,vale){
					 if($(vale).val() == value.day){
						 $(vale).prop("checked",true);
						 $.each(daysStartTimesInEdit,function(ind,valu){
							 $(vale).siblings().each(function (idx, ele){
								 $(ele).find('input[id="'+valu+'"]').val(value.startTime);
							 })
						 })
					 }
				 })
			})
			$.each(daysList,function(index,value){
				$.each(daysForCheckboxInEdit,function(ind,vale){
					 if($(vale).val() == value.day){
						 $.each(daysEndTimesInEdit,function(ind,valu){
							 $(vale).siblings().each(function (idx, ele){
								 $(ele).find('input[id="'+valu+'"]').val(value.endTime);
							 })
						 })
					 }
				 })
			})
			
		},

		fnAdd : function(submitBtnId){
			numberOfDays = ($(addModal.eleNoMonths).val()*30) + ($(addModal.eleNoDays).val()*1);
			spinner.showSpinner();
			$.each(daysForCheckbox,function(index,value){
				var days={};
				if($(value).is(":checked")){
					days.day=$(value).val();
					 $.each(daysStartTimes,function(ind,valu){
						 $(value).siblings().each(function (idx, ele){
							 var startTime =$(ele).find('input[id="'+valu+'"]').val();
							 if(startTime != null)
							 {
								days.startTime = startTime;
							 }
							 
						 })
					 })
					  $.each(daysEndTimes,function(ind,valu){
						 $(value).siblings().each(function (idx, ele){
							 var endTime =$(ele).find('input[id="'+valu+'"]').val();
							 if(endTime != null)
							 {
								 days.endTime = endTime;
							 }
							 
						 })
					 })
					 selectedDays.push(days);
				}
			})
			var ajaxData={
					"batchName"	:$(addModal.eleName).val(),
					"size"	:$(addModal.eleSize).val(),
					"startDate"	:$(addModal.eleStartDate).val(),
					"endDate"	:$(addModal.eleEndDate).val(),
					"duration"	:numberOfDays,
					"description":$(addModal.eleDescription).val(),
					//"batchDays" :selectedDays
			}
			var addAjaxCall = shiksha.invokeAjax("batch", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == "addBatchSaveButton"){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : messages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						})
					}
					if(submitBtnId == "addBatchSaveMoreButton"){
						$(addModal.modal).modal("show");
					}
					batch.resetForm(addModal.form);
					batch.refreshTable(pageContextElements.table);
					$(addModal.modal).find("#divSaveAndAddNewMessage").show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.batchName);

					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(batchId){
			batch.resetForm(editModal.form);
			selectedDaysInEdit=[];
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-plus-sign").removeClass("green");
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
			$('#editDaysColspFilter').collapse('show');
			$('#editDaysColspFilter').addClass('in');
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("batch/"+batchId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					var days=editAjaxCall.duration%30;
					var months=editAjaxCall.duration/30;
					$(editModal.eleName).val(editAjaxCall.batchName);
					$(editModal.eleId).val(editAjaxCall.batchId);
					$(editModal.eleSize).val(editAjaxCall.size);
					$(editModal.eleStartDate).val(editAjaxCall.startDate);
					$(editModal.eleEndDate).val(editAjaxCall.endDate);
					$(editModal.eleEndDate).datepicker('setDate', editAjaxCall.endDate);
					$(editModal.eleDescription).val(editAjaxCall.description);
					$(editModal.eleNoMonths).val(parseInt(months));
					$(editModal.eleNoDays).val(days);
					batch.addDaysInEdit(editAjaxCall.batchDays);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			numberOfDays = ($(editModal.eleNoMonths).val()*30) + ($(editModal.eleNoDays).val()*1);
			$.each(daysForCheckboxInEdit,function(index,value){
				var days={};
				if($(value).is(":checked")){
					days.day=$(value).val();
					 $.each(daysStartTimesInEdit,function(ind,valu){
						 $(value).siblings().each(function (idx, ele){
							 var startTime =$(ele).find('input[id="'+valu+'"]').val();
							 if(startTime != null)
							 {
								days.startTime = startTime;
							 }
							 
						 })
					 })
					  $.each(daysEndTimesInEdit,function(ind,valu){
						 $(value).siblings().each(function (idx, ele){
							 var endTime =$(ele).find('input[id="'+valu+'"]').val();
							 if(endTime != null)
							 {
								 days.endTime = endTime;
							 }
							 
						 })
					 })
					 selectedDaysInEdit.push(days);
				}
			})
			var ajaxData={
					"batchName":$(editModal.eleName).val(),
					"batchId":$(editModal.eleId).val(),
					"size"	:$(editModal.eleSize).val(),
					"startDate"	:$(editModal.eleStartDate).val(),
					"endDate"	:$(editModal.eleEndDate).val(),
					"duration"	:numberOfDays,
					"description":$(editModal.eleDescription).val(),
					"batchDays"  :selectedDaysInEdit
			}

			var updateAjaxCall = shiksha.invokeAjax("batch", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					batch.resetForm(editModal.form);
					batch.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: messages.sucessAlert,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(batchId,name){
			var msg = $("#deleteBatchMessage").data("message");
			$("#deleteBatchMessage").html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			batchIdForDelete=batchId;
		},
		initLaunch: function(batchId,name){
			var msg = $("#launchBatchMessage").data("message");
			$("#launchBatchMessage").html(msg.replace('@NAME@',name));
			$(launchModal.modal).modal("show");
			batchIdForLaunch=batchId;
			
		},
		fnLaunch : function(batchId){
			var launchAjaxCall = shiksha.invokeAjax("batch/"+batchId+"/launch",null,"GET");
			spinner.hideSpinner();
			if(launchAjaxCall != null){	
				if(launchAjaxCall.response == "shiksha-200"){
					$(launchModal.modal).modal("hide");
					batch.refreshTable(pageContextElements.table);
					AJS.flag({
						type  : "success",
						title : messages.sucessAlert,
						body  : launchAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : launchAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
		},
		fnDelete :function(batchId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("batch/"+batchId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					batch.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : messages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					batchName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					size: {
						required: true,
						noSpace:true,
						min:1
					},
					startDate:{
						required: true,
					},
					endDate: {
						required: true,
					},
					months:{
						required: true,
					},
					days:{
						required: true,
					},
					description:{
						required: true,
						noSpace:true,
						minlength: 1,
						maxlength: 255,
					},
				},

				messages: {
					batchName: {
						required: messages.nameRequired,
						noSpace: messages.noSpace,
						minlength:messages.minLength ,
						maxlength:messages.maxLength,
					},
					size: {
						required: messages.sizeRequired,
						nospace: messages.sizeNoSpace,
						min:messages.minNumber
					},
					startDate:{
						required: messages.startDateRequired,
					},
					endDate: {
						required: messages.endDateRequired,
					},
					months:{
						required: messages.monthsRequired,
					},
					days:{
						required: messages.daysRequired,
					},
					description:{
						required: messages.descriptionRequuired,
						noSpace:messages.descriptionNoSpace,
						minlength: messages.descriptionMinLength,
						maxlength: messages.descriptionMaxLength,
					},
				},
				errorPlacement: function(error, element) {
				    if($(element).hasClass("datepicker")){
				    	$(element).parent().parent().append(error);
				    } else {
				    	$(error).insertAfter(element);
				    }
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						batch.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						batch.fnUpdate();
					}
				},
				
			});
		},
		batchActionFormater: function(value, row, index) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="batch.initEdit(\''+row.batchId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="batch.initDelete(\''+row.batchId+'\',\''+row.batchName+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		statusActionFormater: function(value, row) {
			var label;
			if(row.status.statusName == "Yet to start")
			{
				label='<span class="aui-lozenge">'+row.status.statusName+'</span>';
			}
			else{
				label='<span class="aui-lozenge aui-lozenge-success">'+row.status.statusName+'</span>';
			}
			return label;
		},
		calculateDurationAddModal : function(startDate,endDate){
			$(addModal.eleNoMonths).val("0");
			$(addModal.eleNoDays).val("0");
			if(startDate && endDate){
				var duration = batch.calcDate(new Date(startDate), new Date(endDate));
				$(addModal.eleNoMonths).val(duration.months);
				$(addModal.eleNoDays).val(duration.days);
			}
		},
		calculateDurationEditModal : function(startDate,endDate){
			$(editModal.eleNoMonths).val("0");
			$(editModal.eleNoDays).val("0");
			if(startDate && endDate){
				var duration = batch.calcDate(new Date(startDate), new Date(endDate));
				$(editModal.eleNoMonths).val(duration.months);
				$(editModal.eleNoDays).val(duration.days);
			}
		},
		calcDate : function(date2,date1) {
		    var diff = Math.floor(date1.getTime() - date2.getTime());
		    var day = 1000 * 60 * 60 * 24;
		    var days = Math.floor(diff/day);
		    var duration = {
		    		days : days -  (Math.floor(days/30) * 30) + 1,
		    		months : Math.floor(days/30),	
		    }
		    return duration;
		    }
};

$( document ).ready(function() {

	batch.formValidate(addModal.form);
	batch.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	batch.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: "#toolbarBatch",
		url : "batch",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		formatShowingRows : batchFilterObj.formatShowingRows,
		clickToSelect: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
	var start = new Date(); 
	var end = new Date();
	$('.datepicker').datepicker({
		format: 'dd-M-yyyy',
		todayHighlight: true,
		autoclose: true,
		startDate:start
	});
	$(addModal.eleStartDate).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		$(addModal.eleEndDate).val("");
		$(addModal.eleEndDate).datepicker('setStartDate', $(this).val());
		batch.calculateDurationAddModal( $(this).val(),$(addModal.eleEndDate).val());
	}); 
	$(addModal.eleEndDate).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		//$(addModal.eleStartDate).datepicker('setEndDate', $(this).val());
		batch.calculateDurationAddModal($(addModal.eleStartDate).val(),$(this).val());
	}); 
	$(editModal.eleStartDate).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		$(editModal.eleEndDate).val("");
		$(editModal.eleEndDate).datepicker('setStartDate', $(this).val());
		batch.calculateDurationEditModal( $(this).val(),$(editModal.eleEndDate).val());
	}); 
	$(editModal.eleEndDate).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		batch.calculateDurationEditModal($(editModal.eleStartDate).val(),$(this).val());
		//$(editModal.eleStartDate).datepicker('setEndDate', $(this).val());
	});
});