//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForBlock =[];
var allInputEleNamesOfFilter =[];
var btnAddMoreBlockId;
var deleteBlockId;
var selectedIds;
var elementIdMap;
var deleteBlockName;
var thLabels;
//initialize global variables
var initGloabalVarsBlock =function(){
	resetButtonObj='';
	allInputEleNamesForBlock=['addBlockName'];
	allInputEleNamesOfFilter=['stateId','zoneId','districtId','tehsilId'];
	btnAddMoreBlockId ='';
	deleteBlockId='';
}

//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}

//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
			"State" : '#mdlAddBlock #statesForZone',
			"Zone" : '#mdlAddBlock #selectBox_zonesByState',
			"District" : '#mdlAddBlock #selectBox_districtsByZone',
			"Tehsil" : '#mdlAddBlock #selectBox_tehsilsByDistrict',
		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
			"State" : '#mdlEditBlock #statesForZone',
			"Zone" : '#mdlEditBlock #selectBox_zonesByState',
			"District" : '#mdlEditBlock #selectBox_districtsByZone',
			"Tehsil" : '#mdlEditBlock #selectBox_tehsilsByDistrict',
		};
		displayLocationsOfSurvey($('#divFilter'));
	}
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};

// smart filter utility--- start here

var initSmartFilter = function(type) {
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){
		
		resetFormValidatonForLocFilters("addBlockForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addBlockForm",allInputEleNamesForBlock);
		displaySmartFilters("add")
		fnCollapseMultiselect();
	
	}else{
		
		resetFvForAllInputExceptLoc("editBlockForm",['editBlockName']);
		displaySmartFilters("edit")
		fnCollapseMultiselect();
	}
	}



//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	if($(resetButtonObj).parents("form").attr("id")=="addBlockForm"){
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");
		
		resetFormValidatonForLocFilters("addBlockForm",allInputEleNamesOfFilter);
	}else{
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
	
		resetFormValidatonForLocFilters("addBlockForm",allInputEleNamesOfFilter);
	}

	fnCollapseMultiselect();
	
	
	
	
}


// reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addBlockForm');
	resetFormById('editBlockForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}

//create a row after save
var getBlockRow =function(block){
	var row = 
		"<tr id='"+block.blockId+"' data-id='"+block.blockId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+block.stateId+"'	title='"+block.stateName+"'>"+block.stateName+"</td>"+
		"<td data-zoneid='"+block.zoneId+"'	title='"+block.zoneName+"'>"+block.zoneName+"</td>"+
		"<td data-districtid='"+block.districtId+"' title='"+block.districtName+"' >"+block.districtName+"</td>"+
		"<td data-tehsilid='"+block.thesilId+"' title='"+block.tehsilName+"' >"+block.tehsilName+"</td>"+
		"<td data-blockid='"+block.blockId+"' title='"+block.blockName+"' >"+block.blockName+"</td>"+
		"<td data-blockcode='"+block.blockCode+"' title='"+block.blockCode+"'>"+block.blockCode+"</td>"+		

		"<td><div class='div-tooltip'><a class='tooltip tooltip-top' id='btnEditBlock'" +
		"onclick=editBlockData(&quot;"+block.stateId+"&quot;,&quot;"+block.zoneId+"&quot;,&quot;"+block.districtId+"&quot;,&quot;"+block.tehsilId+"&quot;,&quot;"+block.blockId+"&quot;,&quot;"+escapeHtmlCharacters(block.blockName)+"&quot;);><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top' id=deleteBlock	onclick=deleteBlockData(&quot;"+block.blockId+"&quot;,&quot;"+escapeHtmlCharacters(block.blockName)+"&quot;);> <span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12'  ></span></a></td>"+
		"</tr>";
	
	return row;
}


var applyPermissions=function(){
	if (!editBlockPermission){
	$("#btnEditBlock").remove();
		}
	if (!deleteBlockPermission){
	$("#deleteBlock").remove();
		}
	}

// smart filter utility--- end here

// perform CRUD

// add Block

var addBlock = function(event) {
	$('.outer-loader').show();
	var tehsilId = $('#mdlAddBlock #selectBox_tehsilsByDistrict').val();
	var blockName = $('#mdlAddBlock #addBlockName').val().trim().replace(/\u00a0/g," ");
	var json = {
		"tehsilId" : tehsilId,
		"blockName" : blockName,
	};
	
	//update data table without reloading the whole table
	//save&add new
	btnAddMoreBlockId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var block = customAjaxCalling("block", json, "POST");
	if(block!=null){
		
	if(block.response=="shiksha-200"){
		$(".outer-loader").hide();
		
		var newRow =getBlockRow(block);
		appendNewRow("tblBlock",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#tblBlock',[2,4],8,[0,7],thLabels);
		setShowAllTagColor(colorClass);
		if(btnAddMoreBlockId=="addBlockSaveMoreButton"){
			$("#mdlAddBlock").find('#errorMessage').hide();
		resetFvForAllInputExceptLoc("addBlockForm",allInputEleNamesForBlock);
			fnDisplaySaveAndAddNewElement('#mdlAddBlock',blockName);
		}else{			
			$('#mdlAddBlock').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: appMessgaes.success,
				    body: '<p>'+ block.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			
		}		
	}else {
		showDiv($('#mdlAddBlock #alertdiv'));
		$("#mdlAddBlock").find('#successMessage').hide();
		$("#mdlAddBlock").find('#okButton').hide();
		$("#mdlAddBlock").find('#errorMessage').show();
		$("#mdlAddBlock").find('#exception').show();
		$("#mdlAddBlock").find('#buttonGroup').show();
		$("#mdlAddBlock").find('#exception').text(block.responseMessage);
	}
	}
	$(".outer-loader").hide();
};


// edit
// get data for edit
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editTehsilId = 0;
var editBlockId = 0;
var editBlockData = function(stateId, zoneId, districtId, tehsilId, blockId,
		blockName) {
	initSmartFilter("edit");
	var idMap = {
		'#mdlEditBlock #statesForZone' : stateId,
		'#mdlEditBlock #selectBox_zonesByState' : zoneId,
		'#mdlEditBlock #selectBox_districtsByZone' : districtId,
		'#mdlEditBlock #selectBox_tehsilsByDistrict' : tehsilId,
	};

	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
							.prop('selected', true);
				enableMultiSelect(ele_key);
			});

	editBlockId = blockId;
	$('#mdlEditBlock #editBlockName').val(blockName);
	$("#mdlEditBlock").modal().show();
	fnCollapseMultiselect();
};

// edit Block

var editBlock = function() {
	$('.outer-loader').show();
	var tehsilId = $('.editBlock #selectBox_tehsilsByDistrict').val();
	var blockName = $('#editBlockName').val().trim().replace(/\u00a0/g," ");
	var blockId = editBlockId;
	var json = {
			"tehsilId" : tehsilId,
			"blockId" : blockId,
			"blockName" : blockName,
	};

	var block = customAjaxCalling("block", json, "PUT");
	if(block!=null){
	if(block.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("tblBlock",block.blockId);
		var newRow =getBlockRow(block);
		appendNewRow("tblBlock",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#tblBlock',[2,4],8,[0,7],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	

		$('#mdlEditBlock').modal("hide");
		
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ block.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
	}		
	else {
		showDiv($('#mdlEditBlock #alertdiv'));
		$("#mdlEditBlock").find('#successMessage').hide();
		$("#mdlEditBlock").find('#errorMessage').show();
		$("#mdlEditBlock").find('#exception').show();
		$("#mdlEditBlock").find('#buttonGroup').show();
		$("#mdlEditBlock").find('#exception').text(block.responseMessage);
	}

	}
	$(".outer-loader").hide();
};



// getting data for delete

var deleteBlockData = function(blockId, blockName) {
	deleteBlockName = blockName;
	deleteBlockId = blockId;
	$('#deleteBlockMessage').text(columnMessages.deleteConfirm.replace("@NAME",blockName) + " ?");
	$("#mdlDelBlock").modal().show();
	hideDiv($('#mdlDelBlock #alertdiv'));
};

// delete block

$('#deleteBlockButton').click(function() {
	$('.outer-loader').show();
	

	var block = customAjaxCalling("block/"+deleteBlockId, null, "DELETE");
	if(block!=null){
	if(block.response=="shiksha-200"){
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("tblBlock",deleteBlockId);
		
		fnUpdateDTColFilter('#tblBlock',[2,4],8,[0,7],thLabels);
		setShowAllTagColor(colorClass);
		$('#mdlDelBlock').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ block.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
	} else {
		showDiv($('#mdlDelBlock #alertdiv'));
		$("#mdlDelBlock").find('#errorMessage').show();
		$("#mdlDelBlock").find('#exception').text(block.responseMessage);
		$("#mdlDelBlock").find('#buttonGroup').show();
	}
	}
	$(".outer-loader").hide();
});



// form validation

$(function() {
	
	
	thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.district,columnMessages.tehsil,columnMessages.block,columnMessages.blockCode,columnMessages.action];
	fnSetDTColFilterPagination('#tblBlock',8,[0,7],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tblBlock',[2,4]);
	});$("#tableDiv").show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$( "#addBlockName" ).keypress(function() {		
		hideDiv($('#mdlAddBlock #alertdiv'));
	})
	$( "#editBlockForm" ).keypress(function() {		
		hideDiv($('#mdlEditBlock #alertdiv'));
	})
	initGloabalVarsBlock();
	colorClass = $('#t0').prop('class');
	$("#addBlockForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {

							addBlockName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.block+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),

										callback : function(value) {
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				addBlock(e);

			});
	$("#editBlockForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							
							editBlockName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: validationMsg.nameMaxLength
				                    },
									callback : {
										message :columnMessages.block+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),

										callback : function(value) {
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				editBlock();

			});
});
