var  colorClass='';
var gEvent;
var prevStageTagValue;
var thLabels=[];

var initInput =function(){	
	$("#mdlAddStage").modal().show();
	$("#addStageName").val('');
	$("#mdlAddStage #addStageForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddstage #addStageForm").data('formValidation').resetForm();		
}


/////////////////////////////////////////////////////
//create stage 


//create a row after save
var getStageRow =function(stage){
	var row = 
		"<tr id='"+stage.stageId+"' data-id='"+stage.stageId+"'>"+
		"<td></td>"+
		"<td data-stageid='"+stage.satgeId+"'	title='"+stage.stageName+"'>"+stage.stageName+"</td>"+
		"<td data-stagecode='"+stage.stageCode+"' title='"+stage.stageCode+"'>"+stage.stageCode+"</td>"+		
		"<td><div class='div-tooltip'><a class='tooltip tooltip-top btnEditStage' id='btnEditStage'" +
		"onclick='editStageData(&quot;"+stage.stageId+"&quot;,&quot;"+escapeHtmlCharacters(stage.stageName)+"&quot;,&quot;"+stage.stageName+"&quot;); '><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top btnDeleteStage' id='deleteStage"+stage.stageId+"' onclick='deleteStageData(&quot;"+stage.stageId+"&quot;,&quot;"+escapeHtmlCharacters(stage.stageName)+"&quot;,&quot;"+stage.stageName+"&quot;);' ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	return row;
}

/////////////

var applyPermissions=function(){
	if (!editStagePermission){
	$(".btnEditStage").remove();
		}
	if (!deleteStagePermission){
	$(".btnDeleteStage").remove();
		}
	}

/////////////



var addStageData = function(event) {
	var stageName = $('#addStageName').val().trim().replace(/\u00a0/g," ");
	var json = {
           "stageName" : stageName,
	};

	//update data table without reloading the whole table
	//save&add new
var	btnAddMoreStageId =$(event.target).data('formValidation').getSubmitButton().data('id');

	var stage = customAjaxCalling("stage", json, "POST");
	if(stage!=null){
		if(stage.response=="shiksha-200"){
			$(".outer-loader").hide();

			var newRow =getStageRow(stage);
			appendNewRow("stageTable",newRow);
			applyPermissions();			
			fnUpdateDTColFilter('#stageTable',[],4,[0,3],thLabels);
			setShowAllTagColor(colorClass);
			if(btnAddMoreStageId=="addStageSaveMoreButton"){
				fnDisplaySaveAndAddNewElement('#mdlAddStage',stageName);		
				initInput();
			}else{	
				$("#mdlAddStage").modal("hide");	
				setTimeout(function() {   
					 AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>'+ stage.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);
				
			}	
		}else {
			showDiv($('#mdlAddStage #alertdiv'));
			$("#mdlAddStage").find('#successMessage').hide();
			$("#mdlAddStage").find('#okButton').hide();
			$("#mdlAddStage").find('#errorMessage').show();
			$("#mdlAddStage").find('#exception').show();
			$("#mdlAddStage").find('#buttonGroup').show();
			$("#mdlAddStage").find('#exception').text(stage.responseMessage);
		}
	}
	$(".outer-loader").hide();
};




	
////////////////////////////////////////////////////////////

var editStageId = null;
var deleteStageName = null;
var deleteStageId = null;
var editStageData = function(stageId, stageName,editStgName) {
	prevStageTagValue =editStgName.replace(/\u00a0/g," ");
	$('#editStageName').val(stageName);
	editStageId = stageId;
	$("#mdlEditStage").modal().show();
	$("#mdlEditStage #editStageForm").data('formValidation').resetForm();
};

/////////////


var editData = function() {

	var stageName = $('#editStageName').val().trim().replace(/\u00a0/g," ");
	var json = {
			"stageId" : editStageId,
			"stageName" : stageName,
	};



	var stage = customAjaxCalling("stage", json, "PUT");
	if(stage!=null){
		if(stage.response=="shiksha-200"){
			$(".outer-loader").hide();
			deleteRow("stageTable",stage.stageId);

			var newRow =getStageRow(stage);
			appendNewRow("stageTable",newRow);
			applyPermissions();
			fnUpdateDTColFilter('#stageTable',[],4,[0,3],thLabels);
			setShowAllTagColor(colorClass);
			displaySuccessMsg();	

			$('#mdlEditStage').modal("hide");
			setTimeout(function() {   
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ stage.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			

		}		
		else {
			showDiv($('#mdlEditStage #alertdiv'));
			$("#mdlEditStage").find('#successMessage').hide();
			$("#mdlEditStage").find('#errorMessage').show();
			$("#mdlEditStage").find('#exception').show();
			$("#mdlEditStage").find('#buttonGroup').show();
			$("#mdlEditStage").find('#exception').text(stage.responseMessage);
		}
	}
	$(".outer-loader").hide();
};


/////////////////////////////////////////////////////
//edit stage





var deleteStageData = function(stageId, stageName,delStgSgName) {
	deleteStageId = stageId;
	deleteStageName = stageName;
	
	 $('#mdlDeleteStage #deleteStageMessage').text(
			"Do you want to delete " + stageName + " ?");
	$("#mdlDeleteStage").modal().show();
	hideDiv($('#mdlDeleteStage #alertdiv'));
};

/////////////////////////////////////////////////////
//delete 


$(document).on('click', '#deleteStageButton', function() {
	
	var stage = customAjaxCalling("stage/"+deleteStageId, null, "DELETE");
	if(stage!=null){
		if(stage.response=="shiksha-200"){
			$(".outer-loader").hide();						
			// Delete Row from data table and refresh the table without refreshing the whole table
            deleteRow("stageTable",deleteStageId);
			fnUpdateDTColFilter('#stageTable',[],4,[0,3],thLabels);
			setShowAllTagColor(colorClass);


			$('#mdlDeleteStage').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ stage.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
					
		} else {
			showDiv($('#mdlDeleteStage #alertdiv'));
			$("#mdlDeleteStage").find('#errorMessage').show();
			$("#mdlDeleteStage").find('#exception').text(stage.responseMessage);
			$("#mdlDeleteStage").find('#buttonGroup').show();
		}
	}
	$(".outer-loader").hide();
});



var fnHideStageAlertDiv =function(){
	hideDiv($('#mdlAddStage #alertdiv'));
	hideDiv($('#mdlEditstage #alertdiv'));
}    
   

$(function() {
	
	thLabels =['#','Stage  ','Stage Code  ','Action'];	
	fnSetDTColFilterPagination('#stageTable',4,[0,3],thLabels);	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#stageTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	
	fnHideStageAlertDiv();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$("#addStageForm")
			.formValidation(
					{
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							addStageName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: 'Stage name must be less than 250 characters'
				                    },
									callback : {
										message : 'Stage '+invalidInputMessage,
										callback : function(value) {
											
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))	
											return false;
											return regx.test(value);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				
				addStageData(e);

			});
	$("#editStageForm")
			.formValidation(
					{    
						excluded:':disabled',
						feedbackIcons : {
							validating : 'glyphicon glyphicon-refresh'
						},
						fields : {
							stageName : {
								validators : {
									stringLength: {
				                        max: 250,
				                        message: 'Stage name must be less than 250 characters'
				                    },
									callback : {
										message : 'Stage '+invalidInputMessage,
										callback : function(value) {
											var regx=/^[^'?\"/\\]*$/;
											if(/\ {2,}/g.test(value))
												return false;
											return regx.test(value);
										}
									}
								}
							}
						}
					}).on('success.form.fv', function(e) {
				e.preventDefault();
				
				editData();
			});
});