//global variable
var addAssmntForm ="addAssessmentForm";
var editAssmntForm ="editAssessmentForm";
var isValidData=false;
var sourceId;
var sourceVal;
var stageId;
var stageVal;
var gradeId;
var gradeVal;
var subjectId;
var subjectVal;
var unitId;
var unitVal;
var assmntName;
var unitIds;
var assessmentAutoName;
var createAssmentAPIUrl;

var sgSurveyCode;
var aAssessmentId;
var isEditAssessment;
var	$select;
var thLabels;
var sgApiUnPublishAssmentUrl;
var sgResponseDetailsRequestData=[];
var $ele;
var stageIds =[];
var sourceIds =[];
var gradeIds =[];
var subjectIds =[];
var sgTokenBody;
var spanDiv;
var aMajorVersionSelectedUnitIds;
var aMajorVersionSelectedSubject;
var isErrorShown;
var sgApiPublishAssmentUrl;
var aChkInAssessmentId;
var aChkInSgAssessmentCode;
var aChkInAssessmentVersion;
var exportedAssessmentFileName;


var createAssessmentRequestData;
var globalEvent;

//global select divs
var assmntPageContext = {};
assmntPageContext.source ="#selectBox_source";
assmntPageContext.stage ="#selectBox_stage";
assmntPageContext.grade ="#selectBox_grade";
assmntPageContext.subject ="#selectBox_subject";
assmntPageContext.unit ="#selectBox_unit";
assmntPageContext.viewButton ="#btnViewAssessment";

//addmodalelements
var addAssmntMdlElements ={};
addAssmntMdlElements.modal="#mdlAddAssessment";
addAssmntMdlElements.source ="#mdlAddAssessment #selectBox_source";
addAssmntMdlElements.stage ="#mdlAddAssessment #selectBox_stage";
addAssmntMdlElements.grade ="#mdlAddAssessment #selectBox_grade";
addAssmntMdlElements.subject ="#mdlAddAssessment #selectBox_subject";
addAssmntMdlElements.unit =" #mdlAddAssessment #selectBox_unit";
addAssmntMdlElements.assmntName ="#mdlAddAssessment #assessmentName";
addAssmntMdlElements.description="#mdlAddAssessment #descriptionId";
addAssmntMdlElements.textAreaMsg="#mdlAddAssessment #textAreaMsg";

//addmodal ele divs
var addAssmntMdlDivs ={};
addAssmntMdlDivs.source ="#mdlAddAssessment #sourceDiv";
addAssmntMdlDivs.stage ="#mdlAddAssessment #stageDiv";
addAssmntMdlDivs.grade ="#mdlAddAssessment #gradeDiv";
addAssmntMdlDivs.subject ="#mdlAddAssessment #divSubject";
addAssmntMdlDivs.unit =" #mdlAddAssessment #divUnit";	


/**
 * SHK-1353
 * */	
var majorVersion = {};
majorVersion.modal ='#mdlMajorVersionConfirmation';
majorVersion.qpList='#mdlMajorVersionConfirmation #majorVersionList';
majorVersion.buttonYes='#mdlMajorVersionConfirmation #btnMajorVersionYes';
majorVersion.buttonCancel ='#mdlMajorVersionConfirmation #cancel';
majorVersion.isMajorVersion=false;
/**
 * Check is there any question paper exists for major version of the selected combinations--SHK-1353
 * */	

function findUniqueData(array) {
	var unique = [];
	for ( var i = 0 ; i < array.length ; ++i ) {
		if ( unique.indexOf(array[i]) == -1 )
			unique.push(array[i]);
	}
	return unique;
}


var pushToArray =function(element,arr){
	$(element).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){		
				arr.push(parseInt($(option).data('id')));
			}
		});
	});
}


var fnAjax =function(URL, jsonData, requestType) {
	spinner.showSpinner();
	var msg=null;
	$.ajax({
		type : requestType,
		url : URL,
		contentType:"application/json",
		async : false,
		data: JSON.stringify(jsonData),
		success : function(data) {
			msg=data;
		},
		error : function(data) {
			msg =data;
			spinner.hideSpinner();
		}
	});		
	return msg;
};
var addAssessment =function(e){
	$(majorVersion.modal).modal('hide');
	spinner.showSpinner();

	var sourceVal = $(addAssmntMdlElements.source).find('option:selected').data('name').toString().trim();
	var stageVal = $(addAssmntMdlElements.stage).find('option:selected').data('name').toString().trim();
	var gradeVal = $(addAssmntMdlElements.grade).find('option:selected').data('name').toString().trim();
	var subjectVal = $(addAssmntMdlElements.subject).find('option:selected').data('subjectname').toString().trim();

	unitIds =[];
	var unitVal ='';
	$(addAssmntMdlElements.unit).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){				
				unitIds.push($(option).data('id').toString().trim());
				unitVal =unitVal +','+$(option).data('name').toString().trim();
			}
		});
	});

	var assessmentName = $("#assessmentName").val().toString().trim().replace(/\u00a0/g," ");
	assessmentAutoName = sourceVal+'_'+gradeVal+'_'+subjectVal+'_'+unitVal.substring(1)+'_'+stageVal;
	var createAssessmetData ={
			"assessmentName":$("#assessmentName").val().trim().replace(/\u00a0/g," "),
			"sourceId":$(addAssmntMdlElements.source).find('option:selected').data('id'),
			"gradeId":$(addAssmntMdlElements.grade).find('option:selected').data('id'),
			"stageId":$(addAssmntMdlElements.stage).find('option:selected').data('id'),
			"subjectId":$(addAssmntMdlElements.subject).find('option:selected').data('id'),
			"unitIds": unitIds,
			"gradeName": gradeVal,
			"subjectName": subjectVal,
			"stageName": stageVal,
			"sourceName": sourceVal,
			"description":($(addAssmntMdlElements.description).val())!=''?$(addAssmntMdlElements.description).val().trim().replace(/\u00a0/g," "):'',
					"assessmentAutoName":assessmentAutoName.replace(/\u00a0/g," "),
	}

	var responseData =fnAjax("assessment",createAssessmetData, "POST");

	if(responseData.response == "shiksha-200"){
		sgSurveyCode=responseData.sgAssessmentCode;
		$('#mdlAddAssessment').modal("hide");
		setTimeout(function() { 
			AJS.flag({
				type: 'success',
				title: 'Success!',
				body: '<p>Information Added</p>',
				close :'auto'
			})
		}, 1000);

		aAssessmentId =responseData.assessmentId;
		$('#mdlRedirectConfirmation').modal();
		spinner.hideSpinner();

	}else {
		$("#mdlAddAssessment").find('#successMessage').hide();			
		$("#mdlAddAssessment").find('#errorMessage').show();
		$("#mdlAddAssessment").find('#exception').show();

		$("#mdlAddAssessment").find('#exception').text(responseData.responseMessage);
		$('#mdlAddAssessment #alertdiv').css('display','block');
		spinner.hideSpinner();
	}

	spinner.hideSpinner();

	enableTooltip();
}


///////////////////// check major version /////////////////////////////////

var isMajorVersion =function(event){	
	globalEvent =event;
	unitIds =[];
	pushToArray((addAssmntMdlElements.unit),unitIds);
	var majorVersionData ={				
			"sourceId":$(addAssmntMdlElements.source).find('option:selected').data('id'),
			"gradeId":$(addAssmntMdlElements.grade).find('option:selected').data('id'),
			"stageId":$(addAssmntMdlElements.stage).find('option:selected').data('id'),
			"subjectId":$(addAssmntMdlElements.subject).find('option:selected').data('id'),
			"unitIds": unitIds,
	}
	majorVersion.isMajorVersion=false;
	var mResponse =fnAjax("assessment/majorversion/list",majorVersionData, "POST");
	if(mResponse!=null && mResponse!=""){
		if(mResponse.response == "shiksha-200" && mResponse.isQuestionPaperEmpty=="false" ){

			majorVersion.isMajorVersion=true;				

			$(majorVersion.qpList).empty();
			var assessmentNames=[];
			$.each((mResponse.assessments), function (index,assessment) {
				assessmentNames.push(assessment.assessmentName);
			});
			var assessmentNamesData=	findUniqueData(assessmentNames);
			$.each((assessmentNamesData), function (index,assessment) {					
				$(majorVersion.qpList).append('<div class="parent"></div>');
				$('.parent').last().append('<li> '+(assessment)+'</li>');
			});
			$(majorVersion.qpList).last().append('<div class="parent""><br/></div>');	
			spinner.hideSpinner();
			$(majorVersion.modal).modal().show();
			
		}else{

			majorVersion.isMajorVersion=false;
			spinner.showSpinner();
			$.wait(50).then(addAssessment);
			//addAssessment(globalEvent);
		}
	}
};

/////////////// Create Major Version //////////////////////
$(majorVersion.buttonYes).on('click',function(){
	$(majorVersion.modal).modal('hide');
	spinner.showSpinner();
	$.wait(50).then(addAssessment);
	//addAssessment(globalEvent);
});

$(majorVersion.buttonCancel).on('click',function(){

	$(majorVersion.modal).modal('hide');
	$(addAssmntMdlElements.modal).modal('hide');
});		


//initialise all gloabl variables on document ready
var initGlobalVars =function(){
	sourceId ='';
	sourceVal='';
	stageId ='';
	stageVal ='';
	gradeId ='';
	gradeVal ='';
	subjectId ='';
	subjectVal ='';
	unitId ='';
	unitVal ='';
	assmntName ='';
	createAssmentAPIUrl ='';
	createAssessmentRequestData ='';

}

//empty all select box
var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}
var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');	
}

var fnBindSubjectByGradeToListBox =function(bindToElementId,selectedGradeId){	
	var subjects =customAjaxCalling("subject/"+selectedGradeId,null,'GET');	
	if(subjects!=null && subjects.length>0){
		$select	=bindToElementId;
		$select.html('');
		$.each(subjects,function(index,obj){
			if(obj.subjectId ==aMajorVersionSelectedSubject){
				$select.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-subjectname="' +obj.subjectName+ '" selected>' +escapeHtmlCharacters(obj.subjectName)+ '</option>');	
			}else{
				$select.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-subjectname="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');	
			}	
		});
	}else{
		hideDiv($(addAssmntMdlDivs.subject));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no subjects for the selected grade.");
		$('#mdlError').modal().show();
	}
};


var setOptionsForViewAssessmentMultipleSelect =function(){
	$(arguments).each(function(index,ele){
		$(ele).multiselect('destroy');
		$(ele).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,		
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			disableIfEmpty: true,
			onChange : function(event) {
				hideResponseDiv(event);				
				if (ele == (addAssmntMdlElements.grade)) {
					if ($(addAssmntMdlElements.grade).find('option:selected').length == 0) {
						hideDiv($(addAssmntMdlDivs.subject),$(addAssmntMdlDivs.unit));
					} else {
						fnGetSubjectByGrade();						
					}
				}else if (ele == (addAssmntMdlElements.subject)) {
					if ($(addAssmntMdlElements.subject).find('option:selected').length == 0) {
						hideDiv($(addAssmntMdlDivs.unit));
					} else {
						fnGetUnitsBySub();
					}					
				}				
			}
		});		
	});
}

//bind chapters by grade and subject to respective select box
var fnBindChapter =function(element,gradeId,subjectId){
	$ele =$(element);
	var data =customAjaxCalling("unit/grade/"+gradeId+"/subject/"+subjectId,null,'GET');
	if(data.length>0){
		$.each(data,function(index,obj){
			if(obj!=null){
				var isSelected= $.inArray(obj.unitId, aMajorVersionSelectedUnitIds);
				if(isSelected !=-1){
					$ele.append('<option value="'+obj.unitId+'" data-id="' + obj.unitId + '"data-name="' +obj.unitName+ '" selected>' +escapeHtmlCharacters(obj.unitName)+ '</option>');	
				}else{
					$ele.append('<option value="'+obj.unitId+'" data-id="' + obj.unitId + '"data-name="' +obj.unitName+ '">' +escapeHtmlCharacters(obj.unitName)+ '</option>');
				}
			}
		});
	}else{
		hideDiv($(addAssmntMdlDivs.unit));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no chapters related to Question Bank found for the selected Subject and Grade.");
		$('#mdlError').modal().show();
	}
	destroyRefresh(element);
	setOptionsForViewAssessmentMultipleSelect($(element));
}
//onchange of subject, get chapters and bind to chapter list box
var fnGetUnitsBySub =function(){
	subjectId =$(addAssmntMdlElements.subject).find('option:selected').data('id');
	gradeId =$(addAssmntMdlElements.grade).find('option:selected').data('id');

	fnEmptyAll(addAssmntMdlElements.unit);
	fnBindChapter(addAssmntMdlElements.unit,gradeId,subjectId);
	showDiv($(addAssmntMdlDivs.unit));
	$("#"+addAssmntForm).data('formValidation').updateStatus('unit', 'NOT_VALIDATED');


	fnCollapseMultiselect();
}



//onchange of grade, get subject and bind to subject list box
var fnGetSubjectByGrade =function(){
	gradeId =$(addAssmntMdlElements.grade).find('option:selected').data('id');

	fnEmptyAll(addAssmntMdlElements.subject);
	fnEmptyAll(addAssmntMdlElements.unit);
	showDiv($(addAssmntMdlDivs.subject));
	hideDiv($(addAssmntMdlDivs.unit));

	fnBindSubjectByGradeToListBox($(addAssmntMdlElements.subject),gradeId);
	destroyRefresh(addAssmntMdlElements.subject);
	setOptionsForViewAssessmentMultipleSelect($(addAssmntMdlElements.subject));

	destroyRefresh(addAssmntMdlElements.unit);
	setOptionsForViewAssessmentMultipleSelect($(addAssmntMdlElements.unit));

	$("#"+addAssmntForm).data('formValidation').updateStatus('subject', 'NOT_VALIDATED');
	$("#"+addAssmntForm).data('formValidation').updateStatus('unit', 'NOT_VALIDATED');

	fnCollapseMultiselect();

}

///redirect to question bank 
var fnPickupQuestionBankRedirect=function(){

	/**
	 * if edited QP, redirect to QuestionBuilder page in sg else redirect to pick question in SG
	 *  SHK-1339,SHK-1344
	 * */

	if(isEditAssessment){
		isEditAssessment =false;
		window.location =baseContextPath+'/questionpaper/'+sgSurveyCode+'/'+aAssessmentId+'/true';
	}else{
		window.location =baseContextPath+'/questionpaper/'+sgSurveyCode+'/'+aAssessmentId+'/false';
	}
}
var fnCreateQuestionBankRedirect=function(){
	window.location =baseContextPath+'/questionbank/fromQuestionPaper';
}
//open SG in iframe
var openSgInIframe =function(pSgSurveyCode){

	spinner.showSpinner();

	var frametarget = baseContextPath+'/sg/assessment/'+pSgSurveyCode;

	$('iframe').attr("src", frametarget );
	spinner.hideSpinner();
	return false;	
}




//create row for view assessment table
var createAssessmentRow =function(assessmentData){
	var obj =assessmentData;

	var statusName;

	if(obj.status=='Yet to start' || obj.status=='Draft'){
		statusName='Draft';
	}else{
		statusName=obj.status
	}

	if(obj.status=='Yet to start' ){
		spanDiv="<span class='label label-default text-uppercase'>Draft</span>";



	}else if(obj.status=='Draft'){
		spanDiv="<span class='label label-default text-uppercase'>"+statusName+"</span>";


	}else if(obj.status=='Finalized'){
		spanDiv="<span class='label label-success text-uppercase'>"+statusName+"</span>"							


	}


	var a = '<ul>', b = '</ul>', m = [],result;
	var chapterList= obj.unitName;

	var temp = chapterList.split("#,");
	$.each(temp, function (index, value) {
		m[index] = '<li>' + value + '</li>';
	});
	result=a+m+b;
	result=result.replace(/i>,/g , "i>");
	result=result.replace(/#/g , "");

	var row;

	var dCheckInOrCheckoutCol;
	var aDownloadCol;
	if(obj.isPublished !="TRUE"){
		dCheckInOrCheckoutCol ="<a  style='margin-left:10px;' class='tooltip tooltip-top' id='btnCheckIn'"+
		"onclick=fnCheckInAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+obj.sgAssessmentCode+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.assessmentVersion+"&quot;);> <span class='tooltiptext'>Finalize</span><i class='fa fa-level-up fa-fw fa-lg'></i></a>";
		aDownloadCol	="<a style='margin-left:10px;' class='tooltip tooltip-top notactive' id='btnDownload'"+
		"onclick=fnDownloadAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.exportedFileName)+"&quot;);> <span class='tooltiptext'>Download</span><span class='glyphicon glyphicon-cloud-download font-size12' ></span></a></div>";
	}else{
		dCheckInOrCheckoutCol ="<a  style='margin-left:10px;' class='tooltip tooltip-top' id='btnCheckOut'"+
		"onclick=fnCheckOutAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+obj.sgAssessmentCode+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.assessmentVersion+"&quot;);> <span class='tooltiptext'>Create a new version</span><span class='glyphicon glyphicon-copy'></span></a>";

		aDownloadCol	="<a style='margin-left:10px;' class='tooltip tooltip-top ' id='btnDownload'"+
		"onclick=fnDownloadAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+escapeHtmlCharacters(obj.exportedFileName)+"&quot;);> <span class='tooltiptext'>Download</span><span class='glyphicon glyphicon-cloud-download font-size12' ></span></a></div>";
	}

	var aEditCol;
	var aPreviewCol;
	if(obj.isPublishedLatestVersion=="TRUE"){
		aEditCol = "<a class='tooltip tooltip-top notactive'	id='btnEditAssessment'"+
		"onclick=editAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgAssessmentCode)+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;); ><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a>";

		aPreviewCol =	"<a   style='margin-left:10px;' class='tooltip tooltip-top' id='btnPreviewAssessment'"+
		"onclick=previewAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgAssessmentCode)+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;);> <span class='tooltiptext'>Preview</span><i class='fa fa-desktop fontSize-14'></i></a>";
	}else{
		aEditCol = "<a class='tooltip tooltip-top'	id='btnEditAssessment'"+
		"onclick=editAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgAssessmentCode)+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;); ><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a>";
		aPreviewCol =	"<a style='margin-left:10px;' class='tooltip tooltip-top' id='btnPreviewAssessment'"+
		"onclick=previewAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgAssessmentCode)+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;);> <span class='tooltiptext'>Preview</span><i class='fa fa-desktop fontSize-14'></i></a>";
	}


	if(!obj.isDeleteable){
		row ="<tr id='"+obj.assessmentId+"' data-id='"+obj.assessmentId+"'  data-parent='"+obj.assessmentId+"'>"+
		"<td></td>"+

		"<td title='"+obj.gradeName+"'>"+obj.gradeName+"</td>"+
		"<td title='"+obj.subjectName+"'>"+obj.subjectName+"</td>"+
		"<td title='"+obj.unitName+"'>"+result+"</td>"+	
		"<td title='"+obj.stageName+"'>"+obj.stageName+"</td>"+
		"<td title='"+obj.sourceName+"'>"+obj.sourceName+"</td>"+
		"<td title='"+obj.assessmentName+"'>"+obj.assessmentName+"</td>"+
		"<td title='"+obj.assessmentAutoName+"'>"+obj.assessmentAutoName+"</td>"+
		"<td title='"+obj.assessmentVersion+"'>"+obj.assessmentVersion+"</td>"+
		"<td title='"+escapeHtmlCharacters(obj.description)+"'>"+escapeHtmlCharacters(obj.description)+"</td>"+
		"<td title='"+statusName+"'>"+spanDiv+"</td>"+
		"<td title='"+obj.createdBy+"'>"+obj.createdBy+"</td>"+
		"<td title='"+obj.modifiedBy+"'>"+obj.modifiedBy+"</td>"+
		"<td title='"+obj.modifiedDate+"'>"+obj.modifiedDate+"</td>"+
		"<td><div  class='div-tooltip' >"+aEditCol+			
		"<a   style='margin-left:10px;' class='tooltip tooltip-top notactive'	id='btnRemoveAssessment'"+
		"onclick=removeAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgAssessmentCode)+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.assessmentVersion+"&quot;); ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a>"+
		aPreviewCol+
		dCheckInOrCheckoutCol+
		aDownloadCol+
		"</td>"+
		"</tr>";
	}else{
		row ="<tr id='"+obj.assessmentId+"' data-id='"+obj.assessmentId+"'  data-parent='"+obj.assessmentId+"'>"+
		"<td></td>"+

		"<td title='"+obj.gradeName+"'>"+obj.gradeName+"</td>"+
		"<td title='"+obj.subjectName+"'>"+obj.subjectName+"</td>"+
		"<td title='"+obj.unitName+"'>"+result+"</td>"+	
		"<td title='"+obj.stageName+"'>"+obj.stageName+"</td>"+
		"<td title='"+obj.sourceName+"'>"+obj.sourceName+"</td>"+
		"<td title='"+obj.assessmentName+"'>"+obj.assessmentName+"</td>"+
		"<td title='"+obj.assessmentAutoName+"'>"+obj.assessmentAutoName+"</td>"+
		"<td title='"+obj.assessmentVersion+"'>"+obj.assessmentVersion+"</td>"+
		"<td title='"+escapeHtmlCharacters(obj.description)+"'>"+escapeHtmlCharacters(obj.description)+"</td>"+
		"<td title='"+statusName+"'>"+spanDiv+"</td>"+
		"<td title='"+obj.modifiedDate+"'>"+obj.modifiedDate+"</td>"+
		"<td><div  class='div-tooltip' >"+aEditCol+

		"<a   style='margin-left:10px;' class='tooltip tooltip-top'	id='btnRemoveAssessment'"+
		"onclick=removeAssessment(&quot;"+obj.assessmentId+"&quot;,&quot;"+escapeHtmlCharacters(obj.sgAssessmentCode)+"&quot;,&quot;"+escapeHtmlCharacters(obj.assessmentName)+"&quot;,&quot;"+obj.assessmentVersion+"&quot;); ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a>"+

		aPreviewCol+
		dCheckInOrCheckoutCol+
		aDownloadCol+
		"</td>"+
		"</tr>";
	}

	appendNewRow("assessmentTable",row);
	spinner.hideSpinner();
}




//view Assessment list
var viewAssessmentList =function(assessmentData){
	spinner.showSpinner();	
	stageIds =[];
	sourceIds =[];
	gradeIds =[];
	subjectIds =[];
	unitIds =[];
	pushToArray((assmntPageContext.stage),stageIds);
	pushToArray((assmntPageContext.source),sourceIds);
	pushToArray((assmntPageContext.grade),gradeIds);
	pushToArray((assmntPageContext.subject),subjectIds);
	pushToArray((assmntPageContext.unit),unitIds);

	if(assessmentData!=null && assessmentData!=""){	
		if(assessmentData.response == "shiksha-200"){
			createAssessmentRow(assessmentData);
			fnUpdateDTColFilter('#assessmentTable',[4,5,6,8,9,12,13],15,[0,13,14],thLabels);
		}
	}else{
		hideDiv($("#divAssessmentTable"));
	}
	spinner.hideSpinner();
}






//giving permissions for edit and delete
var applyPermissions=function(){
	if (!editAssessmentPermission){
		$("#btnEditAssessment").remove();
	}
	if (!deleteAssessmentPermission){
		$("#btnRemoveAssessment").remove();
	}

}

var deleteAssessmentId;
//remove assessment 
var removeAssessment =function(id,sgAssessmentCode,name,pRemoveAssessmentVersion){	
	deleteAssessmentId=id;
	var deleteAssmntName=name+"-"+pRemoveAssessmentVersion;
    
	hideDiv($('#mdlDelAssessment #alertdiv'));
	$('#mdlDelAssessment  #deleteAssessmentMessage').text("Do you want to remove question paper '"+deleteAssmntName+ " '?");
	$('#mdlDelAssessment').modal().show();
};
$(document).on('click','#deleteAssessmentButton', function(){
	spinner.showSpinner();
	$.wait(50).then(deleteAssessment);
	
});

var deleteAssessment=function(){
	spinner.showSpinner();
	var aResponse = fnAjax("assessment/"+deleteAssessmentId, null, "DELETE");
	if(aResponse!=null && aResponse!=""){
		if(aResponse.response=="shiksha-200"){
			deleteRow("assessmentTable",deleteAssessmentId);		
			fnUpdateDTColFilter('#assessmentTable',[4,5,6,8,9,12,13],15,[0,13,14],thLabels);
			//location.reload();
          //  $("#success-msg").css('display','block');
 			//$("#success-msg").fadeOut(3000);
 			$("html, body").animate({ scrollTop: 0 }); 
 			 $('#mdlDelAssessment').modal("hide");
 	           
 			setTimeout(function() {   //calls click event after a one sec
 				AJS.flag({
 					type: 'success',
 					title: 'Success!',
 					body: '<p>'+aResponse.responseMessage+'</p>',
 					close :'auto'
 				})
 			}, 1000);
 			location.reload();
		} else {
			spinner.hideSpinner();
			showDiv($('##mdlDelAssessment #alertdiv'));
			$("#mdlDelAssessment").find('#errorMessage').show();
			$("#mdlDelAssessment").find('#exception').text(aResponse.responseMessage);
			$("#mdlDelAssessment").find('#buttonGroup').show();
		}
	}	
	spinner.hideSpinner();


}

//edit Assessment
var editAssessment =function(assessmentId,sgAssessmentCode){
	spinner.showSpinner();
	/**
	 * SHK-1339
	 * */
	isEditAssessment =true;

	/**
	 * SHK-1344
	 * */
	aAssessmentId =assessmentId;

	fnAjax("updateAssessmentModifyDate/"+assessmentId,null, "POST");
	// open the survey page in SG Engine							
	spinner.hideSpinner();
	$('#mdlRedirectConfirmation').modal();
	sgSurveyCode=sgAssessmentCode;	

}

//preview assessment
var previewAssessment =function(assessmentId,sgAssessmentCode){
	// open the survey page in SG Engine

	var previewUrlForIframe = assmentPreviewUrl.replace('SURVEY_CODE',sgAssessmentCode);
	$("#surveypreviewiframe").attr("src",previewUrlForIframe+"?deviceMode=touch");
	$("#surveyPreviewPopup").modal("show");
}

var fnBindSourceToListBox = function(element, data,selectedSrc) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		if(obj.sourceId ==selectedSrc){
			$ele.append('<option value="' + obj.sourceId + '" data-id="'+ obj.sourceId + '"data-name="' + obj.sourceName + '" selected>'+ escapeHtmlCharacters(obj.sourceName) + '</option>');	
		}else{
			$ele.append('<option value="' + obj.sourceId + '" data-id="'+ obj.sourceId + '"data-name="' + obj.sourceName + '">'+ escapeHtmlCharacters(obj.sourceName) + '</option>');
		}

	});
	setOptionsForViewAssessmentMultipleSelect(element);
}


var fnBindStageToListBox = function(element, data,bSelectedStage) {
	$ele = $(element);
	$.each(data, function(index, obj) {

		if(obj.stageId==bSelectedStage){
			$ele.append('<option value="' + obj.stageId + '" data-id="'+ obj.stageId + '"data-name="' + obj.stageName + '" selected>'+ escapeHtmlCharacters(obj.stageName) + '</option>');	
		}else{
			$ele.append('<option value="' + obj.stageId + '" data-id="'+ obj.stageId + '"data-name="' + obj.stageName + '">'+ escapeHtmlCharacters(obj.stageName) + '</option>');
		}

	});
	setOptionsForViewAssessmentMultipleSelect(element);
}


var fnBindGradeToListBox = function(element, data,bSelectedGrade) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		if(obj.gradeId ==bSelectedGrade ){
			$ele.append('<option value="' + obj.gradeId + '" data-id="'+ obj.gradeId + '"data-name="' + obj.gradeName + '" selected>'+ escapeHtmlCharacters(obj.gradeName) + '</option>');	
		}else{
			$ele.append('<option value="' + obj.gradeId + '" data-id="'+ obj.gradeId + '"data-name="' + obj.gradeName + '">'+ escapeHtmlCharacters(obj.gradeName) + '</option>');
		}

	});
	setOptionsForViewAssessmentMultipleSelect(element);
}

//bind source
var fnBindSource = function(pSelected) {	
	var sources = customAjaxCalling("source/list",null, "GET");
	fnEmptyAll(addAssmntMdlElements.source);
	if (sources!=null&&sources != "") {
		if(sources[0]!=null && sources[0]!=''){
			if (sources[0].response == "shiksha-200") {
				fnBindSourceToListBox(addAssmntMdlElements.source,sources,pSelected);
			}
		}
	}
}

//bind stage
var fnBindStage = function(aSelectedStage) {
	var stages = customAjaxCalling("getStageList",null, "GET");
	fnEmptyAll(addAssmntMdlElements.stage);
	if (stages!=null && stages != "" && stages[0]!=null && stages[0].response == "shiksha-200") {


		fnBindStageToListBox(addAssmntMdlElements.stage, stages,aSelectedStage);


	}
}


//bind grade
var fnBindGrade = function(aSelectedGrade) {	
	var grades = customAjaxCalling("getGradeList", "GET");
	fnEmptyAll(addAssmntMdlElements.grade);
	if (grades!=null && grades != "" && grades[0]!=null && grades[0].response == "shiksha-200") {

		fnBindGradeToListBox(addAssmntMdlElements.grade, grades,aSelectedGrade);

	}
}

//reinitialize filters for create assessment
//SHK-630
var fnInitFilters =function(){
	resetFormById('addAssessmentForm');
	fnEmptyAll(addAssmntMdlElements.source,addAssmntMdlElements.stage,addAssmntMdlElements.grade,addAssmntMdlElements.subject);
	fnEmptyAll(addAssmntMdlElements.unit);
	destroyAndRefreshMultiselect($(addAssmntMdlElements.source),$(addAssmntMdlElements.stage),
			$(addAssmntMdlElements.grade),$(addAssmntMdlElements.subject),
			$(addAssmntMdlElements.unit));


	hideDiv($(addAssmntMdlDivs.subject),$(addAssmntMdlDivs.unit));
	limitTextArea(addAssmntMdlElements.textAreaMsg);


	aMajorVersionSelectedUnitIds =[];
	aMajorVersionSelectedSubject='';

	fnBindSource(0);
	fnBindStage(0);
	fnBindGrade(0);

	fnCollapseMultiselect();
}


//hide any succeess/error div on add assessment modal
function hideResponseDiv(event){

	var key = event.keyCode;
	if(key != 13){
		$('#mdlAddAssessment #alertdiv').css('display','none');
	}
}
//checkout--check-in assessment in sg

var fnCheckInAssessment	=function(pChkInAssessmentId,pChkInSgAssessmentCode,pChkInAsessmentName,pChkInAssessmentVersion){

	var chkAssmntname =pChkInAsessmentName+"-"+pChkInAssessmentVersion;
	$('#btnChkInAssmnt').prop('disabled',false);
	$('#chkInAssmnt').text('');
	$('#chkInAssmnt').text('Do you want to finalize the question paper "'+chkAssmntname+' ."');
	hideDiv($('#mdlPublishAssessment #alertdiv'));
	$('#mdlPublishAssessment').modal('show');

	sgApiPublishAssmentUrl='';
	sgApiPublishAssmentUrl =sgApiCheckInAssmentUrl;
	aChkInAssessmentId	=pChkInAssessmentId;
	aChkInSgAssessmentCode	=pChkInSgAssessmentCode;

	aChkInAssessmentVersion =pChkInAssessmentVersion;




	exportedAssessmentFileName=aChkInSgAssessmentCode+"_"+aChkInAssessmentVersion;
}



var fnPublish=function(){
	spinner.showSpinner();
	$.wait(50).then(fnPublishAssessment);
}

var fnPublishAssessment =function(){
	spinner.showSpinner();
     
	var aPublishRequestData ={
			"assessmentId":aChkInAssessmentId,
			"sgAssessmentCode":aChkInSgAssessmentCode,
			"assessmentVersion":aChkInAssessmentVersion
			};
	var publishAssmntRsponse =fnAjax("assessment/publish",aPublishRequestData, "POST");
	if( publishAssmntRsponse.response=="shiksha-200"){

		sgGetAssessmentDetails ='';
		sgGetAssessmentDetails =sgApiGetAssmentDetailsUrl;
		$('#mdlPublishAssessment').modal('hide');
		spinner.hideSpinner();

		setTimeout(function() {   //calls click event after a one sec
			AJS.flag({
				type: 'success',
				title: 'Success!',
				body: '<p>Information saved.</p>',
				close :'auto'
			})
		}, 1000);
		location.reload();

	}else{
		spinner.hideSpinner();
		showDiv($('#mdlPublishAssessment #alertdiv'));			
		$("#mdlPublishAssessment").find('#errorMessage').show();
		$("#mdlPublishAssessment").find('#exception').show();			
		$("#mdlPublishAssessment").find('#exception').text(publishAssmntRsponse.responseMessage);


	}
	spinner.hideSpinner();

}


var fnShowQuestionTaxonomyMessage=function(zMessage){
	spinner.hideSpinner();
	$('#btnChkInAssmnt').prop('disabled',true);
	showDiv($('#mdlPublishAssessment #alertdiv'));			
	$("#mdlPublishAssessment").find('#errorMessage').show();
	$("#mdlPublishAssessment").find('#exception').show();			
	$("#mdlPublishAssessment").find('#exception').text(zMessage);

}
//unpublish
var fnCheckOutAssessment	=function(pChkOutAssessmentId,pChkOutSgAssessmentCode,pChkOutAsessmentName,pChkOutAssessmentVersion){
	spinner.hideSpinner();
	var chkOutAssmntname =pChkOutAsessmentName+"-"+pChkOutAssessmentVersion;

	hideDiv($('#mdlUnpublishAssessment #alertdiv'));
	$('#btnChkOutAssmnt').prop('disabled',false);
	$('#chkOutAssmnt').text('');
	$('#chkOutAssmnt').text('Do you want to create a new minor version of question paper "'+chkOutAssmntname +'"  with minor modifications?');
	$('#mdlUnpublishAssessment').modal('show');

    sgApiUnPublishAssmentUrl='';
	sgApiUnPublishAssmentUrl =sgApiCheckOutAssmentUrl;
	bChkOutAssessmentId	=pChkOutAssessmentId;
	bChkOutSgAssessmentCode	=pChkOutSgAssessmentCode;
	bChkOutAsessmentName	=pChkOutAsessmentName;

}

///////////Un publish Question paper/////////////////

var fnUnpublish=function(){
	spinner.showSpinner();
	$.wait(50).then(fnUnpublishAssessment);
}
var fnUnpublishAssessment =function(){
	spinner.showSpinner();
	var aUnPublishRequestData ={
			"assessmentId":bChkOutAssessmentId,
			"sgAssessmentCode":bChkOutSgAssessmentCode,
			
	};
	var unPublishResponse =fnAjax("assessment/unpublish",aUnPublishRequestData, "POST");
	if(unPublishResponse.response=="shiksha-200"){
		$('#mdlUnpublishAssessment').modal('hide');				

		spinner.hideSpinner();
		bChkOutSgAssessmentCode=unPublishResponse.sgAssessmentCode;
		sgSurveyCode=unPublishResponse.sgAssessmentCode;

		isEditAssessment =true; //SHK-1339
		aAssessmentId =bChkOutAssessmentId; //SHK-1344

		$('#mdlRedirectConfirmation').modal();
		return false;

	}else{
		spinner.hideSpinner();
		$('#btnChkOutAssmnt').prop('disabled',true);
		showDiv($('#mdlUnpublishAssessment #alertdiv'));			
		$("#mdlUnpublishAssessment").find('#errorMessage').show();
		$("#mdlUnpublishAssessment").find('#exception').show();			
		$("#mdlUnpublishAssessment").find('#exception').text(unPublishResponse.responseMessage);

	}
	spinner.hideSpinner();
	enableTooltip();
}

//download assessment
var fnDownloadAssessment	=function(pDownloadAssessmentId,pDoanloadSgAssessmentCode,pDownloadAsessmentLink){	
	window.open(pDownloadAsessmentLink,'_blank');
}


//SHK-1264
//create a major version with same properties
var fnCreateMajorVersion =function(pSourceId,pStageId,pGradeId,pSubjectId,pUnitIds,pDescription){

	 resetFormById('addAssessmentForm');
	fnEmptyAll(addAssmntMdlElements.source,addAssmntMdlElements.stage,addAssmntMdlElements.grade,addAssmntMdlElements.subject);
	fnEmptyAll(addAssmntMdlElements.unit);
	destroyAndRefreshMultiselect($(addAssmntMdlElements.source),$(addAssmntMdlElements.stage),
			$(addAssmntMdlElements.grade),$(addAssmntMdlElements.subject),
			$(addAssmntMdlElements.unit));			
	limitTextArea(addAssmntMdlElements.textAreaMsg);
	$(addAssmntMdlElements.description).val(pDescription);
	$('textarea').trigger('keyup');

	fnBindSource(parseInt(pSourceId));
	fnBindStage(parseInt(pStageId));
	fnBindGrade(parseInt(pGradeId));

	aMajorVersionSelectedSubject =pSubjectId;
	aMajorVersionSelectedUnitIds =JSON.parse(pUnitIds);


	fnGetSubjectByGrade();
	fnGetUnitsBySub();


}
var loadAssessmentPage=function(){
	if(isFromProjectPage || isFromProjectPage=="true"){
		window.location =baseContextPath+"/assessment"
	}
	else{
		location.reload();
	}
};

$(function() {

	isEditAssessment =false;
	aAssessmentId=0;

	$('[data-toggle="tooltip"]').tooltip();			 


	thLabels =['#','Grade ','Subject ','Chapter(s) ','Stage  ','Source  ','Question Paper Name ','Question Paper Description ','Version ','Objective Of Question Paper ','No.Of Questions ','Status ','Created By ','Modified By ','Modification Date ','Action'];
	fnSetDTColFilterPagination('#assessmentTable',16,[0,15],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#assessmentTable',[4,5,6,9,10,12,13]);
	});
	fnColSpanIfDTEmpty('assessmentTable',16);

	showDiv($('#tableDiv'));
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	setOptionsForViewAssessmentMultipleSelect($(addAssmntMdlElements.source),$(addAssmntMdlElements.stage),
			$(addAssmntMdlElements.grade),$(addAssmntMdlElements.subject),$(addAssmntMdlElements.unit));

	limitTextArea(addAssmntMdlElements.textAreaMsg);



	$("#"+addAssmntForm).formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields:{
			assessmentName : {
				validators : {
					stringLength: {
						max: 250,
						message: 'Question paper name must be less than 250 characters'
					},
					callback : {
						message : 'Question paper '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			description : {
				validators : {
					callback : {
						message : 'Description does not contains these (", ?, \', /, \\) special characters',
						callback : function(value) {

							var regx=/^[^'?\"/\\]*$/;

							return regx.test(value);
						}
					}
				}
			},

			source: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addAssmntMdlElements.source).val();
							return (option != null&& option!="NONE");
						}
					}
				}
			},
			stage : {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addAssmntMdlElements.stage).val();
							return (option != null&& option!="NONE");
						}
					}
				}
			},
			grade: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addAssmntMdlElements.grade).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			subject: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addAssmntMdlElements.subject).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			unit: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addAssmntMdlElements.unit).val();
							return (option != null && option != "multiselect-all" && option!="NONE");
						}
					}
				}
			},
			description : {
				validators : {
					callback : {
						message : 'Does not contains these (", ?, \', /, \\) special characters',

						callback : function(value) {
							var regx = /^[^'?\"/\\]*$/;

							return regx.test(value);
						}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();

		isMajorVersion(e); //SHK-1353
	});




	//if request from create project then open Createmodal popup
	if(isFromProjectPage || isFromProjectPage=="true"){
		$("#btnCreate").trigger( "click" );
	}

	$(".close-survey-preview").on('click',function(){
		$("#surveypreviewiframe").attr("src","");
	});
	spinner.hideSpinner();
});
