var accessToken;
var tokenType;
var expireTime;
var getAccessTokenUrl;

var getAccessToken = function(URL,requestData,requestType,callback) {
	getAccessTokenUrl =URL;
	$.ajax({
		type : requestType,
		url : URL,
		data: requestData,
		contentType:"application/x-www-form-urlencoded",
		success : function(data) {
			
			callback(data);
		},
		error : function(data) {
			callback(data);
		}
	});	
};

function createAssessmentSgURICall(URL,requestData,requestType,callback) {
	
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(requestData),
	    contentType:"application/json",
	    success: function( sgResponse ) {
	    	callback(sgResponse);
	    },
	    error : function(sgResponse) {
	    	
			callback(sgResponse);
		}
	});
};




//publish assessment
function fnPublishAssessmentInSg(URL,requestData,requestType,callback) {
	
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(requestData),
	    contentType:"application/json",
	    success: function( sgResponse ) {
	    	callback(sgResponse);
	    },
	    error : function(sgResponse) {
	    	
			callback(sgResponse);
		}
	});
};



//unpublish assessment
function fnUnPublishAssessmentInSg(URL,requestData,requestType,callback) {
	
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(requestData),
	    contentType:"application/json",
	    success: function( sgResponse ) {
	    	callback(sgResponse);
	    },
	    error : function(sgResponse) {
	    	
			callback(sgResponse);
		}
	});
};




//get assessment details
function fnGetAssessmentDetailsFromSg(URL,requestData,requestType,callback) {
	
	$.ajax({
		url: URL,
	    type: requestType,	    
	    data: JSON.stringify(requestData),
	    contentType:"application/json",
	    success: function( sgResponse ) {
	    	callback(sgResponse);
	    },
	    error : function(sgResponse) {
	    	
			callback(sgResponse);
		}
	});
};