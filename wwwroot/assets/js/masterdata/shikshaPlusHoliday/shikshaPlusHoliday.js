//TODO-remove unused params
//TODO-remove logs
//TODO-remove commented codes
//TODO-support i18n
//TODO-same as shikshaHoliday
var start = new Date(); 
var end = new Date();
var dateLabel=dateLabel=columnMessages.date;
var pageContextElements = {
		table:$('#holidayTable'),
		addBtn:$("#addHoliday"),
		uploadBtn : $('#uploadHoliday'),
		uploadList : $("#uploadHolidayList"),
		eleYear:$("#selectYear")
};
var addModal = {
		modal:$('#mdlAddHoliday'),
		form:$('#addHolidayForm'),
		eleEndDateCheckBox:$('#endDateCheckBox'),
		eleEndDate:$("#addEndDate"),
		eleStartDate:$("#addStartDate"),
		eleName:$("#addHolidayName"),
		eleYear:$("#mdlAddSelectYear")
};
var editModal = {
		modal:$('#mdlEditHoliday'),
		form:$('#editHolidayForm'),
		eleHolidayId:$('#editHolidayId'),
		eleEndDateCheckBox:$('#editEndDateCheckBox'),
		eleEndDate:$("#editEndDate"),
		eleStartDate:$("#editStartDate"),
		eleName:$("#editHolidayName"),
		eleYear:$("#mdlEditYear")
};
var deleteModal = {
		modal:$('#mdlDelHoliday'),
		confirm: $('#btnDelHoliday')
};
var submitActor = null;
var optionSelected="option:selected";
var $submitActors = $(addModal.form).find('button[type=submit]');
var shikshaPlusHolidayFilterObj = {
		toolBar : "#toolbarShikshaPlusHoliday",
		init: function(){
			$(this.toolBar).show();
		},
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(value, row, index, field){ 
			return {
				classes:"dropdown dropdown-td"
			}
		},
		fiterQueryParmams : function(params){
			params['year'] = $("#selectYear").val();
			return params;
		}
	};
var holiday ={
		holidayId:0,
		init:function(){
			console.log('self.init');
			self =holiday;
			setOptionsForMultipleSelect($("#selectYear"));
			pageContextElements.uploadBtn.on('click',self.uploadHolidayList);
			
			$('input[type=file]').on('change', self.prepareUpload);

			pageContextElements.addBtn.on('click',self.initAddModal);

			addModal.eleEndDateCheckBox.on('click',self.enableEndDate);
			editModal.eleEndDateCheckBox.on('click',self.enableEndDateEdit);
			deleteModal.confirm.on('click',self.deleteFunction);
		},
		prepareUpload: function(event){
			var file = event.target.file;
			holiday.fnUpload(file);
		},
		enableEndDateEdit: function(){
			if($(this).is(':checked')) {
				$("#editEndDateDiv").show();
				$(editModal.eleStartDate).attr('name', 'startDate');
				$("#editStartDateLabel").text(columnMessages.startDate);
				$("#addStartDate-error").hide();
				$("#editEndDate-error").hide();
			} else {
				editModal.eleEndDate.val('');
				$("#editStartDateLabel").text(columnMessages.date);
				$(editModal.eleStartDate).attr('name', 'date');
				$("#editStartDate-error").hide();
				$("#editEndDate-error").hide();
				$("#editEndDateDiv").hide();
			}
		},
		enableEndDate: function(){
			
			$(addModal.eleStartDate).datepicker('remove');
			$(addModal.eleStartDate).datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start,
			}).on("hide",function(e){
				addModal.eleEndDate.datepicker('setStartDate', $(this).val());
				$(this).trigger("blur");
			});
			
			if($(this).is(':checked')) {
				$(".add-enddate-div").show();
				$(addModal.eleStartDate).attr('name', 'startDate');
				$("#addStartDate-error").hide();
				$("#addEndDate-error").hide();
				$("#addStartDateLabel").text(columnMessages.startDate);
			} else {
				addModal.eleStartDate.datepicker();
				$("#addStartDateLabel").text(columnMessages.date);
				$(addModal.eleStartDate).attr('name', 'date');
				$("#addStartDate-error").hide();
				$("#addEndDate-error").hide();
				addModal.eleEndDate.val('');
				$(".add-enddate-div").hide();
			}
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		uploadHolidayList: function(event){
			pageContextElements.uploadList.click();
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			$("#addStartDateLabel").text(columnMessages.date);
			addModal.eleEndDate.val('');
			$(".add-enddate-div").hide();
			holiday.resetForm("#addHolidayForm");
			addModal.form.trigger("reset");
			holiday.enableDatePicker();
			addModal.modal.modal("show");
		},

		initDelete : function(id,name){
			var msg = $("#deleteHolidayMessage").data("message");
			$("#deleteHolidayMessage").html(msg.replace('@NAME@',name));
			deleteModal.modal.modal("show");
			holidayId=id;
		},

		deleteFunction : function(){
			holiday.fnDelete(holidayId);
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		
		fnUpload : function(file){
			spinner.showSpinner();
			var ajaxData= file;
			console.log(ajaxData);
			var addAjaxCall = shiksha.invokeAjax("shikshaPlus/holiday/upload", ajaxData, "POST");

			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					
					holiday.refreshTable(pageContextElements.table);

					AJS.flag({
						type : "success",
						title : "Successfully Created!"	,
						body : addAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":addModal.eleName.val(),
					"startDate":addModal.eleStartDate.val(),
					"endDate":addModal.eleEndDate.val()
			};
			console.log(ajaxData);
			var addAjaxCall = shiksha.invokeAjax("shikshaPlus/holiday", ajaxData, "POST");

			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == "addHolidaySaveButton"){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type : "success",
							title : appMessgaes.success	,
							body : addAjaxCall.responseMessage,
							close :'auto'
						})
					}
					if(submitBtnId == "addHolidaySaveMoreButton"){
						$(addModal.modal).modal("show");
						$(addModal.modal).find("#divSaveAndAddNewMessage").show();
						fnDisplaySaveAndAddNewElementAui(addModal.modal,ajaxData.name);
					}
					addModal.form.trigger("reset");
					$(".add-enddate-div").hide();
					holiday.refreshTable(pageContextElements.table);
					holiday.enableDatePicker();
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(holidayId){
			editModal.eleEndDate.val('');
			$("#editStartDateLabel").text("Date");
			$("#editEndDateDiv").hide();
			holiday.resetForm("#editHolidayForm");
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaPlus/holiday/"+holidayId,null, "GET");
			spinner.hideSpinner();
			console.log(editAjaxCall);
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					editModal.eleHolidayId.val(editAjaxCall.holidayId);
					editModal.eleName.val(editAjaxCall.name);
					$(editModal.eleStartDate).val(editAjaxCall.startDate);
					if(editAjaxCall.endDate && editAjaxCall.endDate != editAjaxCall.startDate){
						$("#editStartDateLabel").text(columnMessages.startDate);
						 $(editModal.eleStartDate).attr('name', 'startDate');
						$("#editEndDateCheckBox").prop("checked",true);
						$("#editStartDate-error").hide();
						$("#editEndDate-error").hide();
						$("#editEndDateDiv").show();
						editModal.eleEndDate.datepicker('setDate', editAjaxCall.endDate);
					}else{
						$(editModal.eleEndDateCheckBox).prop("checked",false);
						$(editModal.eleEndDateDiv).hide();
					}
					//editModal.eleStartDate.val(editAjaxCall.startDate);
					//editModal.eleEndDate.val(editAjaxCall.endDate);
					/*editModal.eleStartDate.datepicker('setDate', editAjaxCall.startDate);
					editModal.eleEndDate.datepicker('setDate', editAjaxCall.endDate);
					if(editAjaxCall.endDate){
						$("#editStartDateLabel").text("Start Date");
						$("#editEndDateDiv").show();
						$("#editEndDateCheckBox").prop("checked",true);
					}*/
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"holidayId":editModal.eleHolidayId.val(),
					"name":editModal.eleName.val(),
					"startDate":editModal.eleStartDate.val(),
					"endDate":editModal.eleEndDate.val()
			}

			var updateAjaxCall = shiksha.invokeAjax("shikshaPlus/holiday/", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					holiday.refreshTable(pageContextElements.table);
					AJS.flag({
						type : "success",
						title : appMessgaes.success	,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : updateAjaxCall.responseMessage,
						close :'auto'
					});
				}
				holiday.enableDatePicker();
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				},function(){
					window.location.reload();
				})
			}
		},
		fnDelete :function(holidayId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shikshaPlus/holiday/"+holidayId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					holiday.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					})
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : deleteAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {

					name: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace: true
					},
					startDate: {
						required: function(element){
							return $(element).parents(".form-group").is(":visible");
						},
						validDate : true,
					},
					date: {
						required: function(element){
							return $(element).parents(".form-group").is(":visible");
						},
						validDate : true,
					},
					endDate: {
						required: function(element){
							return $(element).parents(".form-group").is(":visible");
						},
						validDate : true,
					}
				},

				messages: {
					name: {
						required: holidayMessages.nameRequired,
						noSpace: holidayMessages.nameNoSpace,
						minlength: holidayMessages.nameMinlength,
						maxlength: holidayMessages.nameMaxlength,

					},
					startDate: {
						required: holidayMessages.startDateRequired.replace("@NAME",columnMessages.startDate),
						validDate: holidayMessages.startDateFormat,

					},
					date: {
						required: holidayMessages.startDateRequired.replace("@NAME",columnMessages.date),
						validDate: holidayMessages.startDateFormat,

					},
					endDate: {
						required: holidayMessages.endDateRequired,
						validDate: holidayMessages.endDateFormat,
					}
				},
				errorPlacement: function(error, element) {
				    if($(element).hasClass("datepicker")){
				    	$(element).parent().parent().append(error);
				    } else {
				    	$(error).insertAfter(element);
				    }
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						holiday.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						holiday.fnUpdate();
					}
				}
			});
		},
		holidayActionFormater: function(value, row, index) {			
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="holiday.initEdit(\''+row.holidayId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="holiday.initDelete(\''+row.holidayId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		getDataByYear: function(){
			selectedYear= $(pageContextElements.eleYear).find(optionSelected).val();
			$table.bootstrapTable('refresh', {
				url : "shikshaPlus/holiday/year/"+selectedYear,
				method:'post',
				queryParamsType :'limit',
            });
			
			
		},
		enableDatePicker : function(){
			$('.datepicker').datepicker("remove");
			$('.datepicker').datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start
			}).on("hide",function(){
				$(this).trigger("blur");
			});
		}
};

$( document ).ready(function() {

	holiday.init();
	var  selectedYear=$(pageContextElements.eleYear).find(optionSelected).val();
	$table = pageContextElements.table.bootstrapTable({
		toolbar: "#toolbarShikshaPlusHoliday",
		url : "shikshaPlus/holiday/year/"+selectedYear,
		method : "post",
		toolbarAlign :"left",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 50,
		formatShowingRows : shikshaPlusHolidayFilterObj.formatShowingRows,
		clickToSelect: false,
		queryParamsType :'limit',
		queryParams : shikshaPlusHolidayFilterObj.fiterQueryParmams
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
	jQuery.validator.addMethod("validDate", function(value, element) {
        return this.optional(element) || moment(value,"DD/MMM/YYYY").isValid();
    },"");
	
	holiday.formValidate(addModal.form);
	holiday.formValidate(editModal.form);
	var start = new Date(); 
	var end = new Date();
	$('.datepicker').datepicker({
		format: 'dd-M-yyyy',
		todayHighlight: true,
		autoclose: true,
		startDate:start,
	}).on("hide",function(e){
		$(this).trigger("blur");
	});
	addModal.eleStartDate.datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		addModal.eleEndDate.datepicker('setStartDate', $(this).val());
	}); 
	addModal.eleEndDate.datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		addModal.eleStartDate.datepicker('setEndDate', $(this).val());
	}); 
	$submitActors.click(function() {
		submitActor = this;
	});
	shikshaPlusHolidayFilterObj.init();
});
