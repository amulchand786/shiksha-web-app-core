var  colorClass='';
var thLabels;
var initInput =function(){	
	
	$("#mdlAddRole").modal().show();
	$("#addRoleName").val('');
	$("#addRoleCode").val('');
	$("#addRoleSequenceNumber").val('');
	$('#addMappedToSchool').prop('checked',false);
	$("#mdlAddRole #addRoleForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddRole #addRoleForm").data('formValidation').resetForm();		
	 
}

//create a row after save
var getRoleRow =function(role){
	var isMappedToSchool=(!role.isMappedToSchool?'No':"Yes");
	var disabledDeleteBtn="<a   style='margin-left:10px;' class='tooltip tooltip-top'	 id=deleteRole	onclick=deleteRoleData(&quot;"+role.roleId+"&quot;,&quot;"+escapeHtmlCharacters(role.roleName)+"&quot;); > <span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>";
	
	if(loggedUserRoleId==role.roleId){
		disabledDeleteBtn=	"<a   style='margin-left:10px;' class='tooltip tooltip-top notactive'	 id=deleteRole> <span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>";
	}
	
	var row = 
		"<tr id='"+role.roleId+"' data-id='"+role.roleId+"'>"+
		"<td></td>"+
		"<td data-roleid='"+role.roleId+"'	title='"+role.roleName+"'>"+role.roleName+"</td>"+
		
		"<td data-rolecode='"+role.roleCode+"' title='"+role.roleCode+"'>"+role.roleCode+"</td>"+	
		"<td data-sequencenumber='"+role.sequenceNumber+"' title='"+role.sequenceNumber+"'>"+role.sequenceNumber+"</td>"+
		"<td data-programType='"+role.programType+"' title='"+role.programType+"'>"+role.programType+"</td>"+
		"<td data-isMappedtoSchool='"+role.isMappedtoSchool+"'>"+isMappedToSchool+"</td>"+
		"<td><div  class='div-tooltip'><a   class='tooltip tooltip-top'	 id='btnEditRole'" +
		"onclick=editRoleData(&quot;"+role.roleId+"&quot;,&quot;"+escapeHtmlCharacters(role.roleName)+"&quot;,&quot;"+escapeHtmlCharacters(role.roleCode)+"&quot;,&quot;"+escapeHtmlCharacters(role.sequenceNumber)+"&quot;,&quot;"+role.isMappedToSchool+"&quot;); > <span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></a>"+
		disabledDeleteBtn+
		"</tr>";
	return row;
}


var applyPermissions=function(){
	if (!editRolePermission){
	$("#btnEditRole").remove();
	}
	if (!deleteRolePermission){
	$("#deleteRole").remove();
	}
	}


var addRoleData=function(event){
	
	
    var roleName = $('#addRoleName').val().trim().replace(/\u00a0/g," ");
    var roleCode = $('#addRoleCode').val().trim().replace(/\u00a0/g," ");
    var sequenceNumber = $('#addRoleSequenceNumber').val();
    var isMappedToSchool;
    if($('#mdlAddRole #addMappedToSchool').not(':checked').length){	      
    	 isMappedToSchool=0;
	}else{
		 isMappedToSchool=1;
	}
    
    
    var programCode=($("#isShiksha").prop("checked")&&$("#isShikshaPlus").prop("checked"))?2
    		:$("#isShiksha").prop("checked")?0:1;
    var json ={ 

    		"roleName" : roleName,
    		"roleCode" : roleCode,
    		"isMappedToSchool" : isMappedToSchool,
    		"sequenceNumber" : sequenceNumber,
    		"programCode":programCode
    };  
    
    
	  //update datatable without reloading the whole table
	//save&add new
	var btnAddMoreRoleId =$(event.target).data('formValidation').getSubmitButton().data('id');
	
	var role = customAjaxCalling("role", json, "POST");
	if(role!=null){
	if(role.response=="shiksha-200"){
		$(".outer-loader").hide();

		var newRow =getRoleRow(role);
		appendNewRow("roleTable",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#roleTable',[],7,[0,6],thLabels);
		setShowAllTagColor(colorClass);
		if(btnAddMoreRoleId=="addRoleSaveMoreButton"){
			$("#mdlAddRole").find('#errorMessage').hide();

			
			fnDisplaySaveAndAddNewElement('#mdlAddRole',roleName);
			initInput();
		}else{	
			$("#mdlAddRole").modal("hide");	
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: appMessgaes.success,
				    body: '<p>'+ role.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			
			

		}	
	}else {
		showDiv($('#mdlAddRole #alertdiv'));
		$("#mdlAddRole").find('#successMessage').hide();
		$("#mdlAddRole").find('#okButton').hide();
		$("#mdlAddRole").find('#errorMessage').show();
		$("#mdlAddRole").find('#exception').show();
		$("#mdlAddRole").find('#buttonGroup').show();
		$("#mdlAddRole").find('#exception').text(role.responseMessage);
	}
	}
	$(".outer-loader").hide();
	enableTooltip();
};






var editRoleId=null;
var deleteRoleName=null;
var deleteRoleId=null;
 
var editRoleData=function(roleId,roleName,roleCode,sequenceNumber,isMappedToSchool){	
	var roleData = customAjaxCalling("role/"+roleId, json, "GET");
	
	 
		if(roleData.programCode==2){
			$('#mdlEditRole #isShiksha').prop('checked',true).prop('disabled', true);
			$('#mdlEditRole #isShikshaPlus').prop('checked',true).prop('disabled', true);
		}else if(roleData.programCode==0){
			$('#mdlEditRole #isShiksha').prop('checked',true).prop('disabled', true);	
			$('#mdlEditRole #isShikshaPlus').prop('checked',false).prop('disabled', true);
		} else if(roleData.programCode==1){
			$('#mdlEditRole #isShiksha').prop('checked',false).prop('disabled', true);
			$('#mdlEditRole #isShikshaPlus').prop('checked',true).prop('disabled', true);
		
	   }  
	
	$('#editRoleName').val(roleName);
	$('#editRoleCode').val(roleCode);
	$('#editRoleSequenceNumber').val(sequenceNumber);
	
	editRoleId=roleId;
	
	if(isMappedToSchool=="true"){
		$('#mdlEditRole #editMappedToSchool').prop('checked',true);
	}else{
		$('#mdlEditRole #editMappedToSchool').prop('checked',false);
	}
	
	$("#mdlEditRole").modal().show();
	  $("#mdlEditRole #editRoleForm").data('formValidation').resetForm();
};
var editData=function(){

    var roleName = $('#editRoleName').val().trim().replace(/\u00a0/g," ");
    var roleCode = $('#editRoleCode').val().trim().replace(/\u00a0/g," ");
    var sequenceNumber = $('#editRoleSequenceNumber').val();
    var isMappedToSchool;
    if($('#mdlEditRole #editMappedToSchool').not(':checked').length){	      
    	 isMappedToSchool=0;
	}else{
		 isMappedToSchool=1;
	}
	var json ={ 
    	  "roleId" : editRoleId,
    	  "roleName" : roleName,
    	  "roleCode" : roleCode,
    	  "sequenceNumber" : sequenceNumber,
    	  "isMappedToSchool" : isMappedToSchool,
    };
	
	var role = customAjaxCalling("role", json, "PUT");
	if(role!=null){
	if(role.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("roleTable",role.roleId);
		var newRow =getRoleRow(role);
		appendNewRow("roleTable",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#roleTable',[],7,[0,6],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	
		
			$('#mdlEditRole').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: appMessgaes.success,
				    body: '<p>'+ role.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			
			
		}		
	else {
		
		showDiv($('#mdlEditRole #alertdiv'));
		$("#mdlEditRole").find('#successMessage').hide();
		$("#mdlEditRole").find('#errorMessage').show();
		$("#mdlEditRole").find('#exception').show();
		$("#mdlEditRole").find('#buttonGroup').show();
		$("#mdlEditRole").find('#exception').text(role.responseMessage);
	}
	}
	$(".outer-loader").hide();
	enableTooltip();
};

var deleteRoleData=function(roleId,roleName){
	deleteRoleId=roleId;
	deleteRoleName=roleName;
	$('#mdlDeleteRole #deleteRoleMessage').text(columnMessages.deleteConfirm.replace("@NAME",roleName));
	$("#mdlDeleteRole").modal().show();
	hideDiv($('#mdlDeleteRole #alertdiv'));
};
$(document).on('click','#deleteRoleButton', function(){
	
    
    
    var role = customAjaxCalling("role/"+deleteRoleId, null, "DELETE");
    if(role!=null){
	if(role.response=="shiksha-200"){
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("roleTable",deleteRoleId);
		
		fnUpdateDTColFilter('#roleTable',[],7,[0,6],thLabels);
		setShowAllTagColor(colorClass);
		$('#mdlDeleteRole').modal("hide");
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: appMessgaes.success,
			    body: '<p>'+ role.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
		
		
	} else {
		showDiv($('#mdlDeleteRole #alertdiv'));
		$("#mdlDeleteRole").find('#errorMessage').show();
		$("#mdlDeleteRole").find('#exception').text(role.responseMessage);
		$("#mdlDeleteRole").find('#buttonGroup').show();
	}
    }
	$(".outer-loader").hide();
});



$(function() {
	thLabels =['#',columnMessages.role,columnMessages.roleAcronym,columnMessages.sequenceNumber,columnMessages.programType,columnMessages.mappedToSchool,columnMessages.Action];
	fnSetDTColFilterPagination('#roleTable',7,[0,6],thLabels);
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#roleTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$('#tableDiv').show();
	
	$( "#addRoleForm" ).keypress(function() {		
		hideDiv($('#mdlAddRole #alertdiv'));
	})
		$( "#editRoleForm" ).keypress(function() {		
		hideDiv($('#mdlEditRole #alertdiv'));
	})
    $("#addRoleForm") .formValidation({
    	feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			roleName : {
				validators : {
					stringLength: {
	                    max: 250,
	                    message: validationMsg.nameMaxLength
	                },
					callback : {
						message : columnMessages.roleValidation +' (", ?, \', /, \\) ',
						callback : function(value) {
							var regx=/^(?=.*[a-zA-Z])[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			roleCode : {
				validators : {
					stringLength: {
	                    max: 250,
	                    message: validationMsg.nameRoleAcronym
	                },
					callback : {
						message : columnMessages.roleAcronymValidation +' (", ?, \', /, \\)',
						callback : function(value) {
							var regx=/^(?=.*[a-zA-Z])[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			addRoleSequenceNumber : {
				validators : {
					callback : {
						message : ' ',
						callback : function(value) {
							
							var regx=/^[1-9][0-9]*$/;
							return regx.test(value);}
					}
				}
			},
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();
             addRoleData(e);
      
        });
	    
    $("#editRoleForm") .formValidation({
    	feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			roleName : {
				validators : {
					stringLength: {
	                    max: 250,
	                    message: validationMsg.nameMaxLength
	                },
					callback : {
						message : columnMessages.roleValidation +' (", ?, \', /, \\) ',
						callback : function(value) {
							var regx=/^(?=.*[a-zA-Z])[^'?\"/\\]*$/;
							
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
		
		roleCode : {
			validators : {
				stringLength: {
                    max: 250,
                    message:  validationMsg.nameRoleAcronym
                },
				callback : {
					message :  columnMessages.roleAcronymValidation +' (", ?, \', /, \\)',
					callback : function(value) {
						var regx=/^(?=.*[a-zA-Z])[^'?\"/\\]*$/;
						if(/\ {2,}/g.test(value))
							return false;
						return regx.test(value);}
				}
			}
		},
		editRoleSequenceNumber : {
			validators : {
				callback : {
					message : ' ',
					callback : function(value) {
					
						var regx=/^[1-9][0-9]*$/;
						return regx.test(value);}
				}
			}
		}
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();
             editData();
      
        });
});