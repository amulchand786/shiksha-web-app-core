var $table;
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		center 	: "#divSelectCenter",
		level	: "#divSelectLevel",
		batch	: "#divSelectBatch",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementCenter		: "#selectBox_centersByLoc",
		eleLevel			: "#selectBox_Level",
		eleBatch			: "#selectBox_Batch",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
		divFilter   		: '#divFilter',
		eleLocType  		: '#divSchoolLocType input:radio[name=locType]',
		filter				: '#locColspFilter',
		panelLink			: '#locationPnlLink',
		form				: '#listDeviceForm',
		eleCollapseFilter	: '#collapseFilter',
		eleToggleCollapse	: '[data-toggle="collapse"]',
		resetModal 			: '#mdlResetLoc',
		resetConfirm 		: '#btnResetLocYes',
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var locationUtility={
		fnInitLocation :function(){
			$(locationPanel.divFilter).css('display','none');
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(locationPanel.resetModal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			$(locationPanel.eleLevel).multiselect('destroy').multiselect('refresh');
			$(locationPanel.eleBatch).multiselect('destroy').multiselect('refresh');
			$(locationPanel.batch).hide();
			$(locationPanel.level).hide();
			enableMultiSelect($("#selectBox_Batch"));
			enableMultiSelect($("#selectBox_Level"));
			if ($(resetButtonObj).parents("form").attr("id") == 'mapDeviceForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divSchoolLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divSchoolLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
			}$(locationPanel.school).val()
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		displaySmartFilters : function(lCode,lvlName){			
				elementIdMap = {
						"State" 	: '#statesForZone',
						"Zone" 		: '#selectBox_zonesByState',
						"District" 	: '#selectBox_districtsByZone',
						"Tehsil" 	: '#selectBox_tehsilsByDistrict',
						"Block" 	: '#selectBox_blocksByTehsil',
						"City" 		: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" 	: '#selectBox_villagebyRv',
						"Center" 	: '#selectBox_centersByLoc'
				};
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));				
				locationUtility.reinitializeSmartFilter(elementIdMap);				
				var lLocTypeCode =$('#divSchoolLocType input:radio[name=locType]:checked').data('code');
				var lLLevelName =$('#divSchoolLocType input:radio[name=locType]:checked').data('levelname');				
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));				
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(
						function(ind, ele_key) {
							$(elementIdMap[ele_key]).find("option:selected").prop(
									'selected', false);
							$(elementIdMap[ele_key]).multiselect('destroy');
							enableMultiSelect(elementIdMap[ele_key]);
						});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#btnsRow,#divFilter').show();
				}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});			
			$('#rowDiv1,#divFilter').show();			
			spinner.hideSpinner();
		},		
};
var device = {	
		
		table : "#listDevices",
		tableContainer : "#tableContainer",
		init:function(){
			$(locationPanel.resetConfirm).on('click',locationUtility.doResetLocation);
			$(locationPanel.elementCenter).on('change',this.showLevels);
			$(locationPanel.eleLevel).on('change',this.showBatches);
			$(locationPanel.eleLocType).change(function() {				
					$(locationPanel.filter).removeClass('in');
					$(locationPanel.panelLink).text(selectLocation);
					$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
					$(locationPanel.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
					var typeCode =$(this).data('code');				
					if(typeCode.toLowerCase() =='U'.toLowerCase()){
						showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
						hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
						locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
						positionFilterDiv("U");
						fnCollapseMultiselect();
					}else{
						hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
						showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
						locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
						positionFilterDiv("R");
						fnCollapseMultiselect();						
					}
					$(locationPanel.eleCollapseFilter).css('display','block');
					$(locationPanel.level).hide();
					$(locationPanel.batch).hide();
			});
		},
		showLevels : function(){
			device.fnGetLevelsByCenter($(locationPanel.eleLevel),0,$(locationPanel.elementCenter).val());
			setOptionsForMultipleSelect(locationPanel.eleLevel);
			$(locationPanel.eleLevel).multiselect("rebuild");
			$(locationPanel.level).show();
		},
		fnGetLevelsByCenter : function(elementId,selectedId,centerId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("/level/list/center/"+centerId+"/device/"+mapDevice.deviceId, null, "GET");
			if(responseData != null){
				var row = '<option value="0">None Selected</option>';
				$.each(responseData, function(index,obj){
						if(obj.levelId == selectedId){
							row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'" selected>'+obj.levelName+'</option>';
						}
						else{
							row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'">'+obj.levelName+'</option>';
						}
							$(elementId).append(row);
					})
				spinner.hideSpinner();
			}
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		showBatches : function(){
			device.fnGetBatchesByCenterAndLevel($(locationPanel.eleBatch),0,$(locationPanel.elementCenter).val(),$(locationPanel.eleLevel).val());
			setOptionsForMultipleSelect(locationPanel.eleBatch);
			$(locationPanel.eleBatch).multiselect("rebuild");
			$(locationPanel.batch).show();
		},
		fnGetBatchesByCenterAndLevel : function(elementId,selectedId,centerId,levelId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("/batch/center/"+centerId+"/level/"+levelId+"/device/"+mapDevice.deviceId, null, "GET");
			if(responseData){
				$.each(responseData, function(index,obj){
					$(elementId).append('<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'">'+obj.batchName+'</option>');
				})
				spinner.hideSpinner();
			}
		},
		filterDevicesData : function(){
			//$(device.tableContainer).show();
			//$(device.table).bootstrapTable("refresh");
		},
		fiterQueryParmams : function(){
			var centerId = $(locationPanel.elementCenter).val() ? $(locationPanel.elementCenter).val() : 0;
			var levelId  = $(locationPanel.eleLevel).val() ? $(locationPanel.eleLevel).val() : 0;
			var batchId = $(locationPanel.eleBatch).find("option:selected").data("id") ? $(locationPanel.eleBatch).find("option:selected").data("id") : 0;
			var paramsTemp = {};
			paramsTemp.center = centerId;
			paramsTemp.level  = levelId;
			paramsTemp.batch = batchId;
			return paramsTemp;
		},
		
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		formValidate : function(validateFormName){
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				rules: {
					deviceType : {
						required : true
					},
					macId : {
						required : true,
						macid	 : true,
					},
					imeiId : {
						required : true
					},
					startDate:{
						required : true
					},
					endDate:{
						required : true
					},
					make : {
						required : true
					},
					model : {
						required : true
					},
					os : {
						required : true
					},
					serialNumber : {
						required : true
					},
					assetNumber : {
						required : true
					},
					desktopAppVersion : {
						required : true
					},
					tabletContentAppVersion : {
						required : true
					},
					tabletAssessmentAppVersion : {
						required : true
					}
				},
				messages: {
					deviceType : {
						required : validationMsg.deviceType
					},
					macId : {
						required : validationMsg.macId,
						macid	 : validationMsg.validMACId
					},
					imeiId : {
						required : validationMsg.imeiId
					},
					
					
					startDate:{
						required : validationMsg.startDate
					},
					endDate:{
						required : validationMsg.endDate
					},
					
					make : {
						required : validationMsg.make
					},
					model : {
						required : validationMsg.model
					},
					os : {
						required : validationMsg.os
					},
					serialNumber : {
						required : validationMsg.serialNumber
					},
					assetNumber : {
						required : validationMsg.assetNumber
					},
					desktopAppVersion : {
						required : validationMsg.desktopAppVersion
					},
					tabletContentAppVersion : {
						required : validationMsg.tabletContentAppVersion
					},
					tabletAssessmentAppVersion : {
						required : validationMsg.tabletAssessmentAppVersion
					}
				},
				errorPlacement: function(error, element) {
				    if($(element).hasClass("select")){
				    	$(element).parent().append(error);
				    }else  if($(element).hasClass("datepicker")){
				    	$(element).parent().parent().append(error);
				    } else {
				    	$(element).after(error);
				    }
				},
				submitHandler : function(){
					if(validateFormName === addDevice.addForm){
						addDevice.fnAdd(submitActor.id);
					}
					if(validateFormName === editDevice.editForm){
						editDevice.fnEdit();
					}
				}
			});
		},
		actionFormater : function(value, row, index){
			
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"]){
					action+='<li><a class="edit-device-btn" data-deviceid="'+row.deviceId+'" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
					action+='<li><a class="map-device-btn"  data-islocmapped="'+row.isLocationMapped+'" data-locmapperid="'+row.locationMapperId+'" data-deviceid="'+row.deviceId+'" href="javascript:void(0)">'+deviceMessages.mapCenter+'</a></li>';
				}
				if(permission["delete"]){
					row.deviceLocationMappers.length ?  action+='<li><a class="unmap-device-btn" data-deviceid="'+row.deviceId+'" href="javascript:void(0)" >'+deviceMessages.unmap+'</a></li>' : "";
					action+='<li><a class="delete-device-btn"  data-deviceid="'+row.deviceId+'" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				}
				if(permission["emailLicense"]){
					action+='<li><a class="key-device-btn" data-deviceid="'+row.deviceId+'" href="javascript:void(0)">'+validationMsg.emailLicenceKey+'</a></li>';	
					action+='<li><a class="view-license-btn" data-devicetype="'+row.deviceType+'  "data-imeiid="'+row.imeiId+'  "data-macid="'+row.macId+'  "data-encodedmacid="'+row.encodedMacId+' "data-deviceid="'+row.deviceId+'" href="javascript:void(0)">'+validationMsg.viewLicense+'</a></li>';	
					}
				action+='</ul>';
			}
			return action;
		},
		cellFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
};
var addDevice = {
		addDeviceBtn : "#addDeviceBtn",
		addModal : "#mdlAddDevice",
		addForm : "#addDeviceForm",
		addDeviceType : "#addDeviceType",
		addMacId : "#addMacId",
		addIMEIId : "#addIMEIId",
		addMake : "#addMake",
		addModel : "#addModel", 
		addOS : "#addOS",
		addSerialNo : "#addSerialNo",
		addAssetNo : "#addAssetNo",
		addDesktopVersion : "#addDesktopVersion",
		addContentVersion : "#addContentVersion",
		addAssessmentVersion : "#addAssessmentVersion",
		addMackIdDiv : "#addMackIdDiv",
		addIMEIDiv : "#addIMEIDiv",
		addMoreBtn : "#addShikshaPlusDeviceSaveMoreButton",
		eleStartDate   	: '#addStartDate',
		eleEndDate	   	: '#addEndDate',
		
		
		init : function(){
			device.formValidate(this.addForm);	
			$(this.addDeviceBtn).on("click",this.initAdd);
			$(this.addDeviceType).on("change",function(){
				if($(this).val() == "DESKTOP"){
					$(addDevice.addIMEIId).val("");
					$(addDevice.addMackIdDiv).show();
					$(addDevice.addIMEIDiv).hide();
				} else {
					 $(addDevice.addMacId).val("");
					$(addDevice.addMackIdDiv).hide();
					$(addDevice.addIMEIDiv).show();
				}
			});
		},
		initAdd : function(){
			fnInitSaveAndAddNewList();
			device.resetForm(addDevice.addForm);
			device.resetForm(addDevice.addForm);
			$(addDevice.addModal).modal("show");
			
			var start = new Date(); 
			var end = new Date();
			$('.datepicker').datepicker("remove");
			$('.datepicker').datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start
			});
			$(addDevice.eleStartDate).datepicker({
				startDate : start,
				endDate   : end
			}).on('hide', function(){				
				$(addDevice.eleEndDate).val("");
				$(addDevice.eleEndDate).datepicker('setStartDate', $(this).val());
				
			});
			
		},
		fnAdd : function(submitBtnId){
			var ajaxData = {
					deviceType 	: $(addDevice.addDeviceType).val(),
					macId 		: $(addDevice.addMacId).val(),
					imeiId		: $(addDevice.addIMEIId).val(),
					make		: $(addDevice.addMake).val(),
					model 		: $(addDevice.addModel).val(),
					os 			: $(addDevice.addOS).val(),
					serialNumber: $(addDevice.addSerialNo).val(),
					assetNumber : $(addDevice.addAssetNo).val(),
					desktopAppVersion 			: $(addDevice.addDesktopVersion).val(),
					tabletContentAppVersion 	: $(addDevice.addContentVersion).val(),
					tabletAssessmentAppVersion 	: $(addDevice.addAssessmentVersion).val(),
					startDate:$(addDevice.eleStartDate).val(),
					endDate:$(addDevice.eleEndDate).val(),
					
			};
			spinner.showSpinner();
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/device/", ajaxData, "POST");
		
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					$(device.table).bootstrapTable("refresh");
					if(submitBtnId == "addDeviceSaveButton"){
					$(addDevice.addModal).modal("hide");
						AJS.flag({
							type : "success",
							title : appMessgaes.success,
							body : addAjaxCall.responseMessage,
							close :'auto'
						});				
					} else {
						$(addDevice.addForm)[0].reset();
						$(addDevice.addModal).find("#divSaveAndAddNewMessage").show();
						var name = addAjaxCall.macId ? addAjaxCall.macId : addAjaxCall.imeiId;
						fnDisplaySaveAndAddNewElementAui(addDevice.addModal,name);
					}
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : addAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			
			spinner.hideSpinner();
		}
		
};


var editDevice = {
		editDeviceBtn : ".edit-device-btn",
		editModal : "#mdlEditDevice",
		editForm : "#editDeviceForm",
		editDeviceType : "#editDeviceType",
		editMacId : "#editMacId",
		editIMEIId : "#editIMEIId",
		editMake : "#editMake",
		editModel: "#editModel",
		editOS : "#editOS",
		editSerialNo : "#editSerialNo",
		editAssetNo : "#editAssetNo",
		editDesktopVersion : "#editDesktopVersion",
		editContentVersion : "#editContentVersion",
		editAssessmentVersion : "#editAssessmentVersion",
		editMackIdDiv : "#editMackIdDiv",
		editIMEIDiv : "#editIMEIDiv",
		editDeviceId : "#editDeviceId",
		
		eleStartDate   	: '#editStartDate',
		eleEndDate	   	: '#editEndDate',
		
		init : function(){
			device.formValidate(this.editForm);	
			$(document).on("click",this.editDeviceBtn,this.initEdit);
			
			$(this.editDeviceType).on("change",function(){
				if($(this).val() == "DESKTOP"){
					$(editDevice.editMackIdDiv).show();
					$(editDevice.editIMEIDiv).hide();
					$(editDevice.editIMEIId).val("");
				} else {
					$(editDevice.editMackIdDiv).hide();
					$(editDevice.editIMEIDiv).show();
					$(editDevice.editMacId).val("");
				}
			});
		},
		initEdit : function(){
			device.resetForm(editDevice.editForm);
			$(editDevice.editForm)[0].reset();
			
			
			
			var start = new Date(); 
			var end = new Date();
			$('.datepicker').datepicker({
				format: 'dd-M-yyyy',
				todayHighlight: true,
				autoclose: true,
				startDate:start
			});
			$(editDevice.eleStartDate).datepicker({
				startDate : start,
				endDate   : end
			}).on('hide', function(){				
				$(editDevice.eleEndDate).val("");
				$(editDevice.eleEndDate).datepicker('setStartDate', $(this).val());
				
			});
			
			
			var deviceId = $(this).data("deviceid");
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/device/"+deviceId, null, "GET");

			$(editDevice.editDeviceId).val(getAjaxCall.deviceId);
			$(editDevice.editDeviceType).val(getAjaxCall.deviceType);
			$(editDevice.editMacId).val(getAjaxCall.macId);
			$(editDevice.editIMEIId).val(getAjaxCall.imeiId);
			$(editDevice.editMake).val(getAjaxCall.make);
			$(editDevice.editModel).val(getAjaxCall.model);
			$(editDevice.editOS).val(getAjaxCall.os);
			$(editDevice.editSerialNo).val(getAjaxCall.serialNumber);
			$(editDevice.editAssetNo).val(getAjaxCall.assetNumber);
			$(editDevice.editDesktopVersion).val(getAjaxCall.desktopAppVersion);
			$(editDevice.editContentVersion).val(getAjaxCall.tabletContentAppVersion);
			$(editDevice.editAssessmentVersion).val(getAjaxCall.tabletAssessmentAppVersion);
			$(editDevice.eleStartDate).val(getAjaxCall.startDate);
			$(editDevice.eleEndDate).val(getAjaxCall.endDate);
			
			if(getAjaxCall.deviceType == "DESKTOP"){
				$(editDevice.editMackIdDiv).show();
				$(editDevice.editIMEIDiv).hide();
			} else {
				$(editDevice.editMackIdDiv).hide();
				$(editDevice.editIMEIDiv).show();
			}
			
			spinner.hideSpinner();
			$(editDevice.editModal).modal("show");
		},
		fnEdit : function(){
			var ajaxData = {
					
					deviceId 	: $(editDevice.editDeviceId).val(),
					deviceType 	: $(editDevice.editDeviceType).val(),
					macId 		: $(editDevice.editMacId).val(),
					imeiId		: $(editDevice.editIMEIId).val(),
					make		: $(editDevice.editMake).val(),
					model 		: $(editDevice.editModel).val(),
					os 			: $(editDevice.editOS).val(),
					serialNumber: $(editDevice.editSerialNo).val(),
					assetNumber : $(editDevice.editAssetNo).val(),
					desktopAppVersion 			: $(editDevice.editDesktopVersion).val(),
					tabletContentAppVersion 	: $(editDevice.editContentVersion).val(),
					tabletAssessmentAppVersion 	: $(editDevice.editAssessmentVersion).val(),
					startDate:$(editDevice.eleStartDate).val(),
					endDate:$(editDevice.eleEndDate).val(),
					/*center : {
						id :  parseInt($(locationPanel.elementCenter).val()),
					},
					level : {
						levelId :  parseInt($(locationPanel.eleLevel).val()),
					},
					batch : {
						batchId :  parseInt($(locationPanel.eleBatch).find("option:selected").data("id")),
					},*/
					
			};
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/device/", ajaxData, "PUT");
			
			if(editAjaxCall != null){
				if(editAjaxCall.response == "shiksha-200"){
					$(device.table).bootstrapTable("refresh");
					$(editDevice.editModal).modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : editAjaxCall.responseMessage,
						close :'auto'
					})		
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : editAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			
			spinner.hideSpinner();
			
			style="display: block; float: right; margin-top: 25px; height: 30px;"	
		}
		
};

var mapDevice = {
		mapDeviceBtn : ".map-device-btn",
		mapModal : "#mdlMapDevice",
		mapForm : "#mapDeviceForm",
		mapDeviceId : "#mapDeviceId",
		unMapSectionBtn : ".unmap-device",
		
		deviceId:'',
		isLocationMapped:false,
		locationMapperId:0,
		
		init : function(){	
			$(document).on("click",this.mapDeviceBtn,this.initMap);
			$(document).on("click",this.unMapSectionBtn,this.initUnampSection);
			$(this.mapForm).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					centerId : {
						required : true
					},
					levelId : {
						required : true
					},
					batchId : {
						required : true
					}
				},
				messages: {
					centerId : {
						required : deviceMessages.centerRequired,
					},
					levelId : {
						required : deviceMessages.levelRequired,
					},
					batchId : {
						required : deviceMessages.batchRequired,
					}
				},
				errorPlacement: function(error, element) {
				    if($(element).hasClass("select")){
				    	$(element).parent().append(error);
				    } else {
				    	$(element).after(error);
				    }
				},
				submitHandler : function(){
					mapDevice.fnMap();
				}
			});
		},
		initMap : function(){
			spinner.showSpinner();
			$(mapDevice.mapForm)[0].reset();
			device.resetForm(mapDevice.mapForm);
			mapDevice.deviceId = $(this).data("deviceid");
			mapDevice.isLocationMapped =$(this).data("islocmapped");
			mapDevice.locationMapperId =$(this).data("locmapperid");
			
			var getAjaxCall = shiksha.invokeAjax("shikshaplus/device/"+mapDevice.deviceId, null, "GET");
			if(getAjaxCall != null){
				if(getAjaxCall.response == "shiksha-200"){
					$(mapDevice.mapModal).modal("show");
					spinner.showSpinner();
					locationUtility.doResetLocation();
					$("#tblMappedBatches tbody").empty();
					$(locationPanel.divFilter).css('display','none');
					$.each($(locationPanel.eleLocType),function(i,ele){$(ele).prop('checked',false);});
					if(getAjaxCall.deviceLocationMappers.length){
							mapDevice.fnEditLocation(getAjaxCall);
							mapDevice.showMappedSections(getAjaxCall);
					}
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : getAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		fnMap : function(){
			var ajaxData = {
					
					locationMapperId:mapDevice.locationMapperId,
					deviceId 	: mapDevice.deviceId,
					center : {
						id :  parseInt($(locationPanel.elementCenter).val()),
					},
					deviceLocationMappers : [{
						levelId : parseInt($(locationPanel.eleLevel).val()),
						batchIds : []
					}]
					
			};
			$.each($(locationPanel.eleBatch).val(),function(x,batch){
				if(batch != "multiselect-all"){
					ajaxData.deviceLocationMappers[0]['batchIds'].push(batch);
				}
			});
			spinner.showSpinner();
			var mapAjaxCall = shiksha.invokeAjax("shikshaplus/device/location", ajaxData, "POST");
			
			if(mapAjaxCall != null){
				if(mapAjaxCall.response == "shiksha-200"){
					$(device.table).bootstrapTable("refresh");
					mapDevice.showMappedSections(mapAjaxCall)
					$(locationPanel.elementCenter).trigger('change');
					$(locationPanel.batch).hide();
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : mapAjaxCall.responseMessage,
						close :'auto'
					})
					device.refreshTable(device.table);
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : mapAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}
			spinner.hideSpinner();	
		},
		fnEditLocation : function(deviceLoc){
			spinner.showSpinner();
			console.log(deviceLoc);
			var center 	=deviceLoc.center;
			var level 	=deviceLoc.level;
			var batch	=deviceLoc.batch;
			var locCode =center.locationType.code;
			$(locationPanel.divFilter).css('display','block');
			if(locCode=='R'){
				var idMap = {
						'#statesForZone'				:center.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.zone.state.stateId,
						'#selectBox_zonesByState'		:center.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.zone.zoneId,
						'#selectBox_districtsByZone'	:center.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.district.districtId,
						'#selectBox_tehsilsByDistrict'	:center.village.revenueVillage.gramPanchayat.nyayPanchayat.block.tehsil.tehsilId,
						'#selectBox_blocksByTehsil'		:center.village.revenueVillage.gramPanchayat.nyayPanchayat.block.blockId,
						'#selectBox_cityByBlock'		:'',
						'#selectBox_npByBlock'			:center.village.revenueVillage.gramPanchayat.nyayPanchayat.nyayPanchayatId,
						'#selectBox_gpByNp'				:center.village.revenueVillage.gramPanchayat.gramPanchayatId,
						'#selectBox_revenueVillagebyGp'	:center.village.revenueVillage.revenueVillageId,
						'#selectBox_villagebyRv'		:center.village.villageId,
						'#selectBox_centersByLoc'		:center.id
				};
			} else {
				var idMap = {
						'#statesForZone'				:center.city.tehsil.district.zone.state.stateId,
						'#selectBox_zonesByState'		:center.city.tehsil.district.zone.zoneId,
						'#selectBox_districtsByZone'	:center.city.tehsil.district.districtId,
						'#selectBox_tehsilsByDistrict'	:center.city.tehsil.tehsilId,
						'#selectBox_blocksByTehsil'		:'',
						'#selectBox_cityByBlock'		:center.city.cityId,
						'#selectBox_npByBlock'			:'',
						'#selectBox_gpByNp'				:'',
						'#selectBox_revenueVillagebyGp'	:'',
						'#selectBox_villagebyRv'		:'',
						'#selectBox_centersByLoc'		:center.id
				};
			}
						
			$.each($(locationPanel.eleLocType),function(i,ele){
				if($(ele).data('code')==locCode){
					$(ele).trigger('change');
					$(ele).prop('checked',true);
				}
			});
			var ele_keys = Object.keys(idMap);
			$(ele_keys).each(function(ind, ele_key){
				$(ele_key).multiselect('destroy');
				if(idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value="+idMap[ele_key]+"]").prop('selected', true);		
				enableMultiSelect(ele_key);
			});
			
			device.showLevels();
			/*$(locationPanel.eleLevel).val(level.levelId);
			$(locationPanel.eleLevel).trigger('change');
			device.showBatches();
			$(locationPanel.eleBatch).val(batch.batchId);
			$(locationPanel.eleBatch).trigger('change');*/
		
			spinner.hideSpinner();
		},
		showMappedSections : function(deviceDetails){
			$("#tblMappedBatches tbody").empty();
			if(deviceDetails.deviceLocationMappers.length){
				$.each(deviceDetails.deviceLocationMappers, function(x, level){
					$.each(level.batches, function(y, batch){
						$("#tblMappedBatches tbody").append('<tr data-centerid="'+deviceDetails.center.id+'" data-levelid="'+level.level.levelId+'" data-batchid="'+batch.batchId+'"><td>'+level.level.levelName+'</td><td>'+batch.batchName+'</td><td><div class=""><a class="tooltip tooltip-top unmap-device"><span class="tooltiptext">Unmap</span><span class="glyphicon glyphicon-remove font-size12"></span></a></div></td><tr>');
					})
				})
			}
			
		},
		initUnampSection : function(){
			mapDevice.unampSectionBtn = $(this);
			$("#mdlWarningUnamp").modal("show");
		},
		fnUnampSection :function(){
			var centerId 	= mapDevice.unampSectionBtn.closest("tr").attr("data-centerid");
			var levelId  	= mapDevice.unampSectionBtn.closest("tr").attr("data-levelid");
			var batchId 	= mapDevice.unampSectionBtn.closest("tr").attr("data-batchid");
			
			spinner.showSpinner();
			var unMapAjaxCall = shiksha.invokeAjax("/shikshaplus/device/"+mapDevice.deviceId+"/location/center/"+centerId+"/level/"+levelId+"/batch/"+batchId+"/unmap", null, "PUT");

			if(unMapAjaxCall != null){
				if(unMapAjaxCall.response == "shiksha-200"){
					$("#mdlWarningUnamp").modal("hide");
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : unMapAjaxCall.responseMessage,
						close :'auto'
					});
					mapDevice.showMappedSections(unMapAjaxCall);
					$(locationPanel.elementCenter).trigger('change');
					$(locationPanel.batch).hide();
					$(device.table).bootstrapTable("refresh");
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : unMapAjaxCall.responseMessage,
						close :'auto'
					});
				}
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.serverError,
					close :'auto'
				})
			}

			spinner.hideSpinner();	
		}
};

var deleteDevice = {
	deleteDeviceBtn : ".delete-device-btn",
	deleteModal : "#mdlDeleteDevice",
	deleteDeviceId : "#deleteDeviceId",
	deleteConfirmBtn : "#deleteDeleteButton",
	init : function(){
		$(document).on("click",this.deleteDeviceBtn,this.initDelete);
		$(document).on("click",this.deleteConfirmBtn,this.fnDelete);
	},
	initDelete :function(){
		var deviceId = $(this).data("deviceid");
		$(deleteDevice.deleteDeviceId).val(deviceId);
		$(deleteDevice.deleteModal).modal("show");																																																					
	},
	fnDelete : function(){
				
	var deviceId 	=  $(deleteDevice.deleteDeviceId).val();

	var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/device/"+deviceId, null, "DELETE");
	
	if(deleteAjaxCall != null){
		if(deleteAjaxCall.response == "shiksha-200"){
			$(device.table).bootstrapTable("refresh");
			$(deleteDevice.deleteModal).modal("hide");
			AJS.flag({
				type : "success",
				title : appMessgaes.success,
				body : deleteAjaxCall.responseMessage,
				close :'auto'
			})				
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.error,
				body : deleteAjaxCall.responseMessage,
				close :'auto'
			});
		}
	} else {
		AJS.flag({
			type : "error",
			title : appMessgaes.oops,
			body : appMessgaes.serverError,
			close :'auto'
		})
	}
	
	spinner.hideSpinner();
	}
};

var unmapDevice = {
		unmapDeviceBtn : ".unmap-device-btn",
		unmapModal : "#mdlUnmapDevice",
		unmapDeviceMapperId : "#unmapDeviceMapperId",
		unmapConfirmBtn : "#unmapDeviceButton",
		init : function(){
			$(document).on("click",this.unmapDeviceBtn,this.initUnmap);
			$(document).on("click",this.unmapConfirmBtn,this.fnUnmap);
		},
		initUnmap :function(){
			var deviceId = $(this).data("deviceid");
			$(unmapDevice.unmapDeviceMapperId).val(deviceId);
			$(unmapDevice.unmapModal).modal("show");																																																					
		},
		fnUnmap : function(){
		var deviceId =  $(unmapDevice.unmapDeviceMapperId).val();

		var unampAjaxCall = shiksha.invokeAjax("shikshaplus/device/"+deviceId+"/location/unmap", null, "PUT");
		
		if(unampAjaxCall != null){
			if(unampAjaxCall.response == "shiksha-200"){
				$(device.table).bootstrapTable("refresh");
				$(unmapDevice.unmapModal).modal("hide");
				AJS.flag({
					type : "success",
					title : appMessgaes.success,
					body : unampAjaxCall.responseMessage,
					close :'auto'
				})				
			} else {
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : unampAjaxCall.responseMessage,
					close :'auto'
				});
			}
		} else {
			AJS.flag({
				type : "error",
				title : appMessgaes.oops,
				body : appMessgaes.serverError,
				close :'auto'
			})
		}
		
		spinner.hideSpinner();
		}
	};

var viewLicense={

		licenceViewBtn 	: ".view-license-btn",
		modal 		: "#mdlViewDeviceLicence",
		macIdTxt            :"#macId",
		imeiIdText      :"#imeiId",
		encodedMacIdText    :"#encodedMacId",
		init : function(){
			
			$(document).on("click",this.licenceViewBtn,this.initViewLicence);
		},
		initViewLicence :function(){
			$(viewLicense.macIdTxt).text("");
			$(viewLicense.imeiIdText).text("");
			$(viewLicense.encodedMacIdText).val("");
			var deviceType=$(this).data("devicetype").trim();
			if(deviceType=="DESKTOP"){
				$('#tabletDiv').hide()
				$('#desktopDiv').show();
				
			$(viewLicense.macIdTxt).text($(this).data("macid"));
		
			}else if(deviceType=="TABLET"){
				$('#desktopDiv').hide()
				$('#tabletDiv').show()
				$(viewLicense.imeiIdText).text($(this).data("imeiid"));
			}
			$(viewLicense.encodedMacIdText).val($(this).data("encodedmacid"));
            $(viewLicense.modal).modal("show");																																																					
		},
}
var emailLicense={
		licenceDeviceBtn 	: ".key-device-btn",
		licenceModal 		: "#mdlDeviceLicence",
		licenceDeviceId 	: "#licenceDeviceId",
		btnSend				: "#btnSend",



		eleRole		:"#selectBox_role",
		eleUser		:"#selectBox_user",
		eleEmail	:"#email",
		roleDiv		:"#roleDiv",
		userDiv		:"#userDiv",
		emailDiv	:"#emailDiv",

		user		: '',
		device		: '',

		init : function(){
			$(document).on("click",this.licenceDeviceBtn,this.initEmailLicence);
			$(emailLicense.eleRole).on('change',this.showUsers);
			$(emailLicense.eleUser).on('change',this.showEmail);
			$(emailLicense.btnSend).on('click',this.sendEmail);

			emailLicense.user ='';
			emailLicense.device ='';

		},
		initEmailLicence :function(){

			emailLicense.device =$(this).data("deviceid");						
			emailLicense.showRoles();
			$(emailLicense.licenceModal).modal("show");																																																					
		},
		showRoles : function(){
			emailLicense.fnGetRoles($(emailLicense.eleRole));
			setOptionsForMultipleSelect(emailLicense.eleRole);
			$(emailLicense.eleRole).multiselect("rebuild");
			$(emailLicense.userDiv).hide();
			$(emailLicense.emailDiv).hide();
			$(emailLicense.roleDiv).show();
			$(emailLicense.btnSend).prop("disabled",true);

		},
		showUsers : function(){
			var flag =emailLicense.fnGetUsers($(emailLicense.eleUser),$(emailLicense.eleRole).val());
			if(!flag){
				setOptionsForMultipleSelect(emailLicense.eleUser);
				$(emailLicense.eleUser).multiselect("rebuild");
				$(emailLicense.emailDiv).hide();
				$(emailLicense.userDiv).show();					
			}
			$(emailLicense.btnSend).prop("disabled",true);

		},
		showEmail : function(){
			$(emailLicense.eleEmail).val('');
			$(emailLicense.emailDiv).show();
			var email =$(this).find('option:selected').data('email');
			if(email.lastIndexOf('@')!=-1){
				$(emailLicense.eleEmail).val(email);
				emailLicense.user =$(this).find('option:selected').val();
				$(emailLicense.btnSend).prop("disabled",false);
			}else{
				$(emailLicense.btnSend).prop("disabled",true);
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : validationMsg.emailRequired,
					close :'auto'
				});
			}
		},

		sendEmail : function(){
			spinner.showSpinner();
			var sendResopnse =shiksha.invokeAjax("device/license/"+emailLicense.device+"/user/"+emailLicense.user, null, "POST");
			if(sendResopnse != null){
				if(sendResopnse.response == "shiksha-200"){
					AJS.flag({
						type : "success",
						title : appMessgaes.success,
						body : sendResopnse.responseMessage,
						close :'auto'
					})	;
					$(emailLicense.licenceModal).modal("hide");
				} else {
					AJS.flag({
						type : "error",
						title : appMessgaes.error,
						body : sendResopnse.responseMessage,
						close :'auto'
					});
				}
				spinner.hideSpinner();
			}
		},



		fnGetRoles : function(elementId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("role/list",null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.roleId+'" data-id="'+object.roleId+'"data-name="'+object.roleName+'">'+object.roleName+'</option>';
					$(elementId).append(row);
				})


			}
			spinner.hideSpinner();
		},
		fnGetUsers : function(elementId,roleId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("user/role/"+roleId,null, "GET");

			if(responseData!=null?responseData.length==0:false){
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : validationMsg.noUserForRole,
					close :'auto'
				});
				$(emailLicense.emailDiv).hide();
				$(emailLicense.userDiv).hide();
				spinner.hideSpinner();
				return false;
			}
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.id+'" data-id="'+object.id+' " data-email="'+object.email+' "data-name="'+object.userName+'">'+object.userName+'</option>';
					$(elementId).append(row);
				})
			}
			spinner.hideSpinner();
		},


};

var submitActor = null;
var $submitActors = $(addDevice.addForm).find('button[type=submit]');

$( document ).ready(function() {
	device.init();
	addDevice.init();
	editDevice.init();
	deleteDevice.init();
	mapDevice.init();
	unmapDevice.init();
	emailLicense.init();
	viewLicense.init();
	$(device.table).bootstrapTable({
		toolbar: "#deviceToolBar",
		url : "shikshaplus/device/page",
		method : "post",
		search: true,
		sidePagination: "server",
		showToggle: false,
		showColumns: true,
		pagination: true,
		pageSize: 50,
		clickToSelect: false,
		//queryParamsType :'limit',
		//queryParams : device.fiterQueryParmams
	});
	$("#deviceToolBar").show();
	
	$submitActors.click(function() {
		submitActor = this;
	});
	$(document).on("keypress",".numberonly",function (e) {
        //if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 9 && e.which != 0 && (e.which < 48 || e.which > 57) && !e.ctrlKey && (e.which < 96 || e.which > 105)) {
            return false;
        }
    });
	
	
	$(addDevice.eleStartDate).on('changeDate', function(){
		$(addDevice.eleEndDate).datepicker('setStartDate', $(this).val());
	});
	jQuery.validator.addMethod("macid", function(value) { 
		return /^([0-9A-Fa-f]{2}[\.:-]){5}([0-9a-fA-F]{2})$/.test(value); 
	}, "");
	
});