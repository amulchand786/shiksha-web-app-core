var pageContextElements = {
		divFilter   :'#divFilter',
		eleLocType  : '#divCenterLocType input:radio[name=locType]',
		filter		: '#locColspFilter',
		panelLink	: '#locationPnlLink',
		form		: '#listCenterForm',
		eleCollapseFilter		:'#collapseFilter',
		eleToggleCollapse		: '[data-toggle="collapse"]',
		clearBtn	 : '#btnResetLocation',
		levelDiv	 : '#divSelectLevel',
		batchDiv	 : '#divSelectBatch',
		yearDiv                 :'#divSelectYear',
		academicYearDiv : '#divSelectAcademicYear',
		btnDiv		 : "#viewLearnerDiv",
		learnerDiv  : '#viewLearner',
		learnerBtn	: '#viewLearnerBtn',
		tableDiv	: '#rowDiv2',
		table		: '#tableDiv',
		learnerTable: "#bulkRollNoUpdateTable",
		promoteDiv  :'#levelPromotion',
		promoteBtn	: '#learnerPromotionBtn',
		
};
var selectBox ={
	center		: '#selectBox_centersByLoc',
	level		: '#selectBox_Level',
	batch		: '#selectBox_Batch',
	academicYear: '#selectBox_AcademicYear',
	year		: '#selectBox_year'
};
var reset = {
	modal : '#mdlResetLoc',
	confirm : '#btnResetLocYes',
};
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		level 				: "#selectBox_Level",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter(
				$(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter(
				$(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter(
				$(locationPanel.city));
	}
};
var learners=[];
var promotion ={
	modal :'#mdlPromoteLearner',
	form  :'#learnerPromotionForm',
	fromAC: '#fromAC',
	toAC  : '#toAC',
	fromLevel : '#fromLevel',
	toLevel   : '#toLevel',
	batchDiv  : '#batchDiv',
	toBatch   : '#toBatch',
	confirm	  : '#promoteButton'
};
var resetButtonObj;
var elementIdMap;
var $table;
var locationUtility={
		fnInitLocation :function(){
			$(pageContextElements.divFilter).css('display','none');
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(reset.modal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},
		
		doResetLocation : function() {
			
			$(selectBox.level).multiselect('destroy').multiselect('refresh');
			$(selectBox.batch).multiselect('destroy').multiselect('refresh');
			$(selectBox.academicYear).multiselect('destroy').multiselect('refresh');
			$(pageContextElements.levelDiv).hide();
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
			$(pageContextElements.btnDiv).hide();
			$(pageContextElements.promoteDiv).hide();
			enableMultiSelect($(selectBox.level));
			enableMultiSelect($(selectBox.batch));
			deleteAllRow("studentTable");
			if ($(resetButtonObj).parents("form").attr("id") == 'listCenterForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);
				
			}
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},
		
		//displaying robo-filters
		displaySmartFilters : function(lCode,lvlName){
			
				elementIdMap = {
						"State" 	: '#statesForZone',
						"Zone" 		: '#selectBox_zonesByState',
						"District" 	: '#selectBox_districtsByZone',
						"Tehsil" 	: '#selectBox_tehsilsByDistrict',
						"Block" 	: '#selectBox_blocksByTehsil',
						"City" 		: '#selectBox_cityByBlock',
						"Nyaya Panchayat" 	: '#selectBox_npByBlock',
						"Gram Panchayat" 	: '#selectBox_gpByNp',
						"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
						"Village" : '#selectBox_villagebyRv',
						"Center" : '#selectBox_centersByLoc'
				};
				
				displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));
				
				locationUtility.reinitializeSmartFilter(elementIdMap);
				
				var lLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var lLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				
				displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));
				
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(
						function(ind, ele_key) {
							$(elementIdMap[ele_key]).find("option:selected").prop(
									'selected', false);
							$(elementIdMap[ele_key]).multiselect('destroy');
							enableMultiSelect(elementIdMap[ele_key]);
						});
				setTimeout(function() {
					$(".outer-loader").hide();
					$('#rowDiv1,#divFilter').show();
				}, 100);
				//
			
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});
			
			$('#rowDiv1,#divFilter').show();
			
			spinner.hideSpinner();
		},
		
};
var learnerLevelPromotion = {
		
		fnInit:function(){
			$(reset.confirm).on('click',locationUtility.doResetLocation);
			$(selectBox.center).on('change',learnerLevelPromotion.showLevels);
			$(selectBox.level).on('change',learnerLevelPromotion.showBatches);
			$(selectBox.batch).on('change',learnerLevelPromotion.showAcademicCycles);
			$(pageContextElements.learnerBtn).on('click',learnerLevelPromotion.showTable);
			$("#bulkRollNoUpdateTable").on('check.bs.table',learnerLevelPromotion.checkboxChecked);
			$("#bulkRollNoUpdateTable").on('uncheck.bs.table',learnerLevelPromotion.checkboxUnChecked);
			$("#bulkRollNoUpdateTable").on('check-all.bs.table',learnerLevelPromotion.checkboxCheckedAll);
			$("#bulkRollNoUpdateTable").on('uncheck-all.bs.table',learnerLevelPromotion.checkboxUnCheckedAll);
			$(pageContextElements.promoteBtn).on('click',learnerLevelPromotion.fnGetPromotionData);
			$(promotion.toLevel).on('change',learnerLevelPromotion.showBatchDiv);
			$(selectBox.academicYear).on('change',learnerLevelPromotion.showViewBtn);
			$(pageContextElements.eleLocType).change(function() {				
				$(pageContextElements.filter).removeClass('in');
				$(pageContextElements.panelLink).text(centerColumn.SelectLocation);
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
				var typeCode =$(this).data('code');
				$(pageContextElements.levelDiv).hide();
				$(pageContextElements.batchDiv).hide();
				$(pageContextElements.yearDiv).hide();
				$(pageContextElements.academicYearDiv).hide();
				$(pageContextElements.tableDiv).hide();
				$(pageContextElements.learnerDiv).hide();
				$(pageContextElements.btnDiv).hide();
				$(pageContextElements.promoteDiv).hide();
				if(typeCode.toLowerCase() =='U'.toLowerCase()){
					showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("U");
					fnCollapseMultiselect();
				}else{
					hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("R");
					fnCollapseMultiselect();
					
				}
				$(pageContextElements.eleCollapseFilter).css('display','');
			});
		},
		refreshTable: function(table){
			//$(table).bootstrapTable("refresh");
		},
		showLevels : function(){
			shikshaPlus.fnGetLevelsByCenter(selectBox.level,0,$(selectBox.center).val());
			setOptionsForMultipleSelect(selectBox.level);
			$(selectBox.level).multiselect("rebuild");
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
			$(pageContextElements.btnDiv).hide();
			$(pageContextElements.promoteDiv).hide();
			$(pageContextElements.levelDiv).show();
		},
		showBatches : function(){
			shikshaPlus.fnGetBatchesByCenterAndLevel(selectBox.batch,0,$(selectBox.center).val(),$(selectBox.level).val());
			setOptionsForMultipleSelect(selectBox.batch);
			$(selectBox.batch).multiselect("rebuild");
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.learnerDiv).hide();
			$(pageContextElements.btnDiv).hide();
			$(pageContextElements.promoteDiv).hide();
			$(pageContextElements.batchDiv).show();
		},
		showAcademicCycles : function(){
			shikshaPlus.fnGetCurrentYear(selectBox.year,0);
			setOptionsForMultipleSelect(selectBox.year);
			$(selectBox.year).multiselect("rebuild");
			shikshaPlus.fnGetPreviousACYear(selectBox.academicYear,0);
			setOptionsForMultipleSelect(selectBox.academicYear);
			$(selectBox.academicYear).multiselect("rebuild");
			$(pageContextElements.academicYearDiv).show();
			$(pageContextElements.btnDiv).show();
			$(pageContextElements.yearDiv).show();
			$(pageContextElements.learnerDiv).show();
			$(pageContextElements.promoteDiv).hide();
		},
		showViewBtn:function(){
			$(pageContextElements.academicYearDiv).show();
			$(pageContextElements.btnDiv).show();
			$(pageContextElements.yearDiv).show();
			$(pageContextElements.learnerDiv).show();
			$(pageContextElements.promoteDiv).hide();
			$(pageContextElements.tableDiv).hide();
		},
		checkboxChecked : function(){
			$(pageContextElements.learnerDiv).hide();
			$(pageContextElements.promoteDiv).show();
		},
		checkboxUnChecked: function(){
			var count=0;
			$.each($(pageContextElements.learnerTable+" tbody tr"),function(index,value){
				if($(value).hasClass('selected')){
					count++;
				}
			})
			if(count==0){
				$(pageContextElements.promoteDiv).hide();
				$(pageContextElements.learnerDiv).show();
			}
			else{
				$(pageContextElements.learnerDiv).hide();
				$(pageContextElements.promoteDiv).show();
			}
			
			
		},
		checkboxCheckedAll : function(){
			var rowCount=$('#bulkRollNoUpdateTable').bootstrapTable('getData').length
			if(rowCount!=0){
				$(pageContextElements.learnerDiv).hide();
				$(pageContextElements.promoteDiv).show();
			}
		},
		checkboxUnCheckedAll: function(){
			$(pageContextElements.promoteDiv).hide();
			$(pageContextElements.learnerDiv).show();
		},
		fnEmptySelectBox:function(){
			$.each(arguments,function(i,obj){
				$(obj).empty();
			});
		},
		fnGetFromLevel:function(elementId,toLevelElementId){
			
			var	responseData = shiksha.invokeAjax("level/list", null, "GET");
			var levelId=$(selectBox.level).val();
			var sequenceNumber;
			if(responseData!=null){
				$.each(responseData, function(index,obj){					
					if(obj.levelId == levelId){
						sequenceNumber=obj.sequenceNumber;
						$(elementId).append('<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'"  selected>'+obj.levelName+'</option>');
						
						}
				})
				$(toLevelElementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
				$.each(responseData, function(index,obj){
					
					if(obj.sequenceNumber == (sequenceNumber+1)){
						$(toLevelElementId).append('<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'" >'+obj.levelName+'</option>');
					}
				})
			}
		},
		resetFormById : function(formId) {
			$(formId).find('#alertdiv').hide();
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		fnGetPromotionData:function(){
			var learnerData=[];
			$('tbody tr.selected').each(function(index,value){
				var learner={};
				learner.learnerId =$(value).attr('learnerid');
				learnerData.push(learner);
				});
			var data={
					'center':{"id": $(selectBox.center).val()},
					'level' :{"levelId":$(selectBox.level).val()},
					'batch':{"batchId":$(selectBox.batch).val()},
					'learners'   : learnerData,
					'academicCycle': {"academicCycleId":$(selectBox.academicYear).val()}
			}
			var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=validate",data, "POST");
			if(ajaxCall.response == "shiksha-200"){
				learnerLevelPromotion.showPromotionModal();
			}else if(ajaxCall.response == "shiksha-800"){
				if(ajaxCall.responseMessages.length>0){
					var names="";
					$.each(ajaxCall.responseMessages, function(index,obj){
						var num=index+1;
						names=names+"<ul>"+num+". "+obj+"</ul>"
					});
					AJS.flag({
						type  : "warning",
						title : appMessgaes.warning,
						body  : centerColumn.noPromotionWithoutRollNumber+names,
						close : 'auto'
					});
				}	
			}else{
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : ajaxCall.responseMessage,
					close :'auto'
				});
			}

			spinner.hideSpinner();
		},
		showPromotionModal :function(){
			learnerLevelPromotion.resetFormById(promotion.form);
			shikshaPlus.fnGetPreviousACYear(promotion.fromAC,0);
			setOptionsForMultipleSelect(promotion.fromAC);
			$(promotion.fromAC).multiselect("rebuild");
			shikshaPlus.fnGetCurrentACYear(promotion.toAC,0)
			setOptionsForMultipleSelect(promotion.toAC);
			$(promotion.toAC).multiselect("rebuild");
			 learnerLevelPromotion.fnEmptySelectBox(promotion.fromLevel,promotion.toLevel);
			learnerLevelPromotion.fnGetFromLevel(promotion.fromLevel,promotion.toLevel);
			
			setOptionsForMultipleSelect(promotion.fromLevel);
			$(promotion.fromLevel).multiselect("rebuild");
			setOptionsForMultipleSelect(promotion.toLevel);
			$(promotion.toLevel).multiselect("rebuild");
			$(promotion.batchDiv).hide();
			spinner.hideSpinner();
			$(promotion.modal).modal('show');
		},
		showBatchDiv:function(){
			shikshaPlus.fnGetBatchesByCenterAndLevel(promotion.toBatch,0,$(selectBox.center).val(),$(promotion.toLevel).val());
			setOptionsForMultipleSelect(promotion.toBatch);
			$(promotion.toBatch).multiselect("rebuild");
			$(promotion.batchDiv).show();
		},
				
		fnPromote :function(){
			learners=[];
			$('tbody tr.selected').each(function(index,value){
				var learner={};
				learner.learnerId =$(value).attr('learnerid');
				learners.push(learner);
				});
			var data={
					'center'    : {"id":$(selectBox.center).val()},
					'fromLevel' : {"levelId": $(promotion.fromLevel).val()},
					'toLevel'   : {"levelId":$(promotion.toLevel).val()},
					'fromAcademicCycle': {"academicCycleId":$(promotion.fromAC).val()},
					'toAcademicCycle'  :{"academicCycleId": $(promotion.toAC).val()},
					'fromBatch'  : {'batchId':$(selectBox.batch).val()},
					'toBatch'    :{ "batchId":$(promotion.toBatch).val()},
					'learners'   : learners
			}
			var promoteCall=shiksha.invokeAjax("learner/level/promote",data,"POST");
			spinner.hideSpinner();
			console.log(promoteCall);
			if(promoteCall.response == 'shiksha-200'){
				console.log("success");
				$(pageContextElements.learnerDiv).show();
				$(pageContextElements.promoteDiv).hide();
				$(pageContextElements.tableDiv).hide();
				AJS.flag({
					type  : "success",
					title : appMessgaes.success,
					body  : promoteCall.responseMessage,
					close : 'auto'
				});
				$(promotion.modal).modal('hide');
			}else if(promoteCall.responseMessages.length>0){
				var names="";
				$.each(promoteCall.responseMessages, function(index,obj){
					var num=index+1;
					var name=obj.split(":")[0];
					var cutOff=obj.split(":")[1];
					names=names+"<ul>"+num+".<span> "+name+"</span><span>"+cutOff+"</span></ul>"
					
				});
				console.log("partial success");
				$(pageContextElements.learnerDiv).show();
				$(pageContextElements.promoteDiv).hide();
				$(pageContextElements.tableDiv).hide();
				AJS.flag({
					type  : "warning",
					title : appMessgaes.warning,
					body  : promoteCall.responseMessage+names,
					close : 'manual'
				});
				$(promotion.modal).modal('hide');
				setTimeout(function(){
					$("#viewLearnerBtn").trigger( "click" );	
				}, 1500);
				
			
			}else{
				AJS.flag({
					type  : "Error",
					title : appMessgaes.error,
					body  : promoteCall.responseMessage,
					close : 'auto'
				});
			}
		},
		showTable :function(){
			
			var data={
					'center':{"id": $(selectBox.center).val()},
					'level' :{"levelId":$(selectBox.level).val()},
					'batch':{"batchId":$(selectBox.batch).val()},
					'academicCycle': {"academicCycleId":$(selectBox.academicYear).val()}
			}
			var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=promote",data, "POST");
			
			if(ajaxCall.response == "shiksha-200"){
				
			}else if(ajaxCall.response == "shiksha-800"){
				if(ajaxCall.responseMessages.length>0){
					var names="";
					$.each(ajaxCall.responseMessages, function(index,obj){
						var num=index+1;
						names=names+"<ul>"+num+". "+obj+"</ul>"
					});
					AJS.flag({
						type  : "warning",
						title : appMessgaes.warning,
						body  : ajaxCall.responseMessage+names,
						close : 'auto'
					});
				}	
			}else{
				AJS.flag({
					type : "error",
					title : appMessgaes.error,
					body : ajaxCall.responseMessage,
					close :'auto'
				});
			}
			spinner.hideSpinner();
			var learnerData=ajaxCall.learners;
			$(pageContextElements.learnerTable).bootstrapTable('load',learnerData);	
			$(pageContextElements.tableDiv).show();
			
		},
	    formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					toLevel:{
						required: true
					},
					toBatch:{
						required: true
					},
				},

				messages: {
					toLevel:{
						required:centerColumn.toLevelRequired
							},
					toBatch: {
						required:centerColumn.toBatchRequired
							}
				},
				errorPlacement: function(error, element) {
					if($(element).hasClass("select")){
						$(element).parent().append(error);
					} else {
						$(element).after(error);
					}
				},
				submitHandler : function(){
					learnerLevelPromotion.fnPromote();
				}
			});
		},
		learnerIdHide :function(){
			return {
			    classes: 'learnerId',
			  };
		},
		learnerAttributes : function(row){
			return {"learnerId":row.learnerId,
					"learnerName":row.learnerName,
					"learnerRollNumber":row.learnerRollNumber};
		},
};
$( document ).ready(function() {
	learnerLevelPromotion.formValidate(promotion.form);
	learnerLevelPromotion.fnInit();
});