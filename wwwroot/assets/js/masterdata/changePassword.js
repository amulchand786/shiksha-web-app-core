var changePassword = function () {
	var oldPassword = $('#currentpassword').val();
	var newPassword = $('#newpassword').val();
	var confirmPassword = $('#confirmPassword').val();

	var json = {
		"oldPassword": oldPassword,
		"newPassword": newPassword,
		"confirmPassword": confirmPassword
	}
	var result = customAjaxCalling("changePassword", json, "PUT");

	if (result.response == "shiksha-200") {
		$("#successPwdMsg").show();

		setTimeout(function () {
			window.location = 'logout';
		}, 2000);
	} else {

		$('#errorPwdMsg').show().text(result.responseMessage);

	}
}
//Start Forgot Password
var forgotPassword = function () {
	var email = $('#email').val().trim();
	var json =
		{
			"email": email
		}

	$('.outer-loader').show();
	/*	setTimeout(function(){*/
	var result = customAjaxCalling("forgotPassword", json, "POST");

	if (result != null) {
		if (result.response == "shiksha-200") {
			$('#emailHelpText').hide();
			$('#email').prop('disabled', true);
			$('#errorPwdMsg').hide();
			$('#btnDiv').hide();
			$("#successPwdMsg").show();
			$("#successPwdMsg").text(result.responseMessage);
			/*}else if(result.response == "shiksha-400" && result.responseMessage==null ) {	
				$('#emailHelpText').hide();
				$('#errorPwdMsg').show().text("This email address is not registered with Shiksha.").fadeOut(3000);
				$('#emailHelpText').show().fadeIn(3000);*/
		} else {
			$('#emailHelpText').hide();
			$('#errorPwdMsg').show().text(result.responseMessage);
		}
	} else {
		$('#emailHelpText').hide();
		$('#errorPwdMsg').show().text("Something went wrong with your internet !!");
	}
	$(".outer-loader").hide();


	/*}, 200);*/

}

//set New Password
var setNewPassword = function () {

	var confirmPassword = $('#confirmPassword').val();
	var json = {
		"hashCode": hashCode,
		"confirmPassword": confirmPassword
	}
	var result = customAjaxCalling("setNewPassword", json, "POST");

	if (result.response == "shiksha-200") {
		$("#successPwdMsg").show();

		setTimeout(function () {
			window.location = 'logout';
		}, 2000);
	} else {

		$('#errorPwdMsg').show().text(result.responseMessage);

	}

}
var uploadProfileImage = function (file) {
	spinner.showUploadSpinner();
	var formData = new FormData();
	formData.append('uploadfile', file);
	$.ajax({
		url: "user/profile/avtar",
		type: "POST",
		data: formData,
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		cache: false,
		async: true,
		success: function (response) {
			spinner.hideUploadSpinner();
			console.log(response, "successfully uploaded");
			if (response.response == "shiksha-200") {
				uploadSegment.getUnmappedfiles();
				$("#pImage").
					AJS.flag({
						type: "success",
						title: appMessgaes.success,
						body: response.responseMessage,
						close: 'auto'
					});
				$(".li-file").draggable({
					refreshPositions: true,
					revert: true,
					helper: "clone"
				});
			} else {
				AJS.flag({
					type: "error",
					title: appMessgaes.error,
					body: response.responseMessage,
					close: 'auto'
				});
			}
		}
	});
}


function resetTheFormFields() {
	$('#changePasswordForm').data('formValidation').resetForm(true);
};

$(function () {
	$("#forgotPasswordForm #email").focusin(function () {
		$('#errorPwdMsg').hide();
		$('#emailHelpText').show();
	});


	$("#changePasswordForm")
		.formValidation(
		{
			feedbackIcons: {
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
				oldPassword: {
					validators: {
						callback: {
							message: 'State does not contains these (", ?, \', /, \\) special characters',
							callback: function (value) {
								var regx = /^[^'?\"/\\]*$/;
								return regx.test(value);
							}
						}
					}
				},
				newpassword: {
					validators: {
						callback: {
							message: 'Password should be a minimum of 8 characters long and should include at least one each of an uppercase letter, a lowercase letter, a special character and a number.',
							callback: function (value) {
								// var regex = new

								var regex = new RegExp(
									/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@~`!#\$\^%&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]).*$/);
								return regex.test(value);
							}
						}
					}
				},
				confirmPassword: {
					validators: {
						notEmpty: {
							message: ' '
						},
						identical: {
							field: 'newpassword',
							//message : 'The Password and its Confirm Password must be the same'
							message: ' '
						}
					}
				}
			}
		}).on('success.form.fv', function (e) {
			e.preventDefault();
			changePassword();

		});


	// for forgot password schreen
	$("#forgotPasswordForm")
		.formValidation(
		{
			feedbackIcons: {
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
				email: {
					validators: {
						notEmpty: {
							message: 'The Email is required'
						},
						emailAddress: {
							message: 'The value is not a valid email address'
						},
						regexp: {
							regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
							message: 'The value is not a valid email address'
						}
					}
				}
			}
		})
		.on('err.validator.fv', function (e, data) {
			data.element
				.data('fv.messages')
				.find('.help-block[data-fv-for="' + data.field + '"]').hide()
				.filter('[data-fv-validator="' + data.validator + '"]').show();

		}).on('success.form.fv', function (e) {
			e.preventDefault();
			forgotPassword();
		});


	$("#newPasswordForm")
		.formValidation(
		{
			feedbackIcons: {
				validating: 'glyphicon glyphicon-refresh'
			},
			fields: {
				newpassword: {
					validators: {
						callback: {
							message: 'Password should be a minimum of 8 characters long and should include at least one each of an uppercase letter, a lowercase letter, a special character and a number.',
							callback: function (value) {
								// var regex = new

								var regex = new RegExp(
									/^(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[@~`!#\$\^%&_*()+=\-\[\]\\\';,\.\/\{\}\|\":<>\? ]).*$/);
								return regex.test(value);
							}
						}
					}
				},
				confirmPassword: {
					validators: {
						notEmpty: {
							message: ' '
						},
						identical: {
							field: 'newpassword',
							message: ' '
						}
					}
				}
			}
		}).on('success.form.fv', function (e) {
			e.preventDefault();
			setNewPassword();

		});

	$("#profileImage").on('change', 'input', function () {
		if (this.files && this.files[0]) {
			uploadProfileImage(this.files[0]);
		}
	});
});