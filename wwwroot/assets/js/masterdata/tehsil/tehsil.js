//global variables
var  colorClass='';
var resetButtonObj;
var allInputEleNamesForTehsil = [];
var allInputEleNamesOfFilter =[];
var btnAddMoreTehsilId;
var deleteTehsilId;
var selectedIds;
var thLabels;
var elementIdMap;

//initialize global variables
var initGloabalVarsTehsil = function() {
	resetButtonObj = '';
	allInputEleNamesForTehsil = [ 'addTehsilName' ];
	allInputEleNamesOfFilter=['stateId','zoneId','districtId'];
	btnAddMoreTehsilId = '';
	deleteTehsilId = '';
}


//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}


//displaying smart-filters
var displaySmartFilters = function(type) {
	$(".outer-loader").show();
	if (type == "add") {
		elementIdMap = {
				"State" : '#mdlAddTehsil #statesForZone',
				"Zone" : '#mdlAddTehsil #selectBox_zonesByState',
				"District" : '#mdlAddTehsil #selectBox_districtsByZone',

		};
		displayLocationsOfSurvey($('#divFilter'));
	} else {
		elementIdMap = {
				"State" : '#mdlEditTehsil #statesForZone',
				"Zone" : '#mdlEditTehsil #selectBox_zonesByState',
				"District" : '#mdlEditTehsil #selectBox_districtsByZone',

		};
		displayLocationsOfSurvey($('#divFilter'));
	};
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};

//smart filter utility--- start here

var initSmartFilter = function(type) {
	$(".editResetFilterDiv").show();
	reinitializeSmartFilter(elementIdMap);
	if (type == "add"){
		
		resetFormValidatonForLocFilters("addTehsilForm",allInputEleNamesOfFilter);
		resetFvForAllInputExceptLoc("addTehsilForm",allInputEleNamesForTehsil);
		displaySmartFilters("add")
		fnCollapseMultiselect();
	}
	else{
		
		resetFvForAllInputExceptLoc("editTehsilForm",['editTehsilName']);
		displaySmartFilters("edit")
		fnCollapseMultiselect();
	}
}


//reset only location when click on reset button icon
var resetLocation = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;
}
//invoking on click of  confirmation dialog box
var doResetLocation = function() {
	selectedIds=[];//it is for smart filter utility..SHK-369
	if ($(resetButtonObj).parents("form").attr("id") == "addTehsilForm") {
		
		resetFormValidatonForLocFilters("addTehsilForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add");

	} else {
		
		resetFormValidatonForLocFilters("addTehsilForm",allInputEleNamesOfFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
	}
	fnCollapseMultiselect();

}

//reset form
var resetLocationAndForm = function(type) {
	$('#divFilter').hide();
	resetFormById('addTehsilForm');
	resetFormById('editTehsilForm');
	reinitializeSmartFilter(elementIdMap);
	if (type == "add")
		initSmartFilter("add");
	else
		initSmartFilter("edit");
}

//create a row after save
var getTehsilRow = function(tehsil) {
	var row = "<tr id='"
		+ tehsil.tehsilId
		+ "' data-id='"
		+ tehsil.tehsilId
		+ "'>"
		+ "<td></td>"
		+ "<td data-stateid='"
		+ tehsil.stateId
		+ "'	title='"
		+ tehsil.stateName
		+ "'>"
		+ tehsil.stateName
		+ "</td>"
		+ "<td data-zoneid='"
		+ tehsil.zoneId
		+ "'	title='"
		+ tehsil.zoneName
		+ "'>"
		+ tehsil.zoneName
		+ "</td>"
		+ "<td data-districtid='"
		+ tehsil.districtId
		+ "' title='"
		+ tehsil.districtName
		+ "' >"
		+ tehsil.districtName
		+ "</td>"
		+ "<td data-tehsilid='"
		+ tehsil.tehsilId
		+ "' title='"
		+ tehsil.tehsilName
		+ "' >"
		+ tehsil.tehsilName
		+ "</td>"
		+ "<td data-tehsilcode='"
		+ tehsil.tehsilCode
		+ "' title='"
		+ tehsil.tehsilCode
		+ "'>"
		+ tehsil.tehsilCode
		+ "</td>"
		+

		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top'  id='btnEditTehsil'"
		+ "onclick=editTehsilData(&quot;"
		+ tehsil.stateId
		+ "&quot;,&quot;"
		+ tehsil.zoneId
		+ "&quot;,&quot;"
		+ tehsil.districtId
		+ "&quot;,&quot;"
		+ tehsil.tehsilId
		+ "&quot;,&quot;"
		+ escapeHtmlCharacters(tehsil.tehsilName)
		+ "&quot;); ><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></button><a style='margin-left:10px;' class='tooltip tooltip-top' id=deleteTehsil onclick=deleteTehsilData(&quot;"
		+ tehsil.tehsilId
		+ "&quot;,&quot;"
		+ escapeHtmlCharacters(tehsil.tehsilName)
		+ "&quot;);><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"
		+ "</tr>";

	return row;
}


var applyPermissions=function(){
	if (!editTehsilPermission){
	$("#btnEditTehsil").remove();
		}
	if (!deleteTehsilPermission){
	$("#deleteTehsil").remove();
		}
	}

//smart filter utility--- end here

//perform Crud

//add District

var addTehsil = function(event) {
	$(".outer-loader").show();
	var districtId = $('#mdlAddTehsil #selectBox_districtsByZone').val();
	var tehsilName = $('#mdlAddTehsil #addTehsilName').val().trim().replace(/\u00a0/g," ");
	var json = {
			"districtId" : districtId,
			"tehsilId" : 0,
			"tehsilName" : tehsilName,
	};

	// update data table without reloading the whole table
	// save&add new
	btnAddMoreTehsilId = $(event.target).data('formValidation')
	.getSubmitButton().data('id');

	var tehsil = customAjaxCalling("tehsil", json, "POST");
	if(tehsil!=null){
		if (tehsil.response == "shiksha-200") {
			$(".outer-loader").hide();

			var newRow = getTehsilRow(tehsil);
			appendNewRow("tehsilTable", newRow);
			applyPermissions();
		
			fnUpdateDTColFilter('#tehsilTable',[2],7,[0,6],thLabels);
			setShowAllTagColor(colorClass);
			if (btnAddMoreTehsilId == "addTehsilSaveMoreButton") {
				$("#mdlAddTehsil").find('#errorMessage').hide();
				
				resetFvForAllInputExceptLoc("addTehsilForm",allInputEleNamesForTehsil);
				fnDisplaySaveAndAddNewElement('#mdlAddTehsil',tehsilName);
			} else {
				$('#mdlAddTehsil').modal("hide");
				
				setTimeout(function() {   //calls click event after a one sec
					 AJS.flag({
					    type: 'success',
					    title: validationMsg.sucessAlert,
					    body: '<p>'+ tehsil.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);			
				
			}
		} else {
			showDiv($('#mdlAddTehsil #alertdiv'));
			$("#mdlAddTehsil").find('#successMessage').hide();
			$("#mdlAddTehsil").find('#okButton').hide();
			$("#mdlAddTehsil").find('#errorMessage').show();
			$("#mdlAddTehsil").find('#exception').show();
			$("#mdlAddTehsil").find('#buttonGroup').show();
			$("#mdlAddTehsil").find('#exception').text(tehsil.responseMessage);
		}
	}
	$(".outer-loader").hide();

};



//edit
//get data for edit
var editStateId = 0;
var editZoneId = 0;
var editDistrictId = 0;
var editTehsilId = 0;
var editTehsilData = function(stateId, zoneId, districtId, tehsilId,tehsilname) {
	initSmartFilter("edit");
	$(".editResetFilterDiv").hide();
	var idMap = {
			'#mdlEditTehsil #statesForZone' : stateId,
			'#mdlEditTehsil #selectBox_zonesByState' : zoneId,
			'#mdlEditTehsil #selectBox_districtsByZone' : districtId,

	};

	// make selected in smart filter for selected locations of corresponding
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(ele_key).multiselect('destroy');
				if (idMap[ele_key] != "" && idMap[ele_key] != null)
					$(ele_key).find("option[value=" + idMap[ele_key] + "]")
					.prop('selected', true);
				enableMultiSelect(ele_key);
			});
	$('#mdlEditTehsil #editTehsilName').val(tehsilname);
	editTehsilId = tehsilId;
	$("#mdlEditTehsil").modal().show();
	fnCollapseMultiselect();
};

//edit city

var editTehsil = function() {
	$(".outer-loader").show();
	var districtId = $('.editTehsil #selectBox_districtsByZone').val();
	var tehsilName = $('#editTehsilName').val().trim().replace(/\u00a0/g," ");
	var tehsilId = editTehsilId;

	var json = {
			"districtId" : districtId,
			"tehsilId" : tehsilId,
			"tehsilName" : tehsilName,
	};

	var tehsil = customAjaxCalling("tehsil", json, "PUT");
	if(tehsil!=null){
		if(tehsil.response=="shiksha-200"){
			$(".outer-loader").hide();
			deleteRow("tehsilTable",tehsil.tehsilId);
			var newRow =getTehsilRow(tehsil);
			appendNewRow("tehsilTable",newRow);
			applyPermissions();			
			fnUpdateDTColFilter('#tehsilTable',[2],7,[0,6],thLabels);
			setShowAllTagColor(colorClass);
			displaySuccessMsg();	

			$('#mdlEditTehsil').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: validationMsg.sucessAlert,
				    body: '<p>'+ tehsil.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);

		}else {
			showDiv($('#mdlEditTehsil #alertdiv'));
			$("#mdlEditTehsil").find('#successMessage').hide();
			$("#mdlEditTehsil").find('#errorMessage').show();
			$("#mdlEditTehsil").find('#exception').show();
			$("#mdlEditTehsil").find('#buttonGroup').show();
			$("#mdlEditTehsil").find('#exception').text(tehsil.responseMessage);
		}
	}
	$(".outer-loader").hide();

};


//getting data for delete

var deleteTehsilData = function(tehsilid, tehsilname) {
	deleteTehsilId = tehsilid;	
	$('#deleteTehsilMessage')
	.text(columnMessages.deleteConfirm.replace("@NAME",  tehsilname)+ " ?");
	$("#mdlDelTehsil").modal().show();
	hideDiv($('#mdlDelTehsil #alertdiv'));

};

//delete tehsil

$(document).on('click', '#deleteTehsilButton', function() {
	$(".outer-loader").show();
	

		var tehsil = customAjaxCalling("tehsil/" + deleteTehsilId, null, "DELETE");
		if(tehsil!=null){
		if (tehsil.response == "shiksha-200") {
			$(".outer-loader").hide();
			// Delete Row from data table and refresh the table without refreshing
			// the whole table
			deleteRow("tehsilTable", deleteTehsilId);
			fnUpdateDTColFilter('#tehsilTable',[2],7,[0,6],thLabels);	
			
			setShowAllTagColor(colorClass);
			$('#mdlDelTehsil').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: validationMsg.sucessAlert,
				    body: '<p>'+ tehsil.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
		} else {
			showDiv($('#mdlDelTehsil #alertdiv'));
			$("#mdlDelTehsil").find('#errorMessage').show();
			$("#mdlDelTehsil").find('#exception').text(tehsil.responseMessage);
			$("#mdlDelTehsil").find('#buttonGroup').show();
		}
	}
	$(".outer-loader").hide();
});



//form validation
$(function() {	

	thLabels =['#',columnMessages.state,columnMessages.zone,columnMessages.district,columnMessages.tehsil,columnMessages.tehsilCode,columnMessages.action];
	fnSetDTColFilterPagination('#tehsilTable',7,[0,6],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#tehsilTable',[2]);
	});
	colorClass = $('#t0').prop('class');

	$("#tableDiv").show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	initGloabalVarsTehsil();
	$( "#addTehsilName" ).keypress(function() {		
		hideDiv($('#mdlAddTehsil #alertdiv'));
	})
	$( "#editTehsilForm" ).keypress(function() {		
		hideDiv($('#mdlEditTehsil #alertdiv'));
	})
	$("#addTehsilForm")
	.formValidation(

			{
				excluded : ':disabled',
				feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
				},
				fields : {

					addTehsilName : {
						validators : {
							stringLength: {
		                        max: 250,
		                        message: validationMsg.nameMaxLength
		                    },
							callback : {
								message :columnMessages.tehsil+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
								callback : function(value) {
									var regx=/^[^'?\"/\\]*$/;
									if(/\ {2,}/g.test(value))
										return false;
									return regx.test(value);
								}
							}
						}
					}
				}
			}).on('success.form.fv', function(e) {
				e.preventDefault();
				addTehsil(e);

			});
	$("#editTehsilForm")
	.formValidation(
			{
				excluded : ':disabled',
				feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
				},
				fields : {
					editTehsilName : {
						validators : {
							stringLength: {
		                        max: 250,
		                        message: validationMsg.nameMaxLength
		                    },
							callback : {
								message :columnMessages.tehsil+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
								callback : function(value) {
									var regx=/^[^'?\"/\\]*$/;
									if(/\ {2,}/g.test(value))
										return false;
									return regx.test(value);
								}
							}
						}
					}
				}
			}).on('success.form.fv', function(e) {
				e.preventDefault();
				editTehsil();

			});

});
/*
 * var refreshScreen = function(event) { location.reload(); };
 */
