//global variable
var addQbForm ="addQuestionForm";


var sourceId;
var sourceVal;
var stageId;
var stageVal;
var gradeId;
var gradeVal;
var subjectId;
var subjectVal;
var unitId;
var unitVal;
var qbName;
var createQBRequestData;
var qbAutoName;
var sgSurveyCode;
var unitIds;
var thLabels;
//global select divs
var questionBankPageContext = {};
questionBankPageContext.source ="#selectBox_source";
questionBankPageContext.stage ="#selectBox_stage";
questionBankPageContext.grade ="#selectBox_grade";
questionBankPageContext.subject ="#selectBox_subject";
questionBankPageContext.unit ="#selectBox_unit";
questionBankPageContext.viewButton ="#btnViewQB";

//addmodalelements
var addQBankMdlElements ={};
addQBankMdlElements.source ="#mdlAddQuestion #selectBox_source";
addQBankMdlElements.stage ="#mdlAddQuestion #selectBox_stage";
addQBankMdlElements.grade ="#mdlAddQuestion #selectBox_grade";
addQBankMdlElements.subject ="#mdlAddQuestion #selectBox_subject";
addQBankMdlElements.unit =" #mdlAddQuestion #selectBox_unit";
addQBankMdlElements.qbName ="#mdlAddQuestion #qbName";
addQBankMdlElements.qbDescription ="#mdlAddQuestion #descriptionId";
addQBankMdlElements.textAreaMsg="#mdlAddQuestion #textAreaMsg";



//addmodal ele divs
var addQBankMdlDivs = {};
addQBankMdlDivs.source ="#mdlAddQuestion #sourceDiv";
addQBankMdlDivs.stage ="#mdlAddQuestion #stageDiv";
addQBankMdlDivs.grade ="#mdlAddQuestion #gradeDiv";
addQBankMdlDivs.subject ="#mdlAddQuestion #divSubject";
addQBankMdlDivs.unit =" #mdlAddQuestion #divUnit";	
addQBankMdlDivs.description =" #mdlAddQuestion #descriptionDiv";	

//initialise all gloabl variables on document ready
var initGlobalVars =function(){
	sourceId ='';
	sourceVal='';
	stageId ='';
	stageVal ='';
	gradeId ='';
	gradeVal ='';
	subjectId ='';
	subjectVal ='';
	unitId ='';
	unitVal ='';
	qbName ='';
	createQBRequestData ='';
}

//==============================

//empty all select box
var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}
var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');	
}

var fnBindSubjectByGradeToListBox =function(bindToElementId,selectedGradeId){
	spinner.showSpinner();
	var subjects =customAjaxCalling("subject/"+selectedGradeId,null,'GET');
	if(subjects!=null && subjects.length>0){
		var	$select	=bindToElementId;
		$select.html('');
		$.each(subjects,function(index,obj){
			$select.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-subjectname="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
		});
	}else{
		hideDiv($(addQBankMdlDivs.subject));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no subjects for the selected grade.");
		$('#mdlError').modal().show();
	}
	spinner.hideSpinner();
};


//hide any succeess/error div on add assessment modal
var hideResponseDiv =function(){
	//hideDiv($('#mdlAddQuestion#alertdiv'))
	$('#mdlAddQuestion #alertdiv').css('display','none');
}


//onchange of grade, get subject and bind to subject list box
var fnGetSubjectByGrade =function(){
	gradeId =$(addQBankMdlElements.grade).find('option:selected').data('id');

	fnEmptyAll(addQBankMdlElements.subject);
	fnEmptyAll(addQBankMdlElements.unit);

	showDiv($(addQBankMdlDivs.subject));
	hideDiv($(addQBankMdlDivs.unit));

	fnBindSubjectByGradeToListBox($(addQBankMdlElements.subject),gradeId);
	destroyRefresh(addQBankMdlElements.subject);
	setOptionsForViewQBMultipleSelect($(addQBankMdlElements.subject));

	destroyRefresh(addQBankMdlElements.unit);
	setOptionsForViewQBMultipleSelect($(addQBankMdlElements.unit));

	$("#"+addQbForm).data('formValidation').updateStatus('subject', 'NOT_VALIDATED');

	fnCollapseMultiselect();
}

//bind chapters by grade and subject to respective select box
//SHK-1608
var fnBindChapter =function(element,gradeId,subjectId,data){
	spinner.showSpinner();
	var $ele =$(element);
	$.each(data,function(index,obj){		
		$ele.append('<option value="'+obj.unitId+'" data-id="' + obj.unitId + '"data-name="' + obj.unitName+ '">'+escapeHtmlCharacters(obj.unitName)+'</option>');
	});
	destroyRefresh(element);
	setOptionsForViewQBMultipleSelect($(element));
	spinner.hideSpinner();
}
var fnShowRemarks =function(){
	showDiv($(addQBankMdlDivs.description));
}

//onchange of subject, get chapters and bind to chapter list box
var fnGetUnitsBySub =function(){
	spinner.showSpinner();
	subjectId =$(addQBankMdlElements.subject).find('option:selected').data('id');
	gradeId =$(addQBankMdlElements.grade).find('option:selected').data('id');

	var data =customAjaxCalling("unit/?grade="+gradeId+"&subject="+subjectId,null,'GET');
	fnEmptyAll(addQBankMdlElements.unit);
	destroyRefresh(addQBankMdlElements.unit);
	setOptionsForViewQBMultipleSelect($(addQBankMdlElements.unit));
	if(data.length>0){
		fnBindChapter(addQBankMdlElements.unit,gradeId,subjectId,data);
	}else{
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no more chapters of the selected subject and grade.");
		$('#mdlError').modal().show();
	}
	$("#"+addQbForm).data('formValidation').updateStatus('unit', 'NOT_VALIDATED');
	showDiv($(addQBankMdlDivs.unit));
	fnCollapseMultiselect();
	spinner.hideSpinner();
}


var setOptionsForViewQBMultipleSelect =function(){
	$(arguments).each(function(index,ele){
		$(ele).multiselect('destroy');
		$(ele).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onChange : function() {
				hideResponseDiv();
				if (ele == (addQBankMdlElements.grade)) {
					if ($(addQBankMdlElements.grade).find('option:selected').length == 0) {
						hideDiv($(addQBankMdlDivs.subject),$(addQBankMdlDivs.unit));
					} else {
						fnGetSubjectByGrade();
					}
				}else if (ele == (addQBankMdlElements.subject)) {
					if ($(addQBankMdlElements.subject).find('option:selected').length == 0) {
						hideDiv($(addQBankMdlDivs.unit));
					} else {
						fnGetUnitsBySub();

					}					
				}
			}
		});		
	});
}



//==============================================


var pushToArray =function(element,arr){
	$(element).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				arr.push(parseInt($(option).data('id')));
			}
		});
	});
}

var fnAddQuestionBank =function(){
	spinner.showSpinner();
	$.wait(50).then(saveQuestionBank);
}

var saveQuestionBank=function(){
    spinner.showSpinner();

	var sourceVal = $(addQBankMdlElements.source).find('option:selected').data('name').toString().trim();
	var gradeVal = $(addQBankMdlElements.grade).find('option:selected').data('name').toString().trim();
	var subjectVal = $(addQBankMdlElements.subject).find('option:selected').data('subjectname').toString().trim();
	var unitVal =$(addQBankMdlElements.unit).find('option:selected').data('name').toString().trim();

	var qbName = $(addQBankMdlElements.qbName).val().toString().trim().replace(/\u00a0/g," ");
	qbAutoName = sourceVal+'_'+gradeVal+'_'+subjectVal+'_'+unitVal;

	//UAT-12 27-05-16
	unitIds =[];
	pushToArray((addQBankMdlElements.unit),unitIds);
	var createQBData ={
			"questionBankName":$(addQBankMdlElements.qbName).val().trim().replace(/\u00a0/g," "),
			"sourceId":$(addQBankMdlElements.source).find('option:selected').data('id'),
			"gradeId":$(addQBankMdlElements.grade).find('option:selected').data('id'),

			"subjectId":$(addQBankMdlElements.subject).find('option:selected').data('id'),
			//SHK-1608
			"unitId": $(addQBankMdlElements.unit).find('option:selected').data('id'),
			"description":$(addQBankMdlElements.qbDescription).val().replace(/\u00a0/g," "),
			"questionBankAutoName":qbAutoName.replace(/\u00a0/g," "),
			"gradeName":gradeVal,
			"subjectName":subjectVal,
			"sourceName":sourceVal,
			"unitName":unitVal
	}

	var responseData =customAjaxCalling("questionbank",createQBData, "POST");
	   if(responseData.response == "shiksha-200"){
			$('#mdlAddQuestion').modal("hide");
			var sgSurveyCode=responseData.sgQbCode;
			window.location =baseContextPath+'/questionbank/'+sgSurveyCode+"/"+responseData.questionBankId;
		}else{
			$("#mdlAddQuestion").find('#successMessage').hide();			
			$("#mdlAddQuestion").find('#errorMessage').show();
			$("#mdlAddQuestion").find('#exception').show();
			$("#mdlAddQuestion").find('#exception').text(responseData.responseMessage);			
			$('#mdlAddQuestion #alertdiv').css('display','block');
		}
	
	spinner.hideSpinner();

}
//preview assessment
var previewQB =function(qbId,sgQbCode){
	// open the survey page in SG Engine
	previewSurveyInSg(sgQbCode,qbPreviewUrl);
}

//edit Assessment
var editQb =function(qbId,sgQbCode){
	// open the survey page in SG Engine							
	window.location =baseContextPath+'/questionbank/'+sgQbCode+'/'+qbId;
}



var fnBindSourceToListBox = function(element, data) {
	var $ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.sourceId + '" data-id="'+ obj.sourceId + '"data-name="' + obj.sourceName + '">'+ escapeHtmlCharacters(obj.sourceName) + '</option>');
	});
	setOptionsForViewQBMultipleSelect(element);
}


var fnBindStageToListBox = function(element, data) {
	var $ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.stageId + '" data-id="'+ obj.stageId + '"data-name="' + obj.stageName + '">'+ escapeHtmlCharacters(obj.stageName) + '</option>');
	});
	setOptionsForViewQBMultipleSelect(element);
}




var fnBindGradeToListBox = function(element, data) {
	var $ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.gradeId + '" data-id="'+ obj.gradeId + '"data-name="' + obj.gradeName + '">'+ escapeHtmlCharacters(obj.gradeName) + '</option>');
	});
	setOptionsForViewQBMultipleSelect(element);
}

//bind source
var fnBindSource = function() {	
	var sources = customAjaxCalling("source/list",null, "GET");
	fnEmptyAll(addQBankMdlElements.source);

	if(sources[0]!=null && sources[0]!=''&& sources[0].response == "shiksha-200"){

		fnBindSourceToListBox(addQBankMdlElements.source,sources);

	}

}


//bind stage
var fnBindStage = function() {
	var stages = customAjaxCalling("getStageList",null, "GET");
	fnEmptyAll(addQBankMdlElements.stage);
	if (stages!=null && stages != "" && stages[0]!=null && stages[0].response == "shiksha-200") {

		fnBindStageToListBox(addQBankMdlElements.stage, stages);

	}
}

//bind grade
var fnBindGrade = function() {	
	var grades = customAjaxCalling("getGradeList", "GET");
	fnEmptyAll(addQBankMdlElements.grade);
	if (grades!=null && grades != "" && grades[0]!=null && grades[0].response == "shiksha-200") {

		fnBindGradeToListBox(addQBankMdlElements.grade, grades);

	}
}

//reinitialize filters for create assessment
//SHK-630
var fnInitFilters =function(){
	resetFormById('addQuestionForm');
	fnEmptyAll(addQBankMdlElements.source,addQBankMdlElements.stage,addQBankMdlElements.grade,addQBankMdlElements.subject);
	fnEmptyAll(addQBankMdlElements.unit);
	destroyAndRefreshMultiselect($(addQBankMdlElements.source),$(addQBankMdlElements.stage),
			$(addQBankMdlElements.grade),$(addQBankMdlElements.subject),
			$(addQBankMdlElements.unit));


	$(addQBankMdlElements.qbDescription).text('');
	hideDiv($(addQBankMdlDivs.subject),$(addQBankMdlDivs.unit),$(addQBankMdlDivs.description));
	limitTextArea(addQBankMdlElements.textAreaMsg);
	fnBindSource();
	fnBindStage();
	fnBindGrade();

	fnCollapseMultiselect();
}



$(function() {
	$('[data-toggle="tooltip"]').tooltip(); 



	thLabels =['#','Source  ','Grade ','Subject ','Chapter ','Question Bank Name ','Question Bank Description ','No.Of Questions ','CreatedBy ','Modifiedby ','Remarks ','Action'];
	fnSetDTColFilterPagination('#qbTable',12,[0,10,11],thLabels);

	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#qbTable',[1,5]);
	});
	$('#qbTable .dataTables_empty').prop('colspan',12);
	showDiv($('#tableDiv'));	
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	setOptionsForViewQBMultipleSelect($(addQBankMdlElements.source),
			$(addQBankMdlElements.grade),$(addQBankMdlElements.subject),$(addQBankMdlElements.unit));

	limitTextArea(addQBankMdlElements.textAreaMsg);
	spinner.hideSpinner();
	$("#"+addQbForm).formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields:{
			qbName : {
				validators : {
					stringLength: {
						max: 250,
						message: 'Question bank name must be less than 250 characters'
					},
					callback : {
						message : 'Question bank '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			description : {
				validators : {
					callback : {
						message : 'Description does not contains these (", ?, \', /, \\) special characters',
						callback : function(value) {

							var regx=/^[^'?\"/\\]*$/;

							return regx.test(value);
						}
					}
				}
			},

			source: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addQBankMdlElements.source).val();
							return (option != null&& option!="NONE");
						}
					}
				}
			},
			grade: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addQBankMdlElements.grade).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			subject: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addQBankMdlElements.subject).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			unit: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addQBankMdlElements.unit).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		fnAddQuestionBank();
	});		

	//if request from Question Paper then open Createmodal popup
	if(isFromQuestionBank || isFromQuestionBank=="true"){
		$("#btnCreate").trigger( "click" );
		spinner.hideSpinner();
	}
	spinner.hideSpinner();
});