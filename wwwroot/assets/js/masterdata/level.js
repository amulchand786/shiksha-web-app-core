var LevelController = function() {

};
LevelController.prototype = function() {

	addLevel = function() {

	}, updateLevel = function() {

	}, deleteLevel = function() {

	};

	return {
		addLevel : addLevel,
		updateLevel : updateLevel,
		deleteLevel : deleteLevel
	};

}();
var deletelevelName=null;
var deletelevelId=null;

var levelController=new LevelController();

var addLevel = function(event) {
	var levelName = $('#addLevelName').val();
	

	var json = {
		"levelId" : 0,
		"levelName" : levelName
	};
	var message=customAjaxCalling("level", json, "POST");
	if (message.indexOf("saved successfully") > -1) {
		$("#add-level").find('#errorMessage').hide();
		$("#add-level").find('#exception').hide();
		$("#add-level").find('#buttonGroup').hide();
		$("#add-level").find('#successMessage').show();
		$("#add-level").find('#okButton').show();
		
		} else {
		//$('#errorMessage').text(message);
		$("#add-level").find('#successMessage').hide();
		$("#add-level").find('#okButton').hide();
		$("#add-level").find('#errorMessage').show();
		$("#add-level").find('#exception').show();
		$("#add-level").find('#buttonGroup').show();
		$("#add-level").find('#exception').text(message);
	 	}
};

var editLevel = function(event) {

	var levelName = $('#editLevelName').val();
    var levelId=$('#editLevelId').val();

	console.log("edit");
	var json = {

		"levelName" : levelName,
		"levelId" : levelId
	};
	var message=customAjaxCalling("level", json, "PUT");
	if(message.indexOf("updated successfully")>-1){
		
		 $('#deleteLevelMessage').hide();
		 $("#edit-level").find('#buttonGroup').hide();
		 $("#edit-level").find('#successMessage').show();
		 $('#editLevelButton').attr("disabled", true);
		 $("#edit-level").find('#okButton').show();
		
		 

		 } else {
		 //$('#errorMessage').text(message);
		 $("#edit-level").find('#okButton').hide();
		 $("#edit-level").find('#errorMessage').show();
		 $("#edit-level").find('#exception').text(message);
		 $("#edit-level").find('#buttonGroup').show();
 	}
};

$('#deleteLevelButton').click(function(event) {

	var json = {
		"levelId" : deletelevelId,
		"levelName":deletelevelName

	};
	var message=customAjaxCalling("level", json, "DELETE");
	if(message.indexOf("deleted successfully")>-1){
		console.log(message);

    	// $('#edit-state').modal('toggle');
    	// $('#successMessage').text(message);
 	/*	$("#delete-level").find('#successMessage').show();
 		$('#deleteLevelMessage').hide();
 		$('#deleteLevelButton').attr("disabled", true);*/
 		
 		$('#deleteLevelButton').hide();
		$("#delete-level").find('#buttonGroup').hide();
		$("#delete-level").find('#successMessage').show();
		$('#deleteLevelMessage').attr("disabled", true);
		$("#delete-level").find('#okButton').show();

 	} else {
 		//$('#errorMessage').text(message);
 	/*	$("#delete-level").find('#errorMessage').show();
 		$("#delete-level").find('#exception').text(message);*/
 		
 		$("#delete-level").find('#okButton').hide();
		$("#delete-level").find('#errorMessage').show();
		$("#delete-level").find('#exception').text(message);
		$("#delete-level").find('#buttonGroup').show();
 	}
});
var editLevelData= function (levelid,levelName) {
	
	$('#editLevelName').val(levelName);
	$('#editLevelId').val(levelid);

};
var deleteLevelData= function (levelid,levelName) {
	deletelevelId=levelid;
	deletelevelName=levelName;
	$('#deleteLevelMessage').text("Do you want to delete "+levelName+ " ?");

};



$(function() {

	

	$("#addLevelForm").formValidation({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addLevel();

	});
	$("#editLevelForm").formValidation({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		editLevel();

	});
});