var pageContextElements = {	
		addButton 						: '#addNewGroup',
		table 							: '#groupTable',
		toolbar							: "#toolbarGroup",
		addGroupForm		 			: '#addGroupForm',
		modalAddGroup		 			: '#mdlAddGroup',
		addGroupSaveMoreButton		 	: '#addGroupSaveMoreButton',
		addGroupSaveButton 				: '#addGroupSaveButton',
		modalEditGroup	 				: '#mdlEditGroup',
		modalDeleteGroup 				: '#mdlDeleteGroup',
		editGroupForm 					: '#editGroupForm',
		deleteGroupButton				: '#deleteGroupButton'
};
var addModal ={
		eleLevel : '#addLevel',
};
var editModal={
		eleLevel: '#editLevel',
};
var $table;
var submitActor = null;
var $submitActors = $(pageContextElements.addGroupForm).find('button[type=submit]');	
var groupIdForDelete;
var levelIdForDelete;
var groupFilterObj = {
		toolBar : "#toolbarGroup",
		levelSelect : "#filterBox_levels",
		searchBox : "#filter-search",
		searchBtn : "#filter-search-btn",
		init : function(){
			shikshaPlus.fnGetLevelsForChapterFilter(this.levelSelect,0);
			this.enableMultiSelectForFilters(this.levelSelect);
			$(this.searchBtn).on("click",this.filterData);
			$(this.searchBox).on("keyup",this.searchKeyUp);
			$(this.toolBar).show();
		},
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		},
		searchKeyUp : function(e){
			if (e.keyCode == 13) {
		        e.preventDefault();
		        groupFilterObj.filterData();
		    }
		},
		filterData : function(){
			$table.bootstrapTable("refresh");
		},
		fiterQueryParmams : function(params){
			
			//params["levelIds"] = groupFilterObj.fnGetSelectedFiteredValue(groupFilterObj.levelSelect);
			//params["searchText"] =$(groupFilterObj.searchBox).val();
			params["sortName"]='asc';
			params["sortOrder"]='asc';
			return params;
		},
		fnGetSelectedFiteredValue : function(ele){
			var arr =[];
			var selector = "option" ;
			if($(ele).val()!=null)
				selector = "option:selected";
			$(ele).find(selector).each(function(ind,option){
				if($(option).val()!="multiselect-all"){		
					arr.push(parseInt($(option).val()));
				}
			});
			return arr;
		},
		enableMultiSelectForFilters : function(element){
			$(element).multiselect({
				maxHeight: 325,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,	
				filterPlaceholder: 'Search here...',
				onChange : groupFilterObj.filterData
			});	
		},
	};
var group={
		name:'',
		groupId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',group.initAddModal);
			$(pageContextElements.deleteGroupButton).on('click',group.deleteFunction);

		},
		deleteFunction : function(){
			group.fnDelete(groupIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			group.resetForm(pageContextElements.addGroupForm);
			
			$(pageContextElements.modalAddGroup).modal("show");
			shikshaPlus.fnGetLevelsForChapterFilter(addModal.eleLevel,self.levelId);
			
			setOptionsForMultipleSelect(addModal.eleLevel);
			$(addModal.eleLevel).multiselect("rebuild");

		},
		resetForm : function(formId){
			$(formId).find('#alertdiv').hide();
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
			
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var levels =$(addModal.eleLevel).val();
			var levelObj = [];
			$.each(levels,function(index,val){
				if(val != "multiselect-all")
				{
					levelObj.push({levelId : val});
				}
			})
			
			var ajaxData={
					"groupName"	:$('#addGroupName').val(),
					"levels"	:levelObj,
			}
			var addAjaxCall = shiksha.invokeAjax("group", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == "addGroupSaveButton"){
						$(pageContextElements.modalAddGroup).modal("hide");
						$(pageContextElements.modalAddGroup).find("#divSaveAndAddNewMessage").hide();
						AJS.flag({
							type  : "success",
							title : groupMessages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
						group.resetForm(pageContextElements.addGroupForm);
						$(addModal.eleLevel).multiselect("rebuild");
					}
					if(submitBtnId == "addGroupSaveMoreButton"){
						$('#addGroupName').val("");
						shikshaPlus.fnGetLevelsForChapterFilter(addModal.eleLevel,self.levelId);
						setOptionsForMultipleSelect(addModal.eleLevel);
						$(addModal.eleLevel).multiselect("rebuild");
						$(pageContextElements.modalAddGroup).modal("show");
						$(pageContextElements.modalAddGroup).find("#divSaveAndAddNewMessage").show();
						fnDisplaySaveAndAddNewElementAui(pageContextElements.modalAddGroup,addAjaxCall.groupName);
					}
					
					group.refreshTable(pageContextElements.table);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(groupId){
			group.resetForm(pageContextElements.editGroupForm);
			$(editModal.eleLevel).multiselect('destroy');
			$(pageContextElements.modalEditGroup).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("group/"+groupId,null, "GET");
			spinner.hideSpinner();
			
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$('#editGroupName').val(editAjaxCall.groupName);
					$('#groupId').val(editAjaxCall.groupId);
					var selectedIds=[];
					$.each(editAjaxCall.levels,function(index,object){
						selectedIds.push(object.levelId);
					})
						shikshaPlus.fnGetLevelsForChapterFilter(editModal.eleLevel,selectedIds);
						setOptionsForMultipleSelect(editModal.eleLevel);
					
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var levels =$(editModal.eleLevel).val();
			var levelObj = [];
			$.each(levels,function(index,val){
				if(val != "multiselect-all")
				{
					levelObj.push({levelId : val});
				}
			})
			var ajaxData={
					"groupName":$('#editGroupName').val(),
					"groupId":$('#groupId').val(),
					"levels"	:levelObj,
			}

			var updateAjaxCall = shiksha.invokeAjax("group", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(pageContextElements.modalEditGroup).modal("hide");
					group.resetForm(pageContextElements.editGroupForm);
					group.refreshTable(pageContextElements.table);
					AJS.flag({
						type  : "success",
						title : groupMessages.sucessAlert,
						body  : updateAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : updateAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				},function(){
					window.location.reload();
				})
			}
		},
		initDelete : function(groupId,name){
			var msg = $("#deleteGroupMessage").data("message");
			$("#deleteGroupMessage").html(msg.replace('@NAME@',name));
			$(pageContextElements.modalDeleteGroup).modal("show");
			groupIdForDelete=groupId;
		},
		fnDelete :function(groupId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("group/"+groupId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					group.refreshTable(pageContextElements.table);
					$(pageContextElements.modalDeleteGroup).modal("hide");
					AJS.flag({
						type  : "success",
						title : groupMessages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					
					level : "required",
					groupName: {
						required  : true,
						minlength : 1,
						maxlength : 255,
						noSpace   : true,
						accept:/^[^'?\"/\\]*$/,
					}			            
				},

				messages: {
					
					level : groupMessages.levelRequired,
					groupName: {
						required : groupMessages.nameRequired,
						noSpace  : groupMessages.nameNoSpace,
						//minlength: groupMessages.nameMinLength,
						maxlength: groupMessages.nameMaxLength,

					}
				},
				errorPlacement: function(error, element) {
					$(element).parent('div').addClass('has-error');
				    if($(element).hasClass("select")){
				    	$(element).parent().append(error);
				    } else {
				    	$(element).after(error);
				    }
				},
				 success: function(label,element) {
					 $(element).parent('div').removeClass('has-error');
		            },
				submitHandler : function(){
					if(validateFormName === pageContextElements.addGroupForm){
						group.fnAdd(submitActor.id);
					}
					if(validateFormName === pageContextElements.editGroupForm){
						group.fnUpdate();
					}
				}
			});
		},
		groupActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="group.initEdit(\''+row.groupId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="group.initDelete(\''+row.groupId+'\',\''+row.groupName+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		groupNumber: function(value, row, index) {
			return index+1;
		},
		levelNamesActionFormater: function(value, row){
			var levels = "";
			$.each(row.levels,function(index,levelObj){
				if(row.levels.length){
					levels=levels+"<div>"+levelObj.levelName+"</div>";
				}else{
					levels+=levelObj.levelName;
				}
			});
			return levels;
		},
};

$( document ).ready(function() {

	group.formValidate(pageContextElements.addGroupForm);
	group.formValidate(pageContextElements.editGroupForm);

	$submitActors.click(function() {
		submitActor = this;
	});
	group.init();
	//groupFilterObj.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar		 	: "#toolbarGroup",
		url 		 	: "group/page",
		method 			: "post",
		search		 	: false,
		sidePagination	: "server",
		showToggle		: false,
		showColumns		: false,
		pagination		: true,
		searchAlign		: 'left',
		pageSize		: 20,
		formatShowingRows : groupFilterObj.formatShowingRows,
		clickToSelect	: false,
		queryParamsType :'limit',
		//queryParams : groupFilterObj.fiterQueryParmams
		/*pageList		: [10, 15, 25, 50, 100]*/
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});