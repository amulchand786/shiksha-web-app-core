var pageContextElements = {	
		eleAddButton 	: '#addNewActivityName',
		table 			: '#activityTable',
		toolbar			: "#toolbarActivity"
};
var addModal ={
	form 			: '#addActivityForm',
	modal   		: '#mdlAddActivity',
	saveMoreButton	: 'addActivitySaveMoreButton',
	saveButton		: 'addActivitySaveButton',
	eleActivity   : '#addActivityName',
	eleActivityRemarks: '#addActivityRemarks',
	message			: "#divSaveAndAddNewMessage"
};
var editModal ={
		form	 : '#editActivityForm',
		modal	 : '#mdlEditActivity',
		eleActivity: '#editActivity',
		eleActivityId: '#activityId',
		eleActivityRemarks : '#editActivityRemarks'
};
var deleteModal ={
		modal	: '#mdlDeleteActivity',
		confirm : '#deleteActivityButton',
		message	: "#deleteActivityMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var activityIdForDelete;
var activityFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var activity={
		name:'',
		activityId:0,

		init:function(){

			$(pageContextElements.eleAddButton).on('click',activity.initAddModal);
			$(deleteModal.confirm).on('click',activity.deleteFunction);

		},
		deleteFunction : function(){
			activity.fnDelete(activityIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			activity.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleActivity).val(),
					"remarks":$(addModal.eleActivityRemarks).val()
			}
			var addAjaxCall = shiksha.invokeAjax("activity", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : activityMessages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					activity.resetForm(addModal.form);
					activity.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(activityId){
			activity.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("activity/"+activityId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleActivity).val(editAjaxCall.name);
					$(editModal.eleActivityId).val(editAjaxCall.activityId);
					$(editModal.eleActivityRemarks).val(editAjaxCall.remarks);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"			:$(editModal.eleActivity).val(),
					"activityId"	:$(editModal.eleActivityId).val(),
					"remarks"		: $(editModal.eleActivityRemarks).val()
			}

			var updateAjaxCall = shiksha.invokeAjax("activity", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					activity.resetForm(editModal.form);
					activity.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: activityMessages.sucessAlert,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(activityId,name){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			activityIdForDelete=activityId;
		},
		fnDelete :function(activityId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("activity/"+activityId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					activity.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					name: {
						required : true,
						noSpace	 :true,
						minlength : 2,
						maxlength : 50,
					},
					/*remarks: {
						required : true,
						noSpace	 :true,
						minlength : 2,
						maxlength : 50,
					}*/
				
				},

				messages: {
					name: {
						required : activityMessages.nameRequired,
						noSpace  :activityMessages.nameNoSpace,
						minlength : activityMessages.nameMinLength,
						maxlength : activityMessages.nameMaxLength,
					},
					/*remarks: {
						required : activityMessages.remarksRequired,
						noSpace	 : activityMessages.remarksNoSpace,
						minlength : activityMessages.remarksMinLength,
						maxlength : activityMessages.remarksMaxLength,
					}*/
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						activity.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						activity.fnUpdate();
					}
				}
			});
		},
		activityActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="activity.initEdit(\''+row.activityId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="activity.initDelete(\''+row.activityId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		activityNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	activity.formValidate(addModal.form);
	activity.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	activity.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar			: pageContextElements.toolbar,
		url 			: "activity",
		method 			: "get",
		toolbarAlign 	: "right",
		search			: false,
		sidePagination	: "client",
		showToggle		: false,
		showColumns		: false,
		pagination		: true,
		searchAlign		: 'left',
		pageSize		: 20,
		clickToSelect	: false,
		formatShowingRows : activityFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});