
/********************************************************************/

//added by debendra 
//for getting all state and bind to selected state list box
//keep the id of stateListBox as "statesForZone"
var $select;
var json;

var bindStateToListBox =function(bindToElementId,selectedId){
	var jsonData = customAjaxCalling("getStateList", {}, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>None Selected</option>');
	var stateData = JSON.parse(jsonData);
	for (var key in stateData) {				
			if(stateData[key].stateId == selectedId)
				$select.append('<option value="' + stateData[key].stateId + '"selected>' +stateData[key].stateName + '</option>');
			else
				$select.append('<option value="' + stateData[key].stateId + '">' +stateData[key].stateName + '</option>');
	}
};

var getAllState =function(){
	$select	=$('#statesForZone');	
	bindStateToListBox($select,0)
};

var fnGetStates =function(elementId,selectedId){
	$(elementId).empty();
	var	responseData = shiksha.invokeAjax("state/list", null, "GET");
	if(responseData != null){
			var row;
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			$.each(responseData, function(index,obj){
				if(obj.stateId == selectedId){
					row ='<option value="'+obj.stateId+'" data-id="'+obj.stateId+'"data-name="'+obj.stateName+'" selected>'+obj.stateName+'</option>';
				}
				else{
					row ='<option value="'+obj.stateId+'" data-id="'+obj.stateId+'"data-name="'+obj.stateName+'">'+obj.stateName+'</option>';
				}
					$(elementId).append(row);
			})
	}
	spinner.hideSpinner();
};



//@Debendra
//get zones on the basis of state Id and bind them to the select box
//for getting all zone based on state and bind to selected zone list box
//keep the id of zoneListBox as "selectBox_zonesByState"

var bindZoneToListBox =function(stateId,bindToElementId,selectedId){
	var jsonData =customAjaxCalling("zone/"+stateId,null, "GET"); 		
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	for (var key in jsonData) {
		if (jsonData.hasOwnProperty(key)) {
			var val = jsonData[key];
			if(val.zoneId == selectedId)
				$select.append('<option value="' + val.zoneId + '"selected>' + val.zoneName + '</option>');
			else
				$select.append('<option value="' + val.zoneId + '">' + val.zoneName + '</option>');
		}
	}
};

var getAllZoneByState=function(obj){
	$select	=$(obj).parent().next().find('#selectBox_zonesByState');	
	var stateId = $(obj).val();
	bindZoneToListBox(stateId,$select,0);
};


//added by debendra 
//for getting all district based on zone and bind to selected district list box
//keep the id of districtListBox as "selectBox_districtsByZone"
var bindDistrictToListBox =function(zoneId,bindToElementId,selectedId){
	var json = {
			"zoneId" : zoneId
		};
	var jsonData = customAjaxCalling("districtListByZoneId",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var districtData = JSON.parse(jsonData);
	for (var key in districtData) {				
			if(districtData[key].districtId == selectedId)
				$select.append('<option value="' + districtData[key].districtId + '"selected>' +districtData[key].districtName + '</option>');
			else
				$select.append('<option value="' + districtData[key].districtId + '">' +districtData[key].districtName + '</option>');
	}
};


var getAllDistrictByZone =function(obj){	
	$select	=$(obj).parent().next().find('#selectBox_districtsByZone');
	var zoneId = $(obj).val();
	bindDistrictToListBox(zoneId,$select,0)
};

//added by debendra 
//for getting all tehsil based on district and bind to selected tehsil list box
//keep the id of tehsilsListBox as "selectBox_tehsilsByDistrict"
var bindTehsilToListBox =function(districtId,bindToElementId,selectedId){
	var json = {
			"districtId" : districtId
		};	
	var jsonData = customAjaxCalling("tehsilListByDistrictId",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var tehsilsData = JSON.parse(jsonData);
	for (var key in tehsilsData) {				
			if(tehsilsData[key].tehsilId == selectedId)
				$select.append('<option value="' + tehsilsData[key].tehsilId + '"selected>' +tehsilsData[key].tehsilName+ '</option>');
			else
				$select.append('<option value="' + tehsilsData[key].tehsilId + '">' +tehsilsData[key].tehsilName + '</option>');
	}
};

var getAllTehsilsByDistrict =function(obj){	
	$select	=$(obj).parent().next().find('#selectBox_tehsilsByDistrict');
	var districtId = $(obj).val();
	bindTehsilToListBox(districtId,$select,0)
};



//added by debendra 
//for getting all block based on tehsil and bind to selected block list box
//keep the id of blockListBox as "selectBox_blocksByTehsil"

var bindBlocksToListBox =function(tehsilId,bindToElementId,selectedId){
	var json = {
			"tehsilId" : tehsilId
		};	
	var jsonData = customAjaxCalling("blockListByTehsilId",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var blocksData = JSON.parse(jsonData);
	for (var key in blocksData) {				
			if(blocksData[key].blockId == selectedId)
				$select.append('<option value="' + blocksData[key].blockId + '"selected>' +blocksData[key].blockName+ '</option>');
			else
				$select.append('<option value="' + blocksData[key].blockId + '">' +blocksData[key].blockName+ '</option>');
	}
};

var getAllBlockByTehsil =function(obj){	
	$select	=$(obj).parent().next().find('#selectBox_blocksByTehsil');
	var tehsilId = $(obj).val();
	bindBlocksToListBox(tehsilId,$select,0)
};


//added by debendra 
//for getting all NyayPanchayat based on block and bind to selected NyayPanchayat list box
//keep the id of nyaPanchayatListBox as "selectBox_npByBlock"
var bindNpToListBox =function(blockId,bindToElementId,selectedId){
	var json = {
			"blockId" : blockId
		};	
	var jsonData = customAjaxCalling("nyayPanchayatListByBlockId",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var nyaPanchayatData = JSON.parse(jsonData);
	for (var key in nyaPanchayatData) {				
			if(nyaPanchayatData[key].nyayPanchayatId == selectedId)
				$select.append('<option value="' +nyaPanchayatData[key].nyayPanchayatId+ '"selected>' +nyaPanchayatData[key].nyayPanchayatName+ '</option>');
			else
				$select.append('<option value="' + nyaPanchayatData[key].nyayPanchayatId + '">' +nyaPanchayatData[key].nyayPanchayatName+ '</option>');
	}
};

var getAllNPByBlock =function(obj){	
	
	$select	=$('select#selectBox_npByBlock');
	var blockId = $(obj).val();
	bindNpToListBox(blockId,$select,0)
};




//added by debendra 
//for getting all GP_Panchayat based on NyayPanchayat and bind to selected gramPanchayat list box
//keep the id of gramPanchayatListBox as "selectBox_gpByNp"
var bindGPToListBox =function(npId,bindToElementId,selectedId){
	var json = {
			"nyayPanchayatId" : npId
		};	
	var jsonData = customAjaxCalling("gramPanchayatListByNpId",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var gramPanchayatData = JSON.parse(jsonData);
	for (var key in gramPanchayatData) {				
			if(gramPanchayatData[key].gramPanchayatId == selectedId)
				$select.append('<option value="' +gramPanchayatData[key].gramPanchayatId+ '"selected>' +gramPanchayatData[key].gramPanchayatName+ '</option>');
			else
				$select.append('<option value="' + gramPanchayatData[key].gramPanchayatId + '">' +gramPanchayatData[key].gramPanchayatName+ '</option>');
	}
};

var getAllGpByNp =function(obj){	
	$select	=$(obj).parent().next().find('#selectBox_gpByNp');
	var npId = $(obj).val();
	bindGPToListBox(npId,$select,0)
};



 
//for getting all RevenueVillage based on GramPanchayat and bind to selected RevenueVillage list box
//keep the id of RevenueVillageListBox as "selectBox_revenueVillagebyGp"
var bindRevenueVilageToListBox =function(gpId,bindToElementId,selectedId){
	var json = {
			"gramPanchayatId" : gpId
		};	
	var jsonData = customAjaxCalling("revenueVillageListByGPId",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var revenueVillageData = JSON.parse(jsonData);
	for (var key in revenueVillageData) {				
			if(revenueVillageData[key].revenueVillageId == selectedId)
				$select.append('<option value="' +revenueVillageData[key].revenueVillageId+ '"selected>' +revenueVillageData[key].revenueVillageName+ '</option>');
			else
				$select.append('<option value="' + revenueVillageData[key].revenueVillageId + '">' +revenueVillageData[key].revenueVillageName+ '</option>');
	}
};

var getAllRevenueVillageByGp =function(obj){	
	$select	=$(obj).parent().next().find('#selectBox_revenueVillagebyGp');
	var gpId = $(obj).val();
	bindRevenueVilageToListBox(gpId,$select,0)
};


//for getting all Village based on RV
//keep the id of VillageListBox as "selectBox_villagebyRv"
var bindVilageToListBox =function(rVId,bindToElementId,selectedId){
	var json = {
			"revenueVillageId" : rVId
		};	
	var jsonData = customAjaxCalling("villageListByRVId",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var villageData = JSON.parse(jsonData);
	for (var key in villageData) {				
			if(villageData[key].villageId == selectedId)
				$select.append('<option value="' +villageData[key].villageId+ '"selected>' +villageData[key].villageName+ '</option>');
			else
				$select.append('<option value="' +villageData[key].villageId+ '">' +villageData[key].villageName+ '</option>');
			}
};


var getAllVillageByRv =function(obj){	
	$select	=$('select#selectBox_villagebyRv');
	var rVId = $(obj).val();
	bindVilageToListBox(rVId,$select,0)
};



//for getting all Schools based on City
//keep the id of SchoolListBox as "selectBox_schools"
var bindSchoolByCityToListBox =function(cityId,bindToElementId,selectedId){
	var json = {
			"cityId" : cityId
		};	
	var jsonData = customAjaxCalling("schoolsByCity",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var schoolData = JSON.parse(jsonData);
	for (var key in schoolData) {				
		if(schoolData[key].schoolId == selectedId)
			$select.append('<option value="'+schoolData[key].schoolId+'" data-id="'+schoolData[key].schoolId+'" data-name="'+schoolData[key].schoolName+'" data-code="'+schoolData[key].schoolCode+'"data-parentCode="'+schoolData[key].parentCode+'" data-parentid="'+schoolData[key].parentId+'" data-parenttype="'+schoolData[key].parentType+'" data-district="'+schoolData[key].districtName+'" data-state="'+schoolData[key].stateName+' selected ">'+schoolData[key].schoolName+'</option>');				
		else
			$select.append('<option value="'+schoolData[key].schoolId+'" data-id="'+schoolData[key].schoolId+'" data-name="'+schoolData[key].schoolName+'" data-code="'+schoolData[key].schoolCode+'"data-parentCode="'+schoolData[key].parentCode+'" data-parentid="'+schoolData[key].parentId+'" data-parenttype="'+schoolData[key].parentType+'" data-district="'+schoolData[key].districtName+'" data-state="'+schoolData[key].stateName+'">'+schoolData[key].schoolName+'</option>');
	}
};

var getAllSchoolsByCity =function(obj){	
	$select	=$('select#selectBox_schools');
	var cityId = $(obj).val();
	bindSchoolByCityToListBox(cityId,$select,0)
};




//for getting all Schools based on village
var bindSchoolByVillageToListBox =function(villageId,bindToElementId,selectedId){
	var json = {
			"villageId" : rVId
		};	
	var jsonData = customAjaxCalling("schoolsByVillage",json, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var schoolData = JSON.parse(jsonData);
	for (var key in schoolData) {				
			if(schoolData[key].schoolId == selectedId)
				$select.append('<option value="'+schoolData[key].schoolId+'" data-id="'+schoolData[key].schoolId+'" data-name="'+schoolData[key].schoolName+'" data-code="'+schoolData[key].schoolCode+'"data-parentCode="'+schoolData[key].parentCode+'" data-parentid="'+schoolData[key].parentId+'" data-parenttype="'+schoolData[key].parentType+'" data-district="'+schoolData[key].districtName+'" data-state="'+schoolData[key].stateName+' selected ">'+schoolData[key].schoolName+'</option>');				
			else
				$select.append('<option value="'+schoolData[key].schoolId+'" data-id="'+schoolData[key].schoolId+'" data-name="'+schoolData[key].schoolName+'" data-code="'+schoolData[key].schoolCode+'"data-parentCode="'+schoolData[key].parentCode+'" data-parentid="'+schoolData[key].parentId+'" data-parenttype="'+schoolData[key].parentType+'" data-district="'+schoolData[key].districtName+'" data-state="'+schoolData[key].stateName+'">'+schoolData[key].schoolName+'</option>');
	}
};


var getAllSchoolsByVillage =function(obj){	
	$select	=$('select#selectBox_schools');
	var villageId = $(obj).val();
	bindSchoolByVillageToListBox(villageId,$select,0)
};





//for getting all schoolType
var bindSchoolTypeToListBox =function(bindToElementId,selectedId){	
	var jsonData = customAjaxCalling("schooltype",null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>Select</option>');
	
	$.each(jsonData,function(index,obj){
		if(obj.schoolTypeId == selectedId){
			$select.append('<option data-id="' + obj.schoolTypeId + '" value="' + obj.schoolTypeId + ' selected">' +obj.schoolTypeName + '</option>');
		}else{
			$select.append('<option data-id="' + obj.schoolTypeId + '" value="' + obj.schoolTypeId + '">' +obj.schoolTypeName + '</option>');	
		}
		
	});
};

var getAllSchoolType =function(){	
	$select	=$('select#selectBox_schoolType');	
	bindSchoolTypeToListBox($select,0)
};


var bindSchoolTypeForCampaignToListBox =function(bindToElementId){	
	var jsonData = customAjaxCalling("schooltype",null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	$.each(jsonData,function(index,obj){
		$select.append('<option data-id="' + obj.schoolTypeId + '" value="' + obj.schoolTypeId + '">' +obj.schoolTypeName + '</option>');
	});
};




//for getting all schoolTypeLocation
var bindSchoolTypeLocToListBox =function(bindToElementId,selectedId){
	
	var jsonData = customAjaxCalling("schoollocationtype",null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	var schoolLocType = JSON.parse(jsonData);
	for (var key in schoolLocType) {				
			if(schoolLocType[key].id == selectedId)
				$select.append('<option value="'+schoolLocType[key].id+'" data-id="'+schoolLocType[key].id+' "data-code="'+schoolLocType[key].code+'" data-levelName="'+schoolLocType[key].levelName+'"selected ">'+schoolLocType[key].name+'</option>');				
			else
				$select.append('<option value="'+schoolLocType[key].id+'" data-id="'+schoolLocType[key].id+'"data-code="'+schoolLocType[key].code+'"data-levelName="'+schoolLocType[key].levelName+'">'+schoolLocType[key].name+'</option>');
	}
};

var getAllSchoolTypeLocation =function(){	
	$select	=$('select#selectBox_schoolLocationType');	
	bindSchoolTypeLocToListBox($select,0)
};






/********************************************************************/


//clear child list box on basis of parent list box changed and display default
var  clearAllListBoxOnChange=function() {
	  for (var i = 0; i < arguments.length; i++) {
		  arguments[i].empty();
		  $select	=arguments[i];	
		  $select.html('');
		 $select.find("option").not("option[value='NONE']").remove().attr("selected","selected");
		 
		  $select.append('<option value="NONE" selected disabled hidden>-- Select--</option>');
		  $select.parent().removeClass("has-success").find(".glyphicon-ok").hide();
		  
	  }
	  
	  
	  
	
}

//for getting all Schools based on village






var bindSchoolByVillageToListBox =function(villageId,bindToElementId,selectedId){		
	var json = {		
			"villageId" : rVId		
		};			
	var jsonData = customAjaxCalling("schoolsByVillage",json, "POST");		
	$select	=bindToElementId;			
	$select.html('');		
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');		
	var schoolData = JSON.parse(jsonData);		
	for (var key in schoolData) {						
			if(schoolData[key].schoolId == selectedId)		
				$select.append('<option value="'+schoolData[key].schoolId+'" data-id="'+schoolData[key].schoolId+'" data-name="'+schoolData[key].schoolName+'" data-code="'+schoolData[key].schoolCode+'"data-parentCode="'+schoolData[key].parentCode+'" data-parentid="'+schoolData[key].parentId+'" data-parenttype="'+schoolData[key].parentType+'" data-district="'+schoolData[key].districtName+'" data-state="'+schoolData[key].stateName+' selected ">'+schoolData[key].schoolName+'</option>');						
			else		
				$select.append('<option value="'+schoolData[key].schoolId+'" data-id="'+schoolData[key].schoolId+'" data-name="'+schoolData[key].schoolName+'" data-code="'+schoolData[key].schoolCode+'"data-parentCode="'+schoolData[key].parentCode+'" data-parentid="'+schoolData[key].parentId+'" data-parenttype="'+schoolData[key].parentType+'" data-district="'+schoolData[key].districtName+'" data-state="'+schoolData[key].stateName+'">'+schoolData[key].schoolName+'</option>');		
	}		
};


var getAllSourceBySchools =function(obj){	
	$select	=$('select#selectBox_schools');
	var villageId = $(obj).val();
	bindSchoolByVillageToListBox(villageId,$select,0)
};

//@Mukteshwar
var bindSourceToListBox =function(bindToElementId,selectedId){
	var jsonData = customAjaxCalling("getSourceList", {}, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var sourceData = JSON.parse(jsonData);
	for (var key in sourceData) {				
			if(sourceData[key].sourceId == selectedId)
				$select.append('<option value="' + sourceData[key].sourceId + '"selected>' +sourceData[key].sourceName + '</option>');
			else
				$select.append('<option value="' + sourceData[key].sourceId + '">' +sourceData[key].sourceName + '</option>');
	}
};

var bindStageToListBox =function(bindToElementId,selectedId){
	var jsonData = customAjaxCalling("getStageList", {}, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var stageData = JSON.parse(jsonData);
	for (var key in stageData) {				
						if(stageData[key].stageId == selectedId)
							$select.append('<option value="' + stageData[key].stageId + '"selected>' +stageData[key].stageName + '</option>');
						else
							$select.append('<option value="' + stageData[key].stageId + '">' +stageData[key].stageName + '</option>');
				}
};


var bindGradeToListBox =function(bindToElementId,selectedId){
	var jsonData = customAjaxCalling("getGradeList", {}, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>Select</option>');
	var gradeData = JSON.parse(jsonData);
	for (var key in gradeData) {				
			if(gradeData[key].gradeId == selectedId)
				$select.append('<option data-id="' + gradeData[key].gradeId + '" value="' + gradeData[key].gradeId + '"selected>' +gradeData[key].gradeName + '</option>');
			else
				$select.append('<option data-id="' + gradeData[key].gradeId + '" value="' + gradeData[key].gradeId + '">' +gradeData[key].gradeName + '</option>');
	}
};


//get grades by school
//get the grades from school grade section mapper on basis of schoolId
var bindGradesBySchoolToListBox =function(bindToElementId,selectedSchoolId,selectedId){
	var grades = customAjaxCalling("gradelist/" + selectedSchoolId,null,"GET");
	if(grades!=null || grades!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(grades,function(index,obj){
			$select.append('<option value="' + obj.gradeId + '" data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '">' +obj.gradeName+ '</option>');
		});
	}
};
var bindSourceBySchoolAndGradeToListBox = function(bindToElementId,selectedSchoolId,selectedGradeId)
{	
	 json ={
			"schoolId": selectedSchoolId,
			"gradeId": selectedGradeId
	}
	var sections = customAjaxCalling("sectionlist", json,"POST");
	if(sections!=null || sections!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(sections,function(index,obj){
			$select.append('<option value="' + obj.sectionId + '" data-id="' + obj.sectionId + '"data-gradename="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
		});
	}
	
	}
//Get All academic Year List & make current year as selecte
var bindAcademicYearToLustBix =function(bindToElementId,currentYearId){
	var academicYearData = customAjaxCalling("currentAndPastAcademicYears", null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	
	
	for (var key in academicYearData) {				
			if(academicYearData[key].academicYearId == currentYearId)
				$select.append('<option data-id="' + academicYearData[key].academicYearId + '" value="' + academicYearData[key].academicYearId + '"selected>' +academicYearData[key].academicYear + '</option>');
			else
				$select.append('<option data-id="' + academicYearData[key].academicYearId + '" value="' + academicYearData[key].academicYearId + '">' +academicYearData[key].academicYear + '</option>');
	}
};





//get subjects by grade
//get the subjects from  grade subject unit mapper on basis of gradeId
var bindSubjectByGradeToListBox =function(bindToElementId,selectedGradeId){
	
	var subjects =customAjaxCalling("subject/"+selectedGradeId,null,'GET');
	
	if(subjects!=null || subjects!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(subjects,function(index,obj){
			$select.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-subjectname="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
		});
	}
};


//get subjects by grade
//get the subjects from  grade subject unit mapper on basis of gradeId
var bindUnitsByGradeAndSub =function(bindToElementId,selectedGradeId,selectedSubId){	
	var units =customAjaxCalling("unit/"+selectedGradeId+"/"+selectedSubId,null,'GET');	
	if(units!=null || units!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(units,function(index,obj){
			$select.append('<option value="'+obj.unitId+'" data-id="' + obj.unitId + '"data-unitname="' +obj.unitName+ '">' +escapeHtmlCharacters(obj.unitName)+ '</option>');
		});
	}
};






var bindSubjectToListBox =function(bindToElementId,selectedId){
	
	var jsonData = customAjaxCalling("getSubjectList", {}, "POST");
	
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var subjectData = JSON.parse(jsonData);
	for (var key in subjectData) {				
			if(subjectData[key].subjectId == selectedId)
				$select.append('<option data-id="' + subjectData[key].subjectId + '" value="' + subjectData[key].subjectId + '"selected>' +subjectData[key].subjectName + '</option>');
			else
				$select.append('<option data-id="' + subjectData[key].subjectId + '" value="' + subjectData[key].subjectId + '">' +subjectData[key].subjectName + '</option>');
	}
};

var bindUnitToListBox =function(bindToElementId,selectedId){
	var jsonData = customAjaxCalling("getUnitList", {}, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>--Select--</option>');
	var unitData = JSON.parse(jsonData);
	for (var key in unitData) {				
			if(unitData[key].unitId == selectedId)
				$select.append('<option data-id="' + unitData[key].unitId + '" value="' + unitData[key].unitId + '"selected>' +unitData[key].unitName + '</option>');
			else
				$select.append('<option data-id="' + unitData[key].unitId + '" value="' + unitData[key].unitId + '">' +unitData[key].unitName + '</option>');
	}
};

//bind units to respective select box
var bindUnitsToListBox =function(bindToElementId){
	var unitData = customAjaxCalling("unit/list", null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	$.each(unitData,function(index,obj){
		$select.append('<option data-id="' + obj.unitId + '" value="' + obj.unitId + '">' +obj.unitName + '</option>');
	});			
};

//bind sections to respective select box
var bindSectionToListBox =function(bindToElementId){
	var sectionData = customAjaxCalling("section/list", null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	$.each(sectionData,function(index,obj){
		$select.append('<option data-id="' + obj.sectionId + '" value="' + obj.sectionId + '">' +obj.sectionName + '</option>');
	});			
};





//get schools by locations and types
//locations may be villages /cities and types can be pvt,public..
var getAndBindSchoolByLocAndType =function(bindToElementId,selectedLocType,locations,schoolTypes){
	var requestData={
			"locations":locations,
			"schoolTypes":schoolTypes			
	};
	var schools = customAjaxCalling("school/"+selectedLocType, requestData, "POST");
	$select	=bindToElementId;	
	$select.html('');
	if(schools!=null && schools !=""){
		$.each(schools,function(index,obj){
			var cityName =(obj.cityName==null)?"N/A":obj.cityName ;
			var villageName =(obj.villageName==null)?"N/A":obj.villageName;
			$select.append('<option data-id="' + obj.id 
					+ '" value="' + obj.id
					+ '"data-statename="' + obj.stateName 
					+ '"data-zonename="' + obj.zoneName 
					+ '"data-distname="' + obj.districtName 
					+ '"data-tehsilname="' + obj.tehsilName 
					+ '"data-blockname="' + obj.blockName 
					+ '"data-cityname="' + cityName
					+ '"data-nyaypanchayatname="' + obj.nyayPanchayatName
					+ '"data-grampanchayatname="' + obj.gramPanchayatName
					+ '"data-revenuevillagename="' + obj.revenueVillageName
					+ '"data-villagename="' + villageName 
					+ '" value="' + obj.id + '">' +obj.schoolName + '</option>');
		});		
	}
	return schools;
}

var getAndBindAssessmentByParentFilter =function(bindToElementId,sourceIds,stageIds,gradeIds,subjectIds,unitIds){
	var requestData={
			"sourceIds":sourceIds,
			"stageIds":stageIds,
			"gradeIds":gradeIds,
			"subjectIds":subjectIds,
			"unitIds":unitIds
	};
	var assessments = customAjaxCalling("getAssessmentList", requestData, "POST");
	$select	=bindToElementId;	
	$select.html('');
	if(assessments!=null && assessments!=""){
		$.each(assessments,function(index,obj){
			$select.append('<option value="'+obj.assessmentId+'" data-id="' + obj.assessmentId + '"data-sgCode="' + obj.sgAssessmentCode +'"data-sourcename="' + obj.sourceName +'"data-stagename="' + obj.stageName +'"data-gradename="' + obj.gradeName + '"data-subjectname="' + obj.subjectName +'"data-unitname="' + obj.unitName + '"  value="' + obj.assessmentId + '">' +obj.assessmentName + '</option>');
		});	
	}	
}


var bindSequenceNumberFromGradeToListBox =function(pBindToElementId,paSelectedId){

	var jsonData = customAjaxCalling("getSequenceNumberList", null, "GET");
	$select	=pBindToElementId;	
	$select.html('');
	$select.append('<option value="null" selected disabled hidden>--Select--</option>');
	if(paSelectedId != 0 ){
		$select.append('<option value="'+paSelectedId+'" selected>' +paSelectedId+ '</option>');
	}
	$.each(jsonData,function(i,val){
	$select.append('<option value="'+val+'">' +val+ '</option>');
	});
	$(pBindToElementId).multiselect('refresh');
	$(pBindToElementId).multiselect('destroy');
	
	
	
};

//Get All academic Year List & make current year as selecte
var bindAcademicYearToListBox =function(bindToElementId){
	var academicYearData = customAjaxCalling("currentAndPastAcademicYears", null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option value="NONE" selected disabled hidden>Select</option>');
	
	for(var key in academicYearData) {				
			$select.append('<option data-id="' + academicYearData[key].academicYearId + '" value="' + academicYearData[key].academicYearId + '">' +academicYearData[key].academicYear + '</option>');
	}
};

var bindStudentBySchoolGradeSectionAcaYearToListBox = function(bindToElementId,schoolId,gradeId,sectionId,academicYearId){
	{	
		var json = {
				"schoolId" : schoolId,
				"gradeId" : gradeId,
				"sectionId" : sectionId,
				"academicYearId":academicYearId
		};

		var result = customAjaxCalling("studentlistSortByName", json, "POST");
		if(result!=null || result!=""){
			$select	=bindToElementId;
			$select.html('');
			$.each(result,function(index,obj){
				$select.append('<option data-id="' + obj.studentId + '"data-fathername="' + obj.fatherName+ '"data-rollnumber="' + obj.rollNumber+ '"data-studentname="' + obj.name+ '">' +obj.name+ '</option>');
			});
		}
		
		}
	}



//shiksha+ drop-down utility 
var shikshaPlus ={
		fnGetLevelsForChapterFilter :function(elementId,selectedIds){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("level/list", null, "GET");
			
			if(responseData != null){
					var row;
					if(typeof selectedIds != 'object'){
						selectedIds = [selectedIds];
					}
					$.each(responseData, function(index,obj){
						
						if(selectedIds.indexOf(obj.levelId) > -1){
							row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'"  selected>'+obj.levelName+'</option>';
						}
						else{
							row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'">'+obj.levelName+'</option>';
						}
						$(elementId).append(row);
					})
					spinner.hideSpinner();
			}
		},
		
		fnGetGroupsForChapterFilter :function(elementId,selectedId){
			$(elementId).empty();
			var excludedIds=[];
			var	responseData = shiksha.invokeAjax("group/list?exclude="+excludedIds, null, "POST");
			if(responseData != null){
					var row;
					$.each(responseData, function(index,obj){
						if(obj.groupId == selectedId){
							row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'" selected>'+obj.groupName+'</option>';
						}
						else{
							row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'">'+obj.groupName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetLevels :function(elementId,selectedIds){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("level/list", null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
					var row = '<option value="0">None Selected</option>';
					if(typeof selectedIds != 'object'){
						selectedIds = [selectedIds];
					}
					$.each(responseData, function(index,obj){
						
						if(selectedIds.indexOf(obj.levelId) > -1){
							row ='<option value="'+obj.levelId+'" sequenceNumber="'+obj.sequenceNumber+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'"  selected>'+obj.levelName+'</option>';
						}
						else{
							row ='<option value="'+obj.levelId+'" sequenceNumber="'+obj.sequenceNumber+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'">'+obj.levelName+'</option>';
						}
						$(elementId).append(row);
					})
					spinner.hideSpinner();
			}
		},
		
		fnGetGroups :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("group/list", null, "GET");
			if(responseData != null){
					var row;
					//$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.groupId == selectedId){
							row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'" selected>'+obj.groupName+'</option>';
						}
						else{
							row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'">'+obj.groupName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetGroupsByLevelId :function(elementId,selectedId,levelId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("group/level/"+levelId, null, "GET");
			if(responseData != null){
				var row;
				$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
				$.each(responseData, function(index,obj){
					if(obj.groupId == selectedId){
						row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'" selected>'+obj.groupName+'</option>';
					}
					else{
						row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'">'+obj.groupName+'</option>';
					}
					$(elementId).append(row);
				});
				if(responseData.length==0){
					AJS.flag({
						type : "error",
						title : appMessgaes.oops,
						body : appMessgaes.groupNotExist,
						close :'auto'
					})
				}
			}
			spinner.hideSpinner();
		},
		fnGetGroupsByLevelIdInFilter :function(elementId,selectedId,levelId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("group/level/"+levelId, null, "GET");
			if(responseData != null){
					var row;
					$.each(responseData, function(index,obj){
						if(obj.groupId == selectedId){
							row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'" selected>'+obj.groupName+'</option>';
						}
						else{
							row ='<option value="'+obj.groupId+'" data-id="'+obj.groupId+'"data-name="'+obj.groupName+'">'+obj.groupName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetSegmentTypes :function(elementId,selectedId,presentSegmentTypeId){
			$(elementId).empty();
			var excludedIds=[];
			if(typeof selectedId != 'object'){
				excludedIds = [selectedId];
			}
			var	responseData = shiksha.invokeAjax("segment/type?exclude="+excludedIds, null, "GET");
			if(responseData != null){
					var row;
					//$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					
					$.each(responseData, function(index,obj){
						if(obj.segmentTypeId == presentSegmentTypeId){
							row ='<option value="'+obj.segmentTypeId+'" data-id="'+obj.segmentTypeId+'"data-name="'+obj.segmentTypeName+'" selected>'+obj.segmentTypeName+'</option>';
						}
						else{
							row ='<option value="'+obj.segmentTypeId+'" data-id="'+obj.segmentTypeId+'"data-name="'+obj.segmentTypeName+'">'+obj.segmentTypeName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetContentTypes :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("content/type/list", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.contentTypeId == selectedId){
							row ='<option value="'+obj.contentTypeId+'" data-id="'+obj.contentTypeId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option value="'+obj.contentTypeId+'" data-id="'+obj.contentTypeId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetSegments :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("segment/list", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.segmentId == selectedId){
							row ='<option value="'+obj.segmentId+'" data-id="'+obj.segmentId+'"data-name="'+obj.segmentName+'" selected>'+obj.segmentName+'</option>';
						}
						else{
							row ='<option value="'+obj.segmentId+'" data-id="'+obj.segmentId+'"data-name="'+obj.segmentName+'">'+obj.segmentName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetSources :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("shikshaplus/source/list", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.sourceId == selectedId){
							row ='<option value="'+obj.sourceId+'" data-id="'+obj.sourceId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option value="'+obj.sourceId+'" data-id="'+obj.sourceId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetChapters:function(elementId,selectedId,levelId,groupId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("shikshaplus/chapter/?level="+levelId+"&group="+groupId+"", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.chapterId == selectedId){
							row ='<option value="'+obj.chapterId+'" data-id="'+obj.chapterId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option value="'+obj.chapterId+'" data-id="'+obj.chapterId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetNuggets :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("shikshaplus/nugget/page", null, "POST");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData.rows, function(index,obj){
						if(obj.nuggetId == selectedId){
							row ='<option value="'+obj.nuggetId+'" data-id="'+obj.nuggetId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option value="'+obj.nuggetId+'" data-id="'+obj.nuggetId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetSegmentTypesForShikshaPlusSegment :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("segment/type/list", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					
					$.each(responseData, function(index,obj){
						if(obj.segmentTypeId == selectedId){
							row ='<option value="'+obj.segmentTypeId+'" data-id="'+obj.segmentTypeId+'"data-name="'+obj.segmentTypeName+'" selected>'+obj.segmentTypeName+'</option>';
						}
						else{
							row ='<option value="'+obj.segmentTypeId+'" data-id="'+obj.segmentTypeId+'"data-name="'+obj.segmentTypeName+'">'+obj.segmentTypeName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetBatches :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("batch/list", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.batchId == selectedId){
							row ='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'" selected>'+obj.batchName+'</option>';
						}
						else{
							row ='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'">'+obj.batchName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetAcademicCycles :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.flagCurrentAC == true){
							row ='<option data-iscurrent="true" data-start-date="'+obj.startDate+'" data-end-date="'+obj.endDate+'" value="'+obj.academicCycleId+'" data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option data-iscurrent="false" data-start-date="'+obj.startDate+'" data-end-date="'+obj.endDate+'" value="'+obj.academicCycleId+'" data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetAcademicCyclesUptoCurrent :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academiccycle", null, "POST");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.flagCurrentAC == true){
							row ='<option data-iscurrent="true" data-start-date="'+obj.startDate+'" data-end-date="'+obj.endDate+'" value="'+obj.academicCycleId+'" data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option data-iscurrent="false" data-start-date="'+obj.startDate+'" data-end-date="'+obj.endDate+'" value="'+obj.academicCycleId+'" data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		
		fnGetAcademicCyclesByYear :function(elementId,selectedId,year){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle/year/"+year, null, "GET");
			if(responseData != null?responseData.length>0:false){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.flagCurrentAC == true){
							row ='<option value="'+obj.academicCycleId+'" data-cycletype="'+obj.cycleType+'" data-current="'+obj.flagCurrentAC+'"  data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option value="'+obj.academicCycleId+'" data-cycletype="'+obj.cycleType+'" data-current="'+obj.flagCurrentAC+'" data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}else{
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.academicCyclesNotExist,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		fnGetAcademicCyclesByYearAndBatch :function(elementId,selectedId,data){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle/", data, "POST");
			if(responseData != null?responseData.length>0:false){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.flagCurrentAC == true){
							row ='<option value="'+obj.academicCycleId+'" data-cycletype="'+obj.cycleType+'" data-current="'+obj.flagCurrentAC+'"  data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
						}
						else{
							row ='<option value="'+obj.academicCycleId+'" data-cycletype="'+obj.cycleType+'" data-current="'+obj.flagCurrentAC+'" data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'">'+obj.name+'</option>';
						}
							$(elementId).append(row);
					})
			}else{
				AJS.flag({
					type : "error",
					title : appMessgaes.oops,
					body : appMessgaes.academicCyclesNotExist,
					close :'auto'
				})
			}
			spinner.hideSpinner();
		},
		fnGetYears :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle/year", null, "GET");
			if(responseData != null){
				var row;
				$(elementId).append('<option value="NONE" selected disabled >None Selected</option>');
				$.each(responseData, function(index,value){
					
					if(index== selectedId){
					row ='<option value="'+index+'" data-id="'+index+'"data-name="'+index+'" selected>'+index+'</option>';
				}
				else{
					row ='<option value="'+index+'" data-id="'+index+'"data-name="'+index+'">'+index+'</option>';
				}
					$(elementId).append(row);
			})
			}
			spinner.hideSpinner();
		},
		fnGetCurrentYear :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle/year", null, "GET");
			if(responseData != null){
				var row;
				$(elementId).append('<option value="NONE" selected disabled >None Selected</option>');
				$.each(responseData, function(index,value){
					
				if(value){
					row ='<option value="'+index+'" data-id="'+index+'"data-name="'+index+'"selected >'+index+'</option>';
					$(elementId).append(row);
				}
				
					
			})
			}
			spinner.hideSpinner();
		},
		fnGetShikshaSources :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("source/list", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.sourceId == selectedId){
							row ='<option value="'+obj.sourceId+'" data-id="'+obj.sourceId+'"data-name="'+obj.sourceName+'" selected>'+obj.sourceName+'</option>';
						}
						else{
							row ='<option value="'+obj.sourceId+'" data-id="'+obj.sourceId+'"data-name="'+obj.sourceName+'">'+obj.sourceName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetGrades :function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("getGradeList", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.gradeId == selectedId){
							row ='<option value="'+obj.gradeId+'" data-id="'+obj.gradeId+'"data-name="'+obj.gradeName+'" selected>'+obj.gradeName+'</option>';
						}
						else{
							row ='<option value="'+obj.gradeId+'" data-id="'+obj.gradeId+'"data-name="'+obj.gradeName+'">'+obj.gradeName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetSubjectsByGradeId : function(elementId,selectedId,gradeId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("subject/"+gradeId, null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.subjectId == selectedId){
							row ='<option value="'+obj.subjectId+'" data-id="'+obj.subjectId+'"data-name="'+obj.subjectName+'" selected>'+obj.subjectName+'</option>';
						}
						else{
							row ='<option value="'+obj.subjectId+'" data-id="'+obj.subjectId+'"data-name="'+obj.subjectName+'">'+obj.subjectName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetUnits:function(elementId,selectedId,gradeId,subjectId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("/unit/"+gradeId+"/"+subjectId+"", null, "GET");
			if(responseData != null){
					var row;
					$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
					$.each(responseData, function(index,obj){
						if(obj.unitId == selectedId){
							row ='<option value="'+obj.unitId+'" data-id="'+obj.unitId+'"data-name="'+obj.unitName+'" selected>'+obj.unitName+'</option>';
						}
						else{
							row ='<option value="'+obj.unitId+'" data-id="'+obj.unitId+'"data-name="'+obj.unitName+'">'+obj.unitName+'</option>';
						}
							$(elementId).append(row);
					})
			}
			spinner.hideSpinner();
		},
		fnGetCurrentACYear:function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle/?type=current",null, "GET");
			if(responseData != null){
				var row;
				$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
				row ='<option value="'+responseData.academicCycleId+'" data-id="'+responseData.academicCycleId+'"data-name="'+responseData.name+'" selected>'+responseData.name+'</option>';
				$(elementId).append(row);
				
		}
			spinner.hideSpinner();
		},
		fnGetNextACYear:function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle/?type=future",null, "GET");
			if(responseData != null){
				var row;
				$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
				row ='<option value="'+responseData.academicCycleId+'" data-id="'+responseData.academicCycleId+'"data-name="'+responseData.name+'" selected>'+responseData.name+'</option>';
				$(elementId).append(row);
				
		}
			spinner.hideSpinner();
		},
		fnGetPreviousACYear:function(elementId,selectedId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("academicCycle/?type=previous",null, "GET");
			if(responseData != null){
				var row;
				$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
				row ='<option value="'+responseData.academicCycleId+'" data-id="'+responseData.academicCycleId+'"data-name="'+responseData.name+'" selected>'+responseData.name+'</option>';
				$(elementId).append(row);
				
		}
			spinner.hideSpinner();
		},
		fnGetBatchesByCenterAndLevel :function(elementId,selectedId,centerId,levelId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("batch/center/"+centerId+"/level/"+levelId, null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
					var row = '<option value="0">None Selected</option>';
					$.each(responseData, function(index,obj){
							if(obj.batchId == selectedId){
								row ='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'" selected>'+obj.batchName+'</option>';
							}
							else{
								row ='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'">'+obj.batchName+'</option>';
							}
								$(elementId).append(row);
						})
					spinner.hideSpinner();
			}
		},
		fnGetBatchesByCenterAndLevelInFilter :function(elementId,selectedId,centerId,levelId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("batch/center/"+centerId+"/level/"+levelId, null, "GET");
			if(responseData != null){
					var row = '<option value="0">None Selected</option>';
					$.each(responseData, function(index,obj){
							if(obj.batchId == selectedId){
								row ='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'" selected>'+obj.batchName+'</option>';
							}
							else{
								row ='<option value="'+obj.batchId+'" data-id="'+obj.batchId+'"data-name="'+obj.batchName+'">'+obj.batchName+'</option>';
							}
								$(elementId).append(row);
						})
					spinner.hideSpinner();
			}
		},
		fnGetLevelsByCenter :function(elementId,selectedId,centerId){
			$(elementId).empty();
			center=[centerId];
			var	responseData = shiksha.invokeAjax("level/?center="+center, null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
					var row = '<option value="0">None Selected</option>';
					$.each(responseData, function(index,obj){
							if(obj.levelId == selectedId){
								row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'" selected>'+obj.levelName+'</option>';
							}
							else{
								row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'">'+obj.levelName+'</option>';
							}
								$(elementId).append(row);
						})
					spinner.hideSpinner();
			}
		},
		fnGetLevelsByCenterinFilter :function(elementId,selectedId,centerId){
			$(elementId).empty();
			center=[centerId];
			var	responseData = shiksha.invokeAjax("level/?center="+center, null, "GET");
			if(responseData != null){
					var row = '<option value="0">None Selected</option>';
					$.each(responseData, function(index,obj){
							if(obj.levelId == selectedId){
								row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'" selected>'+obj.levelName+'</option>';
							}
							else{
								row ='<option value="'+obj.levelId+'" data-id="'+obj.levelId+'"data-name="'+obj.levelName+'">'+obj.levelName+'</option>';
							}
								$(elementId).append(row);
						})
					spinner.hideSpinner();
			}
		},
		fnGetActivity : function(elementId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("activity",null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.activityId+'" data-id="'+object.activityId+'"data-name="'+object.name+'">'+object.name+'</option>';
					$(elementId).append(row);
				})
				
				
			}
			spinner.hideSpinner();
		},
		fnGetOccupations : function(elementId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("occupation",null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.occupationId+'" data-id="'+object.occupationId+'"data-name="'+object.name+'">'+object.name+'</option>';
					$(elementId).append(row);
				})
				
				
			}
			spinner.hideSpinner();
		},
		fnGetIncomes : function(elementId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("income",null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.incomeId+'" data-id="'+object.incomeId+'"data-name="'+object.value+'">'+object.value+'</option>';
					$(elementId).append(row);
				})
				
				
			}
			spinner.hideSpinner();
		},
		fnGetReferenceTypes : function(elementId){
			$(elementId).empty();
			var	responseData = shiksha.invokeAjax("referenceType",null, "GET");
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			if(responseData != null){
				$.each(responseData,function(index,object){
					var row;
					row ='<option value="'+object.referenceTypeId+'" data-id="'+object.referenceTypeId+'"data-name="'+object.name+'">'+object.name+'</option>';
					$(elementId).append(row);
				})
				
				
			}
			spinner.hideSpinner();
		},
		
};
var setOptionsForMultiSelect = function(arg){	
	$(arg).multiselect({
		maxHeight: 200,
		includeSelectAllOption: false,
		enableFiltering: false,
		enableCaseInsensitiveFiltering: false,
		includeFilterClearBtn: true,	
		//filterPlaceholder: 'Search here...',
	});	
	fnCollapseMultiselect();
}