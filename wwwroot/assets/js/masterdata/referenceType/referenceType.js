var pageContextElements = {	
		eleAddButton 	: '#addNewReferenceType',
		table 			: '#referenceTypeTable',
		toolbar			: "#toolbarReferenceType"
};
var addModal ={
	form 			: '#addReferenceTypeForm',
	modal   		: '#mdlAddReferenceType',
	saveMoreButton	: 'addReferenceTypeSaveMoreButton',
	saveButton		: 'addReferenceTypeSaveButton',
	eleReferenceType   	: '#addReferenceType',
	message			: "#divSaveAndAddNewMessage"
};
var editModal ={
		form	 : '#editReferenceTypeForm',
		modal	 : '#mdlEditReferenceType',
		eleReferenceType: '#editReferenceType',
		eleReferenceTypeId: '#referenceTypeId'
};
var deleteModal ={
		modal	: '#mdlDeleteReferenceType',
		confirm : '#deleteReferenceTypeButton',
		message	: "#deleteReferenceTypeMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var referenceTypeIdForDelete;
var referenceTypeFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var referenceType={
		name:'',
		referenceTypeId:0,

		init:function(){

			$(pageContextElements.eleAddButton).on('click',referenceType.initAddModal);
			$(deleteModal.confirm).on('click',referenceType.deleteFunction);

		},
		deleteFunction : function(){
			referenceType.fnDelete(referenceTypeIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			referenceType.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleReferenceType).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("referenceType", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title :referenceTypeMessages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					referenceType.resetForm(addModal.form);
					referenceType.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);

				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(referenceTypeId){
			referenceType.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("referenceType/"+referenceTypeId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleReferenceType).val(editAjaxCall.name);
					$(editModal.eleReferenceTypeId).val(editAjaxCall.referenceTypeId);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"		:$(editModal.eleReferenceType).val(),
					"referenceTypeId"	:$(editModal.eleReferenceTypeId).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("referenceType", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					referenceType.resetForm(editModal.form);
					referenceType.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: referenceTypeMessages.sucessAlert,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(referenceTypeId,name){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME',name));
			$(deleteModal.modal).modal("show");
			referenceTypeIdForDelete=referenceTypeId;
		},
		fnDelete :function(referenceTypeId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("referenceType/"+referenceTypeId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					referenceType.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : referenceTypeMessages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					referenceType: {
						required : true,
						noSpace	 :true,
						
					}			            
				},

				messages: {
					referenceType: {
						required : referenceTypeMessages.valueRequired,
						noSpace  : referenceTypeMessages.noSpace,
						
					}
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						referenceType.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						referenceType.fnUpdate();
					}
				}
			});
		},
		referenceTypeActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="referenceType.initEdit(\''+row.referenceTypeId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="referenceType.initDelete(\''+row.referenceTypeId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		referenceTypeNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	referenceType.formValidate(addModal.form);
	referenceType.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	referenceType.init();
	
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar			: pageContextElements.toolbar,
		url 			: "referenceType",
		method 			: "get",
		toolbarAlign 	: "right",
		search			: false,
		sidePagination	: "client",
		showToggle		: false,
		showColumns		: false,
		pagination		: true,
		searchAlign		: 'left',
		pageSize		: 20,
		formatShowingRows : referenceTypeFilterObj.formatShowingRows,
		clickToSelect	: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});