var addModal ={};
	addModal.grade ='#mdlAddUnit #grade';
	addModal.subject='#mdlAddUnit #subject';
	addModal.divSubject="#mdlAddUnit #divSubject"
	
var editModal ={};
	editModal.grade ='#mdlEditUnit #grade';
	editModal.subject='#mdlEditUnit #subject';
	editModal.divSubject="#mdlEditUnit #divSubject"
var self;
var ajaxResponseData='';

var gEvent;

var thLabels=[];

var chapter ={
		
		gradeId:0,
		gradeList :null,
		subjectList:null,
		
		subjectId:0,
		chapterId :0,
		
		ajaxURL:'',
		ajaxMethodType:'',
		ajaxContentType:'',
		ajaxRequestData :'',
		ajaxResponseData:'',
		
		init:function(){
			self =chapter;
			
			self.gradeId =0;
			self.gradeList =[];
			self.subjectList =[];
			self.chapterId =0;
			self.subjectId=0;
			
			self.ajaxURL='';
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';
			
			
			 self.fnGetGrades();
			
		},
		/**
		 * ajax call
		 * */
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
				},
				error : function() {					
					return false;
				}
			});
			return ajaxResponseData;
		},
		/**
		 * 
		 * Multiselect Initialization 
		 * 
		 * */
		fnMultiselect	:function(ele){
			$(ele).multiselect('destroy');
			$(ele).multiselect({
				maxHeight: 180,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,
				filterPlaceholder: 'Search here...',
				onChange : function() {
					if (ele == (addModal.grade)) {
						if ($(addModal.grade).find('option:selected').length != 0) {							
							self.fnBindSubject(addModal.subject,0,$(addModal.grade).find('option:selected').val(),[addModal.divSubject],[]);
							self.fnMultiselect(addModal.subject);
						} 
					}else if(ele == (editModal.grade)){
						self.fnBindSubject(editModal.subject,0,$(editModal.grade).find('option:selected').val(),[editModal.divSubject],[]);
						self.fnMultiselect(editModal.subject);
					}
				}
			});
			fnCollapseMultiselect();
		},
		
		/**
		 * Get grde list 
		 * 
		 * */
		fnGetGrades :function(){
			self.ajaxURL ="getGradeList";
			self.ajaxMethodType ='GET';
			self.ajaxContentType ='application/json';
			
			self.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			
			self.gradeList =ajaxResponseData;
		},
		
		/**
		 * Bind grade 
		 * @param
		 * @pSelector	: String 	: selectBox element
		 * @selectedValue : Integer 
		 * */
		fnBindGrade :function(pSelector,selectedValue){			
			shk.fnEmptyAllSelectBox(pSelector);
		var	$ele = $(pSelector);
			var data =self.gradeList;
			$.each(data, function(index, obj) {
				if(obj.gradeId ==selectedValue){
					$ele.append('<option value="'+obj.gradeId+'" data-id="'+obj.gradeId+'"data-name="'+obj.gradeName+'" selected>'+escapeHtmlCharacters(obj.gradeName)+'</option>');	
				}else{
					$ele.append('<option value="'+obj.gradeId+'" data-id="'+obj.gradeId+'"data-name="'+obj.gradeName+'">'+escapeHtmlCharacters(obj.gradeName)+'</option>');
				}			
			});
			self.fnMultiselect(pSelector);
			fnCollapseMultiselect();
		},
		
		
		
		
		/**
		 * Get grde list 
		 * 
		 * */
		fnGetSubject :function(aGradeId){
			self.ajaxURL ="subject/"+aGradeId;
			self.ajaxMethodType ='GET';
			self.ajaxContentType ='application/json';
			
			self.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			
			self.subjectList =ajaxResponseData;
		},
		
		
		
		/**
		 * Bind subject 
		 * @param
		 * @pSelector	: String 	: selectBox element
		 * @selectedValue : Integer 
		 * */		
		fnBindSubject :function(pSelector,selectedValue,bGradeId,bShowDivs){			
			shk.fnEmptyAllSelectBox(pSelector);
		var	$ele = $(pSelector);
			self.fnGetSubject(bGradeId);
			var data =self.subjectList;
			$.each(data, function(index, obj) {
				if(obj.subjectId ==selectedValue){
					$ele.append('<option value="'+obj.subjectId+'" data-id="'+obj.subjectId+'"data-name="'+obj.subjectName+'" selected>'+escapeHtmlCharacters(obj.subjectName)+'</option>');	
				}else{
					$ele.append('<option value="'+obj.subjectId+'" data-id="'+obj.subjectId+'"data-name="'+obj.subjectName+'">'+escapeHtmlCharacters(obj.subjectName)+'</option>');
				}			
			});
			self.fnMultiselect(pSelector);
			fnCollapseMultiselect();
			self.fnShowDivs(bShowDivs);
		},
		fnShowDivs :function(bDivs){
			if(bDivs.length>0){
				$.each(bDivs,function(index,ele){
					$(ele).show();
				});	
			}
			
		},
		fnHideDivs :function(aDivs){
			if(aDivs.length>0){
				$.each(aDivs,function(index,ele){
					$(ele).hide();
				});	
			}
			
		},
		
		
};	


var fnHideChapterAlertDiv =function(){
	hideDiv($('#mdlAddUnit #alertdiv'));
	hideDiv($('#mdlEditUnit #alertdiv'));
}



var  colorClass='';
var initInput =function(type){	
	
	$("#mdlAddUnit").modal().show();
	$("#addUnitName").val('');
	$("#addUnitNumber").val('');
	$("#addUnitDescription").val('');
	$("#textAreaMsg").text(5000);
	$("#mdlAddUnit #addUnitForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddUnit #addUnitForm").data('formValidation').resetForm();		
	 
	
	if(type!='addmore'){
		self.fnBindGrade(addModal.grade,0);
		self.fnHideDivs([addModal.divSubject]);	
	}
	
	
	fnHideChapterAlertDiv();
}


//create a row after save
var getUnitRow =function(unit){
	var unitDescriptions = (unit.unitDescription).replace(/\\n/g, ' ');
	var row = 
		"<tr id='"+unit.unitId+"' data-id='"+unit.unitId+"'>"+
		"<td></td>"+
		
		"<td data-unitid='"+unit.unitId+"'	title='"+unit.unitName+"'>"+unit.gradeName+"</td>"+
		"<td data-unitid='"+unit.unitId+"'	title='"+unit.subjectName+"'>"+unit.subjectName+"</td>"+
		
		"<td data-unitnumber='"+unit.unitNumber+"' title='"+unit.unitNumber+"'>"+unit.unitNumber+"</td>"+		
		"<td data-unitid='"+unit.unitId+"'	title='"+unit.unitName+"'>"+unit.unitName+"</td>"+
		
		"<td data-unitdescription='"+unit.unitDescription+"' title='"+unit.unitDescription+"'>"+unitDescriptions+"</td>"+
		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top btnEditUnit' id='btnEditUnit'" +
		"onclick='editUnitData(&quot;"+unit.unitId+"&quot;,&quot;"+escapeHtmlCharacters(unit.unitName)+"&quot;,&quot;"+unit.unitName+"&quot;,&quot;"+unit.unitNumber+"&quot;,&quot;"+unit.gradeId+"&quot;,&quot;"+unit.subjectId+"&quot;,&quot;"+unit.unitDescription+"&quot;);'><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;'' class='tooltip tooltip-top btnDeleteUnit' id=deleteUnit"+unit.unitId+"'	onclick='deleteUnitData(&quot;"+unit.unitId+"&quot;,&quot;"+escapeHtmlCharacters(unit.unitName)+"&quot;,&quot;"+unit.unitName+"&quot;);'><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	return row;
}



var applyPermissions=function(){
	if (!editUnitPermission){
	$(".btnEditUnit").remove();
		}
	if (!deleteUnitPermission){
	$(".btnDeleteUnit").remove();
		}
	}

////////////////////////////////////////////////


var addUnitData = function(event) {

	var unitName = $('#addUnitName').val().trim().replace(/\u00a0/g," ");
	var unitNumber= Math.floor($('#addUnitNumber').val().trim());
	var unitDescription=$('#addUnitDescription').val().trim();
	spinner.showSpinner();
	var json = {
			"subjectId" : self.subjectId,
			"gradeId"	:self.gradeId,
			"unitName" : unitName,
			"unitNumber": unitNumber, 
			"unitDescription": unitDescription
	};


//	update datatable without reloading the whole table
//	save&add new
	var btnAddMoreUnitId =$(event.target).data('formValidation').getSubmitButton().data('id');
	self.ajaxMethodType ='POST';
	self.ajaxContentType ='application/json';
	
	var unit = self.invokeAjax("unit",json,self.ajaxMethodType,self.ajaxContentType);
	
	if(unit!=null){
		if(unit.response=="shiksha-200"){
			var newRow =getUnitRow(unit);
			appendNewRow("unitTable",newRow);
			applyPermissions();

			fnUpdateDTColFilter('#unitTable',[],7,[0,5,6],thLabels);
			setShowAllTagColor(colorClass);
			if(btnAddMoreUnitId=="addUnitSaveMoreButton"){
				$("#mdlAddUnit").find('#errorMessage').hide();

				fnDisplaySaveAndAddNewElement('#mdlAddUnit',unitName);		
				initInput('addmore');

			}else{	
				$("#mdlAddUnit").modal("hide");
				setTimeout(function() {   
					AJS.flag({
						type: 'success',
						title: 'Success!',
						body: '<p>'+ unit.responseMessage+'</p>',
						close :'auto'
					})
				}, 1000);


			}	
		}else {
			spinner.showSpinner();
			showDiv($('#mdlAddUnit #alertdiv'));
			$("#mdlAddUnit").find('#successMessage').hide();
			$("#mdlAddUnit").find('#okButton').hide();
			$("#mdlAddUnit").find('#errorMessage').show();
			$("#mdlAddUnit").find('#exception').show();
			$("#mdlAddUnit").find('#buttonGroup').show();
			$("#mdlAddUnit").find('#exception').text(unit.responseMessage);
		}
	}
	spinner.showSpinner();
};


/////////////////////////////////////////////////////


var editUnitId = null;
var deleteUnitName = null;
var editUnitNumber=null;
var deleteUnitId = null;
var prevChapterTagValue='';
var editUnitData = function(unitId, unitName,sgChapterName,unitNumber,pGradeId,pSubjectId,unitDescription) {
	
	prevChapterTagValue =sgChapterName.replace(/\u00a0/g," ");
	
	$('#editUnitName').val(unitName);
	$('#editUnitNumber').val(Math.floor(unitNumber));
	$('#editUnitDescription').val(unitDescription);
	$('#editTextAreaMsg').text(5000 - unitDescription.length);
	editUnitId = unitId;
	$("#mdlEditUnit").modal().show();
	$("#mdlEditUnit #editUnitForm").data('formValidation').resetForm();
	
	self.gradeId =pGradeId;	
	self.subjectId=pSubjectId;
	self.chapterId = unitId;
	
	
	self.fnBindGrade(editModal.grade,parseInt(pGradeId,10));
	self.fnBindSubject(editModal.subject,parseInt(pSubjectId,10),pGradeId,[editModal.divSubject],[]);
	
	fnHideChapterAlertDiv();
};



var editData = function() {

	var unitName = $('#editUnitName').val().trim().replace(/\u00a0/g," ");
	var unitNumber=  Math.floor($('#editUnitNumber').val().trim());
	var unitDescription= $('#editUnitDescription').val().trim();
	$(".outer-loader").show();
	var json = {
			"unitId"		: editUnitId,
			"unitName" 		: unitName,
			"unitNumber" 	: unitNumber,
			"oldSubjectId" 	: self.subjectId,
			"oldGradeId"	: self.gradeId,
			"newGradeId"	: $(editModal.grade).find('option:selected').val(),
			"newSubjectId"	: $(editModal.subject).find('option:selected').val(),
			"unitDescription" : unitDescription
	};
	
	self.ajaxMethodType ='PUT';
	self.ajaxContentType ='application/json';
	
	var unit = self.invokeAjax("unit",json,self.ajaxMethodType,self.ajaxContentType);
	
	if(unit!=null){
		if(unit.response=="shiksha-200"){
			$(".outer-loader").hide();
			deleteRow("unitTable",unit.unitId);
			var newRow =getUnitRow(unit);
			appendNewRow("unitTable",newRow);
			applyPermissions();
			displaySuccessMsg();	
			
			fnUpdateDTColFilter('#unitTable',[],7,[0,5,6],thLabels);
			setShowAllTagColor(colorClass);
			$('#mdlEditUnit').modal("hide");
			setTimeout(function() {   
				AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ unit.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			

		}else {
			$(".outer-loader").hide();
			showDiv($('#mdlEditUnit #alertdiv'));
			$("#mdlEditUnit").find('#successMessage').hide();
			$("#mdlEditUnit").find('#errorMessage').show();
			$("#mdlEditUnit").find('#exception').show();
			$("#mdlEditUnit").find('#buttonGroup').show();
			$("#mdlEditUnit").find('#exception').text(unit.responseMessage);
		}
	}
	$(".outer-loader").hide();
};


//edit chapter in SG


var fnIsAssessmentExists =function(e){
	
	var isChanged =false;
	if($(editModal.subject).find('option:selected').val()!=self.subjectId){
		isChanged =true;
	}
	if($(editModal.grade).find('option:selected').val()!=self.gradeId ){
		isChanged =true;
	}
	if(isChanged){
		var reqData ={
				"unitId":self.chapterId,
				"subjectId" :self.subjectId,
				"gradeId"	:self.gradeId
		};
		var isValid =customAjaxCalling("chapter/edit", reqData, "POST");
		if(isValid!=null){
			if(isValid.response=="shiksha-200"){
				editData();
			}else{
				showDiv($('#mdlEditUnit #alertdiv'));
				$("#mdlEditUnit").find('#successMessage').hide();
				$("#mdlEditUnit").find('#errorMessage').show();
				$("#mdlEditUnit").find('#exception').show();
				$("#mdlEditUnit").find('#buttonGroup').show();
				$("#mdlEditUnit").find('#exception').text(isValid.responseMessage);
			}
		}
	}else{
		editData();
	}
	
}



//check is chapter sequence number unique
var fnIsUniqueChapterNumber =function(e,oType){
	
	if(oType=="add"){
		var unitNo=  Math.floor($('#addUnitNumber').val().trim());
		self.gradeId	=$(addModal.grade).find('option:selected').val();	
		self.subjectId	=$(addModal.subject).find('option:selected').val();
		self.ajaxMethodType ='POST';
		self.ajaxContentType ='application/json';
		var reqData ={
				"unitNumber": unitNo,
				"subjectId" : self.subjectId,
				"gradeId"	:self.gradeId
		};
		spinner.showSpinner();
		var isValid = self.invokeAjax("chapter/indexNumber",reqData,self.ajaxMethodType,self.ajaxContentType);
		
		if(isValid!=null){
			if(isValid.response=="shiksha-200"){
				
				addUnitData(e);
			}else{
				showDiv($('#mdlAddUnit #alertdiv'));
				$("#mdlAddUnit").find('#successMessage').hide();
				$("#mdlAddUnit").find('#okButton').hide();
				$("#mdlAddUnit").find('#errorMessage').show();
				$("#mdlAddUnit").find('#exception').show();
				$("#mdlAddUnit").find('#buttonGroup').show();
				$("#mdlAddUnit").find('#exception').text(isValid.responseMessage);
			}
		}
		spinner.hideSpinner();
	}else if(oType=="edit"){
		var unitNoEdit= Math.floor($('#editUnitNumber').val().trim());
		spinner.showSpinner();
		self.ajaxMethodType ='POST';
		self.ajaxContentType ='application/json';
		var reqDataEdit ={
				"unitNumber": unitNoEdit,
				"unitId":self.chapterId,
				"subjectId" :$(editModal.subject).find('option:selected').val(),
				"gradeId"	:$(editModal.grade).find('option:selected').val(),
		};
		
		var isValidEdit = self.invokeAjax("chapter/indexNumber",reqDataEdit,self.ajaxMethodType,self.ajaxContentType);
		
		if(isValidEdit!=null){
			if(isValidEdit.response=="shiksha-200"){
				spinner.hideSpinner();
				fnIsAssessmentExists(e);
			}else{
				showDiv($('#mdlEditUnit #alertdiv'));
				$("#mdlEditUnit").find('#successMessage').hide();
				$("#mdlEditUnit").find('#errorMessage').show();
				$("#mdlEditUnit").find('#exception').show();
				$("#mdlEditUnit").find('#buttonGroup').show();
				$("#mdlEditUnit").find('#exception').text(isValidEdit.responseMessage);
			}
		}
		spinner.hideSpinner();
	}	
}





/////////////////////////////////////////////////////

var sgRequestDelChapterData=null;





var deleteUnitData = function(unitId, unitName,pSgDelChapterName) {
	deleteUnitId = unitId;
	deleteUnitName = unitName;
	$('#mdlDeleteUnit #deleteUnitMessage').text(
			"Do you want to delete " + unitName + " ?");
	$("#mdlDeleteUnit").modal().show();
	hideDiv($('#mdlDeleteUnit #alertdiv'));
};
$(document).on('click', '#deleteUnitButton', function() {

	$(".outer-loader").show();
	var unit = self.invokeAjax("unit/"+deleteUnitId,null,"DELETE","application/json");
   
    if(unit!=null){
	if(unit.response=="shiksha-200"){
		
	
		
		$(".outer-loader").hide();						
		// Delete Row from data table and refresh the table without refreshing the whole table
		deleteRow("unitTable",deleteUnitId);
		fnUpdateDTColFilter('#unitTable',[],7,[0,5,6],thLabels);
		setShowAllTagColor(colorClass);
		
		$('#mdlDeleteUnit').modal("hide");
		
		setTimeout(function() {   
			AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+ unit.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
		
		
	} else {
		showDiv($('#mdlDeleteUnit #alertdiv'));
		$("#mdlDeleteUnit").find('#errorMessage').show();
		$("#mdlDeleteUnit").find('#exception').text(unit.responseMessage);
		$("#mdlDeleteUnit").find('#buttonGroup').show();
	}
    }
	$(".outer-loader").hide();
});



///////////





var unitDescriptionArea =function(msgId){
	var maxchars = 5000;
	$('textarea').on("keyup paste keydown input change drop",function (e) {
		if(e.type == "drop")
		    e.preventDefault();
	    var tlength = $(this).val().length;
	    $(this).val($(this).val().substring(0, maxchars));
	    
	    var remain = maxchars - parseInt(tlength);
	    $(msgId).text(remain);
	});
}


$(function() {
	thLabels =['#','Grade ','Subject ','Chapter No ','Chapter Name  ','Learning Objective','Action'];
	fnSetDTColFilterPagination('#unitTable',7,[0,5,6],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#unitTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();
	
	unitDescriptionArea($('#mdlAddUnit #textAreaMsg'));
	unitDescriptionArea($('#mdlEditUnit #editTextAreaMsg'));
	
	$( "#mdlAddUnit #addUnitDescription" ).keypress(function() {
		showDiv($('#mdlAddUnit #AddTextAreaMsg'));
	})
	
	$( "#mdlEditUnit #editUnitDescription" ).keydown(function() {
		showDiv($('#mdlEditUnit #editTextAreaMesg'));
	})

	fnHideChapterAlertDiv();
	chapter.init();
	fnColSpanIfDTEmpty('unitTable',7);
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	/////////////////////// ADD Form Validation ////////////////////
	$('#addUnitForm').formValidation({ 
		excluded:':disabled',
		framework : 'bootstrap',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			addgrade: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addModal.grade).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			addsubject: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addModal.subject).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			addUnitName : {
				validators : {
					stringLength: {
						max: 250,
						message: 'Chapter name must be less than 250 characters'
					},
					callback : {
						message : 'Chapter '+invalidInputMessage,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);
						}
					}
				}
			},
			addUnitDescription : {
				validators : {
					callback : {
						message : 'Learning Objective contains atleast one alpha character and does not contains these (", ?, \', /, \\) characters and two consecutive spaces',
						callback : function(value) {
							if(value!=""){
								var regx=/^[^'?\"/\\]*$/;
								/*if(/\ {2,}/g.test(value))
									return false;*/
								return regx.test(value);
								}else{
									return true;
								}
							}
					}
				}
			}
		}
	}).on('err.form.fv', function(e) {
		e.preventDefault();
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		fnIsUniqueChapterNumber(e,"add");

	});
	$('#addUnitSaveMoreButton,#addUnitSaveButton').on('click', function() {
		// Revalidate the fields
		$('#addUnitForm').formValidation('revalidateField', 'addsubject');
		$('#addUnitForm').formValidation('revalidateField', 'addgrade');
		$('#addUnitForm').formValidation('revalidateField', 'addUnitName');
		$('#addUnitForm').formValidation('revalidateField', 'addUnitDescription');
		$('#addUnitForm').formValidation('revalidateField', 'addUnitNumber');
	});

///////////////////////// EDIT Form Validation //////////////////////
	$("#editUnitForm").formValidation({ 
		excluded:':disabled',
		framework : 'bootstrap',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			editgrade: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(editModal.grade).val();
							 $(editModal.subject).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			editsubject: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(editModal.subject).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			unitName : {
				validators : {
					stringLength: {
						max: 250,
						message: 'Chapter name must be less than 250 characters'
					},
					callback : {
						message : 'Chapter '+invalidInputMessage,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);
						}
					}
				}
			},
				editUnitDescription : {
					validators : {
						callback : {
							message : 'Learning Objective contains atleast one alpha character and does not contains these (", ?, \', /, \\) characters and two consecutive spaces',
							callback : function(value) {
								if(value!=""){
									var regx=/^[^'?\"/\\]*$/;
									/*if(/\ {2,}/g.test(value))
										return false;*/
									return regx.test(value);
									}else{
										return true;
									}
								}
						}
					}
				}
			
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		fnIsUniqueChapterNumber(e,"edit");
	});
	$('#editUnitSaveButton').on('click', function() {

		// Revalidate the fields
		$('#editUnitForm').formValidation('revalidateField', 'editsubject');
	});
});
