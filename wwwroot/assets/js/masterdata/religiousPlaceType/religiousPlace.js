var pageContextElements = {	
		addButton 	: '#addNewReligiousPlace',
		table 		: '#religiousPlaceTable',
		toolbar		: "#toolbarReligiousPlace",
};
var addModal ={
		form 			: '#addReligiousPlaceForm',
		modal   		: '#mdlAddReligiousPlace',
		saveMoreButton	: 'addReligiousPlaceSaveMoreButton',
		saveButton		: 'addReligiousPlaceSaveButton',
		eleName		   	: '#addReligiousPlaceName',
		
};
var editModal ={
			form	 : '#editReligiousPlaceForm',
			modal	 : '#mdlEditReligiousPlace',
			eleName	 : '#editReligiousPlaceName',
			eleId	 : '#religiousPlaceTypeId',
			
};
var deleteModal ={
			modal	: '#mdlDeleteReligiousPlace',
			confirm : '#deleteReligiousPlaceButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var religiousPlaceId;
var name;
var religiousPlaceFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var religiousPlace={
		name:'',
		religiousPlaceTypeId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',religiousPlace.initAddModal);
			$(deleteModal.confirm).on('click',religiousPlace.deleteFunction);

		},
		deleteFunction : function(){
			religiousPlace.fnDelete(religiousPlaceId,name);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			religiousPlace.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleName).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("religiousplace/type", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){ 
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : messages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){ 
						$(addModal.modal).modal("show");
						$(addModal.modal).find("#divSaveAndAddNewMessage").show();
						fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					}
					religiousPlace.resetForm(addModal.form);
					religiousPlace.refreshTable(pageContextElements.table);

				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(religiousPlaceTypeId){
			religiousPlace.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("religiousplace/type/"+religiousPlaceTypeId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.name);
					$(editModal.eleId).val(editAjaxCall.religiousPlaceTypeId);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name":$(editModal.eleName).val(),
					"religiousPlaceTypeId":$(editModal.eleId).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("religiousplace/type", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					religiousPlace.resetForm(editModal.form);
					religiousPlace.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: messages.sucessAlert,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(religiousPlaceTypeId,rname){
			var msg = $("#deleteReligiousPlaceMessage").data("message");
			$("#deleteReligiousPlaceMessage").html(msg.replace('@NAME',rname));
			$(deleteModal.modal).modal("show");
			religiousPlaceId=religiousPlaceTypeId;
			name = rname;
		},
		fnDelete :function(religiousPlaceTypeId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("religiousplace/type/"+religiousPlaceTypeId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					religiousPlace.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : messages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					religiousPlaceName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
						accept:/^[^'?\"/\\]*$/,
					}			            
				},

				messages: {
					religiousPlaceName: {
						required: messages.nameRequired,
						noSpace:messages.noSpace,
						minlength: messages.minLength,
						maxlength: messages.maxLength,
						accept:messages.accept
					}
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						religiousPlace.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						religiousPlace.fnUpdate();
					}
				}
			});
		},
		religiousPlaceActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="religiousPlace.initEdit(\''+ row.religiousPlaceTypeId+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="religiousPlace.initDelete(\''+ row.religiousPlaceTypeId+ '\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		religiousPlaceNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {
	religiousPlace.formValidate(addModal.form);
	religiousPlace.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	religiousPlace.init();
	var excludedIds=[];
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar,
		url : "religiousplace/type?exclude="+excludedIds,
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: true,
		formatShowingRows : religiousPlaceFilterObj.formatShowingRows,
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});