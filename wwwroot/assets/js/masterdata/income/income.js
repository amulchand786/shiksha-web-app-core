var pageContextElements = {	
		eleAddButton 	: '#addNewIncome',
		table 			: '#incomeTable',
		toolbar			: "#toolbarIncome"
};
var addModal ={
	form 			: '#addIncomeForm',
	modal   		: '#mdlAddIncome',
	saveMoreButton	: 'addIncomeSaveMoreButton',
	saveButton		: 'addIncomeSaveButton',
	eleIncome   	: '#addIncome',
	message			: "#divSaveAndAddNewMessage"
};
var editModal ={
		form	 : '#editIncomeForm',
		modal	 : '#mdlEditIncome',
		eleIncome: '#editIncome',
		eleIncomeId: '#incomeId'
};
var deleteModal ={
		modal	: '#mdlDeleteIncome',
		confirm : '#deleteIncomeButton',
		message	: "#deleteIncomeMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var incomeIdForDelete;
var incomeFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var income={
		name:'',
		incomeId:0,

		init:function(){

			$(pageContextElements.eleAddButton).on('click',income.initAddModal);
			$(deleteModal.confirm).on('click',income.deleteFunction);

		},
		deleteFunction : function(){
			income.fnDelete(incomeIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			income.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"value":$(addModal.eleIncome).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("income", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title :incomeMessages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					income.resetForm(addModal.form);
					income.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.value);

				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(incomeId){
			income.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("income/"+incomeId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleIncome).val(editAjaxCall.value);
					$(editModal.eleIncomeId).val(editAjaxCall.incomeId);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"value"		:$(editModal.eleIncome).val(),
					"incomeId"	:$(editModal.eleIncomeId).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("income", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					income.resetForm(editModal.form);
					income.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: incomeMessages.sucessAlert,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(incomeId,value){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',value));
			$(deleteModal.modal).modal("show");
			incomeIdForDelete=incomeId;
		},
		fnDelete :function(incomeId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("income/"+incomeId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					income.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : incomeMessages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					income: {
						required : true,
						noSpace	 :true,
						
					}			            
				},

				messages: {
					income: {
						required : incomeMessages.valueRequired,
						noSpace  : incomeMessages.noSpace,
						
					}
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						income.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						income.fnUpdate();
					}
				}
			});
		},
		incomeActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="income.initEdit(\''+row.incomeId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="income.initDelete(\''+row.incomeId+'\',\''+row.value+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		incomeNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	income.formValidate(addModal.form);
	income.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	income.init();
	
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar			: pageContextElements.toolbar,
		url 			: "income",
		method 			: "get",
		toolbarAlign 	: "right",
		search			: false,
		sidePagination	: "client",
		showToggle		: false,
		showColumns		: false,
		pagination		: true,
		searchAlign		: 'left',
		pageSize		: 20,
		formatShowingRows : incomeFilterObj.formatShowingRows,
		clickToSelect	: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});