var pageContextElements = {
		divFilter   			: '#divFilter',
		eleLocType  			: '#divCenterLocType input:radio[name=locType]',
		filter					: '#locColspFilter',
		panelLink				: '#locationPnlLink',
		form					: '#uploadLearnerForm',
		eleCollapseFilter		: '#collapseFilter',
		eleToggleCollapse		: '[data-toggle="collapse"]',
		clearBtn				: '#btnResetLocation',
		levelDiv				: '#divSelectLevel',
		batchDiv				: '#divSelectBatch',
		yearDiv                 :'#divSelectYear',
		academicYearDiv 		: '#divSelectAcademicYear',
		btnsRow					: '#btnsRow',
		uploadBtn				:'#uploadBtn',
		uploadFileDiv           :"#uploadFileDiv",
		inputFile               :"#uploadLearnerFile",
		cancelButton			:'#cancelButton',
		downloadButton			:'#downloadButton'
};


var selectBox ={
		center		: '#selectBox_centersByLoc',
		level		: '#selectBox_Level',
		batch		: '#selectBox_Batch',
		academicYear: '#selectBox_AcademicYear',
		year		: '#selectBox_year'
};

var reset = {
		modal : '#mdlResetLoc',
		confirm : '#btnResetLocYes',
	};
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		level 				: "#selectBox_Level",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;

var locationUtility={
		fnInitLocation :function(){
			$(pageContextElements.divFilter).css('display','none');
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(reset.modal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},

		doResetLocation : function() {
			$(selectBox.level).multiselect('destroy').multiselect('rebuild');
			$(selectBox.batch).multiselect('destroy').multiselect('rebuild');
			$(selectBox.academicYear).multiselect('destroy').multiselect('rebuild');
			$(pageContextElements.levelDiv).hide();
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.uploadFileDiv).hide();
			enableMultiSelect($(selectBox.batch));
			enableMultiSelect($(selectBox.level));
			$(pageContextElements.inputFile).val('');
			uploadLearner.resetForm(pageContextElements.form);
			if ($(resetButtonObj).parents("form").attr("id") == 'uploadLearnerForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);

			}
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},

		//displaying robo-filters
		displaySmartFilters : function(lCode,lvlName){

			elementIdMap = {
					"State" 	: '#statesForZone',
					"Zone" 		: '#selectBox_zonesByState',
					"District" 	: '#selectBox_districtsByZone',
					"Tehsil" 	: '#selectBox_tehsilsByDistrict',
					"Block" 	: '#selectBox_blocksByTehsil',
					"City" 		: '#selectBox_cityByBlock',
					"Nyaya Panchayat" 	: '#selectBox_npByBlock',
					"Gram Panchayat" 	: '#selectBox_gpByNp',
					"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
					"Village" 	: '#selectBox_villagebyRv',
					"Center" 	: '#selectBox_centersByLoc'
			};

			displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));

			locationUtility.reinitializeSmartFilter(elementIdMap);

			var lLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
			var lLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');

			displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));

			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(
					function(ind, ele_key) {
						$(elementIdMap[ele_key]).find("option:selected").prop(
								'selected', false);
						$(elementIdMap[ele_key]).multiselect('destroy');
						enableMultiSelect(elementIdMap[ele_key]);
					});
			setTimeout(function() {
				$(".outer-loader").hide();
				$('#rowDiv1,#btnsRow,#divFilter').show();
			}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});

			$('#rowDiv1,#divFilter').show();

			spinner.hideSpinner();
		},

};

var uploadLearner = {

		fnInit:function(){
			doResetLocation=locationUtility.doResetLocation;

			 $(pageContextElements.cancelButton).on("click",uploadLearner.fnCancel);
			$(pageContextElements.downloadButton).on("click",uploadLearner.fnDownloadTemplate);

			$(selectBox.center).on('change',uploadLearner.showLevels);
			$(selectBox.level).on('change',uploadLearner.showBatches);
			$(selectBox.batch).on('change',uploadLearner.showAcademicCycles);

			$(pageContextElements.eleLocType).change(function() {				
				$(pageContextElements.filter).removeClass('in');
				$(pageContextElements.panelLink).text(centerColumn.selectLocation);
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");				
				$(pageContextElements.levelDiv).hide();
				$(pageContextElements.batchDiv).hide();
				$(pageContextElements.yearDiv).hide();
				$(pageContextElements.academicYearDiv).hide();
				$(pageContextElements.uploadFileDiv).hide()				

				var typeCode =$(this).data('code');

				if(typeCode.toLowerCase() =='U'.toLowerCase()){
					showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("U");
					fnCollapseMultiselect();
				}else{
					hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("R");
					fnCollapseMultiselect();

				}
				$(pageContextElements.eleCollapseFilter).css('display','');
			});
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			
		},
		showLevels : function(){
			shikshaPlus.fnGetLevelsByCenter(selectBox.level,0,$(selectBox.center).val());
			setOptionsForMultipleSelect(selectBox.level);
			$(selectBox.level).multiselect("rebuild");
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.uploadFileDiv).hide()		
			$(pageContextElements.levelDiv).show();
		},
		showBatches : function(){
			shikshaPlus.fnGetBatchesByCenterAndLevel(selectBox.batch,0,$(selectBox.center).val(),$(selectBox.level).val());
			setOptionsForMultipleSelect(selectBox.batch);
			$(selectBox.batch).multiselect("rebuild");
			$(pageContextElements.yearDiv).hide();
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.uploadFileDiv).hide()	
			$(pageContextElements.batchDiv).show();
			
		},
		showAcademicCycles : function(){
			shikshaPlus.fnGetCurrentYear(selectBox.year,0);
			setOptionsForMultipleSelect(selectBox.year);
			$(selectBox.year).multiselect("rebuild");
			$(pageContextElements.yearDiv).show();
			
			var year=$(selectBox.year).val();
			var centerId = $(selectBox.center).val();
			var levelId  = $(selectBox.level).val();
			var batchId  = $(selectBox.batch).val();
			
			var data =  {
					center : {
						id : centerId
					},
					level : {
						levelId : levelId
					},
					batch : {
						batchId : batchId
					},
					academicCycle : {
						year : year
					}
			};

			fnGetAcademicCyclesByYearAndBatch(selectBox.academicYear,0,data);
			if($(selectBox.academicYear).find('option').length>1){
				setOptionsForMultipleSelect(selectBox.academicYear);
				$(selectBox.academicYear).multiselect("rebuild");
				$(pageContextElements.academicYearDiv).show();
				$(pageContextElements.uploadFileDiv).show();
			} else {
				$(pageContextElements.uploadFileDiv).hide();
			}
		},
		 submitUploadFile : function(file){

			var formData = new FormData();
			formData.append( 'uploadfile',file );
			formData.append( 'center',$(selectBox.center).val() );
			formData.append( 'level',$(selectBox.level).val() );
			formData.append( 'batch',$(selectBox.batch).val() );
			formData.append( 'academicCycle',$(selectBox.academicYear).val() );
			spinner.showSpinner();
			$.ajax({
				url: "learner/upload",
				type: "POST",
				data: formData,
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				success: function (response) {
					spinner.hideSpinner();
					if(response.response == "shiksha-200"){
						$(pageContextElements.inputFile).val('');
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : response.responseMessage,
							close : 'auto'
						});		

					} else {
						if(response.responseMessages!=null?response.responseMessages.length>0:false){
							var item="";
							$.each(response.responseMessages,function(index,value){
								var num=index+1;
								  item=item+num+'.'+value+'<br>';
								 						
							});
							AJS.flag({
								type  : "error",
								title : appMessgaes.error,
								body  : response.responseMessage+"<br> "+item,
								close : 'auto'
							});
							
						}else{
							uploadLearner.fnShowError(response.responseMessage);
						}
						

					}
				},
				error: function (error) {
					spinner.hideSpinner();
					console.log(error);
					uploadLearner.fnShowError(appMessgaes.serverError);

				}
			});
		},
		fnDownloadTemplate:function(){
			var villageName=$('#selectBox_villagebyRv').find('option:selected').text();
			var centerName=$(selectBox.center).find('option:selected').text();
			var levelName=$(selectBox.level).find('option:selected').text();
			var batchName=$(selectBox.batch).find('option:selected').text();
			
			var excelFileName=villageName+"_"+centerName+"_"+levelName+"_"+batchName+".xlsx";
			window.location=baseContextPath+'/learner/download?excelFileName='+excelFileName;
		},
		fnCancel:function(){
			$(pageContextElements.inputFile).val('');
			uploadLearner.resetForm(pageContextElements.form);
		},
		fnShowError:function(message){
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : message,
				close : 'auto'
			});
		},
		formValidate : function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					uploadfile: {
						required: true,
						fileFormat:true
					},
				},
				messages: {
					uploadfile: {
						required:uploadMessages.fileRequired,
						fileFormat:uploadMessages.fileFormat.replace("@EXTENSION", ".xlsx"),
					},

				},
				errorPlacement: function(error, element) {
					$(error).insertAfter(element);
				},
				submitHandler : function(){
					uploadLearner.submitUploadFile($(pageContextElements.inputFile)[0].files[0]);
				}
			});
		},


};

var fnGetAcademicCyclesByYearAndBatch =function(elementId,selectedId,data){
	$(elementId).empty();
	var	responseData = shiksha.invokeAjax("academicCycle/", data, "POST");
	if(responseData != null?responseData.length>0:false){
			var row;
			$(elementId).append('<option value="NONE" selected disabled hidden>None Selected</option>');
			$.each(responseData, function(index,obj){
				if(obj.flagCurrentAC == true){
					row ='<option value="'+obj.academicCycleId+'" data-cycletype="'+obj.cycleType+'" data-current="'+obj.flagCurrentAC+'"  data-id="'+obj.academicCycleId+'"data-name="'+obj.name+'" selected>'+obj.name+'</option>';
				}
				 
					$(elementId).append(row);
			})
	}else{
		AJS.flag({
			type : "error",
			title : appMessgaes.oops,
			body : appMessgaes.academicCyclesNotExist,
			close :'auto'
		})
	}
	spinner.hideSpinner();
}

$( document ).ready(function() {
	uploadLearner.formValidate(pageContextElements.form);
	uploadLearner.fnInit();

	$('input[name=uploadfile]').change(function() {
		uploadLearner.resetForm(pageContextElements.form);
	   
	});



	jQuery.validator.addMethod("noSpace", function(value) {
		return !value.trim().length <= 0;
	}, "");
	
	jQuery.validator.addMethod("fileFormat", function() {

    	var filename=	$(pageContextElements.inputFile).val();
    	var fileExtensionName = filename.split('.').pop();
		if (fileExtensionName!="xlsx") {
			return false;
		}
		return true;
	 
	}, "");

});