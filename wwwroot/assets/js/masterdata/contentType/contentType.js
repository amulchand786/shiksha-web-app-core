var pageContextElements = {	
		addButton 		: '#addNewContentType',
		table 			: '#contentTypeTable',
		toolbar			: "#toolbarContentType",
};
var addModal = {
		form	: '#addContentTypeForm'	,
		modal	: '#mdlAddContentType',
		saveMoreBtn	: 'addContentTypeSaveMoreButton',
		saveBtn		: 'addContentTypeSaveButton',
		eleName		: '#addContentTypeName',
		message		: "#divSaveAndAddNewMessage"
		
};
var editModal = {
			form	: '#editContentTypeForm',
			modal	: '#mdlEditContentType',
			eleName	: '#editSegmentTypeName',
			eleId	: '#editSegmentTypeId',
			
};
var deleteModal = {
			modal	: '#mdlDeleteContentType',
			confirm	: '#deleteContentTypeButton',
			message	: "#deleteContentTypeMessage"
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var contentId;
var contentTypeFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var content={
		name:'',
		contentTypeId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',content.initAddModal);
			$(deleteModal.confirm).on('click',content.deleteFunction);

		},
		deleteFunction : function(){
			content.fnDelete(contentId);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			content.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find('#alertdiv').hide();
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$(addModal.eleName).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("content/type", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveBtn){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : "Success!",
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId ==  addModal.saveMoreBtn){
						$(addModal.modal).modal("show");
					}
					content.resetForm(addModal.form);
					content.refreshTable(pageContextElements.table);
					$(addModal.modal).find(addModal.message).show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);

				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(contentTypeId){
			content.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("content/type/"+contentTypeId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$('#editContentTypeName').val(editAjaxCall.name);
					$('#editcontentTypeId').val(editAjaxCall.contentTypeId);
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name":$('#editContentTypeName').val(),
					"contentTypeId":$('#editcontentTypeId').val(),
			}
			var updateAjaxCall = shiksha.invokeAjax("content/type", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					content.resetForm(editModal.form);
					content.refreshTable(pageContextElements.table);
					AJS.flag({
						type  : "success",
						title : "Success!",
						body  : updateAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : updateAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'})
			}
		},
		
		initDelete : function(contentTypeId,name){
			var msg = $(deleteModal.message).data("message");
			$(deleteModal.message).html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			contentId=contentTypeId;
		},
		fnDelete :function(contentTypeId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("content/type/"+contentTypeId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					content.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : "Success!",
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					contentTypeName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
						accept:/^[^'?\"/\\]*$/,
						
					}			            
				},

				messages: {
					contentTypeName: {
						required: messages.nameRequired,
						noSpace:messages.noSpace,
						minlength: messages.minLength,
						maxlength: messages.maxLength,

					}
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						content.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						content.fnUpdate();
					}
				}
			});
		},
		contentTypeActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="content.initEdit(\''+row.contentTypeId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="content.initDelete(\''+row.contentTypeId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
};

$( document ).ready(function() {

	content.formValidate(addModal.form);
	content.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	content.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar,
		url : "content/type",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		clickToSelect: false,
		formatShowingRows : contentTypeFilterObj.formatShowingRows,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});