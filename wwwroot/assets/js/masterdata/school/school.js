//global variables
var  colorClass='';
var schoolName='';
var schoolType;
var schoolLocType;
var schoolId;
var address;
var phoneNumber;
var nearestLandMark;
var distanceOfSchoolFromNearestLandmark;
var headTeacherName;
var isOpenLab;	
var cityId;
var email;
var villageId;
var schoolTypeId;
var schoolLocationTypeId;
var deleteSchoolId;
var deleteSchoolName;
var deleteFlag;
var totalNumberOfStudents;
var editedSchoolId;
var resetButtonObj;
var lObj;
var elementIdMap;
var thLabels;

var addFormId="addSchoolForm";
var editFormId="editSchoolForm";
var buttonId;
var allInputEleNamesForSchool=[];
var allInputEleNamesOfCityFilter =[];
var allInputEleNamesOfVillageFilter =[];
var allInputEleNamesOfCityFilterSchool=[];
var selectedChkBoxForSchoolDel=[];
var rowIdForShowMessage='';
var colFilterChecked=false;
//CR-670
var activeDeactivateSchoolId=0;
var activeDeactivateSchoolName;;
var activeDeactivateFlag ;
var activeDeactivateschoolLocType;
//CR-670
var schoolDatePicker={};
schoolDatePicker.modifyDate ="#modifiedDatePicker";
schoolDatePicker.createdDate ="#createdDatePicker";

//global selectDivIds
var addMdlSelectDivs = {};	
addMdlSelectDivs.city 	="#mdlAddSchool #divSelectCity";
addMdlSelectDivs.block 	="#mdlAddSchool #divSelectBlock";
addMdlSelectDivs.NP 	="#mdlAddSchool #divSelectNP";
addMdlSelectDivs.GP 	="#mdlAddSchool #divSelectGP";
addMdlSelectDivs.RV 	="#mdlAddSchool #divSelectRV";
addMdlSelectDivs.village ="#mdlAddSchool #divSelectVillage";

addMdlSelectDivs.schoolLocationType ="#mdlAddSchool #selectBox_schoolLocationType";
addMdlSelectDivs.schoolType ="#mdlAddSchool #selectBox_schoolType";

addMdlSelectDivs.resetLocDivCity ="#mdlAddSchool #divResetLocCity";
addMdlSelectDivs.resetLocDivVillage ="#mdlAddSchool #divResetLocVillage";


var editMdlSelectDivs = {};	
editMdlSelectDivs.city 	="#mdlEditSchool #divSelectCity";
editMdlSelectDivs.block ="#mdlEditSchool #divSelectBlock";
editMdlSelectDivs.NP	="#mdlEditSchool #divSelectNP";
editMdlSelectDivs.GP 	="#mdlEditSchool #divSelectGP";
editMdlSelectDivs.RV 	="#mdlEditSchool #divSelectRV";
editMdlSelectDivs.village ="#mdlEditSchool #divSelectVillage";

editMdlSelectDivs.schoolLocationType ="#mdlEditSchool #selectBox_schoolLocationType";
editMdlSelectDivs.schoolType ="#mdlEditSchool #selectBox_schoolType";

editMdlSelectDivs.resetLocDivCity ="#mdlEditSchool #divResetLocCity";
editMdlSelectDivs.resetLocDivVillage ="#mdlEditSchool #divResetLocVillage";


//initialise global variables
var initGlblVariables =function(){
	schoolName='';
	schoolType='';
	schoolLocType='';
	schoolId='';
	address='';
	phoneNumber='';
	nearestLandMark='';
	distanceOfSchoolFromNearestLandmark='';
	
	isOpenLab='';	
	cityId='';
	villageId='';
	schoolTypeId='';
	schoolLocationTypeId='';
	deleteSchoolId='';
	deleteSchoolName='';
	deleteFlag ='';
	headTeacherName='';
	resetButtonObj='';
	totalNumberOfStudents='';
	buttonId='';
	allInputEleNamesForSchool=['schoolName','schoolId','nearestLandmarkToSchool','landmark','distanceFromLandmark','address','phoneNumber','numberOfStudent','headTeacherlName','schoolType','addEmail'];
	allInputEleNamesOfCityFilter =['stateId','zoneId','districtId','tehsilId','cityId'];
	allInputEleNamesOfVillageFilter =['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
	allInputEleNamesOfCityFilterSchool =['blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
} 



//get all schooltype and location and bind to respective list box at add school
var getSchoolTypeAndLoc =function(){
	resetFormById('addSchoolForm');
	resetFormById('editSchoolForm');
	getAllSchoolType();
	destroyAndRefreshMultiselect($(addMdlSelectDivs.schoolType),$(editMdlSelectDivs.schoolType));
	setOptionsForMultipleSelect($(addMdlSelectDivs.schoolType),$(editMdlSelectDivs.schoolType));
	$('input:radio[name=locType]').each(function(i,ele){$(ele).prop('checked',false)});
	$('#mdlAddSchool #addSchoolSaveMoreButton').prop('disabled','disabled');
	$('#mdlAddSchool #addSchoolSaveButton').prop('disabled','disabled');
	$('#mdlAddSchool #pnlSchoolDetails').css('display','none');
	$('#mdlAddSchool #divFilter').css('display','none');
	$('#mdlAddSchool #openLab').prop('checked',false);
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

}

//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
}


//displaying smart-filters
var displaySmartFilters =function(type,lCode,lvlName){
	$(".outer-loader").show();
	if(type=="add"){
		elementIdMap = {
				"State" : '#mdlAddSchool #statesForZone',
				"Zone" : '#mdlAddSchool #selectBox_zonesByState',
				"District" : '#mdlAddSchool #selectBox_districtsByZone',
				"Tehsil" : '#mdlAddSchool #selectBox_tehsilsByDistrict',
				"Block" : '#mdlAddSchool #selectBox_blocksByTehsil',
				"City" : '#mdlAddSchool #selectBox_cityByBlock',
				"Nyaya Panchayat" : '#mdlAddSchool #selectBox_npByBlock',
				"Gram Panchayat" : '#mdlAddSchool #selectBox_gpByNp',
				"Revenue Village" : '#mdlAddSchool #selectBox_revenueVillagebyGp',
				"Village" : '#mdlAddSchool #selectBox_villagebyRv'				
		};
		displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'),type);
	}else{
		elementIdMap = {
				"State" : '#mdlEditSchool #statesForZone',
				"Zone" : '#mdlEditSchool #selectBox_zonesByState',
				"District" : '#mdlEditSchool #selectBox_districtsByZone',
				"Tehsil" : '#mdlEditSchool #selectBox_tehsilsByDistrict',
				"Block" : '#mdlEditSchool #selectBox_blocksByTehsil',
				"City" : '#mdlEditSchool #selectBox_cityByBlock',
				"Nyaya Panchayat" : '#mdlEditSchool #selectBox_npByBlock',
				"Gram Panchayat" : '#mdlEditSchool #selectBox_gpByNp',
				"Revenue Village" : '#mdlEditSchool #selectBox_revenueVillagebyGp',
				"Village" : '#mdlEditSchool #selectBox_villagebyRv'				
		};

		var aLocTypeCode =$('#mdlEditSchool input:radio[name=locType]:checked').data('code');
		var aLLevelName =$('#mdlEditSchool input:radio[name=locType]:checked').data('levelname');
		displayLocationsOfSurvey(aLocTypeCode,aLLevelName, $('#divFilter'),"edit");
		$("#mdlEditSchool #hrDiv").show();
		if(aLocTypeCode.toLowerCase() =='U'.toLowerCase()){
					
			fnUpdateFVElementStatus(addFormId,allInputEleNamesOfCityFilterSchool,'VALID');
			showDiv($(editMdlSelectDivs.city),$(editMdlSelectDivs.resetLocDivCity));
			hideDiv($(editMdlSelectDivs.block),$(editMdlSelectDivs.NP),$(editMdlSelectDivs.GP),$(editMdlSelectDivs.RV),$(editMdlSelectDivs.village),$(editMdlSelectDivs.resetLocDivVillage));			
		}else{
			
			$("#"+addFormId).data('formValidation').updateStatus('cityId', 'VALID');
			hideDiv($(editMdlSelectDivs.city),$(editMdlSelectDivs.resetLocDivCity));
			showDiv($(editMdlSelectDivs.block),$(editMdlSelectDivs.NP),$(editMdlSelectDivs.GP),$(editMdlSelectDivs.RV),$(editMdlSelectDivs.village),$(editMdlSelectDivs.resetLocDivVillage));			
		}
		//
	}
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){
	$(".outer-loader").hide(); 
	$('#rowDiv1,#divFilter').show(); }, 100);


	resetFormValidatonForLoc(editFormId,"villageId");
	resetFormValidatonForLoc(editFormId,"cityId");
	var eLocTypeCode =$('#mdlEditSchool input:radio[name=locType]:checked').data('code');
	if(eLocTypeCode!="" && eLocTypeCode!=null){
		if(eLocTypeCode.toLowerCase() =='U'.toLowerCase()){
			$("#"+editFormId).data('formValidation').updateStatus('villageId', 'VALID');								
		}else{
			$("#"+editFormId).data('formValidation').updateStatus('cityId', 'VALID');		
		}
	}
};


//show or hide locations as per school location type
//after UI chanaged......
$('#mdlAddSchool input:radio[name=locType]').change(function() {	
	$('#mdlAddSchool #pnlSchoolDetails').css('display','none');
	$('#mdlAddSchool #addLocColspFilter').removeClass('in');
	$('#mdlAddSchool #locationPnlLink').text('Select Location');
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	$("#hrDiv").show();
	lObj =this;
	var typeCode =$(this).data('code');
	reinitializeSmartFilter(elementIdMap);	
	
	resetFormValidatonForLocFilters(addFormId,allInputEleNamesOfCityFilter);
	resetFormValidatonForLocFilters(addFormId,allInputEleNamesOfVillageFilter);
	if(typeCode.toLowerCase() =='U'.toLowerCase()){

		fnUpdateFVElementStatus(addFormId,allInputEleNamesOfCityFilterSchool,'VALID');
		
		showDiv($(addMdlSelectDivs.city),$(addMdlSelectDivs.resetLocDivCity));
		hideDiv($(addMdlSelectDivs.block),$(addMdlSelectDivs.NP),$(addMdlSelectDivs.GP),$(addMdlSelectDivs.RV),$(addMdlSelectDivs.village),$(addMdlSelectDivs.resetLocDivVillage));
		displaySmartFilters("add",$(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
	}else{
		
		$("#"+addFormId).data('formValidation').updateStatus('cityId', 'VALID');
		hideDiv($(addMdlSelectDivs.city),$(addMdlSelectDivs.resetLocDivCity));
		showDiv($(addMdlSelectDivs.block),$(addMdlSelectDivs.NP),$(addMdlSelectDivs.GP),$(addMdlSelectDivs.RV),$(addMdlSelectDivs.village),$(addMdlSelectDivs.resetLocDivVillage));
		displaySmartFilters("add",$(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
	}
	$('#mdlAddSchool #collapseFilter').css('display','');
});



//show or hide div as per school location type...edit
$('#mdlEditSchool input:radio[name=locType]').change(function() {
	$('#editPanelHead').find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('#editPanelHead').find("span").addClass("glyphicon-plus-sign").addClass("green");

	$('#mdlEditSchool #editPnlScDetails').collapse('hide');
	$('#mdlEditSchool #editPnlScDetails').removeClass('in');

	reinitializeSmartFilter(elementIdMap);
	displaySmartFilters("edit");
});







//reset only location when click on reset button icon
var resetLocation =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;

}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	$("#mdlResetLoc").modal("hide");
	setTimeout(function(){$("body").addClass("modal-open");},150);
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

	$('#mdlAddSchool #addLocColspFilter').removeClass('in');
	$('#mdlAddSchool #locationPnlLink').text('Select Location');
	if($(resetButtonObj).parents("form").attr("id")=="addSchoolForm"){
		
		resetFormValidatonForLocFilters("addSchoolForm",allInputEleNamesOfCityFilter);
		resetFormValidatonForLocFilters("addSchoolForm",allInputEleNamesOfVillageFilter);
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("add",$(lObj).data('code'),$(lObj).data('levelname'));
		var cLocTypeCode =$('input:radio[name=locType]:checked').data('code');
		if(cLocTypeCode!="" && cLocTypeCode!=null){
			if(cLocTypeCode.toLowerCase() =='U'.toLowerCase()){
				
				fnUpdateFVElementStatus(addFormId,allInputEleNamesOfCityFilterSchool,'VALID');
			}else{
						
				$("#"+addFormId).data('formValidation').updateStatus('cityId', 'VALID');
			}

		}
		
		$('#mdlAddSchool #pnlSchoolDetails').css('display','none');
	}else{
		resetFormValidatonForLoc(editFormId,"villageId");
		resetFormValidatonForLoc(editFormId,"cityId");
		reinitializeSmartFilter(elementIdMap);
		displaySmartFilters("edit");
		var dLocTypeCode =$('#mdlEditSchool input:radio[name=locType]:checked').data('code');
		if(dLocTypeCode!="" && dLocTypeCode!=null){
			if(dLocTypeCode.toLowerCase() =='U'.toLowerCase()){
				fnUpdateFVElementStatus(addFormId,allInputEleNamesOfCityFilterSchool,'VALID');							
			}else{
				$("#"+addFormId).data('formValidation').updateStatus('cityId', 'VALID');
			}

		}
	}
	fnCollapseMultiselect();

}



//reset form
var resetLocatonAndForm =function(type){
	$('#divFilter').hide();
	if(type=="add"){
		resetFormById('addSchoolForm');	
	}else{
		resetFormById('editSchoolForm');	
	}	
	reinitializeSmartFilter(elementIdMap);
	getSchoolTypeAndLoc();
}



//refresh data table once add success
var getNewRow = function(school){
	var isOpenLab=(school.isOpenLab==0?'No':"Yes");
	var schoolAddress = (school.address).replace(/\\n/g, ' ');
	var stateName=(school.stateName!=null?school.stateName:'');
	var zoneName= (school.zoneName!=null?school.zoneName:'');
	var districtName=( school.districtName!=null? school.districtName:'');
	var tehsilName= (school.tehsilName!=null?school.tehsilName:'');
	var cityName= (school.cityName!=null?school.cityName:'');
	var blockName= (school.blockName!=null?school.blockName:'');
	var villageName= (school.villageName!=null?school.villageName:'');
	var nearestLandMark= (school.nearestLandMark!=null?school.nearestLandMark:'');
	var distanceOfSchoolFromNearestLandmark= (school.distanceOfSchoolFromNearestLandmark!=null?school.distanceOfSchoolFromNearestLandmark:'');
	var schoolTypeName= (school.schoolTypeName!=null?school.schoolTypeName:'');
	var schoolLocationTypeName= (school.schoolLocationTypeName!=null?school.schoolLocationTypeName:'');
	var schoolName= (school.schoolName!=null?school.schoolName:'');
	var phoneNumber=( school.phoneNumber!=null? school.phoneNumber:'');
	var headTeacherName= (school.headTeacherName!=null?school.headTeacherName:'');
	var noOfStudents= (school.noOfStudents!=null || school.noOfStudents!=0 || school.noOfStudents!=undefined?school.noOfStudents:'');
	var schoolCode=(school.schoolId!=null?school.schoolId:'');
	var email=(school.email!=null?school.email:'');
  var isMemeber=school.isMember;
  var activeDeactiveSpan;
  var inactiveflag;
  if(isMemeber){
  	activeDeactiveSpan='<span class="fa fa-toggle-on font-size12 "></span>';
  	inactiveflag="Inactivate";
  }else{
  	activeDeactiveSpan='<span class="fa fa-toggle-off font-size12 "></span>';
  	inactiveflag="Activate";
  }

	var row =
		"<tr id='"+school.id+"' data-id='"+school.id+"'>"+
		/*"<td></td>"+	*/
		"<td  id='"+school.id+"'><center><input class='chkboxDelSchoolForSchool'  type='checkbox' id='chkboxDelSchoolForSchool"+school.id+"' onchange='fnChkUnchkDelSchoolForSchool("+school.id+");'></input></center></td>"+
		"<td data-stateid='"+school.stateId+"'	title='"+stateName+"'>"+escapeHtmlCharacters(stateName)+"</td>"+
		"<td data-zoneid='"+school.zoneId+"'	title='"+zoneName+"'>"+escapeHtmlCharacters(zoneName)+"</td>"+
		"<td data-districtid='"+school.districtId+"' title='"+districtName+"' >"+escapeHtmlCharacters(districtName)+"</td>"+
		"<td data-tehsilid='"+school.tehsilId+"' title='"+tehsilName+"'>"+escapeHtmlCharacters(tehsilName)+"</td>"+
		"<td data-cityid='"+school.cityId+"' 	 title='"+cityName+"'>"+escapeHtmlCharacters(cityName)+"</td>"+
		"<td data-blockid='"+school.blockId+"'	 title='"+blockName+"'>"+escapeHtmlCharacters(blockName)+"</td>"+
		"<td data-villageid='"+school.villageId+"'title='"+villageName+"'>"+escapeHtmlCharacters(villageName)+"</td>"+
		"<td data-nearestlandmark='"+school.nearestLandMark+"' title='"+nearestLandMark+"'>"+escapeHtmlCharacters(nearestLandMark)+"</td>"+
		"<td data-distance_from_nearestlandmark='"+school.distanceOfSchoolFromNearestLandmark+"' title='"+escapeHtmlCharacters(distanceOfSchoolFromNearestLandmark)+"km'>"+escapeHtmlCharacters(distanceOfSchoolFromNearestLandmark)+" km</td>"+
		"<td data-schooltypeid='"+school.schoolTypeId+"' title='"+school.schoolTypeName+"'>"+school.schoolTypeName+"</td>"+	
		"<td data-school_loc_typeid='"+school.schoolLocationTypeId+"' title='"+schoolLocationTypeName+"'>"+escapeHtmlCharacters(schoolLocationTypeName)+"</td>"+

		"<td data-schoolname='"+school.schoolName+"' title='"+schoolName+"'>"+escapeHtmlCharacters(schoolName)+"</td>"+
		"<td data-schoolid='"+school.schoolId+"' title='"+schoolCode+"'>"+escapeHtmlCharacters(schoolCode)+"</td>"+

		"<td data-address='"+school.address+"' title='"+school.address+"'>"+escapeHtmlCharacters(schoolAddress)+"</td>"+
		"<td data-phonenumber='"+school.phoneNumber+"' title='"+school.phoneNumber+"'>"+escapeHtmlCharacters(school.phoneNumber)+"</td>"+
		"<td data-headteacherid='"+school.headTeacherName+"' title='"+ headTeacherName+"'>"+(escapeHtmlCharacters(school.headTeacherName))+"</td>"+
		"<td data-noofstudents='"+school.noOfStudents+"' title='"+noOfStudents+"'>"+noOfStudents+"</td>"+
		"<td data-isopenLab='"+school.isOpenLab+"'>"+isOpenLab+"</td>"+
		"<td data-email='"+email+"'>"+email+"</td>"+
		"<td data-modifiedBy='"+school.modifiedBy+"'>"+school.modifiedBy+"</td>"+
		"<td data-modifiedBy='"+school.uploadedBy+"'>"+school.uploadedBy+"</td>"+
		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top'data-toggle ='modal' data-target='#mdlEditSchool' id='btnEditSchool'" +
		"onclick=editSchoolSetData(&quot;"+school.id+"&quot;,&quot;"+school.schoolTypeId+"&quot;,&quot;"+school.schoolLocationTypeId+"&quot;,&quot;"+school.stateId+"&quot;,&quot;"+school.zoneId+"&quot;,&quot;"+school.districtId+"&quot;,&quot;"+school.tehsilId+"&quot;,&quot;"+school.blockId+"&quot;,&quot;"+school.cityId+"&quot;,&quot;"+school.nyayPanchayatId+"&quot;,&quot;"+school.gramPanchayatId+"&quot;,&quot;"+school.revenueVillageId+"&quot;,&quot;"+school.villageId+"&quot;,&quot;"+escapeHtmlCharacters(school.nearestLandMark)+"&quot;,&quot;"+escapeHtmlCharacters(school.distanceOfSchoolFromNearestLandmark)+"&quot;,&quot;"+escapeHtmlCharacters(schoolCode)+"&quot;,&quot;"+escapeHtmlCharacters(school.schoolName)+"&quot;,&quot;"+escapeHtmlCharacters(school.address)+"&quot;,&quot;"+school.phoneNumber+"&quot;,&quot;"+escapeHtmlCharacters(school.headTeacherName)+"&quot;,&quot;"+school.isOpenLab+"&quot;,&quot;"+school.noOfStudents+"&quot;,&quot;"+school.email+"&quot;); ><span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12' ></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top'	 id=deleteSchool	onclick=deleteSchoolSetData(&quot;"+school.id+"&quot;,&quot;"+escapeHtmlCharacters(school.schoolName)+"&quot;,&quot;"+school.isDeleteable+"&quot;); ><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12'></span></a>"+
		"<a  style='margin-left:10px;' class='tooltip tooltip-top'	 id=activeSchool"+school.id+"	onclick=inActiveSchoolSetData(&quot;"+school.id+"&quot;,&quot;"+escapeHtmlCharacters(school.schoolName)+"&quot;,&quot;"+school.isMember+"&quot;,&quot;"+school.schoolLocationTypeCode+"&quot;); ><span class='tooltiptext'>"+inactiveflag+"</span>"+activeDeactiveSpan+"</a></div></td>"+
		"</tr>";
return row;

}


var fnAppendNewRow =function(tableName,aRow){
	var oaTable =$("#"+tableName).DataTable();

	oaTable.page();


	oaTable.row.add($(aRow)).draw( false );
	var currentRows = oaTable.data().toArray();  // current table data
	var newRow=currentRows.pop();
	var info = $('#'+tableName).DataTable().page.info();
	var startRow=info.start;
	currentRows.splice(startRow, 0,newRow );
	oaTable.clear();
	oaTable.rows.add(currentRows);

	oaTable.draw(false);


}


var applyPermissions=function(){
	if (!editSchoolPermission){
		$("#btnEditSchool").remove();
	}
	if (!deleteSchoolPermission){
		$("#deleteSchool").remove();
	}
	if(!activeOrInactiveSchoolPermission){
		$("#activeSchool").remove();
	}
}

//addSchool
var addSchool =function(event){
	initGlblVariables();
	schoolName =$('#mdlAddSchool #schoolName').val().trim().replace(/\u00a0/g," ");
	schoolType =$('#mdlAddSchool').find('[name="schoolType"]:checked').data('id')
	schoolLocType =$('#mdlAddSchool input:radio[name=locType]:checked').data('code');
	schoolId =$('#mdlAddSchool  #schoolId').val()!=""?$('#mdlAddSchool  #schoolId').val().trim().replace(/\u00a0/g," "):null;
	address =$('#mdlAddSchool #address').val().trim().replace(/\u00a0/g," ");
	phoneNumber =$('#mdlAddSchool #phoneNumber').val().trim().replace(/\u00a0/g," ");
	nearestLandMark =$('#mdlAddSchool #landmark').val().trim().replace(/\u00a0/g," ");
	distanceOfSchoolFromNearestLandmark =$('#mdlAddSchool #distanceFromLandmark').val().trim().replace(/\u00a0/g," ");
	headTeacherName=$('#mdlAddSchool #headTeacherlName').val().trim().replace(/\u00a0/g," ");
	
	if($('#mdlAddSchool #openLab').not(':checked').length){	      
		isOpenLab=0;
	}else{
		isOpenLab=1;
	}
	if(schoolLocType.toLowerCase() =='R'.toLowerCase()){
		villageId=$('#mdlAddSchool #selectBox_villagebyRv').find('option:selected').val();
	}else{
		cityId=$('#mdlAddSchool #selectBox_cityByBlock').find('option:selected').val();
	}	
	schoolTypeId=$('#mdlAddSchool').find('[name="schoolType"]:checked').data('id')
	email=$('#addEmail').val();
	var json ={
			"schoolId":schoolId,
			"schoolName":schoolName,				
			"address":address,
			"phoneNumber":phoneNumber,
			"nearestLandMark":nearestLandMark,
			"distanceOfSchoolFromNearestLandmark":distanceOfSchoolFromNearestLandmark,			
			"isOpenLab":isOpenLab,	
			"cityId":cityId,
			"villageId":villageId,
			"schoolTypeId":schoolType,						
			"schoolLocationTypeCode":schoolLocType,
			"noOfStudents":totalNumberOfStudents,
			"headTeacherName":headTeacherName,
			"email":email,
	}
	$('.outer-loader').show();
	var school=customAjaxCalling("school",json, "POST");
	//update datatable without reloading the whole table
	//save&add new
	buttonId=$(event.target).data('formValidation').getSubmitButton().data('id');

	if(school.response=="shiksha-200"){
		limitTextArea('#mdlAddSchool #textAreaMsg');
		var newRow =getNewRow(school);
		fnAppendNewRow("schoolTable",newRow);
		applyPermissions();	
		$("#chkboxSelectAllDelSchoolForSchool").prop("checked",false);
		selectedChkBoxForSchoolDel=[];
		if(deleteMultipleSchoolPermission){
			fnUpdateDTColFilterForMultiDelete('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
		} else{
			fnUpdateDTColFilter('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
		}
		setShowAllTagColor(colorClass);
		if(buttonId=="addSchoolSaveMoreButton"){
			$("#mdlAddSchool").find('#errorMessage').hide();
			$("#mdlAddSchool").find('#successMessage').show();

			$('input[name=schoolType]').prop('checked',false);
			$('input[name=openLab]').prop('checked',false);
			resetFvForAllInputExceptLoc("addSchoolForm",allInputEleNamesForSchool);
			fnDisplaySaveAndAddNewElement('#addSchoolForm',schoolName);	

		}else{			
			$('#mdlAddSchool').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ school.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			
		}		
	}else {
		showDiv($('#mdlAddSchool #alertdiv'));
		$("#addSchoolForm").find('#alertDiv').show();
		$("#addSchoolForm").find('#successMessage').hide();
		$("#addSchoolForm").find('#okButton').hide();
		$("#addSchoolForm").find('#errorMessage').show();
		$("#addSchoolForm").find('#exception').show();
		$("#addSchoolForm").find('#buttonGroup').show();
		$("#addSchoolForm").find('#exception').text(school.responseMessage);
	}
	enableTooltip();
	$(window).scrollTop(0);
	$('.outer-loader').hide();
}

//enable or disable delete all button for school
var fnEnableOrDisableDelAllButtonForSchool =function(selectedArr){
	if(selectedArr.length>=2){
		$('#btnDeleteAllCampaignSchoolForSchool').css('display','block');

	}else{
		$('#btnDeleteAllCampaignSchoolForSchool').css('display','none');

	}
}


var fnDateParsing=function(data){
	var sArray =data.split('-');
	var monthData =sArray[1];
	var dateData =sArray[0];
	var yearData =sArray[2];
	var dateStringData =Date.parse(monthData+' '+dateData+' ,'+yearData);
	return dateStringData;
}

var isValidModifiedDate=function(){

	var createdDateData=fnDateParsing($(schoolDatePicker.createdDate).datepicker('getFormattedDate'));
	var modifyDateData=fnDateParsing($(schoolDatePicker.modifyDate).datepicker('getFormattedDate'));
	if(modifyDateData<createdDateData) {
		return false;
	} 
	return true;

}

var fnGetSchoolByFiltered=function(){
	var cdDate=$('[name="createdDate"]').val();
	if(cdDate==""){
		$('.outer-loader').hide();
		return;
	}
	var isValid= isValidModifiedDate();
	if(isValid){
		var createdDate=$('[name="createdDate"]').val();
		var modifiedDate=$(schoolDatePicker.modifyDate).datepicker('getFormattedDate');
		var jsonData={
				"toDate":modifiedDate,
				"fromDate":createdDate
		}
		var schoolData=customAjaxCalling("getSchoolsBetweenPeriod", jsonData, "POST");
		deleteAllRow('schoolTable');
		selectedChkBoxForSchoolDel=[];
		fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
		$("#chkboxSelectAllDelSchoolForSchool").prop("checked",false);
		var validSchoolData=schoolData!=null?schoolData.length>0?true:false:false;
		if(validSchoolData){
			$.each(schoolData, function(index, obj) {
				var row = getNewRow(obj);
				fnAppendNewRow('schoolTable', row);
			});
			applyPermissions();
			if(deleteMultipleSchoolPermission){
				fnUpdateDTColFilterForMultiDelete('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
			} else{
				fnUpdateDTColFilter('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
			}
			$('#schoolTable').dataTable().fnDraw();
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>School filtered successfully.</p>',
				    close :'auto'
				})
				}, 1000);
		
			$('.outer-loader').hide();
		}else{
			$('.outer-loader').hide();
			$('#mdlSchoolError #msgText').text("School data not available");
			$('#mdlSchoolError').modal();
		}

	}
	$('.outer-loader').hide();

}
///////////////////// filterd by date //////////////
var schoolListByFilteredDate=function(){
	$('.outer-loader').show();
	$.wait(50).then(fnGetSchoolByFiltered);
}


//editSchoolSetData to modal
var editSchoolSetData =function(){
	
var editSchoolId =arguments[0];
var	schoolTypeId=arguments[1];
var	schoolLocationTypeId=arguments[2];
var	stateId=arguments[3];
var	zoneId=arguments[4];
var districtId=arguments[5];
var tehsilId=arguments[6];
var	blockId=arguments[7];
var	cityId=arguments[8];
var	nyayPanchayatId=arguments[9];
var	gramPanchayatId=arguments[10];
var	revenueVillageId=arguments[11];
var	villageId=arguments[12];						
var	nearestLandMark=arguments[13];
var	distanceOfSchoolFromNearestLandmark=arguments[14];
var	schoolId=arguments[15];
var	schoolName=arguments[16];
var	address=arguments[17];
var	phoneNumber=arguments[18];
var	headTeacherName=arguments[19];
var	isOpenLab=arguments[20];
var	noOfStudents=arguments[21];
var	email=arguments[22];
	var idMap = {
			'#mdlEditSchool #statesForZone': stateId,
			'#mdlEditSchool #selectBox_zonesByState':zoneId,
			'#mdlEditSchool #selectBox_districtsByZone':districtId,
			'#mdlEditSchool #selectBox_tehsilsByDistrict':tehsilId,
			'#mdlEditSchool #selectBox_blocksByTehsil':blockId,
			'#mdlEditSchool #selectBox_cityByBlock':cityId,
			'#mdlEditSchool #selectBox_npByBlock':nyayPanchayatId,
			'#mdlEditSchool #selectBox_gpByNp':gramPanchayatId,
			'#mdlEditSchool #selectBox_revenueVillagebyGp':revenueVillageId,
			'#mdlEditSchool #selectBox_villagebyRv':villageId
	};

	$('#mdlEditSchool #editPnlScDetails').removeClass('in');
	$('#mdlEditSchool #editCollapse').removeClass('in');
	$('#mdlEditSchool #editLocationPnlLink').text('Select Location');
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	hideDiv($('#mdlEditSchool #alertdiv'));

	

	getAllSchoolType();
	destroyAndRefreshMultiselect($(addMdlSelectDivs.schoolType),$(editMdlSelectDivs.schoolType));
	setOptionsForMultipleSelect($(addMdlSelectDivs.schoolType),$(editMdlSelectDivs.schoolType));
	$('input:radio[name=locType]').each(function(i,ele){
		if($(ele).data('id')==schoolLocationTypeId)
			$(ele).prop('checked',true);
	});

	reinitializeSmartFilter(elementIdMap);
	if(schoolTypeId !="" && schoolTypeId!=null)
		$('input:radio[name=schoolType]').each(function(i,ele){
			if($(ele).data('id')==schoolTypeId)
				$(ele).prop('checked',true);
		});

	refreshMultiselect($(editMdlSelectDivs.schoolType),$(editMdlSelectDivs.schoolLocationType));
	displaySmartFilters("edit");

	//make selected in smart filter for selected locations of corresponding school
	var ele_keys = Object.keys(idMap);
	$(ele_keys).each(function(ind, ele_key){
		$(ele_key).multiselect('destroy');
		if(idMap[ele_key] != "" && idMap[ele_key] != null)
			$(ele_key).find("option[value="+idMap[ele_key]+"]").prop('selected', true);		

		enableMultiSelect(ele_key);

	});	

	$('#mdlEditSchool #schoolName').val(schoolName);	
	$('#mdlEditSchool #schoolId').val(schoolId);
	$('#mdlEditSchool #address').val(address);
	$('#mdlEditSchool #phoneNumber').val(phoneNumber);
	$('#mdlEditSchool #landmark').val(nearestLandMark);
	$('#mdlEditSchool #numberOfStudent').val(noOfStudents);
	$('#mdlEditSchool #headTeacherlName').val(headTeacherName);
	$('#mdlEditSchool #distanceFromLandmark').val(distanceOfSchoolFromNearestLandmark);
	if(isOpenLab==1){
		$('#mdlEditSchool #editOpenLab').prop('checked',true);
	}else{
		$('#mdlEditSchool #editOpenLab').prop('checked',false);
	}
	$('#mdlEditSchool #editEmail').val(email);
	editedSchoolId =editSchoolId;
	fnCollapseMultiselect();


}
//save edited data
var editSchool =function(){
	initGlblVariables();
	$('.outer-loader').show();
	schoolName =$('#mdlEditSchool #schoolName').val().trim().replace(/\u00a0/g," ");
	schoolType =$('#mdlEditSchool').find('[name="schoolType"]:checked').data('id');
	schoolLocType =$('#mdlEditSchool input:radio[name=locType]:checked').data('code');
	schoolId =$('#mdlEditSchool #schoolId').val()!=""?$('#mdlEditSchool #schoolId').val().trim().replace(/\u00a0/g," "):null;
	address =$('#mdlEditSchool #address').val().trim().replace(/\u00a0/g," ");
	phoneNumber =$('#mdlEditSchool #phoneNumber').val().trim().replace(/\u00a0/g," ");
	nearestLandMark =$('#mdlEditSchool #landmark').val().trim().replace(/\u00a0/g," ");
	distanceOfSchoolFromNearestLandmark =$('#mdlEditSchool #distanceFromLandmark').val().trim().replace(/\u00a0/g," ");
	headTeacherName=$('#mdlEditSchool #headTeacherlName').val().trim().replace(/\u00a0/g," ");
	totalNumberOfStudents=$('#mdlEditSchool #numberOfStudent').val().trim().replace(/\u00a0/g," ");
	if(!$("#mdlEditSchool #editOpenLab").is(':checked')){	      
		isOpenLab=0;
	}else{
		isOpenLab=1;
	}
	if(schoolLocType.toLowerCase() =='R'.toLowerCase()){
		villageId=$('#mdlEditSchool #selectBox_villagebyRv').find('option:selected').val();
	}else{
		cityId=$('#mdlEditSchool #selectBox_cityByBlock').find('option:selected').val();
	}	
	schoolTypeId=$('#mdlEditSchool').find('[name="schoolType"]:checked').data('id');
	email=$('#editEmail').val();
	var json ={
			"id":editedSchoolId,
			"schoolId":schoolId,
			"schoolName":schoolName,				
			"address":address,
			"phoneNumber":phoneNumber,
			"nearestLandMark":nearestLandMark,
			"distanceOfSchoolFromNearestLandmark":distanceOfSchoolFromNearestLandmark,			
			"isOpenLab":isOpenLab,	
			"cityId":cityId,
			"villageId":villageId,
			"schoolTypeId":schoolType,						
			"schoolLocationTypeCode":schoolLocType,
			"noOfStudents":totalNumberOfStudents,
			"headTeacherName":headTeacherName,
			"email":email
	}
	var school=customAjaxCalling("school",json, "PUT");   
	if(school.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("schoolTable",school.id);
		var zNewRow =getNewRow(school);
		fnAppendNewRow("schoolTable",zNewRow);
		applyPermissions();
		selectedChkBoxForSchoolDel=[];
		fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
		$("#chkboxSelectAllDelSchoolForSchool").prop("checked",false);
		if(deleteMultipleSchoolPermission){
			fnUpdateDTColFilterForMultiDelete('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
		} else{
			fnUpdateDTColFilter('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);

		}
		displaySuccessMsg();	
		$('#mdlEditSchool').modal("hide");
		setTimeout(function() {  
			 AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+school.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);
		

	}
	else {
		$(".outer-loader").hide();
		showDiv($('#mdlEditSchool #alertdiv'));
		$("#mdlEditSchool").find('#successMessage').hide();
		$("#mdlEditSchool").find('#errorMessage').show();
		$("#mdlEditSchool").find('#exception').show();
		$("#mdlEditSchool").find('#buttonGroup').show();
		$("#mdlEditSchool").find('#exception').text(school.responseMessage);
	}
	enableTooltip();
	$(window).scrollTop(0);


	$('.outer-loader').hide();

	return;
}




//delete schoolSet data
var deleteSchoolSetData =function(schoolId,schoolName,isDeleteable){
	deleteSchoolId=schoolId;
	deleteSchoolName=schoolName;
	deleteFlag =isDeleteable;
	$('#mdlDelSchool #deleteSchoolMessage').text("Do you want to delete "+deleteSchoolName+ " ?");	
	$("#mdlDelSchool").modal().show();
}
$(document).on('click','#btnDelSchool', function(){
	$('.outer-loader').show();
	var school=customAjaxCalling("school/"+deleteSchoolId, null, "DELETE");
	if(school.response=="shiksha-200"){
		$(".outer-loader").hide();	
		deleteRow("schoolTable",deleteSchoolId);
		selectedChkBoxForSchoolDel = jQuery.grep(selectedChkBoxForSchoolDel, function(value) {
			  return value != deleteSchoolId;
			});
		fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
		
		if(deleteMultipleSchoolPermission){
			fnUpdateDTColFilterForMultiDelete('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
		} else {
			fnUpdateDTColFilter('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
		}
		setShowAllTagColor(colorClass);
		$('#mdlDelSchool').modal("hide") ;
		$('.outer-loader').hide();
		setTimeout(function() {   //calls click event after a one sec
			 AJS.flag({
			    type: 'success',
			    title: 'Success!',
			    body: '<p>'+school.responseMessage+'</p>',
			    close :'auto'
			})
			}, 1000);		
		
		
	}else {
		$('.outer-loader').hide();
		showDiv($('#mdlDelSchool #alertdiv'));
		$("#mdlAddBlock").find('#successMessage').hide();
		$("#mdlDelSchool").find('#errorMessage').show();
		$("#mdlDelSchool").find('#exception').text(school.responseMessage);
		$("#mdlDelSchool").find('#buttonGroup').show();
	}
	$(window).scrollTop(0);
	$('.outer-loader').hide();
	return;
});
//CR-670
var inActiveSchoolSetData =function(schoolId,schoolName,isMember,activeSchoolLocType){
	activeDeactivateSchoolId=schoolId;
	activeDeactivateSchoolName=schoolName;
	activeDeactivateFlag =isMember;
	activeDeactivateschoolLocType=activeSchoolLocType;
	var action="Inactivate"
	if(isMember=="true"){
		$("#mdlinActivateSchool #btnActiveDeactiveSchool").addClass("btn-danger").removeClass("btn-prinary");

	}else{
		action="Activate";
		$("#mdlinActivateSchool #btnActiveDeactiveSchool").removeClass("btn-danger").addClass("btn-primary")
	}
	$("#mdlinActivateSchool").find('#successMessage').hide();
	$("#mdlinActivateSchool").find('#errorMessage').hide();
   $("#mdlinActivateSchool #btnActiveDeactiveSchool").text(action);
	$('#mdlinActivateSchool #activateSchoolMessage').text("Do you want to "+action +" "+activeDeactivateSchoolName+ " ?");	
	$('#mdlinActivateSchool #activateSchoolMessage').show();
	$("#mdlinActivateSchool").modal().show();
}

var activeDeactiveSchool=function(){
    var school=customAjaxCalling("school/"+activeDeactivateSchoolId+"?activate="+activeDeactivateFlag+"&locationTypeCode="+activeDeactivateschoolLocType, null, "PUT");
	if(school.response=="shiksha-200"){
		$(".outer-loader").hide();	
		deleteRow("schoolTable",activeDeactivateSchoolId);
		var zNewRow =getNewRow(school);
		fnAppendNewRow("schoolTable",zNewRow);
		applyPermissions();
		selectedChkBoxForSchoolDel=[];
		fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
		$("#chkboxSelectAllDelSchoolForSchool").prop("checked",false);
		if(deleteMultipleSchoolPermission){
			fnUpdateDTColFilterForMultiDelete('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);
		} else{
			fnUpdateDTColFilter('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);

		}
		setShowAllTagColor(colorClass);
		$('#mdlinActivateSchool').modal("hide") ;
		$('#success-msg').show();
		$('.outer-loader').hide();
		$('#showMessage').text(school.responseMessage);
		$("#success-msg").fadeOut(3000);
	}else {
		$('.outer-loader').hide();
		showDiv($('#mdlinActivateSchool #alertdiv'));
		$("#mdlinActivateSchool").find('#successMessage').hide();
		$('#mdlinActivateSchool #activateSchoolMessage').hide();
		$("#mdlinActivateSchool").find('#errorMessage').show();
		$("#mdlinActivateSchool").find('#exception').text(school.responseMessage);
		$("#mdlinActivateSchool").find('#buttonGroup').show();
	}
	$(window).scrollTop(0);
	$('.outer-loader').hide();
	return;

}


$(document).on('click','#btnActiveDeactiveSchool', function(){
	$('.outer-loader').show();
	$.wait(50).then(activeDeactiveSchool);
});


//hide alert div
var fnHideAlertDiv =function(){
	hideDiv($('#mdlAddSchool #alertdiv'));
	hideDiv($('#mdlEditSchool #alertdiv'));
} 
//to change school detail  collaps button
$('#selectBox_cityByBlock').change(function() {
	$('#panelSchoolDetail').find("span").removeClass("glyphicon-plus-sign").removeClass("green");
	$('#panelSchoolDetail').find("span").addClass("glyphicon-minus-sign").addClass("red");
});
$('#selectBox_villagebyRv').change(function() {
	$('#panelSchoolDetail').find("span").removeClass("glyphicon-plus-sign").removeClass("green");
	$('#panelSchoolDetail').find("span").addClass("glyphicon-minus-sign").addClass("red");
});



//delete multiple schools checkbox functionality
var fnDoselectAllForSchoolDeleteForMultiSelect =function(status){	

	var rows = $("#schoolTable").dataTable().fnGetNodes();
	
	if(status){
		selectedChkBoxForSchoolDel =[];
		$.each(rows,function(i,row){			
			var id="#chkboxDelSchoolForSchool"+$(row).prop('id').toString();
			selectedChkBoxForSchoolDel.push($(row).data('id'));

		});	

	}else if(selectedChkBoxForSchoolDel.length==0){

		$.each(rows,function(){			
			$('.chkboxDelSchoolForSchool').prop('checked',false);			
		});	
	}
}








//on click of select all in table header of campaign school table (after added)
var fnInvokeSelectAllDeleteSchoolForSchool =function(){
	var isDelChecked =$('#chkboxSelectAllDelSchoolForSchool').is(':checked');

	var rows = $("#schoolTable").dataTable().fnGetNodes();
	if(isDelChecked){
		fnDoselectAllForSchoolDeleteForMultiSelect(isDelChecked);
		fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
	}else{
		selectedChkBoxForSchoolDel =[];
		$.each(rows,function(i,row){
			var id="#chkboxDelSchoolForSchool"+($(row).prop('id').toString());
			$(id).prop('checked',false);
		});
		fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
	}		
}




//Handle click on "Select all" control
$('#chkboxSelectAllDelSchoolForSchool').on('click', function(){
	selectedChkBoxForSchoolDel =[];
	
	var isChecked =$('#chkboxSelectAllDelSchoolForSchool').is(':checked');

	var dRows = $("#schoolTable").DataTable().rows({ 'search': 'applied' }).nodes();
	$('input[type="checkbox"]', dRows).prop('checked', this.checked);

	fnDoselectAllForSchoolDeleteForMultiSelect(isChecked);
	fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
});




//delete school from table by selecting checkbox
var fnChkUnchkDelSchoolForSchool =function(rId){


	var nRows = $("#schoolTable").dataTable().fnGetNodes().length;
	var idx = $.inArray(rId, selectedChkBoxForSchoolDel);	
	if (idx == -1) {
		selectedChkBoxForSchoolDel.push(rId);
		selectedChkBoxForSchoolDel.length==nRows? $('#chkboxSelectAllDelSchoolForSchool').prop('checked',true): $('#chkboxSelectAllDelSchoolForSchool').prop('checked',false);
	} else {
		selectedChkBoxForSchoolDel.splice(idx, 1);
		$('#chkboxSelectAllDelSchoolForSchool').prop('checked',false);
	}
	fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);

}

$('#mdlSchoolCheckBoxMessage #btnCheckBoxSchool').on('click',function(){	
	$('#mdlSchoolCheckBoxMessage').modal('hide');
	$("#chkboxDelSchoolForSchool"+rowIdForShowMessage).prop('checked',false);

	fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);

});

//delete all selected schools if deleteAll button pressed
var fnDeleteAllSchoolsForSchool =function(){	
	$('#mdlRemoveAllSchool #msgText').text('');
	$('#mdlRemoveAllSchool #msgText').text("Do you want to remove all selected School(s) ?");
	$('#mdlRemoveAllSchool').modal().show();
	$('#errorMessage').hide();
}

$('#mdlRemoveAllSchool #btnRemoveSchool').on('click',function(){	
	var aDtRows = $("#schoolTable").dataTable().fnGetNodes();
	
	var schoolIds=[];
	$.each(aDtRows,function(i,row){			
		if($(row).find('.chkboxDelSchoolForSchool').is(':checked') === true){
			schoolIds.push($(row).prop('id'));
		}
	});
	

	var schooldata=customAjaxCalling("school/multipleDelete/"+schoolIds, null, "DELETE");
	if(schooldata!=null && schooldata!=""){
		if(schooldata.schoolIds!=null && schooldata.schoolIds!=""){
			$(schooldata.schoolIds).each(function(index,dataObj){
				deleteRow("schoolTable",dataObj);
				selectedChkBoxForSchoolDel = jQuery.grep(selectedChkBoxForSchoolDel, function(value) {
					  return value != dataObj;
					});
				fnEnableOrDisableDelAllButtonForSchool(selectedChkBoxForSchoolDel);
			});
			$('#success-msg').show();
			$('.outer-loader').hide();
			$('#showMessage').text("School deleted");
			$("#success-msg").fadeOut(3000);
		}
		if(schooldata.notDeletSchoolNames!=null && schooldata.notDeletSchoolNames!=""){
			$('#mdlRemoveAllSchool').modal("hide");
			$('#mdlSchoolCheckBoxMessage #msgText').text('');
			$(schooldata.notDeletSchoolNames).each(function(index,dataObj){
				$('#mdlSchoolCheckBoxMessage #msgText').append('<ul><li>'+dataObj+'</li></ul>');
			});

			$('#mdlSchoolCheckBoxMessage').modal().show();

		}
		$('#mdlRemoveAllSchool').modal("hide")
	}
});

var btnCheckBoxSchool =function(){
	$('#mdlSchoolCheckBoxMessage').modal("hide");
	$('body').removeClass('modal-open');
	$('.modal-backdrop').remove();
	$('.chkboxDelSchoolForSchool').prop('checked',false);
	$('.chkboxDelAllSchoolForSchool').prop('checked',false);
	selectedChkBoxForSchoolDel=[];
	$('#btnDeleteAllCampaignSchoolForSchool').css('display','none');
}


var fnInitFilterDatePicker =function(pDatePickerId){
	var todayDate=new Date();
	 $(pDatePickerId).datepicker({
			allowPastDates: true,
			momentConfig : {
				culture : 'en', // change to specific culture
				format : 'DD-MMM-YYYY' // change for specific format
			},
		})
		$(pDatePickerId).datepicker('setDate', todayDate);
}

var validateFiltereddate=function(isValidDate){
	if(isValidDate){
		$('#modifiedDateMsg').css('display','none');
		$("#viewSchoolButton").removeClass("disabled");
		$("#divModifiedDate").removeClass("has-error");
	}else{
		$('#modifiedDateMsg').css('display','block');
		$("#viewSchoolButton").addClass("disabled");
		$("#divModifiedDate").addClass("has-error");

	}
}

$(schoolDatePicker.modifyDate).datepicker({
	allowPastDates: true,
	momentConfig : {
		culture : 'en', // change to specific culture
		format : 'DD-MMM-YYYY' // change for specific format
	}
}).on('changed.fu.datepicker dateClicked.fu.datepicker',function() {
	var modifiedDate = $(schoolDatePicker.modifyDate).datepicker('getFormattedDate');
	var isValidDate=isValidModifiedDate();
	if (modifiedDate != "Invalid Date") {
		validateFiltereddate(isValidDate);
	}
});

$(schoolDatePicker.createdDate).datepicker({
	allowPastDates: true,
	momentConfig : {
		culture : 'en', // change to specific culture
		format : 'DD-MMM-YYYY' // change for specific format
	}
}).on('changed.fu.datepicker dateClicked.fu.datepicker',function() {
	var createdDate = $(schoolDatePicker.createdDate).datepicker('getFormattedDate');
	$("#divModifiedDate").css("display","block");
	var isValidDate=isValidModifiedDate();
	if (createdDate != "Invalid Date") {
		validateFiltereddate(isValidDate);
	}
});
$(function() {
	fnInitFilterDatePicker(schoolDatePicker.createdDate);
	fnInitFilterDatePicker(schoolDatePicker.modifyDate);
	$('[name="createdDate"]').val('');
	
	thLabels =['#','State  ','Zone  ','District ','Tehsil ','City ','Block ','Village ','Nearest Land Mark ','Distance From Land Mark ','School Type ','School Location Type ','School Name ','School Code ','Full Address ','Contact Number ','Head Teacher Name ','No. of Students ','Is Open Lab ','Email ','ModifiedBy ','UploadedBy/CreatedBy ','Action'];

	if(deleteMultipleSchoolPermission){
		fnSetDTColFilterPaginationForMultiDelete('#schoolTable',23,[0,22],thLabels);
	} else{
		fnSetDTColFilterPagination('#schoolTable',23,[0,22],thLabels);
	}
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21]);
	});
	colorClass = $('#t0').prop('class');
	$('#schoolTableDiv').show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	initGlblVariables();


	//init bootstrap tooltip
	$('[data-toggle="tooltip"]').tooltip(); 




	$("#mdlAddSchool #schoolName").keypress(function(){
		hideDiv($('#alertDiv'));
	});
	$("#mdlEditSchool #schoolName").keypress(function(){
		hideDiv($('#alertDiv'));
	});
	limitTextArea('#mdlAddSchool #textAreaMsg');
	limitTextArea('#mdlEditSchool #textAreaMsg');


	
	$('#mdlAddSchool #addLocColspFilter').on('show.bs.collapse', function(){
		$('#mdlAddSchool #locationPnlLink').text('Hide Location');
		$("#collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green ");
		$("#collapseSpanSign").addClass("glyphicon-minus-sign").addClass("red");

	});  
	$('#mdlAddSchool #addLocColspFilter').on('hide.bs.collapse', function(){
		$('#mdlAddSchool #locationPnlLink').text('Select Location');
		$("#collapseSpanSign").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green");
		$("#collapseSpanSign").addClass("glyphicon-plus-sign").addClass("green");
	});

	$('#mdlEditSchool #editCollapse').on('show.bs.collapse', function(){
		$('#mdlEditSchool #editLocationPnlLink').text('Hide Location');
	});  
	$('#mdlEditSchool #editCollapse').on('hide.bs.collapse', function(){
		$('#mdlEditSchool #editLocationPnlLink').text('Select Location');
	});

	$("#pnlSchoolDetails").on('show.bs.collapse',function(){
		$(this).find(".btn-collapse").find("span").removeClass("glyphicon-plus-sign").removeClass("green");
		$(this).find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
	});
	$("#pnlSchoolDetails").on('hide.bs.collapse',function(){
		$(this).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
		$(this).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	});
	$("#editPnlSchoolDetails").on('show.bs.collapse',function(){
		$(this).find(".btn-collapse").find("span").removeClass("glyphicon-plus-sign").removeClass("green");
		$(this).find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
	});
	$("#editPnlSchoolDetails").on('hide.bs.collapse',function(){
		$(this).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
		$(this).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	});

	$("#editCollapseFilter").on('show.bs.collapse',function(){
		$(this).find(".btn-collapse").find("span").removeClass("glyphicon-plus-sign").removeClass("green");
		$(this).find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
	});
	$("#editCollapseFilter").on('hide.bs.collapse',function(){
		$(this).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
		$(this).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	});
	
	$("#addSchoolForm").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields: {
			schoolType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options =$('#mdlAddSchool').find('[name="schoolType"]:checked').val();
							return (options !=null);
						}
					}
				}
			},

			schoolLocType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options =$('#mdlAddSchool #selectBox_schoolLocationType').val();							
							return (options != null);
						}
					}
				}
			},
			schoolName: {
				validators : {
					stringLength: {
						max: 250,
						message: 'School name must be less than 250 characters'
					},
					callback : {
						message : 'School name '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;

							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			schoolId: {
				validators : {
					stringLength: {
						max: 250,
						message: 'School code must be less than 250 characters'
					},
					callback : {
						message : 'School code '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			nearestLandmarkToSchool: {
				validators : {
					stringLength: {
						max: 250,
						message: 'Landmark name must be less than 250 characters'
					},
					callback : {
						message : 'Landmark '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			distanceFromLandmark: {
				validators : {
					callback : {
						message : 'Please enter valid number/decimal number upto 2 decimal places',
						callback : function(value) {
							var regx=/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/;
							return regx.test(value);}
					}
				}
			},
			totalNumberOfStudentInSchool: {
				validators: {
					integer: {
						message: 'Please enter valid number',
						// The default separators
						thousandsSeparator: '',
						decimalSeparator: '.'
					}
				}
			},
			phoneNumber: {
				validators : {
					integer: {
						message: 'Enter valid phone no.'
					}
			/*callback : {
						message : 'Enter a valid 10 digit phone number',
						callback : function(value, validator, $field) {
							// if(/^[0][0-9]*$/.test(value) && /^[0]\d{2,4}\d{6,8}$/.test(value)){
							if(/^[0][1-9]{2}[0-9]{7}$/.test(value) && /^[0][0-9]{3}[0-9]{6}$/.test(value) && /^[0][0-9]{4}[0-9]{5}$/.test(value) ){
									console.log("lanline...");
									return true;
								}else if(/^[789]\d{9}$/.test(value)){
									console.log("mobile");
									return true;
								}
						return false;							
						}
					}*/
				}
			},
			headTeacherlName: {
				validators : {
					stringLength: {
						max: 250,
						message: 'Head Teacher name must be less than 250 characters'
					},
					callback : {
						message : 'Head Teacher '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;

							return regx.test(value);}
					}
				}
			},
			address: {
				validators : {
					callback : {
						message : 'Address does not contains these (", ?, \', /, \\) special characters',
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;

							return regx.test(value);}
					}
				}
			},
			addEmail: {
				validators: {
					emailAddress: {
						message: 'The value is not a valid email address'
					},
					regexp: {
						regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message: 'The value is not a valid email address'
					}
				}
			}

		}
	}) .on('err.form.fv', function(e) {
		$('#mdlAddSchool #pnlScDetails').collapse('show');
	}).on('err.validator.fv', function(e, data) {
		data.element
		.data('fv.messages')
		.find('.help-block[data-fv-for="' + data.field + '"]').hide()
		.filter('[data-fv-validator="' + data.validator + '"]').show();

	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addSchool(e);
	});




	$("#editSchoolForm").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields: {
			schoolType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options =$('#mdlEditSchool').find('[name="schoolType"]:checked').val();
							return (options != null);
						}
					}
				}
			},

			schoolLocType: {
				validators: {
					callback: {
						message: '      ',
						callback: function() {
							// Get the selected options
							var options =$('#mdlEditSchool #selectBox_schoolLocationType').val();
							return (options != null);
						}
					}
				}
			},			

			schoolName: {	
				validators : {
					stringLength: {
						max: 250,
						message: 'School name must be less than 250 characters'
					},
					callback : {
						message : 'School name '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			schoolId: {
				validators : {
					stringLength: {
						max: 250,
						message: 'School code must be less than 250 characters'
					},
					callback : {
						message : 'School code '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			nearestLandmarkToSchool: {
				validators : {
					stringLength: {
						max: 250,
						message: 'LandMark must be less than 250 characters'
					},
					callback : {
						message : 'Landmark '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			/*distanceFromLandmark : {
                validators: {
                    numeric: {
                        message: 'Please enter valid number/decimal number',
                        // The default separators
                        thousandsSeparator: '',
                        decimalSeparator: '.'
                    }
                }
            },*/
			distanceFromLandmark: {
				validators : {
					callback : {
						message : 'Please enter valid number/decimal number upto 2 decimal places',
						callback : function(value) {
							var regx=/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/;
							return regx.test(value);}
					}
				}
			},
			totalNumberOfStudentInSchool: {
				validators: {
					integer: {
						message: 'Please enter valid number',
						// The default separators
						thousandsSeparator: '',
						decimalSeparator: '.'
					}
				}
			},
			phoneNumber: {
				validators : {
					integer: {
						message: 'Enter valid phone no.'
					}
			/*callback : {
						message : 'Enter a valid 10 digit phone number',
						callback : function(value, validator, $field) {
							// if(/^[0][0-9]*$/.test(value) && /^[0]\d{2,4}\d{6,8}$/.test(value)){
							if(/^[0][1-9]{2}[0-9]{7}$/.test(value) && /^[0][0-9]{3}[0-9]{6}$/.test(value) && /^[0][0-9]{4}[0-9]{5}$/.test(value) ){
									console.log("lanline...");
									return true;
								}else if(/^[789]\d{9}$/.test(value)){
									console.log("mobile");
									return true;
								}
						return false;							
						}
					}*/
				}
			},
			headTeacherlName: {
				validators : {
					stringLength: {
						max: 250,
						message: 'Head Teacher Name must be less than 250 characters'
					},
					callback : {
						message : 'HeadT eacherl Name '+invalidInput,
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			address: {
				validators : {
					callback : {
						message : 'Address does not contains these (", ?, \', /, \\) special characters',
						callback : function(value) {
							var regx=/^[^'?\"/\\~`;:]*$/;

							return regx.test(value);}
					}
				}
			},
			email: {
				validators: {
					emailAddress: {
						message: 'The value is not a valid email address'
					},
					regexp: {
						regexp: '^[^@\\s]+@([^@\\s]+\\.)+[^@\\s]+$',
						message: 'The value is not a valid email address'
					}
				}
			}

		}

	}) .on('err.form.fv', function(e) {
		$('#mdlEditSchool #editPnlScDetails').collapse('show');
	}).on('err.validator.fv', function(e, data) {
		data.element
		.data('fv.messages')
		.find('.help-block[data-fv-for="' + data.field + '"]').hide()
		.filter('[data-fv-validator="' + data.validator + '"]').show();

	}).on('success.form.fv', function(e) {
		e.preventDefault();
		editSchool();
	});
});