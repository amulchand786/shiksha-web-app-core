/* 
 * @Mukteshwar 
 * 
*/
var  colorClass='';
var gEvent;
var thLabels=[];
var prevGradeTagValue='';
var sgDelGradeName='';


var initInput =function(){	
	if ($('#gradeTable >tbody>tr').length == 10){
		$('#mdlErrorForGrade #msgTextForGrade').text('');
		$('#mdlErrorForGrade #msgTextForGrade').text("You have added already 10 grades. So you can not add more grades.");
		$('#mdlErrorForGrade').modal().show();	
		$('#mdlAddGrade').modal('hide');	
	}else{
		$("#mdlAddGrade").modal().show();
		$("#addGradeName").val('');
		$("#mdlAddGrade #addGradeForm").find("#alertDiv #errorMessage").hide();
		$("#mdlAddGrade #addGradeForm").data('formValidation').resetForm();	
		$('#mdlAddGrade #addSequenceNumber').multiselect('destroy')
		bindSequenceNumberFromGradeToListBox($('#mdlAddGrade #addSequenceNumber'),0);
		setOptionsForMultipleSelectWithoutSearch('#addSequenceNumber');
	}
}


var applyPermissions=function(){
	if (!editGradePermission){
	$(".btnEditGrade").remove();
		}
	if (!deleteGradePermission){
	$(".btnDeleteGrade").remove();
		}
      }


//create a row after save
var getGradeRow =function(grade){
	var row = 
		"<tr id='"+grade.gradeId+"' data-id='"+grade.gradeId+"'>"+
		"<td></td>"+
		"<td data-gradeid='"+grade.gradeId+"'	title='"+grade.gradeName+"'>"+grade.gradeName+"</td>"+
		"<td data-gradecode='"+grade.gradeCode+"' title='"+grade.gradeCode+"'>"+grade.gradeCode+"</td>"+	
		"<td data-sequenceNumber='"+grade.sequenceNumber+"' title='"+grade.sequenceNumber+"'>"+grade.sequenceNumber+"</td>"+
		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top btnEditGrade'	 id='btnEditGrade'" +
		"onclick='editGradeData(&quot;"+grade.gradeId+"&quot;,&quot;"+escapeHtmlCharacters(grade.gradeName)+"&quot;,&quot;"+grade.gradeName+"&quot;,&quot;"+grade.sequenceNumber+"&quot;);'> <span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12' ></span></a><a  style='margin-left:10px;' class='tooltip tooltip-top btnDeleteGrade'	 id='deleteGrade"+grade.gradeId+"'	onclick='deleteGradeData(&quot;"+grade.gradeId+"&quot;,&quot;"+escapeHtmlCharacters(grade.gradeName)+"&quot;,&quot;"+grade.gradeName+"&quot;);'><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	return row;
}

var addGradeData=function(event){
	
	var gradeId=0;
	var gradeName = $('#addGradeName').val().trim().replace(/\u00a0/g," ");
	var sequenceNumber = $('#addSequenceNumber').val();
	var json ={ 
			"gradeId" : gradeId,
			"gradeName" : gradeName,
			"sequenceNumber" : sequenceNumber
	};     

	//update data table without reloading the whole table
	//save&add new
var	btnAddMoreGradeId =$(event.target).data('formValidation').getSubmitButton().data('id');

	var grade = customAjaxCalling("grade", json, "POST");
	if(grade!=null){
		if(grade.response=="shiksha-200"){
			$(".outer-loader").hide();

			var newRow =getGradeRow(grade);
			appendNewRow("gradeTable",newRow);
			applyPermissions();
			fnUpdateDTColFilter('#gradeTable',[],5,[0,4],thLabels);
			setShowAllTagColor(colorClass);
			if(btnAddMoreGradeId=="addGradeSaveMoreButton"){

				fnDisplaySaveAndAddNewElement('#mdlAddGrade',gradeName);		
				initInput();
			}else{	
				$("#mdlAddGrade").modal("hide");
				setTimeout(function() {   //calls click event after a one sec
					 AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>'+ grade.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);
				
			}	
		}else {
			showDiv($('#mdlAddGrade #alertdiv'));
			$("#mdlAddGrade").find('#successMessage').hide();
			$("#mdlAddGrade").find('#okButton').hide();
			$("#mdlAddGrade").find('#errorMessage').show();
			$("#mdlAddGrade").find('#exception').show();
			$("#mdlAddGrade").find('#buttonGroup').show();
			$("#mdlAddGrade").find('#exception').text(grade.responseMessage);
		}
	}

	$(".outer-loader").hide();
};




////////////////////////////////////////////////


var editGradeId=null;
var deleteGradeName=null;
var deleteGradeId=null;
var editGradeData=function(gradeId,gradeName , editGrdSgName,editSequenceNumber){
	prevGradeTagValue =editGrdSgName.replace(/\u00a0/g," ");
	
	$('#editGradeName').val(gradeName);	
	editGradeId=gradeId;
	$('#mdlEditGrade #editSequenceNumber').multiselect('destroy')
	bindSequenceNumberFromGradeToListBox($('#mdlEditGrade #editSequenceNumber'),editSequenceNumber);
	setOptionsForMultipleSelectWithoutSearch('#editSequenceNumber');
	$("#mdlEditGrade").modal().show();
	$("#mdlEditGrade #editGradeForm").data('formValidation').resetForm();
};




var editData=function(){
	$(".outer-loader").show();
	var gradeName = $('#editGradeName').val().trim().replace(/\u00a0/g," ");  
	var sequenceNumber= $('#editSequenceNumber').val();
	var json ={ 
			"gradeId" : editGradeId,
			"gradeName" : gradeName,
			"sequenceNumber" : sequenceNumber,
	};



	var grade = customAjaxCalling("grade", json, "PUT");
	if(grade!=null){
		if(grade.response=="shiksha-200"){
			$(".outer-loader").hide();
			deleteRow("gradeTable",grade.gradeId);
			var newRow =getGradeRow(grade);
			appendNewRow("gradeTable",newRow);
			applyPermissions();
			displaySuccessMsg();	
			
			fnUpdateDTColFilter('#gradeTable',[],5,[0,4],thLabels);
			setShowAllTagColor(colorClass);
			$('#mdlEditGrade').modal("hide");
			
			setTimeout(function() {   
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ grade.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			
		}		
		else {

			showDiv($('#mdlEditGrade #alertdiv'));
			$("#mdlEditGrade").find('#successMessage').hide();
			$("#mdlEditGrade").find('#errorMessage').show();
			$("#mdlEditGrade").find('#exception').show();
			$("#mdlEditGrade").find('#buttonGroup').show();
			$("#mdlEditGrade").find('#exception').text(grade.responseMessage);
		}
	}
	$(".outer-loader").hide();
};




/////////////


 
 var deleteGradeData=function(gradeId,gradeName,delGrdSgName){
		deleteGradeId=gradeId;
		deleteGradeName=gradeName;
		
		sgDelGradeName =delGrdSgName.replace(/\u00a0/g," ");
		
		$('#mdlDeleteGrade #deleteGradeMessage').text("Do you want to delete "+gradeName+ " ?");	
		$("#mdlDeleteGrade").modal().show();
		hideDiv($('#mdlDeleteGrade #alertdiv'));
	};


	$(document).on('click','#deleteGradeButton', function(){
		
	 	    
	    var grade = customAjaxCalling("grade/"+deleteGradeId, null, "DELETE");
	    if(grade!=null){
		if(grade.response=="shiksha-200"){
			$(".outer-loader").hide();
			
			// Delete Row from data table and refresh the table without refreshing the whole table
			deleteRow("gradeTable",deleteGradeId);
			fnUpdateDTColFilter('#gradeTable',[],5,[0,4],thLabels);
			setShowAllTagColor(colorClass);
			
			$('#mdlDeleteGrade').modal("hide");
			
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ grade.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
						
		} else {
			showDiv($('#mdlDeleteGrade #alertdiv'));
			$("#mdlDeleteGrade").find('#errorMessage').show();
			$("#mdlDeleteGrade").find('#exception').text(grade.responseMessage);
			$("#mdlDeleteGrade").find('#buttonGroup').show();
		}
	    }
		$(".outer-loader").hide();
	});

///////////

		

var fnHideGradeAlertDiv =function(){
	hideDiv($('#mdlAddGrade #alertdiv'));
	hideDiv($('#mdlEditGrade #alertdiv'));
}


$(function() {

	thLabels =['#','Grade Name  ','Grade Code  ','Grade Number ','Action'];
	fnSetDTColFilterPagination('#gradeTable',5,[0,4],thLabels);
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#gradeTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();

	fnHideGradeAlertDiv();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	setOptionsForMultipleSelectWithoutSearch('#mdlAddGrade #addSequenceNumber');
	setOptionsForMultipleSelectWithoutSearch('#mdlEditGrade #editSequenceNumber');
	
    $("#addGradeForm") .formValidation({
    	excluded : ':disabled',
    	feedbackIcons : {
    		validating : 'glyphicon glyphicon-refresh'
		},fields : {
			addGradeName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: 'Grade name must be less than 250 characters'
                    },
					callback : {
						message : 'Grade '+invalidInputMessage,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
			addSequenceNumber : {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var options = $('#addSequenceNumber').val();
							return (options != null || options !="null");
						}
					}
				}
			}
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();           
             addGradeData(e);
      
        });
    
    $("#editGradeForm") .formValidation({
    	feedbackIcons : {
    		validating : 'glyphicon glyphicon-refresh'

		},fields : {
			editGradeName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: 'Grade name must be less than 250 characters'
                    },
					callback : {
						message : 'Grade '+invalidInputMessage,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			}
		}
    })
        .on('success.form.fv', function(e) {
             e.preventDefault();             
             editData();
      
        });
});