var pageContextElements = {	
		addButton 						: '#addNewShikshaPlusNugget',
		table 							: '#shikshaPlusNuggetTable',
		toolbar							: "#toolbarShikshaPlusNugget"
};
var addModal ={
		form 			: '#addShikshaPlusNuggetForm',
		modal   		: '#mdlAddShikshaPlusNugget',
		saveMoreButton	: 'addShikshaPlusNuggetSaveMoreButton',
		saveButton		: 'addShikshaPlusNuggetSaveButton',
		eleName		   	: '#addShikshaPlusNuggetName',
		eleDescription	: '#addDescription',
		eleSequence		: '#addShikshaPlusNuggetSequence',
		eleSource		: '#addSource',
		eleLevel		: '#addLevel',
		eleGroup		: '#addGroup',
		eleGroupDiv	    : '#groupDiv',
		eleChapter		: '#addChapter',
		eleChapterDiv	: '#chapterDiv',
		eleNuggetDiv	: '#nuggetDiv',
};
var editModal ={
			form	 : '#editShikshaPlusNuggetForm',
			modal	 : '#mdlEditShikshaPlusNugget',
			eleName	 : '#editShikshaPlusNuggetName',
			eleId	 : '#nuggetId',
			eleDescription	: '#editDescription',
			eleSequence		: '#editShikshaPlusNuggetSequence',
			eleSource		: '#editSource',
			eleLevel		: '#editLevel',
			eleGroup		: '#editGroup',
			eleChapter		: '#editChapter',
			eleChapterDiv	: '#editChapterDiv',
			eleNuggetDiv	: '#nuggetDiv',
};
var deleteModal ={
			modal	: '#mdlDeleteShikshaPlusNugget',
			confirm : '#deleteShikshaPlusNuggetButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var ShikshaPlusNuggetIdForDelete;

var shikshaPlusNuggetFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var shikshaPlusNugget={
		name:'',
		shikshaPlusNuggetId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',shikshaPlusNugget.initAddModal);
			$(deleteModal.confirm).on('click',shikshaPlusNugget.deleteFunction);
			$(addModal.eleLevel).on('change',shikshaPlusNugget.showHideGroups);
			$(addModal.eleGroup).on('change',shikshaPlusNugget.showHideChapters);
			$(addModal.eleChapter).on('change',shikshaPlusNugget.showHideNugget);
			$(editModal.eleLevel).on('change',shikshaPlusNugget.showHideGroupsInEdit);
			$(editModal.eleGroup).on('change',shikshaPlusNugget.showHideChaptersInEdit);
		},
		deleteFunction : function(){
			shikshaPlusNugget.fnDelete(ShikshaPlusNuggetIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			shikshaPlusNugget.resetForm(addModal.form);
			shikshaPlus.fnGetSources(addModal.eleSource,0);
			setOptionsForMultipleSelect(addModal.eleSource);
			$(addModal.eleSource).multiselect("refresh");
			shikshaPlus.fnGetLevels(addModal.eleLevel,0);
			setOptionsForMultipleSelect(addModal.eleLevel);
			$(addModal.eleLevel).multiselect("refresh");
			$(addModal.eleGroupDiv).hide();
			$(addModal.eleChapterDiv).hide();
			$(addModal.eleNuggetDiv).hide();
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		showHideGroups: function(){
			shikshaPlus.fnGetGroupsByLevelId(addModal.eleGroup,0,$(addModal.eleLevel).val());
			setOptionsForMultipleSelect(addModal.eleGroup);
			$(addModal.eleGroup).multiselect("rebuild");
			$(addModal.eleGroupDiv).show();
		},
		showHideChapters: function(){
			shikshaPlus.fnGetChapters(addModal.eleChapter,0,$(addModal.eleLevel).val(),$(addModal.eleGroup).val());
			setOptionsForMultipleSelect(addModal.eleChapter);
			$(addModal.eleChapter).multiselect("rebuild");
			$(addModal.eleChapterDiv).show();
		},
		showHideNugget: function(){
			$(addModal.eleNuggetDiv).show();
		},
		showHideGroupsInEdit: function(){
			shikshaPlus.fnGetGroupsByLevelId(editModal.eleGroup,0,$(editModal.eleLevel).val());
			setOptionsForMultipleSelect(editModal.eleGroup);
			$(editModal.eleGroup).multiselect("rebuild");
			shikshaPlus.fnGetChapters(editModal.eleChapter,0,0,0);
			setOptionsForMultipleSelect(editModal.eleGroup);
			$(editModal.eleChapter).multiselect("rebuild");
		},
		showHideChaptersInEdit: function(){
			shikshaPlus.fnGetChapters(editModal.eleChapter,0,$(editModal.eleLevel).val(),$(editModal.eleGroup).val());
			setOptionsForMultipleSelect(editModal.eleChapter);
			$(editModal.eleChapter).multiselect("rebuild");
			
		},
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name"		  :$(addModal.eleName).val(),
					"description" : $(addModal.eleDescription).val(),
					"sequence"	  : $(addModal.eleSequence).val(),
					"shikshaPlusSource" : { "sourceId": $(addModal.eleSource).val() },
					"shikshaPlusChapter" : {"chapterId": $(addModal.eleChapter).val()},
					"level" 			 : {"levelId": $(addModal.eleLevel).val()},
					"group" 			 : {"groupId": $(addModal.eleGroup).val()},
			}
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/nugget", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						
						$(addModal.eleGroupDiv).hide();
						$(addModal.eleChapterDiv).hide();
						$(addModal.eleNuggetDiv).hide();
						$(addModal.modal).modal("show");
					}
					shikshaPlusNugget.resetForm(addModal.form);
					$(addModal.eleSource).multiselect("rebuild");
					$(addModal.eleLevel).multiselect("rebuild");
					shikshaPlusNugget.refreshTable(pageContextElements.table);
					$(addModal.modal).find("#divSaveAndAddNewMessage").show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(shikshaPlusNuggetId){
			shikshaPlusNugget.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/nugget/"+shikshaPlusNuggetId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.name);
					$(editModal.eleId).val(editAjaxCall.nuggetId);
					$(editModal.eleDescription).val(editAjaxCall.description);
					$(editModal.eleSequence).val(editAjaxCall.sequence);
					shikshaPlus.fnGetSources(editModal.eleSource,editAjaxCall.shikshaPlusSource.sourceId);
					setOptionsForMultipleSelect(editModal.eleSource);
					shikshaPlus.fnGetLevels(editModal.eleLevel,editAjaxCall.level.levelId);
					setOptionsForMultipleSelect(editModal.eleLevel);
					shikshaPlus.fnGetGroupsByLevelId(editModal.eleGroup,editAjaxCall.group.groupId,$(editModal.eleLevel).val());
					setOptionsForMultipleSelect(editModal.eleGroup);
					shikshaPlus.fnGetChapters(editModal.eleChapter,editAjaxCall.shikshaPlusChapter.chapterId,$(editModal.eleLevel).val(),$(editModal.eleGroup).val());
					setOptionsForMultipleSelect(editModal.eleChapter);
					$(editModal.eleSource).multiselect("rebuild");
					$(editModal.eleLevel).multiselect("rebuild");
					$(editModal.eleGroup).multiselect("rebuild");
					$(editModal.eleChapter).multiselect("rebuild");
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"		:$(editModal.eleName).val(),
					"nuggetId"	:$(editModal.eleId).val(),
					"sequence"	  : $(editModal.eleSequence).val(),
					"description" : $(editModal.eleDescription).val(),
					"shikshaPlusSource" : { "sourceId": $(editModal.eleSource).val() },
					"shikshaPlusChapter" : {"chapterId": $(editModal.eleChapter).val()},
					"level" 			 : {"levelId": $(editModal.eleLevel).val()},
					"group" 			 : {"groupId": $(editModal.eleGroup).val()},
			}

			var updateAjaxCall = shiksha.invokeAjax("shikshaplus/nugget", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					shikshaPlusNugget.resetForm(editModal.form);
					shikshaPlusNugget.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: appMessgaes.success,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(shikshaPlusNuggetId,name){
			var msg = $("#deleteShikshaPlusNuggetMessage").data("message");
			$("#deleteShikshaPlusNuggetMessage").html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			ShikshaPlusNuggetIdForDelete=shikshaPlusNuggetId;
		},
		fnDelete :function(shikshaPlusNuggetId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/nugget/"+shikshaPlusNuggetId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					shikshaPlusNugget.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.success,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					source:{
						required:true,
					},
					level:{
						required:true,
					},
					group:{
						required:true,
					},
					chapter:{
						required:true,
					},
					shikshaPlusNuggetName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					shikshaPlusNuggetSequence:{
						required: true,
						noSpace:true,
					},
					shikshaPlusNuggetDescription: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					}
				},

				messages: {
					source:{
						required:nuggetMessages.source,
					},
					level:{
						required:nuggetMessages.level,
					},
					group:{
						required:nuggetMessages.group,
					},
					chapter:{
						required:nuggetMessages.chapter,
					},
					shikshaPlusNuggetName: {
						required: nuggetMessages.nameRequired,
						noSpace: nuggetMessages.nameNoSpace,
						minlength: nuggetMessages.nameMinLength,
						maxlength: nuggetMessages.nameMaxLength,

					},
					shikshaPlusNuggetSequence:{
						required: nuggetMessages.sequenceRequiered,
						noSpace:nuggetMessages.sequenceRequiered,
					},
					shikshaPlusNuggetDescription: {
						required: nuggetMessages.descriptionRequired,
						minlength: nuggetMessages.descriptionMinLength,
						maxlength: nuggetMessages.descriptionMaxLength,
						noSpace: nuggetMessages.descriptionNoSpace,
					}
				},
				errorPlacement: function(error, element) {
					$(element).parent().append(error);
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						shikshaPlusNugget.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						shikshaPlusNugget.fnUpdate();
					}
				}
			});
		},
		shikshaPlusNuggetActionFormater: function(value, row) {
			
			return '<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">'
			+'<li><a onclick="shikshaPlusNugget.initEdit(\''+row.nuggetId+'\')" href="javascript:void(0)">Edit</a></li>'
			+'<li><a onclick="shikshaPlusNugget.initDelete(\''+row.nuggetId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">Delete</a></li>'
			+'</ul>';
		},
		shikshaPlusNuggetNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	shikshaPlusNugget.formValidate(addModal.form);
	shikshaPlusNugget.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	shikshaPlusNugget.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar ,
		url : "shikshaplus/nugget/page",
		method : "post",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		formatShowingRows : shikshaPlusNuggetFilterObj.formatShowingRows,
		clickToSelect: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});