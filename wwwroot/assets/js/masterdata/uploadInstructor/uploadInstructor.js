var pageContextElements = {
		divFilter   			: '#divFilter',
		eleLocType  			: '#divCenterLocType input:radio[name=locType]',
		filter					: '#locColspFilter',
		panelLink				: '#locationPnlLink',
		form					: '#uploadLearnerForm',
		eleCollapseFilter		: '#collapseFilter',
		eleToggleCollapse		: '[data-toggle="collapse"]',
		clearBtn				: '#btnResetLocation',
		levelDiv				: '#divSelectLevel',
		batchDiv				: '#divSelectBatch',
		academicYearDiv 		: '#divSelectAcademicYear',
		btnsRow					: '#btnsRow',
		uploadBtn				:'#uploadBtn',
		uploadFileDiv           :"#uploadFileDiv",
		inputFile               :"#uploadLearnerFile",
		cancelButton			:'#cancelButton',
		downloadButton			:'#downloadButton',
		messageCloseBtn     :"#messageCloseBtn",
		failureMessageDiv:"#failureMessageDiv"
};


var selectBox ={
		center		: '#selectBox_centersByLoc',
		
};

var reset = {
		modal : '#mdlResetLoc',
		confirm : '#btnResetLocYes',
	};
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		level 				: "#selectBox_Level",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;

var locationUtility={
		fnInitLocation :function(){
			$(pageContextElements.divFilter).css('display','none');
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(reset.modal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},

		doResetLocation : function() {
			 $(pageContextElements.uploadFileDiv).hide();
			 $(pageContextElements.inputFile).val('');
			 uploadInstructor.resetForm(pageContextElements.form);
			if ($(resetButtonObj).parents("form").attr("id") == 'uploadLearnerForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);

			}
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},

		//displaying robo-filters
		displaySmartFilters : function(lCode,lvlName){

			elementIdMap = {
					"State" 	: '#statesForZone',
					"Zone" 		: '#selectBox_zonesByState',
					"District" 	: '#selectBox_districtsByZone',
					"Tehsil" 	: '#selectBox_tehsilsByDistrict',
					"Block" 	: '#selectBox_blocksByTehsil',
					"City" 		: '#selectBox_cityByBlock',
					"Nyaya Panchayat" 	: '#selectBox_npByBlock',
					"Gram Panchayat" 	: '#selectBox_gpByNp',
					"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
					"Village" 	: '#selectBox_villagebyRv',
					"Center" 	: '#selectBox_centersByLoc'
			};

			displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));

			locationUtility.reinitializeSmartFilter(elementIdMap);

			var lLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
			var lLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');

			displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));

			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(
					function(ind, ele_key) {
						$(elementIdMap[ele_key]).find("option:selected").prop(
								'selected', false);
						$(elementIdMap[ele_key]).multiselect('destroy');
						enableMultiSelect(elementIdMap[ele_key]);
					});
			setTimeout(function() {
				$(".outer-loader").hide();
				$('#rowDiv1,#btnsRow,#divFilter').show();
			}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});

			$('#rowDiv1,#divFilter').show();

			spinner.hideSpinner();
		},

};

var uploadInstructor = {

		fnInit:function(){
			doResetLocation=locationUtility.doResetLocation;

			 $(pageContextElements.cancelButton).on("click",uploadInstructor.fnCancel);
			$(pageContextElements.downloadButton).on("click",uploadInstructor.fnDownloadTemplate);
			$(selectBox.center).on('change',uploadInstructor.showUploadFile);
			

			$(pageContextElements.eleLocType).change(function() {				
				$(pageContextElements.filter).removeClass('in');
				$(pageContextElements.panelLink).text(centerColumn.selectLocation);
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");				
				 $(pageContextElements.uploadFileDiv).hide()				

				var typeCode =$(this).data('code');

				if(typeCode.toLowerCase() =='U'.toLowerCase()){
					showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("U");
					fnCollapseMultiselect();
				}else{
					hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("R");
					fnCollapseMultiselect();

				}
				$(pageContextElements.eleCollapseFilter).css('display','');
			});
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(pageContextElements.failureMessageDiv).hide();
			$(pageContextElements.failureMessageDiv).empty();
			
		},
		 
		showUploadFile : function(){
			 $(pageContextElements.uploadFileDiv).show();
		},
		 submitUploadFile : function(file){
		
			var formData = new FormData();
			formData.append( 'uploadfile',file );
			formData.append( 'center',$(selectBox.center).val() );
			
			spinner.showSpinner();
			$.ajax({
				url: "user/upload",
				type: "POST",
				data: formData,
				enctype: 'multipart/form-data',
				processData: false,
				contentType: false,
				cache: false,
				success: function (response) {
					spinner.hideSpinner();
					if(response.response == "shiksha-200"){
						$(pageContextElements.failureMessageDiv).hide();
						
						$(pageContextElements.inputFile).val('');
						AJS.flag({
							type  : "success",
							title : appMessgaes.success,
							body  : response.responseMessage,
							close : 'auto'
						});		

					} else {

						$(pageContextElements.failureMessageDiv).empty();

						var msgDiv= '<div class="aui-message aui-message-error closeable" >'+
						'<p class="title"><strong><span id="errorHeader">'+response.responseMessage+'</span></strong></p>'+
						'<p class="instructors"><ul></ul> </p>'+
						'<p> <br><br>'+uploadMessages.correctAndUploadMsg+'</p>'+
						'<span class="aui-icon icon-close" role="button" id="messageCloseBtn" tabindex="0"></span> </div>';	
						$(pageContextElements.failureMessageDiv).append(msgDiv);
						$(pageContextElements.messageCloseBtn).on('click',function(){	
							$(pageContextElements.failureMessageDiv).empty();
							$(pageContextElements.failureMessageDiv).hide();});

						if(response.responseMessages!=null?response.responseMessages.length>0:false){
							var item;
							$.each(response.responseMessages,function(index,value){
								item='<li>'+value+'</li>';
								$('.instructors').append(item);

							})
						}
						$(pageContextElements.failureMessageDiv).show();
					}
				},
				error: function (error) {
					spinner.hideSpinner();
					uploadInstructor.fnShowError(appMessgaes.serverError);

				}
			});
		},
		fnDownloadTemplate:function(){
			var villageName=$('#selectBox_villagebyRv').find('option:selected').text();
			var schoolName=$(selectBox.center).find('option:selected').text();
			var excelFileName=villageName+"_"+schoolName+".xlsx";
			window.location=baseContextPath+'/user/download?type=instructor&excelFileName='+excelFileName;
		},
		fnCancel:function(){
			$(pageContextElements.inputFile).val('');
			uploadInstructor.resetForm(pageContextElements.form);
		},
		fnHideMsg:function(){
		$(pageContextElements.failureMessageDiv).empty();
		},
		fnShowError:function(message){
			AJS.flag({
				type  : "error",
				title : appMessgaes.error,
				body  : message,
				close : 'auto'
			});
		},
		formValidate : function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					uploadfile: {
						required: true,
						fileFormat:true
					},
				},
				messages: {
					uploadfile: {
						required:uploadMessages.fileRequired,
						fileFormat:uploadMessages.fileFormat.replace("@EXTENSION", ".xlsx"),
					},

				},
				errorPlacement: function(error, element) {
					$(error).insertAfter(element);
				},
				submitHandler : function(){
					uploadInstructor.submitUploadFile($(pageContextElements.inputFile)[0].files[0]);
				}
			});
		},


};



$( document ).ready(function() {
	uploadInstructor.formValidate(pageContextElements.form);
	uploadInstructor.fnInit();
	$('input[name=uploadfile]').change(function() {
		uploadInstructor.resetForm(pageContextElements.form);
	   
	});


	jQuery.validator.addMethod("noSpace", function(value) {
		return !value.trim().length <= 0;
	}, "");
	
	jQuery.validator.addMethod("fileFormat", function() {

    	var filename=	$(pageContextElements.inputFile).val();
    	var fileExtensionName = filename.split('.').pop();
		if (fileExtensionName!="xlsx") {
			return false;
		}
		return true;
	 
	}, "");

});