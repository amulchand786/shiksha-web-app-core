var pageContextElements = {	
		eleAddButton 	: '#addNewLearner',
		table 			: '#learnerTable',
};
var addModal ={
	form 			: '#addLearnerForm',
	modal   		: '#mdlAddLearner',
	saveMoreButton	: '#addLearnerSaveMoreButton',
	saveButton		: '#addLearnerSaveButton',
	eleLearner      : '#addLearnerName',
	eleFormNo		: '#addFormNumber',
	eleAge			: '#addLearnerAge',
	eleGender		: '#addGender:checked',
	eleAddress		: '#addLearnerAddress',
	eleEyeTestResult: '#addEyeTestResult',
	elePreTestScore	: '#addPreTestScore',
	elePostTestScore: '#addPostTestScore',
	eleOccupation	: '#addOccupation',
	eleMaritalStatus: '#addMaritalStatus',
	eleSpouseDetails: "#spouseDetails",
	eleSpouseName	: '#addSpouseName',
	eleSpouseAge	: '#addSpouseAge',
	eleSpouseIncome	: '#addSpouseIncome',
	eleSpouseLocation: '#addSpouseWorkLocation',
	eleFamilySize	: '#addFamilySize',
	eleEarningMembers: '#addEarningMembers',
	eleCenter		 : '#addCenter',
	
};
var editModal ={
		form	 : '#editLearnerForm',
		modal	 : '#mdlEditLearner',
		eleLearner: '#editLearner',
		eleLearnerId: '#learnerId',
		eleFormNo		: '#editFormNumber',
		eleAge			: '#editLearnerAge',
		eleGender		: '#editGender:checked',
		eleAddress		: '#editLearnerAddress',
		eleEyeTestResult: '#editEyeTestResult',
		elePreTestScore	: '#editPreTestScore',
		elePostTestScore: '#editPostTestScore',
		eleOccupation	: '#editOccupation',
		eleMaritalStatus: '#editMaritalStatus',
		eleSpouseDetails: "#editSpouseDetails",
		eleSpouseName	: '#editSpouseName',
		eleSpouseAge	: '#editSpouseAge',
		eleSpouseIncome	: '#editSpouseIncome',
		eleSpouseLocation: '#editSpouseWorkLocation',
		eleFamilySize	: '#editFamilySize',
		eleEarningMembers: '#editEarningMembers',
		eleCenter		 : '#editCenter',
};
var deleteModal ={
		modal	: '#mdlDeleteLearner',
		confirm : '#deleteLearnerButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var learnerIdForDelete;
var learner={
		name:'',
		learnerId:0,

		init:function(){
			$("#tableDiv").hide();
			$(pageContextElements.eleAddButton).on('click',learner.initAddModal);
			$(deleteModal.confirm).on('click',learner.deleteFunction);
			$(addModal.eleMaritalStatus).on('change',learner.showHideSpouseDetails);
		},
		deleteFunction : function(){
			learner.fnDelete(learnerIdForDelete);
		},
		initAddModal :function(){
			learner.resetForm(addModal.form);
			$(addModal.modal).modal("show");
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		showHideSpouseDetails : function(){
			if($(addModal.eleMaritalStatus).find(":selected").text() == "Unmarried")
				{
				$(addModal.eleSpouseDetails).find("input").attr('disabled',true);
				$(addModal.eleSpouseDetails).find("select").attr('disabled',true);
				$(addModal.eleSpouseName).val("");
				$(addModal.eleSpouseAge).val("");
				$(addModal.eleSpouseIncome).val("");
				$(addModal.eleSpouseLocation).val("");
				}
			else{
				$(addModal.eleSpouseDetails).find("input").removeAttr('disabled');
				$(addModal.eleSpouseDetails).find("select").removeAttr('disabled');
			}
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					  "name" 	:$(addModal.eleLearner).val(),
					  "formNo"	: $(addModal.eleFormNo).val(),
					  "age"		: $(addModal.eleAge).val(),
					  "gender"	: $(addModal.eleGender).val(),
					  "address"	: $(addModal.eleAddress).val(),
					  "eyeTestResult"	: $(addModal.eleEyeTestResult).val(),
					  "preTestScore"	: $(addModal.elePreTestScore).val(),
					  "postTestScore"	: $(addModal.elePostTestScore).val(),
					  "occupationId"	: $(addModal.eleOccupation).val(),
					  "maritalStatus"	: $(addModal.eleMaritalStatus).find(":selected").text(),
					  "spouseName"		: $(addModal.eleSpouseName).val(),
					  "spouseAge"		: $(addModal.eleSpouseAge).val(),
					  "incomeId"		: $(addModal.eleSpouseIncome).val(),
					  "spouseWorkLocation": $(addModal.eleSpouseLocation).val(),
					  "familySize"		: $(addModal.eleFamilySize).val(),
					  "noOfEarningMembers": $(addModal.eleEarningMembers).val(),
					  "centerId"		: $(addModal.eleCenter).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("learner", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == "addLearnerSaveButton"){
						$(addModal.modal).modal("hide");
					}
					if(submitBtnId == "addLearnerSaveMoreButton"){
						$(addModal.modal).modal("show");
					}
					learner.resetForm(addModal.form);
					learner.refreshTable(pageContextElements.table);

					AJS.flag({
						type  : "success",
						title : appMessgaes.saved,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(learnerId){
			learner.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("learner/"+learnerId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					  $(editModal.eleLearner).val(editAjaxCall.name);
					  $(editModal.eleLearner).val(editAjaxCall.photoUrl);
					  $(editModal.eleLearnerId).val(editAjaxCall.learnerId);
					  $(editModal.eleFormNo).val(editAjaxCall.formNo);
					  $(editModal.eleAge).val(editAjaxCall.age);
					  $(editModal.eleGender).val(editAjaxCall.gender);
					  $(editModal.eleAddress).val(editAjaxCall.address);
					  $(editModal.eleEyeTestResult).val(editAjaxCall.eyeTestResult);
					  $(editModal.elePreTestScore).val(editAjaxCall.preTestScore);
					  $(editModal.elePostTestScore).val(editAjaxCall.postTestScore);
					  $(editModal.eleOccupation).val(editAjaxCall.occupationId);
					  $(editModal.eleMaritalStatus).val(editAjaxCall.maritalStatus);
					  $(editModal.eleSpouseName).val(editAjaxCall.spouseName);
					  $(editModal.eleSpouseAge).val(editAjaxCall.spouseAge);
					  $(editModal.eleSpouseIncome).val(editAjaxCall.incomeId);
					  $(editModal.eleSpouseLocation).val(editAjaxCall.spouseWorkLocation);
					  $(editModal.eleFamilySize).val(editAjaxCall.familySize);
					  $(editModal.eleEarningMembers).val(editAjaxCall.noOfEarningMembers);
					  $(editModal.eleCenter).val(editAjaxCall.centerId);
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					  "name"			: $(editModal.eleLearner).val(),
					  "learnerId"		: $(editModal.eleLearnerId).val(),
					  "formNo"			: $(editModal.eleFormNo).val(),
					  "age"				: $(editModal.eleAge).val(),
					  "gender"			: $(editModal.eleGender).val(),
					  "address"			: $(editModal.eleAddress).val(),
					  "eyeTestResult"	: $(editModal.eleEyeTestResult).val(),
					  "preTestScore"	: $(editModal.elePreTestScore).val(),
					  "postTestScore"	: $(editModal.elePostTestScore).val(),
					  "occupationId"	: $(editModal.eleOccupation).val(),
					  "maritalStatus"	: $(editModal.eleMaritalStatus).find(":selected").text(),
					  "spouseName"		: $(editModal.eleSpouseName).val(),
					  "spouseAge"		: $(editModal.eleSpouseAge).val(),
					  "incomeId"		: $(editModal.eleSpouseIncome).val(),
					  "spouseWorkLocation": $(editModal.eleSpouseLocation).val(),
					  "familySize"		: $(editModal.eleFamilySize).val(),
					  "noOfEarningMembers": $(editModal.eleEarningMembers).val(),
					  "centerId"		: $(editModal.eleCenter).val(),

			}

			var updateAjaxCall = shiksha.invokeAjax("learner", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					learner.resetForm(editModal.form);
					learner.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: appMessgaes.updated,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: "Error..!",
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: "Oops..!",
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(learnerId){
			$(deleteModal.modal).modal("show");
			learnerIdForDelete=learnerId;
		},
		fnDelete :function(learnerId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("learner/"+learnerId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					learner.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : appMessgaes.deleted,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : "Error..!",
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : "Oops..!",
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					name: {
						required : true,
						noSpace	 :true,
						minlength : 2,
						maxlength : 50,
					},
					formNo: {
						required : true,
						noSpace	 :true,
						minlength : 2,
						maxlength : 50,
					},
					age: {
						
					},
					gender: {
						
					},
					address: {
						
					},
					eyeTestResult:{
						
					},
					preTestScore: {
						
					},
					postTestScore:{
						
					},
					occupation: {
						
					},
					maritalStatus: {
						
					},
					spouseName: {
						
					},
					spouseAge: {
						
					},
					spouseWorkLocation: {
						
					},
					familySize: {
						
					},
					earningMembers: {
						
					},
					center: {
						
					},
				},

				messages: {
					name: {
						required : "Please enter Learner Name",
						noSpace  :"Please enter valid Learner Name",
						minlength : "The Learner name must be more than 2 and less than 50 characters long",
						maxlength : "The Learner name must be more than 2 and less than 50 characters long",
					},

					formNo: {
						
					},
					age: {
						
					},
					gender: {
						
					},
					address: {
						
					},
					eyeTestResult:{
						
					},
					preTestScore: {
						
					},
					postTestScore:{
						
					},
					occupation: {
						
					},
					maritalStatus: {
						
					},
					spouseName: {
						
					},
					spouseAge: {
						
					},
					spouseWorkLocation: {
						
					},
					familySize: {
						
					},
					earningMembers: {
						
					},
					center: {
						
					},
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						learner.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						learner.fnUpdate();
					}
				}
			});
		},
		learnerActionFormater: function(value, row, index) {
			console.log(value, row, index);
			var actionBtns = '<a rel="tooltip" data-tooltip="true" data-original-title="Edit" class="tooltip tooltip-top" onclick="learner.initEdit(\''+row.learnerId+'\')"> <span class="tooltiptext">Edit</span><span class="glyphicon glyphicon-edit font-size12"></span> </a>'
			+ '<a style="margin-left:10px;" class="tooltip tooltip-top" onclick="learner.initDelete(\''+row.learnerId+'\')"><span class="tooltiptext">Delete</span><span class="glyphicon glyphicon-trash font-size12"></span></a>';

			return actionBtns;
		},
		learnerNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	learner.formValidate(addModal.form);
	learner.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	learner.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar			: "#toolbarLearner",
		url 			: "learner",
		method 			: "get",
		toolbarAlign 	: "right",
		search			: true,
		sidePagination	: "sever",
		showToggle		: false,
		showColumns		: true,
		pagination		: true,
		searchAlign		: 'left',
		pageSize		: 10,
		clickToSelect	: false,
		pageList		: [10, 15, 25, 50, 100]
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});