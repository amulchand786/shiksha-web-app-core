var selectLocations = {
	locationBar		: "#selectLocationBar",
	villageDetails	: "#selectVillageDetatils",
	cityDetails		: "#selectCityDetatils",
	table			: "#tableDiv",
	urbanDetails	: "#notForUrban",
};
var locationDetails = {
	state			: "#statesForZone",
	zone			: "#zones",
	district		: "#districts",
	tehsil			: "#tehsils",
	block			: "#blocks",
	nyayaPanchayat  : "#nyayPanchayats",
	gramPanchayat	: "#gramPanchayat",
	revenueVillage	: "#revenueVillage",
	
};
var learnerLocation={
		init:function(){
			$(selectLocations.locationBar).hide();
			$(selectLocations.villageDetails).hide();
			$(selectLocations.cityDetails).hide();
			$(selectLocations.table).hide();
			setOptionsForMultipleSelect(locationDetails.state);
			setOptionsForMultipleSelect(locationDetails.zone);
			setOptionsForMultipleSelect(locationDetails.district);
			setOptionsForMultipleSelect(locationDetails.tehsil);
			setOptionsForMultipleSelect(locationDetails.block);
			setOptionsForMultipleSelect(locationDetails.nyayaPanchayat);
			setOptionsForMultipleSelect(locationDetails.gramPanchayat);
			setOptionsForMultipleSelect(locationDetails.revenueVillage);
			$("#divCenterLocType [name='locType']").on('change',learnerLocation.showHideLocations);
		},	
		showHideLocations:function(){
			if($(this).val() == "Rural")
			{
				$(selectLocations.cityDetails).hide();
				$(selectLocations.locationBar).show();
				$(selectLocations.villageDetails).show();
				$(selectLocations.urbanDetails).show();
			}
			else{
				$(selectLocations.villageDetails).hide();
				$(selectLocations.locationBar).show();
				$(selectLocations.cityDetails).show();
				$(selectLocations.urbanDetails).hide();
			}
			
		},
};
$( document ).ready(function() {

	learnerLocation.init();
	
});