var pageContextElements = {
		divFilter   			: '#divFilter',
		eleLocType  			: '#divCenterLocType input:radio[name=locType]',
		filter					: '#locColspFilter',
		panelLink				: '#locationPnlLink',
		form					: '#listLearnerForm',
		eleCollapseFilter		: '#collapseFilter',
		eleToggleCollapse		: '[data-toggle="collapse"]',
		clearBtn				: '#btnResetLocation',
		levelDiv				: '#divSelectLevel',
		batchDiv				: '#divSelectBatch',
		academicYearDiv 		: '#divSelectAcademicYear',
		yearDiv                 :'#divSelectYear',
		createFromDiv			: '#divCreateDateFrom',
		createToDiv				: '#divCreateDateTo',
		createDateFrom			: '#createDateFrom',
		createDateTo			: '#createDateTo',
		addLearnerDiv  			: '#addLearnerDiv',
		addLearnerDetailsBtn	: '#addLearnerDetails',
		viewLearnerDiv  		: '#viewLearnerDiv',
		viewLearnerDetailsBtn	: '#viewLearnerDetails',
		tableDiv				: '#rowDiv2',
		table					: '#tableDiv',
		btnsRow					: '#btnsRow',
		bootstrapTable		 	: '#learnerTable',
		updateBtn				: '#updateRollNumberBtn',
		datesForm				: '#locationForm',
		addIdentificationNumberDiv 	:'#addIdentificationNumberDiv',
		editIdentificationNumberDiv 	:'#editIdentificationNumberDiv',
		lvlAddIdentificationNumber  :"#lvlAddIdentificationNumber",
		lvlEditIdentificationNumber  :"#lvlEditIdentificationNumber",
		spanAddIdentificationNumber :"#spanAddIdentificationNumber",
		spanEditIdentificationNumber :"#spanEditIdentificationNumber",

};
var addLearner ={
		modal 		: '#mdlAddLearner',	
		form		: '#addLearnerForm',
		formNo		: '#addFormNo',
		saveMoreButton	: 'addLearnerSaveMoreButton',
		saveButton		: 'addLearnerSaveButton',
		name		: '#addName',
		gender		: '#addGender',
		age			: '#addAge',
		instructor	: '#addInstructor',
		eyeTestResult	: '#addEyeTestResult',
		preTestScore	: '#addPreTestScore',
		occupation		: '#addOccupation',
		familySize		: '#addFamilySize',
		maritalStatus	: '#addMaritalStatus',
		spouseDiv		: '#spouseDetails',
		spouseName		: '#addSpouseName',
		spouseAge		: '#addSpouseAge',
		spouseIncome	: '#addSpouseIncome',
		spouseWorkLoc	: '#addSpouseWorkLocation',
		earningMembers	: '#addEarningMembers',
		rollNumber      : '#addLearnerForm #rollNumber',
		memberDetails	: '#earningMembersDetails',
		memberName		: '#addMemberName',
		memberAge		: '#addMemberAge',
		memberGender	: '#addMemberGender',
		memberRelation	: '#addMemberRelationship',
		memberOtherRela	: '#addMemberRelationshipOther',
		memberEmpStatus	: '#addEmpStatus',
		memberIncome	: '#addMemberIncome',
		address			: '#addLearnerAddress',
		learnerFamilyEarningMemberDetails : [],
		bloodGroup                        :'#addBloodGroup',
		identificationNumber 			  :'#addIdentificationNumber',
		identificationType                :'#addIdentificationType',
};
var editLearner ={
		modal : '#mdlEditLearner',	
		form		: '#editLearnerForm',
		formNo		: '#editFormNo',
		id			: '#learnerId',
		name		: '#editName',
		gender		: '#editGender',
		age			: '#editAge',
		instructor	: '#editInstructor',
		eyeTestResult	: '#editEyeTestResult',
		preTestScore	: '#editPreTestScore',
		occupation		: '#editOccupation',
		familySize		: '#editFamilySize',
		maritalStatus	: '#editMaritalStatus',
		rollNumber      : '#editLearnerForm #rollNumber',
		spouseDiv		: '#spouseDetailsInEdit',
		spouseName		: '#editSpouseName',
		spouseAge		: '#editSpouseAge',
		spouseIncome	: '#editSpouseIncome',
		spouseWorkLoc	: '#editSpouseWorkLocation',
		earningMembers	: '#editEarningMembers',
		memberDetails	: '#earningMembersDetailsInEdit',
		memberName		: '#editMemberName',
		memberAge		: '#editMemberAge',
		memberGender	: '#editMemberGender',
		memberRelation	: '#editMemberRelationship',
		memberOtherRela	: '#editMemberRelationshipOther',
		memberEmpStatus	: '#editEmpStatus',
		memberIncome	: '#editMemberIncome',
		address			: '#editLearnerAddress',
		learnerFamilyEarningMemberDetails : [],
		bloodGroup                        :'#editBloodGroup',
		identificationNumber 			  :'#editIdentificationNumber',
		identificationType                :'#editIdentificationType',
};
var deleteLearner ={
		modal : '#mdlDeleteLearner',
		confirm: '#deleteLearnerButton',
		id	:'',
};
var selectBox ={
		center		: '#selectBox_centersByLoc',
		level		: '#selectBox_Level',
		batch		: '#selectBox_Batch',
		academicYear: '#selectBox_AcademicYear',
		year		: '#selectBox_year'
};
var reset = {
		modal : '#mdlResetLoc',
		confirm : '#btnResetLocYes',
};
var updateElements = {
		table 		: '#bulkRollNoUpdateTable',
		centerId	: '',
		levelId		: '',
		batchId		: '',
		groupId		: '',
		ayCycleId	: '',
		allLearners	: '',
};
var submitActor = null;
var $submitActors = $(addLearner.form).find('button[type=submit]');
var learnerData ={};
var rollsList=[];
var locationPanel = {
		city 	: "#divSelectCity",
		block 	: "#divSelectBlock",
		NP 		: "#divSelectNP",
		GP 		: "#divSelectGP",
		RV 		: "#divSelectRV",
		village : "#divSelectVillage",
		school 	: "#divSelectSchool",
		schoolLocationType  : "#selectBox_schoolLocationType",
		elementSchool 		: "#selectBox_schoolsByLoc",
		level 				: "#selectBox_Level",
		resetLocDivCity 	: "#divResetLocCity",
		resetLocDivVillage	: "#divResetLocVillage",
};
var locationElements={
		allInputEleNamesOfCityFilter :['state','zone','district','tehsil','city'],
		allInputEleNamesOfVillageFilter :['state','zone','district','tehsil','block','nyayPanchayat','panchayat','revenueVillage','village'],
		allInputIgnoreEleNamesOfCityFilter :['block','nyayPanchayat','panchayat','revenueVillage','village'],
		allLocationFilters :['state','zone','district','tehsil','block','city','nyayPanchayat','panchayat','revenueVillage','village'],
}
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(locationPanel.NP).insertAfter($(locationPanel.block));
		$(locationPanel.RV).insertAfter($(locationPanel.GP));
		$(locationPanel.school).insertAfter($(locationPanel.village));
	} else {
		$(locationPanel.school).insertAfter($(locationPanel.city));
	}
};
var resetButtonObj;
var elementIdMap;
var $table;
var locationUtility={
		fnInitLocation :function(){
			$(pageContextElements.divFilter).css('display','none');
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");

		},
		resetLocation :function(obj){	
			$(reset.modal).modal().show();
			resetButtonObj =obj;
		},
		//remove and reinitialize smart filter
		reinitializeSmartFilter : function(eleMap){
			var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
				$(eleMap[ele_key]).find("option:selected").prop('selected', false);
				$(eleMap[ele_key]).find("option[value]").remove();
				$(eleMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(eleMap[ele_key]);
			});	
		},

		doResetLocation : function() {
			$(selectBox.level).multiselect('destroy').multiselect('rebuild');
			$(selectBox.batch).multiselect('destroy').multiselect('rebuild');
			$(selectBox.academicYear).multiselect('destroy').multiselect('rebuild');
			$(selectBox.year).multiselect('destroy').multiselect('rebuild');
			$(pageContextElements.levelDiv).hide();
			$(pageContextElements.batchDiv).hide();
			$(pageContextElements.yearDiv).hide();
			learner.hideAcademicCycleDiv();
			enableMultiSelect($(selectBox.batch));
			enableMultiSelect($(selectBox.level));


			deleteAllRow("studentTable");
			if ($(resetButtonObj).parents("form").attr("id") == 'listLearnerForm') {
				locationUtility.reinitializeSmartFilter(elementIdMap);
				var aLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
				var aLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');
				locationUtility.displaySmartFilters(aLocTypeCode,aLLevelName);

			}
			fnCollapseMultiselect();
		},
		resetFormValidatonForLocFilters : function(formId,elementsName){
			$.each(elementsName,function(index,value){
				$("#"+value).val('');
				$(formId).data('formValidation').updateStatus(value, 'IGNORED');
			});

		},

		//displaying robo-filters
		displaySmartFilters : function(lCode,lvlName){

			elementIdMap = {
					"State" 	: '#statesForZone',
					"Zone" 		: '#selectBox_zonesByState',
					"District" 	: '#selectBox_districtsByZone',
					"Tehsil" 	: '#selectBox_tehsilsByDistrict',
					"Block" 	: '#selectBox_blocksByTehsil',
					"City" 		: '#selectBox_cityByBlock',
					"Nyaya Panchayat" 	: '#selectBox_npByBlock',
					"Gram Panchayat" 	: '#selectBox_gpByNp',
					"Revenue Village" 	: '#selectBox_revenueVillagebyGp',
					"Village" 	: '#selectBox_villagebyRv',
					"Center" 	: '#selectBox_centersByLoc'
			};

			displayLocationsOfSurvey(lCode,lvlName,$('#divFilter'));

			locationUtility.reinitializeSmartFilter(elementIdMap);

			var lLocTypeCode =$('#divCenterLocType input:radio[name=locType]:checked').data('code');
			var lLLevelName =$('#divCenterLocType input:radio[name=locType]:checked').data('levelname');

			displayLocationsOfSurvey(lCode,lLLevelName,$('#divFilter'));

			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(
					function(ind, ele_key) {
						$(elementIdMap[ele_key]).find("option:selected").prop(
								'selected', false);
						$(elementIdMap[ele_key]).multiselect('destroy');
						enableMultiSelect(elementIdMap[ele_key]);
					});
			setTimeout(function() {
				$(".outer-loader").hide();
				$('#rowDiv1,#btnsRow,#divFilter').show();
			}, 100);
			var ele_keys = Object.keys(elementIdMap);
			$(ele_keys).each(function(ind, ele_key){
				$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');		
				enableMultiSelect(elementIdMap[ele_key]);
			});

			$('#rowDiv1,#divFilter').show();

			spinner.hideSpinner();
		},

};

var learner = {

		fnInit:function(){
			$(reset.confirm).on('click',locationUtility.doResetLocation);
			$(selectBox.center).on('change',learner.showLevels);
			$(selectBox.level).on('change',learner.showBatches);
			$(selectBox.batch).on('change',learner.showYears);
			$(selectBox.year).on('change',learner.showAcademicCycles);
			$(selectBox.academicYear).on('change',learner.hideTableDiv);
			$(document).on("change",addLearner.memberEmpStatus,this.changeMemberIncome);
			$(document).on("change",editLearner.memberEmpStatus,this.changeMemberIncome);

			/*$(pageContextElements.viewLearnerDetailsBtn).on('click',this.showLearnerDetails);*/
			$(pageContextElements.addLearnerDetailsBtn).on('click',this.initAddModal);
			$(pageContextElements.table).hide();
			$(addLearner.maritalStatus).on('change',this.checkMaritalStatus);
			$(addLearner.identificationType).on('change',this.showAddIdentificationNumber);
			$(editLearner.identificationType).on('change',this.showEditIdentificationNumber);

			$(editLearner.maritalStatus).on('change',this.checkMaritalStatusInEdit);
			$(addLearner.earningMembers).on('input',this.checkEarningMembers);
			$(addLearner.memberRelation).on('change',this.checkRelationship);
			$(deleteLearner.confirm).on('click',this.fnDelete);
			$(pageContextElements.eleLocType).change(function() {				
				$(pageContextElements.filter).removeClass('in');
				$(pageContextElements.panelLink).text(centerColumn.selectLocation);
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
				$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");				
				$(pageContextElements.levelDiv).hide();
				$(pageContextElements.createDateFrom).val("");
				$(pageContextElements.batchDiv).hide();
				$(pageContextElements.yearDiv).hide();
				learner.hideAcademicCycleDiv();
				var typeCode =$(this).data('code');

				if(typeCode.toLowerCase() =='U'.toLowerCase()){
					showDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					hideDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("U");
					fnCollapseMultiselect();
				}else{
					hideDiv($(locationPanel.city),$(locationPanel.resetLocDivCity));
					showDiv($(locationPanel.block),$(locationPanel.NP),$(locationPanel.GP),$(locationPanel.RV),$(locationPanel.village));
					locationUtility.displaySmartFilters($(this).data('code'),$(this).data('levelname'));
					positionFilterDiv("R");
					fnCollapseMultiselect();

				}
				$(pageContextElements.eleCollapseFilter).css('display','');
			});
		},
		changeMemberIncome :function(){
			$(this).parents(".row").find('[name="memberIncome"]').val("");
			if($(this).val() == "true"){
				$(this).parents(".row").find('#memberIncomeDiv').show();
			} else {
				$(this).parents(".row").find('#memberIncomeDiv').hide();
			}
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		showLevels : function(){
			shikshaPlus.fnGetLevelsByCenter(selectBox.level,0,$(selectBox.center).val());
			setOptionsForMultipleSelect(selectBox.level);
			$(selectBox.level).multiselect("rebuild");
			$(pageContextElements.levelDiv).show();
			learner.hideAcademicCycleDiv();
		},
		showBatches : function(){
			shikshaPlus.fnGetBatchesByCenterAndLevel(selectBox.batch,0,$(selectBox.center).val(),$(selectBox.level).val());
			setOptionsForMultipleSelect(selectBox.batch);
			$(selectBox.batch).multiselect("rebuild");
			$(pageContextElements.batchDiv).show();
			$(pageContextElements.yearDiv).hide();
			learner.hideAcademicCycleDiv();
		},
		hideAcademicCycleDiv:function(){
			$(pageContextElements.academicYearDiv).hide();
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.table).hide();
			$(pageContextElements.addLearnerDiv).hide();
			$(pageContextElements.viewLearnerDiv).hide()	
			$(pageContextElements.createDateFrom).val("");
			$(pageContextElements.createToDiv).hide();
			$(pageContextElements.createFromDiv).hide();
		},
		hideTableDiv:function(){
			var isCurrent=$(selectBox.academicYear).find('option:selected').data('current');
			if(isCurrent){	
				$(pageContextElements.addLearnerDiv).show();
			}else{
				$(pageContextElements.addLearnerDiv).hide();
			}
			$(pageContextElements.createDateFrom).val("");
			$(pageContextElements.tableDiv).hide();
			$(pageContextElements.table).hide();
			$(pageContextElements.createToDiv).hide();
			$(pageContextElements.createFromDiv).show();
		},
		showYears : function(){
			shikshaPlus.fnGetYears(selectBox.year,0);
			setOptionsForMultipleSelect(selectBox.year);
			$(selectBox.year).multiselect("rebuild");
			$(pageContextElements.yearDiv).show();

		},
		showAddIdentificationNumber:function(){
			var identificationValue=$(addLearner.identificationType).find('option:selected').val();
			$(addLearner.identificationNumber).val("");
			
			if(identificationValue==""){
				$(addLearner.identificationNumber).prop("disabled",true);
				$(pageContextElements.spanAddIdentificationNumber).hide();

			}else{
				$(pageContextElements.addIdentificationNumberDiv).show();
				$(pageContextElements.spanAddIdentificationNumber).show();
				$(addLearner.identificationNumber).prop("disabled",false);

			}
			
			learner.updateIdentificationNumberLabel(identificationValue,pageContextElements.lvlAddIdentificationNumber);
			$(addLearner.identificationNumber).valid();
		},
		showEditIdentificationNumber:function(){
			$(editLearner.identificationNumber).val("");
			var identificationValue=$(editLearner.identificationType).find('option:selected').val();
			
			if(identificationValue==""){
				$(editLearner.identificationNumber).prop("disabled",true);
				$(pageContextElements.spanEditIdentificationNumber).hide();
				
			}else{
				$(pageContextElements.spanEditIdentificationNumber).show();
				$(pageContextElements.editIdentificationNumberDiv).show();
				$(editLearner.identificationNumber).prop("disabled",false);
			}
			learner.updateIdentificationNumberLabel(identificationValue,pageContextElements.lvlEditIdentificationNumber);
			$(editLearner.identificationNumber).valid();
		},
		checkIdentificationNumber:function(identificationValue,elementId){
			if(identificationValue!=""){
				return $(elementId).val().trim().length <= 0;
			}else{
				return false;
			}
		},
		updateIdentificationNumberLabel:function(identificationTyp,labelId){
			switch (identificationTyp) { 
			case 'DL': 
				$(labelId).text(centerColumn.dlNumber);
				break;
			case 'PAN': 
				$(labelId).text(centerColumn.panNumber);
				break;
			case 'AADHAAR': 
				$(labelId).text(centerColumn.aadhaarNumber);
				break;		
			default:
				$(labelId).text(centerColumn.identificationNumber);

			}
		},
		showAcademicCycles : function(){
			var year=$(selectBox.year).val();
			shikshaPlus.fnGetAcademicCyclesByYear(selectBox.academicYear,0,year);
			if($(selectBox.academicYear).find('option').length>1){
				var isCurrent=$(selectBox.academicYear).find('option:selected').data('current');
				setOptionsForMultipleSelect(selectBox.academicYear);
				$(selectBox.academicYear).multiselect("rebuild");
				$(pageContextElements.academicYearDiv).show();
				$(pageContextElements.createFromDiv).show();
				$(pageContextElements.createDateFrom).val("");
				if(isCurrent){
					$(pageContextElements.addLearnerDiv).show();
				}else{
					$(pageContextElements.addLearnerDiv).hide();
				}
				$(pageContextElements.viewLearnerDiv).show();
				learner.refreshTable();
			}else{
				learner.hideAcademicCycleDiv();
				learner.refreshTable();
			}
		},
		data:{
			center		 : '',
			level 		 : '',
			batch		 : '',
			academicCycle: ''
		},
		showLearnerDetails :function(){

			learner.data={
					center		 : {"id":$(selectBox.center).val()},
					level 		 : {"levelId": $(selectBox.level).val()},
					batch		 : {"batchId":$(selectBox.batch).val()},
					academicCycle: {"academicCycleId":$(selectBox.academicYear).val()},
					createdDate	 : $(pageContextElements.createDateFrom).val(),
					modifiedDate : $(pageContextElements.createDateTo).val()
			}
			var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=rollNumber",learner.data, "POST");
			if(ajaxCall.response == 'shiksha-200'){
				$(pageContextElements.bootstrapTable).bootstrapTable('load',ajaxCall.learners);			

			}else{
				AJS.flag({
					type  : "Error",
					title :  appMessgaes.error,
					body  : ajaxCall.responseMessage,
					close : 'auto'
				});
				//$(pageContextElements.table).hide();
				$(pageContextElements.bootstrapTable).bootstrapTable('load',[]);			

			}
			$(pageContextElements.table).show();
			$(pageContextElements.btnsRow).show();
			spinner.hideSpinner();

		},
		learnerActionFormater: function(value, row) {
			rollsList.push(row.rollNumber);
			var action = ""; 
			var currentAc=$(selectBox.academicYear).find('option:selected').data('current');
			if(currentAc){
				if(permission["edit"] || permission["delete"]){
					action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
					action+='<li><a onclick="learner.initView(\''+ row.learnerId+ '\')" href="javascript:void(0)">'+appMessgaes.view+'</a></li>';
					if(permission["edit"])
						action+='<li><a onclick="learner.initEdit(\''+ row.learnerId+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
					if(permission["delete"])
						action+='<li><a onclick="learner.initDelete(\''+ row.learnerId+ '\',\''+ row.name+ '\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
					action+='</ul>';
				}
			}
			return action;
		},
		checkMaritalStatus: function(){
			if($(addLearner.maritalStatus).val() == 'MARRIED'){
				$(addLearner.spouseDiv).show();

			}
			else{
				$(addLearner.spouseIncome).val('');
				$(addLearner.spouseDiv).hide();

			}
		},
		checkMaritalStatusInEdit: function(){
			if($(editLearner.maritalStatus).val() == 'MARRIED'){
				$(editLearner.spouseDiv).show();
			}
			else{
				$(editLearner.spouseIncome).val('');
				$(editLearner.spouseDiv).hide();
				$("#spouseDetails").hide();
			}
		},
		checkEarningMembers : function(){
			$(addLearner.memberDetails).empty();
			if($(addLearner.earningMembers).val() > 0){				
				for(var i = 0;i < $(addLearner.earningMembers).val();i++){
					var earningMembers = $(".member-details-template").clone();
					$(earningMembers).find(".member-number").text(messages.member+": "+(i+1))
					$(addLearner.memberDetails).append($(earningMembers).show().removeClass("member-details-template").addClass("member"));
					$(earningMembers).find('input[class="add-gender"]').attr('name','memberGender'+i);
					$(earningMembers).find('input[class="add-emp-status"]').attr('name','memberEmpStatus'+i);
				}
				$(addLearner.memberDetails).show();
			}
			else{
				$(addLearner.memberDetails).hide();
			}
		},
		initAddModal : function(isSave){
			$(addLearner.spouseDiv).hide();
			$(addLearner.memberDetails).hide();
			$(addLearner.memberOtherRela).hide();
			shikshaPlus.fnGetOccupations($(addLearner.occupation));
			setOptionsForMultipleSelect(addLearner.occupation);
			shikshaPlus.fnGetIncomes($(addLearner.spouseIncome));
			setOptionsForMultipleSelect(addLearner.spouseIncome);
			setOptionsForMultipleSelect(addLearner.maritalStatus);
			setOptionsForMultipleSelect(addLearner.bloodGroup);
			setOptionsForMultipleSelect(addLearner.identificationType);
			$(addLearner.identificationNumber).prop("disabled",true);
			if(isSave==""){
				fnInitSaveAndAddNewList();
			}
			learner.resetForm(addLearner.form);
			$(addLearner.bloodGroup).multiselect('rebuild');
			$(addLearner.identificationType).multiselect('rebuild');
			$(addLearner.occupation).multiselect('rebuild');
			$(addLearner.spouseIncome).multiselect('rebuild');
			$(addLearner.maritalStatus).multiselect('rebuild');
			$(pageContextElements.spanAddIdentificationNumber).hide();
			learner.updateIdentificationNumberLabel("",pageContextElements.lvlAddIdentificationNumber);
			$(addLearner.modal).modal('show');

		},
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			addLearner.learnerFamilyEarningMemberDetails=[];
			var error = false;
			$.each($('.member'),function(){
				member={};
				member.memberName=$(this).find(addLearner.memberName).val();
				member.memberAge=$(this).find(addLearner.memberAge).val();
				member.gender=$(this).find(addLearner.memberGender+":checked").val();
				member.relationshipWithLearner=$(this).find(addLearner.memberRelation).val();
				member.memberEmploymentStatus=$(this).find(addLearner.memberEmpStatus+":checked").val();
				member.memberIncome=$(this).find(addLearner.memberIncome).val();
				if(!member.memberName || member.memberAge <= 0 || !member.relationshipWithLearner || (member.memberEmploymentStatus == "true" && !member.memberIncome)){
					error = true;
					return false;
				}
				addLearner.learnerFamilyEarningMemberDetails.push(member);
			});
			if(error){
				AJS.flag({
					type  : "error",
					title : "Error..!",
					body  : "Enter all members details",
					close : 'auto'
				});
				spinner.hideSpinner();
				return false;
			}
			var ajaxData={
					name	: $(addLearner.name).val(),
					formNo	: $(addLearner.formNo).val(),
					age		: $(addLearner.age).val(),
					gender	: $(addLearner.gender+":checked").val(),
					instructor : $(addLearner.instructor).val(),
					address	: $(addLearner.address).val(),
					eyeTestResult	: $(addLearner.eyeTestResult).val(),
					preTestScore	: $(addLearner.preTestScore).val(),
					maritalStatus	: $(addLearner.maritalStatus).val(),
					familySize				: $(addLearner.familySize).val(),
					numberOfEarningMembers	: $(addLearner.earningMembers).val(),
					rollNumber              :$(addLearner.rollNumber).val(),
					occupation		:{'occupationId':$(addLearner.occupation).val()},
					center		 : {"id":$(selectBox.center).val()},
					level 		 : {"levelId": $(selectBox.level).val()},
					batch		 : {"batchId":$(selectBox.batch).val()},
					academicCycle: {"academicCycleId":$(selectBox.academicYear).val()},
					learnerFamilyEarningMemberDetailsMappers :addLearner.learnerFamilyEarningMemberDetails,
					identificationType        :$(addLearner.identificationType).val(),
					identificationNumber      :$(addLearner.identificationNumber).val(),
					bloodGroup                :$(addLearner.bloodGroup).val(),


			}
			if($(addLearner.maritalStatus).val() == 'MARRIED'){
				ajaxData['spouseAge']=$(addLearner.spouseAge).val();
				ajaxData['spouseName']=$(addLearner.spouseName).val();
				ajaxData['spouseWorkLocation']=$(addLearner.spouseWorkLoc).val();
				ajaxData['income']={'incomeId':$(addLearner.spouseIncome).val()};
			}
			var addAjaxCall = shiksha.invokeAjax("learner", ajaxData, "POST");
			spinner.hideSpinner();
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addLearner.saveButton){ 
						$(addLearner.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : centerColumn.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addLearner.saveMoreButton){
						learner.initAddModal(true);
						//$(addLearner.modal).modal("show");
						$(addLearner.modal).find("#divSaveAndAddNewMessage").show();
						fnDisplaySaveAndAddNewElementAui(addLearner.modal,addAjaxCall.name);
					}
					learner.resetForm(addLearner.form);
					var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=rollNumber",learner.data, "POST");
					spinner.hideSpinner();
					$(pageContextElements.bootstrapTable).bootstrapTable('load',ajaxCall.learners);

				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
		},

		initEdit : function(learnerId){
			$(editLearner.spouseDiv).hide();
			$(editLearner.memberDetails).hide();
			$(editLearner.memberOtherRela).hide();
			learner.resetForm(editLearner.form);
			shikshaPlus.fnGetOccupations($(editLearner.occupation));
			setOptionsForMultipleSelect(editLearner.occupation);
			shikshaPlus.fnGetIncomes($(editLearner.spouseIncome));
			setOptionsForMultipleSelect(editLearner.spouseIncome);
			setOptionsForMultipleSelect(editLearner.maritalStatus);
			setOptionsForMultipleSelect(editLearner.bloodGroup);
			setOptionsForMultipleSelect(editLearner.identificationType);
			$(editLearner.bloodGroup).multiselect('rebuild');
			$(editLearner.identificationType).multiselect('rebuild');
			$(editLearner.occupation).multiselect('rebuild');
			$(editLearner.spouseIncome).multiselect('rebuild');
			$(editLearner.modal).modal('show');
			var editAjaxCall = shiksha.invokeAjax("learner/"+learnerId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall.response == "shiksha-200"){
				$(editLearner.id).val(editAjaxCall.learnerId);
				$(editLearner.name).val(editAjaxCall.name);
				$(editLearner.formNo).val(editAjaxCall.formNo);
				$(editLearner.age).val(editAjaxCall.age);
				$(editLearner.instructor).val(editAjaxCall.instructor);
				$('input[id=editGender][value="'+editAjaxCall.gender+'"]').prop('checked',true);
				$(editLearner.address).val(editAjaxCall.address);
				$(editLearner.eyeTestResult).val(editAjaxCall.eyeTestResult);
				$(editLearner.preTestScore).val(editAjaxCall.preTestScore);
				$(editLearner.maritalStatus).val(editAjaxCall.maritalStatus);

				$(editLearner.bloodGroup).val(editAjaxCall.bloodGroup);
				var zIdentityType=editAjaxCall.identificationType!=null?editAjaxCall.identificationType:"";
				$(editLearner.identificationType).val(zIdentityType);
				if(zIdentityType==""){
					$(editLearner.identificationNumber).prop("disabled",true);
				}else{
					$(editLearner.identificationNumber).prop("disabled",false);
				}

				learner.updateIdentificationNumberLabel(editAjaxCall.identificationType,pageContextElements.lvlEditIdentificationNumber);
				$(editLearner.identificationNumber).val(editAjaxCall.identificationNumber);
				$(editLearner.spouseAge).val(editAjaxCall.spouseAge);
				$(editLearner.spouseName).val(editAjaxCall.spouseName);
				$(editLearner.spouseWorkLoc).val(editAjaxCall.spouseWorkLocation);
				$(editLearner.spouseIncome).val(editAjaxCall.income.incomeId);
				$(editLearner.familySize).val(editAjaxCall.familySize);
				$(editLearner.rollNumber).val(editAjaxCall.rollNumber);

				$(editLearner.earningMembers).val(editAjaxCall.numberOfEarningMembers);//.prop('readonly','readonly');
				$(editLearner.occupation).val(editAjaxCall.occupation.occupationId);
				$(editLearner.occupation).multiselect('rebuild');
				$(editLearner.spouseIncome).multiselect('rebuild');
				$(editLearner.maritalStatus).multiselect('rebuild');
				$(editLearner.bloodGroup).multiselect('rebuild');
				$(editLearner.identificationType).multiselect('rebuild');
				if($(editLearner.maritalStatus).val() == 'MARRIED'){
					$(editLearner.spouseDiv).show();
				}
				$(editLearner.memberDetails).empty();
				if($(editLearner.earningMembers).val() > 0){	
					$(editLearner.memberDetails).show();
					$.each(editAjaxCall.learnerFamilyEarningMemberDetailsMappers,function(index,value){
						var earningMembers = $(".edit-member-details-template").clone();
						$(earningMembers).find(".member-number").text(messages.member+": "+(index+1))
						$(editLearner.memberDetails).append($(earningMembers).show().removeClass("edit-member-details-template").addClass("edit-member"));
						$(earningMembers).find('input[class="edit-gender"]').attr('name','memberGender'+index);
						$(earningMembers).find('input[class="edit-emp-status"]').attr('name','memberEmpStatus'+index);
						$(earningMembers).attr('data-mapperid',value.learnerFamilyEarningMemberDetailsMapperId);
						$(earningMembers).find(editLearner.memberName).val(value.memberName);
						$(earningMembers).find(editLearner.memberName).attr('mapperId',value.learnerFamilyEarningMemberDetailsMapperId);
						$(earningMembers).find(editLearner.memberAge).val(value.memberAge);
						$(earningMembers).find('input[id=editMemberGender][value="'+value.gender+'"]').prop('checked',true);
						$(earningMembers).find(editLearner.memberRelation).val(value.relationshipWithLearner);
						$(earningMembers).find('input[id=editEmpStatus][value="'+value.memberEmploymentStatus+'"]').prop('checked',true);
						if(value.memberEmploymentStatus){
							$(earningMembers).find("#memberIncomeDiv").show();
							$(earningMembers).find(editLearner.memberIncome).val(value.memberIncome);
						}
					})
				}
			}
		},
		initView : function(learnerId){
			var getAjaxCall = shiksha.invokeAjax("learner/"+learnerId+"/details",null, "GET");
			spinner.hideSpinner();
			if(getAjaxCall.response == "shiksha-200"){
				//console.log(getAjaxCall);
				$("#lrn-name").text(getAjaxCall.name!=null?getAjaxCall.name:"N/A");
				$("#lrn-form").text(getAjaxCall.formNo!=null?getAjaxCall.formNo:"N/A");
				$("#lrn-age").text(getAjaxCall.age!=null?getAjaxCall.age:"N/A");
				$("#lrn-gender").text(getAjaxCall.gender!=null?getAjaxCall.gender:"N/A");
				$("#lrn-refer").text(getAjaxCall.instructor!=null?getAjaxCall.instructor:"N/A");
				$("#lrn-occupation").text(getAjaxCall.occupation!=null?getAjaxCall.occupation.name:"N/A");
				$("#lrn-material").text(getAjaxCall.maritalStatus!=null?getAjaxCall.maritalStatus:"N/A");
				$("#lrn-eye").text(getAjaxCall.eyeTestResult!=null?getAjaxCall.eyeTestResult:"N/A");
				$("#lrn-score").text(getAjaxCall.preTestScore!=null?getAjaxCall.preTestScore:"N/A");
				$("#lrn-family").text(getAjaxCall.familySize!=null?getAjaxCall.familySize:"N/A");
				$("#lrn-earners").text(getAjaxCall.numberOfEarningMembers!=null?getAjaxCall.numberOfEarningMembers:"N/A");
				$("#lrn-roll").text(getAjaxCall.rollNumber!=null?getAjaxCall.rollNumber:"N/A");
				$("#lrn-address").text(getAjaxCall.address!=null?getAjaxCall.address:"N/A");
				if(getAjaxCall.identificationType!=null){
					$("#lrn-identificationtype").text(getAjaxCall.identificationType);
					learner.updateIdentificationNumberLabel(getAjaxCall.identificationType,"#lrn-identificationnumberDiv");
					//$("#lrn-identificationnumberDiv").text(getAjaxCall.identificationType+" "+"Number");

				}
				
				$("#lrn-identificationnumber").text(getAjaxCall.identificationNumber);
				$("#lrn-bloodgroup").text(getAjaxCall.bloodGroup.replace("NVE"," -").replace("PVE"," +"));
				


				if(getAjaxCall.income.incomeId != null){
					$("#spouse-name").text(getAjaxCall.spouseName);
					$("#spouse-age").text(getAjaxCall.spouseAge);
					$("#spouse-income").text(getAjaxCall.income.value);
					$("#spouse-location").text(getAjaxCall.spouseWorkLocation);
					$(".spouse-details").show();
				} else {
					$(".spouse-details").hide();
				}
				$("#membersList tbody").empty();
				if(getAjaxCall.learnerFamilyEarningMemberDetailsMappers.length){
					$.each(getAjaxCall.learnerFamilyEarningMemberDetailsMappers,function(index,member){
						var status=member.memberEmploymentStatus==true || member.memberEmploymentStatus=="true"?"Yes":"No";
						var income = member.memberEmploymentStatus==true || member.memberEmploymentStatus=="true"?member.memberIncome:"";
						$("#membersList tbody").append("<tr><td>"+member.memberName+"</td><td>"+member.memberAge+"</td><td>"+member.gender+"</td><td>"+member.relationshipWithLearner+"</td><td>"+status+"</td><td>"+income+"</td></tr>");
					});
					$(".member-container").show();
				} else {
					$(".member-container").hide();
				}
				$("#viewLeranerModal").modal("show");
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : getAjaxCall.responseMessage,
					close : 'auto'
				});
			}
		},
		fnUpdate : function(){
			spinner.showSpinner();
			editLearner.learnerFamilyEarningMemberDetails=[];
			var error=false;
			$.each($('.edit-member'),function(index,value){
				member={};
				member.learnerFamilyEarningMemberDetailsMapperId=$(this).find(editLearner.memberName).attr('mapperId');
				member.memberName=$(this).find(editLearner.memberName).val();
				member.memberAge=$(this).find(editLearner.memberAge).val();
				member.gender=$(this).find(editLearner.memberGender+":checked").val();
				member.relationshipWithLearner=$(this).find(editLearner.memberRelation).val();
				member.memberEmploymentStatus=$(this).find(editLearner.memberEmpStatus+":checked").val();
				member.memberIncome=$(this).find(editLearner.memberIncome).val();
				if(!member.memberName || member.memberAge <= 0 || !member.relationshipWithLearner || (member.memberEmploymentStatus == "true" && !member.memberIncome)){
					error = true;
					return false;
				}
				editLearner.learnerFamilyEarningMemberDetails.push(member);
			})
			if(error){
				AJS.flag({
					type  : "error",
					title : "Error..!",
					body  : "Enter all members details",
					close : 'auto'
				});
				spinner.hideSpinner();
				return false;
			}
			console.log(editLearner.learnerFamilyEarningMemberDetails);
			var ajaxData={
					learnerId	:$(editLearner.id).val(),
					name	: $(editLearner.name).val(),
					formNo	: $(editLearner.formNo).val(),
					age		: $(editLearner.age).val(),
					gender	: $(editLearner.gender+":checked").val(),
					address	: $(editLearner.address).val(),
					instructor : $(editLearner.instructor).val(),
					eyeTestResult	: $(editLearner.eyeTestResult).val(),
					preTestScore	: $(editLearner.preTestScore).val(),
					maritalStatus	: $(editLearner.maritalStatus).val(),
					spouseAge		: $(editLearner.spouseAge).val(),
					spouseName		: $(editLearner.spouseName).val(),
					spouseWorkLocation		: $(editLearner.spouseWorkLoc).val(),
					familySize				: $(editLearner.familySize).val(),
					numberOfEarningMembers	: $(editLearner.earningMembers).val(),
					rollNumber              : $(editLearner.rollNumber).val(),
					occupation		: {'occupationId':$(editLearner.occupation).val()},
					income			: {'incomeId':$(editLearner.spouseIncome).val()},
					center		 	: {"id":$(selectBox.center).val()},
					level 		 	: {"levelId": $(selectBox.level).val()},
					batch		 	: {"batchId":$(selectBox.batch).val()},
					academicCycle	: {"academicCycleId":$(selectBox.academicYear).val()},
					identificationType        :$(editLearner.identificationType).val(),
					identificationNumber      :$(editLearner.identificationNumber).val(),
					bloodGroup                :$(editLearner.bloodGroup).val(),
					learnerFamilyEarningMemberDetailsMappers :editLearner.learnerFamilyEarningMemberDetails
			}
			var editAjaxCall = shiksha.invokeAjax("learner", ajaxData, "PUT");
			spinner.hideSpinner();
			if(editAjaxCall != null){
				if(editAjaxCall.response == "shiksha-200"){					
					$(editLearner.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : centerColumn.sucessAlert,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
					setTimeout(function(){
						learner.resetForm(editLearner.form);
						var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=rollNumber",learner.data, "POST");
						spinner.hideSpinner();
						$(pageContextElements.bootstrapTable).bootstrapTable('load',ajaxCall.learners);
					}, 500);	


				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
		},
		initDelete : function(learnerId,name){
			var msg = $("#deleteLearnerMessage").data("message");
			$("#deleteLearnerMessage").html(msg.replace('@NAME',name));
			deleteLearner.id=learnerId;
			$(deleteLearner.modal).modal('show');
		},
		fnDelete : function(){
			var deleteAjaxCall = shiksha.invokeAjax("learner/"+deleteLearner.id,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall.response == "shiksha-200"){
				var ajaxCall = shiksha.invokeAjax("learner/?type=bulkRollNumber&op=rollNumber",learner.data, "POST");
				spinner.hideSpinner();
				$(pageContextElements.bootstrapTable).bootstrapTable('load',ajaxCall.learners);
				$(deleteLearner.modal).modal('hide');
				AJS.flag({
					type  : "success",
					title : centerColumn.sucessAlert,
					body  : deleteAjaxCall.responseMessage,
					close : 'auto'
				})
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.error,
					body  : deleteAjaxCall.responseMessage,
					close : 'auto'
				});
			}			
		},
		formValidate : function(validateFormName) {
			var spouseDiv="";
			var idType="";

			if(validateFormName==editLearner.form){
				spouseDiv=editLearner.spouseDiv;
				idType=editLearner.identificationType;
			}
			if(validateFormName==addLearner.form){
				spouseDiv=addLearner.spouseDiv;
				idType=addLearner.identificationType;
			}


			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					toDate:{
						required: function(element){
							return $(element).parents(pageContextElements.createToDiv).is(":visible");
						},
					},
					formNo:{
						required: true,
						noSpace: true,
					},
					name: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace: true
					},
					age:{
						required: true,
						noSpace: true,
						min:1

					},
					gender:{

					},
					instructor:{
						required: true,
						noSpace: true,
					},
					eyeTestResult:{
						required: true,
						noSpace: true,
					},
					preTestScore:{
						required: true,
						noSpace: true,
					},
					occupation:{
						required: true,
						noSpace: true,
					},
					familySize:{
						required: true,
						noSpace: true,
						min:1

					},
					maritalStatus:{
						required: true,
						noSpace: true,
					},
					spouseName:{
						required: function(element){  return $(element).parents(spouseDiv).is(":visible"); },
					},
					spouseAge:{
						required: function(element){ return $(element).parents(spouseDiv).is(":visible");},
					},
					spouseIncome:{
						required: function(element){ return $(element).parents(spouseDiv).is(":visible");},
					},
					spouseWorkLocation:{
						required: function(element){ return $(element).parents(spouseDiv).is(":visible");},
					},
					earningMembers:{
						required: true,
						noSpace: true,
						min:0,
						maxMembers: validateFormName

					},

					rollNumber:{
						min:0
					},

					memberName:{
						required: true,
						noSpace: true,
					},
					memberAge:{
						required: true,
						noSpace: true,
					},
					memberRelation:{
						required: true,
						noSpace: true,
					},
					memberIncome:{
						required: function(element){
							return $(element).parents(".row").find('[name="memberEmpStatus0"]:checked').val() == "true";
						},

					},
					address:{
						required: true,
						noSpace: true,
						maxlength: 255,
					},
					identificationNumber:{
						identificationrequired:idType,
						alphanumeric:idType,
						identificationLength:idType,

					},
				},
				messages: {
					toDate:{
						required:messages.toDateRequired
					},
					formNo:{
						required:messages.formNoRequired
					},
					name: {
						required:messages.nameRequired,
						minlength:messages.nameMinLength,
						maxlength:messages.nameMaxLength
					},
					age:{
						required:messages.ageRequired
					},
					gender:{
						required:messages.genderRequired
					},
					instructor:{
						required:messages.instructorRequired
					},
					eyeTestResult:{
						required:messages.eyeTestResultRequired
					},
					preTestScore:{
						required:messages.preTestScoreRequired
					},
					occupation:{
						required:messages.occupationRequired
					},
					familySize:{
						required:messages.familySizeRequired
					},
					maritalStatus:{
						required:messages.maritalStatusRequired
					},
					spouseName:{
						required:messages.spouseNameRequired
					},
					spouseAge:{
						required:messages.spouseAgeRequired
					},
					spouseIncome:{
						required:messages.spouseIncomeRequired
					},
					spouseWorkLocation:{
						required:messages.spouseWorkLocationRequired
					},
					earningMembers:{
						min:appMessgaes.minZero,
						required:messages.earningMembersRequired,
						maxMembers:messages.maxEarningMember

					},
					rollNumber:{
						min:appMessgaes.minZero,
					},
					memberName:{
						required:messages.memberNameRequired
					},
					memberAge:{
						required:messages.memberAgeRequired
					},
					memberRelation:{
						required:messages.memberRelationRequired
					},
					memberIncome:{
						required:messages.memberIncomeRequired
					},
					address:{
						required:messages.addressRequired,
						maxlength:messages.nameMaxLength
					},
					/*identificationNumber:{
						noSpace: true,
						//required:messages.identificationNumberRequired ,

					},*/
				},
				errorPlacement: function(error, element) {
					if($(element).hasClass("datepicker")){
						$(element).parents().find('#divCreateDateTo').append(error);
					}else if($(element).hasClass("select")){
						$(element).parent().append(error);
					} else {
						$(error).insertAfter(element);
					}
				},
				submitHandler : function(){
					if(validateFormName === addLearner.form)
					{
						learner.fnAdd(submitActor.id);
					}
					else if(validateFormName === editLearner.form){
						learner.fnUpdate();
					}
					else{
						learner.showLearnerDetails();
					}
				}
			});
		},
};
var deleteMember  = {
		deleleteMemberBtn 	: ".action-delete-member",
		deleteMemberModal 	: "#mdlDeleteMember",
		deleteMemberConfirmBtn 	: "#deleteMemberButton",
		message				: "#deleteMemberMessage",
		deleteMemberId : '',
		init : function(){
			$(document).on("click",this.deleleteMemberBtn,this.initDelete);
			$(this.deleteMemberConfirmBtn).on("click",this.fnDeleteMember);
		},
		initDelete : function(){
			var dayId=$(this).parent().data("mapperid");
			if(!dayId){
				$(this).parent().remove();
				$(editLearner.earningMembers).val($('.edit-member').length);
				$(editLearner.earningMembers).valid();
				return false;
			}		
			var name = $(this).parent().find(editLearner.memberName).val();
			var msg = $(deleteMember.message).data("message");
			$(deleteMember.message).html(msg.replace('@NAME@',name));
			deleteMember.deleteMemberId=dayId;
			$(deleteMember.deleteMemberModal).modal("show");
		},

		fnDeleteMember : function(){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("learner/learnerFamilyEarningMemberDetailsMapper/"+deleteMember.deleteMemberId, null, "DELETE");
			if(deleteAjaxCall != null){
				if(deleteAjaxCall.response == "shiksha-200"){
					$(".edit-member[data-mapperid='"+deleteMember.deleteMemberId+"']").remove();
					$(deleteMember.deleteMemberModal).modal("hide");
					$(editLearner.earningMembers).val($('.edit-member').length);
					$(editLearner.earningMembers).valid();
					AJS.flag({
						type  : "success",
						title : centerColumn.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});

				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		}
};
var addMember= {
		addBtn	: '.action-add-member',
		init : function(){
			$(document).on('click',this.addBtn,this.addNewMember);
		},
		addNewMember : function(){			
			var earningMembers = $(".edit-member-details-template").clone();
			$(earningMembers).find(".member-number").text("Member: "+($('.edit-member').length+1))
			$(addMember.addBtn).parent().next().append($(earningMembers).show().removeClass("edit-member-details-template").addClass("edit-member"));
			$(earningMembers).find('input[class="edit-gender"]').attr('name','memberGender'+($('.edit-member').length+1));
			$(earningMembers).find('input[class="edit-emp-status"]').attr('name','memberEmpStatus'+($('.edit-member').length+1));
			$(editLearner.earningMembers).val($('.edit-member').length);
			$(editLearner.earningMembers).valid();
			$(editLearner.memberDetails).show();
		},
};
var learnerFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
};
//trim whitespace 
$(':input').change(function() {
	$(this).val($(this).val().trim());
});


$( document ).ready(function() {
	learner.formValidate(addLearner.form);
	learner.formValidate(editLearner.form);
	learner.formValidate(pageContextElements.form);
	learner.fnInit();
	addMember.init();
	deleteMember.init();
	$submitActors.click(function() {
		submitActor = this;
	});
	var start = new Date(); 
	var end = new Date();
	$('.datepicker').datepicker({
		format: 'dd-M-yyyy',
		todayHighlight: true,
		autoclose: true,
		/*startDate:start*/
	});
	$(pageContextElements.createDateFrom).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(){
		$(pageContextElements.createToDiv).show();
		$(pageContextElements.createDateTo).val("");
		$(pageContextElements.tableDiv).hide();
		$(pageContextElements.table).hide();
		$(pageContextElements.createDateTo).datepicker('setStartDate', $(this).val());
	}); 
	$(pageContextElements.createDateTo).datepicker({
		startDate : start,
		endDate   : end
	}).on('changeDate', function(e){	
		$(pageContextElements.tableDiv).hide();
		$(pageContextElements.table).hide();
		if($(pageContextElements.createDateFrom).val() < $(pageContextElements.createDateTo).val())
			e.preventDefault();
	}); 
	jQuery.validator.addMethod("noSpace", function(value) {
		return !value.trim().length <= 0;
	}, "");

	jQuery.validator.addMethod("maxMembers", function(value, element, params) {
		if(params === addLearner.form) {
			return  parseInt($(addLearner.familySize).val())>=parseInt(value);
		}
		else if(params === editLearner.form){
			return  parseInt($(editLearner.familySize).val())>=parseInt(value);
		}
	}, "");


	jQuery.validator.addMethod("identificationLength", function(value, element,params) {

		var identificationValue=$(params).find('option:selected').val();
		switch (identificationValue) { 
		case 'DL': 
			$.validator.messages.identificationLength=messages.dlNumberLength;
			return value.length==16?true:false;
			break;
		case 'PAN': 
			$.validator.messages.identificationLength=messages.panNumberLength;
			return value.length==10?true:false;
			break;
		case 'AADHAAR': 
			$.validator.messages.identificationLength=messages.adharNumberLength;
			return value.length==12 || value.length==16?true:false;
			break;		
		default:
			return true;
		}
	}, $.validator.messages.identificationLength);

	jQuery.validator.addMethod("alphanumeric", function(value, element,params) {
		var identificationValue=$(params).find('option:selected').val();
		switch (identificationValue) { 
		case 'DL': 
			$.validator.messages.alphanumeric=messages.dlNumberAlphaNumeric;
			return this.optional(element) || /^[a-zA-Z0-9-]+$/.test(value);
			break;
		case 'PAN': 
			$.validator.messages.alphanumeric=messages.panNumberAlphaNumeric;
			return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
			break;
		case 'AADHAAR': 
			$.validator.messages.alphanumeric=messages.aadhaarNumberAlphaNumeric;
			return this.optional(element) || /^[a-zA-Z0-9]+$/.test(value);
			break;		
		default:
			return true;
		}
	},  $.validator.messages.alphanumeric);

	jQuery.validator.addMethod("identificationrequired", function(value, element,params) {
		var identificationValue=$(params).find('option:selected').val();
		var isValid=learner.checkIdentificationNumber(identificationValue,element);
		switch (identificationValue) { 
		case 'DL': 
			$.validator.messages.identificationrequired=messages.dlNumberRequired;
			return !isValid;
			break;
		case 'PAN': 
			$.validator.messages.identificationrequired=messages.panNumberRequired;
			return !isValid;
			break;
		case 'AADHAAR': 
			$.validator.messages.identificationrequired=messages.aadhaarNumberRequired;
			return !isValid;
			break;		
		default:
			return true;
		}
	},  $.validator.messages.identificationrequired);

	

	$('.panel').on('hidden.bs.collapse', function () {
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green ");
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
    });
	
	$('.panel').on('shown.bs.collapse', function () {
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign glyphicon-plus-sign red green ");
		$(pageContextElements.eleToggleCollapse).find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
		
	});

	$(addLearner.familySize).on('change blur keyup', function() {
		if( $(addLearner.earningMembers).val()!=""){
			$(addLearner.earningMembers).valid();
		}
	});

	$(editLearner.familySize).on('change blur keyup', function() {
		if( $(editLearner.earningMembers).val()!=""){
			$(editLearner.earningMembers).valid();
		}
	});

	/*	$(addLearner.identificationType).on('change select', function() {
		var identificationValue=$(addLearner.identificationType).find('option:selected').val();
		if(identificationValue==""){
			$(addLearner.identificationNumber).val("");
			$(pageContextElements.addIdentificationNumberDiv).hide();

		}else{
			$(pageContextElements.addIdentificationNumberDiv).show();
		}
		$(addLearner.identificationNumber).valid();

	});

	$(editLearner.identificationType).on('change select', function() {
		var identificationValue=$(editLearner.identificationType).find('option:selected').val();
		if(identificationValue==""){
			$(editLearner.identificationNumber).val("");
			$(pageContextElements.editIdentificationNumberDiv).hide();
		}else{
			$(pageContextElements.editIdentificationNumberDiv).show();
		}
		$(editLearner.identificationNumber).valid();

	});*/
});