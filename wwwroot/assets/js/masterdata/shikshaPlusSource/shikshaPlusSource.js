var pageContextElements = {	
		addButton 						: '#addNewShikshaPlusSource',
		table 							: '#shikshaPlusSourceTable',
		toolbar							: "#toolbarShikshaPlusSource"
};
var addModal ={
		form 			: '#addShikshaPlusSourceForm',
		modal   		: '#mdlAddShikshaPlusSource',
		saveMoreButton	: 'addShikshaPlusSourceSaveMoreButton',
		saveButton		: 'addShikshaPlusSourceSaveButton',
		eleName		   	: '#addShikshaPlusSourceName',
		eleDescription	: '#addShikshaPlusSourceDescription'
};
var editModal ={
			form	 : '#editShikshaPlusSourceForm',
			modal	 : '#mdlEditShikshaPlusSource',
			eleName	 : '#editShikshaPlusSourceName',
			eleId	 : '#shikshaPlusSourceId',
			eleDescription	: '#editShikshaPlusSourceDescription'
			
};
var deleteModal ={
			modal	: '#mdlDeleteShikshaPlusSource',
			confirm : '#deleteShikshaPlusSourceButton',
};
var $table;
var submitActor = null;
var $submitActors = $(addModal.form).find('button[type=submit]');	
var ShikshaPlusSourceIdForDelete;

var shikshaPlusSourceFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var shikshaPlusSource={
		name:'',
		shikshaPlusSourceId:0,

		init:function(){

			$(pageContextElements.addButton).on('click',shikshaPlusSource.initAddModal);
			$(deleteModal.confirm).on('click',shikshaPlusSource.deleteFunction);

		},
		deleteFunction : function(){
			shikshaPlusSource.fnDelete(ShikshaPlusSourceIdForDelete);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			shikshaPlusSource.resetForm(addModal.form);
			$(addModal.modal).modal("show");

		},
		resetForm : function(formId){
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},

		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name"		  :$(addModal.eleName).val(),
					"description" : $(addModal.eleDescription).val(),
			}
			var addAjaxCall = shiksha.invokeAjax("shikshaplus/source", ajaxData, "POST");
			
			if(addAjaxCall != null){
				if(addAjaxCall.response == "shiksha-200"){
					if(submitBtnId == addModal.saveButton){
						$(addModal.modal).modal("hide");
						AJS.flag({
							type  : "success",
							title : sourceMessages.sucessAlert,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						});
					}
					if(submitBtnId == addModal.saveMoreButton){
						$(addModal.modal).modal("show");
					}
					shikshaPlusSource.resetForm(addModal.form);
					shikshaPlusSource.refreshTable(pageContextElements.table);
					$(addModal.modal).find("#divSaveAndAddNewMessage").show();
					fnDisplaySaveAndAddNewElementAui(addModal.modal,addAjaxCall.name);
					
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : addAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}
			spinner.hideSpinner();
		},

		initEdit: function(shikshaPlusSourceId){
			shikshaPlusSource.resetForm(editModal.form);
			$(editModal.modal).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("shikshaplus/source/"+shikshaPlusSourceId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
				if(editAjaxCall.response == "shiksha-200"){
					$(editModal.eleName).val(editAjaxCall.name);
					$(editModal.eleId).val(editAjaxCall.sourceId);
					$(editModal.eleDescription).val(editAjaxCall.description);
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : editAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close	: 'auto'
				})
			}
		},

		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name"		:$(editModal.eleName).val(),
					"sourceId"	:$(editModal.eleId).val(),
					"description" : $(editModal.eleDescription).val(),
			}

			var updateAjaxCall = shiksha.invokeAjax("shikshaplus/source", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
				if(updateAjaxCall.response == "shiksha-200"){
					$(editModal.modal).modal("hide");
					shikshaPlusSource.resetForm(editModal.form);
					shikshaPlusSource.refreshTable(pageContextElements.table);
					AJS.flag({
						type 	: "success",
						title 	: sourceMessages.sucessAlert,
						body 	:updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				} else {
					AJS.flag({
						type 	: "error",
						title 	: appMessgaes.error,
						body 	: updateAjaxCall.responseMessage,
						close 	: 'auto'
					})
				}
			} else {
				AJS.flag({
					type 	: "error",
					title 	: appMessgaes.oops,
					body 	: appMessgaes.serverError,
					close 	: 'auto'
				})
			}
		},
		initDelete : function(shikshaPlusSourceId,name){
			var msg = $("#deleteShikshaPlusSourceMessage").data("message");
			$("#deleteShikshaPlusSourceMessage").html(msg.replace('@NAME@',name));
			$(deleteModal.modal).modal("show");
			ShikshaPlusSourceIdForDelete=shikshaPlusSourceId;
		},
		fnDelete :function(shikshaPlusSourceId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("shikshaplus/source/"+shikshaPlusSourceId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
				if(deleteAjaxCall.response == "shiksha-200"){
					shikshaPlusSource.refreshTable(pageContextElements.table);
					$(deleteModal.modal).modal("hide");
					AJS.flag({
						type  : "success",
						title : sourceMessages.sucessAlert,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					})
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.error,
						body  : deleteAjaxCall.responseMessage,
						close : 'auto'
					});
				}
			} else {
				AJS.flag({
					type  : "error",
					title : appMessgaes.oops,
					body  : appMessgaes.serverError,
					close : 'auto'
				})
			}

		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
			}).validate({
				ignore: '',
				rules: {
					shikshaPlusSourceName: {
						required: true,
						minlength: 1,
						maxlength: 255,
						noSpace:true,
					},
					/*shikshaPlusSourceDescription: {
						required: true,
						minlength: 2,
						maxlength: 255,
						noSpace:true,
					}*/
				},

				messages: {
					shikshaPlusSourceName: {
						required: sourceMessages.nameRequired,
						noSpace:sourceMessages.nameNoSpace,
						minlength: sourceMessages.nameMinLength,
						maxlength: sourceMessages.nameMaxLength,

					},
					/*shikshaPlusSourceDescription: {
						required: sourceMessages.descriptionRequired,
						minlength: sourceMessages.descriptionMinLength,
						maxlength: sourceMessages.descriptionMaxLength,
						noSpace:sourceMessages.descriptionNoSpace,
					}*/
				},
				submitHandler : function(){
					if(validateFormName === addModal.form){
						shikshaPlusSource.fnAdd(submitActor.id);
					}
					if(validateFormName === editModal.form){
						shikshaPlusSource.fnUpdate();
					}
				}
			});
		},
		shikshaPlusSourceActionFormater: function(value, row) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="shikshaPlusSource.initEdit(\''+row.sourceId+'\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="shikshaPlusSource.initDelete(\''+row.sourceId+'\',\''+row.name+'\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		shikshaPlusSourceNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {

	shikshaPlusSource.formValidate(addModal.form);
	shikshaPlusSource.formValidate(editModal.form);

	$submitActors.click(function() {
		submitActor = this;
	});
	shikshaPlusSource.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: pageContextElements.toolbar ,
		url : "shikshaplus/source",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "server",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		formatShowingRows : shikshaPlusSourceFilterObj.formatShowingRows,
		clickToSelect: false,
		
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});