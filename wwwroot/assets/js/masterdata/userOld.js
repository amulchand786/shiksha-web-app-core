
var elementArr =[$('#addUserName'),$('#addUserLogin'),$('#addUserPassword'),$('#addUserConfirmPassword')
	                 ,$('#editUserName')];
var toHide =[$('#alertdiv')];

/*var addUser = function() {
	$('.outer-loader').show();
	var userName = $('#addUserName').val();
	var userLogin = $('#addUserLogin').val().toLowerCase();
	var userPassword = $('#addUserPassword').val();
	var isAdmin=$("#isAdminCheckbox").prop("checked");
	
	var json = {
		"userName" : userName,
		"userLogin" : userLogin,
		"password" : userPassword,
		"isAdmin" : isAdmin

	};
	var message = customAjaxCalling("user", json, "POST");

	if (message.indexOf("added") > -1) {
		$('.outer-loader').hide();
			location.reload();
		$("#add-user").find('#errorMessage').hide();
		$("#add-user").find('#exception').hide();
		$("#add-user").find('#buttonGroup').hide();
		$("#add-user").find('#successMessage').show();
		$("#add-user").find('#okButton').show();
		
		disableInputs($('#addUserName'),$('#addUserLogin'),$('#addUserPassword'));
		

	} else {
			$('.outer-loader').hide();
			if (message.indexOf("Login name already exist. Try with another") > -1){
				$("#add-user").find('#successMessage').hide();
				$("#add-user").find('#okButton').hide();
				$("#add-user").find('#errorMessage').show();
				$("#add-user").find('#fail').hide();
				$("#add-user").find('#exception').show();
				$("#add-user").find('#buttonGroup').show();
				$("#add-user").find('#exception').text(message);
				$('#alertdiv').show();
				$('#addUserLogin').focus();
				$( "#addUserLogin" ).keypress(function() {
					  console.log( "Handler for .keypress() called." );
					  $('#alertdiv').hide();
				});
			}
		else{
				// $('#errorMessage').text(message);
				$("#add-user").find('#successMessage').hide();
				$("#add-user").find('#okButton').hide();
				$("#add-user").find('#errorMessage').show();
				$("#add-user").find('#exception').show();
				$("#add-user").find('#buttonGroup').show();
				$("#add-user").find('#exception').text(message);
				hideAllInfoOnKeyPress(elementArr,toHide);
			 }
	}
};

var changePassword = function(event) {

	var currentPassword = $('#currentPassword').val();
	var newPassword = $('#newPassword').val();
	var confirmPassword = $('#confirmPassword').val();

	var json = {
		"id" : userId,
		"oldPassword" : currentPassword,
		"newPassword" : newPassword,
		"confirmPassword" : confirmPassword

	};


	var message = customAjaxCalling("changePassword", json, "PUT");
	
	if (message.indexOf("change successfully") > -1) {
		
		$("#change-password").find('#errorMessage').hide();
		$("#change-password").find('#exception').hide();
		$("#change-password").find('#buttonGroup').hide();
		$("#change-password").find('#successMessage').show();
		setTimeout(function(){
			window.location= '/logout';
		},2000);
	} else {
		
		$("#change-password").find('#successMessage').hide();
		$("#change-password").find('#okButton').hide();
		$("#change-password").find('#errorMessage').show();
		$("#change-password").find('#exception').show();
		$("#change-password").find('#buttonGroup').show();
		$("#change-password").find('#exception').text(message);
	
	}
};


var resetPassword = function(event) {
	$('.outer-loader').show();
	//var currentPassword = $('#ResetCurrentPassword').val();
	var newPassword = $('#ResetNewPassword').val();
	var confirmPassword = $('#ResetConfirmPassword').val();
	var userId = $('#resetPasswordUserId').val();

	var json = {
	//	"oldPassword" : currentPassword,
		"newPassword" : newPassword,
		"confirmPassword" : confirmPassword,
		"id" : userId,

	};
	

	var message = customAjaxCalling("resetPassword", json, "PUT");
	

	
	if (message.indexOf("change successfully") > -1) {
		$('.outer-loader').hide();
		location.reload();
		} else {
			$('.outer-loader').hide();
		//$('#errorMessage').text(message);
		$("#reset-password").find('#successMessage').hide();
		$("#reset-password").find('#okButton').hide();
		$("#reset-password").find('#errorMessage').show();
		$("#reset-password").find('#exception').show();
		$("#reset-password").find('#buttonGroup').show();
		$("#reset-password").find('#exception').text(message);
		}
};


var editUser = function(event) {
	$('.outer-loader').show();
	var userName = $('#editUserName').val();
	var userLogin = $('#editUserLogin').val();
	var userId = $('#editUserId').val();
	var isAdmin=$("#isAdminEditCheckbox").prop("checked");

	console.log(userId);
	console.log("edit");
	var json = {
		"userName" : userName,
		"userLogin" : "",
		"id" : userId,
		"isAdmin" : isAdmin

	};
	var message = customAjaxCalling("user", json, "PUT");
	if (message.indexOf("Information Updated") > -1) {
		$('.outer-loader').hide();
		location.reload();
	} else {
		$('.outer-loader').hide();
		// $('#errorMessage').text(message);
		$("#edit-user").find('#successMessage').hide();
		$("#edit-user").find('#okButton').hide();
		$("#edit-user").find('#errorMessage').show();
		$("#edit-user").find('#exception').show();
		$("#edit-user").find('#buttonGroup').show();
		$("#edit-user").find('#exception').text(message);		
	}

};

$('#deleteUserButton').click(function(event) {
	$('.outer-loader').show();

	var json = {
		"id" : deleteUserId,
		"userName" : deleteUserName

	};
	var message = customAjaxCalling("user", json, "DELETE");
	if (message.indexOf("deleted") > -1) {
		$('.outer-loader').hide();
		location.reload();
		
	} else {
		$('.outer-loader').hide();
		
		$("#delete-user").find('#okButton').hide();
		$("#delete-user").find('#errorMessage').show();
		$("#delete-user").find('#exception').text(message);
		$("#delete-user").find('#buttonGroup').show();
	}

});*/

/*
var editUserData = function(userId, userName, userLogin,isAdmin) {
	$('#editUserName').val(userName);
	$('#editUserLogin').val(userLogin);
	$('#editUserId').val(userId);
	var id =$('#divIsAdminChkbox').find('input:checkbox[name=isAdminEditCheckbox]').attr('id');
	if(isAdmin == "true"){				
		   $('#'+id).prop('checked', true);
	}else{
		$('#'+id).prop('checked', false);	
	}
};

var deleteUserData = function(userId, userName) {
	deleteUserId = userId;
	deleteUserName = userName;
	console.log(deleteUserId);
	console.log(deleteUserName);
	$('#deleteUserMessage').text("Do you want to delete " + userName + " ?");

};

var ResetPasswordUserData = function(userId) {
	$('#resetPasswordUserId').val(userId);
	};
var validateAddUser=function(){
	$("#addUserForm").formValidation({

		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'

		},
		fields : {
			userLogin : {
				validators : {
					callback : {
						message : 'Login id should be a min of 5 and upto a max of 20 alpha numeric characters in small letters',
						callback : function(value, validator, $field) {
							var regex = /^[a-z0-9]{5,20}$/;
						    return regex.test(value);
						}
					}
				}
			},
			userPassword : {
				validators : {
					callback : {
						message : 'Password should be a minimum of 8 characters long and should include at least one each of an uppercase letter, a lowercase letter, a special character and a number.',
						callback : function(value, validator, $field) {
							var regex = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!+_~`)(-@#$%&*^])[a-zA-Z0-9!+_~`)(-@#$%&*^]{8,}$/);
						    return regex.test(value);
						}
					}
				}
			}
		},
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addUser();
		});
}*/
/*$('#addFormButton').on('click', function() {
	$('#addUserForm')[0].reset();
	//$('#addUserForm').formValidation("destroy");
	validateAddUser();
});*/
$(function() {
	setDataTablePagination("#tblUser");
	
	//$('#addUserForm').formValidation();
	
	/*
	validateAddUser();
	
	$("#editUserForm").formValidation({
		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
	fields : {
		userLogin : {
			validators : {
				callback : {
					message : 'Login should be 5-20 alphanumeric characters',
					callback : function(value, validator, $field) {
						var regex = /^[a-z0-9]{5,20}$/;
					    return regex.test(value);
					}
				}
			}
		}
	}

	}).on('success.form.fv', function(e) {
		e.preventDefault();
		
		editUser();
		

	});
	
	
	
	$("#changePasswordForm").formValidation({

		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			password : {
				validators : {
					callback : {
						message : 'Password should be a minimum of 8 characters long and should include at least one each of an uppercase letter, a lowercase letter, a special character and a number.',
						callback : function(value, validator, $field) {
							var regex = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!+_~`)(-@#$%&*^])[a-zA-Z0-9!+_~`)(-@#$%&*^]{8,}$/);
						    return regex.test(value);
						}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		changePassword();

	});
	
	$("#resetPasswordForm").formValidation({

		feedbackIcons : {
			valid : 'glyphicon glyphicon-ok',
			invalid : 'glyphicon glyphicon-remove',
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			NewPassword : {
				validators : {
					callback : {
						message : 'Password should be a minimum of 8 characters long and should include at least one each of an uppercase letter, a lowercase letter, a special character and a number.',
						callback : function(value, validator, $field) {
							var regex = new RegExp(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!+_~`)(-@#$%&*^])[a-zA-Z0-9!+_~`)(-@#$%&*^]{8,}$/);
						    return regex.test(value);
						}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		resetPassword();

	});*/
	
	
});