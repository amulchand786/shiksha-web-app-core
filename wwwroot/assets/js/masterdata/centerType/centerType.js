var pageContextElements = {	
	addButton 					: '#addNewCenterType',
	table 						: '#centerTypeTable',
	addCenterTypeForm			: '#addCenterTypeForm',
	modalAddCenterType			: '#mdlAddCenterType',
	addCenterTypeSaveMoreButton : '#addCenterTypeSaveMoreButton',
	addCenterTypeSaveButton 	: '#addCenterTypeSaveButton',
	modalEditCenterType 		: '#mdlEditCenterType',
	modalDeleteCenterType 		: '#mdlDeleteCenterType',
	editCenterTypeForm          : '#editCenterTypeForm',
	deleteCenterTypeButton		: '#deleteCenterTypeButton'
};
var $table;
var submitActor = null;
var $submitActors = $(pageContextElements.addCenterTypeForm).find('button[type=submit]');
var centerId;
var centerTypeFilterObj = {
		formatShowingRows : function(pageFrom,pageTo,totalRows){
			return 'Showing '+pageFrom+' to '+pageTo+' of '+totalRows+' rows';
		},
		actionFormater : function(){
			return {
				classes:"dropdown dropdown-td"
			}
		}
	};
var centerType={
		name:'',
		centerTypeId:0,
		
		init:function(){
						
			$(pageContextElements.addButton).on('click',centerType.initAddModal);
			$(pageContextElements.deleteCenterTypeButton).on('click',centerType.deleteFunction);
		},
		initAddModal :function(){
			fnInitSaveAndAddNewList();
			centerType.resetForm(pageContextElements.addCenterTypeForm);
			$(pageContextElements.modalAddCenterType).modal("show");
		},
		resetForm : function(formId){
			$(formId).find('#alertdiv').hide();
			$(formId).find("label.error").remove();
			$(formId).find(".error").removeClass("error");
			$(formId)[0].reset();
		},
		refreshTable: function(table){
			$(table).bootstrapTable("refresh");
		},
		deleteFunction : function(){
			centerType.fnDelete(centerId);
		},		
		fnAdd : function(submitBtnId){
			spinner.showSpinner();
			var ajaxData={
					"name":$('#addCenterTypeName').val(),
					}
			var addAjaxCall = shiksha.invokeAjax("center/type", ajaxData, "POST");
			spinner.hideSpinner();
			if(addAjaxCall != null){
					if(addAjaxCall.response == "shiksha-200"){
						if(submitBtnId == "addCenterTypeSaveButton"){
							$(pageContextElements.modalAddCenterType).modal("hide");
							AJS.flag({
								type : "success",
								title : messages.sucessAlert,
								body : addAjaxCall.responseMessage,
								close :'auto'
							})
						}
						if(submitBtnId == "addCenterTypeSaveMoreButton"){
							$(pageContextElements.modalAddCenterType).modal("show");
						}
						centerType.resetForm(pageContextElements.addCenterTypeForm);
						centerType.refreshTable(pageContextElements.table);
						$(pageContextElements.modalAddCenterType).find("#divSaveAndAddNewMessage").show();
						fnDisplaySaveAndAddNewElementAui(pageContextElements.modalAddCenterType,addAjaxCall.name);
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : addAjaxCall.responseMessage,
							close : 'auto'
						})
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
		},
		initEdit: function(centerTypeId){
			centerType.resetForm(pageContextElements.editCenterTypeForm);
			$(pageContextElements.modalEditCenterType).modal("show");
			spinner.showSpinner();
			var editAjaxCall = shiksha.invokeAjax("center/type/"+centerTypeId,null, "GET");
			spinner.hideSpinner();
			if(editAjaxCall != null){	
					if(editAjaxCall.response == "shiksha-200"){
						$('#editCenterTypeName').val(editAjaxCall.name);
						$('#centerTypeId').val(editAjaxCall.centerTypeId)
						
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : editAjaxCall.responseMessage,
							close : 'auto'
						})
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
			
		},
		
		fnUpdate:function(){
			spinner.showSpinner();
			var ajaxData={
					"name":$('#editCenterTypeName').val(),
					"centerTypeId":$('#centerTypeId').val(),
					}
			
			var updateAjaxCall = shiksha.invokeAjax("center/type", ajaxData, "PUT");
			spinner.hideSpinner();
			if(updateAjaxCall != null){	
					if(updateAjaxCall.response == "shiksha-200"){
						$(pageContextElements.modalEditCenterType).modal("hide");
						centerType.resetForm(pageContextElements.editCenterTypeForm);
						centerType.refreshTable(pageContextElements.table);
						AJS.flag({
							type  : "success",
							title : messages.sucessAlert ,
							body : updateAjaxCall.responseMessage,
							close : 'auto'
						})
					} else {
						AJS.flag({
							type  : "error",
							title : appMessgaes.error,
							body  : updateAjaxCall.responseMessage,
							close : 'auto'
						})
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
		},
		initDelete : function(centerTypeId,name){
			var msg = $("#deleteCenterTypeMessage").data("message");
			$("#deleteCenterTypeMessage").html(msg.replace('@NAME',name));
			$(pageContextElements.modalDeleteCenterType).modal("show");
			centerId=centerTypeId;
		},
		fnDelete :function(centerTypeId){
			spinner.showSpinner();
			var deleteAjaxCall = shiksha.invokeAjax("center/type/"+centerTypeId,null, "DELETE");
			spinner.hideSpinner();
			if(deleteAjaxCall != null){	
					if(deleteAjaxCall.response == "shiksha-200"){
						centerType.refreshTable(pageContextElements.table);
						$(pageContextElements.modalDeleteCenterType).modal("hide");
						AJS.flag({
							type  : "success",
							title : messages.sucessAlert,
							body : deleteAjaxCall.responseMessage,
							close : 'auto'
						})
					} else {
						AJS.flag({
							type : "error",
							title : appMessgaes.error,
							body : deleteAjaxCall.responseMessage,
							close :'auto'
						});
					}
				} else {
					AJS.flag({
						type  : "error",
						title : appMessgaes.oops,
						body  : appMessgaes.serverError,
						close : 'auto'
					})
				}
			
		},
		formValidate: function(validateFormName) {
			$(validateFormName).submit(function(e) {
				e.preventDefault();
				}).validate({
					ignore: '',
			        rules: {
			        	centerTypeName: {
			                required: true,
			                minlength: 1,
			                maxlength: 255,
			                noSpace:true,
			            }			            
			        },

			        messages: {
			        	centerTypeName: {
			        		required: messages.nameRequired,
							noSpace:messages.noSpace,
							minlength: messages.minLength,
							maxlength: messages.maxLength,

			            }
			        },
					submitHandler : function(){
						if(validateFormName === pageContextElements.addCenterTypeForm){
							centerType.fnAdd(submitActor.id);
						}
						if(validateFormName === pageContextElements.editCenterTypeForm){
							centerType.fnUpdate();
						}
					}
		});
		},
		centerTypeActionFormater: function(value, row, index) {
			var action = ""; 
			if(permission["edit"] || permission["delete"]){
				action +='<a class="btn btn-default actionButton" data-toggle="dropdown" href="#"><span class="aui-icon aui-icon-small aui-iconfont-more">Actions</span> </a><ul id="contextMenu" class="dropdown-menu" role="menu">';
				if(permission["edit"])
					action+='<li><a onclick="centerType.initEdit(\''+ row.centerTypeId+ '\')" href="javascript:void(0)">'+appMessgaes.edit+'</a></li>';
				if(permission["delete"])
					action+='<li><a onclick="centerType.initDelete(\''+ row.centerTypeId+ '\',\''+ row.name+ '\')" href="javascript:void(0)" class="delLink">'+appMessgaes.delet+'</a></li>';
				action+='</ul>';
			}
			return action;
		},
		centerTypeNumber: function(value, row, index) {
			return index+1;
		},
};

$( document ).ready(function() {
	
	centerType.formValidate(pageContextElements.addCenterTypeForm);
	centerType.formValidate(pageContextElements.editCenterTypeForm);
	
	$submitActors.click(function() {
        submitActor = this;
    });
	centerType.init();
	$table = $(pageContextElements.table).bootstrapTable({
		toolbar: "#toolbarCenterType",
		url : "center/type",
		method : "get",
		toolbarAlign :"right",
		search: false,
		sidePagination: "client",
		showToggle: false,
		showColumns: false,
		pagination: true,
		searchAlign: 'left',
		pageSize: 20,
		formatShowingRows : centerTypeFilterObj.formatShowingRows,
		clickToSelect: false,
		pageList: [10, 15, 25, 50, 100]
	});
	jQuery.validator.addMethod("noSpace", function(value) { 
		return !value.trim().length <= 0; 
	}, "");
});