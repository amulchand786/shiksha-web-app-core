


var colorClass='';
var thLabels;
var resetButtonObj;
//initialize global variables
var initGloabalVarsDist =function(){
	 resetButtonObj='';
	
}





var elementInput=[];
elementInput.push("addStateName");
var initInput =function(){	
	$("#mdlAddState").modal().show();
	$("#addSateName").val('');
	$("#mdlAddState #addStateForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddState #addStateForm").data('formValidation').resetForm();
}


var applyPermissions=function(){
	if (!editStatePermission){
	$("#btnEditState").remove();
		}
	if (!deleteStatePermission){
	$("#deleteState").remove();
		}
	}


//create a row after save
var getStateRow =function(state){
	var row = 
		"<tr id='"+state.stateId+"' data-id='"+state.stateId+"'>"+
		"<td></td>"+
		"<td data-stateid='"+state.stateId+"'	title='"+state.stateName+"'>"+state.stateName+"</td>"+
		"<td data-statecode='"+state.stateCode+"' title='"+state.stateCode+"'>"+state.stateCode+"</td>"+		
		"<td><div class='div-tooltip'><a class='tooltip tooltip-top' id='btnEditState'" +
		"onclick=editStateData(&quot;"+state.stateId+"&quot;,&quot;"+escapeHtmlCharacters(state.stateName)+"&quot;);><span class='tooltiptext'>"+appMessgaes.edit+"</span><span class='glyphicon glyphicon-edit font-size12'></span></button><a style='margin-left:10px;' class='tooltip tooltip-top' id=deleteState	onclick=deleteStateData(&quot;"+state.stateId+"&quot;,&quot;"+escapeHtmlCharacters(state.stateName)+"&quot;); ><span class='tooltiptext'>"+appMessgaes.delet+"</span><span class='glyphicon glyphicon-trash font-size12'></span></a></div></td>"+
		"</tr>";

	return row;
}

var addStateData=function(event){

	var stateId=0;
	var stateName = $('#addSateName').val().trim().replace(/\u00a0/g," ");
	var json ={ 
			"stateId" : stateId,
			"stateName" : stateName,    	  
	};   
	//update data table without reloading the whole table
	//save&add new
	var	btnAddMoreStateId =$(event.target).data('formValidation').getSubmitButton().data('id');
	$('.outer-loader').show();
	var state = customAjaxCalling("state", json, "POST");
	if(state!=null){
		if(state.response=="shiksha-200"){
			$('#t0').prop('class');//get the colorClass
			var newRow =getStateRow(state);		
			appendNewRow("stateTable",newRow);
			applyPermissions();
			
			fnUpdateDTColFilter('#stateTable',[],4,[0,3],thLabels);
			setShowAllTagColor(colorClass);
			
			
			
			if(btnAddMoreStateId=="addStateSaveMoreButton"){
			
				showDiv($('#mdlAddState #alertdiv'));
				$("#mdlAddState").find('#errorMessage').hide();				
				fnDisplaySaveAndAddNewElement('#mdlAddState',stateName);				

				initInput();
			}else{			
				$('#mdlAddState').modal("hide");
				setTimeout(function() {   //calls click event after a one sec
					 AJS.flag({
					    type: 'success',
					    title: validationMsg.sucessAlert,
					    body: '<p>'+ state.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);				
				
			}	
		}else {

			showDiv($('#mdlAddState #alertdiv'));
			$("#mdlAddState").find('#successMessage').hide();
			$("#mdlAddState").find('#okButton').hide();
			$("#mdlAddState").find('#errorMessage').show();
			$("#mdlAddState").find('#exception').show();
			$("#mdlAddState").find('#buttonGroup').show();
			$("#mdlAddState").find('#exception').text(state.responseMessage);
		}
		$(".outer-loader").hide();
	}
	$('.outer-loader').hide();
	return;
};



var editStateId=null;
var deleteStateName=null;
var deleteStateId=null;
var editStateData=function(stateId,stateName){	
	$('#editStateName').val(stateName);	
	editStateId=stateId;

	$("#edit-state #editStateDataForm").data('formValidation').resetForm();
	$("#edit-state").modal().show();


};
var editData=function(){
	$(".outer-loader").show();
	var stateName = $('#editStateName').val().trim().replace(/\u00a0/g," ");  
	var json ={ 
			"stateId" : editStateId,
			"stateName" : stateName,


	};


	var state = customAjaxCalling("state", json, "PUT");
	if(state!=null){
		if(state.response=="shiksha-200"){
			
			$('#t0').prop('class');//get the colorClass 
			deleteRow("stateTable",state.stateId);
			var newRow =getStateRow(state);
			appendNewRow("stateTable",newRow);
			applyPermissions();
			
			fnUpdateDTColFilter('#stateTable',[],4,[0,3],thLabels);
			setShowAllTagColor(colorClass);
			displaySuccessMsg();	

			$('#edit-state').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: validationMsg.sucessAlert,
				    body: '<p>'+ state.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);	
			
		}else {		
			showDiv($('#edit-state #alertdiv'));
			$("#edit-state").find('#successMessage').hide();
			$("#edit-state").find('#errorMessage').show();
			$("#edit-state").find('#exception').show();
			$("#edit-state").find('#buttonGroup').show();
			$("#edit-state").find('#exception').text(state.responseMessage);
		}
	}
	$(".outer-loader").hide();

};


var deleteStateData=function(stateId,stateName){
	deleteStateId=stateId;
	deleteStateName=stateName;
	$('#delete-state #deleteStateMessage').text(columnMessages.deleteConfirm.replace("@NAME",stateName)+ " ?");
	$("#delete-state").modal().show();
	hideDiv($('#delete-state #alertdiv'));
};
$(document).on('click','#deleteStateButton', function(){
	$(".outer-loader").show();
	var state = customAjaxCalling("state/"+deleteStateId, null, "DELETE");
	if(state!=null){
		if(state.response=="shiksha-200"){
			$(".outer-loader").hide();						
			// Delete Row from data table and refresh the table without refreshing the whole table
			deleteRow("stateTable",deleteStateId);

			fnUpdateDTColFilter('#stateTable',[],4,[0,3],thLabels);
			setShowAllTagColor(colorClass);

			$('#delete-state').modal("hide");
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: validationMsg.sucessAlert,
				    body: '<p>'+ state.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);	

		} else {
			showDiv($('#delete-state #alertdiv'));
			$("#delete-state").find('#errorMessage').show();
			$("#delete-state").find('#exception').text(state.responseMessage);
			$("#delete-state").find('#buttonGroup').show();
		}
	}
	$(".outer-loader").hide();

});


$(function() {

	thLabels =['#',columnMessages.state,columnMessages.code,columnMessages.action];
	fnSetDTColFilterPagination('#stateTable',4,[0,3],thLabels);

	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#stateTable',[]);
	});
	fnUnbindDTSortListener();
	$('#tableDiv').show();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	$( "#addSateName" ).keypress(function() {		
		hideDiv($('#mdlAddState #alertdiv'));
	})
	$( "#editStateDataForm" ).keypress(function() {		
		hideDiv($('#edit-state #alertdiv'));
	})
	colorClass = $('#t0').prop('class');

	$("#addStateForm") .formValidation({
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			addSateName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: validationMsg.nameMaxLength
                    },
					callback : {
						message :columnMessages.state+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                        callback : function(value) {
                        	var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}							
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addStateData(e);
	});
	$("#editStateDataForm") .formValidation({
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},fields : {
			editStateName : {
				validators : {
					stringLength: {
                        max: 250,
                        message: validationMsg.nameMaxLength
                    },
					callback : {
						message :columnMessages.state+ ' '+validationMsg.invalidInputMessage.replace("@NAME@",'(", ?, \', /, \\)'),
                        callback : function(value) {
                        	var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
					
							return regx.test(value);
						}
					}
				}
			}
		}
	})
	.on('success.form.fv', function(e) {
		e.preventDefault();
		editData();

	});
});