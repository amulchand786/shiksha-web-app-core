/* 
 * @Mukteshwar 
 * 
*/




var addModal ={};
	addModal.grade ='#mdlAddSubject #grade';
var editModal ={};
	editModal.grade ='#mdlEditSubject #grade';
var self;
var ajaxResponseData;
var gEvent;

var prevSubjectTagValue='';

var thLabels=[];

var subject ={
		
		gradeId:0,
		gradeList :null,
		gradeIds :null,
		
		ajaxURL:'',
		ajaxMethodType:'',
		ajaxContentType:'',
		ajaxRequestData :'',
		ajaxResponseData:'',
		
		init:function(){
			self =subject;
			
			self.gradeId =0;
			self.gradeList =[];
			self.gradeIds =[];
			
			self.ajaxURL='';
			self.ajaxMethodType='';
			self.ajaxContentType='';
			self.ajaxRequestData ='';
			self.ajaxResponseData='';
			
			
			 self.fnGetGrades();
			
		},
		/**
		 * ajax call
		 * */
		invokeAjax :function(URL,aRequestData,methodType,pContentType){
			
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					ajaxResponseData =responseData;
				},
				error : function() {					
					return false;
				}
			});
		},
		/**
		 * 
		 * Multiselect Initialization 
		 * 
		 * */
		fnMultiselect	:function(ele){
			$(ele).multiselect('destroy');
			$(ele).multiselect({
				maxHeight: 180,
				includeSelectAllOption: true,
				enableFiltering: true,
				enableCaseInsensitiveFiltering: true,
				includeFilterClearBtn: true,
				filterPlaceholder: 'Search here...',
			});
			fnCollapseMultiselect();
		},
		
		/**
		 * Get grade list 
		 * 
		 * */
		fnGetGrades :function(){
			self.ajaxURL ="getGradeList";
			self.ajaxMethodType ='GET';
			self.ajaxContentType ='application/json';
			
			self.invokeAjax(self.ajaxURL,null,self.ajaxMethodType,self.ajaxContentType);
			
			self.gradeList =ajaxResponseData;
		},
		
		/**
		 * Bind grade 
		 * @param
		 * @pSelector	: String 	: selectBox element
		 * @selectedValue : Integer 
		 * */
		fnBindGrade :function(pSelector,selectedValue){			
			shk.fnEmptyAllSelectBox(pSelector);
			var $ele = $(pSelector);
			var data =self.gradeList;
			$.each(data, function(index, obj) {
				var idx=jQuery.inArray(obj.gradeId, selectedValue);
				if(idx !=-1){				
					$ele.append('<option value="'+obj.gradeId+'" data-id="'+obj.gradeId+'"data-name="'+obj.gradeName+'" selected>'+escapeHtmlCharacters(obj.gradeName)+'</option>');	
				}else{
					$ele.append('<option value="'+obj.gradeId+'" data-id="'+obj.gradeId+'"data-name="'+obj.gradeName+'">'+escapeHtmlCharacters(obj.gradeName)+'</option>');
				}			
			});
			self.fnMultiselect(pSelector);
			fnCollapseMultiselect();
		},
};	


var  colorClass='';
var initInput =function(type){	
	
	$("#mdlAddSubject").modal().show();
	$("#addSubjectName").val('');
	$("#mdlAddSubject #addSubjectForm").find("#alertDiv #errorMessage").hide();
	$("#mdlAddSubject #addSubjectForm").data('formValidation').resetForm();
	
	if(type!='addmore'){
		self.fnBindGrade(addModal.grade,0);	
	}
	
	 
}



var applyPermissions=function(){
	if (!editSubjectPermission){
	$(".btnEditSubject").remove();
		}
	if (!deleteSubjectPermission){
	$(".btnDeleteSubject").remove();
		}
	}

//create a row after save
var getSubjectRow =function(subject){
	var gIds =subject.gradeIds;
	
	
	var ulStart = '', li = [],grades='';
	var gradeNames= subject.gradeName;
	
	if(gradeNames!=null){
	var	temp = gradeNames.split("#,");
		$.each(temp, function (index, value) {
			 li[index] = '<li>' + value + '</li>';
		});
		grades=ulStart+li+ulStart;
		grades=grades.replace(/i>,/g , "i>");
		grades=grades.replace(/#/g , "");
	}
	  
	var row = 
		"<tr id='"+subject.subjectId+"' data-id='"+subject.subjectId+"'>"+
		"<td></td>"+
		
		"<td data-subjectid='"+subject.subjectId+"'	title='"+subject.subjectName+"'>"+subject.subjectName+"</td>"+
		"<td data-subjectid='"+subject.subjectId+"'	title='"+subject.gradeName+"'>"+grades+"</td>"+
		
		"<td data-subjectcode='"+subject.subjectCode+"' title='"+subject.subjectCode+"'>"+subject.subjectCode+"</td>"+		

		"<td><div class='div-tooltip'><a  class='tooltip tooltip-top btnEditSubject'	 id='btnEditGrade'" +
		"onclick='editSubjectData(&quot;"+subject.subjectId+"&quot;,&quot;"+escapeHtmlCharacters(subject.subjectName)+"&quot;,&quot;"+subject.subjectName+"&quot;,&quot;"+gIds+"&quot;);'>  <span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a  style='margin-left:10px;'' class='tooltip tooltip-top btnDeleteSubject' id=deleteSubject"+subject.subjectId+"'	onclick='deleteSubjectData(&quot;"+subject.subjectId+"&quot;,&quot;"+escapeHtmlCharacters(subject.subjectName)+"&quot;,&quot;"+subject.subjectName+"&quot;,&quot;"+subject.gradeIds+"&quot;);'><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
	return row;
}

////////////////////////////////////////////////


var addSubjectData=function(event){
	var subjectId=0;
	var subjectName = $('#addSubjectName').val().trim().replace(/\u00a0/g," ");
	
	self.gradeIds =[];
	$(addModal.grade).each(function(index,ele){
		$(ele).find("option:selected").each(function(index,option){
			if($(option).val()!="multiselect-all"){				
				self.gradeIds.push($(option).val());				
			}
		});
	});
	
	
	
	var json ={ 
			"subjectId" : subjectId,
			"subjectName" : subjectName,
			"gradeIds" :self.gradeIds
	};


	//update data table without reloading the whole table
	//save&add new
	var btnAddMoreSubjectId =$(event.target).data('formValidation').getSubmitButton().data('id');

	var subject = customAjaxCalling("subject", json, "POST");
	if(subject!=null){
		if(subject.response=="shiksha-200"){
			$(".outer-loader").hide();

			var newRow =getSubjectRow(subject);
			appendNewRow("subjectTable",newRow);  
			applyPermissions();
			fnUpdateDTColFilter('#subjectTable',[],5,[0,2,4],thLabels);
			setShowAllTagColor(colorClass);
			if(btnAddMoreSubjectId=="addSubjectSaveMoreButton"){
				$("#mdlAddSubject").find('#errorMessage').hide();

				fnDisplaySaveAndAddNewElement('#mdlAddSubject',subjectName);		
				initInput('addmore');
			}else{	
				$("#mdlAddSubject").modal("hide");
				
				setTimeout(function() {   
					AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>'+ subject.responseMessage+'</p>',
					    close :'auto'
					})
					}, 1000);
				
			}	
		}else {
			showDiv($('#mdlAddSubject #alertdiv'));
			$("#mdlAddSubject").find('#successMessage').hide();
			$("#mdlAddSubject").find('#okButton').hide();
			$("#mdlAddSubject").find('#errorMessage').show();
			$("#mdlAddSubject").find('#exception').show();
			$("#mdlAddSubject").find('#buttonGroup').show();
			$("#mdlAddSubject").find('#exception').text(subject.responseMessage);
		}
	}
	$(".outer-loader").hide();
};

var editSubjectId=null;
var deleteSubjectName=null;
var deleteSubjectId=null;
var editSubjectData=function(subjectId,subjectName,editSubSgName,pGradeIds){
	
	prevSubjectTagValue =editSubSgName.replace(/\u00a0/g," ");
	
	$('#editSubjectName').val(subjectName);	
	editSubjectId=subjectId;
	$("#mdlEditSubject").modal().show();
	hideDiv($('#mdlEditSubject #alertdiv'));
	$("#mdlEditSubject #editSubjectForm").data('formValidation').resetForm();
	
	pGradeIds =pGradeIds.replace('[','').replace(']','');
	var spilitedGrades =pGradeIds.split(',');
	var gIds =[];
	$.each(spilitedGrades,function(index,ele){
		gIds.push(parseInt(ele,10));
	});
	
	
	if(typeof(gIds)!='object'){
		self.gradeIds =gIds;
		
	}else{
		self.gradeIds =gIds;	
	}	
	
	self.fnBindGrade(editModal.grade,self.gradeIds);
	
};

/////////////


var editData=function(){
    var subjectName = $('#editSubjectName').val().trim().replace(/\u00a0/g," ");
    
    
    
    
    self.gradeIds =[];
	$(editModal.grade).each(function(index,ele){
		$(ele).find("option:selected").each(function(index,option){
			if($(option).val()!="multiselect-all"){				
				self.gradeIds.push($(option).val());				
			}
		});
	});
    
	var json ={ 
    	  "subjectId" : editSubjectId,
    	  "subjectName" : subjectName,
    	  "gradeIds" :self.gradeIds
    };
	

	var subject = customAjaxCalling("subject", json, "PUT");
	if(subject!=null){
	if(subject.response=="shiksha-200"){
		$(".outer-loader").hide();
		deleteRow("subjectTable",subject.subjectId);
		var newRow =getSubjectRow(subject);
		appendNewRow("subjectTable",newRow);
		applyPermissions();
		
		fnUpdateDTColFilter('#subjectTable',[],5,[0,2,4],thLabels);
		setShowAllTagColor(colorClass);
		displaySuccessMsg();	
		
			$('#mdlEditSubject').modal("hide");
			
			setTimeout(function() {   
				AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ subject.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			
						
		}		
	else {
		
		showDiv($('#mdlEditSubject #alertdiv'));
		$("#mdlEditSubject").find('#successMessage').hide();
		$("#mdlEditSubject").find('#errorMessage').show();
		$("#mdlEditSubject").find('#exception').show();
		$("#mdlEditSubject").find('#buttonGroup').show();
		$("#mdlEditSubject").find('#exception').text(subject.responseMessage);
	}
	}
	$(".outer-loader").hide();
};
	

/////////////////////////////////////////////////////
//check is grade deletable from this subject
var fnIsSubjectEditable =function(e){
	
var	aExistedGrades =self.gradeIds;
var	newGrades =[];
	$(editModal.grade).each(function(index,ele){
			$(ele).find("option:selected").each(function(index,option){
				if($(option).val()!="multiselect-all"){				
					newGrades.push($(option).val());				
				}
		});
	});
	    
	var json ={ 
	    	  "subjectId" : editSubjectId,
	    	  "existedGradeIds" : aExistedGrades,
	    	  "newGradeIds" :newGrades
	};		
	var flag= customAjaxCalling("subject/editable", json, "POST");
	if(flag=="true"){
		editData();
	}else{
		showDiv($('#mdlEditSubject #alertdiv'));
		$("#mdlEditSubject").find('#successMessage').hide();
		$("#mdlEditSubject").find('#errorMessage').show();
		$("#mdlEditSubject").find('#exception').show();
		$("#mdlEditSubject").find('#buttonGroup').show();
		$("#mdlEditSubject").find('#exception').text("Grades can not be delete from this subject. Some chapters are associated with this grade and subject.");
	}

};


/////////////////////////////////////////////////////

 
 var deleteSubjectData=function(subjectId,subjectName,delSubSgName){
		deleteSubjectId=subjectId;
		deleteSubjectName=subjectName;
		$('#mdlDeleteSubject #deleteSubjectMessage').text("Do you want to delete "+subjectName+ " ?");
		$("#mdlDeleteSubject").modal().show();
		hideDiv($('#mdlDeleteSubject #alertdiv'));
	};
	$(document).on('click','#deleteSubjectButton', function(){
		
	  	   
	    var subject = customAjaxCalling("subject/"+deleteSubjectId, null, "DELETE");
		if(subject!=null){
		if(subject.response=="shiksha-200"){
			$(".outer-loader").hide();
			// Delete Row from data table and refresh the table without refreshing the whole table
			deleteRow("subjectTable",deleteSubjectId);
			
			fnUpdateDTColFilter('#subjectTable',[],5,[0,2,4],thLabels);
			setShowAllTagColor(colorClass);
			
			
			
			$('#mdlDeleteSubject').modal("hide");
			
			setTimeout(function() {   
				AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>'+ subject.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);		
			
		
		} else {
			showDiv($('#mdlDeleteSubject #alertdiv'));
			$("#mdlDeleteSubject").find('#errorMessage').show();
			$("#mdlDeleteSubject").find('#exception').text(subject.responseMessage);
			$("#mdlDeleteSubject").find('#buttonGroup').show();
		}
		}
		$(".outer-loader").hide();
	});



/////////////

	
	
	
	
	
	
	
	
	

var fnHideSubjectAlertDiv =function(){
	hideDiv($('#mdlAddSubject #alertdiv'));
	hideDiv($('#mdlEditsubject #alertdiv'));
}



$(function() {
	thLabels =['#','Subject  ','Grade(s)  ','Subject Code  ','Action'];
	fnSetDTColFilterPagination('#subjectTable',5,[0,2,4],thLabels);
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#subjectTable',[]);
	});
	colorClass = $('#t0').prop('class');
	$("#tableDiv").show();

	fnColSpanIfDTEmpty('subjectTable',5);
	subject.init();

	fnHideSubjectAlertDiv();
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	/////////////////////////// ADD Form Validation /////////////////
	$('#addSubjectForm').formValidation({ 
		excluded:':disabled',
		framework : 'bootstrap',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			addgrade: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(addModal.grade).val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			addSubjectName : {
				validators : {
					stringLength: {
						max: 250,
						message: 'Subject name must be less than 250 characters'
					},
					callback : {
						message : 'Subject '+invalidInputMessage,
						callback : function(value) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}
					}
				}
			},
		}
	}).on('err.form.fv', function(e) {
		e.preventDefault();
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addSubjectData(e);
	});
/////////////////////////// EDIT Form Validation ///////////////////
	$('#editSubjectForm').formValidation(
			{
				excluded:':disabled',
				framework : 'bootstrap',
				feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
				},
				fields : {
					editgrade: {
						validators : {
							callback : {
								message : '      ',
								callback : function() {
									// Get the selected options
									var option = $(editModal.grade).val();
									return (option != null && option!="NONE");
								}
							}
						}
					},
					editSubjectName : {
						validators : {
							stringLength: {
								max: 250,
								message: 'Subject name must be less than 250 characters'
							},
							callback : {
								message : 'Subject '+invalidInputMessage,
								callback : function(value) {
									var regx=/^[^'?\"/\\]*$/;
									if(/\ {2,}/g.test(value))
										return false;
									return regx.test(value);}
							}
						}
					}
				}
			}).on('err.form.fv', function(e) {
				e.preventDefault();
			}).on('success.form.fv', function(e) {
				e.preventDefault();
				
				fnIsSubjectEditable(e);
			})
});