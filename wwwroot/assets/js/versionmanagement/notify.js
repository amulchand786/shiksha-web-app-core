var pageContextDiv =new Object();
	
	


var pageContextElement =new Object();
	pageContextElement.form ="#frm_notification";
	pageContextElement.role ="#selectBox_roleType";
	pageContextElement.appType=	"#selectBox_AppType";
	pageContextElement.fileLoc ="#fileLoc";
	pageContextElement.fileVersion ="#fileVersion";
	pageContextElement.btnNotify ="#btnNotify";


var notify={
		type:'',
		roleIds:[],
		fileLocation:'',
		fileVersion:'',
		
		
		ajaxURL:'',
		ajaxMethodType:'',
		ajaxContentType:'',
		ajaxRequestData :'',
		ajaxResponseData:'',
		
		
		fnInit :function(){
			self =notify;
			self.type ='';
			self.roleIds =[];
			self.fileLocation ='';
			self.fileVersion ='';
			
			
			self.ajaxURL ='';
			self.ajaxMethodType ='';
			self.ajaxContentType ='';
			self.ajaxRequestData ='';
			self.ajaxResponseData ='';
		},
		fnSendNotification :function(){
			$('.outer-loader').show();
			
			self.fnGetRoles();
			self.type =$(pageContextElement.appType).find('option:selected').val();
			self.fileLocation =$(pageContextElement.fileLoc).val();
			self.fileVersion =$(pageContextElement.fileVersion).val();
			self.ajaxURL='';
			self.ajaxURL ='version/app';
			self.ajaxMethodType ='POST';
			self.ajaxContentType ='application/json';
			self.ajaxRequestData ={
				"type":self.type,
				"roleIds":self.roleIds,
				"fileLocation":self.fileLocation,
				"version":self.fileVersion
			};
			
			notify.fnInvokeAjax(self.ajaxURL,self.ajaxRequestData,self.ajaxMethodType,self.ajaxContentType);
		
			if(self.ajaxResponseData!=null && self.ajaxResponseData!='undefined'){
				if(self.ajaxResponseData.response=="shiksha-200"){
					$(pageContextElement.role).multiselect('refresh');
					resetFormById('frm_notification');
					$(pageContextElement.role).multiselect('refresh');
					$(pageContextElement.appType).multiselect('refresh');
					$('#success-msg').show();
					$("#success-msg").fadeOut(5000);
				}else{
					console.log("error...");
				}	
			}	
		},
		
		
		fnGetRoles :function(){
			self.roleIds =[];
			$(pageContextElement.role).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all"){				
					self.roleIds.push($(option).data('id'));
				}
			});
			console.log("Roles:"+self.roleIds);
			return self.roleIds;
		},
		
		
		/**
		 * ajax call
		 * */
		fnInvokeAjax :function(URL,aRequestData,methodType,pContentType){
			
			console.log("self.invokeAjax");
			ajaxResponseData ='';
			$.ajax({
				url : URL,
				type : methodType,
				contentType:pContentType,
				async : false,
		        data :JSON.stringify(aRequestData ),
				success : function(responseData) {
					self.ajaxResponseData =responseData;
				},
				error : function(e) {
					return false;
				}
			});
		}
}


$(function() {
	setOptionsForMultipleSelect(pageContextElement.role);
	setOptionsForMultipleSelect(pageContextElement.appType);
	fnCollapseMultiselect();
	
	notify.fnInit();
	
	
	$(pageContextElement.form) .formValidation({
		excluded:':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			fileLoc : {
				 validators: {
	                    stringLength: {
	                        max: 500,
	                        message: 'File location name must be less than 500 characters'
	                    }
	              }
			},
			fileVersion : {
				validators : {
					stringLength: {
                        max: 6,
                        message: 'File version must be less than 6 characters'
                    },
				}
			},
			
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		setTimeout(function(){$('.outer-loader').show();},100);
		setTimeout(function(){$('.outer-loader').hide();},4500);
		notify.fnSendNotification();
	});
});
		