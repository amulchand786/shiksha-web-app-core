function LocationData(){	
	self = this;
	self.location = new Object();
	self.maxLevelType = "";
}

LocationData.prototype.initialize = function(locationInfo){
	self.location = locationInfo;	
};

LocationData.prototype.getLocationByTypeAndId = function(id, type){
	var res_location;
	$(self.location[type]).each(function(index, location){
		if(parseInt(location.id) == id){
			res_location = location;
			return false;
		}
	});
	return res_location;
};

LocationData.prototype.getLocationByName = function(name){
	return self.location[name];
};

LocationData.prototype.getLocationByTypeAndParentId = function(id, type){
	var keys = Object.keys(self.location);
	var locationMap = new Object();
	
	$(keys).each(function(keyIndex, key){
		$(self.location[key]).each(function(index, location){
			if(parseInt(location.parentLocationid) == id && location.parentLocationType == type){
				var locType = location.locationType;
				if(locationMap[locType] == null || typeof locationMap[locType] == "undefined"){
					locationMap[locType] = new Array();
				}
				locationMap[locType].push(location);
			}
		});
	});
	return locationMap;
};

LocationData.prototype.getAllLocations = function(){	
	return self.location;
};

LocationData.prototype.setMaxLevelType = function(levelType){	
	self.maxLevelType = levelType;
};

LocationData.prototype.getMaxLevelType = function(){
	return self.maxLevelType;
};