//global variables
var schoolGradeFormId;
var addFormId;
var editFormId;
var gradeIdList;
var sectionIdList;
var addSectionsIds;
var editSectionsIds;
var editMapperId;
var schoolId;
var mappedGrades;
var mappedSchool;
var deletedGradeId;
var deletedSchoolId;
var deleteSchoolGradeId;
var addButtonId;

var editedRowId; 

var listTableName;
var sectionObj;


//global selectDivIds for smart filter
var schoolGradePageContext = new Object();	
	schoolGradePageContext.city 	="#divSelectCity";
	schoolGradePageContext.block 	="#divSelectBlock";
	schoolGradePageContext.NP 	="#divSelectNP";
	schoolGradePageContext.GP 	="#divSelectGP";
	schoolGradePageContext.RV 	="#divSelectRV";
	schoolGradePageContext.village ="#divSelectVillage";
	schoolGradePageContext.school ="#divSelectSchool";
	
	schoolGradePageContext.schoolLocationType ="#selectBox_schoolLocationType";
	schoolGradePageContext.elementSchool ="#selectBox_schoolsByLoc";
	schoolGradePageContext.grade ="#selectBox_grades";
	
	schoolGradePageContext.resetLocDivCity ="#divResetLocCity";
	schoolGradePageContext.resetLocDivVillage ="#divResetLocVillage";
	
var addSchGrdSecModalElements =new Object();
	addSchGrdSecModalElements.section ="#mdlAddSchoolGradeSec #selectBox_section";
	addSchGrdSecModalElements.grade ="#mdlAddSchoolGradeSec #selectBox_grades";
	
var editSchGrdSecModalElements =new Object();
	editSchGrdSecModalElements.section ="#mdlEditSchoolGradeSec #selectBox_section";
	editSchGrdSecModalElements.grade ="#mdlEditSchoolGradeSec #selectBox_grades";

	
//initialise global variables
var initGlblVariables =function(){
	schoolGradeFormId ='listSchoolGradeForm';
	addFormId ='addSchoolGradeSecForm';
	editFormId ='editSchoolGradeSecForm';
	gradeIdList=[];schoolId='';
	mappedGrades=[];
	mappedSchool='';
	deletedGradeId='';
	deletedSchoolId='';
	deleteSchoolGradeId='';
	listTableName='schoolGradeTable';
	editedRowId='';
	addSectionsIds =[];editSectionsIds=[];editMapperId='';addButtonId='';
	//get all section on page load
	sectionObj =customAjaxCalling("section/list", null, "GET");
	sectionIdList=[];
	$.each(sectionObj,function(index,obj){
		sectionIdList.push(obj.sectionId);
	});
} 


//////////////////////
//Smart filter		//
//////////////////////

//show or hide div as per school location type
$(schoolGradePageContext.schoolLocationType).change(function(){
	$('.outer-loader').show();
	setTimeout(function(){
	deleteAllRow("schoolGradeTable");
		
	var typeCode =$('#selectBox_schoolLocationType').find('option:selected').data('code');
	reinitializeSmartFilter(elementIdMap);	
	$("#hrDiv").show();
	if(typeCode.toLowerCase() =='U'.toLowerCase()){			
		showDiv($(schoolGradePageContext.city),$(schoolGradePageContext.resetLocDivCity));
		hideDiv($(schoolGradePageContext.block),$(schoolGradePageContext.NP),$(schoolGradePageContext.GP),$(schoolGradePageContext.RV),$(schoolGradePageContext.village),$(schoolGradePageContext.resetLocDivVillage));
		displaySmartFilters();
		positionFilterDiv("U");
	}else{
		hideDiv($(schoolGradePageContext.city),$(schoolGradePageContext.resetLocDivCity));
		showDiv($(schoolGradePageContext.block),$(schoolGradePageContext.NP),$(schoolGradePageContext.GP),$(schoolGradePageContext.RV),$(schoolGradePageContext.village),$(schoolGradePageContext.resetLocDivVillage));
		displaySmartFilters();
		positionFilterDiv("R");
	}
	$('.outer-loader').hide();
	return;
}, 100);
});


//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionFilterDiv =function(type){
	if(type=="R"){		
		$(schoolGradePageContext.NP).insertAfter( $(schoolGradePageContext.block));
		$(schoolGradePageContext.RV).insertAfter( $(schoolGradePageContext.GP));
		$(schoolGradePageContext.school).insertAfter( $(schoolGradePageContext.village));
	}else{
		$(schoolGradePageContext.school).insertAfter( $(schoolGradePageContext.city));
	}
}
//displaying smart-filters
var displaySmartFilters =function(){
	$(".outer-loader").show();

	elementIdMap = {
				"State" : '#statesForZone',
				"Zone" : '#selectBox_zonesByState',
				"District" : '#selectBox_districtsByZone',
				"Tehsil" : '#selectBox_tehsilsByDistrict',
				"Block" : '#selectBox_blocksByTehsil',
				"City" : '#selectBox_cityByBlock',
				"Nyaya Panchayat" : '#selectBox_npByBlock',
				"Gram Panchayat" : '#selectBox_gpByNp',
				"Revenue Village" : '#selectBox_revenueVillagebyGp',
				"Village" : '#selectBox_villagebyRv',
				"School" : '#selectBox_schoolsByLoc'
	};
	displayLocationsOfSurvey($(schoolGradePageContext.schoolLocationType), $('#divFilter'));

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){$(".outer-loader").hide(); $('#rowDiv1,#divFilter').show(); }, 100);	
};


//reset only location when click on reset button icon
var resetLocAndSchool =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	$('#btnAddGradetoSchool').prop('disabled', true);
	deleteAllRow("schoolGradeTable");
	if($(resetButtonObj).parents("form").attr("id")=="listSchoolGradeForm"){		
		reinitializeSmartFilter(elementIdMap);displaySmartFilters();
	}
}


//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
}
//////////////////////
//Smart filter ended//
//////////////////////

//////////////////////
//Operations--CRUD	//
//////////////////////


//show table as per school
$(schoolGradePageContext.elementSchool).change(function(){
	$('#btnAddGradetoSchool').prop('disabled', false);
	//$(".outer-loader").show();
	//$('#schoolGradeTable thead').empty();
	deleteAllRow(listTableName);
	/*$('#schoolGradeTable thead').append('<tr><th></th><th>Grade</th><th>Grade code</th><th>Section(s)</th><th class="nosort"></th></tr>'+'<tr><th>#</th><th>Grade</th><th>Grade code</th><th>Section(s)</th><th class="nosort">Action</th></tr>');*/
	
	var schoolId =$(this).find('option:selected').data('id');
	var schoolGradeData =customAjaxCalling("schoolgradesectionmapper/"+schoolId, null, "GET");
	if(schoolGradeData!=null && schoolGradeData!=""){
		if(schoolGradeData[0].response=="shiksha-200"){
			$(schoolGradeData).each(function(index,dataObj){
				appendNewRow(listTableName,getNewRow(dataObj));
				applyPermissions();
			});
		}
	}	
	//setDataTableColFilterPagination('#schoolGradeTable',5,[0,4],false);
	fnSetDTColFilterPagination('#schoolGradeTable',5,[0,4],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnShowHideColumns('#schoolGradeTable',[]);
	});
	enableTooltip();
});

//permissions for edit and delete
var applyPermissions=function(){
	if (!editSchoolGradeSectionMapperPermission){
		$("#editSchoolGrade").remove();
	}
	if (!deleteSchoolGradeSectionMapperPermission){
		$("#deleteSchoolGrade").remove();
	}

	}

var getNewRow =function(schoolGradeData){
	var newRow =
		"<tr id='"+schoolGradeData.schoolGradeSectionMapperId+"_"+schoolGradeData.gradeVm.gradeId+"' ids='"+schoolGradeData.schoolGradeSectionMapperId+"' data-id='"+schoolGradeData.schoolGradeSectionMapperId+"' data-schoolid='"+schoolGradeData.schoolID+"' data-gradeid='"+schoolGradeData.gradeVm.gradeId+"'  data-secids='"+schoolGradeData.sectionIds+"'data-isdelete_able='"+schoolGradeData.isDeleteable+"'>"+
			"<td></td>"+			
			"<td data-gradeid='"+schoolGradeData.gradeVm.gradeId+"'	title='"+escapeHtmlCharacters(schoolGradeData.gradeVm.gradeName)+"'>"+schoolGradeData.gradeVm.gradeName+"</td>"+
			"<td data-gradeid='"+schoolGradeData.gradeVm.gradeId+"'	title='"+escapeHtmlCharacters(schoolGradeData.gradeVm.gradeCode)+"'>"+schoolGradeData.gradeVm.gradeCode+"</td>"+
			"<td data-sectionids='"+schoolGradeData.sectionIds+"'	title='"+escapeHtmlCharacters(schoolGradeData.sectionNames)+"'>"+schoolGradeData.sectionNames+"</td>"+
			"<td><div  style='display:inline-flex;padding-left:0px;position:absolute;' ><a  class='tooltip tooltip-top'  id='editSchoolGrade'onclick=editSchoolGradeSetData(&quot;"+schoolGradeData.schoolGradeSectionMapperId+"_"+schoolGradeData.gradeVm.gradeId+"&quot;,&quot;"+schoolGradeData.gradeVm.gradeId+"&quot;,&quot;"+schoolGradeData.sectionIds+"&quot;); > <span class='tooltiptext'>Edit</span><span class='glyphicon glyphicon-edit font-size12'></span></a><a style='margin-left:10px;' class='tooltip tooltip-top'   id='deleteSchoolGrade'	onclick=deleteSchoolGradeSetData(&quot;"+schoolGradeData.schoolGradeSectionMapperId+"_"+schoolGradeData.gradeVm.gradeId+"&quot;); >  <span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"+
		"</tr>";
		return newRow;
};

//bindGrades and sections  to list box on add modal show
var getAllGrades =function(obj){
	
	
	resetFormValidatonForLoc(addFormId,"grades");
	resetFormValidatonForLoc(addFormId,"section");
	
	// bind data to add popup dropdown boxes 
	$(addSchGrdSecModalElements.grade).multiselect('destroy');
	bindGradeToListBox($(addSchGrdSecModalElements.grade),0);
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.grade));		
	refreshMultiselect($(addSchGrdSecModalElements.grade));
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.grade));
	
	//add sections
	$(addSchGrdSecModalElements.section).multiselect('destroy');
	bindSectionToListBox($(addSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.section));		
	refreshMultiselect($(addSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(addSchGrdSecModalElements.section));
	
	//disable grade if already added for this school
	disableGrade(addSchGrdSecModalElements.grade);
	
	
	var isSchoolselected =$('#selectBox_schoolsByLoc').find('option:selected');
	var isSchoolLocselected =$('#selectBox_schoolLocationType').find('option:selected').val();
	if(isSchoolselected==null || isSchoolselected.length==0){
		if(isSchoolLocselected=="NONE"){$('#msgText').text('');$('#msgText').text('Please select school location and school.')}else{$('#msgText').text('');$('#msgText').text('Please select school.')}
		$('#mdlError').modal("show");
		return false;
	}else{
		var noOfRows =$('#schoolGradeTable').dataTable().fnSettings().fnRecordsTotal();
		var noOfGrades =$('#mdlAddSchoolGradeSec #selectBox_grades option').length-1;
		if(noOfRows==noOfGrades){
			$('#msgText').text('');$('#msgText').text('All grades have been added for selected school.')
			$('#mdlAddSchoolGradeSec').modal("hide");
			$('#mdlError').modal("show");
			return false;
		}else{
			var addSchoolName = $(schoolGradePageContext.elementSchool).find('option:selected').text();
			$('#addschgrdsec').text('Add Grade-Section(s) to School : '+addSchoolName);
			$('#mdlAddSchoolGradeSec').modal("show");
		}
	}
	
}

//disable grade in select box if already added for this school
var disableGrade =function(elementGrade){
	mappedGrades =[];gradeIdList=[];var i=0;
/*	$('#schoolGradeTable >tbody>tr').each(function(i,row){		
		//mappedGrades.push( parseInt(row.getAttribute('data-gradeid')));
		rows=($(row).prop('id'));
		console.log(rows);
		var array = rows.split('_');
		var gradeId=array[1]

		
		mappedGrades.push( parseInt(gradeId));
	});*/
	
	var nRows =$("#schoolGradeTable").dataTable().fnGetNodes(); 
	$.each(nRows,function(i,row){		
		var id=$(row).prop('id');
		var gradeId =id.split('_')[1];
		mappedGrades.push( parseInt(gradeId));
	});	
	$(elementGrade).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			gradeIdList.push( parseInt($(option).data('id')));
		});
	});	
	/*var mappedSchool =$('#selectBox_schoolsByLoc').find('option:selected').data("id");*/
	
	$.grep(gradeIdList, function(el) {
		if ($.inArray(el, mappedGrades) != -1){
			var option = $(elementGrade).find('option[value="' +el+ '"]');
			option.prop('disabled', true);
			option.parent('label').parent('a').parent('li').remove();
		}
		i++;
	});
	$(elementGrade).multiselect('refresh');
}

//add grades
var addSchoolGrade =function(event){
	$(".outer-loader").show();
	/*gradeIdList=[];*/
	gradeId='';schoolId='';addSectionsIds=[];
/*	$(addSchGrdSecModalElements.grade).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				gradeIdList.push($(option).data('id'));
		});
	});
	*/
	$(addSchGrdSecModalElements.section).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				addSectionsIds.push($(option).data('id'));
		});
	});	
	gradeId =$('#selectBox_grades').find('option:selected').data("id");
	schoolId =$('#selectBox_schoolsByLoc').find('option:selected').data("id");
	if(addSectionsIds.length==0) addSectionsIds=null;
	var requestData ={
		"schoolID":schoolId,
		"gradeId":gradeId,
		"sectionIds":addSectionsIds
	};
	
	var responseData =customAjaxCalling("schoolgradesectionmapper", requestData, "POST");
	if(responseData!=null && responseData!=""){
		if(responseData[0].response=="shiksha-200"){
			$(responseData).each(function(index,dataObj){
				appendNewRow('schoolGradeTable',getNewRow(dataObj));
				//fnUpdateColumnFilter('#schoolGradeTable',false,[],5,[0,4]);
				fnUpdateDTColFilter('#schoolGradeTable',[],5,[0,4],thLabels);
			});
			addButtonId =$(event.target).data('formValidation').getSubmitButton().data('id');
			if(addButtonId=="btnAddSchoolGradeSecSaveMoreButton"){
				
				$("#mdlAddSchoolGradeSec").find('#successMessage').show();
				$("#mdlAddSchoolGradeSec").find('#success').text(responseData[0].responseMessage);
				$("#mdlAddSchoolGradeSec").find('#successMessage').fadeOut(2000);
				$('#notify').fadeIn(1000);
					
				getAllGrades(this);
			}else{			
				$('#mdlAddSchoolGradeSec').modal("hide");
				$('#success-msg').show();
				$('#showMessage').text(responseData[0].responseMessage);
				$("#success-msg").fadeOut(3000);
				$('#notify').fadeIn(3000);
			}
			$(".outer-loader").hide();	
		}else{
			$(".outer-loader").hide();
			$("#addSchoolGradeSecForm").find('#alertDiv').show();
			$("#addSchoolGradeSecForm").find('#successMessage').hide();		
			$("#addSchoolGradeSecForm").find('#errorMessage').show();
			$("#addSchoolGradeSecForm").find('#exception').show();
			$("#addSchoolGradeSecForm").find('#buttonGroup').show();
			$("#addSchoolGradeSecForm").find('#exception').text(responseData[0].responseMessage);
		}
	}
	$(window).scrollTop(0);
	enableTooltip();
}



//edit school grade section
var editSchoolGradeSetData =function(rowId,gradeId,sectionIds){ 
	
	aEditRowId =rowId;

	editMapperId =rowId.split('_')[0];
	
	resetFormValidatonForLoc(editFormId,"grades");
	resetFormValidatonForLoc(editFormId,"section");
	//editMapperId=rowId;
	
	$(editSchGrdSecModalElements.grade).multiselect('destroy');
	bindGradeToListBox($(editSchGrdSecModalElements.grade),0);
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.grade));		
	refreshMultiselect($(editSchGrdSecModalElements.grade));
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.grade));
	
	//add sections
	$(editSchGrdSecModalElements.section).multiselect('destroy');
	bindSectionToListBox($(editSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.section));		
	refreshMultiselect($(editSchGrdSecModalElements.section));
	setOptionsForMultipleSelect($(editSchGrdSecModalElements.section));
	
	//disable grade if already added for this school
	removeGrade(gradeId,editSchGrdSecModalElements.grade);
	makeSectionSelected(sectionIds,$(editSchGrdSecModalElements.section));
	
	
	var editSchoolName = $(schoolGradePageContext.elementSchool).find('option:selected').text();
	$('#editSchGrdSec').text('Edit Grade-Section(s) to School: '+editSchoolName);
	$('#mdlEditSchoolGradeSec').modal("show");
}
//save edited data
var editSchoolGradeSection =function(){
	
	
	 $('.outer-loader').show();
		setTimeout(function(){
			
		
	editSectionsIds=[];
		
	$(editSchGrdSecModalElements.section).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).data('id')!=undefined && $(option).data('id')!="")
				editSectionsIds.push($(option).data('id'));
		});
	});
	if(editSectionsIds.length==0) editSectionsIds=null;
	var requestData ={
		"schoolGradeSectionMapperId":editMapperId,
		"sectionIds":editSectionsIds
	};

	var responseData =customAjaxCalling("schoolgradesectionmapper", requestData, "PUT");
	if(responseData!=null && responseData!=""){
		if(responseData.response=="shiksha-200"){
			deleteRow("schoolGradeTable",aEditRowId);	
			appendNewRow('schoolGradeTable',getNewRow(responseData));
			//fnUpdateColumnFilter('#schoolGradeTable',false,[],5,[0,4]);
			fnUpdateDTColFilter('#schoolGradeTable',[],5,[0,4],thLabels);
			$('#mdlEditSchoolGradeSec').modal("hide");
			$('#success-msg').show();
			$('#showMessage').text(responseData.responseMessage);
			$("#success-msg").fadeOut(3000);
			$('#notify').fadeIn(3000);
			$(".outer-loader").hide();	
		}else{			
			$("#editSchoolGradeSecForm").find('#alertDiv').show();
			$("#editSchoolGradeSecForm").find('#successMessage').hide();		
			$("#editSchoolGradeSecForm").find('#errorMessage').show();
			$("#editSchoolGradeSecForm").find('#exception').show();
			$("#editSchoolGradeSecForm").find('#buttonGroup').show();
			$("#editSchoolGradeSecForm").find('#exception').text('');
			$("#editSchoolGradeSecForm").find('#exception').text(responseData.responseMessage);
			$(".outer-loader").hide();
		}
	}
	$(window).scrollTop(0);
	enableTooltip();
	$('.outer-loader').hide();
	return;
}, 50);
}

//remove all grades except current edited grade in edit form
var removeGrade = function(selectedGrade,elementGrade){
	$(elementGrade).each(function(index,ele){
		$(ele).find("option").each(function(ind,option){
			if($(option).data('id')!=null){
				var currentId=parseInt($(option).data('id'));
				if(currentId==parseInt(selectedGrade)){
					$(option).prop('selected', true);
				}else{
					$(option).prop('disabled', true);
					$(option).parent('label').parent('a').parent('li').remove();
				}
			}
		});

	});		
	$(elementGrade).multiselect('refresh');
};

//make sections seleted in edit form
var makeSectionSelected =function(selectedIds,elementSec){
	var i=0;
	selectedIds = $.map(selectedIds.split(","), function(el) { return parseInt(el, 10); });
	$.grep(sectionIdList, function(el) {
		if ($.inArray(el, selectedIds) != -1){
			var option = $(elementSec).find('option[value="' +el+ '"]');
			option.prop('selected', true);
		}
		i++;
	});
	$(elementSec).multiselect('refresh');
}







//delete grade from school
var deleteSchoolGradeSetData =function(schoolGradeMappingId){
	aDeleteRowId =schoolGradeMappingId;
	deleteSchoolGradeId =schoolGradeMappingId.split('_')[0];
	
	var schName =$('#selectBox_schoolsByLoc').find('option:selected').text();
	$('#mdlDelSchoolGrade #msgDel').text('');
	$('#mdlDelSchoolGrade #msgDel').text('Do you want to remove selected grade from school "' +schName +'"?');
	var deleteSchoolName = $(schoolGradePageContext.elementSchool).find('option:selected').text();
	$('#delSchGrdSec').text('Delete Grade-Section(s) from School : '+deleteSchoolName);
	$("#mdlDelSchoolGrade").modal().show();
	hideDiv($('#mdlDelSchoolGrade #alertdiv'));
}
var deleteGrade =function(obj){

	$(".outer-loader").show();
	var responseData = customAjaxCalling("schoolgradesectionmapper/"+deleteSchoolGradeId, null, "DELETE");
	if(responseData.response=="shiksha-200"){
		
	
		deleteRow("schoolGradeTable",aDeleteRowId);	
		//fnUpdateColumnFilter('#schoolGradeTable',false,[],5,[0,4]);
		fnUpdateDTColFilter('#schoolGradeTable',[],5,[0,4],thLabels);
		$('#mdlDelSchoolGrade').modal("hide");
		
		$('#success-msg').show();
		$('#showMessage').text(responseData.responseMessage);
		$("#success-msg").fadeOut(3000);
		$('#notify').fadeIn(3000);
	
	} else {
		showDiv($('#mdlDelSchoolGrade #alertdiv'));
		$("#mdlDelSchoolGrade").find('#errorMessage').show();
		$("#mdlDelSchoolGrade").find('#exception').text('');
		$("#mdlDelSchoolGrade").find('#exception').text(responseData.responseMessage);
		$("#mdlDelSchoolGrade").find('#buttonGroup').show();
	}
	$(".outer-loader").hide();
	$(window).scrollTop(0);
}



///////////////////////////
$(function() {
	
	thLabels =['#','Grade  ','Grade Code  ','Section(s)  ','Action'];
	fnSetDTColFilterPagination('#schoolGradeTable',5,[0,4],thLabels);
	
	//setDataTableColFilterPagination('#schoolGradeTable',5,[0,4],false);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#schoolGradeTable',[]);
	});
	$("#tableDiv").show();
	$('#schoolGradeTable').dataTable().fnDraw();
	//setOptionsForMultipleSelect('.search_init');
    setOptionsForMultipleSelect($(schoolGradePageContext.schoolLocationType));
    setOptionsForMultipleSelect($(addSchGrdSecModalElements.section));
    $('#rowDiv1').show();
	initGlblVariables();

	//init bootstrap tooltip
	
	$("#addSchoolGradeSecForm").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields: {
			grades: {
				validators: {
					callback: {
						message: '      ',
						callback: function(value, validator, $field) {
							// Get the selected options
							var options =$(addSchGrdSecModalElements.grade).val();
							return (options !=null);
						}
					}
				}
			},
			section: {
				validators: {
					callback: {
						message: '      ',
						callback: function(value, validator, $field) {
							// Get the selected options
							var options =$(addSchGrdSecModalElements.section).val();
							return (options !=null);
						}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		addSchoolGrade(e);
	});
	
	
	
	$("#editSchoolGradeSecForm").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields: {
			grades: {
				validators: {
					callback: {
						message: '      ',
						callback: function(value, validator, $field) {
							// Get the selected options
							var options =$(editSchGrdSecModalElements.grade).val();
							return (options !=null);
						}
					}
				}
			},
			section: {
				validators: {
					callback: {
						message: '      ',
						callback: function(value, validator, $field) {
							// Get the selected options
							var options =$(editSchGrdSecModalElements.section).val();
							return (options !=null);
						}
					}
				}
			}
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		editSchoolGradeSection(e);
	});

});