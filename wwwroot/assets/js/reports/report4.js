
//select divs
var pageContextDivs4 =new Object();
	pageContextDivs4.report ='#divReport';
	pageContextDivs4.campaign ='#performanceAttendanceVsTestScoreDiv #campaignDiv';
	pageContextDivs4.school ='#performanceAttendanceVsTestScoreDiv #schoolDiv';
	pageContextDivs4.grade ='#performanceAttendanceVsTestScoreDiv #gradeDiv';
	pageContextDivs4.section ='#performanceAttendanceVsTestScoreDiv #sectionDiv';
	pageContextDivs4.subject ='#performanceAttendanceVsTestScoreDiv #subjectDiv';
	pageContextDivs4.source ='#performanceAttendanceVsTestScoreDiv #sourceDiv';
	pageContextDivs4.table ='#tableDiv';
// select location divs
var pageContextLocDivs4 =new Object();	
    pageContextLocDivs4.state='#performanceAttendanceVsTestScoreDiv #stateDiv'
    pageContextLocDivs4.zone='#performanceAttendanceVsTestScoreDiv #zoneDiv'
    pageContextLocDivs4.district='#performanceAttendanceVsTestScoreDiv #districtDiv'

//select elements
var pageContextElements4 =new Object();
	//pageContextElements.report ='#selectBox_report';
	pageContextElements4.campaign ='#selectBox_campaign';
	//pageContextElements.assessment ='#selectBox_Assessment';
	pageContextElements4.school ='#performanceAttendanceVsTestScoreDiv #selectBox_school';
	pageContextElements4.grade ='#performanceAttendanceVsTestScoreDiv #selectBox_grade';
	//pageContextElements.section ='#selectBox_section';
	pageContextElements4.subject ='#performanceAttendanceVsTestScoreDiv #selectBox_subject';
	pageContextElements4.stage ='#performanceAttendanceVsTestScoreDiv #selectBox_stage';
	pageContextElements4.source ='#performanceAttendanceVsTestScoreDiv #selectBox_source';
	pageContextElements4.button ='#viewReport';


var performanceAttendanceVSTestScore=function(){
	displayLocationsOfSurvey($('#performanceAttendanceVsTestScoreDiv #locationDivFilter'));
	hideDiv($('#studentPerformanceDiv'));
	showDiv($('#performanceAttendanceVsTestScoreDiv'));
	showDiv($('#performanceAttendanceVsTestScoreDiv #filters'));
	showDiv($('#locationDivFilter'));
	showDiv($('#viewBtnDiv'));
	selectedCampaign =$(pageContextElements4.campaign).find('option:selected').data('id')
	var reportFilterData =customAjaxCalling("report/filters/"+selectedCampaign, null,'GET');
	console.log(reportFilterData);
	if(reportFilterData.response=="shiksha-200"){
		fnEmptyAll(pageContextElements4.school,pageContextElements4.grade,
					pageContextElements4.subject,pageContextElements4.source,pageContextElements4.stage);
		//fnBindAssessment(pageContextElements.assessment,reportFilterData.assessments);
		fnBindSchools(pageContextElements4.school,reportFilterData.schools);
		fnBindGrades(pageContextElements4.grade,reportFilterData.grades);
		fnBindSubjects(pageContextElements4.subject,reportFilterData.subjects);
		//fnBindSections(pageContextElements.section,reportFilterData.sections);
		fnBindStages(pageContextElements4.stage,reportFilterData.stages);
		fnBindSources(pageContextElements4.source,reportFilterData.sources);
		
	}
	
}