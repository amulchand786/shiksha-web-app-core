
var cAssessmentData=[];
var pageContextElement = new Object();
pageContextElement.grade ="#selectBoxGrade";
pageContextElement.section ="#selectBoxSection";
pageContextElement.school ="#selectBox_schoolHome";
pageContextElement.assessmentStatus ="#selectAssessmentStatus";
pageContextElement.subject ="#selectBoxSubject";
pageContextElement.stage ="#divStage";
pageContextElement.assessmentStartDate ="#startDatePicker";
pageContextElement.assessmentEndtDate ="#endDatePicker";




var getSelectedData =function(elementObj,collector){
	$(elementObj).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				collector.push( parseInt($(option).data('parentid')));
		});
	});
}

var fnInitDatePicker =function(pDatePickerId){
	$(pDatePickerId).datepicker({
		allowPastDates: true,
		momentConfig : {
			culture : 'en', // change to specific culture
			format : 'DD-MMM-YYYY' // change for specific format
		},
	})
}
var genrateRow=function(){
	$(".outer-loader").show();
	$('#teacherLevelReportTableDiv').css('display','block');
	$('#chapterWiseDiv').css('display','block');
	$('#reportfilter').removeClass('in');
	var gradeIds=[];
	var sectionIds=[];
	var subjectIds=[];
	var assessmentStatusIds=[];
	var stageId=null;
	var assessmentStartDate = null;
	var assessmentEndtDate = null;

	$('#selectBoxGrade').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				gradeIds.push( parseInt($(option).data('id')));
		});	
	});
	$('#selectBoxSection').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				sectionIds.push( parseInt($(option).data('id')));
		});	
	});
	$('#selectBoxSubject').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				subjectIds.push( parseInt($(option).data('id')));
		});	
	});
	$('#selectAssessmentStatus').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				assessmentStatusIds.push( parseInt($(option).data('id')));
		});	
	});
	
	
	 stageId =$('input:radio[name=stage]:checked').data('id');
	 assessmentStartDate=$('#startDatePicker').datepicker('getFormattedDate');
	 assessmentEndtDate=$('#endDatePicker').datepicker('getFormattedDate');
	var jsonData={
			 "gradeIds":gradeIds,
			 "sectionIds":sectionIds,
			 "subjectIds":subjectIds,
			 "assessmentStatusIds":assessmentStatusIds,
			 "stageId":stageId,
			 "assessmentStartDate":assessmentStartDate,
			 "assessmentEndtDate":assessmentEndtDate
	}
	$(".outer-loader").show();
	var teacherlevelReportData = reportCustomAjaxCalling("report/teacherlevel",jsonData,"POST");
	
	graphData=teacherlevelReportData;
	teacherlevelReport(teacherlevelReportData);
	
}
var aGraph={};
var teacherlevelReport=function(reportData){
	//$(".outer-loader").hide();
	if(reportData.length ==0){
		$("#viewMetricButton").addClass("disabled");
		
	}
	else{
		$("#viewMetricButton").removeClass("disabled");
		
	}
	deleteAllRow('teacherLevelReportTable');
	$.each(reportData, function(index,teacherReportData) {
		var asmentName=teacherReportData.assessmentName+"_"+teacherReportData.campaignName;
		var pid=teacherReportData.assessmentPlannerId;
		var assessmentMapData={
				"assessmentName":asmentName,
				"assessmentPlannerId":pid
		}
		
		////////////// prepare map for graph///////////
		aGraph[pid]=teacherReportData;		
			
		cAssessmentData.push(assessmentMapData);
		var studentEnroldedTD="";
		var studentAppearedTD="";
		var studentAbsentTD="";
		var studentYetToTakeTD="";
		var studentGTETD="";
		var studentLTETD="";
		if(teacherReportData.numberOfStudentsEnrolled.totalStudent=="0"){
			studentEnroldedTD='<td title="'+teacherReportData.numberOfStudentsEnrolled.totalStudent+'" >'+teacherReportData.numberOfStudentsEnrolled.totalStudent+'</td>';
		}else{
			studentEnroldedTD='<td title="'+teacherReportData.numberOfStudentsEnrolled.totalStudent+'" ><a style="cursor: pointer;" onclick="getStudentDetailData('+teacherReportData.totalNumberOfStudentsEnrolled+');">'+teacherReportData.numberOfStudentsEnrolled.totalStudent+'</a></td>';
			
		}
		if(teacherReportData.totalNumberOfStudentsAppearedInAssessment.totalStudent=="0"){
			studentAppearedTD='<td title="'+teacherReportData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'" >'+teacherReportData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'</td>';
			
		}else{
			studentAppearedTD='<td title="'+teacherReportData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'" ><a style="cursor: pointer;" onclick="getStudentDetailData('+teacherReportData.totalNumberOfStudentsAppearedInAssessmentIds+');">'+teacherReportData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'</a></td>';
			
		}
		
		if(teacherReportData.numberOfStudentsAbsentForAssessment.totalStudent=="0"){
			studentAbsentTD='<td title="'+teacherReportData.numberOfStudentsAbsentForAssessment.totalStudent+'" >'+teacherReportData.numberOfStudentsAbsentForAssessment.totalStudent+'</a></td>';
		}else{
			studentAbsentTD='<td title="'+teacherReportData.numberOfStudentsAbsentForAssessment.totalStudent+'" ><a style="cursor: pointer;" onclick="getStudentDetailData('+teacherReportData.numberOfStudentsAbsentForAssessmentTotalIds+');">'+teacherReportData.numberOfStudentsAbsentForAssessment.totalStudent+'</a></td>';
		}
		if(teacherReportData.numberOfStudentsYetToTakeAssessment.totalStudent=="0"){
			studentYetToTakeTD=	'<td title="'+teacherReportData.numberOfStudentsYetToTakeAssessment.totalStudent+'" >'+teacherReportData.numberOfStudentsYetToTakeAssessment.totalStudent+'</td>';
		}else{
			studentYetToTakeTD=	'<td title="'+teacherReportData.numberOfStudentsYetToTakeAssessment.totalStudent+'" ><a style="cursor: pointer;" onclick="getStudentDetailData('+teacherReportData.numberOfStudentsYetToTakeAssessmentTotalIds+');">'+teacherReportData.numberOfStudentsYetToTakeAssessment.totalStudent+'</a></td>';
		}
		if(teacherReportData.numberOfStudentsScoredGTEQ90.totalStudent=="0"){
			studentGTETD='<td title="'+teacherReportData.numberOfStudentsScoredGTEQ90.totalStudent+'" >'+teacherReportData.numberOfStudentsScoredGTEQ90.totalStudent+'</td>';
		}else{
			studentGTETD='<td title="'+teacherReportData.numberOfStudentsScoredGTEQ90.totalStudent+'" ><a style="cursor: pointer;" onclick="getStudentDetailData('+teacherReportData.numberOfStudentsScoredGTEQ90TotalIds+');">'+teacherReportData.numberOfStudentsScoredGTEQ90.totalStudent+'</a></td>';
		}
		if(teacherReportData.numberOfStudentsScoredLTEQ90.totalStudent=="0"){
			studentLTETD='<td title="'+teacherReportData.numberOfStudentsScoredLTEQ90.totalStudent+'" >'+teacherReportData.numberOfStudentsScoredLTEQ90.totalStudent+'</td>';
		}else{
			studentLTETD='<td title="'+teacherReportData.numberOfStudentsScoredLTEQ90.totalStudent+'" ><a style="cursor: pointer;" onclick="getStudentDetailData('+teacherReportData.numberOfStudentsScoredLTEQ90TotalIds+');">'+teacherReportData.numberOfStudentsScoredLTEQ90.totalStudent+'</a></td>';
		}
		
		var row='';
		row='<tr  id="'+teacherReportData.assessmentId+'">'+
		'<td>'+(index+1)+'</td>'+
		'<td title="'+teacherReportData.assessmentName+'" data-assessmentId="'+teacherReportData.id+'"><a style="cursor: pointer;" onclick="getStudentDataByAssmt('+teacherReportData.assessmentPlannerId+','+teacherReportData.assessmentId+','+teacherReportData.campaignId+','+teacherReportData.schoolId+');">'+teacherReportData.assessmentName+'</a></td>'+
		'<td title="'+teacherReportData.campaignName+'" >'+teacherReportData.campaignName+'</td>'+
		'<td title="'+teacherReportData.status+'" >'+teacherReportData.status+'</td>'+
		'<td title="'+teacherReportData.gradeName+'" >'+teacherReportData.gradeName+'</td>'+
		'<td title="'+teacherReportData.stageName+'" >'+teacherReportData.stageName+'</td>'+
		'<td title="'+teacherReportData.subjectName+'" >'+teacherReportData.subjectName+'</td>'+
		studentEnroldedTD+studentAppearedTD+
		'<td title="'+teacherReportData.percentageOfStudentsAttempted.totalStudent+'" >'+teacherReportData.percentageOfStudentsAttempted.totalStudent+'% </td>'+
			studentAbsentTD+studentYetToTakeTD+studentGTETD+
		'<td title="'+teacherReportData.percentageOfStudentsScoredGTEQ90.totalStudent+'" >'+teacherReportData.percentageOfStudentsScoredGTEQ90.totalStudent+'% </td>'+
		studentLTETD+
		'<td title="'+teacherReportData.percentageOfStudentsScoredLTEQ90.totalStudent+'" >'+teacherReportData.percentageOfStudentsScoredLTEQ90.totalStudent+'% </td>'+
		'<td title="'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_K+'</td>'+
		'<td title="'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_U+'</td>'+
		'<td title="'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_O+'</td>'+
		'<td title="'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_An+'</td>'+
		'<td title="'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+teacherReportData.parameterScoringForAssessmentTotalScoring.scored_Ap+'</td>'+
		'</tr>';
		
		appendNewRow('teacherLevelReportTable', row);
	});
	$(".outer-loader").hide();
}

var wSaveAssessmentId=0;
var wCampaignId=0;
var wSchoolId=0;
var wAssessmentPlannerId=0;

//When click Assessment  Link get student detail
var getStudentDataByAssmt = function(zassessmentPlannerId,zassessmentId,zcampaignId,zschoolId){
	$(".outer-loader").css('display','block');
	$.wait(50);

	wSaveAssessmentId = zassessmentId;
	wCampaignId=zcampaignId;
	wSchoolId=zschoolId;
	wAssessmentPlannerId=zassessmentPlannerId;
	$.wait(30).then(fnGetStudentDataByAssmt);



}

var fnGetStudentDataByAssmt=function(){
	var	vAssessmentData = customAjaxCalling("assessment/report/"+wSaveAssessmentId, null, "GET");
	if(vAssessmentData!=null){
		$("#questionPaperNameX").text(vAssessmentData.assessmentName);
	}
	var studentData = customAjaxCalling("report/assessmentWiseStudentData/"+wAssessmentPlannerId+"/"+wSaveAssessmentId+"/"+wCampaignId+"/"+wSchoolId,null,"POST");

	if(studentData!=null){
		assessmentWiseStudentData(studentData,'tableForStudent');
		$(".outer-loader").css('display','none');

	}else{
		$(".outer-loader").css('display','none');
	}
	$('input:radio[name="viewChapterwiseResult"][value="scoreCard"]').removeAttr('checked');
	$('input:radio[name="viewChapterwiseResult"][value="bloomTaxonomyWise"]').removeAttr('checked');
	$('input:radio[name="viewChapterwiseResult"][value="Yes"]').prop('checked', true).trigger("click");
	$("#tableForPATDiv").show();
	$("#assessmentHomeBloomResultDiv").hide();
	$("#assessmentHomeScorecardDiv").hide();
	fnColSpanIfDTEmpty('tableForStudent',20);
	$('#showStudentDetailPopup').modal();

}

var assessmentWiseStudentData=function(reportData,tableId){
	deleteAllRow(tableId);
	var studentDataVm=reportData[0];
	if(studentDataVm!=null){
	$("#tableForStudent #h15").text(studentDataVm.overAllScore);
	$("#tableForStudent #h16").text(studentDataVm.overAllPercentage);
	$("#tableForStudent #h23").text(studentDataVm.totalTaxnomyScore.Knowledge!=undefined?studentDataVm.totalTaxnomyScore.Knowledge:0);
	$("#tableForStudent #h24").text(studentDataVm.totalTaxnomyScore.Understanding!=undefined?studentDataVm.totalTaxnomyScore.Understanding:0);
	$("#tableForStudent #h25").text(studentDataVm.totalTaxnomyScore.Operation!=undefined?studentDataVm.totalTaxnomyScore.Operation:0);
	$("#tableForStudent #h26").text(studentDataVm.totalTaxnomyScore.Analysis!=undefined?studentDataVm.totalTaxnomyScore.Analysis:0);
	$("#tableForStudent #h27").text(studentDataVm.totalTaxnomyScore.Application!=undefined?studentDataVm.totalTaxnomyScore.Application:0);
	$.each(reportData, function(index,studentData) {	
		var row='';
		row='<tr id="tr'+studentData.consolidatedStatusId+'_'+studentData.isAppearedLate+'">'+
		'<td title="'+studentData.rollNumber+'" >'+studentData.rollNumber+'</td>'+
		'<td title="'+studentData.name+'" >'+studentData.name+'</td>'+
		'<td title="'+studentData.fatherName+'" >'+studentData.fatherName+'</td>'+
		
		'<td title="'+studentData.gender+'" >'+studentData.gender+'</td>'+
		'<td title="'+studentData.age+'" >'+studentData.age+'</td>'+
		'<td title="'+studentData.totalMarksScored+'" >'+studentData.totalMarksScored+'</td>'+
		'<td title="'+studentData.scoreInPercent+'" >'+studentData.scoreInPercent+'</td>'+
		'<td title="'+studentData.toUndergoPA+'" >'+studentData.toUndergoPA+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_K+'" >'+studentData.parameterScoring.scored_K+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_U+'" >'+studentData.parameterScoring.scored_U+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_O+'" >'+studentData.parameterScoring.scored_O+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_An+'" >'+studentData.parameterScoring.scored_An+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_Ap+'" >'+studentData.parameterScoring.scored_Ap+'</td>'+
		
		
		'</tr>';
		
		appendNewRow(tableId, row);
		/*if(studentData.isAppearedLate==true){
			$("#tableForStudent #tr"+studentData.consolidatedStatusId).addClass('appread-late-student');
		}*/
	});
	$(".outer-loader").css('display','none');
	}
}
var zfnGetStudentData=function(reportData,tableId){
	deleteAllRow(tableId);
	
	
	$.each(reportData, function(index,studentData) {	
		var row='';
		var count=index+1;
		row='<tr  id="'+studentData.assessmentId+'">'+
		'<td>'+count+'</td>'+
		
		'<td title="'+studentData.name+'" >'+studentData.name+'</td>'+
		'<td title="'+studentData.fatherName+'" >'+studentData.fatherName+'</td>'+
		/*'<td title="'+studentData.rollNumber+'" >'+studentData.rollNumber+'</td>'+*/
		'<td title="'+studentData.gender+'" >'+studentData.gender+'</td>'+
		'<td title="'+studentData.age+'" >'+studentData.age+'</td>'+
		
		
		'</tr>';
		
		appendNewRow(tableId, row);
		
	});
	
	fnDTWithInitialSort('#'+tableId,1,'asc');
}

//when click on absent student no. Link get details of student
var getStudentDetailData = function(){
	var studentIds=[];
	
	
	$.each( arguments, function( index, argData ) {
		studentIds.push(argData);
	});
	var jsonData = {
		"studentIds":studentIds
	}
	var absentStudentData = customAjaxCalling("student/studentlistByIdsSortByName",jsonData,"POST");
	if(absentStudentData!=null){
		zfnGetStudentData(absentStudentData,'tableForAbsentStudent');
		$('#showAbsentStudentDetailPopup').modal();
	}
	
}


// open modal popup when clicking on yes radio button

/////////////////// Display Modal to search student /////////////////////
$('input:radio[name="chapterwise"]').change(function(){
	if(graphData.length>0) {
	resetFormById('studentSearchForm');
    if($(this).val() == 'Yes'){
    	
    	getAssessmentList();
    	$('#mdlChapterWiseView').modal();
    	$('#startDateSpan').text($('#startDatePicker').datepicker('getFormattedDate'));
    	$('#endDateSpan').text($('#endDatePicker').datepicker('getFormattedDate'));
    	$('#mdlChapterWiseView #selectBox_grade').multiselect('destroy');
    	$('#mdlChapterWiseView #selectBox_school').multiselect('destroy');
    	$('#mdlChapterWiseView #selectBox_section').multiselect('destroy');
    	$('#mdlChapterWiseView #selectBox_questionPaper').multiselect('destroy');
    	$("#selectBox_grade option:selected").removeAttr("selected");
    	$("#selectBox_section option:selected").removeAttr("selected");
    	$("#selectBox_school option:selected").removeAttr("selected");
    	$("#selectBox_questionPaper option:selected").removeAttr("selected");
    	setOptionForSchoolChapter('#selectBox_school');
    	setOptionForGradeChapter('#selectBox_grade');
    	setOptionsForMultipleSelect('#selectBox_section');
    	setOptionForQuestionPaper('#selectBox_questionPaper');
    	$("#studentSearchForm #schoolDiv").hide();
		$("#studentSearchForm #gradeDiv").hide();
		$("#studentSearchForm #divSelectSection").hide();
		
    	//$("#searchStudentBtn").prop("disabled",true);
    	
    }else{
    	$('#divAssessement').css('display','none');
    }
}else{
	$("#msgText").text("There are no result for above search criteria. So chapter wise result will not avalible")
	$('#mdlError').modal();
	fnResetChapterWiseRadioButton();
}
});


// onchange grade section list will come
var getAssessmentList = function(){	
	
	//$('#selectBox_questionPaper').find("option:selected").prop('selected', false);
	//$('#selectBox_questionPaper').multiselect('refresh').multiselect('destroy');
	
	bindToElementId = $('#selectBox_questionPaper');
	bindAssessmenteToListBox(bindToElementId);
	setOptionForQuestionPaper('#selectBox_questionPaper');
	$('.outer-loader').hide();
	
	//fnCollapseMultiselect();
}









var bindAssessmenteToListBox = function(bindToElementId){	
	var gradeIds=[];
	var sectionIds=[];
	var subjectIds=[];
	var assessmentStatusIds=[];
	var stageId=null;
	var assessmentStartDate = null;
	var assessmentEndtDate = null;

	$('#selectBoxGrade').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				gradeIds.push( parseInt($(option).data('id')));
		});	
	});
	$('#selectBoxSection').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				sectionIds.push( parseInt($(option).data('id')));
		});	
	});
	$('#selectBoxSubject').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				subjectIds.push( parseInt($(option).data('id')));
		});	
	});
	$('#selectAssessmentStatus').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				assessmentStatusIds.push( parseInt($(option).data('id')));
		});	
	});
	
	
	 stageId =$('input:radio[name=stage]:checked').data('id');
	 assessmentStartDate=$('#startDatePicker').datepicker('getFormattedDate');
	 assessmentEndtDate=$('#endDatePicker').datepicker('getFormattedDate');
	var jsonData={
			 "gradeIds":gradeIds,
			 "sectionIds":sectionIds,
			 "subjectIds":subjectIds,
			 "assessmentStatusIds":assessmentStatusIds,
			 "stageId":stageId,
			 "assessmentStartDate":assessmentStartDate,
			 "assessmentEndtDate":assessmentEndtDate
	}

	var teacherlevelReportData = customAjaxCalling("teacherlevelReport/assessment",jsonData,"POST");
	

	if(teacherlevelReportData!=null || teacherlevelReportData!=""){
		$select	=bindToElementId;
		$select.html('');
		$select.append('<option data-id="-1" > All</option>');
		$.each(teacherlevelReportData,function(index,obj){
			$select.append('<option data-id="' + obj.assessmentId + '"data-name="' + obj.assessmentName+ '">' +obj.assessmentName+ '</option>');
		});
	}

}

var setOptionForQuestionPaper=function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onDropdownHide: function(event) {
				var fAssessmentIds=[];
				var asmentId=$('#selectBox_questionPaper').find("option:selected").data('id');
				if(asmentId=="-1"){
					$('#selectBox_questionPaper').each(function(index,ele){
						$(ele).find("option:not(:selected)").each(function(ind,option){
							if($(option).val()!="multiselect-all")
								fAssessmentIds.push( parseInt($(option).data('id')));
						});	
					});
				}
				else{
				$('#selectBox_questionPaper').each(function(index,ele){
					$(ele).find("option:selected").each(function(ind,option){
						if($(option).val()!="multiselect-all")
							fAssessmentIds.push( parseInt($(option).data('id')));
					});	
				});
				}
				if(fAssessmentIds.length>0){
					getSchoolList(fAssessmentIds);
					$("#studentSearchForm #schoolDiv").show();
					$("#studentSearchForm #gradeDiv").hide();
					$("#studentSearchForm #divSelectSection").hide();
					$('#studentSearchForm').data('formValidation') .updateStatus('schoolName', 'NOT_VALIDATED');
					
					//$('#studentSearchForm').formValidation('revalidateField', 'schoolName');
				}else{
					$("#studentSearchForm #schoolDiv").hide();
					$("#studentSearchForm #gradeDiv").hide();
					$("#studentSearchForm #divSelectSection").hide();
					}
				
				
			} 
		});

	});
	fnCollapseMultiselect();


	}
var setOptionForSchoolChapter=function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onDropdownHide: function(event) {
				var gSchoolId = parseInt($("#selectBox_school").find('option:selected').data('id'));
				
				if($("#selectBox_school").find('option:selected').length>0){
					getGradeList();
					$("#studentSearchForm #gradeDiv").show();
					$("#studentSearchForm #divSelectSection").hide();
					$('#studentSearchForm').data('formValidation') .updateStatus('gradeName', 'NOT_VALIDATED');
					
					//$('#studentSearchForm').formValidation('revalidateField', 'gradeName');
				
				}else{
					$("#studentSearchForm #gradeDiv").hide();
					$("#studentSearchForm #divSelectSection").hide();
				}
			} 
		});

	});
	fnCollapseMultiselect();


	}

var setOptionForGradeChapter=function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onDropdownHide: function(event) {
				var hGradeId = parseInt($("#selectBox_grade").find('option:selected').data("id"));
				if($("#selectBox_grade").find('option:selected').length>0){
					getSectionList();
					$("#studentSearchForm #divSelectSection").show();
					$('#studentSearchForm').data('formValidation') .updateStatus('sectionName', 'NOT_VALIDATED');
					//$('#studentSearchForm').formValidation('revalidateField', 'sectionName');
					
				}else{
					$("#studentSearchForm #divSelectSection").hide();
				}
			} 
		});

	});
	fnCollapseMultiselect();


	}

var getSchoolList=function(gAssessmentIds){
	$('#selectBox_school').find("option:selected").prop('selected', false);
	$('#selectBox_school').multiselect('refresh').multiselect('destroy');
	bindToElementId = $('#selectBox_school');
	bindSchoolByQuestionPaperToListBox(bindToElementId,gAssessmentIds);
	setOptionForSchoolChapter('#selectBox_school');
	$('.outer-loader').hide();
	
	
}

var bindSchoolByQuestionPaperToListBox=function(bindToElementId,fAssessmentIds){
	var jsonData={
			"assementIds":fAssessmentIds
	}
	var schools = customAjaxCalling("school/getSchoolDetailByAssessmentIds", jsonData,"POST");
	if(schools!=null || schools!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(schools,function(index,obj){
			$select.append('<option data-id="' + obj.id + '"data-schoolName="' + obj.schoolName+ '">' +obj.schoolName+ '</option>');
		});
	}
}
///////////////////////////// search student by id or name and father name
var getGradeList=function(){
	$('#selectBox_grade').find("option:selected").prop('selected', false);
	$('#selectBox_grade').multiselect('refresh').multiselect('destroy');
	var fSchoolId = parseInt($("#selectBox_school").find('option:selected').data('id'));
	bindToElementId = $('#selectBox_grade');
	bindGradeBySchoolToListBox(bindToElementId,fSchoolId);
	setOptionForGradeChapter('#selectBox_grade');
	$('.outer-loader').hide();
}



var bindGradeBySchoolToListBox=function(bindToElementId,SchoolDataId){
	
	var grades = customAjaxCalling("gradelist/"+SchoolDataId, null,"GET");
	if(grades!=null || grades!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(grades,function(index,obj){
			$select.append('<option data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '">' +obj.gradeName+ '</option>');
		});
	}


}


var getSectionList = function(){	
	
	$('#selectBox_section').find("option:selected").prop('selected', false);
	$('#selectBox_section').multiselect('refresh').multiselect('destroy');
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data("id"));
	var sSchoolId = parseInt($("#selectBox_school").find('option:selected').data('id'));
	bindToElementId = $('#selectBox_section');
	$('.outer-loader').show();
	bindSectionBySchoolAndGradeToListBox(bindToElementId,sSchoolId,gradeId,0);
	setOptionsForMultipleSelect('#selectBox_section');
	$('.outer-loader').hide();
	$('#divSelectSection').show();
	
	//fnCollapseMultiselect();
}

var bindSectionBySchoolAndGradeToListBox = function(bindToElementId,selectedSchoolId,selectedGradeId,selectedId)
{	
	json ={
			"schoolId": selectedSchoolId,
			"gradeId": selectedGradeId
	}
	var sections = customAjaxCalling("sectionlist", json,"POST");
	if(sections!=null || sections!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(sections,function(index,obj){
			$select.append('<option data-id="' + obj.sectionId + '"data-sectionname="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
		});
	}

}




///////////////////////// enable disable Yes/No chapter wise report button
var fnResetChapterWiseRadioButton=function(){
	resetFormById('studentSearchForm');
	$('#tableDivForSearchStudentDetail').hide();
	deleteAllRow('tableSearchStudentDetail');
	$('input[name="chapterwise"]').prop('checked', false);
	var chapterwiseBtnValue="No";
	$("input[name=chapterwise][value=" + chapterwiseBtnValue + "]").prop('checked', true);
}
//////////////////////// switch to seearch by name and father name //////////////////////////
var fnUncheckORBtn=function(){
$('#studentId').val("");
$('#studentSearchForm').data('formValidation').updateStatus('studentId', 'VALID');
$('#studentSearchForm').formValidation('revalidateField', 'studentName')
.formValidation('revalidateField', 'fatherName');
};
////////////////////////// Switch to search by student Id ///////////////////////
var fnCheckORBtn=function(){
	$('#studentName').val("");
	$('#fatherName').val("");
	$('#studentSearchForm').data('formValidation').updateStatus('studentName', 'VALID');
	$('#studentSearchForm').data('formValidation').updateStatus('fatherName', 'VALID');
	$('#studentSearchForm').formValidation('revalidateField', 'studentId');
	

};


/////////////////////////// Search student by name and father name or student id//////////////////////////
var fnGetStudentData=function(){
	var studentName=$('#studentName').val();
	var fatherName=$('#fatherName').val();
	var studentId=$('#studentId').val();
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId = parseInt($("#selectBox_section").find('option:selected').data('id'));
	var schoolId = parseInt($("#selectBox_school").find('option:selected').data('id'));
	var assessmentId=$('#selectBox_questionPaper').find('option:selected').data("id");
	var zassessmentIds=[];
	if(assessmentId==-1){
		$('#selectBox_questionPaper').each(function(index,ele){
			$(ele).find("option:not(:selected)").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					zassessmentIds.push( parseInt($(option).data('id')));
			});	
		});

	}
	else{
		zassessmentIds.push(assessmentId);
	}
	
	var jsonData={
			"rollNumber":studentId,
			"fatherName":fatherName,
			"name":studentName,
			"schoolId":schoolId,
			"gradeId":gradeId,
			"sectionId":sectionId,
			"assessmentIds":zassessmentIds
	}
	var studentDetailData=	customAjaxCalling("student/search", jsonData, "POST");
	
	var validData=studentDetailData!=null?studentDetailData.length>0?true:false:false;
	if(validData){
		var student=studentDetailData[0];
		$("#roolNumberMsg").hide();
		   $('#resultStudentId').text(student.rollNumber);
			$('#resultStudentName').text(student.name);
			$('#resultFatherName').text(student.fatherName);
			$('#resultGrade').text(student.gradeName);

			var jsonReportData={
					"schoolId":schoolId,
					"studentAcademicYearSchoolGradeSectionMapperId":student.studentAcademicYearSchoolGradeSectionMapperId,
					"assessmentIds":zassessmentIds,
					"studentId":student.studentId
			}


			var zStudentReportData=customAjaxCalling("report/studentTeacherLevelReportData", jsonReportData, "POST");

			if(zStudentReportData!=null){
				createStudentlevelReport(zStudentReportData);
			}
			$('#mdlStudentLevelReportDetailView').modal();
			$('#chapterwiseResult input:radio[name=chapterwises][value="scoreCard"]').removeAttr('checked');
			$('#chapterwiseResult input:radio[name=chapterwises][value="bloomTaxonomyWise"]').removeAttr('checked');
			$('#chapterwiseResult input:radio[name=chapterwises][value="Yes"]').prop('checked', true);

		}else{
		$("#roolNumberMsg").show();
		$("#roolNumberMsg").fadeOut(2500);
	}
	 $(".outer-loader").css('display','none');
	
	

};

/////////////////////////// Create search student table ///////////////////////////////
var fnGenrateStudentRow=function(studentDetailData){
	var schoolId = parseInt($("#selectBox_school").find('option:selected').val());
	$('#tableDivForSearchStudentDetail').show();
	deleteAllRow('tableSearchStudentDetail');
	if(studentDetailData!=null){
		
		$.each(studentDetailData, function(index,student) {
			var row='';
			row='<tr data-id="'+student.studentAcademicYearSchoolGradeSectionMapperId+'">'+
			'<td></td>'+
			'<td title="'+student.name+'" ><a style="cursor: pointer;" onclick="fnGetStudentLevelReport('+student.studentAcademicYearSchoolGradeSectionMapperId+','+schoolId+','+student.studentId+')">'+student.name+'</a></td>'+
			'<td title="'+student.rollNumber+'" >'+student.rollNumber+'</td>'+
			'<td title="'+student.fatherName+'" >'+student.fatherName+'</td>'+
			'<td title="'+student.motherName+'" >'+student.motherName+'</td>'+
			'<td title="'+student.gender+'" >'+student.gender+'</td>'+
			'<td title="'+student.gradeName+'" >'+student.gradeName+'</td>'+
			'<td title="'+student.sectionName+'" >'+student.sectionName+'</td></tr>';
       appendNewRow('tableSearchStudentDetail', row);

		});
	}
}

var fnActivatePanById=function(tabId,tabPanelId){
	$("#mdlViewSchoolMetrics .active").each(function(index,ele){
		$(ele).removeClass('active');
	});
	$(tabId).addClass('active');
	$(tabPanelId).addClass('active');
	$(tabId).trigger('click');
	
}

var fnViewMetrics=function(){
	if(graphData.length>0){
		fnActivatePanById("#aPerformance","#metricPerformance");
		$(".outer-loader").show();
		bindAssessmentForGraph();
		$.wait(50).then(studentPerformanceAssessmentWiseInIt).then(studentPerformanceGenderWiseInIt).then(studentPerformanceGenderWiseTaxonomyInIt).then(showMetricsReport);
	}
}
var showMetricsReport=function(){
	$('#mdlprojectLevelMetrics').find(".modal-body").scrollTop(0);
	$("#mdlprojectLevelMetrics").modal();
	$("#graphDataDiv").show();
	$(".outer-loader").hide();
	
}

var bindAssessmentForGraph=function(){
	var bindToElementDropId = $('#selectBox_zAssessment');
	$(bindToElementDropId).multiselect('refresh').multiselect('destroy');
	$select	=bindToElementDropId;
	$select.html('');
	$.each(cAssessmentData,function(index,obj){
		$select.append('<option value="' + obj.assessmentPlannerId + '" selected="selected" data-id="' + obj.assessmentPlannerId + '" data-name="' + obj.assessmentName+ '">' +obj.assessmentName+ '</option>');

	});
	
	setOptionForGraphAssessment(bindToElementDropId);
}
var planIds=[];
/////////////////// Set option for assessment wise dropdown ////////////////////////
var setOptionForGraphAssessment=function(){
	
	$(arguments).each(function(index,arg){
		//$(arg).multiselect('refresh');
		$(arg).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onDropdownHide: function(event) {
				planIds=[];
				$('#selectBox_zAssessment').each(function(index,ele){
					$(ele).find("option:selected").each(function(ind,option){
						if($(option).val()!="multiselect-all")
							planIds.push( parseInt($(option).data('id')));
					});	
				});
				
				$("#graphDataDiv").hide();
			} 
		});

	});
	fnCollapseMultiselect();


	}

//////////////////Show Assessmentwise graph//////////////////
var fnShowAssessmentWiseGraph=function(){
	graphData=[];
	$.each(planIds,function(index,objData){
		var cAssessmentPlanId=objData;
		var gObj=	aGraph.hasOwnProperty(cAssessmentPlanId)==true?aGraph[cAssessmentPlanId]:"";
		if(gObj!=""){
			graphData.push(gObj);
		}	
	});
	if(graphData.length>0){
		$(".outer-loader").show();
		$.wait(50).then(studentPerformanceAssessmentWiseInIt).then(studentPerformanceGenderWiseInIt).then(studentPerformanceGenderWiseTaxonomyInIt).then(showMetricsReport);

	}

}

var fnPrepareAssessmentWiseGraphDataOnModalClose=function(){
	graphData=[];
	$.each(cAssessmentData,function(index,objData){
		var cAssessmentPlanId=objData.assessmentPlannerId;
		var gObj=	aGraph.hasOwnProperty(cAssessmentPlanId)==true?aGraph[cAssessmentPlanId]:"";
		if(gObj!=""){
			graphData.push(gObj);
		}	
	});
	

}

////////////////////////////////Get student level report /////////////////////////////////////

var fnGetStudentLevelReport=function(studentAcademicYearSchoolGradeSectionMapperId,schoolId,studentId){/*
	var assessmentId=$('#selectBox_questionPaper').find('option:selected').data("id");
	var assessmentIds=[];
	if(assessmentId==-1){
		$('#selectBox_questionPaper').each(function(index,ele){
			$(ele).find("option:not(:selected)").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					assessmentIds.push( parseInt($(option).data('id')));
			});	
		});

	}
	else{
		assessmentIds.push(assessmentId);
	}
	var studentData=customAjaxCalling("student/getStudentDataByStudentAcademicYearSchoolGradeSectionMapper/"+studentAcademicYearSchoolGradeSectionMapperId+"/"+studentId, null, "POST");
	if(studentData!=null){
		$('#resultStudentId').text(studentData.rollNumber);
		$('#resultStudentName').text(studentData.name);
		$('#resultFatherName').text(studentData.fatherName);
		$('#resultGrade').text(studentData.gradeName);

		var jsonData={
				"schoolId":schoolId,
				"studentAcademicYearSchoolGradeSectionMapperId":studentAcademicYearSchoolGradeSectionMapperId,
				"assessmentIds":assessmentIds,
				"studentId":studentData.studentId
		}


		var studentDetailData=customAjaxCalling("report/studentTeacherLevelReportData", jsonData, "POST");

		if(studentDetailData!=null){
			createStudentlevelReport(studentDetailData);
		}
		$('#mdlStudentLevelReportDetailView').modal();
		$('#chapterwiseResult input:radio[name=chapterwises][value="scoreCard"]').removeAttr('checked');
		$('#chapterwiseResult input:radio[name=chapterwises][value="bloomTaxonomyWise"]').removeAttr('checked');
		$('#chapterwiseResult input:radio[name=chapterwises][value="Yes"]').prop('checked', true);

	}

*/}


/////////////////////////////////////////////////////// Student level report ////////////////////


var createStudentlevelReport=function(studentlevelReportData){
	deleteAllRow('studentLevelReportTable');
	$.each(studentlevelReportData, function(index,studentReportData) {	
		appendNewRow('studentLevelReportTable',getNewRow(studentReportData));
		//CR-653 implementation
		/*if(studentReportData.isAppearedLate==true){
			$("#studentLevelReportTable #tr"+studentReportData.consolidatedStatusId).addClass('appread-late-student');
		}*/
	});
	$("#displayResult").show();
	$("#rowDiv2").show();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();
	$('#studentLevelReportTable').show();
	$('#reportfilter').removeClass('in');
	
}

var getNewRow =function(studentReportData){
	var maxMarks=studentReportData.totalMarks;
	var totalMarks=studentReportData.totalMarksScored==null?"":studentReportData.totalMarksScored+"/"+maxMarks;
	var percentageScore=studentReportData.percentageScore==-1?"":studentReportData.percentageScore+"%";
	var toUnderGoPa = (studentReportData.toUnderGoPA==null)?"" 
														: (studentReportData.toUnderGoPA==true)?studentReportData.toUnderGoPA="YES" 
															: studentReportData.toUnderGoPA="NO";
	var knowledgeScore=studentReportData.parameterScoringForAssessmentVm.scored_K==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_K;
	var understandingScore=studentReportData.parameterScoringForAssessmentVm.scored_U==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_U;
	var operationScore=studentReportData.parameterScoringForAssessmentVm.scored_O==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_O;
	var analysisScore=studentReportData.parameterScoringForAssessmentVm.scored_An==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_An;
	var applicationScore=studentReportData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_Ap;

	var	row='<tr id="tr'+studentReportData.consolidatedStatusId+'_'+studentReportData.isAppearedLate+'">'+
	"<td title='"+studentReportData.assessmentName+"' >"+studentReportData.assessmentName+"</td>"+
	"<td title='"+studentReportData.campaignName+"' >"+studentReportData.campaignName+"</td>"+
	"<td title='"+studentReportData.statusOfCompletion+"' >"+studentReportData.statusOfCompletion+"</td>"+
	"<td title='"+studentReportData.stageName+"' >"+studentReportData.stageName+"</td>"+
	"<td title='"+studentReportData.subjectName+"' >"+studentReportData.subjectName+"</td>"+
	"<td title='"+totalMarks+"' >"+totalMarks+"</td>"+
	"<td title='"+percentageScore+"' >"+percentageScore+"</td>"+
	"<td title='"+toUnderGoPa+"' >"+toUnderGoPa+"</td>"+
	"<td title='"+knowledgeScore+"' >"+knowledgeScore+"</td>"+
	"<td title='"+understandingScore+"' >"+understandingScore+"</td>"+
	"<td title='"+operationScore+"' >"+operationScore+"</td>"+
	"<td title='"+analysisScore+"' >"+analysisScore+"</td>"+
	"<td title='"+applicationScore+"' >"+applicationScore+"</td>"+
	"</tr>";
	return row;
	};
	
	$('#chapterwiseResult input:radio[name=chapterwises]').on('click', function() {
		$(".outer-loader").show();
		$.wait(200).then(fnShowChapterWiseStudentResult); 

	});
	
	
	var fnShowChapterWiseStudentResult=function(){
		var elem=$('#chapterwiseResult input:radio[name=chapterwises]:checked').val();
		fnShowSpin();
		if(elem == 'Yes'){
			$("#rowDiv3").hide();
			$("#rowDiv2").show();
			$("#rowDiv4").hide();
			$(".outer-loader").hide();
			$('#studentLevelReportTable').show();
		} else if(elem == 'scoreCard'){
			genrateScoreCardRow();
			$("#rowDiv2").hide();
			$('#studentLevelReportTable').hide();
			$(".outer-loader").hide();

		}else{

			genrateBloomTaxonomyRow();
			$(".outer-loader").hide();
		}

		$(".outer-loader").hide();
	}		
	/*$('#chapterwiseResult input:radio[name=chapterwises]').change(function(){
		fnShowSpin();
		if($(this).val() == 'Yes'){
				$("#rowDiv3").hide();
				$("#rowDiv2").show();
				$("#rowDiv4").hide();
				$('#studentLevelReportTable').show();
		    } else if($(this).val() == 'scoreCard'){
		    	genrateScoreCardRow();
		    	$("#rowDiv2").hide();
		    	$('#studentLevelReportTable').hide();
		    	
		    }else{
		    	
		    	genrateBloomTaxonomyRow();
		    	
		    }
		});
	
	*/
	
	////////////////////////// score card////////////////////////
	
	var fnShowSpin=function()
	{
		$(".outer-loader").show();
	}
	var fnShowSpin=function()
	{
		$(".outer-loader").hide();
	}
	
	var genrateScoreCardRow=function(){
		$('#scoreBoardTableDiv').css('display','block');
		var studentlevelScoreCardData = customAjaxCalling("report/studentTeacherLevelScoreCard",null,"POST");
	   $("#scoreBoardHeader").children("tr").remove();
		if(studentlevelScoreCardData.maxNoOfRows!=0){
		$("#scoreBoardHeader").append(createTableForScoreCard(studentlevelScoreCardData.maxNoOfRows));
	   }else {
			$("#scoreBoardHeader").append(createTableForScoreCard(0));
		}
		createStudentlevelScoreCardReport(studentlevelScoreCardData.studentLevelReportVm);
		}

	var createTableForScoreCard=function(maxNumberOfColumns){
		var noOfColmns=maxNumberOfColumns+6;
		var rowSpan='';
			for(var i=1;i<=maxNumberOfColumns;i++){
				rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
			}
		
		var	thead="<tr>"+
		"<th  class='nosort' rowspan='3'><p>Question Paper Name</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Campaign Name</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
		"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
		"</tr>"+
		"<tr>"+
		"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise scores</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Marks Scored</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Total Marks</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Capterwise % score</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Total marks scored</p></th>"+
		"<th  class='nosort' rowspan='2'><p>% score</p></th>"+
		"</tr>"+
		"<tr>"+
		rowSpan
		"</tr>";
		return thead;
	};



	var createStudentlevelScoreCardReport=function(studentlevelScoreCardData){
		$("#scoreBoardTable tbody tr").remove();
		$.each(studentlevelScoreCardData, function(index,studentScoreCardData) {	
			$("#scoreBoardTable tbody").append(getScoreCardNewRow(studentScoreCardData));
			if(studentScoreCardData.isAppearedLate==true){
				$("#scoreBoardTable #tr"+studentScoreCardData.consolidatedStatusId).addClass('appread-late-student');
				$("#scoreBoardTable #trchap"+studentScoreCardData.consolidatedStatusId).addClass('appread-late-student');
				
			}
			
		});
		$("#rowDiv2").hide();
		$("#rowDiv3").show();
		$("#rowDiv4").hide();
		$('#scoreBoardTable').show();
	}


	var getScoreCardNewRow =function(studentScoreCardData){
		var maxMarks=studentScoreCardData.totalMarks;
		var totalMarks=studentScoreCardData.totalMarksScored==null?"":studentScoreCardData.totalMarksScored+"/"+maxMarks;
		var percentageScore="";
		if(studentScoreCardData.totalMarksScored!=null){
			percentageScore=studentScoreCardData.percentageScore==-1?"":studentScoreCardData.percentageScore+"%";
		}
		
		var rowSpan='';
		var count=studentScoreCardData.numberOfChapters;
		$.each(studentScoreCardData.chapterReportVmList, function(index,chapterVm) {	
			
			if(index > 0){
				rowSpan+= '<tr id="trchap'+studentScoreCardData.consolidatedStatusId+'">';
				}
			rowSpan +="<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
			
			if(jQuery.isEmptyObject(chapterVm.questionScore[0])==false){
			$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
				
				if(qScoreMap == -1){
					qScoreMap="";
				}
				 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
			});
			}else{
				
				 rowSpan +="<td title=''></td>";
			}
			var chapterWisePercent=chapterVm.chapterWisePercentage==-1?"":chapterVm.chapterWisePercentage+"%";
			rowSpan+="<td title='"+chapterVm.chapterWiseScore+"' >"+chapterVm.chapterWiseScore+"</td>"+
			"<td title='"+chapterVm.chapterWiseTotalMarks+"' >"+chapterVm.chapterWiseTotalMarks+"</td>"+
			"<td title='"+chapterWisePercent+"' >"+chapterWisePercent+"</td>";
			
			if(index==0){
			rowSpan+= "<td title='"+totalMarks+"' rowspan="+count+">"+totalMarks+"</td>"+
			"<td title='"+percentageScore+"' rowspan="+count+">"+percentageScore+"</td></tr>";
				}else{
				rowSpan+="</tr>";
			}
			
		});
		var	row='<tr  id="tr'+studentScoreCardData.consolidatedStatusId+'">'+
		"<td title='"+studentScoreCardData.assessmentName+"' rowspan="+count+">"+studentScoreCardData.assessmentName+"</td>"+
		"<td title='"+studentScoreCardData.campaignName+"' rowspan="+count+">"+studentScoreCardData.campaignName+"</td>"+
		"<td title='"+studentScoreCardData.statusOfCompletion+"' rowspan="+count+">"+studentScoreCardData.statusOfCompletion+"</td>"+
		"<td title='"+studentScoreCardData.stageName+"' rowspan="+count+">"+studentScoreCardData.stageName+"</td>"+
		rowSpan;
		return row;
		};
		
	//////////////////////////////////////////Boolm taxonomy //////////////////////////////

		var genrateBloomTaxonomyRow=function(){
			$("#rowDiv3").hide();
			$('#rowDiv4').css('display','block');
			
			var studentlevelBloomTaxonomyData = customAjaxCalling("report/studentTeacherLevelbloomTaxonomy",null,"POST");
			//deleteAllRow('bloomTaxonomyTable');
			$("#bloomTaxonomyHeader").children("tr").remove();
			if(studentlevelBloomTaxonomyData.maxNoOfRows!=0){
			$("#bloomTaxonomyHeader").append(createTableForBloomTaxonomy(studentlevelBloomTaxonomyData.maxNoOfRows));
			}else{
				$("#bloomTaxonomyHeader").append(createTableForBloomTaxonomy(1));
			}createStudentlevelBloomTaxonomyReport(studentlevelBloomTaxonomyData.studentLevelReportVm);
			
				
				
			}

			
			
		var createTableForBloomTaxonomy=function(maxNumberOfColumns){
			var noOfColmns=maxNumberOfColumns+5;
			var rowSpan='';
				for(var i=1;i<=maxNumberOfColumns;i++){
					rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
				}
			
			var	thead="<tr>"+
			"<th  class='nosort' rowspan='3'><p>Question Paper Name</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Campaign Name</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
			"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p> Learning Dimension Wise </p></th>"+
			"</tr>"+
			"<tr>"+
			"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise Parameter Score</p></th>"+
			"<th  class='nosort' colspan='5'><p>Overall Parameter Score</p></th>"+
			
			"</tr>"+
			"<tr>"+
			rowSpan+
			"<th  class='nosort' ><p>K</p></th>"+
			"<th  class='nosort' ><p>U</p></th>"+
			"<th  class='nosort' ><p>O</p></th>"+
			"<th  class='nosort' ><p>AN</p></th>"+
			"<th  class='nosort' ><p>AP</p></th>"+
			"</tr>";
			return thead;
		};	


		var createStudentlevelBloomTaxonomyReport=function(studentlevelBloomTaxonomyData){
			$("#bloomTaxonomyTable tbody tr").remove();
			$.each(studentlevelBloomTaxonomyData, function(index,studentBloomTaxonomyData) {	
				$("#bloomTaxonomyTable tbody").append(getBloomtaxonomyNewRow(studentBloomTaxonomyData));
				if(studentBloomTaxonomyData.isAppearedLate==true){
					$("#bloomTaxonomyTable #tr"+studentBloomTaxonomyData.consolidatedStatusId).addClass('appread-late-student');
					$("#bloomTaxonomyTable #trchap"+studentBloomTaxonomyData.consolidatedStatusId).addClass('appread-late-student');
					
				}
				
			});
			$("#rowDiv2").hide();
			$("#rowDiv3").hide();
			$("#rowDiv4").show();
			$('#bloomTaxonomyTable').show();
		}

			

		var getBloomtaxonomyNewRow =function(studentBloomTaxonomyData){
			var knowledgeScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K;
			var understandingScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U;
			var operationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O;
			var analysisScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An;
			var applicationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap;
			var rowSpan='';
			var count=studentBloomTaxonomyData.numberOfChapters;
			
			rowSpan +="<td title='' ></td>";
			if(studentBloomTaxonomyData.taxonomyHeaderList.length !=0 ){
			$.each(studentBloomTaxonomyData.taxonomyHeaderList, function(indexNumber,taxonomyHeader) {	
			
			 rowSpan +="<td title='"+taxonomyHeader+"' >"+taxonomyHeader+"</td>";
			});
			}else{
				 rowSpan +="<td title='' ></td>";
			}
			$.each(studentBloomTaxonomyData.taxonomyWeightAge, function(indexNumber,taxonomyWeightage) {	
				 rowSpan +="<td title='"+taxonomyWeightage+"' >"+taxonomyWeightage+"</td>";
				});
			rowSpan+="</tr>";
			$.each(studentBloomTaxonomyData.chapterReportVmList, function(index,chapterVm) {	
				rowSpan +="<tr id='trchap"+studentBloomTaxonomyData.consolidatedStatusId+"'>"+ "<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
				if(jQuery.isEmptyObject(chapterVm.questionScore[0])==false){
				$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
					if(qScoreMap == -1){
						qScoreMap="";
					}
					 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
				});
				}else{
					
					 rowSpan +="<td title=''></td>";
				}
				if(index==0){
				rowSpan+= "<td title='"+knowledgeScore+"' rowspan="+count+" >"+knowledgeScore+"</td>"+
				"<td title='"+understandingScore+"' rowspan="+count+">"+understandingScore+"</td>"+
				"<td title='"+operationScore+"' rowspan="+count+">"+operationScore+"</td>"+
				"<td title='"+analysisScore+"' rowspan="+count+">"+analysisScore+"</td>"+
				"<td title='"+applicationScore+"' rowspan="+count+">"+applicationScore+"</td>"+
				"</tr>";
					}else{
					rowSpan+="</tr>";
				}
				
			});
			var	row='<tr  id="tr'+studentBloomTaxonomyData.consolidatedStatusId+'">'+
			"<td title='"+studentBloomTaxonomyData.assessmentName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.assessmentName+"</td>"+
			"<td title='"+studentBloomTaxonomyData.campaignName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.campaignName+"</td>"+
			"<td title='"+studentBloomTaxonomyData.statusOfCompletion+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.statusOfCompletion+"</td>"+
			"<td title='"+studentBloomTaxonomyData.stageName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.stageName+"</td>"+
			rowSpan;
			return row;
			};
//////////////////////////////////////////
			$('#showStudentDetailPopup input:radio[name=viewChapterwiseResult]').on('click', function() {
				$(".outer-loader").show();
				$.wait(200).then(fnShowQuestionPaperWiseStudentResult); 

			});		
			
			
			
			var fnShowQuestionPaperWiseStudentResult=function(){
				var elemValue=$('#showStudentDetailPopup input:radio[name=viewChapterwiseResult]:checked').val();
				fnClearCustomSearch(['.r5','.r4','.r3']);
				if(elemValue == 'Yes'){
					$("#tableForPATDiv").show();
					$("#chapterWiseRadioButtonsDiv").show();
					$("#assessmentHomeBloomResultDiv").hide();
					$("#assessmentHomeScorecardDiv").hide();
					$(".outer-loader").hide();
				} else if(elemValue == 'scoreCard'){
					getStudentScoreCardByAssmt(wAssessmentPlannerId,wSaveAssessmentId,wCampaignId,wSchoolId);
					$("#tableForPATDiv").hide();
					$("#assessmentHomeBloomResultDiv").hide();
					$("#assessmentHomeScorecardDiv").show();
					$(".outer-loader").hide();
				}else{
					getStudentBloomTaxonomyByAssmt(wAssessmentPlannerId,wSaveAssessmentId,wCampaignId,wSchoolId);
					$("#tableForPATDiv").hide();
					$("#assessmentHomeBloomResultDiv").show();
					$("#assessmentHomeScorecardDiv").hide();
					$(".outer-loader").hide();
				}
				$(".outer-loader").hide();
			};


/////////////////////// Create  Assessment wise scorecard Data //////////////////////////

var getStudentScoreCardByAssmt = function(lAssessmentPlannerId,assessmentId,campaignId,schoolId){
	
	$(".outer-loader").css('display','block');
	var studentData = reportCustomAjaxCalling("report/assessmentWiseScoreCard/"+lAssessmentPlannerId+"/"+assessmentId+"/"+campaignId+"/"+schoolId,null,"POST");
	var studentFooterData = reportCustomAjaxCalling("report/getAssessmentLevelScorecardDataFooter/",studentData,"POST");

	
	
	if(studentData!=null){
		$("#assessmentHomeScorecardHeader").children("tr").remove();
		$("#assessmentHomeScorecardFooter").children("tr").remove();
		if(studentData!=""){
		$("#assessmentHomeScorecardHeader").append(createTableForAssessmentWiseScoreCard(studentData[0].maxQuestionCount));
		
		$("#assessmentHomeScorecardFooter").append(createTableForAssessmentWiseScoreCardFooter(studentData,studentFooterData));
		}else{
			$("#assessmentHomeScorecardHeader").append(createTableForAssessmentWiseScoreCard(1));

		}
		assessmentWiseScoreCardData(studentData,'assessmentHomeScorecardTable');
		if(studentData==""){
			$("#assessmentHomeScorecardTable tbody").append("<tr>"+"<td colspan='32' style='text-align:center'>No data available in table</td>"+"</tr>");

		}
		$(".outer-loader").css('display','none');
		
		
	}else{
		$(".outer-loader").css('display','none');
	}
	
}

///////////////////////Create  Table For Assessment wise scorecard Data  //////////////////////////

var createTableForAssessmentWiseScoreCard=function(maxNumberOfColumns){
	
	var noOfColmns=maxNumberOfColumns+6;
	var rowSpan='';
		for(var i=1;i<=maxNumberOfColumns;i++){
			rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
		}
	
	var	thead="<tr>"+
	
	"<th  class='nosort' rowspan='3'><p>Roll No.</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Student Name</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Fathers's Name</p></th>"+
	
	"<th  class='nosort' rowspan='3'><p>Gender</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Age</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Chapter Name</p></th>"+
	"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
	"</tr>"+
	"<tr>"+
	"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise scores</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Marks Scored</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Total Marks</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Capterwise % score</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Total marks scored</p></th>"+
	"<th  class='nosort' rowspan='2'><p>% score</p></th>"+
	"</tr>"+
	"<tr>"+
	rowSpan
	"</tr>";
	return thead;
};

var createTableForAssessmentWiseScoreCardFooter=function(studentDataVM,studentFooterData){
	
	var totalStudent=studentDataVM.length;
	var chapters=studentDataVM[0].chapterReportVmList;
	var totalChapterCount=chapters.length+1;
	var maxNumberOfColumns=studentDataVM[0].maxQuestionCount
	var noOfColmns=maxNumberOfColumns+6;
	var totalMarksScored=0;
	var totalPercentageMarksScored=0;
	var rowSpan='';
	var chapMap={};
	var stdCount=0;
	var xScoredMarks=0;
	$.each(studentDataVM,function(indexCount,studentdata){
		if(studentdata.totalMarksScored!=null){
		$.each(studentdata.chapterReportVmList, function(index,chapterData) {
			if(typeof chapMap[chapterData.chapterName] == "undefined"){
				chapMap[chapterData.chapterName]=chapterData.chapterWiseTotalMarks;
				xScoredMarks=xScoredMarks+chapterData.chapterWiseTotalMarks;
			}else{
				chapMap[chapterData.chapterName]=chapMap[chapterData.chapterName]+chapterData.chapterWiseTotalMarks;
				xScoredMarks=xScoredMarks+chapterData.chapterWiseTotalMarks;
			}
		});
		
			stdCount=stdCount+1;
			totalMarksScored=totalMarksScored+studentdata.totalMarksScored;
			totalPercentageMarksScored=totalPercentageMarksScored+studentdata.percentageScore;
		
	}
	});
	
	$.each(chapters, function(index,chapterVm) {	
		var chapterName=chapterVm.chapterName;
		var marksScored=0;
		var totalMarkScored=0;
		var percentageChapterWise=0;
		if(index > 0){
			rowSpan+= "<tr class='textCenter'>"
		}
		rowSpan +="<td class='textCenter'>"+chapterVm.chapterName+"</td>";
		$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
			var qWiseScore=studentFooterData[chapterName][indexNumber];
			marksScored=marksScored+qWiseScore;
			rowSpan +="<td class='textCenter'>"+ qWiseScore+"</td>";
		});
		
		var totalChapterperData=0;
		if(chapMap.hasOwnProperty(chapterName)==true){
			var chapData=chapMap.hasOwnProperty(chapterName)?chapMap[chapterName]:1;
			if(chapData==0){
				chapData=1;
			}
			totalChapterperData=100*(marksScored/chapData).toFixed(2);
		}
		var chapMapData=chapMap.hasOwnProperty(chapterName)==true?chapMap[chapterName]:0;
		
		rowSpan+="<td class='textCenter' >"+marksScored+"</td>"+
		"<td class='textCenter'>"+chapMapData+"</td>"+
		"<td class='textCenter'>"+totalChapterperData.toFixed(0)+"%"+"</td>";

		if(index==0){
			rowSpan+= "<td class='textCenter' rowspan="+totalChapterCount+">"+totalMarksScored+"/"+xScoredMarks+"</td>"+
			"<td class='textCenter' rowspan="+totalChapterCount+">"+parseFloat(totalPercentageMarksScored/(stdCount!=0?stdCount:1)).toFixed(2)+"%"+"</td></tr>";
		}else{
			rowSpan+="</tr>";
		}

	});


	var	thead="<tr class='textCenter'>"+
	"<td class='textCenter'rowspan='"+totalChapterCount+"' colspan='5'>Over All</td></tr>"+
	rowSpan;
	return thead;
};
var assessmentWiseScoreCardData=function(reportData,tableId){
	$("#assessmentHomeScorecardTable tbody tr").remove();

	$.each(reportData, function(index,studentData) {	
		var maxQuestionCountData=studentData.totalMarks;
		var totalMarks=studentData.totalMarksScored==null?"":studentData.totalMarksScored+"/"+maxQuestionCountData;
		var percentageScore="";
		if(studentData.totalMarksScored!=null){
		 percentageScore=studentData.percentageScore==-1?"":studentData.percentageScore+"%";
		}
		var rowSpan='';
		var count=studentData.numberOfChapters;
		$.each(studentData.chapterReportVmList, function(index,chapterVm) {
			var xChapterWiseScore="";
			var xChapterWisePercentage="";
			if(studentData.totalMarksScored!=null){
				xChapterWiseScore=chapterVm.chapterWiseScore;
				xChapterWisePercentage=chapterVm.chapterWisePercentage+"%";
			}
			
			if(index > 0){
				rowSpan+= '<tr id="trchap'+studentData.consolidatedStatusId+'">';
				}
			rowSpan +="<td title='"+chapterVm.chapterName+"' id=chap"+index+">"+chapterVm.chapterName+"</td>";
			$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
				if(qScoreMap == -1){
					qScoreMap="";
				}
				 rowSpan +="<td title='"+qScoreMap+"' id=chap"+index+"_Q"+indexNumber+">"+qScoreMap+"</td>";
			});
			rowSpan+="<td title='"+chapterVm.chapterWiseScore+"' id=chapterWiseScore"+index+">"+xChapterWiseScore+"</td>"+
			"<td title='"+chapterVm.chapterWiseTotalMarks+"'  id=chapterWiseTotalMarks"+index+">"+chapterVm.chapterWiseTotalMarks+"</td>"+
			"<td title='"+chapterVm.chapterWisePercentage+"'  id=chapterWisePercentage"+index+">"+xChapterWisePercentage+"</td>";
			
			if(index==0){
				rowSpan+= "<td title='"+totalMarks+"' rowspan="+count+">"+totalMarks+"</td>"+
				"<td title='"+percentageScore+"' rowspan="+count+">"+percentageScore+"</td></tr>";
			}else{
				rowSpan+="</tr>";
			}
			
		});
		
		
		row='<tr  id="tr'+studentData.consolidatedStatusId+'">'+
		
		'<td title="'+studentData.rollNumber+'" rowspan="'+count+'">'+studentData.rollNumber+'</td>'+
		
		'<td title="'+studentData.studentName+'" rowspan="'+count+'">'+studentData.studentName+'</td>'+
		'<td title="'+studentData.fatherName+'" rowspan="'+count+'">'+studentData.fatherName+'</td>'+
		'<td title="'+studentData.gender+'" rowspan="'+count+'">'+studentData.gender+'</td>'+
		'<td title="'+studentData.age+'" rowspan="'+count+'">'+studentData.age+'</td>'+
		rowSpan;
		
	
		$("#assessmentHomeScorecardTable tbody").append(row);
		if(studentData.isAppearedLate==true){
			$("#assessmentHomeScorecardTable #tr"+studentData.consolidatedStatusId).addClass('appread-late-student');
			$("#assessmentHomeScorecardTable #trchap"+studentData.consolidatedStatusId).addClass('appread-late-student');
			
		}
	});
	
	

}


/////////////////////// Create  Assessment wise Bloomtaxonomy //////////////////////////



var getStudentBloomTaxonomyByAssmt = function(mAssessmentPlannerId,assessmentId,campaignId,schoolId){

	$(".outer-loader").css('display','block');
	var studentData = reportCustomAjaxCalling("report/assessmentWiseBloomTaxonomy/"+mAssessmentPlannerId+"/"+assessmentId+"/"+campaignId+"/"+schoolId,null,"POST");
	
	if(studentData!=null){
		var studentFooterData = reportCustomAjaxCalling("report/getAssessmentLevelScorecardDataFooter/",studentData.studentLevelReportVm,"POST");
		$("#assessmentHomeBoomTaxonomyHeader").children("tr").remove();
		$("#assessmentHomeBoomTaxonomyFooter").children("tr").remove();
		$("#assessmentHomeBoomTaxonomyHeader").append(createTableForAssessmentWiseBloomTaxonomy(studentData));
		$("#assessmentHomeBoomTaxonomyFooter").append(createTableForAssessmentWiseBloomTaxonomyFooter(studentData.studentLevelReportVm,studentFooterData));
		assessmentWiseBloomTaxonomy(studentData.studentLevelReportVm,'assessmentHomeBloomResultTable');
		if(studentData.studentLevelReportVm==""){
			$("#assessmentHomeBloomResultTable tbody").append("<tr>"+"<td colspan='32' style='text-align:center'>No data available in table</td>"+"</tr>");

		}
		$(".outer-loader").css('display','none');
	}else{
		$(".outer-loader").css('display','none');
	}
	
}


var createTableForAssessmentWiseBloomTaxonomy=function(studentData){
	
	var noOfColmns=(studentData.maxNoOfRows+5);
	var rowSpan='';
		for(var i=1;i<=studentData.maxNoOfRows;i++){
			rowSpan +="<th  class='nosort' colspan='1'><p>Q"+i+"</p></th>";
		}
		var taxonomy='';
		for(var i=0;i<=studentData.maxNoOfRows-1;i++){
			taxonomy +="<th  class='nosort' colspan='1'><p>"+studentData.taxonomyHeaderList[i]+"</p></th>";
		}
	
	var	thead="<tr>"+
	"<th  class='nosort' rowspan='4'><p>Roll No.</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Student Name</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Father's Name</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Gender</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Age</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Chapter Names</p></th>"+
	"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p> Learning Dimension Wise </p></th>"+
	"</tr>"+
	"<tr>"+
	"<th  class='nosort' colspan='"+studentData.maxNoOfRows+"' ><p>Question wise Parameter Score</p></th>"+
	"<th  class='nosort' colspan='5'><p>Overall Parameter Score</p></th>"+
	
	"</tr>"+
	"<tr>"+
	rowSpan+
	"<th  class='nosort' colspan='1' ><p>K</p></th>"+
	"<th  class='nosort' colspan='1'><p>U</p></th>"+
	"<th  class='nosort' colspan='1'><p>O</p></th>"+
	"<th  class='nosort' colspan='1'><p>AN</p></th>"+
	"<th  class='nosort' colspan='1'><p>AP</p></th>"+
	"</tr>"+
	"<tr>"+
	taxonomy+
	"<th  class='nosort' colspan='1' ><p>"+studentData.taxonomyWeightAge[0]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[1]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[2]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[3]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[4]+"</p></th>"+
	"</tr>";
	return thead;
};	

var createTableForAssessmentWiseBloomTaxonomyFooter=function(taxReportData,studendFooterData){
	var studentData=taxReportData[0];
	if(studentData!=null){
	var rowSpan='';
	var count=studentData.numberOfChapters;
	
	var parameterScoring={
			"K":0,
			"U":0,
			"O":0,
			"An":0,
			"Ap":0,
	};
	$.each(taxReportData, function(totalCount,data) {
		    parameterScoring["K"]=parameterScoring["K"]+data.parameterScoringForAssessmentVm["scored_K"];
			parameterScoring["U"]=parameterScoring["U"]+data.parameterScoringForAssessmentVm["scored_U"];
			parameterScoring["O"]=parameterScoring["O"]+data.parameterScoringForAssessmentVm["scored_O"];
			parameterScoring["An"]=parameterScoring["An"]+data.parameterScoringForAssessmentVm["scored_An"];
			parameterScoring["Ap"]=parameterScoring["Ap"]+data.parameterScoringForAssessmentVm["scored_Ap"];
		
		
	});
	
	$.each(studentData.chapterReportVmList, function(chapCount,chapterVm) {	
		var chapterName=chapterVm.chapterName;
		if(chapCount > 0){
			rowSpan+= "<tr class='textCenter'>"
			}
		rowSpan +="<td class='textCenter' title='"+chapterName+"' >"+chapterName+"</td>";
		$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {
			var qWiseScore=studendFooterData[chapterName][indexNumber];
			 rowSpan +="<td class='textCenter'>"+ qWiseScore+"</td>";
			 
		});
		
		if(chapCount==0){
		rowSpan+=	"<td class='textCenter' rowspan="+count+">"+parameterScoring["K"]+"</td>"+
		"<td class='textCenter' rowspan="+count+">"+parameterScoring["U"]+"</td>"+
		"<td class='textCenter' rowspan="+count+">"+parameterScoring["O"]+"</td>"+
		"<td class='textCenter' rowspan="+count+">"+parameterScoring["An"]+"</td>"+
		"<td class='textCenter'  rowspan="+count+">"+parameterScoring["Ap"]+"</td>"+
		"</tr>";
			}else{
			rowSpan+="</tr>";
		}
		
	});
	
	var chaptotalCount=count+1;
	var	thead="<tr>"+
	"<td class='textCenter' rowspan='"+chaptotalCount+"' colspan='5'>Over All</td></tr>"+
	rowSpan;
	return thead;	
}
}
var assessmentWiseBloomTaxonomy=function(reportData,tableId){
	$("#assessmentHomeBloomResultTable tbody tr").remove();
	$.each(reportData, function(index,studentData) {	
			
			var knowledgeScore=studentData.parameterScoringForAssessmentVm.scored_K==null?"0":studentData.parameterScoringForAssessmentVm.scored_K;
			var understandingScore=studentData.parameterScoringForAssessmentVm.scored_U==null?"0":studentData.parameterScoringForAssessmentVm.scored_U;
			var operationScore=studentData.parameterScoringForAssessmentVm.scored_O==null?"0":studentData.parameterScoringForAssessmentVm.scored_O;
			var analysisScore=studentData.parameterScoringForAssessmentVm.scored_An==null?"0":studentData.parameterScoringForAssessmentVm.scored_An;
			var applicationScore=studentData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentData.parameterScoringForAssessmentVm.scored_Ap;

			
			var rowSpan='';
			var count=studentData.numberOfChapters;
			$.each(studentData.chapterReportVmList, function(index,chapterVm) {	
				
				if(index > 0){
					rowSpan+= '<tr id="trchap'+studentData.consolidatedStatusId+'">';
					}
				rowSpan +="<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
				$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
					if(qScoreMap == -1){
						qScoreMap="";
					}
					 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
				});
				
				if(index==0){
				rowSpan+=	"<td title='"+knowledgeScore+"' rowspan="+count+">"+knowledgeScore+"</td>"+
				"<td title='"+understandingScore+"' rowspan="+count+">"+understandingScore+"</td>"+
				"<td title='"+operationScore+"' rowspan="+count+">"+operationScore+"</td>"+
				"<td title='"+analysisScore+"' rowspan="+count+">"+analysisScore+"</td>"+
				"<td title='"+applicationScore+"' rowspan="+count+">"+applicationScore+"</td>"+
				"</tr>";
					}else{
					rowSpan+="</tr>";
				}
				
			});
			
		
		
		
		
			var	row='<tr  id="tr'+studentData.consolidatedStatusId+'">'+
	   "<td title='"+studentData.rollNumber+"' rowspan="+count+">"+studentData.rollNumber+"</td>"+
	    "<td title='"+studentData.studentName+"' rowspan="+count+">"+studentData.studentName+"</td>"+
		"<td title='"+studentData.fatherName+"' rowspan="+count+">"+studentData.fatherName+"</td>"+
		"<td title='"+studentData.gender+"' rowspan="+count+">"+studentData.gender+"</td>"+
		"<td title='"+studentData.age+"' rowspan="+count+">"+studentData.age+"</td>"+
		
		rowSpan;
	
	
	$("#assessmentHomeBloomResultTable tbody").append(row);
	if(studentData.isAppearedLate==true){
		$("#assessmentHomeBloomResultTable #tr"+studentData.consolidatedStatusId).addClass('appread-late-student');
		$("#assessmentHomeBloomResultTable #trchap"+studentData.consolidatedStatusId).addClass('appread-late-student');
		
	}
	});
}

$(function() {
	
	/*$('#tabContent a').hover(function (e) {
        e.preventDefault()
        $(this).tab('show')
     });*/
	$('#mdlStudentLevelReportDetailView').on('hidden.bs.modal', function () {
		fnUpdateBody();
	})
	 thLabelsMetrics3 =['Assessments Name','Status of Completion  ','Grade  ','Stage ','Subject ','Number of Students Enrolled ','Number of Students Appeared in Assessment ','% of Students Appeared ','Number of Students Absent for Assessment ',
	     	           'Number of Students Yet to Take Assessment ','Number of students scored &gt;= 90% (Appeared)','% of students scored &gt;= 90% (Appeared) ', 'Number of students scored &lt; 90%  (To Undergo Post Augmentation)','%students scored &lt; 90%  (To Undergo Post Augmentation) '];
	          fnSetDTColFilterPaginationWithoutExportButton('#metrics3ReportTable',14,[0,1,2,3,4,5,6,7,8,9,10,11,12,13],thLabelsMetrics3);
	     	           	
	          thLabelsMetrics4 =['Assessments Name','Status of Completion  ','Grade  ','Stage ','Subject ','Parameter Scoring For Assessment ','K ','U ','O ','An','Ap'];
	                	fnSetDTColFilterPaginationWithoutExportButton('#metrics4ReportTable',14,[0,1,2,3,4,5,6,7,8,9,10,11,12,13],thLabelsMetrics4);
	         
	
	
	
	
	thLabels =['Assessments Name','Status of Completion  ','Grade  ','Stage ','Subject ','Number of Students Enrolled ','Number of Students Appeared in Assessment ','% of Students Appeared ','Number of Students Absent for Assessment ',
'Number of Students Yet to Take Assessment ','Number of students scored &gt;= 90% (Appeared)','% of students scored &gt;= 90% (Appeared) ', 'Number of students scored &lt; 90%  (To Undergo Post Augmentation)','%students scored &lt; 90%  (To Undergo Post Augmentation) ','Parameter Scoring For Assessment ','K ','U ','O ','An','Ap'];
	fnSetDTColFilterPaginationWithoutExportButton('#teacherLevelReportTable',20,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],thLabels);
	
	
	thLabelsStudentDetails =['Roll No.','Student Name  ','Father Name  ','Gender ','Age ','Total Marks Scored ','% Score ','To Undergo PA ','Parameter Scoring For Assessment ','K ','U ','O ','An','Ap'];
	thSum=[8,9,10,11,12];
	fnSetDTColFilterPaginationWithoutExportButtonFooterCalBack('#tableForStudent',19,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18],thLabelsStudentDetails,thSum);
	
	
	setOptionsForMultipleSelect('#selectBox_grade');
	setOptionsForMultipleSelect('#selectBox_section');
	setOptionsForMultipleSelect('#selectBox_school');
	//setOptionsForMultipleSelect('#selectBox_zAssessment');
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		/* fnHideColumns('#schoolwiseAssementDetailTable',[2,3,4,6,8,9,10,11,13,15,16,17,18,19]); */
	});
	
	setDataTablePaginationForStudent('#studentLevelReportTable');
	setDataTablePagination('#tableSearchStudentDetail');
	setDataTablePagination('#tableForAbsentStudent');
	
	
	//$('#teacherLevelReportTable').dataTable();
	//$('#tableForStudent').dataTable();
	setOptionsForMultipleSelect(pageContextElement.grade);
	setOptionsForMultipleSelect(pageContextElement.section);
	setOptionsForMultipleSelect(pageContextElement.subject);
	setOptionsForMultipleSelect(pageContextElement.assessmentStatus);
	setOptionsForMultipleSelect(pageContextElement.school);
	fnInitDatePicker(pageContextElement.assessmentStartDate);
	fnInitDatePicker(pageContextElement.assessmentEndtDate);
	
	
	$("#frmTeacherLevelReport").formValidation(
			{
				excluded : ':disabled',
				feedbackIcons : {
					validating : 'glyphicon glyphicon-refresh'
				},
				grade: {
						validators: {
							callback: {
								message: '      ',
								callback: function(value, validator, $field) {
									// Get the selected options
									var options =$('divGrade #selectBoxGrade').val();
									return (options !=null);
								}
							}
						}
					},
					
					section: {
						validators: {
							callback: {
								message: '      ',
								callback: function(value, validator, $field) {
									// Get the selected options
									var options =$('#divSection #selectBoxSection').val();
									return (options !=null);
								}
							}
						}
					},
					subject: {
						validators: {
							callback: {
								message: '      ',
								callback: function(value, validator, $field) {
									// Get the selected options
									var options =$('#divSubject #selectBoxSubject').val();
									return (options !=null);
								}
							}
						}
					},
					assessmentStatus: {
						validators: {
							callback: {
								message: '      ',
								callback: function(value, validator, $field) {
									// Get the selected options
									var options =$('#divAssessmentStatus #selectAssessmentStatus').val();
									return (options !=null);
								}
							}
						}
					},
		            startDate: {
		              	 validators: {
		                       notEmpty: {
		                           message: ' '
		                       },
		                   }
		              },
		              endDate: {
		                  validators: {
		                      notEmpty: {
		                          message: ' '
		                      },
		                  }
		              },
					 
					stage: {
						validators: {
						callback: {
							message: '      ',
							callback: function(value, validator, $field) {
								// Get the selected options
								var options =$('input[name=stage]:checked', '#frmTeacherLevelReport').val();
								return (options !='undefined');
							}
						}
					}}
			}).on('success.form.fv', function(e) {
				e.preventDefault();
				$(".outer-loader").show();
				$.wait(50).then(genrateRow);
				
			}).on('success.field.fv', function(e, data) {
	            if (data.fv.getSubmitButton()) {
	                data.fv.disableSubmitButtons(false);
	            }
	        });
			
	
	
	
	$("#frmGraphMetrics") .formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
		 cAssessment: {
			validators: {
				callback: {
					message: '      ',
					callback: function(value, validator, $field) {
						// Get the selected options
						var options =$('#selectBox_zAssessment').find('option:selected').val();
						
						return (options !=null && options !=undefined);
					}
				}
			}
		}
		}
	
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		fnShowAssessmentWiseGraph(e);
	
});
	
	$("#studentSearchForm") .formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			studentName : {
				validators : {
					callback : {
						message : ' ',
						callback : function(value, validator, $field) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}							
					}
				}
			},
		fatherName : {
			validators : {
				callback : {
					message : ' ',
					callback : function(value, validator, $field) {
						var regx=/^[^'?\"/\\]*$/;
						if(/\ {2,}/g.test(value))
							return false;
						return regx.test(value);}							
				}
			}
		},
		studentId : {
			validators : {
				 callback : {
					message : ' ',
					callback : function(value, validator, $field) {
						var regx=/^[1-9]([0-9]{0,9}$)/;
						return regx.test(value);}							
				}
			}
		},
		assessmentName: {
			validators: {
				callback: {
					message: '      ',
					callback: function(value, validator, $field) {
						// Get the selected options
						var options =$('#selectBox_questionPaper').find('option:selected').val();
						
						return (options !=null && options !=undefined);
					}
				}
			}
		}
		}
	
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		 $(".outer-loader").css('display','block');
		 $.wait(30).then(fnGetStudentData);
		
		
	
});
	fnCustomSearchFilter("#tableForStudentSearch",".r3");
	fnCustomSearchFilter("#assessmentHomeScorecardTableSearch",".r4");
	fnCustomSearchFilter("#assessmentHomeBloomResultTableSearch",".r5");
	
	$('#mdlprojectLevelMetrics').on('hide.bs.modal', function (e) {
	    $(this).find(".modal-body").scrollTop(0);
	});
	$('#showAbsentStudentDetailPopup').on('hide.bs.modal', function (e) {
	    $(this).find(".modal-body").scrollTop(0);
	});
	
	$('#mdlStudentDetailView').on('hide.bs.modal', function (e) {
	    $(this).find(".modal-body").scrollTop(0);
	});
	
	fnColSpanIfDTEmpty('tableForStudent',20);
});

//custom search bar
function fnCustomSearchFilter(searchBox,tblResult){
	$(searchBox).keyup(function () {
		var searchTerm = $(searchBox).val();
		var listItem = $(tblResult+' tbody').children('tr');
		var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
	
		$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
			return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
		});
	
		$(tblResult+" tbody td").removeAttr("style");
		if($(searchBox).val().length!=0){
			$(tblResult+" tbody td:containsi('" + searchSplit + "')").each(function(e){			
				$(this).css('background','yellow');
			});
		}else{
			$(tblResult+" tbody td").removeAttr("style");
		}
	
	});
}


function exportMeSecond(ele,typeName){
	
	var  qpName =$('#questionPaperNameX').text();
	var tableTypeName="Student Wise Result : "+typeName
	var addInfo =['Question paper:'+qpName,tableTypeName];
	$(ele).tableExport(
					{
						type:'excel',
						escape:'false',
						additionalInfo:addInfo
						}
				);
}

function exportMe(ele,ele2,typeName){
	
	var tableTypeName="View Student Wise Result : "+typeName
	var addInfo =[tableTypeName];
	var tabledata=[ele,ele2];
	$(ele).tableExport(
					{   tables:tabledata,
						type:'excel',
						escape:'false',
						additionalInfo:addInfo
						}
				);
}



