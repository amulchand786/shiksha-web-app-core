var chartYaxisName='Value';
////////////////////////Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";



var studentPerformanceGenderWiseValue=new Object();
studentPerformanceGenderWiseValue.canvasDiv="#canvasStudentPerformanceGenderWiseValueDiv";
studentPerformanceGenderWiseValue.canvas="#chart-studentPerformanceGenderWiseValue";
studentPerformanceGenderWiseValue.canvasSpan="#spanStudentPerformanceGenderWiseValue";
studentPerformanceGenderWiseValue.tbody="#studentPerformanceGenderWiseValueTable tbody"
studentPerformanceGenderWiseValue.thead="#studentPerformanceGenderWiseValueTable thead"
studentPerformanceGenderWiseValue.table="#studentPerformanceGenderWiseValueTable"
studentPerformanceGenderWiseValue.tableDiv="#studentPerformanceGenderWiseValueTableDiv"
studentPerformanceGenderWiseValue.canvasElement="chart-studentPerformanceGenderWiseValue";


var studentPerformanceGenderWisePercentage=new Object();
studentPerformanceGenderWisePercentage.canvasDiv="#canvasStudentPerformanceGenderWisePercentageDiv";
studentPerformanceGenderWisePercentage.canvas="#chart-studentPerformanceGenderWisePercentage";
studentPerformanceGenderWisePercentage.canvasSpan="#spanStudentPerformanceGenderWisePercentage";
studentPerformanceGenderWisePercentage.tbody="#studentPerformanceGenderWisePercentageTable tbody"
studentPerformanceGenderWisePercentage.thead="#studentPerformanceGenderWisePercentageTable thead"
studentPerformanceGenderWisePercentage.table="#studentPerformanceGenderWisePercentageTable"
studentPerformanceGenderWisePercentage.tableDiv="#studentPerformanceGenderWisePercentageTableDiv"
studentPerformanceGenderWisePercentage.canvasElement="chart-studentPerformanceGenderWisePercentage";






var studentPerformanceGenderWiseBarChart={
		fnEmptyCanvas:function(message,canvas,canvasSpan){
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(" ");
			$(studentPerformanceAssessmentWisePercentage.canvas).remove();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(message)
		},
		fnUpdateBarChart:function(){
			    var asessmentMetricsReportData=graphData;
                var result=asessmentMetricsReportData!=null?asessmentMetricsReportData.length>0?true:false:false;
				var labels= ["Appeared Male", "Appeared Female", "Appeared Total", 
				             "Absent Male", "Absent Female", "Absent Total",
				             "Pending Male", "Pending Female", "Pending Total",
				             "Scored >= 90% Male", "Scored >= 90% Female", "Scored >= 90% Total",
				             "Scored <90 % Male", "Scored < 90% Female", "Scored < 90% Total"];
				
				
				
				if(result==true){
					$(studentPerformanceGenderWisePercentage.thead).children("tr").remove();
					$(studentPerformanceGenderWisePercentage.tbody).children("tr").remove();
					$(studentPerformanceGenderWiseValue.thead).children("tr").remove();
					$(studentPerformanceGenderWiseValue.tbody).children("tr").remove();
					
					var studentPerformanceValueDataSet=[]
					var studentPerformancePercentageDataSet=[]
					$.each(asessmentMetricsReportData,function(index,reportData){
						var valueData=[]
						var percentageData=[]

						////value///////////
						var assessmentName=reportData.assessmentName;
						var studentAppeared=reportData.totalNumberOfStudentsAppearedInAssessment;
						var studentScoredGTEQ90=reportData.numberOfStudentsScoredGTEQ90;
						var studentScoredLTEQ90=reportData.numberOfStudentsScoredLTEQ90;
						var numberOfStudentsAbsent=reportData.numberOfStudentsAbsentForAssessment;
						var numberOfStudentsPending=reportData.numberOfStudentsYetToTakeAssessment;

						var percentageStudentAttempted=reportData.percentageOfStudentsAttempted;
						var percentageStudentScoredGTEQ90=reportData.percentageOfStudentsScoredGTEQ90;
						var percentageStudentScoredLTEQ90=reportData.percentageOfStudentsScoredLTEQ90;
						


						valueData.push(
								studentAppeared.maleStudent,studentAppeared.femaleStudent,studentAppeared.totalStudent,
								numberOfStudentsAbsent.maleStudent,numberOfStudentsAbsent.femaleStudent,numberOfStudentsAbsent.totalStudent,
								numberOfStudentsPending.maleStudent,numberOfStudentsPending.femaleStudent,numberOfStudentsPending.totalStudent,
								studentScoredGTEQ90.maleStudent,studentScoredGTEQ90.femaleStudent,studentScoredGTEQ90.totalStudent,
								studentScoredLTEQ90.maleStudent,studentScoredLTEQ90.femaleStudent,studentScoredLTEQ90.totalStudent
						);
						percentageData.push(
								percentageStudentAttempted.maleStudent,percentageStudentAttempted.femaleStudent,percentageStudentAttempted.totalStudent,
								percentageStudentScoredGTEQ90.maleStudent,percentageStudentScoredGTEQ90.femaleStudent,percentageStudentScoredGTEQ90.totalStudent,
								percentageStudentScoredLTEQ90.maleStudent,percentageStudentScoredLTEQ90.femaleStudent,percentageStudentScoredLTEQ90.totalStudent 

						);
						var performanceValueData={
								label: assessmentName,
								backgroundColor: randomColor(),
								data: valueData
						};
						var performancePercentageData={
								label: assessmentName,
								backgroundColor: randomColor(),
								data: percentageData
						}

						studentPerformanceValueDataSet.push(performanceValueData);
						studentPerformancePercentageDataSet.push(performancePercentageData);

					})	;
					var studentPerformanceGenderWiseValueBarChartData = {
							labels : labels,
							datasets : studentPerformanceValueDataSet
					};
					var studentPerformanceGenderWisePercentageBarChartData = {
							labels : labels,
							datasets : studentPerformancePercentageDataSet
					};
					var studentPerformanceGendeWiseValueBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceGenderWiseValueBarChartData,"Value");
					//var studentPerformanceGenderWisePercentageBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceGenderWisePercentageBarChartData,"Percentage");
					studentPerformanceGenderWiseBarChart.createChartData(studentPerformanceGenderWiseValue,studentPerformanceGendeWiseValueBarChartDataConfig);
					//studentPerformanceGenderWiseBarChart.createChartData(studentPerformanceGenderWisePercentage,studentPerformanceGenderWisePercentageBarChartDataConfig);
					studentPerformanceGenderWiseBarChart.fnCreateTableGenderWise(studentPerformanceGenderWiseValueBarChartData,studentPerformanceGenderWiseValue,"Value");
					//studentPerformanceGenderWiseBarChart.fnCreateTableGenderWise(studentPerformanceGenderWisePercentageBarChartData,studentPerformanceGenderWisePercentage,"Percentage");
					}
				else{
					$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
					studentPerformanceGenderWiseBarChart.fnEmptyCanvas(emptyChartMessage,studentPerformanceGenderWiseValue.canvas,studentPerformanceGenderWiseValue.canvasSpan);
				
				}
			
		},
		createChartData:function(chartElement,configData){
			$(chartElement.canvas).remove();
			$(chartElement.canvasDiv).append('<canvas id="'+chartElement.canvasElement+'" />');
			var canvas = document.getElementById(chartElement.canvasElement);
			var context = canvas.getContext('2d');
			context.clear();
			window.myBar = new Chart(context, configData);
			window.myBar.update();
		},
         barChartDataConfig:function(data,label){
			var studentPerformanceBarChartDataConfig = {
					type: 'bar',
					data: data,
					options: {

						scales: {
							xAxes: [{
								display: true,
								gridLines: {
									display: false
								},
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: false
								},
								ticks: {
									beginAtZero: true,
									steps: 10,
									stepValue: 5,
								},
								scaleLabel: {
									display: true,
									labelString: label
								}
							}]
						},


						elements: {
							rectangle: {
								borderWidth: 0.5,
								borderColor: randomColor(),
								borderSkipped: 'bottom'
							}
						},
						responsive: true,
						legend: {
							position: 'bottom',
						},

					}


			}
			return studentPerformanceBarChartDataConfig;
		},
		fnCreateTableGenderWise : function(barData,element,type){
			//create heades
			var percentageSign="";
			if (type=="Percentage"){
				percentageSign="%";
			}
			var pData=barData.datasets;
			var pLables=barData.labels;
			var th ='';
			var openTh='<th>',closedTh='</th>';
			
			$.each(pLables,function(i,val){
				th=th+openTh+val+closedTh;
			});
			$(element.thead).append('<tr><th style="min-width: 115px !important;"> Question Paper '+closedTh+th+'</tr>');
			
			var tr='<tr>',closedTr='</tr>',td='<td>',closedtd='</td>';
			$.each(pData,function(i,val){
				var nData =tr+td+(val.label)+closedtd;
				var mData='';
				$.each(val.data,function(i,e){
					mData=mData+td+(e)+percentageSign+closedtd;
				});
				nData =nData+mData+closedTr;
				$(element.tbody).append(nData);
			});
			
		}

}




var studentPerformanceGenderWiseInIt=function(){
	studentPerformanceGenderWiseBarChart.fnUpdateBarChart();
	
}