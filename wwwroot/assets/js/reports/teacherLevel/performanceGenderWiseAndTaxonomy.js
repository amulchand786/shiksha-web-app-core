var chartYaxisName='Value';
////////////////////////Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";



var studentPerformanceGenderWiseTaxonomyValue=new Object();
studentPerformanceGenderWiseTaxonomyValue.canvasDiv="#canvasStudentPerformanceGenderWiseTaxonomyValueDiv";
studentPerformanceGenderWiseTaxonomyValue.canvas="#chart-studentPerformanceGenderWiseTaxonomyValue";
studentPerformanceGenderWiseTaxonomyValue.canvasSpan="#spanStudentPerformanceGenderWiseTaxonomyValue";
studentPerformanceGenderWiseTaxonomyValue.tbody="#studentPerformanceGenderWiseTaxonomyValueTable tbody"
studentPerformanceGenderWiseTaxonomyValue.thead="#studentPerformanceGenderWiseTaxonomyValueTable thead"
studentPerformanceGenderWiseTaxonomyValue.table="#studentPerformanceGenderWiseTaxonomyValueTable"
studentPerformanceGenderWiseTaxonomyValue.tableDiv="#studentPerformanceGenderWiseTaxonomyValueTableDiv"
studentPerformanceGenderWiseTaxonomyValue.canvasElement="chart-studentPerformanceGenderWiseTaxonomyValue";


var studentPerformanceGenderWiseTaxonomyPercentage=new Object();
studentPerformanceGenderWiseTaxonomyPercentage.canvasDiv="#canvasStudentPerformanceGenderWiseTaxonomyPercentageDiv";
studentPerformanceGenderWiseTaxonomyPercentage.canvas="#chart-studentPerformanceGenderWiseTaxonomyPercentage";
studentPerformanceGenderWiseTaxonomyPercentage.canvasSpan="#spanStudentPerformanceGenderWiseTaxonomyPercentage";
studentPerformanceGenderWiseTaxonomyPercentage.tbody="#studentPerformanceGenderWiseTaxonomyPercentageTable tbody"
studentPerformanceGenderWiseTaxonomyPercentage.thead="#studentPerformanceGenderWiseTaxonomyPercentageTable thead"
studentPerformanceGenderWiseTaxonomyPercentage.table="#studentPerformanceGenderWiseTaxonomyPercentageTable"
studentPerformanceGenderWiseTaxonomyPercentage.tableDiv="#studentPerformanceGenderWiseTaxonomyPercentageTableDiv"
studentPerformanceGenderWiseTaxonomyPercentage.canvasElement="chart-studentPerformanceGenderWiseTaxonomyPercentage";






var studentPerformanceGenderWiseTaxonomyWiseBarChart={
		fnEmptyCanvas:function(message,canvas,canvasSpan){
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(" ");
			$(studentPerformanceAssessmentWisePercentage.canvas).remove();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(message)
		},
		fnUpdateBarChart:function(){
			    var asessmentMetricsReportData=graphData;
                var result=asessmentMetricsReportData!=null?asessmentMetricsReportData.length>0?true:false:false;
				var labels= ["K Male", "K Female", "K Total", "U Male", "U Female", "U Total", "O Male", "O Female", "O Total", "An Male", "An Female", "An Total", "Ap Male", "Ap Female", "Ap Total" ];
				
				
				if(result==true){
					$(studentPerformanceGenderWiseTaxonomyPercentage.thead).children("tr").remove();
					$(studentPerformanceGenderWiseTaxonomyPercentage.tbody).children("tr").remove();
					$(studentPerformanceGenderWiseTaxonomyValue.thead).children("tr").remove();
					$(studentPerformanceGenderWiseTaxonomyValue.tbody).children("tr").remove();
					
					var studentPerformanceValueDataSet=[]
					var studentPerformancePercentageDataSet=[]
					$.each(asessmentMetricsReportData,function(index,reportData){
						var valueData=[]
						var percentageData=[]

						////value///////////
						var assessmentName=reportData.assessmentName;
						var studentAppeared=reportData.totalNumberOfStudentsAppearedInAssessment;
						var parameterScoringMale=reportData.parameterScoringForAssessmentMale;
						var parameterScoringFemale=reportData.parameterScoringForAssessmentFemale;
						var parameterScoringTotal=reportData.parameterScoringForAssessmentTotalScoring;
						var taxonomyScoreData=reportData.totalTaxonomyScoreMap;
						
						var kTaxnomyVal=taxonomyScoreData.hasOwnProperty('Knowledge')==true?taxonomyScoreData.Knowledge:0;
						var uTaxnomyVal=taxonomyScoreData.hasOwnProperty('Understanding')==true?taxonomyScoreData.Understanding:0;
						var oTaxnomyVal=taxonomyScoreData.hasOwnProperty('Operation')==true?taxonomyScoreData.Operation:0;
						var anTaxnomyVal=taxonomyScoreData.hasOwnProperty('Analysis')==true?taxonomyScoreData.Analysis:0;
						var apTaxnomyVal=taxonomyScoreData.hasOwnProperty('Application')==true?taxonomyScoreData.Application:0;
						
						var maleStudentCount=studentAppeared.maleStudent;
						var femaleStudentCount=studentAppeared.femaleStudent;
						var totalStudentCount=studentAppeared.totalStudent;

						var maleSoreMap_K=(100*parameterScoringMale.scored_K)/((kTaxnomyVal*maleStudentCount)!=0?(kTaxnomyVal*maleStudentCount):1);
						var maleScoreMap_U=(100*parameterScoringMale.scored_U)/((uTaxnomyVal*maleStudentCount)!=0?(uTaxnomyVal*maleStudentCount):1);
						var maleScoreMap_O=(100*parameterScoringMale.scored_O)/((oTaxnomyVal*maleStudentCount)!=0?(oTaxnomyVal*maleStudentCount):1);
						var maleScoreMap_An=(100*parameterScoringMale.scored_An)/((anTaxnomyVal*maleStudentCount)!=0?(anTaxnomyVal*maleStudentCount):1);
						var maleScoreMap_Ap=(100*parameterScoringMale.scored_Ap)/((apTaxnomyVal*maleStudentCount)!=0?(apTaxnomyVal*maleStudentCount):1);

						var femaleScoreMap_K=(100*parameterScoringFemale.scored_K)/((kTaxnomyVal*femaleStudentCount)!=0?(kTaxnomyVal*femaleStudentCount):1);
						var femaleScoreMap_U=(100*parameterScoringFemale.scored_U)/((uTaxnomyVal*femaleStudentCount)!=0?(uTaxnomyVal*femaleStudentCount):1);
						var femaleScoreMap_O=(100*parameterScoringFemale.scored_O)/((oTaxnomyVal*femaleStudentCount)!=0?(oTaxnomyVal*femaleStudentCount):1);
						var femaleScoreMap_An=(100*parameterScoringFemale.scored_An)/((anTaxnomyVal*femaleStudentCount)!=0?(anTaxnomyVal*femaleStudentCount):1);
						var femaleScoreMap_Ap=(100*parameterScoringFemale.scored_Ap)/((apTaxnomyVal*femaleStudentCount)!=0?(apTaxnomyVal*femaleStudentCount):1);
						
						var totalScoreMap_K=(100*parameterScoringTotal.scored_K)/((kTaxnomyVal*totalStudentCount)!=0?(kTaxnomyVal*totalStudentCount):1);
						var totalScoreMap_U=(100*parameterScoringTotal.scored_U)/((uTaxnomyVal*totalStudentCount)!=0?(uTaxnomyVal*totalStudentCount):1);
						var totalScoreMap_O=(100*parameterScoringTotal.scored_O)/((oTaxnomyVal*totalStudentCount)!=0?(oTaxnomyVal*totalStudentCount):1);
						var totalScoreMap_An=(100*parameterScoringTotal.scored_An)/((anTaxnomyVal*totalStudentCount)!=0?(anTaxnomyVal*totalStudentCount):1);
						var totalScoreMap_Ap=(100*parameterScoringTotal.scored_Ap)/((apTaxnomyVal*totalStudentCount)!=0?(apTaxnomyVal*totalStudentCount):1);



						valueData.push(
								parameterScoringMale.scored_K,parameterScoringFemale.scored_K,parameterScoringTotal.scored_K,
								parameterScoringMale.scored_U,parameterScoringFemale.scored_U,parameterScoringTotal.scored_U,
								parameterScoringMale.scored_O,parameterScoringFemale.scored_O,parameterScoringTotal.scored_O,
								parameterScoringMale.scored_An,parameterScoringFemale.scored_An,parameterScoringTotal.scored_An,
								parameterScoringMale.scored_Ap,parameterScoringFemale.scored_Ap,parameterScoringTotal.scored_Ap
						);
						
						percentageData.push(
								maleSoreMap_K.toFixed(2), femaleScoreMap_K.toFixed(2), totalScoreMap_K.toFixed(2),
								maleScoreMap_U.toFixed(2), femaleScoreMap_U.toFixed(2), totalScoreMap_U.toFixed(2),
								maleScoreMap_O.toFixed(2), femaleScoreMap_O.toFixed(2), totalScoreMap_O.toFixed(2),
								maleScoreMap_An.toFixed(2), femaleScoreMap_An.toFixed(2), totalScoreMap_An.toFixed(2),
								maleScoreMap_Ap.toFixed(2), femaleScoreMap_Ap.toFixed(2), totalScoreMap_Ap.toFixed(2)
						);
						var performanceValueData={
								label: assessmentName,
								backgroundColor: randomColor(),
								data: valueData
						};
						var performancePercentageData={
								label: assessmentName,
								backgroundColor: randomColor(),
								data: percentageData
						}

						studentPerformanceValueDataSet.push(performanceValueData);
						studentPerformancePercentageDataSet.push(performancePercentageData);

					})	;
					var studentPerformanceGenderWiseValueBarChartData = {
							labels : labels,
							datasets : studentPerformanceValueDataSet
					};
					var studentPerformanceGenderWisePercentageBarChartData = {
							labels : labels,
							datasets : studentPerformancePercentageDataSet
					};
					var studentPerformanceGendeWiseValueBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceGenderWiseValueBarChartData,"Value");
					var studentPerformanceGenderWisePercentageBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceGenderWisePercentageBarChartData,"Percentage");
					studentPerformanceGenderWiseTaxonomyWiseBarChart.createChartData(studentPerformanceGenderWiseTaxonomyValue,studentPerformanceGendeWiseValueBarChartDataConfig);
					studentPerformanceGenderWiseTaxonomyWiseBarChart.createChartData(studentPerformanceGenderWiseTaxonomyPercentage,studentPerformanceGenderWisePercentageBarChartDataConfig);
					studentPerformanceGenderWiseTaxonomyWiseBarChart.fnCreateTableGenderWise(studentPerformanceGenderWiseValueBarChartData,studentPerformanceGenderWiseTaxonomyValue,"Value");
					studentPerformanceGenderWiseTaxonomyWiseBarChart.fnCreateTableGenderWise(studentPerformanceGenderWisePercentageBarChartData,studentPerformanceGenderWiseTaxonomyPercentage,"Percentage");
					}
				else{
					$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
					studentPerformanceGenderWiseTaxonomyWiseBarChart.fnEmptyCanvas(emptyChartMessage,studentPerformanceAssessmentWisePercentage.canvas,studentPerformanceAssessmentWisePercentage.canvasSpan);
				}
			
		},
		createChartData:function(chartElement,configData){
			$(chartElement.canvas).remove();
			$(chartElement.canvasDiv).append('<canvas id="'+chartElement.canvasElement+'" />');
			var canvas = document.getElementById(chartElement.canvasElement);
			var context = canvas.getContext('2d');
			context.clear();
			window.myBar = new Chart(context, configData);
			window.myBar.update();
		},
         barChartDataConfig:function(data,label){
			var studentPerformanceBarChartDataConfig = {
					type: 'bar',
					data: data,
					options: {

						scales: {
							xAxes: [{
								display: true,
								gridLines: {
									display: false
								},
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: false
								},
								ticks: {
									beginAtZero: true,
									steps: 10,
									stepValue: 5,
								},
								scaleLabel: {
									display: true,
									labelString: label
								}
							}]
						},


						elements: {
							rectangle: {
								borderWidth: 0.5,
								borderColor: randomColor(),
								borderSkipped: 'bottom'
							}
						},
						responsive: true,
						legend: {
							position: 'bottom',
						},

					}


			}
			return studentPerformanceBarChartDataConfig;
		},
		fnCreateTableGenderWise : function(barData,element,type){
			//create heades
			var percentageSign="";
			if (type=="Percentage"){
				percentageSign="%";
			}
			var pData=barData.datasets;
			var pLables=barData.labels;
			var th ='';
			var openTh='<th>',closedTh='</th>';
			
			$.each(pLables,function(i,val){
				th=th+openTh+val+closedTh;
			});
			$(element.thead).append('<tr><th style="min-width: 115px !important;"> Question Paper '+closedTh+th+'</tr>');
			
			var tr='<tr>',closedTr='</tr>',td='<td>',closedtd='</td>';
			$.each(pData,function(i,val){
				var nData =tr+td+(val.label)+closedtd;
				var mData='';
				$.each(val.data,function(i,e){
					mData=mData+td+(e)+percentageSign+closedtd;
				});
				nData =nData+mData+closedTr;
				$(element.tbody).append(nData);
			});
			
		}

}




var studentPerformanceGenderWiseTaxonomyInIt=function(){
	studentPerformanceGenderWiseTaxonomyWiseBarChart.fnUpdateBarChart();
	
}