var chartYaxisName='Value';
////////////////////////Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";

var studentPerformanceAssessmentWisePercentage=new Object();
studentPerformanceAssessmentWisePercentage.canvasDiv="#canvasStudentPerformanceAssessmentWisePercentageDiv";
studentPerformanceAssessmentWisePercentage.canvas="#chart-studentPerformanceAssessmentWisePercentage";
studentPerformanceAssessmentWisePercentage.canvasElement="chart-studentPerformanceAssessmentWisePercentage";
studentPerformanceAssessmentWisePercentage.canvasSpan="#spanStudentPerformanceAssessmentWisePercentage";
studentPerformanceAssessmentWisePercentage.tbody="#studentPerformanceAssessmentWisePercentageTable tbody"
studentPerformanceAssessmentWisePercentage.table="#studentPerformanceAssessmentWisePercentageTable"
studentPerformanceAssessmentWisePercentage.tableDiv="#studentPerformanceAssessmentWisePercentageTableDiv"

var studentPerformanceAssessmentWiseValue=new Object();
studentPerformanceAssessmentWiseValue.canvasDiv="#canvasStudentPerformanceAssessmentWiseValueDiv";
studentPerformanceAssessmentWiseValue.canvas="#chart-studentPerformanceAssessmentWiseValue";
studentPerformanceAssessmentWiseValue.canvasElement="chart-studentPerformanceAssessmentWiseValue";
studentPerformanceAssessmentWiseValue.canvasSpan="#spanStudentPerformanceAssessmentWiseValue";
studentPerformanceAssessmentWiseValue.tbody="#studentPerformanceAssessmentWiseValueTable tbody"
studentPerformanceAssessmentWiseValue.table="#studentPerformanceAssessmentWiseValueTable"
studentPerformanceAssessmentWiseValue.tableDiv="#studentPerformanceAssessmentWiseValueTableDiv"

var studentPerformanceAssessmentWiseTaxonomy=new Object();
studentPerformanceAssessmentWiseTaxonomy.canvasDiv="#canvasStudentPerformanceAssessmentWiseTaxonomyDiv";
studentPerformanceAssessmentWiseTaxonomy.canvas="#chart-studentPerformanceAssessmentWiseTaxonomy";
studentPerformanceAssessmentWiseTaxonomy.canvasSpan="#spanStudentPerformanceAssessmentWiseTaxonomy";
studentPerformanceAssessmentWiseTaxonomy.tbody="#studentPerformanceAssessmentWiseTaxonomyTable tbody"
studentPerformanceAssessmentWiseTaxonomy.table="#studentPerformanceAssessmentWiseTaxonomyTable"
studentPerformanceAssessmentWiseTaxonomy.tableDiv="#studentPerformanceAssessmentWiseTaxonomyTableDiv"
studentPerformanceAssessmentWiseTaxonomy.canvasElement="chart-studentPerformanceAssessmentWiseTaxonomy";


var studentPerformanceAssessmentWiseTaxonomyValue=new Object();
studentPerformanceAssessmentWiseTaxonomyValue.canvasDiv="#canvasStudentPerformanceAssessmentWiseTaxonomyValueDiv";
studentPerformanceAssessmentWiseTaxonomyValue.canvas="#chart-studentPerformanceAssessmentWiseTaxonomyValue";
studentPerformanceAssessmentWiseTaxonomyValue.canvasSpan="#spanStudentPerformanceAssessmentWiseTaxonomyValue";
studentPerformanceAssessmentWiseTaxonomyValue.tbody="#studentPerformanceAssessmentWiseTaxonomyValueTable tbody"
studentPerformanceAssessmentWiseTaxonomyValue.table="#studentPerformanceAssessmentWiseTaxonomyValueTable"
studentPerformanceAssessmentWiseTaxonomyValue.tableDiv="#studentPerformanceAssessmentWiseTaxonomyValueTableDiv"
studentPerformanceAssessmentWiseTaxonomyValue.canvasElement="chart-studentPerformanceAssessmentWiseTaxonomyValue";


var studentPerformanceAssessmentWiseBarChart={
		fnEmptyCanvas:function(message,canvas,canvasSpan){
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(" ");
			$(studentPerformanceAssessmentWisePercentage.canvas).remove();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(message)
		},
		fnUpdateBarChart:function(){
			    var asessmentMetricsReportData=graphData;
                var result=asessmentMetricsReportData!=null?asessmentMetricsReportData.length>0?true:false:false;
				var labels= ["Attempted", "Scored > 90%", "Scored < 90%"];
				var  dataSets =[];
                var labelsValue= ["Enrolled", "Appeared", "Absent","Pending","Scored >= 90%","Scored < 90%"];
				var  dataSetsValue =[];
                var labelsTaxonomy= ["K", "U", "O","An","Ap"];
				var  dataSetsTaxonomy =[];
				var  dataSetsTaxonomyForValue =[];
				if(result==true){

					$(studentPerformanceAssessmentWisePercentage.tbody).children("tr").remove();
					$(studentPerformanceAssessmentWiseValue.tbody).children("tr").remove();
					$(studentPerformanceAssessmentWiseTaxonomy.tbody).children("tr").remove();
					$(studentPerformanceAssessmentWiseTaxonomyValue.tbody).children("tr").remove();


					$.each(asessmentMetricsReportData, function(index,assessmentMetricsData) {	
						var row=studentPerformanceAssessmentWiseBarChart.createStudentPerformanceAssessmentWisePercentageRow(assessmentMetricsData);

						$(studentPerformanceAssessmentWisePercentage.tbody).append(row);
						var gData ={
								type: 'bar',
								label: assessmentMetricsData.assessmentName,
								backgroundColor: randomColor(),
								data: [assessmentMetricsData.percentageOfStudentsAttempted.totalStudent, assessmentMetricsData.percentageOfStudentsScoredGTEQ90.totalStudent, assessmentMetricsData.percentageOfStudentsScoredLTEQ90.totalStudent]
						}
						dataSets.push(gData);


						var performanceValueRow=studentPerformanceAssessmentWiseBarChart.createStudentPerformanceAssessmentWiseValueRow(assessmentMetricsData);

						$(studentPerformanceAssessmentWiseValue.tbody).append(performanceValueRow);

						var performanceValueData ={
								type: 'bar',
								label: assessmentMetricsData.assessmentName,
								backgroundColor: randomColor(),
								data: [assessmentMetricsData.numberOfStudentsEnrolled.totalStudent, 
								       assessmentMetricsData.totalNumberOfStudentsAppearedInAssessment.totalStudent, 
								       assessmentMetricsData.numberOfStudentsAbsentForAssessment.totalStudent,
								       assessmentMetricsData.numberOfStudentsYetToTakeAssessment.totalStudent, 
								       assessmentMetricsData.numberOfStudentsScoredGTEQ90.totalStudent, 
								       assessmentMetricsData.numberOfStudentsScoredLTEQ90.totalStudent]
						}
						dataSetsValue.push(performanceValueData);
						////////////////////////taxonomy value/////////////////////
						var totalStudentAppeared=assessmentMetricsData.totalNumberOfStudentsAppearedInAssessment.totalStudent;
						
						var taxonomyScoreMapData=assessmentMetricsData.totalTaxonomyScoreMap;
						var kTaxnomy=taxonomyScoreMapData.hasOwnProperty('Knowledge')==true?taxonomyScoreMapData.Knowledge:0;
						var uTaxnomy=taxonomyScoreMapData.hasOwnProperty('Understanding')==true?taxonomyScoreMapData.Understanding:0;
						var oTaxnomy=taxonomyScoreMapData.hasOwnProperty('Operation')==true?taxonomyScoreMapData.Operation:0;
						var anTaxnomy=taxonomyScoreMapData.hasOwnProperty('Analysis')==true?taxonomyScoreMapData.Analysis:0;
						var apTaxnomy=taxonomyScoreMapData.hasOwnProperty('Application')==true?taxonomyScoreMapData.Application:0;
						var totalKnowledgeScore=kTaxnomy*totalStudentAppeared;
						var totalUnderstandingScore=uTaxnomy*totalStudentAppeared;
						var totalOperationScore=oTaxnomy*totalStudentAppeared;
						var totalAnalysisScore=anTaxnomy*totalStudentAppeared;
						var totalApplicationScore=apTaxnomy*totalStudentAppeared;

						var taxonomyValueRow=   studentPerformanceAssessmentWiseBarChart.createStudentPerformanceAssessmentWiseTaxonomyValueRow(assessmentMetricsData);
						var taxonomyValueTempRow='<tr> <td></td> <td></td>  <td></td> <td></td>  <td>Total</td> <td>'+totalKnowledgeScore+'</td> <td>'+totalUnderstandingScore+'</td> <td>'+totalOperationScore+'</td> <td>'+totalAnalysisScore+'</td> <td>'+totalApplicationScore+'</td></tr>'


						
							$(studentPerformanceAssessmentWiseTaxonomyValue.tbody).append(taxonomyValueTempRow);
							$(studentPerformanceAssessmentWiseTaxonomyValue.tbody).append(taxonomyValueRow);
						
						var taxonomyValueData ={
								type: 'bar',
								label: assessmentMetricsData.assessmentName,
								backgroundColor: randomColor(),
								data: [assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_U,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_O,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_An,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap]
						}
						dataSetsTaxonomyForValue.push(taxonomyValueData);

						//////////////////// taxonomy wise/////////////////////////
						var secondTableRow='';


						var scoredPercentageKnowledge=0;
						var scoredPercentageUnderstanding=0;
						var scoredPercentageOperation=0;
						var scoredPercentageAnalysis=0;
						var scoredPercentageApplication=0;

						if(totalKnowledgeScore!=0){
							scoredPercentageKnowledge=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K)/(totalKnowledgeScore!=0?totalKnowledgeScore:1)).toFixed(2);
							scoredPercentageUnderstanding=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_U)/(totalUnderstandingScore!=0?totalUnderstandingScore:1)).toFixed(2);
							scoredPercentageOperation=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_O)/(totalOperationScore!=0?totalOperationScore:1)).toFixed(2);
							scoredPercentageAnalysis=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_An)/(totalAnalysisScore!=0?totalAnalysisScore:1)).toFixed(2);
							scoredPercentageApplication=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap)/(totalApplicationScore!=0?totalApplicationScore:1)).toFixed(2);
						}

                         var totalK=100*(totalKnowledgeScore/(totalKnowledgeScore!=0?totalKnowledgeScore:1)).toFixed(0);
                         var totalU=100*(totalUnderstandingScore/(totalUnderstandingScore!=0?totalUnderstandingScore:1)).toFixed(0);
                         var totalO=100*(totalOperationScore/(totalOperationScore!=0?totalOperationScore:1)).toFixed(0);
                         var totalAn=100*(totalAnalysisScore/(totalAnalysisScore!=0?totalAnalysisScore:1)).toFixed(0);
                         var totalAp=100*(totalApplicationScore/(totalApplicationScore!=0?totalApplicationScore:1)).toFixed(0);
                         var tempRowPercentage='<tr> <td></td> <td></td><td></td> <td></td>  <td></td> <td></td>  <td>Total %</td> <td>'+totalK+'%'+'</td> <td>'+totalU+'%'+'</td> <td>'+totalO+'%'+'</td> <td>'+totalAn+'%'+'</td> <td>'+totalAp+'%'+'</td></tr>'
	
							
							secondTableRow='<tr  id="'+assessmentMetricsData.assessmentId+'">'+
							'<td title="'+assessmentMetricsData.assessmentName+'" data-assessmentId="'+assessmentMetricsData.id+'">'+assessmentMetricsData.assessmentName+'</td>'+
							'<td title="'+assessmentMetricsData.campaignName+'" >'+assessmentMetricsData.campaignName+'</td>'+
							'<td title="'+assessmentMetricsData.status+'" >'+assessmentMetricsData.status+'</td>'+
							'<td title="'+assessmentMetricsData.gradeName+'" >'+assessmentMetricsData.gradeName+'</td>'+
							'<td title="'+assessmentMetricsData.stageName+'" >'+assessmentMetricsData.stageName+'</td>'+
							'<td title="'+assessmentMetricsData.subjectName+'" >'+assessmentMetricsData.subjectName+'</td>'+
							'<td> Scored % </td>'+
							'<td title="'+scoredPercentageKnowledge+'" >'+scoredPercentageKnowledge+'%'+'</td>'+
							'<td title="'+scoredPercentageUnderstanding+'" >'+scoredPercentageUnderstanding+'%'+'</td>'+
							'<td title="'+scoredPercentageOperation+'" >'+scoredPercentageOperation+'%'+'</td>'+
							'<td title="'+scoredPercentageAnalysis+'" >'+scoredPercentageAnalysis+'%'+'</td>'+
							'<td title="'+scoredPercentageApplication+'" >'+scoredPercentageApplication+'%'+'</td>'+
							'</tr>';
						$(studentPerformanceAssessmentWiseTaxonomy.tbody).append(tempRowPercentage);

						$(studentPerformanceAssessmentWiseTaxonomy.tbody).append(secondTableRow);

						var taxonomyPercentageData ={
								type: 'bar',
								label: assessmentMetricsData.assessmentName,
								backgroundColor: randomColor(),
								data: [scoredPercentageKnowledge,scoredPercentageUnderstanding,scoredPercentageOperation,scoredPercentageAnalysis,scoredPercentageApplication]
						}
						dataSetsTaxonomy.push(taxonomyPercentageData);
					});

					var studentPerformanceAssessmentWiseValueBarChartData = {
							labels : labelsValue,
							datasets : dataSetsValue
					};

					var studentPerformanceAssessmentWisePercentageBarChartData = {
							labels : labels,
							datasets : dataSets
					};
					var studentPerformanceAssessmentWiseTaxonomyBarChartData = {
							labels : labelsTaxonomy,
							datasets : dataSetsTaxonomy
					};
					var studentPerformanceAssessmentWiseTaxonomyValueBarChartData = {
							labels : labelsTaxonomy,
							datasets : dataSetsTaxonomyForValue
					};

					var studentPerformanceAssessmentWisePercentageBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWisePercentageBarChartData,"Percentage");
					var studentPerformanceAssessmentWiseTaxonomyBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWiseTaxonomyBarChartData,"Percentage");
					var studentPerformanceAssessmentWiseValueBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWiseValueBarChartData,"Value");
					var studentPerformanceAssessmentWiseTaxonomyValueBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWiseTaxonomyValueBarChartData,"Value");
					studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWisePercentage,studentPerformanceAssessmentWisePercentageBarChartDataConfig);
					if(superRoleCode=="T" || superRoleCode=="RC" || superRoleCode=="FS"){
					studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWiseValue,studentPerformanceAssessmentWiseValueBarChartDataConfig);
					studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWiseTaxonomyValue,studentPerformanceAssessmentWiseTaxonomyValueBarChartDataConfig);
					}
					studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWiseTaxonomy,studentPerformanceAssessmentWiseTaxonomyBarChartDataConfig);

				}
				else{
					$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
					studentPerformanceAssessmentWiseBarChart.fnEmptyCanvas(emptyChartMessage,studentPerformanceAssessmentWisePercentage.canvas,studentPerformanceAssessmentWisePercentage.canvasSpan);
				}
			
		},
		createStudentPerformanceAssessmentWisePercentageRow:function(assessmentData){

			var row='<tr  id="'+assessmentData.assessmentId+'">'+
			'<td title="'+assessmentData.assessmentName+'" data-assessmentId="'+assessmentData.id+'">'+assessmentData.assessmentName+'</td>'+
			'<td title="'+assessmentData.gradeName+'" >'+assessmentData.gradeName+'</td>'+
			'<td title="'+assessmentData.stageName+'" >'+assessmentData.stageName+'</td>'+
			'<td title="'+assessmentData.subjectName+'" >'+assessmentData.subjectName+'</td>'+
			'<td title="'+assessmentData.percentageOfStudentsAttempted.totalStudent+'" >'+assessmentData.percentageOfStudentsAttempted.totalStudent+'% </td>'+
			'<td title="'+assessmentData.percentageOfStudentsScoredGTEQ90.totalStudent+'" >'+assessmentData.percentageOfStudentsScoredGTEQ90.totalStudent+'% </td>'+
			'<td title="'+assessmentData.percentageOfStudentsScoredLTEQ90.totalStudent+'" >'+assessmentData.percentageOfStudentsScoredLTEQ90.totalStudent+'% </td>'+
			'</tr>';
			return row;

		},
		createStudentPerformanceAssessmentWiseValueRow:function(assessmentValueData){
			var studentEnroldedTD="";
			var studentAppearedTD="";
			var studentAbsentTD="";
			var studentYetToTakeTD="";
			var studentGTETD="";
			var studentLTETD="";

			studentEnroldedTD='<td title="'+assessmentValueData.numberOfStudentsEnrolled.totalStudent+'" >'+assessmentValueData.numberOfStudentsEnrolled.totalStudent+'</td>';
			studentGTETD='<td title="'+assessmentValueData.numberOfStudentsScoredGTEQ90.totalStudent+'" >'+assessmentValueData.numberOfStudentsScoredGTEQ90.totalStudent+'</td>';
			studentAppearedTD='<td title="'+assessmentValueData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'" >'+assessmentValueData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'</td>';
			studentLTETD='<td title="'+assessmentValueData.numberOfStudentsScoredLTEQ90.totalStudent+'" >'+assessmentValueData.numberOfStudentsScoredLTEQ90.totalStudent+'</td>';
			studentYetToTakeTD=	'<td title="'+assessmentValueData.numberOfStudentsYetToTakeAssessment.totalStudent+'" >'+assessmentValueData.numberOfStudentsYetToTakeAssessment.totalStudent+'</td>';
			studentAbsentTD='<td title="'+assessmentValueData.numberOfStudentsAbsentForAssessment.totalStudent+'" >'+assessmentValueData.numberOfStudentsAbsentForAssessment.totalStudent+'</a></td>';

			var row ='<tr  id="'+assessmentValueData.assessmentId+'">'+
			'<td title="'+assessmentValueData.assessmentName+'" data-assessmentId="'+assessmentValueData.id+'">'+assessmentValueData.assessmentName+'</td>'+
			'<td title="'+assessmentValueData.gradeName+'" >'+assessmentValueData.gradeName+'</td>'+
			'<td title="'+assessmentValueData.stageName+'" >'+assessmentValueData.stageName+'</td>'+
			'<td title="'+assessmentValueData.subjectName+'" >'+assessmentValueData.subjectName+'</td>'+
			studentEnroldedTD+studentAppearedTD+ studentAbsentTD+studentYetToTakeTD+studentGTETD+ studentLTETD+
			'</tr>';


			return row
		},
		createStudentPerformanceAssessmentWiseTaxonomyValueRow:function(assessmentTaxonomyMetricsData){
			var row='<tr  id="'+assessmentTaxonomyMetricsData.assessmentId+'">'+

			'<td title="'+assessmentTaxonomyMetricsData.assessmentName+'" data-assessmentId="'+assessmentTaxonomyMetricsData.id+'">'+assessmentTaxonomyMetricsData.assessmentName+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.gradeName+'" >'+assessmentTaxonomyMetricsData.gradeName+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.stageName+'" >'+assessmentTaxonomyMetricsData.stageName+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.subjectName+'" >'+assessmentTaxonomyMetricsData.subjectName+'</td>'+
			'<td>Scored</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_U+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_U+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_O+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_O+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_An+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_An+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap+'</td>'+
			'</tr>';

			return row;
		},
		createChartData:function(chartElement,configData){
			$(chartElement.canvas).remove();
			$(chartElement.canvasDiv).append('<canvas id="'+chartElement.canvasElement+'" />');
			var canvas = document.getElementById(chartElement.canvasElement);
			var context = canvas.getContext('2d');
			context.clear();
			window.myBar = new Chart(context, configData);
			window.myBar.update();
		},
         barChartDataConfig:function(data,label){
			var studentPerformanceBarChartDataConfig = {
					type: 'bar',
					data: data,
					options: {

						scales: {
							xAxes: [{
								display: true,
								gridLines: {
									display: false
								},
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: false
								},
								ticks: {
									beginAtZero: true,
									steps: 10,
									stepValue: 5,
								},
								scaleLabel: {
									display: true,
									labelString: label
								}
							}]
						},


						elements: {
							rectangle: {
								borderWidth: 0.5,
								borderColor: randomColor(),
								borderSkipped: 'bottom'
							}
						},
						responsive: true,
						legend: {
							position: 'bottom',
						},

					}


			}
			return studentPerformanceBarChartDataConfig;
		}

}




var studentPerformanceAssessmentWiseInIt=function(){
	studentPerformanceAssessmentWiseBarChart.fnUpdateBarChart();
	
}