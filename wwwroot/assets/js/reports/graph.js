
//initialize global vars
var fnInitGlobalVars =function(){
	reportId='';reportName='';
	reportQuery='';isLocation=false;
	locationType='';
	
	sourceIds =[];stageIds=[];
	gradeIds =[];subjectIds =[];
	schoolIds =[];assessmentIds=[];	
	locationIds=[];campaignIds =[];
}


//view reports on click
var getChartData =function(){
	fnInitGlobalVars();
	fnGetReportRequestData();
	
	var jsonData={
		"reportQuery":	queryName,
		"sourceIds":sourceIds,
		"stageIds":stageIds,
		"gradeIds":gradeIds,
		"subjectIds":subjectIds,
		"schoolIds":schoolIds,
		"campaignIds":campaignIds
	};
	reportData =customAjaxCalling("report/multiseries/bar/",jsonData,'POST')
	
	/*var dataSet =[];
	$.each(reportData.seriesData,function(i,val){
		dataSet.push(JSON.stringify(val));
	});*/
	
	barChart(reportData);
	showDiv($('#graphDiv'));
	
}

//get the report request data
var fnGetReportRequestData =function(){
	
	reportId =$(pageContextElements.report).find('option:selected').data('id')
	reportName =$(pageContextElements.report).find('option:selected').data('id')
	reportQuery =$(pageContextElements.report).find('option:selected').data('qname')
	isLocation =$(pageContextElements.report).find('option:selected').data('id')
	locationType =$(pageContextElements.report).find('option:selected').data('id')
	sourceIds =[];stageIds=[];
	gradeIds =[];subjectIds =[];
	schoolIds =[];assessmentIds=[];campaignIds =[];
	
	pushToArray((pageContextElements.school),schoolIds);
	pushToArray((pageContextElements.grade),gradeIds);
	pushToArray((pageContextElements.subject),subjectIds);
	pushToArray((pageContextElements.stage),stageIds);
	pushToArray((pageContextElements.source),sourceIds);
	pushToArray((pageContextElements.campaign),campaignIds);
	
	console.log(schoolIds);
	console.log(gradeIds);
	console.log(subjectIds);
	console.log(stageIds);
	console.log(sourceIds);
}
var pushToArray =function(element,arr){
	$(element).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all"){
				arr.push(parseInt($(option).data('id')));
			}
		});
	});
}


var barChart =function(reportData){
	//showDiv($('#barChartContainer'));
		
	var categories = (reportData!=null&&reportData!="")?$.parseJSON(reportData.categories):"";
	var dataSet =(reportData!=null&&reportData!="")?$.parseJSON(reportData.dataSet):"";
		
	FusionCharts.ready(function(){	
		displayBarChart = new FusionCharts({
	        //type: 'scrollcolumn2d',
			type: 'mscolumn2d',			
	        dataFormat: 'json',
	        renderAt: 'barChartContainer',
	        width:'700',
	        height:'400',
	        dataSource: {
	            "chart": {
	                "caption": reportTitle,
	                "xAxisname": "Assessment",
	                "yAxisName": "Avg. assessment score ",	                
	                "plotFillAlpha" : "80",
	                //Cosmetics
	                "paletteColors" : "#0075c2,#1aaf5d",
	                "baseFontColor" : "#333333",
	                "baseFont" : "Helvetica Neue,Arial",
	                "captionFontSize" : "14",
	                "subcaptionFontSize" : "14",
	                "subcaptionFontBold" : "0",
	                "showBorder" : "0",
	                "bgColor" : "#ffffff",
	                "showShadow" : "0",
	                "canvasBgColor" : "#ffffff",
	                "canvasBorderAlpha" : "0",
	                "divlineAlpha" : "100",
	                "divlineColor" : "#999999",
	                "divlineThickness" : "1",
	                "divLineIsDashed" : "1",
	                "divLineDashLen" : "1",
	                "divLineGapLen" : "1",
	                "usePlotGradientColor" : "0",
	                "showplotborder" : "0",
	                "valueFontColor" : "#ffffff",
	                "placeValuesInside" : "1",
	                "showHoverEffect" : "1",
	                "rotateValues" : "1",
	                "showXAxisLine" : "1",
	                "xAxisLineThickness" : "1",
	                "xAxisLineColor" : "#999999",
	                "showAlternateHGridColor" : "0",
	                "legendBgAlpha" : "0",
	                "legendBorderAlpha" : "0",
	                "legendShadow" : "0",
	                "legendItemFontSize" : "10",
	                "legendItemFontColor" : "#666666"
	            },
	            "categories": categories,
	            "dataset":dataSet 
	            	/*[
	                {
	                    "seriesname": "Boys",
	                    "data": reportData.series1Data
	                }, 
	                {
	                    "seriesname": "Girls",
	                    "data": reportData.series2Data
	                }
	            ],*/
	        }
	     
	    });
		displayBarChart.render();
		$("#graphPanel").show();
	});	
}
