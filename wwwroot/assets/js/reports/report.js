//select divs
var pageContextDivs =new Object();
	pageContextDivs.report ='#divReport';
	pageContextDivs.campaign ='#campaignDiv';
	pageContextDivs.school ='#schoolDiv';
	pageContextDivs.grade ='#gradeDiv';
	pageContextDivs.section ='#sectionDiv';
	pageContextDivs.subject ='#subjectDiv';
	pageContextDivs.source ='#sourceDiv';
	pageContextDivs.table ='#tableDiv';
// select location divs
var pageContextLocDivs =new Object();	
    pageContextLocDivs.state='#stateDiv'
    pageContextLocDivs.zone='#zoneDiv'
    pageContextLocDivs.district='#districtDiv'

//select elements
var pageContextElements =new Object();
	//pageContextElements.report ='#selectBox_report';
	pageContextElements.campaign ='#selectBox_campaign';
	pageContextElements.assessment ='#selectBox_Assessment';
	pageContextElements.school ='#selectBox_school';
	pageContextElements.grade ='#selectBox_grade';
	//pageContextElements.section ='#selectBox_section';
	pageContextElements.subject ='#selectBox_subject';
	pageContextElements.stage ='#selectBox_stage';
	pageContextElements.source ='#selectBox_source';
	pageContextElements.button ='#viewReport';


	var pageContextLocElements =new Object();
	    pageContextLocElements.state='#statesForZone';
	    pageContextLocElements.zone='#selectBox_zonesByState';
	    pageContextLocElements.district='#selectBox_districtsByZone';
	
	
	
	
//init global vars
var initGlobalVars =function(){
	selectedCampaign ='';	
}


//on change of campaign, get the assessment....etc details
$(pageContextElements.campaign).on('change',function(){
	if(queryName=='1_STUDENT_PERFORMANCE_BOYS_VS_GIRLS'){
		getFilterDataForreport1();
	}
		else if (queryName=='2_STUDENT_PERFORMANCE_DISTRICT_BOYS_VS_GIRLS'){
			getFilterDataForreport2();
		}
		else if (queryName=='3_STUDENT_PERFORMANCE_BY_BLOOM'){
			getFilterDataForreport3();
		}
		else if (queryName=='4_ATTENDENCE_VS_TEST_SCORE'){
			getFilterDataForreport4();
		}
});
//bind filter data for report4

var getFilterDataForreport1=function(){
	
	resetFormById('grpahFrom');
	showDiv($('#filters'));showDiv($('#viewBtnDiv'));
	hideDiv($('#graphDiv'));
	
	selectedCampaign =$(pageContextElements.campaign).find('option:selected').data('id')
	var reportFilterData =customAjaxCalling("report/filters/"+selectedCampaign, null,'GET');
	console.log(reportFilterData);
	
	if(reportFilterData.response=="shiksha-200"){
		fnEmptyAll(pageContextElements.school,pageContextElements.grade,
		pageContextElements.subject,pageContextElements.source,pageContextElements.stage);
		fnBindSchools(pageContextElements.school,reportFilterData.schools);
		fnBindGrades(pageContextElements.grade,reportFilterData.grades);
		fnBindSubjects(pageContextElements.subject,reportFilterData.subjects);
		fnBindStages(pageContextElements.stage,reportFilterData.stages);
		fnBindSources(pageContextElements.source,reportFilterData.sources);
		
	}
	console.log(reportFilterData);
	
}
//bind filter data for report2
var getFilterDataForreport2=function(){
	hideDiv($('#graphDiv'));
	resetFormById('grpahFrom');
	showDiv($('#filters'));showDiv($('#viewBtnDiv'));showDiv($('#locationDivFilter'));
	
	
	selectedCampaign =$(pageContextElements.campaign).find('option:selected').data('id')
	var reportFilterData =customAjaxCalling("report/filters/"+selectedCampaign, null,'GET');
	console.log(reportFilterData);
	
	if(reportFilterData.response=="shiksha-200"){
		fnEmptyAll(pageContextElements.school,pageContextElements.grade,
					pageContextElements.subject,pageContextElements.source,pageContextElements.stage);
		fnBindSchools(pageContextElements.school,reportFilterData.schools);
		fnBindGrades(pageContextElements.grade,reportFilterData.grades);
		fnBindSubjects(pageContextElements.subject,reportFilterData.subjects);
		fnBindStages(pageContextElements.stage,reportFilterData.stages);
		fnBindSources(pageContextElements.source,reportFilterData.sources);
		fnBindStates(pageContextLocElements.state,reportFilterData.states);
		fnBindDistricts(pageContextLocElements.district,reportFilterData.districts);
		fnBindZones(pageContextLocElements.zone,reportFilterData.zones);
		
	}
}

// bind filter data for report3
var getFilterDataForreport3=function(){
	hideDiv($('#graphDiv'));
	resetFormById('grpahFrom');
	showDiv($('#filters'));showDiv($('#viewBtnDiv'));showDiv($('#locationDivFilter'));
	
	
	selectedCampaign =$(pageContextElements.campaign).find('option:selected').data('id')
	var reportFilterData =customAjaxCalling("report/filters/"+selectedCampaign, null,'GET');
	console.log(reportFilterData);
	
	if(reportFilterData.response=="shiksha-200"){
		fnEmptyAll(pageContextElements.school,pageContextElements.grade,
					pageContextElements.subject,pageContextElements.source,pageContextElements.stage);
		fnBindAssessment(pageContextElements.assessment,reportFilterData.assessments);
		fnBindSchools(pageContextElements.school,reportFilterData.schools);
		fnBindGrades(pageContextElements.grade,reportFilterData.grades);
		fnBindSubjects(pageContextElements.subject,reportFilterData.subjects);
		fnBindStages(pageContextElements.stage,reportFilterData.stages);
		fnBindSources(pageContextElements.source,reportFilterData.sources);
		fnBindSchools(pageContextElements.school,reportFilterData.schools);
		
	}
}

//bind filter data for report4
var getFilterDataForreport4=function(){
	hideDiv($('#graphDiv'));
	resetFormById('grpahFrom');
	showDiv($('#filters'));showDiv($('#viewBtnDiv'));showDiv($('#locationDivFilter'));
	
	
	selectedCampaign =$(pageContextElements.campaign).find('option:selected').data('id')
	var reportFilterData =customAjaxCalling("report/filters/"+selectedCampaign, null,'GET');
	console.log(reportFilterData);
	
	if(reportFilterData.response=="shiksha-200"){
		fnEmptyAll(pageContextElements.school,pageContextElements.grade,
					pageContextElements.subject,pageContextElements.source,pageContextElements.stage);
		fnBindAssessment(pageContextElements.assessment,reportFilterData.assessments);
		fnBindSchools(pageContextElements.school,reportFilterData.schools);
		fnBindGrades(pageContextElements.grade,reportFilterData.grades);
		fnBindSubjects(pageContextElements.subject,reportFilterData.subjects);
		fnBindStages(pageContextElements.stage,reportFilterData.stages);
		fnBindSources(pageContextElements.source,reportFilterData.sources);
		fnBindStates(pageContextLocElements.state,reportFilterData.states);
		fnBindDistricts(pageContextLocElements.district,reportFilterData.districts);
		fnBindZones(pageContextLocElements.zone,reportFilterData.zones);
		
	}
}
//view reports on click




//empty all select box
var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}
var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');	
}
//bind assessments to respective select box
var fnBindAssessment =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.assessmentId + '"data-name="' + escapeHtmlCharacters(obj.assessmentName)+ '">' +escapeHtmlCharacters(obj.assessmentName)+ '</option>');
	});
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind schools to respective select box
var fnBindSchools =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.id + '"data-name="' + escapeHtmlCharacters(obj.schoolName)+ '">' +escapeHtmlCharacters(obj.schoolName)+ '</option>');
	});
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind grades to respective select box
var fnBindGrades =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.gradeId + '"data-name="' + escapeHtmlCharacters(obj.gradeName)+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
	});
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind subjects to respective select box
var fnBindSubjects =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.subjectId + '"data-name="' + escapeHtmlCharacters(obj.subjectName)+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
	});
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind sections to respective select box
var fnBindSections =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.sectionId + '"data-name="' + escapeHtmlCharacters(obj.sectionName)+ '">' +escapeHtmlCharacters(obj.sectionName)+ '</option>');
	});
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind stages to respective select box
var fnBindStages =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.stageId + '"data-name="' + escapeHtmlCharacters(obj.stageName)+ '">' +escapeHtmlCharacters(obj.stageName)+ '</option>');
	});	
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind stages to respective select box
var fnBindSources =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.sourceId + '"data-name="' + escapeHtmlCharacters(obj.sourceName)+ '">' +escapeHtmlCharacters(obj.sourceName)+ '</option>');
	});	
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind states to respective select box
var fnBindStates =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.stateId + '"data-name="' + escapeHtmlCharacters(obj.stateName)+ '">' +escapeHtmlCharacters(obj.stateName)+ '</option>');
	});
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind zones to respective select box
var fnBindZones =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.zoneId + '"data-name="' + escapeHtmlCharacters(obj.zoneName)+ '">' +escapeHtmlCharacters(obj.zoneName)+ '</option>');
	});	
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//bind district to respective select box
var fnBindDistricts =function(element,data){
	$ele =$(element);	
	$.each(data,function(index,obj){		
		$ele.append('<option data-id="' + obj.districtId + '"data-name="' + escapeHtmlCharacters(obj.districtName)+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
	});	
	destroyRefresh(element);
	setOptionsForAllMultipleSelect($(element));
}
//document ready
$(function() {
	
	
	
	
	initGlobalVars();
	
	$("#grpahFrom").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields:{
			grade: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(pageContextElements.grade).val();
							return (option != null );
						}
					}
				}
			},
			subject: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(pageContextElements.subject).val();
							return (option != null);
						}
					}
				}
			},
			
			stage: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(pageContextElements.stage).val();
							return (option != null);
						}
					}
				}
			},
			source: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(pageContextElements.source).val();
							return (option != null);
						}
					}
				}
			},
			school: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(pageContextElements.school).val();
							return (option != null);
						}
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
	      e.preventDefault();
	      getChartData();
	}).on('err.form.fv', function(e) {
	      e.preventDefault();
	      $(pageContextElements.button).css('display','block');	
    });
});
