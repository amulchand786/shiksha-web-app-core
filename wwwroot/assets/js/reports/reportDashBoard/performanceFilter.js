//global selectDivIds for smart filter
var commonFilterPageContext = new Object();	
	commonFilterPageContext.city 	="#divSelectCity";
	commonFilterPageContext.block 	="#divSelectBlock";
	commonFilterPageContext.NP 	="#divSelectNP";
	commonFilterPageContext.GP 	="#divSelectGP";
	commonFilterPageContext.RV 	="#divSelectRV";
	commonFilterPageContext.village ="#divSelectVillage";
	commonFilterPageContext.school ="#divSelectSchool";
	commonFilterPageContext.grade ="#divSelectGrade";
	commonFilterPageContext.subject ="#divSelectSubject";
	
	
	
	
	commonFilterPageContext.elementVillage ="#selectBox_villagebyRv";
	commonFilterPageContext.elementSchool ="#selectBox_schoolsByLoc";
	commonFilterPageContext.eleGrade ="#selectBox_gradesBySchool";
	commonFilterPageContext.eleSubject ="#selectBox_subject";
	
	
	commonFilterPageContext.resetLocDivVillage ="#divResetLocVillage";
	commonFilterPageContext.plannerStartDate="#startDatePicker";
	commonFilterPageContext.plannerEndDate="#endDatePicker";
	
	var fnInitDatePicker =function(pDatePickerId){
		 $(pDatePickerId).datepicker({
				allowPastDates: true,
				momentConfig : {
					culture : 'en', // change to specific culture
					format : 'DD-MMM-YYYY' // change for specific format
				},
			})
	}
//school location type on change...get locations...
$('#divSchoolLocType input:radio[name=locType]').change(function() {
		
			fnHideContainer();
			$('#locColspFilter').removeClass('in');
			$('#locationPnlLink').text('Select Location');		
			$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');

			
			lObj =this;
			typeCode =$(this).data('code');		
			$('.outer-loader').show();		
			reinitializeSmartFilter(elementIdMap);	
			$("#hrDiv").show();
			if(typeCode.toLowerCase() =='U'.toLowerCase()){
				showDiv($(commonFilterPageContext.city),$(commonFilterPageContext.resetLocDivCity));
				hideDiv($(commonFilterPageContext.block),$(commonFilterPageContext.NP),$(commonFilterPageContext.GP),$(commonFilterPageContext.RV),$(commonFilterPageContext.village));
				displaySmartFilters($(this).data('code'),$(this).data('levelname'));
				selectedLocType ="U";
				positionFilterDiv("U");
			}else{
				hideDiv($(commonFilterPageContext.city),$(commonFilterPageContext.resetLocDivCity));
				showDiv($(commonFilterPageContext.block),$(commonFilterPageContext.NP),$(commonFilterPageContext.GP),$(commonFilterPageContext.RV),$(commonFilterPageContext.village));
				displaySmartFilters($(this).data('code'),$(this).data('levelname'));
				selectedLocType ="R";
				positionFilterDiv("R");
			}
			
			fnCollapseMultiselect();
			$('.outer-loader').hide();
			return;
	});


//multiselect for grade and subject
var fnMultiselectForGradeAndSubject = function(arg){
	$(arg).multiselect({
		maxHeight: 200,
		includeSelectAllOption: true,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,	
		filterPlaceholder: 'Search here...',
		onDropdownHide : function(event) {
			if (arg == (commonFilterPageContext.eleGrade)) {
				if ($(commonFilterPageContext.eleGrade).find('option:selected').length == 0) {
					fnHideContainer();
				}
			}else if (arg == (commonFilterPageContext.eleSubject)) {
				if ($(commonFilterPageContext.eleSubject).find('option:selected').length == 0) {
					fnHideContainer();
				}
			}				
		}
	});
	fnCollapseMultiselect();
}





$(function() {
	
	
	fnMultiselectForGradeAndSubject(commonFilterPageContext.eleGrade );
	fnMultiselectForGradeAndSubject(commonFilterPageContext.eleSubject);
	
	fnInitDatePicker(commonFilterPageContext.plannerStartDate);
	fnInitDatePicker(commonFilterPageContext.plannerEndDate);
	
	
	
	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-minus-sign red gi-1p5x"></span></a>');
	});
	
	
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	});
	
	$('#pnlMappingElements').on('show.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('Hide school configuration');
	});  
	$('#pnlMappingElements').on('hide.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('View school configuration');
	});
	
	
	
	
	
	
	
	
	
	$("#frmCommonFilter").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields:{
			grade: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(commonFilterPageContext.eleGrade).find('option:selected').val();
							return (option != null&& option!="NONE");
						}
					}
				}
			},
			subject : {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(commonFilterPageContext.eleSubject).find('option:selected').val();
							return (option != null&& option!="NONE");
						}
					}
				}
			},
			/*village: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(commonFilterPageContext.elementVillage).find('option:selected').val();
							return (option != null && option!="NONE");
						}
					}
				}
			},*/
			school: {
				validators : {
					callback : {
						message : '      ',
						callback : function(value, validator, $field) {
							// Get the selected options
							var option = $(commonFilterPageContext.elementSchool).find('option:selected').val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			locType: {
	                validators: {
	                    notEmpty: {
	                        message: ' '
	                    }
	                }
	        },
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		/*$('#btnViewReport').prop('disabled',false);
		$('#btnViewReport').removeClass('disabled');*/
		fnShowContainer();
	}).on('error.form.fv', function(e) {
		e.preventDefault();
		fnHideContainer();
	});
	
	
	
});


var fncheckAndUpdateGradeSubjectFV =function(){
	var fvGrade = $(commonFilterPageContext.eleGrade).find('option:selected').val();
	var fvSubject = $(commonFilterPageContext.eleSubject).find('option:selected').val();
	if(fvGrade != null && fvGrade!="NONE"){
		fnUpdateFVElementStatus("frmCommonFilter",[commonFilterPageContext.eleGrade],"VALID");
	}
	if(fvSubject != null && fvSubject!="NONE"){
		fnUpdateFVElementStatus("frmCommonFilter",[commonFilterPageContext.eleSubject],"VALID");
	}
	
}





//////////////////////
//Smart filter		//
//////////////////////

//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionFilterDiv =function(type){
	if(type=="R"){
		$(commonFilterPageContext.NP).insertAfter( $(commonFilterPageContext.block));
		$(commonFilterPageContext.RV).insertAfter( $(commonFilterPageContext.GP));
		$(commonFilterPageContext.school).insertAfter( $(commonFilterPageContext.village));
	}else{
		$(commonFilterPageContext.school).insertAfter( $(commonFilterPageContext.city));
	}
}
//displaying smart-filters
var displaySmartFilters =function(lCode,lLevelName){
	$(".outer-loader").show();
	elementIdMap = {
			"State" : '#statesForZone',
			"Zone" : '#selectBox_zonesByState',
			"District" : '#selectBox_districtsByZone',
			"Tehsil" : '#selectBox_tehsilsByDistrict',
			"Block" : '#selectBox_blocksByTehsil',
			"City" : '#selectBox_cityByBlock',
			"Nyaya Panchayat" : '#selectBox_npByBlock',
			"Gram Panchayat" : '#selectBox_gpByNp',
			"Revenue Village" : '#selectBox_revenueVillagebyGp',
			"Village" : '#selectBox_villagebyRv',
			"School" : '#selectBox_schoolsByLoc'
	};
	displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){$(".outer-loader").hide(); $('#rowDiv1,#divFilter').show(); }, 100);	
};


//reset only location when click on reset button icon
var resetLocAndSchool =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
	//doResetLocation();
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');

	reinitializeSmartFilter(elementIdMap);
	var aLocTypeCode =$('#divSchoolLocType input:radio[name=locType]:checked').data('code');
	var aLLevelName =$('#divSchoolLocType input:radio[name=locType]:checked').data('levelname');
	displaySmartFilters(aLocTypeCode,aLLevelName);
	fnHideContainer();

	fnCollapseMultiselect();

}


//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
}