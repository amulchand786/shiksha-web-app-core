var chartYaxisName='Value';
//////////////////////// Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";
var selectSchoolMessage="Please select atleast one School. ";
var selectGradeMessage="Please select atleast one Grade. ";
var selectSubjectMessage="Please select atleast one Subject. ";


var studentPerformanceStageWiseAndGenderWise= new Object();

studentPerformanceStageWiseAndGenderWise.chartTypeDiv="#chartType_dropBoxStudentPerformanceStageWiseAndGenderWiseDiv";
studentPerformanceStageWiseAndGenderWise.chartType="#chartType_dropBoxStudentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.dataTypeDiv="#dataType_dropBoxStudentPerformanceStageWiseAndGenderWiseDiv";
studentPerformanceStageWiseAndGenderWise.dataType="#dataType_dropBoxStudentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.canvasDiv="#canvasStudentPerformanceStageWiseAndGenderWiseDiv";
studentPerformanceStageWiseAndGenderWise.canvas="#chart-studentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.canvasSpan="#spanStudentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.thead="#studentPerformanceStageWiseAndGenderWiseTable thead"
studentPerformanceStageWiseAndGenderWise.tbody="#studentPerformanceStageWiseAndGenderWiseTable tbody"
studentPerformanceStageWiseAndGenderWise.schoolDiv ="#divSelectSchool";
studentPerformanceStageWiseAndGenderWise.gradeDiv ="#divSelectGrade";
studentPerformanceStageWiseAndGenderWise.subjectDiv ="#divSelectSubject";
studentPerformanceStageWiseAndGenderWise.school ="#selectBox_schoolsByLoc";
studentPerformanceStageWiseAndGenderWise.grade ="#selectBox_gradesBySchool";
studentPerformanceStageWiseAndGenderWise.subject ="#selectBox_subject";
studentPerformanceStageWiseAndGenderWise.startDate="#startDatePicker";
studentPerformanceStageWiseAndGenderWise.endDate="#endDatePicker";


/////////////////////////////////

////////////////start Total Projects Monthly Sector Wise //////////////////
var studentPerformanceStageWiseAndGenderWiseBarChart={

		fnBindChartTypeData:function(){
			$(studentPerformanceStageWiseAndGenderWise.chartTypeDiv).show();
			destroyRefresh(studentPerformanceStageWiseAndGenderWise.chartType);
			studentPerformanceStageWiseAndGenderWiseBarChart.fnSetOptionsForChartType($(studentPerformanceStageWiseAndGenderWise.chartType));
			$(studentPerformanceStageWiseAndGenderWise.chartTypeDiv).show();
		},
		fnSetOptionsForChartType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						studentPerformanceStageWiseAndGenderWiseBarChart.fnBindDataTypeData(true);
						studentPerformanceStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
						studentPerformanceStageWiseAndGenderWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnBindDataTypeData:function(){
			$(studentPerformanceStageWiseAndGenderWise.sectorDiv).show();
			destroyRefresh(studentPerformanceStageWiseAndGenderWise.dataType);
			studentPerformanceStageWiseAndGenderWiseBarChart.fnSetOptionsForDataType($(studentPerformanceStageWiseAndGenderWise.dataType));
			$(studentPerformanceStageWiseAndGenderWise.dataTypeDiv).show();
		},
		fnSetOptionsForDataType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						chartYaxisName =$(arg).find('option:selected').val();
						studentPerformanceStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
						studentPerformanceStageWiseAndGenderWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnUpdateBarChart:function(){
			var schoolIds=[];
			$(studentPerformanceStageWiseAndGenderWise.school).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						schoolIds.push($(option).val());
					}
				});
			});
			
			var selectedGradeId=[];
			$(studentPerformanceStageWiseAndGenderWise.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selectedGradeId.push($(option).val());
					}
				});
			});
			var selectedSubjectId=[];
			$(studentPerformanceStageWiseAndGenderWise.subject).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selectedSubjectId.push($(option).val());
					}
				});
			});

			var dataTypeId=$(studentPerformanceStageWiseAndGenderWise.dataType).find("option:selected").data('id');
			var chartTypeId=$(studentPerformanceStageWiseAndGenderWise.chartType).find("option:selected").data('id');
			var startDate=$(studentPerformanceStageWiseAndGenderWise.startDate).datepicker('getFormattedDate');
			var endDate=$(studentPerformanceStageWiseAndGenderWise.endDate).datepicker('getFormattedDate');
			var fromDate=moment(startDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
			var toDate=moment(endDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
			
			
			var jsonData={
					"schoolIds":schoolIds,
					"fromDate":fromDate,
					"toDate":toDate,
					"gradeIds":selectedGradeId,
					"subjectIds":selectedSubjectId,
					"dataTypeId":dataTypeId,
					"chartTypeId":chartTypeId
			}
			if(schoolIds.length>0 && selectedGradeId.length>0 && selectedSubjectId.length>0){
				var barChartData=null;
				if(dataTypeId==1){
					 barChartData=customAjaxCalling("reportDashBoard/getBarChartDataForStudentPerformanceStageWiseAndGenderWise", jsonData, "POST");
				}else{
					 barChartData=customAjaxCalling("reportDashBoard/getBarChartDataForStudentPercentagePerformanceStageWiseAndGenderWise", jsonData, "POST");
				}
				
				var result=barChartData.datasets!=null?barChartData.datasets.length>0?true:false:false;
				if(result==true){
					$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(" ");
					$(studentPerformanceStageWiseAndGenderWise.canvasSpan).hide();
					var studentPerformanceStageWiseAndGenderWiseBarChartData = {
							labels: barChartData.labels,
							datasets: barChartData.datasets

					};
					studentPerformanceStageWiseAndGenderWiseBarChart.fnGenrateTable(barChartData.labels,barChartData.datasets);
					
					var studentPerformanceStageWiseAndGenderWiseBarChartDataConfig = {
							type: 'bar',
							data: studentPerformanceStageWiseAndGenderWiseBarChartData,
							
							
							options: {
								
								 scales: {
					                    xAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
					                        }],
					                    yAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
									            ticks: {
					                                beginAtZero: true,
					                                steps: 10,
					                                stepValue: 5,
					                            },
					                            scaleLabel: {
					                                display: true,
					                                labelString: chartYaxisName
					                            }
					                        }]
					                },
								
								
								elements: {
									rectangle: {
										borderWidth: 0.5,
										borderColor: randomColor(),
										borderSkipped: 'bottom'
									}
								},
								responsive: true,
								legend: {
									position: 'bottom',
								},

							}


					}

					$(studentPerformanceStageWiseAndGenderWise.canvas).remove();
					$(studentPerformanceStageWiseAndGenderWise.canvasDiv).append('<canvas id="chart-studentPerformanceStageWiseAndGenderWise" />');
					var canvas = document.getElementById("chart-studentPerformanceStageWiseAndGenderWise");
					var context = canvas.getContext('2d');
					context.clear();
					window.myBar = new Chart(context, studentPerformanceStageWiseAndGenderWiseBarChartDataConfig);
					window.myBar.update();
				}
				else{
					$(studentPerformanceStageWiseAndGenderWise.canvasSpan).show();
					studentPerformanceStageWiseAndGenderWiseBarChart.fnEmptyCanvas(emptyChartMessage);
				}
			}

		},
		fnEmptyCanvas:function(message){
			$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(" ");
			$(studentPerformanceStageWiseAndGenderWise.canvas).remove();
			$(studentPerformanceStageWiseAndGenderWise.canvasSpan).show();
			$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(message)
		},
		fnGenrateTable:function(labels,dataSet){
			$(studentPerformanceStageWiseAndGenderWise.thead).children("tr").remove();
			$(studentPerformanceStageWiseAndGenderWise.tbody).children("tr").remove();
			var thead='<tr style="display: table-row;">';
			var tData="";
			$.each(labels,function(index,thData){
				var count=index+1;
				tData=tData+'<th id="h'+count+'" class="nosort col-xs-2">'+thData+'</th>';
			});
            var finalThead=thead+'<th class="nosort col-xs-3" ></th>'+tData+"</tr>";
            var tbody="";
            $.each(dataSet,function(indexData,data){
            	tbody=tbody+'<tr><td class="text-center-bold">'+data.label+'</td>';
            	$.each(data.data,function(i,edata){
            		tbody=tbody+'<td class="textCenter">'+edata+'</td>';
            	});
            	tbody=tbody+("</tr>")
            });
           var finalTbody="<tr>"+tbody+"</tr>";
            $(studentPerformanceStageWiseAndGenderWise.thead).append(finalThead);
            $(studentPerformanceStageWiseAndGenderWise.tbody).append(tbody);
			
		},
		fnShowError:function(currentElement){
			//$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(" ");
			var elements =[studentPerformanceStageWiseAndGenderWise.grade,studentPerformanceStageWiseAndGenderWise.subject,studentPerformanceStageWiseAndGenderWise.school];
			var message =[selectGradeMessage, selectSubjectMessage,selectSchoolMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(isErrorShwon==false && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(message[i]);
						$(studentPerformanceStageWiseAndGenderWise.canvasSpan).show();
						isErrorShwon =true;
						return false;
					}
				}
			});

		}
}

////////////////////// 
/*Chart.pluginService.register({
    afterDraw: function(chartInstance) {
        var ctx = chartInstance.chart.ctx;

        // render the value of the chart above the bar
       ctx.font = Chart.helpers.fontString(Chart.defaults.global.defaultFontSize, 'normal', Chart.defaults.global.defaultFontFamily);
       ctx.textAlign = 'center';
        ctx.textBaseline = 'bottom';

        chartInstance.data.datasets.forEach(function (dataset) {
            for (var i = 0; i < dataset.data.length; i++) {
                var model = dataset._meta[Object.keys(dataset._meta)[0]].data[i]._model;
                ctx.fillText(dataset.data[i], model.x, model.y - 5);
            }
        });
  }
});*/

var stageWiseAndGenderWiseInIt=function(){
	studentPerformanceStageWiseAndGenderWiseBarChart.fnBindChartTypeData(true);
	studentPerformanceStageWiseAndGenderWiseBarChart.fnBindDataTypeData(true);
	studentPerformanceStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
}

/*$(document) .ready(
		function() {
			stageWiseAndGenderWiseInIt();
		});*/