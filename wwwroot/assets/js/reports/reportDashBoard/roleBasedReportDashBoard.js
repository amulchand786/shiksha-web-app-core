//show container on submit
var fnShowContainer =function(){
	$('#reportfilter').removeClass('in');
	
	if(metricType=='pw'){
		console.log("perfromance metric");
		stageWiseAndGenderWiseInIt();
		taxonomyStageWiseAndGenderWiseInIt();
	}else if(metricType=='school'){
		console.log("School statitics...");
		schoolTypeWiseAndSegmentWiseInIt();
	}else if(metricType=='student'){
		console.log("Student statitics...");
		studentsTypeWiseAndSegmentWiseInIt();
	}
	
	$('#btnViewReport').prop('disabled',false);
	$('#btnViewReport').removeClass('disabled');
	
	$('.chartContainer').css('display','block');
	
}

//hide container
var fnHideContainer =function(){
	
	fnResetChartTypeFilters();
	
	$('.chartContainer').css('display','none');
}

//reset chart type and data type of chart containers

var fnResetChartTypeFilters =function(){
	
	
	$('#chartType_dropBoxStudentPerformanceStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxStudentPerformanceStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);
	
	$('#chartType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);
	
	
	$('#chartType_dropBoxStudentsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxStudentsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);
	
	$('#chartType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);
	
	$('.metrics').each(function(i,ele){
		$(ele).multiselect('refresh');
	});
	
	
}




var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}

var fnEmptyCanvas=function(aCanvas,aCanvasSpan){
	$(aCanvas).remove();
	$(aCanvasSpan).show();

}

var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');
	$(ele).multiselect('refresh');
}

var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');
	$(ele).multiselect('refresh');
}
CanvasRenderingContext2D.prototype.clear = 
	CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
	if (preserveTransform) {
		this.save();
		this.setTransform(1, 0, 0, 1, 0, 0);
	}

	this.clearRect(0, 0, this.canvas.width, this.canvas.height);

	if (preserveTransform) {
		this.restore();
	}           
};