////////////////global variable Total Schools Sector Wise/////////////////

var canvasHeight='300px';
var canvasWidth='300px';
var ctx;
var $ele;
var chartpieconfig;
var selfChart2;
var selfChart3;
var selfChart4;
var selfChart5;
var selfChart6;

var mData;
var popoverElements;
var stateIds=[];
var grades={};
var reportDashBoard= {};
reportDashBoard.state="#state_dropBoxReportDashboard";
reportDashBoard.year="#year_dropBoxReportDashboard";
reportDashBoard.yearDiv="#year_dropBoxReportDashboardDiv";
reportDashBoard.stateDiv="#state_dropBoxReportDashboardDiv";
reportDashBoard.graphDiv='canvas-holder';

var totalSchools= {};

totalSchools.districtDiv="#district_dropBoxTotalSchoolsDiv";
totalSchools.district="#district_dropBoxTotalSchools";
totalSchools.blockDiv="#block_dropBoxTotalSchoolsDiv";
totalSchools.block="#block_dropBoxTotalSchools";
totalSchools.canvasDiv="#canvasTotalSchoolsDiv";
totalSchools.canvas="#chart-area1";
totalSchools.canvasElement="chart-area1";
totalSchools.canvasSpan="#spanTotalSchools";
totalSchools.submit="#submitBtnTotalSchools";

var emptyChartMessage="The chart contains no data. ";
var selectStateMessage="Please select atleast one State. ";
var stateHaveNoDistrict="Selected State have no any District. ";
var districtHaveNoBlock="Selected District have no any Block. ";
var selectDistrictMessage="Please select atleast one District. ";
var selectBlockMessage="Please select atleast one Block. ";
var selectGradeMessage="Please select atleast one Grade. ";
var selectSubjectMessage="Please select atleast one Subject. ";
var selectProjectMessage="Please select atleast one Project."
	var gradeHaveNoSubject="Selected Grade have no any Subject. ";
var filterHaveNoProject="Selected Filter have no any Project. ";
var filterHaveNoGrade="Selected Filter have no any Grade. ";
var selectYearMessage="Please select Academic year";
var filterHaveNoGender="Selected Filter have no any Grade. ";
var selectSectorMessage="Please select Sector";
var selectGenderMessage="Please select atleast one Gender";

var	selectAllStateText="Select all States";
var	nonSelectedStateText= 'Select State';
var nSelectedStateText=" States selected";
var allSelectedStateText="All States selected";

var	selectAllDistrictText="Select all District";
var	nonSelectedDistrictText= 'Select District';
var nSelectedDistrictText=" Districts selected";
var allSelectedDistrictText="All Districts selected";

var	selectAllBlockText="Select all Blocks";
var	nonSelectedBlockText= 'Select Block';
var nSelectedBlockText=" Blocks selected";
var allSelectedBlockText="All Blocks selected";

var	selectAllGradeText="Select all Grades";
var	nonSelectedGradeText= 'Select Grade';
var nSelectedGradeText=" Grades selected";
var allSelectedGradeText="All Grades selected";

var	selectAllSubjectText="Select all Subjects";
var	nonSelectedSubjectText= 'Select Subject';
var nSelectedSubjectText=" Subjects selected";
var allSelectedSubjectText="All Subjects selected";

var	selectAllProjectText="Select all Projects";
var	nonSelectedProjectText= 'Select Project';
var nSelectedProjectText=" Projects selected";
var allSelectedProjectText="All Projects selected";

var	selectAllSectorText="Select all Sectors";
var	nonSelectedSectorText= 'Select Sector';
var nSelectedSectorText=" Sectors selected";
var allSelectedSectorText="All Sectors selected";

var	selectAllGenderText="Select all Genders";
var	nonSelectedGenderText= 'Select Gender';
var nSelectedGenderText=" Genders selected";
var allSelectedGenderText="All Gender selected";
////////////////global Total Projects Monthly Sector Wise //////////////////

	var studentAttendenceTrend={};
studentAttendenceTrend.districtDiv="#district_dropBoxStudentAttendenceTrendDiv";
studentAttendenceTrend.district="#district_dropBoxStudentAttendenceTrend";
studentAttendenceTrend.blockDiv="#block_dropBoxStudentAttendenceTrendDiv";
studentAttendenceTrend.block="#block_dropBoxStudentAttendenceTrend";
studentAttendenceTrend.sectorDiv="#sector_dropBoxStudentAttendenceTrendDiv";
studentAttendenceTrend.sector="#sector_dropBoxStudentAttendenceTrend";

studentAttendenceTrend.canvasDiv="#canvasStudentAttendenceTrendDiv";
studentAttendenceTrend.canvas="#chart-area2";
studentAttendenceTrend.canvasElement="chart-area2";
studentAttendenceTrend.canvasSpan="#spanStudentAttendenceTrend";
studentAttendenceTrend.submit="#submitBtnStudentAttendenceTrend";
////////////////global Total Projects Monthly Sector Wise //////////////////

var totalStudentsSectorWise={};
totalStudentsSectorWise.districtDiv="#district_dropBoxTotalStudentsSectorWiseDiv";
totalStudentsSectorWise.district="#district_dropBoxTotalStudentsSectorWise";
totalStudentsSectorWise.blockDiv="#block_dropBoxTotalStudentsSectorWiseDiv";
totalStudentsSectorWise.block="#block_dropBoxTotalStudentsSectorWise";
totalStudentsSectorWise.sectorDiv="#sector_dropBoxTotalStudentsSectorWiseDiv";
totalStudentsSectorWise.sector="#sector_dropBoxTotalStudentsSectorWise";
totalStudentsSectorWise.canvasDiv="#canvasTotalStudentsSectorWiseDiv";
totalStudentsSectorWise.canvas="#chart-area3";
totalStudentsSectorWise.canvasElement="chart-area3";
totalStudentsSectorWise.canvasSpan="#spanTotalStudentsSectorWise";
totalStudentsSectorWise.submit="#submitBtnTotalStudentsSectorWise";

////////////////global variable Total Schools Sector Wise/////////////////

var studentsPerformanceAcrossStates= {};
studentsPerformanceAcrossStates.districtDiv="#district_dropBoxStudentsPerformanceAcrossStatesDiv";
studentsPerformanceAcrossStates.district="#district_dropBoxStudentsPerformanceAcrossStates";
studentsPerformanceAcrossStates.blockDiv="#block_dropBoxStudentsPerformanceAcrossStatesDiv";
studentsPerformanceAcrossStates.block="#block_dropBoxStudentsPerformanceAcrossStates";
studentsPerformanceAcrossStates.gradeDiv="#grade_dropBoxStudentsPerformanceAcrossStatesDiv";
studentsPerformanceAcrossStates.grade="#grade_dropBoxStudentsPerformanceAcrossStates";
studentsPerformanceAcrossStates.subjectDiv="#subject_dropBoxStudentsPerformanceAcrossStatesDiv";
studentsPerformanceAcrossStates.subject="#subject_dropBoxStudentsPerformanceAcrossStates";
studentsPerformanceAcrossStates.canvasDiv="#canvasTotalStudentsPerformanceAcrossStates";
studentsPerformanceAcrossStates.canvas="#chart-area4";
studentsPerformanceAcrossStates.canvasElement="chart-area4";
studentsPerformanceAcrossStates.canvasSpan="#spanTotalStudentsPerformanceAcrossStates";
studentsPerformanceAcrossStates.submit="#submitBtnStudentsPerformanceAcrossStates";

////////////////global variable Students Performance Across States /////////////////

var studentsPerformanceAcrossPhase= {};
studentsPerformanceAcrossPhase.yearDiv="#year_dropBoxStudentsPerformanceAcrossPhasesDiv";
studentsPerformanceAcrossPhase.year="#year_dropBoxStudentsPerformanceAcrossPhases";
studentsPerformanceAcrossPhase.gradeDiv="#grade_dropBoxStudentsPerformanceAcrossPhasesDiv";
studentsPerformanceAcrossPhase.grade="#grade_dropBoxStudentsPerformanceAcrossPhases";
studentsPerformanceAcrossPhase.subjectDiv="#subject_dropBoxStudentsPerformanceAcrossPhasesDiv";
studentsPerformanceAcrossPhase.subject="#subject_dropBoxStudentsPerformanceAcrossPhases";
studentsPerformanceAcrossPhase.sectorDiv="#sector_dropBoxStudentsPerformanceAcrossPhasesDiv";
studentsPerformanceAcrossPhase.sector="#sector_dropBoxStudentsPerformanceAcrossPhases";
studentsPerformanceAcrossPhase.canvasDiv="#canvasStudentsPerformanceAcrossPhases";
studentsPerformanceAcrossPhase.canvas="#chart-area5";
studentsPerformanceAcrossPhase.canvasElement="chart-area5";
studentsPerformanceAcrossPhase.canvasSpan="#spanTotalStudentsPerformanceAcrossPhases";
studentsPerformanceAcrossPhase.submit="#submitBtnStudentsPerformanceAcrossPhases";

////////////////global variable Students Performance Grade Wise /////////////////


var studentsPerformanceAccrossProject= {};
studentsPerformanceAccrossProject.districtDiv="#district_dropBoxStudentsPerformanceAccrossProjectDiv";
studentsPerformanceAccrossProject.district="#district_dropBoxStudentsPerformanceAccrossProject";
studentsPerformanceAccrossProject.blockDiv="#block_dropBoxStudentsPerformanceAccrossProjectDiv";
studentsPerformanceAccrossProject.block="#block_dropBoxStudentsPerformanceAccrossProject";
studentsPerformanceAccrossProject.gradeDiv="#grade_dropBoxStudentsPerformanceAccrossProjectDiv";
studentsPerformanceAccrossProject.grade="#grade_dropBoxStudentsPerformanceAccrossProject";
studentsPerformanceAccrossProject.projectDiv="#project_dropBoxStudentsPerformanceAccrossProjectDiv";
studentsPerformanceAccrossProject.project="#project_dropBoxStudentsPerformanceAccrossProject";
studentsPerformanceAccrossProject.genderDiv="#gender_dropBoxStudentsPerformanceAccrossProjectDiv";
studentsPerformanceAccrossProject.gender="#gender_dropBoxStudentsPerformanceAccrossProject";
studentsPerformanceAccrossProject.canvasDiv="#canvasStudentsPerformanceAccrossProject";
studentsPerformanceAccrossProject.canvas="#chart-area6";
studentsPerformanceAccrossProject.canvasElement="chart-area6";
studentsPerformanceAccrossProject.canvasSpan="#spanStudentsPerformanceAccrossProject";
studentsPerformanceAccrossProject.submit="#submitBtnStudentsPerformanceAccrossProject";


///////////////////////// start Students Performance Across States  ////////////////////////////////////////////
var fnEmptyAll =function(){
$.each(arguments,function(i,obj){
$(obj).empty();
});
}

var fnEmptyCanvas=function(aCanvas,aCanvasSpan){
$(aCanvas).remove();
$(aCanvasSpan).show();

}

var destroyRefresh =function(ele){

$(ele).multiselect('refresh');
$(ele).multiselect('destroy');
}

var randomColorFactor = function() {
return Math.round(Math.random() * 255);
};
var randomColor = function(opacity) {
return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '0.5') + ')';
};

CanvasRenderingContext2D.prototype.clear = 
CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
if (preserveTransform) {
this.save();
this.setTransform(1, 0, 0, 1, 0, 0);
}

this.clearRect(0, 0, this.canvas.width, this.canvas.height);

if (preserveTransform) {
this.restore();
}           
};



var setOptionForState=function(){
$(arguments).each(function(index,arg){
$(arg).multiselect('destroy');
$(arg).multiselect({
maxHeight: 180,
includeSelectAllOption: true,
enableFiltering: true,
selectAllText:selectAllStateText,
nonSelectedText:nonSelectedStateText,
nSelectedText:nSelectedStateText,
allSelectedText:allSelectedStateText,
numberDisplayed: 1,
enableCaseInsensitiveFiltering: true,
includeFilterClearBtn: true,
filterPlaceholder: 'Search here...',
onDropdownShow:function() {
$('.popover').popover('hide');
},
onDropdownHide: function() {

var eStateIds=[];
$(reportDashBoard.state).each(function(index, ele) {
$(ele).find("option:selected").each(function(ind, option) {
if ($(option).val() != "multiselect-all"){
	eStateIds.push($(option).val());
}
});
});
if(eStateIds.length>0){
stateIds=eStateIds;
$('.spinner').show();
$.wait(5).then(chartInIt);
$("div[id*='"+reportDashBoard.graphDiv+"']").show();
}else{
$("div[id*='"+reportDashBoard.graphDiv+"']").hide();
}
} 
});

});
fnCollapseMultiselect();
$(reportDashBoard.stateDiv).show();
}
var setOptionForYear=function(){
$(arguments).each(function(index,arg){
$(arg).multiselect('destroy');
$(arg).multiselect({
maxHeight: 180,
includeSelectAllOption: true,
enableFiltering: true,
selectAllText:"Select all Year",
nonSelectedText: 'Select Year',
enableCaseInsensitiveFiltering: true,
includeFilterClearBtn: true,
filterPlaceholder: 'Search here...',
onDropdownShow:function() {
$('.popover').popover('hide');
},
onDropdownHide: function() {
var academicYearId=$(reportDashBoard.year).find("option:selected").val();
if(academicYearId!=0){
$('.spinner').show();
$.wait(5).then(chartInIt);
$("div[id*='"+reportDashBoard.graphDiv+"']").show();
}else{
$("div[id*='"+reportDashBoard.graphDiv+"']").hide();
}
} 
});

});
fnCollapseMultiselect();
$(reportDashBoard.yearDiv).show();
}




/*
* @author: debendra
* set the canvas style properties
* */

function setCanvasStyle(cId){
document.getElementById(cId).style.maxHeight = canvasHeight;
document.getElementById(cId).style.maxWidth = canvasWidth;
document.getElementById(cId).style.display = "inline-block";

}

/**
* @author: debendra
* update popover multiselect
* */

var hideOtherPopOver=function(element){
$.each(popoverElements,function(i,obj){
if(element!=obj){
$(obj).closest("#canvas-holder").find(".chart-filter-container").slideUp(0);

}

});
}

function updatePopOver(popOverId){
$(popOverId).on("click",function(){
hideOtherPopOver(popOverId);
$(popOverId).closest("#canvas-holder").find(".chart-filter-container").slideToggle(10);		
});
}



/**
* If no data is there, update chart
* @param chartId : cnavas Id
* @param chartObj : created Chart object
* */
function setMessageIfChartContainsEmptyData(chartId,chartObj){
ctx = document.getElementById(chartId).getContext('2d');
ctx.clear();
if(typeof chartObj!='undefined' )
chartObj.destroy();
ctx.font = "15px " + Chart.defaults.global.tooltipTitleFontFamily;
ctx.textAlign = "center";
ctx.textBaseline = "middle";
ctx.fillStyle = "#333333";
ctx.fillText("Chart contains no data.", ctx.canvas.clientWidth / 2, ctx.canvas.clientHeight / 2);


$('#'+chartId).closest("#canvas-holder").find(".panelheadingcolor").addClass("hide-legend");
$('#'+chartId).closest("#canvas-holder").find(".chart-legend").html('');
}


var fnGetStateId=function(){
	var selectedStateIds=[];
	$(reportDashBoard.state).each(function(index, ele) {
		$(ele).find("option:selected").each(function(ind, option) {
			if ($(option).val() != "multiselect-all"){
				selectedStateIds.push($(option).val());
			}
		});
	});
	stateIds=selectedStateIds;
	return selectedStateIds;
}
////////////////start Total Schools Sector Wise/////////////////
var stateHaveDistrict=false;
var districtHaveBlock=false;
var totalSchoolsChart={
		// bind data in district drop box
		fnBindDistrictData:function(selStateIds,selected,aSetOptionFlag){
			fnEmptyAll(totalSchools.district);
			destroyRefresh(totalSchools.district);
			$ele =$(totalSchools.district);
			$ele.html('');
			var aStateIds=fnGetStateId();
			var data =customAjaxCalling("district/dashboard/state",aStateIds,'POST');
			var isDistrict=data!=null?data.length>0?true:false:false;
			stateHaveDistrict=isDistrict;
			if(isDistrict){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}
					}
				});

				$(totalSchools.districtDiv).show();
				destroyRefresh(totalSchools.district);
				if(aSetOptionFlag){
					totalSchoolsChart.fnSetOptionsForDistrict($(totalSchools.district));
				}
			}else{
				$(totalSchools.districtDiv).hide();
				destroyRefresh(totalSchools.district);
				totalSchoolsChart.fnEmptyCanvas(stateHaveNoDistrict);
				
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForDistrict:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180, 
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllDistrictText,
					nonSelectedText: nonSelectedDistrictText,
					allSelectedText:allSelectedDistrictText,
					nSelectedText:nSelectedDistrictText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedDistrictId=[];
						$(totalSchools.canvasSpan).hide();
						$(totalSchools.district).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedDistrictId.push($(option).val());
								}
							});
						});
						if(selectedDistrictId.length>0){
							totalSchoolsChart.fnEnableDisableSubmitBtn(false);
							totalSchoolsChart.fnBindBlockData(true,true);
							
						}else{
							totalSchoolsChart.fnEmptyCanvas(selectDistrictMessage);
							$(totalSchools.blockDiv).hide();
							totalSchoolsChart.fnShowError(arg);

						}
					 } 
				});

			});
			fnCollapseMultiselect();
		},
		// bind data in district drop box
		fnBindBlockData:function(selected,bSetOptionFlag){
			fnEmptyAll(totalSchools.block);
			destroyRefresh(totalSchools.block);
			$ele =$(totalSchools.block);
			$ele.html('');
			var selDistrictIds=[];
			$(totalSchools.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selDistrictIds.push($(option).val());
					}
				});
			});
			var data =customAjaxCalling("block/dashboard/district",selDistrictIds,'POST');
			var isBlock=data!=null?data.length>0?true:false:false;
			districtHaveBlock=isBlock;
			if(isBlock){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}
					}
				});

				$(totalSchools.blockDiv).show();
				destroyRefresh(totalSchools.block);
				if(bSetOptionFlag){
					totalSchoolsChart.fnSetOptionsForBlock($(totalSchools.block));
				}
				$(totalSchools.blockDiv).show();
				
			}else{
				$(totalSchools.blockDiv).hide();
				destroyRefresh(totalSchools.block);
				totalSchoolsChart.fnEnableDisableSubmitBtn(true);
				totalSchoolsChart.fnEmptyCanvas(districtHaveNoBlock);
				
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForBlock:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllBlockText,
					nonSelectedText: nonSelectedBlockText,
					nSelectedText:nSelectedBlockText,
					allSelectedText:allSelectedBlockText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedBlockId=[];
						$(totalSchools.canvasSpan).hide();
						$(totalSchools.block).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedBlockId.push($(option).val());
								}
							});
						});
						if(selectedBlockId.length>0){
							totalSchoolsChart.fnEnableDisableSubmitBtn(false);

						}else{
							totalSchoolsChart.fnEmptyCanvas(selectBlockMessage);
							totalSchoolsChart.fnShowError(arg);
						}
					} 
				});

			});
			fnCollapseMultiselect();
		},
		// draw chart
		fnUpdatePieChart:function(){

			var zSelectedDistrictId=[];
			var zSelectedStateId=[];
			var zSelectedBlockId=[];
			$(totalSchools.canvasSpan).hide();
			$(totalSchools.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						zSelectedDistrictId.push($(option).val());
					}
				});
			});

			$(totalSchools.block).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						zSelectedBlockId.push($(option).val());
					}
				});
			});

			$(reportDashBoard.state).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						zSelectedStateId.push($(option).val());
					}
				});
			});

			var jsonPieData={
					"districtIds":zSelectedDistrictId,
					"stateIds":zSelectedStateId,
					"blockIds":zSelectedBlockId
			}
			if(zSelectedStateId.length>0 && zSelectedDistrictId.length>0 && zSelectedBlockId.length>0){
				var pieChartData=customAjaxCalling("dashboard/schools",jsonPieData,'POST');
				if(pieChartData!=""){
					chartpieconfig = {
							type: 'pie',
							data: {
								datasets: [{
									data:pieChartData.series,
									backgroundColor:pieChartData.backgroundColor ,
								}],
								labels: pieChartData.labels
							},
							options: {
								responsive: false,
								legend: {
									display: true,
									position: 'bottom',
									 labels: {
							                fontColor: 'black'
							          }
								},
								animation:{
									animateScale:true
								},
								tooltips: {
									enabled: true,

								}
							}
					};
					$(totalSchools.canvas).remove();
					$(totalSchools.canvasDiv).append('<canvas id="chart-area1" width="280" height="280"/>');
					var canvas = document.getElementById("chart-area1");
					var context = canvas.getContext('2d');
					context.clear();
					window.myPie = new Chart(context, chartpieconfig);
					window.myPie.update();
					
					setCanvasStyle("chart-area1");
					
					
					
				}else{
					totalSchoolsChart.fnEmptyCanvas(emptyChartMessage);
					setMessageIfChartContainsEmptyData("chart-area1",window.myPie);

				}
			}else{
				setMessageIfChartContainsEmptyData("chart-area1",window.myPie);
			}
			$('.spinner').hide();

		},
		fnShowError:function(currentElement){

			var elements =[totalSchools.district,totalSchools.block];
			var message =[selectDistrictMessage,selectBlockMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(!isErrorShwon && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(totalSchools.canvasSpan).text(message[i]);
						$(totalSchools.canvasSpan).show();
						totalSchoolsChart.fnEnableDisableSubmitBtn(true);
						isErrorShwon =true;
						return false;
					}
				}
			});

		} ,
		// empty canvas
		fnEmptyCanvas:function(message){
			$(totalSchools.canvasSpan).text(" ");
			
			totalSchoolsChart.fnEnableDisableSubmitBtn(true);
			$(totalSchools.canvasSpan).text(message);
			$(totalSchools.canvasSpan).show();
			return;

		},
		fnPopoverInit:function(){
			updatePopOver('#popoverTotalSchools');
		},
		fnSubmitHandler:function(){
			totalSchoolsChart.fnUpdatePieChart();
			totalSchoolsChart.fnHidePopOver();
		},
		fnHidePopOver:function(){
			$('#popoverTotalSchools').closest("#canvas-holder").find(".chart-filter-container").slideUp(0);
		},
		fnEnableDisableSubmitBtn:function(isDisabled){
			$(totalSchools.submit).prop('disabled',isDisabled)
		},
		fnTotalSchoolInIt:function(){
			totalSchoolsChart.fnBindDistrictData(stateIds,true);
			totalSchoolsChart.fnBindBlockData(true);
			totalSchoolsChart.fnUpdatePieChart();
			
			totalSchoolsChart.fnSetOptionsForDistrict($(totalSchools.district));
			totalSchoolsChart.fnSetOptionsForBlock($(totalSchools.block));
		},
		


}

////////////////start Student attendence //////////////////
var studentAttendenceTrendLineChart={
		init:function(){
			selfChart2=studentAttendenceTrendLineChart;
		},
		// bind data in district drop box
		fnBindDistrictData:function(selStateIds,selected,cSetOptionFlag){
			fnEmptyAll(studentAttendenceTrend.district);
			destroyRefresh(studentAttendenceTrend.district);
			$ele =$(studentAttendenceTrend.district);
			$ele.html('');
			var vStateIds=fnGetStateId();
			var data =customAjaxCalling("district/dashboard/state",vStateIds,'POST');
			var isDistrict=data!=null?data.length>0?true:false:false;
			stateHaveDistrict=isDistrict;
			if(isDistrict){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}
					}
				});

				$(studentAttendenceTrend.districtDiv).show();
				destroyRefresh(studentAttendenceTrend.district);
				if(cSetOptionFlag){
					selfChart2.fnSetOptionsForDistrict($(studentAttendenceTrend.district));
				}
				$(studentAttendenceTrend.districtDiv).show();
			}else{
				$(studentAttendenceTrend.districtDiv).hide();
				destroyRefresh(studentAttendenceTrend.district);
				selfChart2.fnEmptyCanvas(stateHaveNoDistrict);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForDistrict:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllDistrictText,
					nonSelectedText: nonSelectedDistrictText,
					nSelectedText:nSelectedDistrictText,
					allSelectedText:allSelectedDistrictText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedDistrictId=[];
						$(studentAttendenceTrend.canvasSpan).hide();
						$(studentAttendenceTrend.district).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedDistrictId.push($(option).val());
								}
							});
						});
						if(selectedDistrictId.length>0){
							selfChart2.fnEnableDisableSubmitBtn(false);
							selfChart2.fnBindBlockData(true,true);
						}else{
							selfChart2.fnEmptyCanvas(selectDistrictMessage);
							$(studentAttendenceTrend.blockDiv).hide();
							selfChart2.fnShowError(arg);
						}
						

					} 
				});

			});
			fnCollapseMultiselect();
		},
		
		// bind data in district drop box
		fnBindBlockData:function(selected,dSetOptionFlag){
			fnEmptyAll(studentAttendenceTrend.block);
			destroyRefresh(studentAttendenceTrend.block);
			$ele =$(studentAttendenceTrend.block);
			$ele.html('');
			var selDistrictIds=[];
			$(studentAttendenceTrend.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selDistrictIds.push($(option).val());
					}
				});
			});
			var data =customAjaxCalling("block/dashboard/district",selDistrictIds,'POST');
			var isBlock=data!=null?data.length>0?true:false:false;
			districtHaveBlock=isBlock;
			if(isBlock){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}
					}
				});

				$(studentAttendenceTrend.blockDiv).show();
				$(studentAttendenceTrend.sectorDiv).show();
				destroyRefresh(studentAttendenceTrend.block);
				if(dSetOptionFlag){
					selfChart2.fnSetOptionsForBlock($(studentAttendenceTrend.block));
				}
			}else{
				$(studentAttendenceTrend.blockDiv).hide();
				$(studentAttendenceTrend.sectorDiv).hide();
				destroyRefresh(studentAttendenceTrend.block);
				selfChart2.fnEmptyCanvas(districtHaveNoBlock);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForBlock:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					selectAllText:selectAllBlockText,
					nonSelectedText: nonSelectedBlockText,
					nSelectedText:nSelectedBlockText,
					allSelectedText:allSelectedBlockText,
					numberDisplayed: 1,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedBlockId=[];
						$(studentAttendenceTrend.canvasSpan).hide();
						$(studentAttendenceTrend.block).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedBlockId.push($(option).val());
								}
							});
						});
						if(selectedBlockId.length>0){
							selfChart2.fnEnableDisableSubmitBtn(false);
							selfChart2.fnBindSectorData(true,true);
						}else{
							selfChart2.fnEmptyCanvas(selectBlockMessage);
							$(studentAttendenceTrend.sectorDiv).hide();
						}
						selfChart2.fnShowError(arg);

					} 
				});

			});
			fnCollapseMultiselect();
		},
//		bind data in district drop box
		fnBindSectorData:function(selected,eSetOptionFlag){
			destroyRefresh(studentAttendenceTrend.sector);
			$ele =$(studentAttendenceTrend.sector);
			if(eSetOptionFlag){
				selfChart2.fnSetOptionsForSectors($(studentAttendenceTrend.sector));
			}
			$(studentAttendenceTrend.sectorDiv).show();

		},
//		sector drop box dropdown event handling
		fnSetOptionsForSectors:function(){

			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllSectorText,
					nonSelectedText: nonSelectedSectorText,
					nSelectedText:nSelectedSectorText,
					allSelectedText:allSelectedSectorText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						
						$(studentAttendenceTrend.canvasSpan).hide();
						var selectedSectorId=$(studentAttendenceTrend.sector).find("option:selected").val();

						if(selectedSectorId>0){
						}else{
							$(studentAttendenceTrend.canvasSpan).show();
							selfChart2.fnEmptyCanvas();
						}
						selfChart2.fnShowError(arg);
					} 
				});

			});
			fnCollapseMultiselect();


		},
		fnUpdateLineChart:function(){
			var selStateIds=[];

			$(reportDashBoard.state).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selStateIds.push($(option).val());
					}
				});
			});
			var selYearIds=[];
			$(reportDashBoard.year).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selYearIds.push($(option).val());
					}
				});
			});
			var selBlockId=[];
			$(studentAttendenceTrend.block).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selBlockId.push($(option).val());
					}
				});
			});
			var selDistrictId=[];
			$(studentAttendenceTrend.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selDistrictId.push($(option).val());
					}
				});
			});
			var selectedSectorId=$(studentAttendenceTrend.sector).find("option:selected").val();
			var jsonData={
					"stateIds":selStateIds,
					"academicYearIds":selYearIds,
					"sectorId":selectedSectorId,
					"blockIds":selBlockId
			}
			if(selStateIds.length>0 && selDistrictId.length>0 && selBlockId.length>0 && selYearIds.length>0){
				$(studentAttendenceTrend.canvasSpan).text(" ");
				var lineChartData=customAjaxCalling("dashboard/students/attendance", jsonData, "POST");
				if(lineChartData.datasets!=null){
					$(studentAttendenceTrend.canvasSpan).text(" ");
					var chart2lineconfig = {
							type: 'line',
							data: {
								labels: lineChartData.labels,
								datasets:lineChartData.datasets
							},
							options: {
								responsive: true,
								 legend: {                                                                              
							            display: false,
							            labels: {
							                fontColor: 'black'
							          }
							     },
								scales: {
									yAxes: [{
										ticks: {
											callback: function(value, index, values) {
												return value +"%";
											},
											beginAtZero:true,
											suggestedMin: 0,
											suggestedMax: 10,
											fontColor: 'black'
										}
									}],
									xAxes: [{
						                    ticks: {
						                        fontColor: "black",				                   
						                    }
						            }],
								},
								tooltips: {
									mode: 'label',
									callbacks: {
										title: function(tooltipItems, data) {
											return data.labels[tooltipItems[0].index] + ' ';
										},
										label: function(tooltipItem, data) { 
											return data.datasets[tooltipItem.datasetIndex].label + ": " + tooltipItem.yLabel+"%";
										},
									}
								},
								hover: {
									mode: 'dataset'
								},
								legendCallback: function(chart) {
						            
						            var html = [];
						            html.push('<ul class="' + chart.id + '-legend">');
						            for (var i=0; i<chart.data.datasets.length; i++) {
						            	html.push('<li class="strikeout" onclick="studentAttendenceTrendObj.updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')"><span class="strikeout" style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
										if (chart.data.datasets[i].label) {
											html.push(chart.data.datasets[i].label);
										}
										html.push('</li>');
						            }
						            html.push('</ul>');                                                          
						            return html.join("");                                                        
						        }, 

							}
					};

					$.each(chart2lineconfig.data.datasets, function(i, dataset) {
						var background = randomColor(0.5);
						dataset.borderColor = background;
						dataset.backgroundColor = background;
						dataset.pointBorderColor = background;
						dataset.pointBackgroundColor = background;
						dataset.pointBorderWidth = 1;
					});

					$(studentAttendenceTrend.canvas).remove();
					$(studentAttendenceTrend.canvasDiv).append('<canvas id="chart-area2" width="280" height="280"/>');
					var canvas = document.getElementById("chart-area2");
					var context = canvas.getContext('2d');
					context.clear();
					window.studentAttendenceTrendChart = new Chart(context, chart2lineconfig);
					window.studentAttendenceTrendChart.update();
					
					studentAttendenceTrendObj.updateChartLegend("spanStudentAttendenceTrendLegend");
					
				}else{
					selfChart2.fnEmptyCanvas(emptyChartMessage);
					setMessageIfChartContainsEmptyData("chart-area2",window.studentAttendenceTrendChart);
				}
			}else{
				setMessageIfChartContainsEmptyData("chart-area2",window.studentAttendenceTrendChart);
			}
			$('.spinner').hide();

		},
		fnEmptyCanvas:function(message){
			$(studentAttendenceTrend.canvasSpan).text(" ");
			
			$(studentAttendenceTrend.canvasSpan).text(message);
			selfChart2.fnEnableDisableSubmitBtn(true);
			$(studentAttendenceTrend.canvasSpan).show();
		},
		fnEnableDisableSubmitBtn:function(isDisabled){
			$(studentAttendenceTrend.submit).prop('disabled',isDisabled)
		},
		fnShowError:function(currentElement){
			$(studentAttendenceTrend.canvasSpan).text(" ");
			var elements =[reportDashBoard.year,reportDashBoard.state,studentAttendenceTrend.district,studentAttendenceTrend.block,studentAttendenceTrend.sector];
			var message =[selectYearMessage,selectStateMessage,selectDistrictMessage,selectBlockMessage,selectSectorMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(!isErrorShwon && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentAttendenceTrend.canvasSpan).text(message[i]);
						$(studentAttendenceTrend.canvasSpan).show();
						selfChart2.fnEnableDisableSubmitBtn(true);
						$.each(elements,function(j,data){
							if(j>i){
								$(data+'Div').hide();
							}else{
								$(data+'Div').show();
							}
						});
						isErrorShwon =true;
						return false;
					}else{
						$.each(elements,function(j,data){
							$(data+'Div').show();
						});
					}
				}
			});

		}, 
		fnPopoverInit:function(){
			updatePopOver('#popoverStudentAttendenceTrend');
		},
		fnSubmitHandler:function(){
		    selfChart2.fnUpdateLineChart();
		    selfChart2.fnHidePopOver();
		},
		fnHidePopOver:function(){
			$('#popoverStudentAttendenceTrend').closest("#canvas-holder").find(".chart-filter-container").slideUp(0);
		},
		fnTotalProjectsMonthlySectorWiseInIt:function(){
			selfChart2.fnBindDistrictData(stateIds,true);
			selfChart2.fnBindBlockData(true);
			selfChart2.fnBindSectorData(true);
			selfChart2.fnUpdateLineChart();
			
			selfChart2.fnSetOptionsForDistrict($(studentAttendenceTrend.district));
			selfChart2.fnSetOptionsForBlock($(studentAttendenceTrend.block));
			selfChart2.fnSetOptionsForSectors($(studentAttendenceTrend.sector));
		}
}




////////////////end Total Projects Monthly Sector Wise //////////////////

////////////////Start Total Students across Projects //////////////////
var totalStudentSectorWiseDoughnutChart={
		init:function(){
			selfChart3=totalStudentSectorWiseDoughnutChart;
		},

		// bind data in district drop box
		fnBindDistrictData:function(selStateIds,selected,fSetOptionFlag){
			fnEmptyAll(totalStudentsSectorWise.district);
			destroyRefresh(totalStudentsSectorWise.district);
			$ele =$(totalStudentsSectorWise.district);
			$ele.html('');
			var vStateIds=fnGetStateId();
			var data =customAjaxCalling("district/dashboard/state",vStateIds,'POST');
			var isDistrict=data!=null?data.length>0?true:false:false;
			stateHaveDistrict=isDistrict;
			if(isDistrict){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}
					}
				});

				$(totalStudentsSectorWise.districtDiv).show();
				destroyRefresh(totalStudentsSectorWise.district);
				if(fSetOptionFlag){
				selfChart3.fnSetOptionsForDistrict($(totalStudentsSectorWise.district));
				}
			}else{
				$(totalStudentsSectorWise.districtDiv).hide();
				$(totalStudentsSectorWise.blockDiv).hide();
				$(totalStudentsSectorWise.sectorDiv).hide();
				destroyRefresh(totalStudentsSectorWise.district);
				selfChart3.fnEmptyCanvas(stateHaveNoDistrict);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForDistrict:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllDistrictText,
					nonSelectedText: nonSelectedDistrictText,
					nSelectedText:nSelectedDistrictText,
					allSelectedText:allSelectedDistrictText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedDistrictId=[];
						$(totalStudentsSectorWise.canvasSpan).hide();
						$(totalStudentsSectorWise.district).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedDistrictId.push($(option).val());
								}
							});
						});
						if(selectedDistrictId.length>0){
							selfChart3.fnEnableDisableSubmitBtn(false);
							selfChart3.fnBindBlockData(true,true);
						}else{
							$(totalStudentsSectorWise.blockDiv).hide();
							 selfChart3.fnShowError(arg);
                         }
						

					} 
				});

			});
			fnCollapseMultiselect();
		},
		// bind data in district drop box
		fnBindBlockData:function(selected,gSetOptionFlag){
			fnEmptyAll(totalStudentsSectorWise.block);
			destroyRefresh(totalStudentsSectorWise.block);
			$ele =$(totalStudentsSectorWise.block);
			$ele.html('');
			var selDistrictIds=[];
			$(totalStudentsSectorWise.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selDistrictIds.push($(option).val());
					}
				});
			});
			var data =customAjaxCalling("block/dashboard/district",selDistrictIds,'POST');
			var isBlock=data!=null?data.length>0?true:false:false;
			districtHaveBlock=isBlock;
			if(isBlock){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}
					}
				});

				$(totalStudentsSectorWise.blockDiv).show();
				$(totalStudentsSectorWise.sectorDiv).show();
				destroyRefresh(totalStudentsSectorWise.block);
				if(gSetOptionFlag){
				selfChart3.fnSetOptionsForBlock($(totalStudentsSectorWise.block));
				}
			}else{
				$(totalStudentsSectorWise.blockDiv).hide();
				$(totalStudentsSectorWise.sectorDiv).hide();
				destroyRefresh(totalStudentsSectorWise.block);
				selfChart3.fnEmptyCanvas(districtHaveNoBlock);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForBlock:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllBlockText,
					nonSelectedText: nonSelectedBlockText,
					nSelectedText:nSelectedBlockText,
					allSelectedText:allSelectedBlockText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedBlockId=[];
						$(totalStudentsSectorWise.canvasSpan).hide();
						$(totalStudentsSectorWise.block).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedBlockId.push($(option).val());
								}
							});
						});
						if(selectedBlockId.length>0){
							selfChart3.fnEnableDisableSubmitBtn(false);
							selfChart3.fnBindSectorData(true,true);
						}else{
							$(totalStudentsSectorWise.sectorDiv).hide();
							selfChart3.fnShowError(arg);
						}
					

					} 
				});

			});
			fnCollapseMultiselect();
		},
//		bind data in district drop box
		fnBindSectorData:function(selected,hSetOptionFlag){
			destroyRefresh(totalStudentsSectorWise.sector);
			$ele =$(totalStudentsSectorWise.sector);
			if(hSetOptionFlag){
			selfChart3.fnSetOptionsForSectors($(totalStudentsSectorWise.sector));
			}
			$(totalStudentsSectorWise.sectorDiv).show();

		},
//		sector drop box dropdown event handling
		fnSetOptionsForSectors:function(){

			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllSectorText,
					nonSelectedText: nonSelectedSectorText,
					nSelectedText:nSelectedSectorText,
					allSelectedText:allSelectedSectorText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						
						$(totalStudentsSectorWise.canvasSpan).hide();
						var selectedSectorId=$(totalStudentsSectorWise.sector).find("option:selected").val();

						if(selectedSectorId>0){
							selfChart3.fnEnableDisableSubmitBtn(false);
						}else{
							$(totalStudentsSectorWise.canvasSpan).show();
							selfChart3.fnEmptyCanvas();
						}
						selfChart3.fnShowError(arg);
					} 
				});

			});
			fnCollapseMultiselect();


		},
		fnUpdateDoughnutChart:function(){
			var pStateIds=[];

			$(reportDashBoard.state).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						pStateIds.push($(option).val());
					}
				});
			});
			var pYearIds=[];
			$(reportDashBoard.year).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						pYearIds.push($(option).val());
					}
				});
			});
			var pBlockId=[];
			$(totalStudentsSectorWise.block).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						pBlockId.push($(option).val());
					}
				});
			});
			var pDistrictId=[];
			$(totalStudentsSectorWise.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						pDistrictId.push($(option).val());
					}
				});
			});
			var pSectorId=$(totalStudentsSectorWise.sector).find("option:selected").val();
			var jsonData={
					"stateIds":pStateIds,
					"academicYearIds":pYearIds,
					"sectorId":pSectorId,
					"blockIds":pBlockId
			}
			if(pStateIds.length>0 && pDistrictId.length>0 && pBlockId.length>0 && pYearIds.length>0){
				$(totalStudentsSectorWise.canvasSpan).text(" ");
				var doughnutChartData=customAjaxCalling("dashboard/students", jsonData, "POST");
				var result=doughnutChartData.series!=null?doughnutChartData.series.length>0?true:false:false;
				if(result){
					$(totalStudentsSectorWise.canvasSpan).hide();
					var doughnutChartDataConfig = {
							type: 'doughnut',
							data: {
								datasets: [{
									data:doughnutChartData.series ,
									backgroundColor:doughnutChartData.backgroundColor,
									label: 'Dataset'
								}

								],

								labels: doughnutChartData.labels

							},
							options: {
								responsive: true,
								display: false,
								legend: {
									position: 'bottom',
									 labels: {
							                fontColor: 'black'
							          }
								},								
								animation: {
									animateScale: true,
									animateRotate: true
								},
								
								
							}
					};

					$(totalStudentsSectorWise.canvas).remove();
					$(totalStudentsSectorWise.canvasDiv).append('<canvas id="chart-area3" width="280" height="280"/>');
					var canvas = document.getElementById("chart-area3");
					var context = canvas.getContext('2d');
					context.clear();
					window.chart3Doughnut = new Chart(context, doughnutChartDataConfig);
					window.chart3Doughnut.update();
					setCanvasStyle("chart-area3");
				}else{
					$(totalStudentsSectorWise.canvasSpan).show();
					selfChart3.fnEmptyCanvas(emptyChartMessage);
					setMessageIfChartContainsEmptyData("chart-area3",window.chart3Doughnut);
				}
			}else{
				setMessageIfChartContainsEmptyData("chart-area3",window.chart3Doughnut);
			}
			$('.spinner').hide();

		},
		fnEmptyCanvas:function(message){
			$(totalStudentsSectorWise.canvasSpan).text(" ");
			selfChart3.fnEnableDisableSubmitBtn(true);
			$(totalStudentsSectorWise.canvasSpan).text(message);
			$(totalStudentsSectorWise.canvasSpan).show();

		},
		fnShowError:function(currentElement){
			$(totalStudentsSectorWise.canvasSpan).text(" ");
			var elements =[reportDashBoard.year,reportDashBoard.state,totalStudentsSectorWise.district,totalStudentsSectorWise.block,totalStudentsSectorWise.sector];
			var message =[selectYearMessage,selectStateMessage,selectDistrictMessage,selectBlockMessage,selectSectorMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(!isErrorShwon && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(totalStudentsSectorWise.canvasSpan).text(message[i]);
						selfChart3.fnEnableDisableSubmitBtn(true);
						$(totalStudentsSectorWise.canvasSpan).show();
						$.each(elements,function(j,data){
							if(j>i){
								$(data+'Div').hide();
							}else{
								$(data+'Div').show();
							}
						});
						isErrorShwon =true;
						return false;
					}else{
						$.each(elements,function(j,data){
							$(data+'Div').show();
						});
					}
				}
			});

		},
		fnTotalStudentsSectorWiseInIt:function(){
			selfChart3.fnBindDistrictData(stateIds,true);
			selfChart3.fnBindBlockData(true);
			selfChart3.fnBindSectorData(true);
			selfChart3.fnUpdateDoughnutChart();
			
			selfChart3.fnSetOptionsForDistrict($(totalStudentsSectorWise.district));
			selfChart3.fnSetOptionsForBlock($(totalStudentsSectorWise.block));
			selfChart3.fnSetOptionsForSectors($(totalStudentsSectorWise.sector));
		},
		fnPopoverInit:function(){
			updatePopOver('#popoverTotalStudentsSectorWise');
		},
		fnSubmitHandler:function(){
			selfChart3.fnUpdateDoughnutChart();
			selfChart3.fnHidePopOver();
			
		},
		fnHidePopOver:function(){
			$('#popoverTotalStudentsSectorWise').closest("#canvas-holder").find(".chart-filter-container").slideUp(0);
		},
		fnEnableDisableSubmitBtn:function(isDisabled){
			$(totalStudentsSectorWise.submit).prop('disabled',isDisabled)
		},


}

////////////////end Total Students //////////////////


var studentsPerformanceAcrossStateBarChart={
		init:function(){
			selfChart4=studentsPerformanceAcrossStateBarChart;
		},
		// bind data in district drop box
		fnBindDistrictData:function(selStateIds,selected,iSetOptionFlag){
			fnEmptyAll(studentsPerformanceAcrossStates.district);
			destroyRefresh(studentsPerformanceAcrossStates.district);
			$ele =$(studentsPerformanceAcrossStates.district);
			$ele.html('');
			var vStateIds=fnGetStateId();
			var data =customAjaxCalling("district/dashboard/state",vStateIds,'POST');
			var isDistrict=data!=null?data.length>0?true:false:false;
			stateHaveDistrict=isDistrict;
			if(isDistrict){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}
					}
				});

				$(studentsPerformanceAcrossStates.districtDiv).show();
				destroyRefresh(studentsPerformanceAcrossStates.district);
				if(iSetOptionFlag){
					selfChart4.fnSetOptionsForDistrict($(studentsPerformanceAcrossStates.district));
				}
			}else{
				$(studentsPerformanceAcrossStates.districtDiv).hide();
				destroyRefresh(studentsPerformanceAcrossStates.district);
				selfChart4.fnEmptyCanvas(stateHaveNoDistrict);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForDistrict:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllDistrictText,
					nonSelectedText: nonSelectedDistrictText,
					nSelectedText:nSelectedDistrictText,
					allSelectedText:allSelectedDistrictText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedDistrictId=[];
						$(studentsPerformanceAcrossStates.canvasSpan).hide();
						$(studentsPerformanceAcrossStates.district).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedDistrictId.push($(option).val());
								}
							});
						});
						if(selectedDistrictId.length>0){
							selfChart4.fnEnableDisableSubmitBtn(false);
							selfChart4.fnBindBlockData(true,true);
						 }else{
							$(studentsPerformanceAcrossStates.blockDiv).hide();
							selfChart4.fnShowError(arg);

						}
						

					} 
				});

			});
			fnCollapseMultiselect();
		},
		// bind data in district drop box
		fnBindBlockData:function(selected,jSetOptionFlag){
			fnEmptyAll(studentsPerformanceAcrossStates.block);
			destroyRefresh(studentsPerformanceAcrossStates.block);
			$ele =$(studentsPerformanceAcrossStates.block);
			$ele.html('');
			var selDistrictIds=[];
			$(studentsPerformanceAcrossStates.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selDistrictIds.push($(option).val());
					}
				});
			});
			var data =customAjaxCalling("block/dashboard/district",selDistrictIds,'POST');
			var isBlock=data!=null?data.length>0?true:false:false;
			districtHaveBlock=isBlock;
			if(isBlock){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}
					}
				});

				$(studentsPerformanceAcrossStates.blockDiv).show();
				$(studentsPerformanceAcrossStates.gradeDiv).show();
				$(studentsPerformanceAcrossStates.subjectDiv).show();
				destroyRefresh(studentsPerformanceAcrossStates.block);
				if(jSetOptionFlag){
					selfChart4.fnSetOptionsForBlock($(studentsPerformanceAcrossStates.block));
				}
			}else{
				$(studentsPerformanceAcrossStates.blockDiv).hide();
				$(studentsPerformanceAcrossStates.gradeDiv).hide();
				$(studentsPerformanceAcrossStates.subjectDiv).hide();
				destroyRefresh(studentsPerformanceAcrossStates.block);
				selfChart4.fnEmptyCanvas(districtHaveNoBlock);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForBlock:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllBlockText,
					nonSelectedText: nonSelectedBlockText,
					nSelectedText:nSelectedBlockText,
					allSelectedText:allSelectedBlockText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedBlockId=[];
						$(studentsPerformanceAcrossStates.canvasSpan).hide();
						$(studentsPerformanceAcrossStates.block).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedBlockId.push($(option).val());
								}
							});
						});
						if(selectedBlockId.length>0){
							selfChart4.fnEnableDisableSubmitBtn(false);
							selfChart4.fnBindGradeData(true,true);
						}else{
							selfChart4.fnEmptyCanvas(selectBlockMessage);
							selfChart4.fnShowError(arg);
						}
						

					} 
				});

			});
			fnCollapseMultiselect();
		},
		fnBindGradeData:function(selected,kSetOptionFlag){
			fnEmptyAll(studentsPerformanceAcrossStates.grade);
			destroyRefresh(studentsPerformanceAcrossStates.grade);
			$ele =$(studentsPerformanceAcrossStates.grade);
			$ele.html('');
			var data=grades;
			if(data!=null){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.gradeId+'" data-id="' + obj.gradeId + '"data-name="' +obj.gradeName+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.gradeId+'" data-id="' + obj.gradeId + '"data-name="' +obj.gradeName+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
						}
					}
				});
			}
			destroyRefresh(studentsPerformanceAcrossStates.grade);
			if(kSetOptionFlag){
				selfChart4.fnSetOptionsForGrades($(studentsPerformanceAcrossStates.grade));
			}
			$(studentsPerformanceAcrossStates.gradeDiv).show();
			$(studentsPerformanceAcrossStates.subjectDiv).show();


		},
//		state drop box dropdown event handling
		fnSetOptionsForGrades:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllGradeText,
					nonSelectedText: nonSelectedGradeText,
					nSelectedText:nSelectedGradeText,
					allSelectedText:allSelectedGradeText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var aselectedGradeId=[];
						$(studentsPerformanceAcrossStates.canvasSpan).hide();
						$(studentsPerformanceAcrossStates.grade).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									aselectedGradeId.push($(option).val());
								}
							});
						});
						if(aselectedGradeId.length>0){
							selfChart4.fnBindSubjectData(true);
							selfChart4.fnBindSubjectData(true,true);
							selfChart4.fnEnableDisableSubmitBtn(false);
							$(studentsPerformanceAcrossStates.subjectDiv).show();
						}else{

							$(studentsPerformanceAcrossStates.subjectDiv).hide();
							selfChart4.fnEmptyCanvas(selectGradeMessage);
						}
						selfChart4.fnShowError(arg);
					} 
				});

			});
			fnCollapseMultiselect();


		},
		fnBindSubjectData:function(selected,lSetOptionFlag){
			var gSelectedGradeId=[];
			$(studentsPerformanceAcrossStates.canvasSpan).hide();
			$(studentsPerformanceAcrossStates.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						gSelectedGradeId.push($(option).val());
					}
				});
			});

			fnEmptyAll(studentsPerformanceAcrossStates.subject);
			destroyRefresh(studentsPerformanceAcrossStates.subject);
			$ele =$(studentsPerformanceAcrossStates.subject);
			$ele.html('');
			var jsonData={
					"gradeIds":gSelectedGradeId

			}
			var data =customAjaxCalling("subject/dashboard/grades",jsonData,'POST');
			var result=data!=null?data.length>0?true:false:false;
			if(result){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-name="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-name="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
						}
					}
				});
			}
			else{
				selfChart4.fnEmptyCanvas(emptyChartMessage);
			}
			destroyRefresh(studentsPerformanceAcrossStates.subject);
			if(lSetOptionFlag){
				selfChart4.fnSetOptionsFoSubject($(studentsPerformanceAcrossStates.subject));
			}
			$(studentsPerformanceAcrossStates.subjectDiv).show();
			fnCollapseMultiselect();
		},

//		subject drop box dropdown event handling
		fnSetOptionsFoSubject:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllSubjectText,
					nonSelectedText: nonSelectedSubjectText,
					nSelectedText:nSelectedSubjectText,
					allSelectedText:allSelectedSubjectText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var aselectedSubjectId=[];
						$(studentsPerformanceAcrossStates.canvasSpan).hide();
						$(studentsPerformanceAcrossStates.subject).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									aselectedSubjectId.push($(option).val());
								}
							});
						});
						if(aselectedSubjectId.length>0){
							selfChart4.fnEnableDisableSubmitBtn(false);
						}else{
							$(studentsPerformanceAcrossStates.canvasSpan).show();
							selfChart4.fnEmptyCanvas(selectSubjectMessage);
						}
						selfChart4.fnShowError(arg);
					} 
				});

			});
			fnCollapseMultiselect();
		},
		fnUpdateBarChart:function(){
			var mStateIds=[];

			$(reportDashBoard.state).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						mStateIds.push($(option).val());
					}
				});
			});
			var mYearIds=[];
			$(reportDashBoard.year).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						mYearIds.push($(option).val());
					}
				});
			});
			var mBlockIds=[];
			$(studentsPerformanceAcrossStates.block).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						mBlockIds.push($(option).val());
					}
				});
			});
			var mDistrictIds=[];
			$(studentsPerformanceAcrossStates.block).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						mDistrictIds.push($(option).val());
					}
				});
			});
			var mGradeId=[];
			$(studentsPerformanceAcrossStates.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						mGradeId.push($(option).val());
					}
				});
			});
			var mSubjectId=[];
			$(studentsPerformanceAcrossStates.subject).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						mSubjectId.push($(option).val());
					}
				});
			});

			var jsonData={
					"stateIds":mStateIds,
					"academicYearIds":mYearIds,
					"districtIds":mDistrictIds,
					"blockIds":mBlockIds,
					"gradeIds":mGradeId,
					"subjectIds":mSubjectId,
			}
			if(mGradeId.length>0 && mSubjectId.length>0){

				var barChartData=customAjaxCalling("dashboard/studentsPerformance/state", jsonData, "POST");
				var result=barChartData.datasets!=null?barChartData.datasets.length>0?true:false:false;
				if(result){
					$(studentsPerformanceAcrossStates.canvasSpan).text(" ");
					$(studentsPerformanceAcrossStates.canvasSpan).hide();
					var studentsPerformanceAcrossSectorsBarChartData = {
							labels: barChartData.labels,
							datasets: barChartData.datasets

					};

					var studentsPerformanceAcrossSectorsBarChartDataConfig = {
							type: 'bar',
							data: studentsPerformanceAcrossSectorsBarChartData,
							options: {
								elements: {
									rectangle: {
										borderWidth: 0.5,
										borderColor: randomColor(),
										borderSkipped: 'bottom'
									}
								},
								responsive: true,
								legend: {
									display: false,
									 labels: {
							                fontColor: 'black'
							          }
								},
								scales: {
									yAxes: [{
										ticks: {
											beginAtZero:true,
											suggestedMin: 0,
											suggestedMax: 10,
											fontColor: "black",
										}
									}],
									xAxes: [{
						                    ticks: {
						                        fontColor: "black",				                   
						                    }
						             }],
								},
								legendCallback: function(chart) {
									
									var html = [];
									html.push('<ul class="' + chart.id + '-legend">');
									for (var i=0; i<chart.data.datasets.length; i++) {
										html.push('<li class="strikeout" onclick="chart4Bar.updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')"><span class="strikeout" style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
										if (chart.data.datasets[i].label) {
											html.push(chart.data.datasets[i].label);
										}
										html.push('</li>');
									}
									html.push('</ul>');                                                          
									return html.join("");                                                        
								}, 

							}


					}

					$(studentsPerformanceAcrossStates.canvas).remove();
					$(studentsPerformanceAcrossStates.canvasDiv).append('<canvas id="chart-area4" width="280" height="280"/>');
					var canvas = document.getElementById("chart-area4");
					var context = canvas.getContext('2d');
					context.clear();
					window.chart4 = new Chart(context, studentsPerformanceAcrossSectorsBarChartDataConfig);
					window.chart4.update();
					chart4Bar.updateChartLegend("chart4Legend");

				}else{
					setMessageIfChartContainsEmptyData("chart-area4",window.chart4);
				}
			}else{
				setMessageIfChartContainsEmptyData("chart-area4",window.chart4);
			}
			$('.spinner').hide();
		},
		fnEmptyCanvas:function(message){
			$(studentsPerformanceAcrossStates.canvasSpan).text(" ");
			selfChart4.fnEnableDisableSubmitBtn(true);
			$(studentsPerformanceAcrossStates.canvasSpan).show();
			$(studentsPerformanceAcrossStates.canvasSpan).text(message)
		},
		fnShowError:function(currentElement){
			var elements =[reportDashBoard.year,reportDashBoard.state,studentsPerformanceAcrossStates.district,studentsPerformanceAcrossStates.block,studentsPerformanceAcrossStates.grade,studentsPerformanceAcrossStates.subject];
			var message =[selectYearMessage,selectStateMessage,selectDistrictMessage,selectBlockMessage,selectGradeMessage, selectSubjectMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(!isErrorShwon && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentsPerformanceAcrossStates.canvasSpan).text(message[i]);
						selfChart4.fnEnableDisableSubmitBtn(true);
						$(studentsPerformanceAcrossStates.canvasSpan).show();
						$.each(elements,function(j,data){
							if(j>i){
								$(data+'Div').hide();
							}else{
								$(data+'Div').show();
							}
						});
						isErrorShwon =true;
						return false;
					}else{
						$.each(elements,function(j,data){
							$(data+'Div').show();
						});
					}
				}
			});

		},
		studentsPerformanceAcrossStateBarChartInIt:function(){
			selfChart4.fnBindDistrictData(stateIds,true);
			selfChart4.fnBindBlockData(true);
			selfChart4.fnBindGradeData(true);
			selfChart4.fnBindSubjectData(true);
			selfChart4.fnUpdateBarChart();
			
			selfChart4.fnSetOptionsForDistrict($(studentsPerformanceAcrossStates.district));
			selfChart4.fnSetOptionsForBlock($(studentsPerformanceAcrossStates.block));
			selfChart4.fnSetOptionsForGrades($(studentsPerformanceAcrossStates.grade));
			selfChart4.fnSetOptionsFoSubject($(studentsPerformanceAcrossStates.subject));
		},
		fnPopoverInit:function(){
			updatePopOver('#popoverStudentsPerformanceAcrossStates');
		},
		fnSubmitHandler:function(){
			$('.spinner').show();
			$.wait(5).then(selfChart4.fnHidePopOver).then(selfChart4.fnUpdateBarChart)
			$('.spinner').hide();
			
			selfChart4.fnHidePopOver();
		},
		fnHidePopOver:function(){
			$('#popoverStudentsPerformanceAcrossStates').closest("#canvas-holder").find(".chart-filter-container").slideUp(0);
		},
		fnEnableDisableSubmitBtn:function(isDisabled){
			$(studentsPerformanceAcrossStates.submit).prop('disabled',isDisabled)
		},

}

////////////////Start Students Performance Accross Project /////////////////



var studentsPerformanceAccrossProjectBarChart={
		init:function(){
			selfChart6=studentsPerformanceAccrossProjectBarChart;
		},
		// bind data in district drop box
		fnBindDistrictData:function(selStateIds,selected,setOptionFlag){
			fnEmptyAll(studentsPerformanceAccrossProject.district);
			destroyRefresh(studentsPerformanceAccrossProject.district);
			$ele =$(studentsPerformanceAccrossProject.district);
			$ele.html('');
			var vStateIds=fnGetStateId();
			var data =customAjaxCalling("district/dashboard/state",vStateIds,'POST');
			var isDistrict=data!=null?data.length>0?true:false:false;
			stateHaveDistrict=isDistrict;
			if(isDistrict){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.districtId+'" data-id="' + obj.districtId + '"data-name="' +obj.districtName+ '">' +escapeHtmlCharacters(obj.districtName)+ '</option>');
						}
					}
				});

				$(studentsPerformanceAccrossProject.districtDiv).show();
				destroyRefresh(studentsPerformanceAccrossProject.district);
				if(setOptionFlag){
				selfChart6.fnSetOptionsForDistrict($(studentsPerformanceAccrossProject.district));
				}
				}else{
				$(studentsPerformanceAccrossProject.districtDiv).hide();
				destroyRefresh(studentsPerformanceAccrossProject.district);
				selfChart6.fnEmptyCanvas(stateHaveNoDistrict);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForDistrict:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllDistrictText,
					nonSelectedText: nonSelectedDistrictText,
					nSelectedText:nSelectedDistrictText,
					allSelectedText:allSelectedDistrictText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedDistrictId=[];
						$(studentsPerformanceAccrossProject.canvasSpan).hide();
						$(studentsPerformanceAccrossProject.district).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedDistrictId.push($(option).val());
								}
							});
						});
						if(selectedDistrictId.length>0){
							selfChart6.fnEnableDisableSubmitBtn(false);
							selfChart6.fnBindBlockData(true,true);
							selfChart6.fnBindProjectData(true,true);
						}else{
							selfChart6.fnEmptyCanvas(selectDistrictMessage);
							$(studentsPerformanceAccrossProject.blockDiv).hide();
							selfChart6.fnShowError(arg);
						}
					} 
				});

			});
			fnCollapseMultiselect();
		},
		// bind data in district drop box
		fnBindBlockData:function(selected,setOptionFlag){
			fnEmptyAll(studentsPerformanceAccrossProject.block);
			destroyRefresh(studentsPerformanceAccrossProject.block);
			$ele =$(studentsPerformanceAccrossProject.block);
			$ele.html('');
			var selDistrictIds=[];
			$(studentsPerformanceAccrossProject.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selDistrictIds.push($(option).val());
					}
				});
			});
			var data =customAjaxCalling("block/dashboard/district",selDistrictIds,'POST');
			var isBlock=data!=null?data.length>0?true:false:false;
			districtHaveBlock=isBlock;
			if(isBlock){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.blockId+'" data-id="' + obj.blockId + '"data-name="' +obj.blockName+ '">' +escapeHtmlCharacters(obj.blockName)+ '</option>');
						}
					}
				});

				$(studentsPerformanceAccrossProject.blockDiv).show();
				$(studentsPerformanceAccrossProject.gradeDiv).show();
				$(studentsPerformanceAccrossProject.genderDiv).show();
				destroyRefresh(studentsPerformanceAccrossProject.block);
				if(setOptionFlag){
				selfChart6.fnSetOptionsForBlock($(studentsPerformanceAccrossProject.block));
				}
			}else{
				$(studentsPerformanceAccrossProject.blockDiv).hide();
				$(studentsPerformanceAccrossProject.gradeDiv).hide();
				$(studentsPerformanceAccrossProject.genderDiv).hide();
				destroyRefresh(studentsPerformanceAccrossProject.block);
				selfChart6.fnEmptyCanvas(districtHaveNoBlock);
			}


		},
		// district drop box dropdown event handling
		fnSetOptionsForBlock:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllBlockText,
					nonSelectedText: nonSelectedBlockText,
					nSelectedText:nSelectedBlockText,
					allSelectedText:allSelectedBlockText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var selectedBlockId=[];
						$(studentsPerformanceAccrossProject.canvasSpan).hide();
						$(studentsPerformanceAccrossProject.block).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									selectedBlockId.push($(option).val());
								}
							});
						});
						if(selectedBlockId.length>0){
							selfChart6.fnEnableDisableSubmitBtn(false);
							selfChart6.fnBindGradeData(true,true);
							selfChart6.fnBindProjectData(true,true);
						}else{
							selfChart6.fnEmptyCanvas(selectBlockMessage);
							selfChart6.fnShowError(arg);
						}
						

					} 
				});

			});
			fnCollapseMultiselect();
		},
		fnBindGradeData:function(selected,setOptionFlag){
			fnEmptyAll(studentsPerformanceAccrossProject.grade);
			destroyRefresh(studentsPerformanceAccrossProject.grade);
			$ele =$(studentsPerformanceAccrossProject.grade);
			$ele.html('');
			var data=grades;
			if(data!=null){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.gradeId+'" data-id="' + obj.gradeId + '"data-name="' +obj.gradeName+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.gradeId+'" data-id="' + obj.gradeId + '"data-name="' +obj.gradeName+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
						}
					}
				});
				$(studentsPerformanceAccrossProject.gradeDiv).show();
			}else{
				$(studentsPerformanceAccrossProject.gradeDiv).hide();
				selfChart6.fnEmptyCanvas(filterHaveNoGrade);
			}
			destroyRefresh(studentsPerformanceAccrossProject.grade);
			if(setOptionFlag){
			selfChart6.fnSetOptionsForGrades($(studentsPerformanceAccrossProject.grade));
			}
		},
//		state drop box dropdown event handling
		fnSetOptionsForGrades:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllGradeText,
					nonSelectedText: nonSelectedGradeText,
					nSelectedText:nSelectedGradeText,
					allSelectedText:allSelectedGradeText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var aselectedGradeId=[];
						$(studentsPerformanceAccrossProject.canvasSpan).hide();
						$(studentsPerformanceAccrossProject.grade).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									aselectedGradeId.push($(option).val());
								}
							});
						});
						if(aselectedGradeId.length>0){
							selfChart6.fnEnableDisableSubmitBtn(false);
							$(studentsPerformanceAccrossProject.projectDiv).show();
							selfChart6.fnBindProjectData(true,true);
							
						}else{
                            $(studentsPerformanceAccrossProject.projectDiv).hide();
							$(studentsPerformanceAccrossProject.genderDiv).hide();
							selfChart6.fnEmptyCanvas(selectGradeMessage);
							selfChart6.fnShowError(arg);
						}
						
					} 
				});

			});
			fnCollapseMultiselect();


		},
		fnBindProjectData:function(selected,setOptionFlag){

			fnEmptyAll(studentsPerformanceAccrossProject.project);
			destroyRefresh(studentsPerformanceAccrossProject.project);
			$ele =$(studentsPerformanceAccrossProject.project);
			$ele.html('');

			var gStateIds=[];
			$(reportDashBoard.state).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						gStateIds.push($(option).val());
					}
				});
			});
			var gYearIds=[];
			$(reportDashBoard.year).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						gYearIds.push($(option).val());
					}
				});
			});

			var gDistrictIds=[];
			$(studentsPerformanceAccrossProject.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						gDistrictIds.push($(option).val());
					}
				});
			});

			var gBlockIds=[];
			$(studentsPerformanceAccrossProject.block).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						gBlockIds.push($(option).val());
					}
				});
			});

			var gGradeId=[];
			$(studentsPerformanceAccrossProject.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						gGradeId.push($(option).val());
					}
				});
			});


			var projectJasonData={
					"academicYearIds":gYearIds,
					"stateIds":gStateIds,
					"districtIds":gDistrictIds,
					"blockIds":gBlockIds,
					"gradeIds":gGradeId
			};

			var data =customAjaxCalling("campaign/dashboard/campaigns",projectJasonData,'POST');
			var result=data!=null?data.length>0?true:false:false;
			if(result){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.campaignId+'" data-id="' + obj.campaignId + '"data-name="' +obj.campaignName+ '">' +escapeHtmlCharacters(obj.campaignName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.campaignId+'" data-id="' + obj.campaignId + '"data-name="' +obj.campaignName+ '">' +escapeHtmlCharacters(obj.campaignName)+ '</option>');
						}
					}
				});
				$(studentsPerformanceAccrossProject.projectDiv).show();
				$(studentsPerformanceAccrossProject.genderDiv).show();
				
			}else{
				$(studentsPerformanceAccrossProject.projectDiv).hide();
				$(studentsPerformanceAccrossProject.genderDiv).hide();
				selfChart6.fnEmptyCanvas(filterHaveNoProject);
			}
			destroyRefresh(studentsPerformanceAccrossProject.project);
			if(setOptionFlag){
				selfChart6.fnSetOptionsForProject($(studentsPerformanceAccrossProject.project));
			}
			fnCollapseMultiselect();
		},

//		subject drop box dropdown event handling
		fnSetOptionsForProject:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllProjectText,
					nonSelectedText: nonSelectedProjectText,
					nSelectedText:nSelectedProjectText,
					allSelectedText:allSelectedProjectText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var aselectedProjectId=[];
						$(studentsPerformanceAccrossProject.canvasSpan).hide();
						$(studentsPerformanceAccrossProject.project).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									aselectedProjectId.push($(option).val());
								}
							});
						});
						if(aselectedProjectId.length>0){
							selfChart6.fnBindGenderData(true,true);
							selfChart6.fnEnableDisableSubmitBtn(false);

						}else{
							$(studentsPerformanceAccrossProject.genderDiv).hide();
							$(studentsPerformanceAccrossProject.canvasSpan).show();
							selfChart6.fnEmptyCanvas(selectProjectMessage);
						}
						selfChart6.fnShowError(arg);
					} 
				});

			});
			fnCollapseMultiselect();
		},
		fnBindGenderData:function(selected,setOptionFlag){

			fnEmptyAll(studentsPerformanceAccrossProject.gender);
			destroyRefresh(studentsPerformanceAccrossProject.gender);
			$ele =$(studentsPerformanceAccrossProject.gender);
			$ele.html('');

			var data =customAjaxCalling("student/dashboard/genders",null,'POST');
			var result=data!=null?data.length>0?true:false:false;
			if(result){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj+'" data-id="' + obj + '"data-name="' +obj+ '">' +escapeHtmlCharacters(obj)+ '</option>');
						}else{
							$ele.append('<option value="'+obj+'" data-id="' + obj + '"data-name="' +obj+ '">' +escapeHtmlCharacters(obj)+ '</option>');
						}
					}
				});
				$(studentsPerformanceAccrossProject.genderDiv).show();
			}
			else{
				$(studentsPerformanceAccrossProject.genderDiv).hide();
				selfChart6.fnEmptyCanvas(filterHaveNoGender);
			}
			destroyRefresh(studentsPerformanceAccrossProject.gender);
			if(setOptionFlag){
			selfChart6.fnSetOptionsForGender($(studentsPerformanceAccrossProject.gender));
			}
			fnCollapseMultiselect();
		},

//		subject drop box dropdown event handling
		fnSetOptionsForGender:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllGenderText,
					nonSelectedText: nonSelectedGenderText,
					nSelectedText:nSelectedGenderText,
					allSelectedText:allSelectedGenderText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var aselectedGenders=[];
						$(studentsPerformanceAccrossProject.canvasSpan).hide();
						$(studentsPerformanceAccrossProject.gender).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									aselectedGenders.push($(option).val());
								}
							});
						});
						if(aselectedGenders.length>0){
							selfChart6.fnEnableDisableSubmitBtn(false);
						}else{
							$(studentsPerformanceAccrossProject.canvasSpan).show();
						}
						selfChart6.fnShowError(arg);
					} 
				});

			});
			fnCollapseMultiselect();
		},

		fnUpdateLineChart:function(){

			var bStateIds=[];
			$(reportDashBoard.state).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bStateIds.push($(option).val());
					}
				});
			});
			var bYearIds=[];
			$(reportDashBoard.year).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bYearIds.push($(option).val());
					}
				});
			});

			var bDistrictIds=[];
			$(studentsPerformanceAccrossProject.district).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bDistrictIds.push($(option).val());
					}
				});
			});

			var bBlockIds=[];
			$(studentsPerformanceAccrossProject.block).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bBlockIds.push($(option).val());
					}
				});
			});

			var bGradeId=[];
			$(studentsPerformanceAccrossProject.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bGradeId.push($(option).val());
					}
				});
			});
			var bProjectId=[];
			$(studentsPerformanceAccrossProject.project).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bProjectId.push($(option).val());
					}
				});
			});
			var bGenders=[];
			$(studentsPerformanceAccrossProject.gender).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bGenders.push("'"+$(option).val()+"'");
					}
				});
			});

			var jsonData={
					"academicYearIds":bYearIds,
					"stateIds":bStateIds,
					"districtIds":bDistrictIds,
					"blockIds":bBlockIds,
					"gradeIds":bGradeId,
					"projectIds":bProjectId,
					"genders":bGenders
			}
			if(bProjectId.length>0 && bGenders.length>0 && bBlockIds.length>0 && bGradeId.length>0 && bStateIds.length>0 && bDistrictIds.length>0 ){

				var lineChartData=customAjaxCalling("dashboard/studentsPerformance/project", jsonData, "POST");
				var result=lineChartData.datasets!=null?lineChartData.datasets.length>0?true:false:false;
				if(result){
					$(studentsPerformanceAccrossProject.canvasSpan).hide();
					$(studentsPerformanceAccrossProject.canvasSpan).text("");
					var chart6lineconfig = {
							type: 'line',
							data: {
								labels: lineChartData.labels,
								datasets:lineChartData.datasets
							},
							
							
							//@author : debendra
							options:lineChart.getOptions(),
					};

					$.each(chart6lineconfig.data.datasets, function(i, dataset) {
						var background = randomColor(0.5);
						dataset.borderColor = background;
						dataset.backgroundColor = background;
						dataset.pointBorderColor = background;
						dataset.pointBackgroundColor = background;
						dataset.pointBorderWidth = 1;
					});

					$(studentsPerformanceAccrossProject.canvas).remove();
					$(studentsPerformanceAccrossProject.canvasDiv).append('<canvas id="chart-area6" width="280" height="280"/>');
					var canvas = document.getElementById("chart-area6");
					var context = canvas.getContext('2d');
					context.clear();
					window.myLine = new Chart(context, chart6lineconfig);
					window.myLine.update();
					
					
					lineChart.updateLegend("legend");
					
				}
				else{
					$(studentsPerformanceAccrossProject.canvasSpan).show();
					selfChart6.fnEmptyCanvas(emptyChartMessage);
					setMessageIfChartContainsEmptyData("chart-area6",window.myLine);

				}
			}else{
				setMessageIfChartContainsEmptyData("chart-area6",window.myLine);
			}
			$('.spinner').hide();

		},
		fnShowError:function(currentElement){
			var elements =[reportDashBoard.year,reportDashBoard.state,studentsPerformanceAccrossProject.district,studentsPerformanceAccrossProject.block,studentsPerformanceAccrossProject.grade,studentsPerformanceAccrossProject.project,studentsPerformanceAccrossProject.gender];
			var message =[selectYearMessage,selectStateMessage,selectDistrictMessage,selectBlockMessage,selectGradeMessage,selectProjectMessage,selectGenderMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(!isErrorShwon && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentsPerformanceAccrossProject.canvasSpan).text(message[i]);
						selfChart6.fnEnableDisableSubmitBtn(true);
						$(studentsPerformanceAccrossProject.canvasSpan).show();
						$.each(elements,function(j,data){
							if(j>i){
								$(data+'Div').hide();
							}else{
								$(data+'Div').show();
							}
						});
						isErrorShwon =true;
						return false;
					}else{
						$.each(elements,function(j,data){
							$(data+'Div').show();
						});
					}
				}
			});

		},
		fnEmptyCanvas:function(message){
			$(studentsPerformanceAccrossProject.canvasSpan).text(' ');
			selfChart6.fnEnableDisableSubmitBtn(true);
			$(studentsPerformanceAccrossProject.canvasSpan).text(message);
			$(studentsPerformanceAccrossProject.canvasSpan).show();
		},
		studentsPerformanceAccrossProjectInIt:function(){
			selfChart6.fnBindDistrictData(stateIds,true);
			selfChart6.fnBindBlockData(true);
			selfChart6.fnBindGradeData(true);
			selfChart6.fnBindProjectData(true);
			selfChart6.fnBindGenderData(true);
			selfChart6.fnUpdateLineChart();
			
			selfChart6.fnSetOptionsForDistrict($(studentsPerformanceAccrossProject.district));
			selfChart6.fnSetOptionsForBlock($(studentsPerformanceAccrossProject.block));
			selfChart6.fnSetOptionsForGrades($(studentsPerformanceAccrossProject.grade));
			selfChart6.fnSetOptionsForProject($(studentsPerformanceAccrossProject.project));
			selfChart6.fnSetOptionsForGender($(studentsPerformanceAccrossProject.gender));
		},
		fnPopoverInit:function(){
			updatePopOver('#popoverStudentsPerformanceAccrossProject');
		},
		fnSubmitHandler:function(){
			$('.spinner').show();
			$.wait(5).then(selfChart6.fnHidePopOver).then(selfChart6.fnUpdateLineChart)
			$('.spinner').hide();
			selfChart6.fnHidePopOver();
		},
		fnHidePopOver:function(){
			$('#popoverStudentsPerformanceAccrossProject').closest("#canvas-holder").find(".chart-filter-container").slideUp(0);
		},
		fnEnableDisableSubmitBtn:function(isDisabled){
			$(studentsPerformanceAccrossProject.submit).prop('disabled',isDisabled)
		},



}

///////////////////////// start Students Performance Across Phase  ////////////////////////////////////////////
var studentsPerformanceAcrossPhaseBarChart={
		init:function(){
			selfChart5=studentsPerformanceAcrossPhaseBarChart;
		},

		fnBindSectorData:function(){
			$(studentsPerformanceAcrossPhase.sectorDiv).show();
			destroyRefresh(studentsPerformanceAcrossPhase.sector);
			
			$(studentsPerformanceAcrossPhase.sectorDiv).show();
		},
		fnSetOptionsForSector:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllSectorText,
					nonSelectedText: nonSelectedSectorText,
					nSelectedText:nSelectedSectorText,
					allSelectedText:allSelectedSectorText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						$(studentsPerformanceAcrossPhase.gradeDiv).show();
						$(studentsPerformanceAcrossPhase.subjectDiv).show();
						selfChart5.fnBindGradeData(true,true);
						selfChart5.fnEnableDisableSubmitBtn(false);
						selfChart5.fnShowError(arg);

					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnBindGradeData:function(selected,nSetOptionFlag){
			fnEmptyAll(studentsPerformanceAcrossPhase.grade);
			destroyRefresh(studentsPerformanceAcrossPhase.grade);
			$ele =$(studentsPerformanceAcrossPhase.grade);
			$ele.html('');
			var data=grades;
			if(data!=null){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.gradeId+'" data-id="' + obj.gradeId + '"data-name="' +obj.gradeName+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.gradeId+'" data-id="' + obj.gradeId + '"data-name="' +obj.gradeName+ '">' +escapeHtmlCharacters(obj.gradeName)+ '</option>');
						}
					}
				});
			}
			$(studentsPerformanceAcrossPhase.gradeDiv).show();
			destroyRefresh(studentsPerformanceAcrossPhase.grade);
			if(nSetOptionFlag){
			selfChart5.fnSetOptionsForGrades($(studentsPerformanceAcrossPhase.grade));
			}

		},
//		state drop box dropdown event handling
		fnSetOptionsForGrades:function(){

			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllGradeText,
					nonSelectedText: nonSelectedGradeText,
					nSelectedText:nSelectedGradeText,
					allSelectedText:allSelectedGradeText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var aselectedGradeId=[];
						$(studentsPerformanceAcrossPhase.canvasSpan).text("");
						$(studentsPerformanceAcrossPhase.grade).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									aselectedGradeId.push($(option).val());
								}
							});
						});
						if(aselectedGradeId.length>0){
							$(studentsPerformanceAcrossPhase.subjectDiv).show();
							selfChart5.fnBindSubjectData(true,true);
							selfChart5.fnEnableDisableSubmitBtn(false);

						}else{

							$(studentsPerformanceAcrossPhase.subjectDiv).hide();

							selfChart5.fnEmptyCanvas(selectGradeMessage);
						}
						selfChart5.fnShowError(arg);

					} 
				});

			});
			fnCollapseMultiselect();


		},
		fnBindSubjectData:function(selected,oSetOptionFlag){
			var bselectedGradeId=[];
			$(studentsPerformanceAcrossPhase.canvasSpan).text(" ");
			$(studentsPerformanceAcrossPhase.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						bselectedGradeId.push($(option).val());
					}
				});
			});

			fnEmptyAll(studentsPerformanceAcrossPhase.subject);
			destroyRefresh(studentsPerformanceAcrossPhase.subject);
			$ele =$(studentsPerformanceAcrossPhase.subject);
			$ele.html('');
			var jsonData={
					"gradeIds":bselectedGradeId

			}
			var data =customAjaxCalling("subject/dashboard/grades",jsonData,'POST');
			var result=data!=null?data.length>0?true:false:false;
			if(result){
				$.each(data,function(index,obj){
					if(obj!=null){
						if(selected){
							$ele.append('<option selected value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-name="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
						}else{
							$ele.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-name="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');
						}
					}
				});
			}else{
				selfChart5.fnEmptyCanvas(gradeHaveNoSubject);
			}
			destroyRefresh(studentsPerformanceAcrossPhase.subject);
			if(oSetOptionFlag){
			selfChart5.fnSetOptionsFoSubject($(studentsPerformanceAcrossPhase.subject));
			}
			$(studentsPerformanceAcrossPhase.subjectDiv).show();

		},

//		subject drop box dropdown event handling
		fnSetOptionsFoSubject:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					selectAllText:selectAllSubjectText,
					nonSelectedText: nonSelectedSubjectText,
					nSelectedText:nSelectedSubjectText,
					allSelectedText:allSelectedSubjectText,
					numberDisplayed: 1,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function() {
						var aselectedSubjectId=[];
						$(studentsPerformanceAcrossPhase.canvasSpan).text(" ");
						$(studentsPerformanceAcrossPhase.subject).each(function(index, ele) {
							$(ele).find("option:selected").each(function(ind, option) {
								if ($(option).val() != "multiselect-all"){
									aselectedSubjectId.push($(option).val());
								}
							});
						});
						if(aselectedSubjectId.length>0){
							selfChart5.fnEnableDisableSubmitBtn(false);
						}else{
							$(studentsPerformanceAcrossPhase.canvasSpan).show();
							selfChart5.fnEmptyCanvas(selectSubjectMessage);
						}
						selfChart5.fnShowError(arg);
					} 

				});

			});
			fnCollapseMultiselect();
		},
		fnUpdateBarChart:function(){

			var eSelectedYearIds=[];
			$(reportDashBoard.year).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						eSelectedYearIds.push($(option).val());
					}
				});
			});
			var eStateIds=[];
			$(reportDashBoard.state).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						eStateIds.push($(option).val());
					}
				});
			});
			var eSelectedGradeIds=[];
			$(studentsPerformanceAcrossPhase.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						eSelectedGradeIds.push($(option).val());
					}
				});
			});
			var eSelectedSubjectIds=[];
			$(studentsPerformanceAcrossPhase.subject).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						eSelectedSubjectIds.push($(option).val());
					}
				});
			});

			var eSelectedSectorId=$(studentsPerformanceAcrossPhase.sector).find("option:selected").data('id');
			var eName=$(studentsPerformanceAcrossPhase.sector).find("option:selected").data('name');
			var eSchoolType=$(studentsPerformanceAcrossPhase.sector).find("option:selected").data('schooltype');
			var eLabType=$(studentsPerformanceAcrossPhase.sector).find("option:selected").data('labtype');
			var	eSchoolTypeOpenLab={
					"schoolTypeOpenLabId":eSelectedSectorId,
					"name":eName,
					"schoolType":eSchoolType,
					"labType":eLabType
			}

			var jsonData={
					"academicYearIds":eSelectedYearIds,
					"schoolTypeOpenLab":eSchoolTypeOpenLab,					
					"gradeIds":eSelectedGradeIds,
					"subjectIds":eSelectedSubjectIds,
					"stateIds":eStateIds

			}

			if( eStateIds.length>0 && eSelectedSubjectIds.length>0 && eSelectedGradeIds.length>0 && eSelectedYearIds.length>0){

				$(studentsPerformanceAcrossPhase.canvasSpan).text(" ");
				var barChartData=customAjaxCalling("dashboard/studentsPerformance/phase", jsonData, "POST");
				var zChartData =[];
				var result=barChartData.datasets!=null?barChartData.datasets.length>0?true:false:false;
				if(result){
					$.each(barChartData.datasets,function(i,val){
						mData ={
								type:val.type,
								label:val.label,
								backgroundColor: val.backgroundColor,
								data:val.data
						};
						zChartData.push(mData);
					});
					mData ={
							type:barChartData.lineChartDataVm.type,
							label:barChartData.lineChartDataVm.label,
							data:barChartData.lineChartDataVm.data,
							fill: false,
							borderDash: barChartData.lineChartDataVm.borderDash,
					};
					zChartData.push(mData);


					$(studentsPerformanceAcrossPhase.canvasSpan).text(" ");
					var studentsPerformanceAcrossStatesBarChartData = {
							labels: barChartData.labels,
							datasets: zChartData
					};

					var studentsPerformanceAcrossStatesBarChartData = {
							labels: barChartData.labels,
							datasets: zChartData	
					};

					var studentsPerformanceAcrossStatesBarChartDataConfig = {
							type: 'bar',
							data: studentsPerformanceAcrossStatesBarChartData,
							options: {

								elements: {
									rectangle: {
										borderWidth: 0.5,
										borderColor: randomColor(),
										borderSkipped: 'bottom'
									}
								},
								responsive: true,
								legend: {
									display:false
								},
								scales: {
									yAxes: [{
										ticks: {
											beginAtZero:true,
											suggestedMin: 0,
											suggestedMax: 10,
											fontColor: "black",
										}
									}],
									xAxes: [{
										ticks: {
											fontColor: "black",				                   
										}
									}],
								},
								legendCallback: function(chart) {
									
									var html = [];
									html.push('<ul class="' + chart.id + '-legend">');
									for (var i=0; i<chart.data.datasets.length; i++) {
										html.push('<li class="strikeout" onclick="chart5Bar.updateDataset(event, ' + '\'' + chart.legend.legendItems[i].datasetIndex + '\'' + ')"><span class="strikeout" style="background-color:' + chart.data.datasets[i].backgroundColor + '"></span>');
										if (chart.data.datasets[i].label) {
											html.push(chart.data.datasets[i].label);
										}
										html.push('</li>');
									}
									html.push('</ul>');                                                          
									return html.join("");                                                        
								}, 

							}


					}


					$.each(studentsPerformanceAcrossStatesBarChartDataConfig.data.datasets, function(i, dataset) {
						var background = randomColor(0.4);
						dataset.borderColor = randomColor(0.5);
						dataset.backgroundColor = randomColor(0.7);
						dataset.pointBorderColor = randomColor(0.5);
						dataset.pointBackgroundColor = randomColor(0.4);
						dataset.pointBorderWidth = 1;
					});

					$(studentsPerformanceAcrossPhase.canvas).remove();
					$(studentsPerformanceAcrossPhase.canvasDiv).append('<canvas id="chart-area5" width="280" height="280"/>');
					var canvas = document.getElementById("chart-area5");
					var context = canvas.getContext('2d');
					context.clear();
					window.chart5 = new Chart(context, studentsPerformanceAcrossStatesBarChartDataConfig);
					window.chart5.update();

					chart5Bar.updateChartLegend("chart5Legend");

				}else{
					setMessageIfChartContainsEmptyData("chart-area5",window.chart5);
				}
			}else{
				setMessageIfChartContainsEmptyData("chart-area5",window.chart5);
			}
			$('.spinner').hide();

		},
		fnEmptyCanvas:function(message){
			$(studentsPerformanceAcrossPhase.canvasSpan).text(" ");
			selfChart5.fnEnableDisableSubmitBtn(true);
			$(studentsPerformanceAcrossPhase.canvasSpan).show();
			$(studentsPerformanceAcrossPhase.canvasSpan).text(message)

		},
		fnStudentsPerformanceAcrossPhaseBarChartInIt:function(){
			selfChart5.fnBindGradeData(true);
			selfChart5.fnBindSubjectData(true);
			selfChart5.fnBindSectorData(true);
			selfChart5.fnUpdateBarChart();
			
			selfChart5.fnSetOptionsForSector($(studentsPerformanceAcrossPhase.sector));
			selfChart5.fnSetOptionsForGrades($(studentsPerformanceAcrossPhase.grade));
			selfChart5.fnSetOptionsFoSubject($(studentsPerformanceAcrossPhase.subject));

		},
		fnShowError:function(currentElement){
			var elements =[reportDashBoard.year,reportDashBoard.state,studentsPerformanceAcrossPhase.sector,studentsPerformanceAcrossPhase.grade,studentsPerformanceAcrossPhase.subject];
			var message =[selectYearMessage,selectStateMessage,selectSectorMessage,selectGradeMessage, selectSubjectMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(!isErrorShwon && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentsPerformanceAcrossPhase.canvasSpan).text(message[i]);
						selfChart5.fnEnableDisableSubmitBtn(true);
						$(studentsPerformanceAcrossPhase.canvasSpan).show();
						$.each(elements,function(j,data){
							if(j>i){
								$(data+'Div').hide();
							}else{
								$(data+'Div').show();
							}
						});
						isErrorShwon =true;
						return false;
					}else{
						$.each(elements,function(j,data){
							$(data+'Div').show();
						});
					}
				}
			});

		},
		fnPopoverInit:function(){
			updatePopOver('#popoverStudentsPerformanceAcrossPhases');
		},
		fnSubmitHandler:function(){
			$('.spinner').show();
			$.wait(5).then(selfChart5.fnHidePopOver).then(selfChart5.fnUpdateBarChart)
			$('.spinner').hide();
			selfChart5.fnHidePopOver();
		},
		fnHidePopOver:function(){
			$('#popoverStudentsPerformanceAcrossPhases').closest("#canvas-holder").find(".chart-filter-container").slideUp(0);
		},
		fnEnableDisableSubmitBtn:function(isDisabled){
			$(studentsPerformanceAcrossPhase.submit).prop('disabled',isDisabled)
		},




}
///////////////////////// end Students Performance Across Phase  ////////////////////////////////////////////



var chartInIt=function(){
	///total school ///
	totalSchoolsChart.fnTotalSchoolInIt();
	studentAttendenceTrendLineChart.fnTotalProjectsMonthlySectorWiseInIt();
	totalStudentSectorWiseDoughnutChart.fnTotalStudentsSectorWiseInIt();
	studentsPerformanceAcrossStateBarChart.studentsPerformanceAcrossStateBarChartInIt();
	studentsPerformanceAccrossProjectBarChart.studentsPerformanceAccrossProjectInIt();
	studentsPerformanceAcrossPhaseBarChart.fnStudentsPerformanceAcrossPhaseBarChartInIt();
	
}






////////////////end Total Schools Sector Wise/////////////////



////////////////Start Students Performance Across State /////////////////



var showSpinner=function(){
	$('.spinner').show();
}
////////////////End Students Performance Across state /////////////////





$(document) .ready( function() {
	
	
	popoverElements=['#popoverTotalSchools','#popoverStudentAttendenceTrend','#popoverTotalStudentsSectorWise','#popoverStudentsPerformanceAcrossStates','#popoverStudentsPerformanceAcrossPhases','#popoverStudentsPerformanceAccrossProject'];
	
	fnGetStateId();
	studentsPerformanceAcrossStateBarChart.init();
	studentAttendenceTrendLineChart.init();
	totalStudentSectorWiseDoughnutChart.init();
	studentsPerformanceAcrossPhaseBarChart.init();
	studentsPerformanceAccrossProjectBarChart.init();
	
	 grades =customAjaxCalling("grade/dashboard/grades",null,'POST');
	setOptionForYear(reportDashBoard.year);
	setOptionForState(reportDashBoard.state);

	chartInIt();
	// popoverinit
	totalSchoolsChart.fnPopoverInit();
	studentAttendenceTrendLineChart.fnPopoverInit();
	totalStudentSectorWiseDoughnutChart.fnPopoverInit();
	studentsPerformanceAcrossStateBarChart.fnPopoverInit();
	studentsPerformanceAccrossProjectBarChart.fnPopoverInit();
	studentsPerformanceAcrossPhaseBarChart.fnPopoverInit();
	
	$('.panel.legend-panel').on('shown.bs.collapse', function () {
		if(!$(this).isOnScreen()){
		    var scrollVal = $(window).scrollTop();
		    $("html,body").animate({
	            scrollTop: scrollVal + 150
	        }, 350);
		}
	})
	
});

$.fn.isOnScreen = function () {
    if (this.length <= 0)
        return true;
    var win = $(window);
    var viewport = {
        top: win.scrollTop(),
        left: win.scrollLeft()
    };
    viewport.right = viewport.left + win.width();
    viewport.bottom = viewport.top + win.height();
    var bounds = this.offset();
    bounds.top = bounds.top + 50;
    bounds.right = bounds.left + this.outerWidth();
    bounds.bottom = bounds.top + this.outerHeight();
    return (!(viewport.right < bounds.left || viewport.left > bounds.right
        || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
};
