var chartYaxisName='Value';
//////////////////////// Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";
var selectSchoolMessage="Please select atleast one School. ";
var selectGradeMessage="Please select atleast one Grade. ";
var selectSubjectMessage="Please select atleast one Subject. ";


var studentPerformanceOnTaxonomyStageWiseAndGenderWise= new Object();

studentPerformanceOnTaxonomyStageWiseAndGenderWise.chartTypeDiv="#chartType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWiseDiv";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.chartType="#chartType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWise";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.dataTypeDiv="#dataType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWiseDiv";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.dataType="#dataType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWise";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasDiv="#canvasStudentPerformanceOnTaxonomyStageWiseAndGenderWiseDiv";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvas="#chart-studentPerformanceOnTaxonomyStageWiseAndGenderWise";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan="#spanStudentPerformanceOnTaxonomyStageWiseAndGenderWise";

studentPerformanceOnTaxonomyStageWiseAndGenderWise.thead="#studentPerformanceOnTaxonomyStageWiseAndGenderWiseTable thead"
studentPerformanceOnTaxonomyStageWiseAndGenderWise.tbody="#studentPerformanceOnTaxonomyStageWiseAndGenderWiseTable tbody"
studentPerformanceOnTaxonomyStageWiseAndGenderWise.schoolDiv ="#divSelectSchool";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.gradeDiv ="#divSelectGrade";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.subjectDiv ="#divSelectSubject";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.school ="#selectBox_schoolsByLoc";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.grade ="#selectBox_gradesBySchool";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.subject ="#selectBox_subject";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.startDate="#startDatePicker";
studentPerformanceOnTaxonomyStageWiseAndGenderWise.endDate="#endDatePicker";


/////////////////////////////////

////////////////start Total Projects Monthly Sector Wise //////////////////
var studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart={

		fnBindChartTypeData:function(){
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.chartTypeDiv).show();
			destroyRefresh(studentPerformanceOnTaxonomyStageWiseAndGenderWise.chartType);
			studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnSetOptionsForChartType($(studentPerformanceOnTaxonomyStageWiseAndGenderWise.chartType));
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.chartTypeDiv).show();
		},
		fnSetOptionsForChartType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnBindDataTypeData(true);
						studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
						studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnBindDataTypeData:function(){
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.sectorDiv).show();
			destroyRefresh(studentPerformanceOnTaxonomyStageWiseAndGenderWise.dataType);
			studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnSetOptionsForDataType($(studentPerformanceOnTaxonomyStageWiseAndGenderWise.dataType));
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.dataTypeDiv).show();
		},
		fnSetOptionsForDataType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						chartYaxisName =$(arg).find('option:selected').val();
						studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
						studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnUpdateBarChart:function(){
			var schoolIds=[];
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.school).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						schoolIds.push($(option).val());
					}
				});
			});
			
			var selectedGradeId=[];
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selectedGradeId.push($(option).val());
					}
				});
			});
			var selectedSubjectId=[];
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.subject).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selectedSubjectId.push($(option).val());
					}
				});
			});

			var dataTypeId=$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.dataType).find("option:selected").data('id');
			var chartTypeId=$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.chartType).find("option:selected").data('id');
			var startDate=$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.startDate).datepicker('getFormattedDate');
			var endDate=$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.endDate).datepicker('getFormattedDate');
			var fromDate=moment(startDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
			var toDate=moment(endDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
			var jsonData={
					"schoolIds":schoolIds,
					"fromDate":fromDate,
					"toDate":toDate,
					"gradeIds":selectedGradeId,
					"subjectIds":selectedSubjectId,
					"dataTypeId":dataTypeId,
					"chartTypeId":chartTypeId
			}
			if(schoolIds.length>0 && selectedGradeId.length>0 && selectedSubjectId.length>0){
				var barChartData=null;
				if(dataTypeId==1){
					 barChartData=customAjaxCalling("reportDashBoard/getBarChartDataForStudentPerformanceOnTaxonomyStageWiseAndGenderWise", jsonData, "POST");
				}else{
					 barChartData=customAjaxCalling("reportDashBoard/getBarChartDataForStudentPercentagePerformanceOnTaxonomyStageWiseAndGenderWise", jsonData, "POST");
				}
				
				var result=barChartData.datasets!=null?barChartData.datasets.length>0?true:false:false;
				if(result==true){
					$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).text(" ");
					$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).hide();
					var studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChartData = {
							labels: barChartData.labels,
							datasets: barChartData.datasets

					};
					studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnGenrateTable(barChartData.labels,barChartData.datasets);
					
					var studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChartDataConfig = {
							type: 'horizontalBar',
							data: studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChartData,
							options: {
								
								 scales: {
					                    xAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
					                            ticks: {
					                                beginAtZero: true,
					                                steps: 10,
					                                stepValue: 5,
					                            },
					                            scaleLabel: {
					                                display: true,
					                                labelString: chartYaxisName
					                            }
					                        }],
					                    yAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
					                            scaleLabel: {
					                                display: true,
					                                labelString: 'Taxonomy'
					                            }
					                        }]
					                },
								
								
								
                               elements: {
									rectangle: {
										borderWidth: 0.5,
										borderColor: randomColor(),
										borderSkipped: 'bottom'
									}
								},
								responsive: true,
								legend: {
									position: 'bottom',
								},

							}


					}

					$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvas).remove();
					$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasDiv).append('<canvas id="chart-studentPerformanceOnTaxonomyStageWiseAndGenderWise" />');
					var canvas = document.getElementById("chart-studentPerformanceOnTaxonomyStageWiseAndGenderWise");
					var context = canvas.getContext('2d');
					context.clear();
					window.myBar = new Chart(context, studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChartDataConfig);
					window.myBar.update();
				}
				else{
					$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).show();
					studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnEmptyCanvas(emptyChartMessage);
				}
			}

		},
		fnEmptyCanvas:function(message){
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).text(" ");
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvas).remove();
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).show();
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).text(message)
		},
		fnGenrateTable:function(labels,dataSet){
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.thead).children("tr").remove();
			$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.tbody).children("tr").remove();
			var thead='<tr style="display: table-row;">';
			var tData="";
			$.each(labels,function(index,thData){
				var count=index+1;
				tData=tData+'<th id="h'+count+'" class="nosort col-xs-2">'+thData+'</th>';
			});
            var finalThead=thead+'<th class="nosort col-xs-2" ></th>'+tData+"</tr>";
            var tbody="";
            $.each(dataSet,function(indexData,data){
            	tbody=tbody+'<tr><td class="text-center-bold">'+data.label+'</td>';
            	$.each(data.data,function(i,edata){
            		tbody=tbody+'<td class="textCenter">'+edata+'</td>';
            	});
            	tbody=tbody+("</tr>")
            });
           var finalTbody="<tr>"+tbody+"</tr>";
            $(studentPerformanceOnTaxonomyStageWiseAndGenderWise.thead).append(finalThead);
            $(studentPerformanceOnTaxonomyStageWiseAndGenderWise.tbody).append(tbody);
			
		},
		fnShowError:function(currentElement){
			//$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).text(" ");
			var elements =[studentPerformanceOnTaxonomyStageWiseAndGenderWise.school,studentPerformanceOnTaxonomyStageWiseAndGenderWise.grade,studentPerformanceOnTaxonomyStageWiseAndGenderWise.subject];
			var message =[selectSchoolMessage,selectGradeMessage, selectSubjectMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(isErrorShwon==false && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).text(message[i]);
						$(studentPerformanceOnTaxonomyStageWiseAndGenderWise.canvasSpan).show();
						isErrorShwon =true;
						return false;
					}
				}
			});

		}
}

////////////////////// 

var taxonomyStageWiseAndGenderWiseInIt=function(){
	studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnBindChartTypeData(true);
	studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnBindDataTypeData(true);
	studentPerformanceOnTaxonomyStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
}
/*
$(document) .ready(
		function() {
			taxonomyStageWiseAndGenderWiseInIt();
		});*/