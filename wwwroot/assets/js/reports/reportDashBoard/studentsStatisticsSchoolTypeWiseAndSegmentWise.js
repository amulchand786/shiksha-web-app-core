var chartYaxisName='Value';
//////////////////////// Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";
var selectSchoolMessage="Please select atleast one School. ";
var selectGradeMessage="Please select atleast one Grade. ";



var studentsStatisticsSchoolTypeWiseAndSegmentWise= new Object();

studentsStatisticsSchoolTypeWiseAndSegmentWise.chartTypeDiv="#chartType_dropBoxSStudentsStatisticsSchoolTypeWiseAndSegmentWiseDiv";
studentsStatisticsSchoolTypeWiseAndSegmentWise.chartType="#chartType_dropBoxStudentsStatisticsSchoolTypeWiseAndSegmentWise";
studentsStatisticsSchoolTypeWiseAndSegmentWise.dataTypeDiv="#dataType_dropBoxStudentsStatisticsSchoolTypeWiseAndSegmentWiseDiv";
studentsStatisticsSchoolTypeWiseAndSegmentWise.dataType="#dataType_dropBoxStudentsStatisticsSchoolTypeWiseAndSegmentWise";
studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasDiv="#canvasStudentsStatisticsSchoolTypeWiseAndSegmentWiseDiv";
studentsStatisticsSchoolTypeWiseAndSegmentWise.canvas="#chart-studentsStatisticsSchoolTypeWiseAndSegmentWise";
studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan="#spanStudentStatisticsSchoolTypeWiseAndSegmentWise";
studentsStatisticsSchoolTypeWiseAndSegmentWise.thead="#studentStatisticsSchoolTypeWiseAndSegmentWiseTable thead";
studentsStatisticsSchoolTypeWiseAndSegmentWise.tbody="#studentStatisticsSchoolTypeWiseAndSegmentWiseTable tbody"

studentsStatisticsSchoolTypeWiseAndSegmentWise.schoolDiv ="#divSelectSchool";
studentsStatisticsSchoolTypeWiseAndSegmentWise.gradeDiv ="#divSelectGrade";
studentsStatisticsSchoolTypeWiseAndSegmentWise.subjectDiv ="#divSelectSubject";
studentsStatisticsSchoolTypeWiseAndSegmentWise.school ="#selectBox_schoolsByLoc";
studentsStatisticsSchoolTypeWiseAndSegmentWise.grade ="#selectBox_gradesBySchool";
studentsStatisticsSchoolTypeWiseAndSegmentWise.subject ="#selectBox_subject";
studentsStatisticsSchoolTypeWiseAndSegmentWise.startDate="#startDatePicker";
studentsStatisticsSchoolTypeWiseAndSegmentWise.endDate="#endDatePicker";
/////////////////////////////////


////////////////start Total Projects Monthly Sector Wise //////////////////
var studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart={
		
		fnBindChartTypeData:function(){
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.chartTypeDiv).show();
			destroyRefresh(studentsStatisticsSchoolTypeWiseAndSegmentWise.chartType);
			studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnSetOptionsForChartType(studentsStatisticsSchoolTypeWiseAndSegmentWise.chartType);
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.chartTypeDiv).show();
		},
		fnSetOptionsForChartType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnBindDataTypeData(true);
						studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnUpdateBarChart();
						studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnBindDataTypeData:function(){
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.sectorDiv).show();
			destroyRefresh(studentsStatisticsSchoolTypeWiseAndSegmentWise.dataType);
			studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnSetOptionsForDataType($(studentsStatisticsSchoolTypeWiseAndSegmentWise.dataType));
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.dataTypeDiv).show();
		},
		fnSetOptionsForDataType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						chartYaxisName =$(arg).find('option:selected').val();
						studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnUpdateBarChart();
						studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnUpdateBarChart:function(){
			var schoolIds=[];
			$(studentPerformanceStageWiseAndGenderWise.school).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						schoolIds.push($(option).val());
					}
				});
			});
			
			var selectedGradeId=[];
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selectedGradeId.push($(option).val());
					}
				});
			});
			
			var dataTypeId=$(studentsStatisticsSchoolTypeWiseAndSegmentWise.dataType).find("option:selected").data('id');
			var chartTypeId=$(studentsStatisticsSchoolTypeWiseAndSegmentWise.chartType).find("option:selected").data('id');
			
			var jsonData={
					"schoolIds":schoolIds,
					"gradeIds":selectedGradeId,
					"dataTypeId":dataTypeId,
					"chartTypeId":chartTypeId
			}
			
			if(selectedGradeId.length>0 && schoolIds.length>0){
				var barChartData=null;
				if(dataTypeId==1){
					 barChartData=customAjaxCalling("reportDashBoard/getBarChartDataForStudentStatisticsSchoolTypeWiseAndSegmentWise", jsonData, "POST");
				}else{
					 barChartData=customAjaxCalling("reportDashBoard/getBarChartDataForStudentPercentageStatisticsSchoolTypeWiseAndSegmentWise", jsonData, "POST");
				}
				
				var result=barChartData.datasets!=null?barChartData.datasets.length>0?true:false:false;
				if(result==true){
					$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(" ");
					$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).hide();
					var studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChartData = {
							labels: barChartData.labels,
							datasets: barChartData.datasets

					};
					studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnGenrateTable(barChartData.labels,barChartData.datasets);
					var studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChartDataConfig = {
							type: 'bar',
							data: studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChartData,
							options: {
									
								 scales: {
					                    xAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
					                        }],
					                    yAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
									            ticks: {
					                                beginAtZero: true,
					                                steps: 10,
					                                stepValue: 5,
					                            },
					                            scaleLabel: {
					                                display: true,
					                                labelString: chartYaxisName
					                            }
					                        }]
					                },
					               onAnimationComplete: function() {
					                    this.showTooltip(this.datasets[0].bars, true);
					                },
								
								
								elements: {
									rectangle: {
										borderWidth: 0.5,
										borderColor: randomColor(),
										borderSkipped: 'bottom'
									}
								},
								responsive: true,
								legend: {
									position: 'bottom',
								},

							}


					}

					$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvas).remove();
					$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasDiv).append('<canvas id="chart-studentsStatisticsSchoolTypeWiseAndSegmentWise" />');
					var canvas = document.getElementById("chart-studentsStatisticsSchoolTypeWiseAndSegmentWise");
					var context = canvas.getContext('2d');
					context.clear();
					window.myBar = new Chart(context, studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChartDataConfig);
					window.myBar.update();
				}
				else{
					$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).show();
					studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnEmptyCanvas(emptyChartMessage);
				}
			}

		},
		fnEmptyCanvas:function(message){
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(" ");
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvas).remove();
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).show();
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(message)
		},
		fnGenrateTable:function(labels,dataSet){
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.thead).children("tr").remove();
			$(studentsStatisticsSchoolTypeWiseAndSegmentWise.tbody).children("tr").remove();
			var thead='<tr style="display: table-row;">';
			var tData="";
			$.each(labels,function(index,thData){
				var count=index+1;
				tData=tData+'<th id="h'+count+'" class="nosort col-xs-2">'+thData+'</th>';
			});
            var finalThead=thead+'<th class="nosort col-xs-2" ></th>'+tData+"</tr>";
            var tbody="";
            $.each(dataSet,function(indexData,data){
            	tbody=tbody+'<tr><td class="text-center-bold">'+data.label+'</td>';
            	$.each(data.data,function(i,edata){
            		tbody=tbody+'<td class="textCenter">'+edata+'</td>';
            	});
            	tbody=tbody+("</tr>")
            });
           var finalTbody="<tr>"+tbody+"</tr>";
            $(studentsStatisticsSchoolTypeWiseAndSegmentWise.thead).append(finalThead);
            $(studentsStatisticsSchoolTypeWiseAndSegmentWise.tbody).append(tbody);
			
		},
		fnShowError:function(currentElement){
			//$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(" ");
			var elements =[studentsStatisticsSchoolTypeWiseAndSegmentWise.school,studentsStatisticsSchoolTypeWiseAndSegmentWise.grade];
			var message =[selectSchoolMessage,selectGradeMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(isErrorShwon==false && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(message[i]);
						$(studentsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).show();
						isErrorShwon =true;
						return false;
					}
				}
			});

		}
}

////////////////////// 

var studentsTypeWiseAndSegmentWiseInIt=function(){
	studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnBindChartTypeData(true);
	studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnBindDataTypeData(true);
	studentsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnUpdateBarChart();
}
/*
$(document) .ready(
		function() {
			studentsTypeWiseAndSegmentWiseInIt();
		});*/