
$("#schoolDashboardBackButton").on('click',function(){
	location.replace('report/schoolDashboard/');
});


var fnShowStudentByIds=function(studentIds){
	var jsonData={
			"studentIds":studentIds
			
	}
var requestData=	customAjaxCalling("student/studentlistByIdsSortByName",jsonData,"POST");
	
	fnCreateStudentRow(requestData);
	
	
	/*$('#mdlStudentDetailView').on('shown.bs.modal', function (e) {
	    $(this).find(".modal-body").scrollTop(0);
	})

	$('#mdlStudentDetailView').modal('show').trigger('shown');*/
	
	$('#mdlStudentDetailView').modal('show');
	//$('#mdlStudentDetailView').find(".modal-body").scrollTop(0);
}
var getSectionList = function(){	
	
	$('#selectBox_section').find("option:selected").prop('selected', false);
	$('#selectBox_section').multiselect('refresh').multiselect('destroy');
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').val());
	bindToElementId = $('#selectBox_section');
	$('.outer-loader').show();
	bindSectionBySchoolAndGradeToListBox(bindToElementId,schoolId,gradeId,0);
	setOptionsForMultipleSelect('#selectBox_section');
	$('.outer-loader').hide();
	$('#divSelectSection').show();
	
	//fnCollapseMultiselect();
}

var bindSectionBySchoolAndGradeToListBox = function(bindToElementId,selectedSchoolId,selectedGradeId,selectedId)
{	
	$('#studentSearchForm').data('formValidation') .updateStatus('sectionName', 'NOT_VALIDATED');

	json ={
			"schoolId": selectedSchoolId,
			"gradeId": selectedGradeId
	}
	var sections = customAjaxCalling("sectionlist", json,"POST");
	if(sections!=null || sections!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(sections,function(index,obj){
			$select.append('<option data-id="' + obj.sectionId + '"data-sectionname="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
		});
	}

}

/////////////////////////////// show selected student detail ////////////////////
var fnCreateStudentRow=function(studentData){
	deleteAllRow('tableStudentDetailView');
	var table = $('#tableStudentDetailView').dataTable();
	table.fnFilterClear();
	//table.fnClearTable();
	table.fnDestroy();
	$.each(studentData, function(index,student) {
		var row='';
		row='<tr>'+
		'<td></td>'+
		'<td title="'+student.name+'" >'+student.name+'</td>'+
		'<td title="'+student.fatherName+'" >'+student.fatherName+'</td>'+
		'<td title="'+student.motherName+'" >'+student.motherName+'</td></tr>';
		
		appendNewRow('tableStudentDetailView', row);
		
	});
	
	fnDTWithInitialSort('#tableStudentDetailView',1,'asc');
}

/////////////////// Display Modal to search student /////////////////////
$('input:radio[name="chapterwise"]').change(function(){
	var xTableLength=$("#schoolwiseAssementDetailTable").find('tbody>tr').length;
	if(xTableLength>0){
		if($(this).val() == 'Yes'){
			resetFormById('studentSearchForm');
			$('#mdlChapterWiseView').modal();
			$('#mdlChapterWiseView #selectBox_grade').multiselect('destroy');
			$('#mdlChapterWiseView #selectBox_section').multiselect('destroy');
			$('#mdlChapterWiseView #selectBox_questionPaper').multiselect('destroy');
			$("#selectBox_grade option:selected").removeAttr("selected");
			$("#selectBox_section option:selected").removeAttr("selected");
			$("#selectBox_questionPaper option:selected").removeAttr("selected");
			setOptionsForMultipleSelect('#selectBox_grade');
			setOptionsForMultipleSelect('#selectBox_section');
			setOptionsForMultipleSelect('#selectBox_questionPaper');


		}else{
			$('#divAssessement').css('display','none');
		}
	}else{
		$("#msgText").text("There are no result for above search criteria. So chapter wise result will not avalible")
		$('#mdlError').modal();
		fnResetChapterWiseRadioButton();
	}
});

///////////////////////// enable disable Yes/No chapter wise report button
var fnResetChapterWiseRadioButton=function(){
	resetFormById('studentSearchForm');
	$('#tableDivForSearchStudentDetail').hide();
	deleteAllRow('tableSearchStudentDetail');
	$('input[name="chapterwise"]').prop('checked', false);
	var chapterwiseBtnValue="No";
	$("input[name=chapterwise][value=" + chapterwiseBtnValue + "]").prop('checked', true);
}
//////////////////////// switch to seearch by name and father name //////////////////////////
var fnUncheckORBtn=function(){
$('#studentId').val("");
$('#studentSearchForm').data('formValidation').updateStatus('studentId', 'VALID');
$('#studentSearchForm').formValidation('revalidateField', 'studentName')
.formValidation('revalidateField', 'fatherName');
};
////////////////////////// Switch to search by student Id ///////////////////////
var fnCheckORBtn=function(){
	$('#studentName').val("");
	$('#fatherName').val("");
	$('#studentSearchForm').data('formValidation').updateStatus('studentName', 'VALID');
	$('#studentSearchForm').data('formValidation').updateStatus('fatherName', 'VALID');
	$('#studentSearchForm').formValidation('revalidateField', 'studentId');
	

};
/////////////////////////// Search student by name and father name or student id//////////////////////////
var fnGetStudentData=function(){
	var studentName=$('#studentName').val();
	var fatherName=$('#fatherName').val();
	var rollNumber=$('#studentId').val();
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').val());
	var sectionId = parseInt($("#selectBox_section").find('option:selected').data('id'));
	var jsonData={
			"rollNumber":rollNumber,
			"fatherName":fatherName,
			"name":studentName,
			"schoolId":schoolId,
			"gradeId":gradeId,
			"sectionId":sectionId,
			"projectId":campaignId,
	}
	var studentDetailData=	customAjaxCalling("student/searchStudentByDetails", jsonData, "POST");
	var validData=studentDetailData!=null?studentDetailData.length>0?true:false:false;
	if(validData==true){
		var student=studentDetailData[0];
		$("#roolNumberMsg").hide();
		fnGetStudentLevelReport(student.studentAcademicYearSchoolGradeSectionMapperId,schoolId,student.studentId);
	}else{
		$("#roolNumberMsg").show();
		$("#roolNumberMsg").fadeOut(2500);
	}
	
	
	
	//fnGenrateStudentRow(studentDetailData);

};
/////////////////////////// Create search student table ///////////////////////////////
var fnGenrateStudentRow=function(studentDetailData){
	$('#tableDivForSearchStudentDetail').show();
	deleteAllRow('tableSearchStudentDetail');
	if(studentDetailData!=null){
		
		$.each(studentDetailData, function(index,student) {
			var row='';
			row='<tr data-id="'+student.studentAcademicYearSchoolGradeSectionMapperId+'">'+
			'<td></td>'+
			'<td title="'+student.name+'" ><a style="cursor: pointer;" onclick="fnGetStudentLevelReport('+student.studentAcademicYearSchoolGradeSectionMapperId+','+schoolId+','+student.studentId+')">'+student.name+'</a></td>'+
			'<td title="'+student.rollNumber+'" >'+student.rollNumber+'</td>'+
			'<td title="'+student.fatherName+'" >'+student.fatherName+'</td>'+
			'<td title="'+student.motherName+'" >'+student.motherName+'</td>'+
			'<td title="'+student.gender+'" >'+student.gender+'</td>'+
			'<td title="'+student.gradeName+'" >'+student.gradeName+'</td>'+
			'<td title="'+student.sectionName+'" >'+student.sectionName+'</td></tr>';
       appendNewRow('tableSearchStudentDetail', row);

		});
	}
}
//////////////////////////////// Get student level report /////////////////////////////////////

var fnGetStudentLevelReport=function(studentAcademicYearSchoolGradeSectionMapperId,schoolId,studentId){
	var assessmentId=$('#selectBox_questionPaper').find('option:selected').val();

	var studentData=customAjaxCalling("student/getStudentDataByStudentAcademicYearSchoolGradeSectionMapper/"+studentAcademicYearSchoolGradeSectionMapperId+"/"+studentId, null, "POST");
	if(studentData!=null){
		$('#resultStudentId').text(studentData.rollNumber);
		$('#resultStudentName').text(studentData.name);
		$('#resultFatherName').text(studentData.fatherName);
		$('#resultGrade').text(studentData.gradeName);
		
		var jsonData={
				"schoolId":schoolId,
				"studentAcademicYearSchoolGradeSectionMapperId":studentAcademicYearSchoolGradeSectionMapperId,
				"assessmentId":assessmentId,
				"studentId":studentData.studentId,
				"campaignId":campaignId
		}
		
		
		var studentDetailData=customAjaxCalling("report/studentLevelReportData", jsonData, "POST");

		if(studentDetailData!=null){
			createStudentlevelReport(studentDetailData);
		}
		$('#mdlStudentLevelReportDetailView').modal();
		$('#chapterwiseResult input:radio[name=chapterwises][value="scoreCard"]').removeAttr('checked');
		$('#chapterwiseResult input:radio[name=chapterwises][value="bloomTaxonomyWise"]').removeAttr('checked');
		$('#chapterwiseResult input:radio[name=chapterwises][value="Yes"]').prop('checked', true);
		fnClearCustomSearch(['.r5','.r4','.r3']);
		$("#search5").hide();
		$("#search4").hide();
		$("#search3").show();
		

	}

}


/////////////////////////////////////////////////////// Student level report ////////////////////


var createStudentlevelReport=function(studentlevelReportData){
	deleteAllRow('studentLevelReportTable');
	$.each(studentlevelReportData, function(index,studentReportData) {	
		appendNewRow('studentLevelReportTable',getNewRow(studentReportData));
		//CR-653 implementation
		/*if(studentReportData.isAppearedLate==true){
			$("#studentLevelReportTable #tr"+studentReportData.consolidatedStatusId).addClass('appread-late-student');
			console.log(studentReportData.consolidatedStatusId);
		}*/
	});
	$("#displayResult").show();
	$("#rowDiv2").show();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();
	$('#studentLevelReportTable').show();
	$('#reportfilter').removeClass('in');
	
}

var getNewRow =function(studentReportData){
	var totalMarks=studentReportData.totalMarksScored==null?"":studentReportData.totalMarksScored+"/"+studentReportData.totalMarks;
	var percentageScore=studentReportData.percentageScore==-1?"":studentReportData.percentageScore+"%";
	var toUnderGoPa = (studentReportData.toUnderGoPA==null)?"" 
														: (studentReportData.toUnderGoPA==true)?studentReportData.toUnderGoPA="YES" 
															: studentReportData.toUnderGoPA="NO";
	var knowledgeScore=studentReportData.parameterScoringForAssessmentVm.scored_K==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_K;
	var understandingScore=studentReportData.parameterScoringForAssessmentVm.scored_U==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_U;
	var operationScore=studentReportData.parameterScoringForAssessmentVm.scored_O==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_O;
	var analysisScore=studentReportData.parameterScoringForAssessmentVm.scored_An==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_An;
	var applicationScore=studentReportData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_Ap;
	
var	row='<tr id="tr'+studentReportData.consolidatedStatusId+'_'+studentReportData.isAppearedLate+'">'+
	"<td title='"+studentReportData.assessmentName+"' >"+studentReportData.assessmentName+"</td>"+
	"<td title='"+studentReportData.statusOfCompletion+"' >"+studentReportData.statusOfCompletion+"</td>"+
	"<td title='"+studentReportData.stageName+"' >"+studentReportData.stageName+"</td>"+
	"<td title='"+studentReportData.subjectName+"' >"+studentReportData.subjectName+"</td>"+
	"<td title='"+totalMarks+"' >"+totalMarks+"</td>"+
	"<td title='"+percentageScore+"' >"+percentageScore+"</td>"+
	"<td title='"+toUnderGoPa+"' >"+toUnderGoPa+"</td>"+
	"<td title='"+knowledgeScore+"' >"+knowledgeScore+"</td>"+
	"<td title='"+understandingScore+"' >"+understandingScore+"</td>"+
	"<td title='"+operationScore+"' >"+operationScore+"</td>"+
	"<td title='"+analysisScore+"' >"+analysisScore+"</td>"+
	"<td title='"+applicationScore+"' >"+applicationScore+"</td>"+
	"</tr>";
	return row;
	};
	

	$('#chapterwiseResult input:radio[name=chapterwises]').on('click', function() {
		$(".outer-loader").show();
		$.wait(200).then(fnShowChapterWiseStudentResult); 

});
	
var fnShowChapterWiseStudentResult=function(){
	var elem=$('#chapterwiseResult input:radio[name=chapterwises]:checked').val();
	fnClearCustomSearch(['.r5','.r4','.r3']);
	if(elem == 'Yes'){
		$("#rowDiv3").hide();
		$("#rowDiv2").show();
		$("#rowDiv4").hide();
		$('#studentLevelReportTable').show();
	} else if(elem== 'scoreCard'){
		$("#search5").hide();
		$("#search3").hide();
		$("#search4").show();
		genrateScoreCardRow();
		$(".outer-loader").hide();
	}else{
		$("#search5").show();
		$("#search3").hide();
		$("#search4").hide();
		genrateBloomTaxonomyRow();
		$(".outer-loader").hide();
	}
	$(".outer-loader").hide();
}	
	
	////////////////////////// score card////////////////////////
	


	var genrateScoreCardRow=function(){
		$('#scoreBoardTableDiv').css('display','block');
	
		var studentlevelScoreCardData = customAjaxCalling("report/studentlevelScoreCard",null,"POST");
		
		$("#scoreBoardHeader").children("tr").remove();
		$("#scoreBoardHeader").append(createTableForScoreCard(studentlevelScoreCardData.maxNoOfRows));
		createStudentlevelScoreCardReport(studentlevelScoreCardData.studentLevelReportVm);
		
		
	}

	var createTableForScoreCard=function(maxNumberOfColumns){
		var noOfColmns=maxNumberOfColumns+6;
		var rowSpan='';
			for(var i=1;i<=maxNumberOfColumns;i++){
				rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
			}
		
		var	thead="<tr>"+
		"<th  class='nosort' rowspan='3'><p>Assessments Name</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
		"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
		"</tr>"+
		"<tr>"+
		"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise scores</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Marks Scored</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Total Marks</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Capterwise % score</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Total marks scored</p></th>"+
		"<th  class='nosort' rowspan='2'><p>% score</p></th>"+
		"</tr>"+
		"<tr>"+
		rowSpan
		"</tr>";
		return thead;
	};


	var createStudentlevelScoreCardReport=function(studentlevelScoreCardData){
		$("#scoreBoardTable tbody tr").remove();
		$.each(studentlevelScoreCardData, function(index,studentScoreCardData) {	
			$("#scoreBoardTable tbody").append(getScoreCardNewRow(studentScoreCardData));
			if(studentScoreCardData.isAppearedLate==true){
				$("#scoreBoardTable #tr"+studentScoreCardData.consolidatedStatusId).addClass('appread-late-student');
				$("#scoreBoardTable #trchap"+studentScoreCardData.consolidatedStatusId).addClass('appread-late-student');
				
			}
		});
		$("#rowDiv2").hide();
		$("#rowDiv3").show();
		$("#rowDiv4").hide();
		$('#scoreBoardTable').show();
	}


	var getScoreCardNewRow =function(studentScoreCardData){
		var maxMarks=studentScoreCardData.totalMarks;
		var totalMarks=studentScoreCardData.totalMarksScored==null?"":studentScoreCardData.totalMarksScored+"/"+maxMarks;
		var percentageScore="";
		if(studentScoreCardData.totalMarksScored!=null){
			percentageScore=studentScoreCardData.percentageScore==-1?"":studentScoreCardData.percentageScore+"%";
		}
		var rowSpan='';
		var count=studentScoreCardData.numberOfChapters;
		$.each(studentScoreCardData.chapterReportVmList, function(index,chapterVm) {	
			chapterWisePercentage=chapterVm.chapterWisePercentage==-1?"":chapterVm.chapterWisePercentage+"%";
			
			if(index > 0){
				rowSpan+= '<tr id="trchap'+studentScoreCardData.consolidatedStatusId+'">';
				}
			rowSpan +="<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
			$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
				if(qScoreMap == -1){
					qScoreMap="";
				}
				 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
			});
			rowSpan+="<td title='"+chapterVm.chapterWiseScore+"' >"+chapterVm.chapterWiseScore+"</td>"+
			"<td title='"+chapterVm.chapterWiseTotalMarks+"' >"+chapterVm.chapterWiseTotalMarks+"</td>"+
			"<td title='"+chapterVm.chapterWisePercentage+"' >"+chapterWisePercentage+"</td>";
			
			if(index==0){
			rowSpan+= "<td title='"+totalMarks+"' rowspan="+count+">"+totalMarks+"</td>"+
			"<td title='"+percentageScore+"' rowspan="+count+">"+percentageScore+"</td></tr>";
				}else{
				rowSpan+="</tr>";
			}
			
		});
		var	row='<tr  id="tr'+studentScoreCardData.consolidatedStatusId+'">'+
		"<td title='"+studentScoreCardData.assessmentName+"' rowspan="+count+">"+studentScoreCardData.assessmentName+"</td>"+
		"<td title='"+studentScoreCardData.statusOfCompletion+"' rowspan="+count+">"+studentScoreCardData.statusOfCompletion+"</td>"+
		"<td title='"+studentScoreCardData.stageName+"' rowspan="+count+">"+studentScoreCardData.stageName+"</td>"+
		rowSpan;
		return row;
		};
		
	//////////////////////////////////////////Boolm taxonomy //////////////////////////////

		var genrateBloomTaxonomyRow=function(){
			$("#rowDiv3").hide();
			$('#rowDiv4').css('display','block');
			
			var studentlevelBloomTaxonomyData = customAjaxCalling("report/studentlevelbloomTaxonomy",null,"POST");
			//deleteAllRow('bloomTaxonomyTable');
			$("#bloomTaxonomyHeader").children("tr").remove();
			$("#bloomTaxonomyHeader").append(createTableForBloomTaxonomy(studentlevelBloomTaxonomyData.maxNoOfRows));
			createStudentlevelBloomTaxonomyReport(studentlevelBloomTaxonomyData.studentLevelReportVm);
				
				
			}

			
			
		var createTableForBloomTaxonomy=function(maxNumberOfColumns){
			var noOfColmns=maxNumberOfColumns+5;
			var rowSpan='';
				for(var i=1;i<=maxNumberOfColumns;i++){
					rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
				}
			
			var	thead="<tr>"+
			"<th  class='nosort' rowspan='3'><p>Assessments Name</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
			"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Bloom Taxonomy Wise</p></th>"+
			"</tr>"+
			"<tr>"+
			"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise Parameter Score</p></th>"+
			"<th  class='nosort' colspan='5'><p>Overall Parameter Score</p></th>"+
			
			"</tr>"+
			"<tr>"+
			rowSpan+
			"<th  class='nosort' ><p>K</p></th>"+
			"<th  class='nosort' ><p>U</p></th>"+
			"<th  class='nosort' ><p>O</p></th>"+
			"<th  class='nosort' ><p>AN</p></th>"+
			"<th  class='nosort' ><p>AP</p></th>"+
			"</tr>";
			return thead;
		};	


		var createStudentlevelBloomTaxonomyReport=function(studentlevelBloomTaxonomyData){
			$("#bloomTaxonomyTable tbody tr").remove();
			$.each(studentlevelBloomTaxonomyData, function(index,studentBloomTaxonomyData) {	
				$("#bloomTaxonomyTable tbody").append(getBloomtaxonomyNewRow(studentBloomTaxonomyData));
				if(studentBloomTaxonomyData.isAppearedLate==true){
					$("#bloomTaxonomyTable #tr"+studentBloomTaxonomyData.consolidatedStatusId).addClass('appread-late-student');
					$("#bloomTaxonomyTable #trchap"+studentBloomTaxonomyData.consolidatedStatusId).addClass('appread-late-student');
					
				}
			});
			$("#rowDiv2").hide();
			$("#rowDiv3").hide();
			$("#rowDiv4").show();
			$('#bloomTaxonomyTable').show();
		}

			

		var getBloomtaxonomyNewRow =function(studentBloomTaxonomyData){
			var knowledgeScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K;
			var understandingScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U;
			var operationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O;
			var analysisScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An;
			var applicationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap;
			var rowSpan='';
			var count=studentBloomTaxonomyData.numberOfChapters;
			
			rowSpan +="<td title='' ></td>";
			$.each(studentBloomTaxonomyData.taxonomyHeaderList, function(indexNumber,taxonomyHeader) {	
			
			 rowSpan +="<td title='"+taxonomyHeader+"' >"+taxonomyHeader+"</td>";
			});
			$.each(studentBloomTaxonomyData.taxonomyWeightAge, function(indexNumber,taxonomyWeightage) {	
				 rowSpan +="<td title='"+taxonomyWeightage+"' >"+taxonomyWeightage+"</td>";
				});
			rowSpan+="</tr>";
			$.each(studentBloomTaxonomyData.chapterReportVmList, function(index,chapterVm) {	
				rowSpan +="<tr id='trchap"+studentBloomTaxonomyData.consolidatedStatusId+"'>"+ "<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
				$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
					if(qScoreMap == -1){
						qScoreMap="";
					}
					if(qScoreMap == 'null'){
						qScoreMap=0;
					}
					 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
				});
				if(index==0){
				rowSpan+= "<td title='"+knowledgeScore+"' rowspan="+count+" >"+knowledgeScore+"</td>"+
				"<td title='"+understandingScore+"' rowspan="+count+">"+understandingScore+"</td>"+
				"<td title='"+operationScore+"' rowspan="+count+">"+operationScore+"</td>"+
				"<td title='"+analysisScore+"' rowspan="+count+">"+analysisScore+"</td>"+
				"<td title='"+applicationScore+"' rowspan="+count+">"+applicationScore+"</td>"+
				"</tr>";
					}else{
					rowSpan+="</tr>";
				}
				
			});
			var	row='<tr  id="tr'+studentBloomTaxonomyData.consolidatedStatusId+'">'+
			"<td title='"+studentBloomTaxonomyData.assessmentName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.assessmentName+"</td>"+
			"<td title='"+studentBloomTaxonomyData.statusOfCompletion+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.statusOfCompletion+"</td>"+
			"<td title='"+studentBloomTaxonomyData.stageName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.stageName+"</td>"+
			rowSpan;
			return row;
			};

			
var jAssessmentId=0;
var jCampaignId=0;
var jSchoolId=0;
var jassessmentPlannerId=0;
var jQname="";
		

/////////////////////// Get Assessment wise student Detail //////////////////////////
var getStudentDataByAssmt = function(assessmentPlannerId,assessmentId,campaignId,schoolId,qname){
	
	 jAssessmentId=assessmentId;
	 jCampaignId=campaignId;
	 jSchoolId=schoolId;
	 jassessmentPlannerId=assessmentPlannerId;
	 jQname=qname;
	 $(".outer-loader").css('display','block');
	 $.wait(30).then(fnGetStudentDataByAssmt);
	
	
}
var saveAssessmentId=0;
var saveAssessmentPlannerId=0;
var fnGetStudentDataByAssmt=function(){
	
    $("#questionPaperNameX").text(jQname);
	saveAssessmentId = jAssessmentId;
	saveAssessmentPlannerId= jassessmentPlannerId;
	var studentData = reportCustomAjaxCalling("report/assessmentWiseStudentData/"+jassessmentPlannerId+"/"+jAssessmentId+"/"+jCampaignId+"/"+jSchoolId,null,"POST");
	deleteAllRow('tableForStudent');
	if(studentData!=null){
		if(studentData.length>0){
		assessmentWiseStudentData(studentData,'tableForStudent');
		$(".outer-loader").css('display','none');
		}
		
		
	}else{
		$(".outer-loader").css('display','none');
	}
	$('input:radio[name="viewChapterwiseResult"][value="scoreCard"]').removeAttr('checked');
	$('input:radio[name="viewChapterwiseResult"][value="bloomTaxonomyWise"]').removeAttr('checked');
	$('input:radio[name="viewChapterwiseResult"][value="Yes"]').prop('checked', true);
	$("#rowDiv1").show();
	$("#chapterWiseRadioButtonsDiv").show();
	$("#assessmentHomeBloomResultDiv").hide();
	$("#assessmentHomeScorecardDiv").hide();
	fnColSpanIfDTEmpty('tableForStudent',20);
	$('#showStudentDetailPopup').modal();
	$(".outer-loader").css('display','none');

}



/////////////////////// Create  Assessment wise student Detail table //////////////////////////
var assessmentWiseStudentData=function(reportData,tableId){
	deleteAllRow(tableId);
	var studentDataVm=reportData[0];
	$("#tableForStudent #h15").text(studentDataVm.overAllScore);
	$("#tableForStudent #h16").text(studentDataVm.overAllPercentage);
	$("#tableForStudent #h23").text(studentDataVm.totalTaxnomyScore.Knowledge!=undefined?studentDataVm.totalTaxnomyScore.Knowledge:0);
	$("#tableForStudent #h24").text(studentDataVm.totalTaxnomyScore.Understanding!=undefined?studentDataVm.totalTaxnomyScore.Understanding:0);
	$("#tableForStudent #h25").text(studentDataVm.totalTaxnomyScore.Operation!=undefined?studentDataVm.totalTaxnomyScore.Operation:0);
	$("#tableForStudent #h26").text(studentDataVm.totalTaxnomyScore.Analysis!=undefined?studentDataVm.totalTaxnomyScore.Analysis:0);
	$("#tableForStudent #h27").text(studentDataVm.totalTaxnomyScore.Application!=undefined?studentDataVm.totalTaxnomyScore.Application:0);
	$.each(reportData, function(index,studentData) {

		var row='';
		row='<tr id="tr'+studentData.consolidatedStatusId+'_'+studentData.isAppearedLate+'">'+
		'<td title="'+studentData.rollNumber+'" >'+studentData.rollNumber+'</td>'+	
		'<td title="'+studentData.name+'" >'+studentData.name+'</td>'+
		'<td title="'+studentData.fatherName+'" >'+studentData.fatherName+'</td>'+
		'<td title="'+studentData.gender+'" >'+studentData.gender+'</td>'+
		'<td title="'+studentData.age+'" >'+studentData.age+'</td>'+
		'<td title="'+studentData.totalMarksScored+'" >'+studentData.totalMarksScored+'</td>'+
		'<td title="'+studentData.scoreInPercent+'" >'+studentData.scoreInPercent+'</td>'+
		'<td title="'+studentData.toUndergoPA+'" >'+studentData.toUndergoPA+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_K+'" >'+studentData.parameterScoring.scored_K+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_U+'" >'+studentData.parameterScoring.scored_U+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_O+'" >'+studentData.parameterScoring.scored_O+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_An+'" >'+studentData.parameterScoring.scored_An+'</td>'+
		'<td title="'+studentData.parameterScoring.scored_Ap+'" >'+studentData.parameterScoring.scored_Ap+'</td>'+
		'</tr>';
		appendNewRow(tableId, row);
		});
}


///////////////////////   Assessment wise Radio buttons //////////////////////////
$('#showStudentDetailPopup input:radio[name=viewChapterwiseResult]').on('click', function() {
	$(".outer-loader").show();
	$.wait(200).then(fnShowQuestionPaperWiseStudentResult); 

});

var fnShowQuestionPaperWiseStudentResult=function(){
	var elemValue=$('#showStudentDetailPopup input:radio[name=viewChapterwiseResult]:checked').val();
	
	fnClearCustomSearch(['.r5','.r4','.r3']);
	if(elemValue == 'Yes'){
	  $("#rowDiv1").show();
			$("#chapterWiseRadioButtonsDiv").show();
	    	$("#assessmentHomeBloomResultDiv").hide();
	    	$("#assessmentHomeScorecardDiv").hide();
	    	$("#search5").hide();
	    	$("#search4").hide();
	    	$("#search3").show();
	    	$(".outer-loader").hide();
	    } else if(elemValue == 'scoreCard'){
	    	getStudentScoreCardByAssmt(saveAssessmentPlannerId,saveAssessmentId,campaignId,schoolId);
	    	$("#rowDiv1").hide();
	    	$("#assessmentHomeBloomResultDiv").hide();
	    	$("#assessmentHomeScorecardDiv").show();
	    	
	    	$("#search5").hide();
	    	$("#search4").show();
	    	$("#search3").hide();
	    	$(".outer-loader").hide();
	    }else{
	    	getStudentBloomTaxonomyByAssmt(saveAssessmentPlannerId,saveAssessmentId,campaignId,schoolId);
	    	$("#rowDiv1").hide();
	    	$("#assessmentHomeBloomResultDiv").show();
	    	$("#assessmentHomeScorecardDiv").hide();
	    	$("#search5").show();
	    	$("#search4").hide();
	    	$("#search3").hide();
	    	$(".outer-loader").hide();
	    }
	$(".outer-loader").hide();
	};
/////////////////////// Create  Assessment wise scorecard Data //////////////////////////

var getStudentScoreCardByAssmt = function(lAssessmentPlannerId,lAssessmentId,lCampaignId,lSchoolId){
	
	$(".outer-loader").css('display','block');
	var studentData = reportCustomAjaxCalling("report/assessmentWiseScoreCard/"+lAssessmentPlannerId+"/"+lAssessmentId+"/"+lCampaignId+"/"+lSchoolId,null,"POST");
	var studentFooterData = reportCustomAjaxCalling("report/getAssessmentLevelScorecardDataFooter/",studentData,"POST");
	if(studentData!=null){
		$("#assessmentHomeScorecardHeader").children("tr").remove();
		$("#assessmentHomeScorecardFooter").children("tr").remove();
		if(studentData!=""){
		$("#assessmentHomeScorecardHeader").append(createTableForAssessmentWiseScoreCard(studentData[0].maxQuestionCount));
		$("#assessmentHomeScorecardFooter").append(createTableForAssessmentWiseScoreCardFooter(studentData,studentFooterData));
		
		
		}else{
			$("#assessmentHomeScorecardHeader").append(createTableForAssessmentWiseScoreCard(1));

		}
		assessmentWiseScoreCardData(studentData,'assessmentHomeScorecardTable');
		if(studentData==""){
			$("#assessmentHomeScorecardTable tbody").append("<tr>"+"<td colspan='32' style='text-align:center'>No data available in table</td>"+"</tr>");

		}
		$(".outer-loader").css('display','none');
		
		
	}else{
		$(".outer-loader").css('display','none');
	}
	
}

///////////////////////Create  Table For Assessment wise scorecard Data  //////////////////////////

var createTableForAssessmentWiseScoreCard=function(maxNumberOfColumns){
	
	var noOfColmns=maxNumberOfColumns+6;
	var rowSpan='';
		for(var i=1;i<=maxNumberOfColumns;i++){
			rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
		}
	
	var	thead="<tr>"+
	"<th  class='nosort' rowspan='3'><p>Roll No.</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Student Name</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Fathers's Name</p></th>"+
	
	"<th  class='nosort' rowspan='3'><p>Gender</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Age</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Chapter Name</p></th>"+
	"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
	"</tr>"+
	"<tr>"+
	"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise scores</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Marks Scored</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Total Marks</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Capterwise % score</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Total marks scored</p></th>"+
	"<th  class='nosort' rowspan='2'><p>% score</p></th>"+
	"</tr>"+
	"<tr>"+
	rowSpan
	"</tr>";
	return thead;
};

var createTableForAssessmentWiseScoreCardFooter=function(studentDataVM,studentFooterData){

	var totalStudent=studentDataVM.length;
	var chapters=studentDataVM[0].chapterReportVmList;
	var totalChapterCount=chapters.length+1;
	var maxNumberOfColumns=studentDataVM[0].maxQuestionCount
	var noOfColmns=maxNumberOfColumns+6;
	var totalMarksScored=0;
	var totalPercentageMarksScored=0;
	var rowSpan='';
	var chapMap={};
	var stdCount=0;
	var xScoredMarks=0;
	$.each(studentDataVM,function(indexCount,studentdata){
		if(studentdata.totalMarksScored!=null){
		$.each(studentdata.chapterReportVmList, function(index,chapterData) {
			if(typeof chapMap[chapterData.chapterName] == "undefined"){
				chapMap[chapterData.chapterName]=chapterData.chapterWiseTotalMarks;
				xScoredMarks=xScoredMarks+chapterData.chapterWiseTotalMarks;
			}else{
				chapMap[chapterData.chapterName]=chapMap[chapterData.chapterName]+chapterData.chapterWiseTotalMarks;
				xScoredMarks=xScoredMarks+chapterData.chapterWiseTotalMarks;
			}
		});
		
			stdCount=stdCount+1;
			totalMarksScored=totalMarksScored+studentdata.totalMarksScored;
			totalPercentageMarksScored=totalPercentageMarksScored+studentdata.percentageScore;
		
	}
	});
	
	$.each(chapters, function(index,chapterVm) {	
		var chapterName=chapterVm.chapterName;
		var marksScored=0;
		var totalMarkScored=0;
		var percentageChapterWise=0;
		if(index > 0){
			rowSpan+= "<tr class='textCenter' >"
		}
		rowSpan +="<td class='textCenter'  >"+chapterVm.chapterName+"</td>";
		$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
			var qWiseScore=studentFooterData[chapterName][indexNumber];
			marksScored=marksScored+qWiseScore;
			rowSpan +="<td class='textCenter' >"+ qWiseScore+"</td>";
		});
		
		var totalChapterperData=0;
		if(chapMap.hasOwnProperty(chapterName)==true){
			var chapData=chapMap.hasOwnProperty(chapterName)?chapMap[chapterName]:1;
			if(chapData==0){
				chapData=1;
			}
			totalChapterperData=100*(marksScored/chapData).toFixed(2);
		}
		var chapMapData=chapMap.hasOwnProperty(chapterName)==true?chapMap[chapterName]:0;
		rowSpan+="<td class='textCenter'  >"+marksScored+"</td>"+
		"<td class='textCenter' >"+chapMapData+"</td>"+
		"<td class='textCenter'  >"+totalChapterperData.toFixed(0)+"%"+"</td>";
		stdCount=stdCount!=0?stdCount:1;
		if(index==0){
			rowSpan+= "<td class='textCenter'  rowspan="+totalChapterCount+">"+totalMarksScored+"/"+xScoredMarks+"</td>"+
			"<td  class='textCenter'  rowspan="+totalChapterCount+">"+parseFloat(totalPercentageMarksScored/stdCount).toFixed(2)+"%"+"</td></tr>";
		}else{
			rowSpan+="</tr>";
		}

	});


	var	thead="<tr class='textCenter' >"+
	"<td class='textCenter'  rowspan='"+totalChapterCount+"' colspan='5'>Over All</td></tr>"+
	rowSpan;
	return thead;
};
var assessmentWiseScoreCardData=function(reportData,tableId){
	$("#assessmentHomeScorecardTable tbody tr").remove();

	$.each(reportData, function(index,studentData) {	
		var maxQuestionCountData=studentData.totalMarks;
		var totalMarks=studentData.totalMarksScored==null?"":studentData.totalMarksScored+"/"+maxQuestionCountData;
		var percentageScore="";
		if(studentData.totalMarksScored!=null){
		 percentageScore=studentData.percentageScore==-1?"":studentData.percentageScore+"%";
		}
		var rowSpan='';
		var count=studentData.numberOfChapters;
		$.each(studentData.chapterReportVmList, function(index,chapterVm) {
			var xChapterWiseScore="";
			var xChapterWisePercentage="";
			if(studentData.totalMarksScored!=null){
				xChapterWiseScore=chapterVm.chapterWiseScore;
				xChapterWisePercentage=chapterVm.chapterWisePercentage+"%";
			}
			
			if(index > 0){
				rowSpan+= '<tr id="trchap'+studentData.consolidatedStatusId+'">';
				}
			rowSpan +="<td title='"+chapterVm.chapterName+"' id=chap"+index+">"+chapterVm.chapterName+"</td>";
			$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
				if(qScoreMap == -1){
					qScoreMap="";
				}
				 rowSpan +="<td title='"+qScoreMap+"' id=chap"+index+"_Q"+indexNumber+">"+qScoreMap+"</td>";
			});
			rowSpan+="<td title='"+chapterVm.chapterWiseScore+"' id=chapterWiseScore"+index+">"+xChapterWiseScore+"</td>"+
			"<td title='"+chapterVm.chapterWiseTotalMarks+"'  id=chapterWiseTotalMarks"+index+">"+chapterVm.chapterWiseTotalMarks+"</td>"+
			"<td title='"+chapterVm.chapterWisePercentage+"'  id=chapterWisePercentage"+index+">"+xChapterWisePercentage+"</td>";
			
			if(index==0){
				rowSpan+= "<td title='"+totalMarks+"' rowspan="+count+">"+totalMarks+"</td>"+
				"<td title='"+percentageScore+"' rowspan="+count+">"+percentageScore+"</td></tr>";
			}else{
				rowSpan+="</tr>";
			}
			
		});
		
		
		row='<tr  id="tr'+studentData.consolidatedStatusId+'">'+
		
		'<td title="'+studentData.rollNumber+'" rowspan="'+count+'">'+studentData.rollNumber+'</td>'+
		
		'<td title="'+studentData.studentName+'" rowspan="'+count+'">'+studentData.studentName+'</td>'+
		'<td title="'+studentData.fatherName+'" rowspan="'+count+'">'+studentData.fatherName+'</td>'+
		'<td title="'+studentData.gender+'" rowspan="'+count+'">'+studentData.gender+'</td>'+
		'<td title="'+studentData.age+'" rowspan="'+count+'">'+studentData.age+'</td>'+
		rowSpan;
		
	
		$("#assessmentHomeScorecardTable tbody").append(row);
		if(studentData.isAppearedLate==true){
			$("#assessmentHomeScorecardTable #tr"+studentData.consolidatedStatusId).addClass('appread-late-student');
			$("#assessmentHomeScorecardTable #trchap"+studentData.consolidatedStatusId).addClass('appread-late-student');
			
		}
		
	});
	
	

}


/////////////////////// Create  Assessment wise Bloomtaxonomy //////////////////////////



var getStudentBloomTaxonomyByAssmt = function(uAssessmentPlannerId,uAssessmentId,uCampaignId,uSchoolId){

	$(".outer-loader").css('display','block');
	var studentData = reportCustomAjaxCalling("report/assessmentWiseBloomTaxonomy/"+uAssessmentPlannerId+"/"+uAssessmentId+"/"+uCampaignId+"/"+uSchoolId,null,"POST");
	if(studentData!=null){
		var studentFooterData = reportCustomAjaxCalling("report/getAssessmentLevelScorecardDataFooter/",studentData.studentLevelReportVm,"POST");
		$("#assessmentHomeBoomTaxonomyHeader").children("tr").remove();
		$("#assessmentHomeBoomTaxonomyFooter").children("tr").remove();
		$("#assessmentHomeBoomTaxonomyHeader").append(createTableForAssessmentWiseBloomTaxonomy(studentData));
		$("#assessmentHomeBoomTaxonomyFooter").append(createTableForAssessmentWiseBloomTaxonomyFooter(studentData.studentLevelReportVm,studentFooterData));
		
		assessmentWiseBloomTaxonomy(studentData.studentLevelReportVm,'assessmentHomeBloomResultTable');
		
		if(studentData.studentLevelReportVm==""){
			$("#assessmentHomeBloomResultTable tbody").append("<tr>"+"<td colspan='32' style='text-align:center'>No data available in table</td>"+"</tr>");

		}
		$(".outer-loader").css('display','none');
	}else{
		$(".outer-loader").css('display','none');
	}
	
}


var createTableForAssessmentWiseBloomTaxonomy=function(studentData){
	
	var noOfColmns=(studentData.maxNoOfRows+5);
	var rowSpan='';
		for(var i=1;i<=studentData.maxNoOfRows;i++){
			rowSpan +="<th  class='nosort' colspan='1'><p>Q"+i+"</p></th>";
		}
		var taxonomy='';
		for(var i=0;i<=studentData.maxNoOfRows-1;i++){
			taxonomy +="<th  class='nosort' colspan='1'><p>"+studentData.taxonomyHeaderList[i]+"</p></th>";
		}
	
	var	thead="<tr>"+
	"<th  class='nosort' rowspan='4'><p>Roll No.</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Student Name</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Father's Name</p></th>"+
	
	"<th  class='nosort' rowspan='4'><p>Gender</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Age</p></th>"+
	"<th  class='nosort' rowspan='4'><p>Chapter Names</p></th>"+
	"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Bloom Taxonomy Wise</p></th>"+
	"</tr>"+
	"<tr>"+
	"<th  class='nosort' colspan='"+studentData.maxNoOfRows+"' ><p>Question wise Parameter Score</p></th>"+
	"<th  class='nosort' colspan='5'><p>Overall Parameter Score</p></th>"+
	
	"</tr>"+
	"<tr>"+
	rowSpan+
	"<th  class='nosort' colspan='1' ><p>K</p></th>"+
	"<th  class='nosort' colspan='1'><p>U</p></th>"+
	"<th  class='nosort' colspan='1'><p>O</p></th>"+
	"<th  class='nosort' colspan='1'><p>AN</p></th>"+
	"<th  class='nosort' colspan='1'><p>AP</p></th>"+
	"</tr>"+
	"<tr>"+
	taxonomy+
	"<th  class='nosort' colspan='1' ><p>"+studentData.taxonomyWeightAge[0]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[1]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[2]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[3]+"</p></th>"+
	"<th  class='nosort' colspan='1'><p>"+studentData.taxonomyWeightAge[4]+"</p></th>"+
	"</tr>";
	return thead;
};	

var createTableForAssessmentWiseBloomTaxonomyFooter=function(taxReportData,studendFooterData){
	var studentData=taxReportData[0];
	if(studentData!=null){
	var rowSpan='';
	var count=studentData!=null?studentData.numberOfChapters:0;
	
	var parameterScoring={
			"K":0,
			"U":0,
			"O":0,
			"An":0,
			"Ap":0,
	};
	$.each(taxReportData, function(totalCount,data) {
		    parameterScoring["K"]=parameterScoring["K"]+data.parameterScoringForAssessmentVm["scored_K"];
			parameterScoring["U"]=parameterScoring["U"]+data.parameterScoringForAssessmentVm["scored_U"];
			parameterScoring["O"]=parameterScoring["O"]+data.parameterScoringForAssessmentVm["scored_O"];
			parameterScoring["An"]=parameterScoring["An"]+data.parameterScoringForAssessmentVm["scored_An"];
			parameterScoring["Ap"]=parameterScoring["Ap"]+data.parameterScoringForAssessmentVm["scored_Ap"];
		
		
	});
	
	$.each(studentData.chapterReportVmList, function(chapCount,chapterVm) {	
		var chapterName=chapterVm.chapterName;
		if(chapCount > 0){
			rowSpan+= "<tr class='text-center-bold' >"
			}
		rowSpan +="<td class='text-center-bold' title='"+chapterName+"' >"+chapterName+"</td>";
		$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {
			var qWiseScore=studendFooterData[chapterName][indexNumber];
			 rowSpan +="<td class='textCenter'>"+ qWiseScore+"</td>";
			 
		});
		
		if(chapCount==0){
		rowSpan+=	"<td class='textCenter' rowspan="+count+">"+parameterScoring["K"]+"</td>"+
		"<td class='textCenter' rowspan="+count+">"+parameterScoring["U"]+"</td>"+
		"<td class='textCenter' rowspan="+count+">"+parameterScoring["O"]+"</td>"+
		"<td class='textCenter' rowspan="+count+">"+parameterScoring["An"]+"</td>"+
		"<td class='textCenter'  rowspan="+count+">"+parameterScoring["Ap"]+"</td>"+
		"</tr>";
			}else{
			rowSpan+="</tr>";
		}
		
	});
	
	var chaptotalCount=count+1;
	var	thead="<tr class='textCenter' >"+
	"<td class='textCenter' rowspan='"+chaptotalCount+"' colspan='5'>Over All</td></tr>"+
	rowSpan;
	return thead;	
}
}
var assessmentWiseBloomTaxonomy=function(reportData,tableId){
	$("#assessmentHomeBloomResultTable tbody tr").remove();
	$.each(reportData, function(index,studentData) {	
			
			var knowledgeScore=studentData.parameterScoringForAssessmentVm.scored_K==null?"0":studentData.parameterScoringForAssessmentVm.scored_K;
			var understandingScore=studentData.parameterScoringForAssessmentVm.scored_U==null?"0":studentData.parameterScoringForAssessmentVm.scored_U;
			var operationScore=studentData.parameterScoringForAssessmentVm.scored_O==null?"0":studentData.parameterScoringForAssessmentVm.scored_O;
			var analysisScore=studentData.parameterScoringForAssessmentVm.scored_An==null?"0":studentData.parameterScoringForAssessmentVm.scored_An;
			var applicationScore=studentData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentData.parameterScoringForAssessmentVm.scored_Ap;

			
			var rowSpan='';
			var count=studentData.numberOfChapters;
			$.each(studentData.chapterReportVmList, function(index,chapterVm) {	
				
				if(index > 0){
					rowSpan+= '<tr id="trchap'+studentData.consolidatedStatusId+'">';
					}
				rowSpan +="<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
				$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
					if(qScoreMap == -1){
						qScoreMap="";
					}
					 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
				});
				
				if(index==0){
				rowSpan+=	"<td title='"+knowledgeScore+"' rowspan="+count+">"+knowledgeScore+"</td>"+
				"<td title='"+understandingScore+"' rowspan="+count+">"+understandingScore+"</td>"+
				"<td title='"+operationScore+"' rowspan="+count+">"+operationScore+"</td>"+
				"<td title='"+analysisScore+"' rowspan="+count+">"+analysisScore+"</td>"+
				"<td title='"+applicationScore+"' rowspan="+count+">"+applicationScore+"</td>"+
				"</tr>";
					}else{
					rowSpan+="</tr>";
				}
				
			});
			
		
		
		
		
	var	row='<tr  id="tr'+studentData.consolidatedStatusId+'">'+
	    "<td title='"+studentData.rollNumber+"' rowspan="+count+">"+studentData.rollNumber+"</td>"+
	    "<td title='"+studentData.studentName+"' rowspan="+count+">"+studentData.studentName+"</td>"+
		"<td title='"+studentData.fatherName+"' rowspan="+count+">"+studentData.fatherName+"</td>"+
		"<td title='"+studentData.gender+"' rowspan="+count+">"+studentData.gender+"</td>"+
		"<td title='"+studentData.age+"' rowspan="+count+">"+studentData.age+"</td>"+
		
		rowSpan;
	
	
	$("#assessmentHomeBloomResultTable tbody").append(row);
	if(studentData.isAppearedLate==true){
		$("#assessmentHomeBloomResultTable #tr"+studentData.consolidatedStatusId).addClass('appread-late-student');
		$("#assessmentHomeBloomResultTable #trchap"+studentData.consolidatedStatusId).addClass('appread-late-student');
		
	}
	});
}

/*$("#genderDetailsForm input[name='genderwise']").click(function(){
	var genderWiseCol=["h22","h23","h25","h26","h28","h29","h31","h32","h34","h35","h37","h38","h40","h41","h43","h44","h46","h47"];
	var genderHeader=["h7","h8","9","10","h11","h12","h13","h14","h15"];
	
	if($('input:radio[name=genderwise]:checked').val() == "No"){
		$.each(genderWiseCol,function(index,obj){
			$("#"+obj).css("display","none");
		});
		$.each(genderHeader,function(index,obj){
			document.getElementById(obj).colSpan="1";
		});
    }else{
    	$.each(genderWiseCol,function(index,obj){
			$("#"+obj).css("display","block");
		});
		$.each(genderHeader,function(index,obj){
			document.getElementById(obj).colSpan="3";
		});
    }
});

var viewWithOutGenderWiseResult=function() {
	
	
	
	
	
	
	
}*/


$('input:radio[name="genderwise"]').change(function(){
    if($(this).val() == 'Yes'){
      $("#schoolwiseAssementDetailTableArea").show();
      $("#search1").show();
      $("#schoolwiseAssementDetailTableAreaWithOutGenderDiv").hide();
   
   }else{
    $("#schoolwiseAssementDetailTableArea").hide();
    $("#search1").hide();
	$("#schoolwiseAssementDetailTableAreaWithOutGenderDiv").show();
   }
});





/*var fnSetDTColFilterPaginationWithoutExportButtonFooterCalBackStudentDetail =function(tId,nCols,exCols,hNames,thSum){
	var exportCols =[];
	for(var n=1;n<(nCols-1);n++){
		exportCols.push(n);
	}
	$(tId).on('order.dt',enableTooltip)
	.on( 'search.dt',enableTooltip )
	.on( 'page.dt',enableTooltip)
	.DataTable({
		"pagingType": "numbers",	
		"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"><"pull-right"><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		"bLengthChange": true,
		"bDestroy": true,
		"bFilter": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"fnDrawCallback": function ( oSettings ) {
			 Need to redo the counters if filtered or sorted 
			if ( oSettings.bSorted || oSettings.bFiltered ){
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ){
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		},
		"footerCallback": function( tfoot, data, start, end, display ) {
			var api = this.api();
			var intVal = function ( i ) {
				return typeof i === 'string' ?
						i.replace(/[\$,%,]/g, '')*1 :
							typeof i === 'number' ?
									i : 0;
			};

			// Total over all pages
			$.each(thSum,function(index,aSum){
				total = api.column( aSum ).data().reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );

				// Total over this page
				pageTotal = api.column( aSum, { page: 'current'} ).data().reduce( function (a, b) {
					return intVal(a) + intVal(b);
				}, 0 );

				// Update footer
				$( api.column( aSum ).footer() ).html(total);
			});

		},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		"aaSorting": [],
		bSortCellsTop:true,
		"fnRowCallback" : function(row,data,parm1,parm2){
			console.log(row,data,parm1,parm2);
		}

	});
	fnApplyColumnFilter(tId,nCols,exCols,hNames);
}*/

$(function() {
	
	
	$('#mdlStudentLevelReportDetailView').on('hidden.bs.modal', function () {
		fnUpdateBody();
	})
	
	thStudentLabels =['#','Student','Father  ','Mother  '];
	setDataTablePagination('#tableStudentDetailView',4,[0,1,2,3],thStudentLabels);
	setOptionsForMultipleSelect('#selectBox_questionPaper');
	
	thLabelsStudentDetails =['Roll No.','Student Name  ','Father Name  ','Gender ','Age ','Total Marks Scored ','% Score ','To Undergo PAT ','Parameter Scoring For Assessment ','K ','U ','O ','An','Ap'];
	thSum=[8,9,10,11,12];
	//fnSetDTColFilterPaginationWithoutExportButtonFooterCalBack
	fnSetDTColFilterPaginationWithoutExportButtonFooterCalBack('#tableForStudent',20,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19],thLabelsStudentDetails,thSum);
	setOptionsForMultipleSelect('#selectBox_grade');
	setOptionsForMultipleSelect('#selectBox_section');
	setDataTablePaginationForStudent('#studentLevelReportTable');
	setDataTablePagination('#tableSearchStudentDetail');
	fnColSpanIfDTEmpty('tableStudentDetailView',4);
	fnColSpanIfDTEmpty('schoolwiseAssementDetailTable',62);
	fnColSpanIfDTEmpty('tableForStudent',20);
	
	
	
	
	
	$("#studentSearchForm") .formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
			studentName : {
				validators : {
					callback : {
						message : ' ',
						callback : function(value, validator, $field) {
							var regx=/^[^'?\"/\\]*$/;
							if(/\ {2,}/g.test(value))
								return false;
							return regx.test(value);}							
					}
				}
			},
		fatherName : {
			validators : {
				callback : {
					message : ' ',
					callback : function(value, validator, $field) {
						var regx=/^[^'?\"/\\]*$/;
						if(/\ {2,}/g.test(value))
							return false;
						return regx.test(value);}							
				}
			}
		},
		studentId : {
			validators : {
				 callback : {
					message : ' ',
					callback : function(value, validator, $field) {
						var regx=/^[1-9]([0-9]{0,9}$)/;
						return regx.test(value);}							
				}
			}
		},
		assessmentName: {
			validators: {
				callback: {
					message: '      ',
					callback: function(value, validator, $field) {
						// Get the selected options
						var options =$('#selectBox_questionPaper').find('option:selected').val();
						
						return (options !=null && options !=undefined);
					}
				}
			}
		}
		}
	
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		 $(".outer-loader").css('display','block');
		 $.wait(30).then(fnGetStudentData);
		
		
	});
	
	
	fnCustomSearchFilter("#schoolwiseAssementDetailTableSearch",".r1");
	fnCustomSearchFilter("#schoolwiseAssementDetailTableWithOutGenderSearch",".r2");
	fnCustomSearchFilter("#tableForStudentSearch",".r3");
	fnCustomSearchFilter("#assessmentHomeScorecardTableSearch",".r4");
	fnCustomSearchFilter("#assessmentHomeBloomResultTableSearch",".r5");
	
	/*$('#mdlStudentDetailView').on('hidden.bs.modal', function (e) {
	    $(this).find(".modal-body").scrollTop(0);
	});*/
	$('#mdlStudentDetailView').on('hide.bs.modal', function (e) {
	    $(this).find(".modal-body").scrollTop(0);
	});
});	





//custom search bar
function fnCustomSearchFilter(searchBox,tblResult){
	$(searchBox).keyup(function () {
		var searchTerm = $(searchBox).val();
		var listItem = $(tblResult+' tbody').children('tr');
		var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
	
		$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
			return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
		});
	
		$(tblResult+" tbody td").removeAttr("style");
		if($(searchBox).val().length!=0){
			$(tblResult+" tbody td:containsi('" + searchSplit + "')").each(function(e){			
				$(this).css('background','yellow');
			});
		}else{
			$(tblResult+" tbody td").removeAttr("style");
		}
	
	});
}


function exportMe(ele,typeName){
	
	var pName =$('#campaignTitleText').text();
	var  qpName =$('#questionPaperNameX').text();
	var tableTypeName="View Student Wise Result : "+typeName
	var addInfo =[pName,'Question paper:'+qpName,tableTypeName];
	$(ele).tableExport(
					{
						type:'excel',
						escape:'false',
						additionalInfo:addInfo
						}
				);
}	