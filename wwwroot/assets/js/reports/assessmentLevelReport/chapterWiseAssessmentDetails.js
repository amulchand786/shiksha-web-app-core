

var getSectionList = function(){	
	
	$('#selectBox_section').find("option:selected").prop('selected', false);
	$('#selectBox_section').multiselect('refresh').multiselect('destroy');
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').val());
	bindToElementId = $('#selectBox_section');
	$('.outer-loader').show();
	bindSectionBySchoolAndGradeToListBox(bindToElementId,schoolId,gradeId,0);
	setOptionsForMultipleSelect('#selectBox_section');
	$('.outer-loader').hide();
	$('#divSelectSection').show();
	
	//fnCollapseMultiselect();
}

var bindSectionBySchoolAndGradeToListBox = function(bindToElementId,selectedSchoolId,selectedGradeId,selectedId)
{	
	json ={
			"schoolId": selectedSchoolId,
			"gradeId": selectedGradeId
	}
	var sections = customAjaxCalling("sectionlist", json,"POST");
	if(sections!=null || sections!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(sections,function(index,obj){
			$select.append('<option data-id="' + obj.sectionId + '"data-sectionname="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
		});
	}

}


$("#chapterWiseUpBackButton").on('click',function(){
	$("#chapterWiseStudentSearchDiv").hide();
	$("#chapterWiseUpBackButtonDiv").hide();
	$("#schoolDashboardBackButtonDiv").show();
	$("#chapterWiseAllStudentDetailDiv").show();
	resetFormById('studentSearchForm');
	
});

$("#studentAssessmentDetailFormBtn").on('click',function(){
	
	$("#chapterWiseAllStudentDetailDiv").hide();
	$("#schoolDashboardBackButtonDiv").hide();
	$("#chapterWiseUpBackButtonDiv").show();
	$("#chapterWiseStudentSearchDiv").show();
	resetFormById('studentSearchForm');
});

////////////////////////switch to seearch by name and father name //////////////////////////
var fnUncheckORBtn=function(){
$('#studentId').val("");
$('#studentSearchForm').data('formValidation').updateStatus('studentId', 'VALID');
$('#studentSearchForm').formValidation('revalidateField', 'studentName')
.formValidation('revalidateField', 'fatherName');
};


////////////////////////// Switch to search by student Id ///////////////////////
var fnCheckORBtn=function(){
	$('#studentName').val("");
	$('#fatherName').val("");
	$('#studentSearchForm').data('formValidation').updateStatus('studentName', 'VALID');
	$('#studentSearchForm').data('formValidation').updateStatus('fatherName', 'VALID');
	$('#studentSearchForm').formValidation('revalidateField', 'studentId');
	

};

var fnGetStudentData=function(){
	var studentName=$('#studentName').val();
	var fatherName=$('#fatherName').val();
	var rollNumber=$('#studentId').val();
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').val());
	var sectionId = parseInt($("#selectBox_section").find('option:selected').data('id'));
	var jsonData={
			"rollNumber":rollNumber,
			"fatherName":fatherName,
			"name":studentName,
			"schoolId":schoolId,
			"gradeId":gradeId,
			"sectionId":sectionId
	}
	var studentDetailData=	customAjaxCalling("student/searchStudentByDetails", jsonData, "POST");
	var validData=studentDetailData!=null?studentDetailData.length>0?true:false:false;
	if(validData==true){
		var student=studentDetailData[0];
		$("#roolNumberMsg").hide();
		fnGetStudentLevelReport(student.studentAcademicYearSchoolGradeSectionMapperId,schoolId,student.studentId);
	}else{
		$("#roolNumberMsg").show();
		$("#roolNumberMsg").fadeOut(2500);
	}
	


};

///////////////////////////
/////////////////////////// Create search student table ///////////////////////////////

//////////////////////////////// Get student level report /////////////////////////////////////

var fnGetStudentLevelReport=function(studentAcademicYearSchoolGradeSectionMapperId,schoolId,studentId){
	var assessmentId=$('#selectBox_questionPaper').find('option:selected').val();

	var studentData=customAjaxCalling("student/getStudentDataByStudentAcademicYearSchoolGradeSectionMapper/"+studentAcademicYearSchoolGradeSectionMapperId+"/"+studentId, null, "POST");
	if(studentData!=null){
		$('#resultStudentId').text(studentData.rollNumber);
		$('#resultStudentName').text(studentData.name);
		$('#resultFatherName').text(studentData.fatherName);
		$('#resultGrade').text(studentData.gradeName);
		
		var jsonData={
				"schoolId":schoolId,
				"studentAcademicYearSchoolGradeSectionMapperId":studentAcademicYearSchoolGradeSectionMapperId,
				"assessmentId":assessmentId,
				"studentId":studentData.studentId
		}
		
		
		var studentDetailData=customAjaxCalling("report/studentLevelReportData", jsonData, "POST");

		if(studentDetailData!=null){
			createStudentlevelReport(studentDetailData);
		}
		$('#mdlStudentLevelReportDetailView').modal();
		$('#chapterwiseResult input:radio[name=chapterwises][value="scoreCard"]').removeAttr('checked');
		$('#chapterwiseResult input:radio[name=chapterwises][value="bloomTaxonomyWise"]').removeAttr('checked');
		$('#chapterwiseResult input:radio[name=chapterwises][value="Yes"]').prop('checked', true);
		fnClearCustomSearch(['.r5','.r4','.r3']);
		$("#search5").hide();
		$("#search4").hide();
		$("#search3").show();
		

	}

}


/////////////////////////////////////////////////////// Student level report ////////////////////


var createStudentlevelReport=function(studentlevelReportData){
	deleteAllRow('studentLevelReportTable');
	$.each(studentlevelReportData, function(index,studentReportData) {	
		appendNewRow('studentLevelReportTable',getNewRow(studentReportData));
	});
	$("#displayResult").show();
	$("#rowDiv2").show();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();
	$('#studentLevelReportTable').show();
	$('#reportfilter').removeClass('in');
	
}

var getNewRow =function(studentReportData){
	var totalMarks=studentReportData.totalMarksScored==null?"":studentReportData.totalMarksScored+"/"+studentReportData.totalMarks;
	var percentageScore="";
	if(studentReportData.totalMarksScored!=null){
		studentReportData.percentageScore==-1?"":studentReportData.percentageScore+"%";
	}
	var toUnderGoPa = (studentReportData.toUnderGoPA==null)?"" 
														: (studentReportData.toUnderGoPA==true)?studentReportData.toUnderGoPA="YES" 
															: studentReportData.toUnderGoPA="NO";
	var knowledgeScore=studentReportData.parameterScoringForAssessmentVm.scored_K==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_K;
	var understandingScore=studentReportData.parameterScoringForAssessmentVm.scored_U==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_U;
	var operationScore=studentReportData.parameterScoringForAssessmentVm.scored_O==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_O;
	var analysisScore=studentReportData.parameterScoringForAssessmentVm.scored_An==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_An;
	var applicationScore=studentReportData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_Ap;

var	row="<tr>"+
	"<td title='"+studentReportData.assessmentName+"' >"+studentReportData.assessmentName+"</td>"+
	"<td title='"+studentReportData.statusOfCompletion+"' >"+studentReportData.statusOfCompletion+"</td>"+
	"<td title='"+studentReportData.stageName+"' >"+studentReportData.stageName+"</td>"+
	"<td title='"+studentReportData.subjectName+"' >"+studentReportData.subjectName+"</td>"+
	"<td title='"+totalMarks+"' >"+totalMarks+"</td>"+
	"<td title='"+percentageScore+"' >"+percentageScore+"</td>"+
	"<td title='"+toUnderGoPa+"' >"+toUnderGoPa+"</td>"+
	"<td title='"+knowledgeScore+"' >"+knowledgeScore+"</td>"+
	"<td title='"+understandingScore+"' >"+understandingScore+"</td>"+
	"<td title='"+operationScore+"' >"+operationScore+"</td>"+
	"<td title='"+analysisScore+"' >"+analysisScore+"</td>"+
	"<td title='"+applicationScore+"' >"+applicationScore+"</td>"+
	"</tr>";
	return row;
	};
	
	
	$('#chapterwiseResult input:radio[name=chapterwises]').change(function(){
		fnClearCustomSearch(['.r5','.r4','.r3']);
		if($(this).val() == 'Yes'){
				$("#rowDiv3").hide();
				$("#rowDiv2").show();
				$("#rowDiv4").hide();
				$('#studentLevelReportTable').show();
		    } else if($(this).val() == 'scoreCard'){
		    	$("#search5").hide();
				$("#search3").hide();
				$("#search4").show();
		    	genrateScoreCardRow();
		    	
		    }else{
		    	$("#search5").show();
				$("#search3").hide();
				$("#search4").hide();
		    	genrateBloomTaxonomyRow();
		   
		    }
		});
	
	
	
	////////////////////////// score card////////////////////////
	


	var genrateScoreCardRow=function(){
		$('#scoreBoardTableDiv').css('display','block');
	
		var studentlevelScoreCardData = customAjaxCalling("report/studentlevelScoreCard",null,"POST");
		
		$("#scoreBoardHeader").children("tr").remove();
		$("#scoreBoardHeader").append(createTableForScoreCard(studentlevelScoreCardData.maxNoOfRows));
		createStudentlevelScoreCardReport(studentlevelScoreCardData.studentLevelReportVm);
		
		
	}

	var createTableForScoreCard=function(maxNumberOfColumns){
		var noOfColmns=maxNumberOfColumns+6;
		var rowSpan='';
			for(var i=1;i<=maxNumberOfColumns;i++){
				rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
			}
		
		var	thead="<tr>"+
		"<th  class='nosort' rowspan='3'><p>Assessments Name</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
		"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
		"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
		"</tr>"+
		"<tr>"+
		"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise scores</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Marks Scored</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Total Marks</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Capterwise % score</p></th>"+
		"<th  class='nosort' rowspan='2'><p>Total marks scored</p></th>"+
		"<th  class='nosort' rowspan='2'><p>% score</p></th>"+
		"</tr>"+
		"<tr>"+
		rowSpan
		"</tr>";
		return thead;
	};


	var createStudentlevelScoreCardReport=function(studentlevelScoreCardData){
		$("#scoreBoardTable tbody tr").remove();
		$.each(studentlevelScoreCardData, function(index,studentScoreCardData) {	
			$("#scoreBoardTable tbody").append(getScoreCardNewRow(studentScoreCardData));
		});
		$("#rowDiv2").hide();
		$("#rowDiv3").show();
		$("#rowDiv4").hide();
		$('#scoreBoardTable').show();
	}


	var getScoreCardNewRow =function(studentScoreCardData){
		var maxMarks=studentScoreCardData.totalMarks;
		var totalMarks=studentScoreCardData.totalMarksScored==null?"":studentScoreCardData.totalMarksScored+"/"+maxMarks;
		var percentageScore="";
		if(studentScoreCardData.totalMarksScored!=null){
			percentageScore=studentScoreCardData.percentageScore==-1?"":studentScoreCardData.percentageScore+"%";
		}
		var rowSpan='';
		var count=studentScoreCardData.numberOfChapters;
		$.each(studentScoreCardData.chapterReportVmList, function(index,chapterVm) {	
			chapterWisePercentage=chapterVm.chapterWisePercentage==-1?"":chapterVm.chapterWisePercentage+"%";
			
			if(index > 0){
				rowSpan+= "<tr>"
				}
			rowSpan +="<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
			$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
				if(qScoreMap == -1){
					qScoreMap="";
				}
				 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
			});
			rowSpan+="<td title='"+chapterVm.chapterWiseScore+"' >"+chapterVm.chapterWiseScore+"</td>"+
			"<td title='"+chapterVm.chapterWiseTotalMarks+"' >"+chapterVm.chapterWiseTotalMarks+"</td>"+
			"<td title='"+chapterVm.chapterWisePercentage+"' >"+chapterWisePercentage+"</td>";
			
			if(index==0){
			rowSpan+= "<td title='"+totalMarks+"' rowspan="+count+">"+totalMarks+"</td>"+
			"<td title='"+percentageScore+"' rowspan="+count+">"+percentageScore+"</td></tr>";
				}else{
				rowSpan+="</tr>";
			}
			
		});
		var	row="<tr>"+
		"<td title='"+studentScoreCardData.assessmentName+"' rowspan="+count+">"+studentScoreCardData.assessmentName+"</td>"+
		"<td title='"+studentScoreCardData.statusOfCompletion+"' rowspan="+count+">"+studentScoreCardData.statusOfCompletion+"</td>"+
		"<td title='"+studentScoreCardData.stageName+"' rowspan="+count+">"+studentScoreCardData.stageName+"</td>"+
		rowSpan;
		return row;
		};
		
	//////////////////////////////////////////Boolm taxonomy //////////////////////////////

		var genrateBloomTaxonomyRow=function(){
			$("#rowDiv3").hide();
			$('#rowDiv4').css('display','block');
			
			var studentlevelBloomTaxonomyData = customAjaxCalling("report/studentlevelbloomTaxonomy",null,"POST");
			//deleteAllRow('bloomTaxonomyTable');
			$("#bloomTaxonomyHeader").children("tr").remove();
			$("#bloomTaxonomyHeader").append(createTableForBloomTaxonomy(studentlevelBloomTaxonomyData.maxNoOfRows));
			createStudentlevelBloomTaxonomyReport(studentlevelBloomTaxonomyData.studentLevelReportVm);
				
				
			}

			
			
		var createTableForBloomTaxonomy=function(maxNumberOfColumns){
			var noOfColmns=maxNumberOfColumns+5;
			var rowSpan='';
				for(var i=1;i<=maxNumberOfColumns;i++){
					rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
				}
			
			var	thead="<tr>"+
			"<th  class='nosort' rowspan='3'><p>Assessments Name</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
			"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
			"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
			"</tr>"+
			"<tr>"+
			"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise Parameter Score</p></th>"+
			"<th  class='nosort' colspan='5'><p>Overall Parameter Score</p></th>"+
			
			"</tr>"+
			"<tr>"+
			rowSpan+
			"<th  class='nosort' ><p>K</p></th>"+
			"<th  class='nosort' ><p>U</p></th>"+
			"<th  class='nosort' ><p>O</p></th>"+
			"<th  class='nosort' ><p>AN</p></th>"+
			"<th  class='nosort' ><p>AP</p></th>"+
			"</tr>";
			return thead;
		};	


		var createStudentlevelBloomTaxonomyReport=function(studentlevelBloomTaxonomyData){
			$("#bloomTaxonomyTable tbody tr").remove();
			$.each(studentlevelBloomTaxonomyData, function(index,studentBloomTaxonomyData) {	
				$("#bloomTaxonomyTable tbody").append(getBloomtaxonomyNewRow(studentBloomTaxonomyData));
			});
			$("#rowDiv2").hide();
			$("#rowDiv3").hide();
			$("#rowDiv4").show();
			$('#bloomTaxonomyTable').show();
		}

			

		var getBloomtaxonomyNewRow =function(studentBloomTaxonomyData){
			var knowledgeScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K;
			var understandingScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U;
			var operationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O;
			var analysisScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An;
			var applicationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap;
			var rowSpan='';
			var count=studentBloomTaxonomyData.numberOfChapters;
			
			rowSpan +="<th title='' ></th>";
			$.each(studentBloomTaxonomyData.taxonomyHeaderList, function(indexNumber,taxonomyHeader) {	
			
			 rowSpan +="<th title='"+taxonomyHeader+"' >"+taxonomyHeader+"</th>";
			});
			$.each(studentBloomTaxonomyData.taxonomyWeightAge, function(indexNumber,taxonomyWeightage) {	
				 rowSpan +="<th title='"+taxonomyWeightage+"' >"+taxonomyWeightage+"</th>";
				});
			rowSpan+="</tr>";
			$.each(studentBloomTaxonomyData.chapterReportVmList, function(index,chapterVm) {	
				rowSpan +="<tr>"+ "<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
				$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
					if(qScoreMap == -1){
						qScoreMap="";
					}
					 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
				});
				if(index==0){
				rowSpan+= "<td title='"+knowledgeScore+"' rowspan="+count+" >"+knowledgeScore+"</td>"+
				"<td title='"+understandingScore+"' rowspan="+count+">"+understandingScore+"</td>"+
				"<td title='"+operationScore+"' rowspan="+count+">"+operationScore+"</td>"+
				"<td title='"+analysisScore+"' rowspan="+count+">"+analysisScore+"</td>"+
				"<td title='"+applicationScore+"' rowspan="+count+">"+applicationScore+"</td>"+
				"</tr>";
					}else{
					rowSpan+="</tr>";
				}
				
			});
			var	row="<tr>"+
			"<td title='"+studentBloomTaxonomyData.assessmentName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.assessmentName+"</td>"+
			"<td title='"+studentBloomTaxonomyData.statusOfCompletion+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.statusOfCompletion+"</td>"+
			"<td title='"+studentBloomTaxonomyData.stageName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.stageName+"</td>"+
			rowSpan;
			return row;
			};
$("#studentSearchForm") .formValidation({
	excluded : ':disabled',
	feedbackIcons : {
		validating : 'glyphicon glyphicon-refresh'
	},
	fields : {
		studentName : {
			validators : {
				callback : {
					message : ' ',
					callback : function(value, validator, $field) {
						var regx=/^[^'?\"/\\]*$/;
						if(/\ {2,}/g.test(value))
							return false;
						return regx.test(value);}							
				}
			}
		},
	fatherName : {
		validators : {
			callback : {
				message : ' ',
				callback : function(value, validator, $field) {
					var regx=/^[^'?\"/\\]*$/;
					if(/\ {2,}/g.test(value))
						return false;
					return regx.test(value);}							
			}
		}
	},
	studentId : {
		validators : {
			 callback : {
				message : ' ',
				callback : function(value, validator, $field) {
					var regx=/^[1-9]([0-9]{0,9}$)/;
					return regx.test(value);}							
			}
		}
	},
	assessmentName: {
		validators: {
			callback: {
				message: '      ',
				callback: function(value, validator, $field) {
					// Get the selected options
					var options =$('#selectBox_questionPaper').find('option:selected').val();
					
					return (options !=null && options !=undefined);
				}
			}
		}
	}
	}

}).on('success.form.fv', function(e) {
	e.preventDefault();
	fnGetStudentData(e);
});










$(function() {


	$('#mdlChapterWiseView #selectBox_grade').multiselect('destroy');
	$('#mdlChapterWiseView #selectBox_section').multiselect('destroy');
	$('#mdlChapterWiseView #selectBox_questionPaper').multiselect('destroy');
	$('#mdlChapterWiseView #selectBox_questionPaperAll').multiselect('destroy');
	$("#selectBox_grade option:selected").removeAttr("selected");
	$("#selectBox_section option:selected").removeAttr("selected");
	$("#selectBox_questionPaper option:selected").removeAttr("selected");
	setOptionsForMultipleSelect('#selectBox_grade');
	setOptionsForMultipleSelect('#selectBox_section');
	setOptionsForMultipleSelect('#selectBox_questionPaperAll');
	setOptionsForMultipleSelect('#selectBox_questionPaper');
});