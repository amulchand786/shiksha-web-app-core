var thLabelsMetrics3;
var thLabelsMetrics4;

var fnActivatePanById=function(tabId,tabPanelId){
	$("#mdlViewSchoolMetrics .active").each(function(index,ele){
		$(ele).removeClass('active');
	});
	$(tabId).addClass('active');
	$(tabPanelId).addClass('active');
	$(tabId).trigger('click');
	
}
var fnViewMetrics=function(){
	fnActivatePanById("#schoolMetrics1","#schoolMetrics1li");
	
	$('#mdlViewSchoolMetrics').modal();


}

var assessmentMetricsReport=function(){
	$(".outer-loader").show();
	var asessmentMetricsReportData = reportCustomAjaxCalling("report/metricsQuestionPaperReportData",null,"POST");
	if(asessmentMetricsReportData!=null){
		$.each(asessmentMetricsReportData, function(index,assessmentMetricsData) {	
			var studentEnroldedTD;
			var studentAppearedTD;
			var studentAbsentTD;
			var studentYetToTakeTD;
			var studentGTETD;
			var studentLTETD;

			studentEnroldedTD='<td title="'+assessmentMetricsData.numberOfStudentsEnrolled.totalStudent+'" >'+assessmentMetricsData.numberOfStudentsEnrolled.totalStudent+'</td>';
			studentGTETD='<td title="'+assessmentMetricsData.numberOfStudentsScoredGTEQ90.totalStudent+'" >'+assessmentMetricsData.numberOfStudentsScoredGTEQ90.totalStudent+'</td>';
			studentAppearedTD='<td title="'+assessmentMetricsData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'" >'+assessmentMetricsData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'</td>';
			studentLTETD='<td title="'+assessmentMetricsData.numberOfStudentsScoredLTEQ90.totalStudent+'" >'+assessmentMetricsData.numberOfStudentsScoredLTEQ90.totalStudent+'</td>';
			studentYetToTakeTD=	'<td title="'+assessmentMetricsData.numberOfStudentsYetToTakeAssessment.totalStudent+'" >'+assessmentMetricsData.numberOfStudentsYetToTakeAssessment.totalStudent+'</td>';
			studentAbsentTD='<td title="'+assessmentMetricsData.numberOfStudentsAbsentForAssessment.totalStudent+'" >'+assessmentMetricsData.numberOfStudentsAbsentForAssessment.totalStudent+'</a></td>';

			var row;
			var secondTableRow;
			row='<tr  id="'+assessmentMetricsData.assessmentId+'">'+
			'<td>'+(index+1)+'</td>'+
			'<td title="'+assessmentMetricsData.assessmentName+'" data-assessmentId="'+assessmentMetricsData.id+'">'+assessmentMetricsData.assessmentName+'</td>'+
			'<td title="'+assessmentMetricsData.campaignName+'" >'+assessmentMetricsData.campaignName+'</td>'+
			'<td title="'+assessmentMetricsData.status+'" >'+assessmentMetricsData.status+'</td>'+
			'<td title="'+assessmentMetricsData.gradeName+'" >'+assessmentMetricsData.gradeName+'</td>'+
			'<td title="'+assessmentMetricsData.stageName+'" >'+assessmentMetricsData.stageName+'</td>'+
			'<td title="'+assessmentMetricsData.subjectName+'" >'+assessmentMetricsData.subjectName+'</td>'+
			studentEnroldedTD+studentAppearedTD+
			'<td title="'+assessmentMetricsData.percentageOfStudentsAttempted.totalStudent+'" >'+assessmentMetricsData.percentageOfStudentsAttempted.totalStudent+' % </td>'+
			studentAbsentTD+studentYetToTakeTD+studentGTETD+
			'<td title="'+assessmentMetricsData.percentageOfStudentsScoredGTEQ90.totalStudent+'" >'+assessmentMetricsData.percentageOfStudentsScoredGTEQ90.totalStudent+' % </td>'+
			studentLTETD+
			'<td title="'+assessmentMetricsData.percentageOfStudentsScoredLTEQ90.totalStudent+'" >'+assessmentMetricsData.percentageOfStudentsScoredLTEQ90.totalStudent+'% </td>'+
			'</tr>';

			appendNewRow('metrics3ReportTable', row);

			secondTableRow='<tr  id="'+assessmentMetricsData.assessmentId+'">'+
			'<td>'+(index+1)+'</td>'+
			'<td title="'+assessmentMetricsData.assessmentName+'" data-assessmentId="'+assessmentMetricsData.id+'">'+assessmentMetricsData.assessmentName+'</td>'+
			'<td title="'+assessmentMetricsData.campaignName+'" >'+assessmentMetricsData.campaignName+'</td>'+
			'<td title="'+assessmentMetricsData.status+'" >'+assessmentMetricsData.status+'</td>'+
			'<td title="'+assessmentMetricsData.gradeName+'" >'+assessmentMetricsData.gradeName+'</td>'+
			'<td title="'+assessmentMetricsData.stageName+'" >'+assessmentMetricsData.stageName+'</td>'+
			'<td title="'+assessmentMetricsData.subjectName+'" >'+assessmentMetricsData.subjectName+'</td>'+
			'<td title="'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'</td>'+
			'<td title="'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_U+'</td>'+
			'<td title="'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_O+'</td>'+
			'<td title="'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_An+'</td>'+
			'<td title="'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap+'</td>'+
			'</tr>';

			appendNewRow('metrics4ReportTable', secondTableRow);
		});

		$(".outer-loader").hide();


	}
	$(".outer-loader").hide();
	$('#mdlViewSchoolMetrics').modal();

}
var fnShowMetrics3=function(){
	$("#metrics3ReportTableDiv").show();
}
var jsonData={};
var fnViewMetrics3=function(){
	var nDTMetrics3Rows = $("#metrics3ReportTable").dataTable().fnGetNodes();
	if(nDTMetrics3Rows.length==0){
	$(".outer-loader").show();
	$("#metrics3ReportTableDiv").hide();
	deleteAllRow('metrics3ReportTable');
	$.wait(50).then(assessmentMetricsReport).then(fnShowMetrics3);
	}else{
		fnShowMetrics3();
	}
	
}

var fnShowMetrics4=function(){
	$("#metrics4ReportTableDiv").show();
}


var fnViewMetrics4=function(){
	var nDTMetrics3Rows = $("#metrics3ReportTable").dataTable().fnGetNodes();
	if(nDTMetrics3Rows.length==0){
	$(".outer-loader").show();
	$("#metrics4ReportTableDiv").hide();
	deleteAllRow('metric4ReportTable');
	$.wait(50).then(assessmentMetricsReport).then(fnShowMetrics4);
	}else{
		fnShowMetrics4();	
	}

}


$(function() {
	enableMultiSelectCampaign("#selectBox_campaignMetrics3");
	enableMultiSelectCampaign("#selectBox_campaignMetrics4");
	thLabelsMetrics3 =['Assessments Name','Status of Completion  ','Grade  ','Stage ','Subject ','Number of Students Enrolled ','Number of Students Appeared in Assessment ','% of Students Appeared ','Number of Students Absent for Assessment ',
		'Number of Students Yet to Take Assessment ','Number of students scored &gt;= 90% (Appeared)','% of students scored &gt;= 90% (Appeared) ', 'Number of students scored &lt; 90%  (To Undergo Post Augmentation)','%students scored &lt; 90%  (To Undergo Post Augmentation) '];
	fnSetDTColFilterPaginationWithoutExportButton('#metrics3ReportTable',14,[0,1,2,3,4,5,6,7,8,9,10,11,12,13],thLabelsMetrics3);

	thLabelsMetrics4 =['Assessments Name','Status of Completion  ','Grade  ','Stage ','Subject ','Parameter Scoring For Assessment ','K ','U ','O ','An','Ap'];
	fnSetDTColFilterPaginationWithoutExportButton('#metrics4ReportTable',14,[0,1,2,3,4,5,6,7,8,9,10,11,12,13],thLabelsMetrics4);
	fnColSpanIfDTEmpty('metrics4ReportTable',14);
	fnColSpanIfDTEmpty('metrics3ReportTable',14);
	
	
});