var fnInitVars =function(){
	selectedLocType ='';
}
var START_STATUS='Pending';
var IN_PROGRESS_STATUS='In progress';
var COMPLETED_STATUS='Completed';

//global selectDivIds for smart filter
var locationDiv = new Object();	
	locationDiv.city 	="#divSelectCity";
	locationDiv.block 	="#divSelectBlock";
	locationDiv.NP 	="#divSelectNP";
	locationDiv.GP 	="#divSelectGP";
	locationDiv.RV 	="#divSelectRV";
	locationDiv.village ="#divSelectVillage";
	
	
var pageContextElement = new Object();
	pageContextElement.campaignStatus ="#selectBoxCampaignStatus";
	pageContextElement.campaignStartDate ="#startDatePicker";
	pageContextElement.campaignEndDate ="#endDatePicker";
	pageContextElement.school="#selectBox_schoolsByLoc";
	pageContextElement.grade="#selectBox_grade";
	pageContextElement.subject="#selectBox_subject";
	pageContextElement.source="#selectBox_source";
	pageContextElement.section="#selectBox_section";
	pageContextElement.stage="#selectBox_stage";
	pageContextElement.btnViewSchool="#btnViewSchool";
	pageContextElement.resetLocButton ="#btnResetLocation";
	
	var divElement = new Object();
	divElement.openLab="#divSelectOpenLab";
	divElement.schoolType="#divSelectSchoolType";
	divElement.btnSchoolView="#divSchoolViewButton";
	divElement.school="#divSelectSchool";
	divElement.grade="#gradeDiv";
	divElement.subject="#subjectDiv";
	divElement.source="#sourceDiv";
	divElement.section="#sectionDiv";
	divElement.stage="#stageDiv";
	divElement.btnSubmit="#submitBtnDiv";
	divElement.btnViewSchool="#divSchoolViewButton";
	
//location type on change...get locations...
$('#divLocType input:radio[name=locType]').change(function() {
		
		$('#locColspFilter').removeClass('in');
		$('#locationPnlLink').text('Select Location');		
		$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
        $("#tableDiv").hide();
		
		lObj =this;
		var typeCode =$(this).data('code');
		selectedLocType =typeCode;
		$('.outer-loader').show();		
		reinitializeSmartFilter(elementIdMap);	
		$("#hrDiv").show();
		if(typeCode.toLowerCase() =='U'.toLowerCase()){
			showDiv($(locationDiv.city),$(locationDiv.resetLocDivCity));
			hideDiv($(divElement.grade),$(divElement.schoolType),
					$(divElement.school),$(divElement.subject),
					$(divElement.source),$(divElement.section),
					$(divElement.stage),$(divElement.openLab),
					$(divElement.btnViewSchool),$(divElement.btnSubmit));
			$('input[name="schoolType"]').prop('checked', false);
			$('input[name="openlab"]').prop('checked', false);
			hideDiv($(locationDiv.block),$(locationDiv.NP),$(locationDiv.GP),$(locationDiv.RV),$(locationDiv.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("U");
			
			
		}else if(typeCode.toLowerCase() =='R'.toLowerCase()){
			hideDiv($(locationDiv.city),$(locationDiv.resetLocDivCity));
			hideDiv($(divElement.grade),$(divElement.schoolType),
					$(divElement.school),$(divElement.subject),
					$(divElement.source),$(divElement.section),
					$(divElement.stage),$(divElement.openLab),
					$(divElement.btnViewSchool),$(divElement.btnSubmit));
			$('input[name="schoolType"]').prop('checked', false);
			$('input[name="openlab"]').prop('checked', false);
			showDiv($(locationDiv.block),$(locationDiv.NP),$(locationDiv.GP),$(locationDiv.RV),$(locationDiv.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("R");
			
		}else {
			$('#divFilter').hide();
			$('#divResetLocVillage').hide();
			
			hideDiv($(divElement.grade),$(divElement.schoolType),
					$(divElement.school),$(divElement.subject),
					$(divElement.source),$(divElement.section),
					$(divElement.stage),$(divElement.openLab),
					$(divElement.btnViewSchool),$(divElement.btnSubmit));
			$('input[name="schoolType"]').prop('checked', false);
			$('input[name="openlab"]').prop('checked', false);
			
			$('#divSelectOpenLab').show();
			$('#detailsDiv').show();
		}
		
//		/fnCollapseMultiselect();
		
		$('.outer-loader').hide();
		return;
});


	var getSelectedData =function(elementObj,collector){
		$(elementObj).each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					collector.push( parseInt($(option).data('parentid')));
			});
		});
	}

var fnInitDatePicker =function(pDatePickerId){
	 $(pDatePickerId).datepicker({
			allowPastDates: true,
			momentConfig : {
				culture : 'en', // change to specific culture
				format : 'DD-MMM-YYYY' // change for specific format
			},
		})
}

$('#divSelectOpenLab input:radio[name=openlab]').change(function() {
	hideDiv($(divElement.grade),$(divElement.school),
			$(divElement.subject),$(divElement.source),
			$(divElement.section),$(divElement.stage),
			$(divElement.btnSubmit),$(divElement.btnViewSchool));
	$('input[name="schoolType"]').prop('checked', false);
	showDiv($(divElement.schoolType));
});
$('#divSelectSchoolType input:radio[name=schoolType]').change(function() {
	hideDiv($(divElement.grade),$(divElement.school),
			$(divElement.subject),$(divElement.source),
			$(divElement.section),$(divElement.stage),
			$(divElement.btnSubmit));
	showDiv($(divElement.btnViewSchool));
	
	
});

$('#btnViewSchool').on('click',function(){

	var locationIds=[];
	var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
	if(aLocTypeCode=="U"){
		$('#selectBox_cityByBlock').each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					locationIds.push( parseInt($(option).val()));
			});	
		});
	}else if(aLocTypeCode=="R"){
		$('#selectBox_villagebyRv').each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					locationIds.push( parseInt($(option).val()));
			});	
		});
	}


	
	var aSchoolType =$('input:radio[name=schoolType]:checked').data('id');
	var aOpenLabType =$('input:radio[name=openlab]:checked').val();
	var schoolTypeData=[];
	schoolTypeData.push(aSchoolType);	

	if(aOpenLabType=='All'){
		aOpenLabType=null;
	}
	var jsonData={
			"locations":locationIds,
			"schoolTypes":schoolTypeData,
			"isOpenLab":aOpenLabType
	}

	var schoolVmData = customAjaxCalling("school/report/"+aLocTypeCode,jsonData,"POST");
	var bintToElementId = $(pageContextElement.school);
	bindSchoolByVillageAndOpenLabAndSchoolTypeToListBox(schoolVmData,bintToElementId);
	
	// setOptionsForMultipleSelect(pageContextElement.school);
});


var bindSchoolByVillageAndOpenLabAndSchoolTypeToListBox =function(schoolData,bindToElementId){

	if(schoolData!=null && schoolData.length>0){
		$select	=bindToElementId;	
		$select.html('');
		for (var key in schoolData) {				
			$select.append('<option value="'+schoolData[key].id+'" data-id="'+schoolData[key].id+'" data-name="'+schoolData[key].schoolName+'">'+schoolData[key].schoolName+'</option>');
		}
		setOptionsForProjectReportMultipleSelect(pageContextElement.school);
		fnCollapseMultiselect();
		showDiv($(divElement.school));
	}else{
		$(bindToElementId).html('');
		hideDiv($(pageContextElement.grade),$(pageContextElement.subject),$(pageContextElement.section),$(pageContextElement.source),$(pageContextElement.stage),$(divElement.school));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no schools for the selected data or logged user is not mapped with school.");
		$('#mdlError').modal().show();
	}
	
};

var setOptionsForProjectReportMultipleSelect =function(){
	$(arguments).each(function(index,ele){
		$(ele).multiselect('destroy');
		$(ele).multiselect({
			maxHeight: 150,
			includeSelectAllOption: true,		
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			disableIfEmpty: true,
			onChange : function(event) {
				if(ele == (pageContextElement.school)){
					if ($(pageContextElement.school).find('option:selected').length == 0) {
						hideDiv($(divElement.grade),$(divElement.subject),
								$(divElement.source),$(divElement.section),
								$(divElement.stage),$(divElement.btnSubmit));
					}else{
						fnGetGradeBySchool();
						showDiv($(divElement.grade));
					}
				}else if(ele == (pageContextElement.grade)){
					if ($(pageContextElement.grade).find('option:selected').length == 0) {
						hideDiv($(divElement.subject),$(divElement.source),
								$(divElement.section),$(divElement.stage)
								,$(divElement.btnSubmit));
					}else{
						fnGetSubjectByGrade();
						showDiv($(divElement.subject));
					}
				}
				else if(ele == (pageContextElement.subject)){
					if ($(pageContextElement.subject).find('option:selected').length == 0) {
						hideDiv($(divElement.source),$(divElement.btnSubmit),
								$(divElement.section),$(divElement.stage),
								$(divElement.btnSubmit));
					}else{
						fnBindSource();
						showDiv($(divElement.source));
					}
				}
				else if(ele == (pageContextElement.source)){
					if ($(pageContextElement.source).find('option:selected').length == 0) {
						hideDiv($(divElement.section),$(divElement.stage)
								,$(divElement.btnSubmit));
					}else{
						fnBindSection();
						showDiv($(divElement.section));
					}
				}
				else if(ele == (pageContextElement.section)){
					if ($(pageContextElement.section).find('option:selected').length == 0) {
						hideDiv($(divElement.stage));
					}else{
						fnBindStage();
						showDiv($(divElement.stage));
					}
				}
				else if(ele == (pageContextElement.stage)){
					if ($(pageContextElement.stage).find('option:selected').length == 0) {
						hideDiv($(divElement.btnSubmit));
					}else{
						showDiv($(divElement.btnSubmit))
					}
				}
			}
		});		
	});
}
	var fnGetGradeBySchool=function(){
		var schoolIds=[];
		$('#selectBox_schoolsByLoc').each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					schoolIds.push( parseInt($(option).data('id')));
			});	
		});
		var jsonData={
				"schoolIds":schoolIds
		}
		var gradeVmData = customAjaxCalling("gradelist/",jsonData,"POST");

		fnBindGradeBySchoolToListBox(gradeVmData,$(pageContextElement.grade));
		setOptionsForProjectReportMultipleSelect(pageContextElement.grade);
		fnCollapseMultiselect();
}
	var fnBindGradeBySchoolToListBox=function(gradeVmData,bindToElementId){

		if(gradeVmData!=null && gradeVmData.length>0){
			$select	=bindToElementId;
			$select.html('');
			$.each(gradeVmData,function(index,obj){
				$select.append('<option value="' + obj.gradeId + '" data-id="'+ obj.gradeId + '"data-name="' + obj.gradeName + '">'+ escapeHtmlCharacters(obj.gradeName) + '</option>');
			});
		}else{
			hideDiv($(pageContextElement.subject));
			$('#mdlError #msgText').text('');
			$('#mdlError #msgText').text("There are no grade(s) for the selected school(s).");
			$('#mdlError').modal().show();
		}

}
//bind subject
	var fnGetSubjectByGrade =function(){
		fnEmptyAll(pageContextElement.subject);
		fnEmptyAll(pageContextElement.source);
		fnEmptyAll(pageContextElement.section);
		fnEmptyAll(pageContextElement.stage);
		fnBindSubjectByGradeToListBox($(pageContextElement.subject));
		setOptionsForProjectReportMultipleSelect(pageContextElement.subject);

		fnCollapseMultiselect();

}
var fnBindSubjectByGradeToListBox =function(bindToElementId){	
	var gradeIds=[];
	
	$('#selectBox_grade').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				gradeIds.push( parseInt($(option).data('id')));
		});	
	});
	var jsonData={
			"gradeIds":gradeIds
	}
	
	var subjects =customAjaxCalling("subject/grade",jsonData,'POST');	
	if(subjects!=null && subjects.length>0){
		$select	=bindToElementId;
		$select.html('');
		$.each(subjects,function(index,obj){
			$select.append('<option value="'+obj.subjectId+'" data-id="' + obj.subjectId + '"data-subjectname="' +obj.subjectName+ '">' +escapeHtmlCharacters(obj.subjectName)+ '</option>');	
			
		});
	}else{
		hideDiv($(pageContextElement.subject));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no subjects for the selected grade.");
		$('#mdlError').modal().show();
	}
};



var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}

var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');	
}

var fnBindSource = function() {	
	fnEmptyAll(pageContextElement.source);
	fnEmptyAll(pageContextElement.section);
	fnEmptyAll(pageContextElement.stage);
    var sources = customAjaxCalling("source/list",null, "GET");
	fnEmptyAll(pageContextElement.source);
	if (sources!=null&&sources != "") {
		if(sources[0]!=null && sources[0]!=''){
			if (sources[0].response == "shiksha-200") {
				fnBindSourceToListBox(pageContextElement.source,sources);
			}
		}
	}else{
		hideDiv($(pageContextElement.source));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no sources.");
		$('#mdlError').modal().show();
	}
}
var fnBindSourceToListBox = function(element, data) {
	$ele = $(element);
	$.each(data, function(index, obj) {
		$ele.append('<option value="' + obj.sourceId + '" data-id="'+ obj.sourceId + '"data-name="' + obj.sourceName + '">'+ escapeHtmlCharacters(obj.sourceName) + '</option>');
	
		
	});
	setOptionsForProjectReportMultipleSelect(pageContextElement.source);
	fnCollapseMultiselect();
}

var fnBindSection = function() {
	fnEmptyAll(pageContextElement.section);
	fnEmptyAll(pageContextElement.stage);
    var schoolIds=[];
	var gradeIds=[];
	$('#selectBox_schoolsByLoc').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				schoolIds.push( parseInt($(option).data('id')));
		});	
	});


	$('#selectBox_grade').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				gradeIds.push( parseInt($(option).data('id')));
		});	
	});
	var jsonData={
			"gradeIds":gradeIds,
			"schoolIds":schoolIds
	}

	var sectionData = customAjaxCalling("getSectionListBySchoolIdsAndGradeIds",jsonData, "POST");
	fnEmptyAll(pageContextElement.section);
	if (sectionData!=null && sectionData != "") {
		fnBindSectionToListBox(sectionData,$(pageContextElement.section));
	}else{
		hideDiv($(pageContextElement.section));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no section for the selected grade and school.");
		$('#mdlError').modal().show();
	}

}
var fnBindSectionToListBox=function(sections,bindToElementId){
	$select	=bindToElementId;
	$select.html('');
	$.each(sections,function(index,obj){
		$select.append('<option data-id="' + obj.sectionId + '"data-sectionname="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
	});
	setOptionsForProjectReportMultipleSelect(pageContextElement.section);
	fnCollapseMultiselect();
}
//bind stage
var fnBindStage = function() {
	var stages = customAjaxCalling("getStageList",null, "GET");
	fnEmptyAll(pageContextElement.stage);
	if (stages!=null && stages != "") {
		if(stages[0]!=null){
			if (stages[0].response == "shiksha-200") {
				fnBindStageToListBox(stages,pageContextElement.stage );
			}
		}
	}else{
		hideDiv($(pageContextElement.stage));
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no stage.");
		$('#mdlError').modal().show();
	}
}
var fnBindStageToListBox = function(data,element ) {
	$ele = $(element);
	$.each(data, function(index, obj) {

		$ele.append('<option value="' + obj.stageId + '" data-id="'+ obj.stageId + '"data-name="' + obj.stageName + '">'+ escapeHtmlCharacters(obj.stageName) + '</option>');
	});
	setOptionsForProjectReportMultipleSelect(pageContextElement.stage);
	fnCollapseMultiselect();
}




var genrateRow=function(){
	$('#tableDiv').css('display','block');
	$('#reportfilter').removeClass('in');
	var locationIds=[];
	var campaignStatusIds=[];
	
	$('#selectBox_villagebyRv').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				locationIds.push( parseInt($(option).data('parentid')));
		});	
		});
	$(pageContextElement.campaignStatus).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				campaignStatusIds.push( parseInt($(option).data('id')));
		});	
		});
	
	var gradeIds=[];
	
	$(pageContextElement.grade).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				gradeIds.push( parseInt($(option).data('id')));
		});	
		});
	var subjectIds=[];
	$(pageContextElement.subject).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				subjectIds.push( parseInt($(option).data('id')));
		});	
		});
	
	var sourceIds=[];
	$(pageContextElement.source).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				sourceIds.push( parseInt($(option).data('id')));
		});	
		});
	
	var schoolIds=[];
	$(pageContextElement.school).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				schoolIds.push( parseInt($(option).data('id')));
		});	
		});
	
	var stageIds=[];
	$(pageContextElement.stage).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				stageIds.push( parseInt($(option).data('id')));
		});	
		});
	
	var sectionIds=[];
	$(pageContextElement.section).each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				sectionIds.push( parseInt($(option).data('id')));
		});	
		});
	
	
	var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
	if(aLocTypeCode=="U"){
		$('#selectBox_cityByBlock').each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					locationIds.push( parseInt($(option).val()));
			});	
		});
	}else{
		$('#selectBox_villagebyRv').each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					locationIds.push( parseInt($(option).val()));
			});	
		});
	}

	var startDateData=$('#startDatePicker').datepicker('getFormattedDate');
	var endDateData=$('#endDatePicker').datepicker('getFormattedDate');
	var assessmentVm={
			"gradeIds":gradeIds,
			"subjectIds":subjectIds,
			"sourceIds":sourceIds,
			"schoolIds":schoolIds,
			"stageIds":stageIds,
			"sectionIds":sectionIds,
	};
	var jsonData={
			"locationType":aLocTypeCode,
			"campaignStartDate":startDateData,
			"campaignEndDate":endDateData,
			"campaignStatusIds":campaignStatusIds,
			"locationIds":locationIds,
			
			"assessmentVm":assessmentVm
	}
	
	var campaignlevelReportData = customAjaxCalling("report/campaignlevel",jsonData,"POST");
	createcampaignlevelReport(campaignlevelReportData);
}
var createcampaignlevelReport=function(reportData){
	deleteAllRow('campaignLevelReportTable');
	$.each(reportData, function(index,campaignReportData) {	
		var campaignData=	campaignReportData.campaignVm;
		var openLabsSchooData=campaignReportData.openLabsSchoolVm;
		var withoutOpenLabsSchoolData=campaignReportData.withoutOpenLabsSchoolVm;
		var assessmentData=campaignReportData.assessmentVm;
		var totalAssessment=campaignData.noOfAssessment!=0?campaignData.noOfAssessment:"";
		var openLabGovtSchool=openLabsSchooData.noOfGovtSchools!=0?openLabsSchooData.noOfGovtSchools:"";
		var openLabPvtSchool=openLabsSchooData.noOfPvtSchools!=0?openLabsSchooData.noOfPvtSchools:"";
		var withOutLabGovtSchool=withoutOpenLabsSchoolData.noOfGovtSchools!=0?withoutOpenLabsSchoolData.noOfGovtSchools:"";
		var withOutLabPvtSchool=withoutOpenLabsSchoolData.noOfPvtSchools!=0?withoutOpenLabsSchoolData.noOfPvtSchools:"";
		var totalGovtSchool=openLabsSchooData.noOfGovtSchools+withoutOpenLabsSchoolData.noOfGovtSchools;
		totalGovtSchool=totalGovtSchool!=0?totalGovtSchool:"";
		var totalPvtSchool=openLabsSchooData.noOfPvtSchools+withoutOpenLabsSchoolData.noOfPvtSchools;
		totalPvtSchool=totalPvtSchool!=0?totalPvtSchool:"";
		var govtOpenLabsSchoolIds=[];
		var pvtOpenLabsSchoolIds=[];
		var govtWithoutOpenLabsSchoolIds=[];
		var pvtWithoutOpenLabschoolIds=[];
		var allGovtSchoolIds=[];
		var allPvtSchools=[];
		var totalSchoolIds=[];
		var assessmentIds=[];
		assessmentIds=campaignReportData.assessmentIds;
		 govtOpenLabsSchoolIds=openLabsSchooData.govtOpenLabsSchoolIds;
		 pvtOpenLabsSchoolIds=openLabsSchooData.pvtOpenLabsSchoolIds;
		 govtWithoutOpenLabsSchoolIds=withoutOpenLabsSchoolData.govtWithoutOpenLabsSchoolIds;
		 pvtWithoutOpenLabschoolIds=withoutOpenLabsSchoolData.pvtWithoutOpenLabschoolIds;
		 allGovtSchoolIds=govtOpenLabsSchoolIds.concat(govtWithoutOpenLabsSchoolIds).sort();
		 allPvtSchools=pvtOpenLabsSchoolIds.concat(pvtWithoutOpenLabschoolIds).sort();
		 totalSchoolIds=allGovtSchoolIds.concat(allPvtSchools).sort();
		
		    console.log(totalSchoolIds);
			var g='',sub='',sch='';
			$.each(campaignReportData.gradeIds,function(index,data){			
				var x='G_'+data;
				g=g+x;	
			})
			$.each(campaignReportData.subjectIds,function(index,data){			
				var x='Sub_'+data;
				sub=sub+x;	
			})
			$.each(totalSchoolIds,function(index,data){			
				var x='Sch_'+data;
				sch=sch+x;	
			})
		 
		 
		 
		 
		 if(campaignData.status==START_STATUS ){
			spanDiv="<span class='label label-default text-uppercase'>"+campaignData.status+"</span>";
		}else if(campaignData.status==IN_PROGRESS_STATUS){
			spanDiv="<span class='label label-warning text-uppercase'>"+campaignData.status+"</span>";
		}else if(campaignData.status==COMPLETED_STATUS){
			spanDiv="<span class='label label-success text-uppercase'>"+campaignData.status+"</span>";
		}
		var row='';
		
		row='<tr title="'+campaignData.startDate+'">'+
		'<td title="'+campaignData.startDate+'" >'+campaignData.startDate+'</td>'+
		'<td title="'+campaignData.endDate+'" >'+campaignData.endDate+'</td>'+
		'<td title="'+campaignData.campaignName+'"><a  title="View Metrics" style="cursor: pointer;" onclick=fnGetProjectWiseSchoolData("'+campaignData.campaignId+'","'+g+'","'+sub+'","'+sch+'")>'+campaignData.campaignName+'</a></td>'+
		'<td title="'+campaignData.status+'" >'+spanDiv+'</td>'+
		'<td  title="'+openLabGovtSchool+'" ><a style="cursor: pointer;"  onclick="fnGetschoolDetailBySchoolIds('+govtOpenLabsSchoolIds+')">'+openLabGovtSchool+'</a></td>'+
		'<td  title="'+openLabPvtSchool+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+pvtOpenLabsSchoolIds+')">'+openLabPvtSchool+'</a></td>'+
		'<td  title="'+withOutLabGovtSchool+'" ><a style="cursor: pointer;" onClick="fnGetschoolDetailBySchoolIds('+govtWithoutOpenLabsSchoolIds+')">'+withOutLabGovtSchool+'</a></td>'+
		'<td  title="'+withOutLabPvtSchool+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+pvtWithoutOpenLabschoolIds+')">'+withOutLabPvtSchool+'</a></td>'+
		'<td title="'+totalGovtSchool+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+allGovtSchoolIds+')">'+totalGovtSchool+'</a></td>'+
		'<td title="'+totalPvtSchool+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+allPvtSchools+')">'+totalPvtSchool+'</a></td>'+
		'<td title="'+campaignData.noOfSchools+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+totalSchoolIds+')">'+campaignData.noOfSchools+'</a></td>'+
		'<td title="'+totalAssessment+'" ><a style="cursor: pointer;"  onClick="fnShowAssessmentDashboard('+campaignData.campaignId+','+assessmentIds+')">'+totalAssessment+'</a></td>'+
		
		'<td title="'+totalAssessment+'" ><a style="cursor: pointer;"  onClick="fnShowAssessmentDashboard('+campaignData.campaignId+','+assessmentIds+')">'+totalAssessment+'</a></td></tr>';
		appendNewRow('campaignLevelReportTable', row);
		
	});
}
var campaignId=0;
var schoolIds=[];
var showSchoolDashboardByCampaignIdSchoolIds=function(){
	 schoolIds=[];
	$.each( arguments, function( index, argData ) {
		if(index==0){
			campaignId=argData;
		}else{
			schoolIds.push(argData);
		}
	});
	var jsonData={
			"campaignId":campaignId,
			"schoolIds":schoolIds
	}
	if(parseInt(campaignId)!=0){
   var requestData=	customAjaxCalling("report/campaignlevel/schoolDashBoard",jsonData,"POST");
	if(requestData=="shiksha-200"){
		window.open('report/schoolDashboard/', '_blank');
		
	}
	else{
		
	}
	}
	//
}
var asmtIds=[];
var fnShowAssessmentDashboard=function(){
	asmtIds=[];
	$.each( arguments, function( index, argData ) {
		if(index==0){
			campaignId=argData;
		}else{
			asmtIds.push(argData);
		}
	});
	var jsonData={
			"campaignId":campaignId,
			"assessmentIds":asmtIds
	}
	var campaignAssessmentMapperData=customAjaxCalling("report/campaignlevel/assessmentDetails", jsonData, "POST")
	deleteAllRow('tableAssessmentDashboard');
	
	createCampaignAssessmentMapperRow(campaignAssessmentMapperData);
	$('#mdlCampaignAssessmentMapping').modal();
	if(parseInt(campaignId)!=0){
	//	window.open('assmntDashboard/'+campaignId, '_blank');
  var requestData=	customAjaxCalling("report/campaignlevel/assessmentDashBoard",jsonData,"POST");
	if(requestData=="shiksha-200"){
		window.open('assmntDashboard/'+campaignId, '_blank');
		
	}
	else{
		
	}
	}
	//
}
var createCampaignAssessmentMapperRow=function(mapperData){
	$.each(mapperData, function(index,assessment) {
		$('#mdlCampaignAssessmentMapping #campaignTitleText').text(assessment.description)
		 
		var a = '', b = '', m = [],result='';
		var chapterList= assessment.unitName;
		var temp = new Array();
		temp = chapterList.split("#,");
		var totalChapter=0;
		$.each(temp, function (index, value) {
			totalChapter=totalChapter+1;
			m[index] = '' + value + '';
		});
		result=a+m+b;
		result=result.replace(/i>,/g , "i>");
		result=result.replace(/#/g , "");
		var row='';
		row='<tr>'+
		'<td></td>'+
		'<td	data-assessmentName="'+assessment.assessmentName+'" title="'+assessment.assessmentName+'">'+assessment.assessmentName+'</td>'+
		'<td	data-gradeName="'+assessment.gradeName+'" title="'+assessment.gradeName+'">'+assessment.gradeName+'</td>'+
		'<td	data-stageName="'+assessment.stageName+'" title="'+assessment.stageName+'">'+assessment.stageName+'</td>'+
		'<td	data-subjectName="'+assessment.subjectName+'" title="'+assessment.subjectName+'">'+assessment.subjectName+'</td>'+
		'<td	data-sourceName="'+assessment.sourceName+'" title="'+assessment.sourceName+'">'+assessment.sourceName+'</td>'+
		'<td>'+totalChapter+'</td>'+
		'<td	data-unitName="'+assessment.unitName+'" title="'+assessment.unitName+'"><span title="'+result+'" class="truncate">'+result+'</span></td><tr>';
		
		appendNewRow('tableAssessmentDashboard', row);
		
		
		
	});
}

var tbPgNum=0;
var fnAppendNewRow =function(tableName,aRow){
	var oaTable =$("#"+tableName).DataTable();
    tbPgNum=oaTable.page();
    oaTable.row.add($(aRow)).draw( false );
	var currentRows = oaTable.data().toArray();  // current table data
	var newRow=currentRows.pop();
	var info = $('#'+tableName).DataTable().page.info();
	var startRow=info.start;
	currentRows.splice(startRow, 0,newRow );
	oaTable.clear();
	oaTable.rows.add(currentRows);

	oaTable.draw(false);


}
/////////////////////////////// school detail /////////////////////
var fnGetschoolDetailBySchoolIds=function(){
	$('.outer-loader').show();
	var rSchoolIds=[];
	$.each( arguments, function( index, argData ) {
		rSchoolIds.push(argData);
	});


	var jsonData={
			"schoolIds":rSchoolIds
	}
	var schoolData=customAjaxCalling("school/getSchoolDetailBySchoolIds", jsonData, "POST");
	deleteAllRow('schoolTable');
	var validSchoolData=schoolData!=null?schoolData.length>0?true:false:false;
	if(validSchoolData==true ){
		$.each(schoolData, function(index, school) {
			var isOpenLab=(school.isOpenLab==0?'No':"Yes");
			var schoolAddress = (school.address).replace(/\\n/g, ' ');
			var stateName=(school.stateName!=null?school.stateName:'');
			var zoneName= (school.zoneName!=null?school.zoneName:'');
			var districtName=( school.districtName!=null? school.districtName:'');
			var tehsilName= (school.tehsilName!=null?school.tehsilName:'');
			var cityName= (school.cityName!=null?school.cityName:'');
			var blockName= (school.blockName!=null?school.blockName:'');
			var villageName= (school.villageName!=null?school.villageName:'');
			var nearestLandMark= (school.nearestLandMark!=null?school.nearestLandMark:'');
			var distanceOfSchoolFromNearestLandmark= (school.distanceOfSchoolFromNearestLandmark!=null?school.distanceOfSchoolFromNearestLandmark:'');
			var schoolTypeName= (school.schoolTypeName!=null?school.schoolTypeName:'');
			var schoolLocationTypeName= (school.schoolLocationTypeName!=null?school.schoolLocationTypeName:'');
			var schoolName= (school.schoolName!=null?school.schoolName:'');
			var phoneNumber=( school.phoneNumber!=null? school.phoneNumber:'');
			var headTeacherName= (school.headTeacherName!=null?school.headTeacherName:'');
			var noOfStudents= (school.noOfStudents!=null || school.noOfStudents!=0 || school.noOfStudents!=undefined?school.noOfStudents:'');
			var schoolCode=(school.schoolId!=null?school.schoolId:'');
			var email=(school.email!=null?school.email:'');
			var row =
				"<tr id='"+school.id+"' data-id='"+school.id+"'>"+
				"<td data-schoolname='"+school.schoolName+"' title='"+school.schoolName+"'>"+escapeHtmlCharacters(school.schoolName)+"</td>"+
				"<td data-isopenLab='"+school.isOpenLab+"'>"+isOpenLab+"</td>"+
				"<td data-schooltypeid='"+school.schoolTypeId+"' title='"+school.schoolTypeName+"'>"+escapeHtmlCharacters(school.schoolTypeName)+"</td>"+	
				"<td data-school_loc_typeid='"+school.schoolLocationTypeId+"' title='"+schoolLocationTypeName+"'>"+escapeHtmlCharacters(schoolLocationTypeName)+"</td>"+
				"<td data-nearestlandmark='"+school.nearestLandMark+"' title='"+nearestLandMark+"'>"+escapeHtmlCharacters(nearestLandMark)+"</td>"+
				"<td data-address='"+school.address+"' title='"+school.address+"'>"+escapeHtmlCharacters(schoolAddress)+"</td>"+
				"<td data-phonenumber='"+school.phoneNumber+"' title='"+school.phoneNumber+"'>"+escapeHtmlCharacters(school.phoneNumber)+"</td>"+
				"<td data-noofstudents='"+school.noOfStudents+"' title='"+noOfStudents+"'>"+noOfStudents+"</td>"+
				"<td data-email='"+email+"'>"+email+"</td>"+
				"<td data-cityid='"+school.cityId+"' 	 title='"+cityName+"'>"+escapeHtmlCharacters(cityName)+"</td>"+
				"<td data-villageid='"+school.villageId+"'title='"+villageName+"'>"+escapeHtmlCharacters(villageName)+"</td>"+

				"</tr>";

			appendNewRow('schoolTable', row);
			
			
			
			
			//var row = getNewRow(obj);
			//fnAppendNewRow('schoolTable', row);
		});

		//fnUpdateDTColFilter('#schoolTable',[2,3,4,6,8,9,10,11,13,14,16,17,18,19,20,21],23,[0,22],thLabels);

		//$('#schoolTable').dataTable().fnDraw();
		$("#mdlSchoolDetailView").modal();
		$('.outer-loader').hide();
	}


	$('.outer-loader').hide();
}



var fnGetProjectWiseSchoolData =function(zaCampignId,grade,subject,school){

	$(".outer-loader").css('display','block');
		
	xSubjectIds =[];
	xGradeIds=[];
	xSchoolIds =[];
	xSCampignId=zaCampignId;
	$.each(grade.split('G_'),function(i,ele){if(ele!=""){xGradeIds.push(parseInt(ele))}});
	$.each(subject.split('Sub_'),function(i,ele){if(ele!=""){xSubjectIds.push(parseInt(ele))}});
	$.each(school.split('Sch_'),function(i,ele){if(ele!=""){xSchoolIds.push(parseInt(ele))}});
	var startDateData=$('#startDatePicker').datepicker('getFormattedDate');
	var endDateData=$('#endDatePicker').datepicker('getFormattedDate');
	var jsonData={
			"campaignId":zaCampignId,
			"schoolIds":xSchoolIds,
			"gradeIds":xGradeIds,
			"subjectIds":xSubjectIds,
			"startDate":startDateData,
			"endDate":endDateData
	}
	if(parseInt(zaCampignId)!=0){
     var requestData=customAjaxCalling("report/campaignlevel/schoolDashBoard",jsonData,"POST");
	if(requestData=="shiksha-200"){
		window.open('report/schoolDashboard/', '_blank');
		//location.replace('report/schoolDashboard/');
		$('.outer-loader').hide();
	}
	else{
		$('.outer-loader').hide();
	}
	$('.outer-loader').hide();
	}
	$('.outer-loader').hide();

	
	
}
//////////////////////
//Smart filter		//
//////////////////////

//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionFilterDiv =function(type){
	if(type=="R"){
		$(locationDiv.NP).insertAfter( $(locationDiv.block));
	}else{
		
	}
}
//displaying smart-filters
var displaySmartFilters =function(lCode,lLevelName){
	$(".outer-loader").show();
	elementIdMap = {
		"State" : '#statesForZone',
		"Zone" : '#selectBox_zonesByState',
		"District" : '#selectBox_districtsByZone',
		"Tehsil" : '#selectBox_tehsilsByDistrict',
		"Block" : '#selectBox_blocksByTehsil',
		"City" : '#selectBox_cityByBlock',
		"Nyaya Panchayat" : '#selectBox_npByBlock',
		"Gram Panchayat" : '#selectBox_gpByNp',
		"Revenue Village" : '#selectBox_revenueVillagebyGp',
		"Village" : '#selectBox_villagebyRv',
	};
	displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){$(".outer-loader").hide(); $('#rowDiv1,#divFilter').show(); }, 100);	
};


//reset only location when click on reset button icon
var resetLocAndSchool =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	
	reinitializeSmartFilter(elementIdMap);
	var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
	var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
	displaySmartFilters(aLocTypeCode,aLLevelName);
	
	hideDiv($(divElement.grade),$(divElement.schoolType),
			$(divElement.school),$(divElement.subject),
			$(divElement.source),$(divElement.section),
			$(divElement.stage),$(divElement.openLab),
			$(divElement.btnViewSchool),$(divElement.btnSubmit));
	$('input[name="schoolType"]').prop('checked', false);
	$('input[name="openlab"]').prop('checked', false);
	fnCollapseMultiselect();
	
	//$(pageContextElement.viewButton).prop('disabled',true);
	$(pageContextElement.resetLocButton).css('display','none');
	$('#tableDiv').css('display','none');
	deleteAllRow('campaignLevelReportTable');
}


//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
		$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
}
//////////////////////
//Smart filter ended//
//////////////////////



///////////////////////////
$(function() {
	
	 thLabelsMetrics3 =['Assessments Name','Status of Completion  ','Grade  ','Stage ','Subject ','Number of Students Enrolled ','Number of Students Appeared in Assessment ','% of Students Appeared ','Number of Students Absent for Assessment ',
         'Number of Students Yet to Take Assessment ','Number of students scored &gt;= 90% (Appeared)','% of students scored &gt;= 90% (Appeared) ', 'Number of students scored &lt; 90%  (To Undergo Post Augmentation)','%students scored &lt; 90%  (To Undergo Post Augmentation) '];
fnSetDTColFilterPaginationWithoutExportButton('#metrics3ReportTable',14,[0,1,2,3,4,5,6,7,8,9,10,11,12,13],thLabelsMetrics3);
         	
thLabelsMetrics4 =['Assessments Name','Status of Completion  ','Grade  ','Stage ','Subject ','Parameter Scoring For Assessment ','K ','U ','O ','An','Ap'];
     	fnSetDTColFilterPaginationWithoutExportButton('#metrics4ReportTable',14,[0,1,2,3,4,5,6,7,8,9,10,11,12,13],thLabelsMetrics4);
     	


	
	thLabels =['Projects Launched within Time Period','Project Name  ','Project Status  ','Schools Under Campaign ','Question papers Under Project ','Open Labs ','Theta ','Total ','Grand Total ','Start Date ','End Date ','Govt. ','Private ','Govt. ','Private ','Govt ','Private '];
	fnSetReportDTColFilterPagination('#campaignLevelReportTable',17,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		//fnHideColumns('#campaignLevelReportTable',[2,3,4,6,8,9,10,11,13,15,16,17]);
	});
	
	thLabelsAsmt =['#','Question Paper ','Grade ','Stage ','Subject ','Source','Chapter(s) '];
	fnSetAssessmentDashBoardDTColFilterPagination('#tableAssessmentDashboard',87,[0,1,2,3,4,5,6,7],thLabelsAsmt);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		//fnHideColumns('#tableAssessmentDashboard',[6,7]);
	});
//	 $('#campaignLevelReportTable').DataTable();
	
	thLabelsSchool =['# ','School Name ','Is Open Lab ','School Type ','School Location Type ','Nearest Land Mark ','Full Address ','Contact Number ','No. of Students ','Email ',,'City ','Village '];
//	fnSetDTColFilterPagination('#schoolTable',20,[2,3,4,6,8,9,10,11,12,14,10,11,12,16,17,18],thLabels);
	fnSetReportDTColFilterPagination('#schoolTable',12,[0,1,2,3,4,5,6,7,8,9,10,11],thLabelsSchool);
	
	//setDataTableColFilterPagination('#schoolTable',20,[0,19],true);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
	//	fnHideColumns('#schoolTable',[]);
	});
	
	
	
	fnInitVars();
	setOptionsForMultipleSelect(pageContextElement.campaignStatus);
	fnInitDatePicker(pageContextElement.campaignStartDate);
	fnInitDatePicker(pageContextElement.campaignEndDate);
	/*setOptionsForMultipleSelect(pageContextElement.grade);
	setOptionsForMultipleSelect(pageContextElement.school);*/
//	$(pageContextElement.viewButton).prop('disabled',true);
	$(pageContextElement.resetLocButton).css('display','none');
	

	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-minus-sign red gi-1p5x"></span></a>');
	});


	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	});

	$('#pnlMappingElements').on('show.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('Hide school configuration');
	});  
	$('#pnlMappingElements').on('hide.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('View school configuration');
	});

});

function exportMe(ele){
	
	var pName =$('#campaignTitleText').text();
	var addInfo =["Project:"+pName];
	$(ele).tableExport(
					{
						type:'excel',
						escape:'false',
						additionalInfo:addInfo
						}
				);
}






