var subElements = new Object();

var selectedIds = new Array(), selectedType = "";
var iscontainsListBox =false; //user at the time of edit modal save for any bool type...

var locationInfoData = new Object();

var maxLevelType =  "";

var elementIdMap = {
	"State" : '#statesForZone',
	"Zone" : '#selectBox_zonesByState',
	"District" : '#selectBox_districtsByZone',
	"Tehsil" : '#selectBox_tehsilsByDistrict',
	"Block" : '#selectBox_blocksByTehsil',
	"Nyaya Panchayat" : '#selectBox_npByBlock',
	"Gram Panchayat" : '#selectBox_gpByNp',
	"Revenue Village" : '#selectBox_revenueVillagebyGp',
	"Village" : '#selectBox_villagebyRv'
};


var getLocationByTypeAndId = function(id, type){
	var res_location;
	$(locationInfoData[type]).each(function(index, location){
		if(parseInt(location.id) == id){
			res_location = location;
			return false;
		}
	});
	return res_location;
};

var getLocationByName = function(name){
	return locationInfoData[name];
};

var getLocationByTypeAndParentId = function(id, type){
	var keys = Object.keys(locationInfoData);
	var locationMap = new Object();
	
	$(keys).each(function(keyIndex, key){
		$(locationInfoData[key]).each(function(index, location){
			if(parseInt(location.parentLocationid) == id && location.parentLocationType == type){
				var locType = location.locationType;
				if(locationMap[locType] == null || typeof locationMap[locType] == "undefined"){
					locationMap[locType] = new Array();
				}
				locationMap[locType].push(location);
			}
		});
	});
	return locationMap;
};

var getAllLocations = function(){	
	return locationInfoData;
};

var setMaxLevelType = function(levelType){	
	maxLevelType = levelType;
};

var getMaxLevelType = function(){
	return maxLevelType;
};

var displayLocationsOfSurvey = function(locCode,currentElement, divElement) {	
	$("#divFilter").prop("hidden", false);
	$($(divElement).children()).each(function(index, child){
		var selectContainer = $(child).find(".select");
		selectContainer.multiselect('destroy');
		selectContainer.html();
		selectContainer.empty();
		$(child).hide();
	});
	
	//var locCode =currentElement.find(":selected").data('code');	
	locationInfoData = sendGETRequest("getAllLocationsByLocTypeCode/"+locCode, "GET");
	if(locationInfoData != null && locationInfoData != ""){
		//displayLocationsUptoLevel(currentElement.find(":selected").attr("data-levelName"));
		displayLocationsUptoLevel("Village");
	}else{
		$($(divElement).children()).each(function(index, emptyChild){
			var emptyContainer = $(emptyChild).find(".select");
			emptyContainer.multiselect('refresh');	
			$(emptyChild).show();
		});
	}
	$(".outer-loader").hide();
};


var displayLocationsUptoLevel = function(levelname){
	var keys = Object.keys(locationInfoData);
	maxLevelType = levelname;
	$(keys).each(function(index, key){
		displayLocations(elementIdMap[key],locationInfoData[key]);
	});
}; 

var displayLocations = function(element, locations){
	$(element).parent().show();
	var zSortedData =fnSortSmartFilterByName(locations);
	$(zSortedData).each(function(index, location){
		$('<option value="'+location.id+'" data-parentid="'+location.parentLocationid+'" data-childtype="'+location.childLocationType+'" data-childids="'+location.childLocationid+'">').val(location.id).text(location.locationName).appendTo(element);
	});
	enableMultiSelect(element);
	
};

$(".select").change(function(){	
	selectedType = "";
	selectedIds = new Array();
	$(this).multiselect('refresh');
	if($(this).val() != null)
		selectedIds = $(this).val();
	selectedType = $(this).attr("data-locationName");
});

var biDirectionalMapping = function(loc_id, loc_type){
	if(loc_id != "multiselect-all"){// multiselect-all is input field default value
		//of "Select-all" option provided by bootstrap multiselect
		
		var locations = new Array();
		var obj = {"id":loc_id, "locationType":loc_type}; 
		locations.push(obj);
		function callBack(result){
			if(result == "Success"){
				recursiveSelection(locations);		
			}
		}
		filterParentElements(loc_id, loc_type, callBack);		
		
	}else{
		$('.outer-loader').show();
		setTimeout(function(){
			var allLocations = getAllLocations();
			var keys = Object.keys(allLocations);
			$(keys).each(function(index, key){
				selectAllSubLocations(elementIdMap[key], allLocations[key]);
			});
			$('.outer-loader').hide();
			return;
		}, 50);
	}
};

var unSelectAll = function(){
	$(".outer-loader").show();
	var allLocations = getAllLocations();
	var keys = Object.keys(allLocations);
	$(keys).each(function(index, key){
		$(allLocations[key]).each(function(index, current){
			$(elementIdMap[key]).find("option[value="+current.id+"]").prop('selected', false);
		});
	});
};

var recursiveSelection = function(subLocations){
	var subLocationMap = null, subLocs = new Array();
	$(subLocations).each(function(index, location){
		if(location.locationType == maxLevelType){
			return;
		}
		subLocationMap = getLocationByParent(parseInt(location.id), location.locationType);
		var keys = Object.keys(subLocationMap);
		var key = keys[0];
		$(subLocationMap[key]).each(function(index, current){
			$(elementIdMap[key]).find("option[value="+current.id+"]").prop('selected', true);
			//fnUpdateFvStatus($(elementIdMap[key]));
		});
		return recursiveSelection(subLocationMap[key]);
	});
};

var filterParentElements = function(loc_id, loc_type, callBack){
	var result;
	while(parseInt(loc_id) != 0){
		var location = getLocationByTypeAndId(parseInt(loc_id), loc_type);
		var element = elementIdMap[location.locationType];
		//fnUpdateFvStatus(element);
		$($(element).children()).each(function(index, current){
			if(parseInt($(current).val()) == location.id){
				$(current).prop('selected', true);
				return false;
			}
		});
		loc_id = location.parentLocationid;
		loc_type = location.parentLocationType;
		if(parseInt(loc_id) == 0){
			result = "Success";
			callBack(result);
		}
	}
};

var selectAllSubLocations = function(element, subLocs){
	$(element).multiselect('destroy');
	$(subLocs).each(function(index, current){
		$(element).find("option[value="+current.id+"]").prop('selected', true);
	});
	enableMultiSelect(element);
//	fnUpdateFvStatus(element);
	
};

var getLocationByParent = function(id, type){
	
	return getLocationByTypeAndParentId(id, type);
};


var enableMultiSelect = function(arg){

	$(arg).multiselect({
		maxHeight: 150,
		includeSelectAllOption: true,
		enableCaseInsensitiveFiltering: true,
		filterPlaceholder: 'Search here...',
		onDropdownHide: function(event) {
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
			$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
				unSelectAll();
				$(selectedIds).each(function(index, id){
					biDirectionalMapping(id, selectedType);
				});
				fnDisable();
				var ele_keys = Object.keys(elementIdMap);
				$(ele_keys).each(function(ind, ele_key){					
					$(elementIdMap[ele_key]).multiselect('destroy');
					enableMultiSelect(elementIdMap[ele_key]);
				});				
			
				$(pageContextElement.resetLocButton).css('display','block');
				if(arg=='#selectBox_villagebyRv'||arg=='selectBox_cityByBlock'){
					$(pageContextElement.viewButton).prop('disabled',false);					
				}
			    $('#divSelectOpenLab').css('display','block');
				fnCollapseMultiselect();
				$('.outer-loader').hide();
		}
	});	
};


var fnDisable =function(){
	selectedStates =[];var parentId =0;
	$(elementIdMap["State"]).find('option').each(function(i,ele){		
			if($(ele).is(':selected')){
				selectedStates.push(parseInt($(ele).val()));
			}else{
				$(ele).prop('disabled', true);
				$(ele).addClass("disabled");
			}
	});
	
	if(selectedStates.length==0){
		doResetLocation();
		return ;
	}
		
	selectedZones =[];
	selectedZones =doDisable(elementIdMap["Zone"],selectedZones,selectedStates);
	
	selectedDist =[];
	selectedDist =doDisable(elementIdMap["District"],selectedDist,selectedZones);
	
	selectedTehsil =[];
	selectedTehsil =doDisable(elementIdMap["Tehsil"],selectedTehsil,selectedDist);
		
	if(selectedLocType.toLowerCase() =='U'.toLowerCase()){
		selectedCity =[];
		selectedCity =doDisable(elementIdMap["City"],selectedCity,selectedTehsil);
	}
	
	selectedBlock =[];
	selectedBlock =doDisable(elementIdMap["Block"],selectedBlock,selectedTehsil);
	
	selectedNp =[];
	selectedNp =doDisable(elementIdMap["Nyaya Panchayat"],selectedNp,selectedBlock);
	
	selectedGp =[];
	selectedGp =doDisable(elementIdMap["Gram Panchayat"],selectedGp,selectedNp);
	
	selectedRv =[];
	selectedRv =doDisable(elementIdMap["Revenue Village"],selectedRv,selectedGp);
	
	selectedVillage =[];
	selectedVillage =doDisable(elementIdMap["Village"],selectedVillage,selectedRv);
		
}

var doDisable =function(key,selectedArray,selectedParents){
	$(key).find('option').each(function(i,ele){		
			if($(ele).is(':selected')){
				selectedArray.push(parseInt($(ele).val()));
			}
			parentId =parseInt($(ele).data('parentid'));
			if (jQuery.inArray(parentId, selectedParents)== -1) {
				$(ele).prop('disabled', true);
				$(ele).addClass("disabled");
			}else{
				$(ele).prop('disabled', false);
				$(ele).removeClass("disabled");
			}			
	});
	return selectedArray;
}


var doDestroyRefresh =function(ele){
	$(ele).multiselect('refresh');
	$(ele).multiselect('destroy');	
}
var disableNonSelected =function(){
	if($(elementIdMap["State"]).find('option:selected').length==0){
		doResetLocation();
		return false;
	}
	$(elementIdMap["State"]).find('option:not(:selected)').each(function(index,ele){		
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["State"]);
		
	
			
	
	$(elementIdMap["Zone"]).find('option:not(:selected)').each(function(index,ele){
			doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Zone"]);
	
	
	$(elementIdMap["District"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["District"]);

	
	$(elementIdMap["Tehsil"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Tehsil"]);

	$(elementIdMap["Block"]).find('option:not(:selected)').each(function(index,ele){		
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Block"]);

	
	$(elementIdMap["Nyaya Panchayat"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Nyaya Panchayat"]);

	
	$(elementIdMap["Gram Panchayat"]).find('option:not(:selected)').each(function(index,ele){	
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Gram Panchayat"]);
	
	$(elementIdMap["Revenue Village"]).find('option:not(:selected)').each(function(index,ele){
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Revenue Village"]);

	
	$(elementIdMap["Village"]).find('option:not(:selected)').each(function(index,ele){
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["Village"]);

	
	$(elementIdMap["City"]).find('option:not(:selected)').each(function(index,ele){
		doDisable(ele);
	});
	doDestroyRefresh(elementIdMap["City"]);
}
