var lObj;
var selectedLocType;
var elementIdMap;
var spanDiv;
var resetButtonObj;
var selectedIds;
var thLabels;
var thLabelsAsmt;
var thLabelsSchool;
var xSubjectIds;
var xGradeIds;
var xSchoolIds ;
var xSCampignId;

var fnInitVars =function(){
	selectedLocType ='';
}
var START_STATUS='Pending';
var IN_PROGRESS_STATUS='In progress';
var COMPLETED_STATUS='Completed';
var allInputEleNamesOfCityFilter =[];
var allInputEleNamesOfVillageFilter =[];
var allInputEleNamesOfCityFilterSchool=[];
var campaignLevelReportForm = "frmCampaignLevelReport";

allInputEleNamesOfCityFilter =['stateId','zoneId','districtId','tehsilId','cityId'];
allInputEleNamesOfVillageFilter =['stateId','zoneId','districtId','tehsilId','blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];
allInputEleNamesOfCityFilterSchool =['blockId','nyayPanchayatId','panchayatId','revenueVillageId','villageId'];


//global selectDivIds for smart filter
var locationDiv = {};	
	locationDiv.city 	="#divSelectCity";
	locationDiv.block 	="#divSelectBlock";
	locationDiv.NP 	="#divSelectNP";
	locationDiv.GP 	="#divSelectGP";
	locationDiv.RV 	="#divSelectRV";
	locationDiv.village ="#divSelectVillage";
	
	
var pageContextElement = {};
	pageContextElement.campaignStatus ="#selectBoxCampaignStatus";
	pageContextElement.campaignStartDate ="#startDatePicker";
	pageContextElement.campaignEndDate ="#endDatePicker";
	
	pageContextElement.resetLocButton ="#btnResetLocation"
	
//remove and reinitialize smart filter
var reinitializeSmartFilter =function(eleMap){
	var ele_keys = Object.keys(eleMap);
		$(ele_keys).each(function(ind, ele_key){
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(eleMap[ele_key]);
	});	
}	

//displaying smart-filters
var displaySmartFilters =function(lCode,lLevelName){
	$(".outer-loader").show();
	elementIdMap = {
		"State" : '#statesForZone',
		"Zone" : '#selectBox_zonesByState',
		"District" : '#selectBox_districtsByZone',
		"Tehsil" : '#selectBox_tehsilsByDistrict',
		"Block" : '#selectBox_blocksByTehsil',
		"City" : '#selectBox_cityByBlock',
		"Nyaya Panchayat" : '#selectBox_npByBlock',
		"Gram Panchayat" : '#selectBox_gpByNp',
		"Revenue Village" : '#selectBox_revenueVillagebyGp',
		"Village" : '#selectBox_villagebyRv',
	};
	displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));
	
	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(function(ind, ele_key){
		$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
		$(elementIdMap[ele_key]).multiselect('destroy');		
		enableMultiSelect(elementIdMap[ele_key]);
	});	
	setTimeout(function(){
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
		}, 100);	
};

//////////////////////
//Smart filter		//
//////////////////////

//arrange filter div based on location type
//if rural, hiding city and moving elements to upper 
var positionFilterDiv =function(type){
	if(type=="R"){
		$(locationDiv.NP).insertAfter( $(locationDiv.block));
	}
}
//location type on change...get locations...
$('#divLocType input:radio[name=locType]').change(function() {
		$("#tableDiv").hide();
		$('#locColspFilter').removeClass('in');
		$('#locationPnlLink').text('Select Location');		
		$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
		resetFormValidatonForLocFilters(campaignLevelReportForm,allInputEleNamesOfCityFilter);
		resetFormValidatonForLocFilters(campaignLevelReportForm,allInputEleNamesOfVillageFilter);
		
		lObj =this;
		var typeCode =$(this).data('code');
		selectedLocType =typeCode;
		$('.outer-loader').show();		
		reinitializeSmartFilter(elementIdMap);	
		$("#hrDiv").show();
		if(typeCode.toLowerCase() =='U'.toLowerCase()){
			fnUpdateFVElementStatus(campaignLevelReportForm,allInputEleNamesOfCityFilterSchool,'VALID');
			showDiv($(locationDiv.city),$(locationDiv.resetLocDivCity));
			hideDiv($(locationDiv.block),$(locationDiv.NP),$(locationDiv.GP),$(locationDiv.RV),$(locationDiv.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("U");
		}else if(typeCode.toLowerCase() =='R'.toLowerCase()){
			 $("#"+campaignLevelReportForm).data('formValidation').updateStatus('cityId', 'VALID');
			hideDiv($(locationDiv.city),$(locationDiv.resetLocDivCity));
			showDiv($(locationDiv.block),$(locationDiv.NP),$(locationDiv.GP),$(locationDiv.RV),$(locationDiv.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("R");
		}else{
			fnUpdateFVElementStatus(campaignLevelReportForm,allInputEleNamesOfCityFilterSchool,'VALID');
			 $("#"+campaignLevelReportForm).data('formValidation').updateStatus('cityId', 'VALID');
			$("#divFilter").hide();
		}
		
//		/fnCollapseMultiselect();
		
		$('.outer-loader').hide();
		return;
});


	var getSelectedData =function(elementObj,collector){
		$(elementObj).each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					collector.push( parseInt($(option).data('parentid')));
			});
		});
	}

var fnInitDatePicker =function(pDatePickerId){
	 $(pDatePickerId).datepicker({
			allowPastDates: true,
			momentConfig : {
				culture : 'en', // change to specific culture
				format : 'DD-MMM-YYYY' // change for specific format
			},
		})
}

var createcampaignlevelReport=function(reportData){
	deleteAllRow('campaignLevelReportTable');
	$.each(reportData, function(index,campaignReportData) {	
		var campaignData=	campaignReportData.campaignVm;
		var openLabsSchooData=campaignReportData.openLabsSchoolVm;
		var withoutOpenLabsSchoolData=campaignReportData.withoutOpenLabsSchoolVm;
		
	
		var openLabPvtSchool=openLabsSchooData.noOfPvtSchools!=0?openLabsSchooData.noOfPvtSchools:"";		
		var totalGovtSchool=openLabsSchooData.noOfGovtSchools+withoutOpenLabsSchoolData.noOfGovtSchools;
		var totalPvtSchool=openLabsSchooData.noOfPvtSchools+withoutOpenLabsSchoolData.noOfPvtSchools;


		var govtOpenLabsSchoolIds;
		var pvtOpenLabsSchoolIds;
		var govtWithoutOpenLabsSchoolIds;
		var pvtWithoutOpenLabschoolIds;
		var allGovtSchoolIds;
		var allPvtSchools;
		var totalSchoolIds;
		var assessmentIds;
		assessmentIds=campaignReportData.assessmentIds;
		govtOpenLabsSchoolIds=openLabsSchooData.govtOpenLabsSchoolIds;
		pvtOpenLabsSchoolIds=openLabsSchooData.pvtOpenLabsSchoolIds;
		govtWithoutOpenLabsSchoolIds=withoutOpenLabsSchoolData.govtWithoutOpenLabsSchoolIds;
		pvtWithoutOpenLabschoolIds=withoutOpenLabsSchoolData.pvtWithoutOpenLabschoolIds;
		allGovtSchoolIds=govtOpenLabsSchoolIds.concat(govtWithoutOpenLabsSchoolIds).sort();
		allPvtSchools=pvtOpenLabsSchoolIds.concat(pvtWithoutOpenLabschoolIds).sort();
		totalSchoolIds=allGovtSchoolIds.concat(allPvtSchools).sort();

		var openLabGovtSchoolSpan;
		var  openLabPvtSchoolSpan;
		var  withOutLabGovtSchoolSpan;
		var withOutLabPvtSchoolSpan;
		var totalGovtSchoolSpan;
		var totalPvtSchoolSpan;
		var totalAssessmentSpan;
		var totalSchoolSpan;
		// no of open lab govt school column
		if(openLabsSchooData.noOfGovtSchools!=0){
			openLabGovtSchoolSpan='<td  title="'+openLabsSchooData.noOfGovtSchools+'" ><a style="cursor: pointer;"  onclick="fnGetschoolDetailBySchoolIds('+govtOpenLabsSchoolIds+')">'+openLabsSchooData.noOfGovtSchools+'</a></td>';
		}else{
			openLabGovtSchoolSpan='<td  title="'+openLabsSchooData.noOfGovtSchools+'">'+openLabsSchooData.noOfGovtSchools+'</td>';
		}
		// no of open lab pvt school column
		if(openLabsSchooData.noOfPvtSchools!=0){
			openLabPvtSchoolSpan= '<td  title="'+openLabPvtSchool+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+pvtOpenLabsSchoolIds+')">'+openLabsSchooData.noOfPvtSchools+'</a></td>';
		}else{
			openLabPvtSchoolSpan= '<td  title="'+openLabPvtSchool+'" >'+openLabsSchooData.noOfPvtSchools+'</td>';
		}
		// no of assessment column
		if(campaignData.noOfAssessment!=0){
			totalAssessmentSpan= '<td title="'+campaignData.noOfAssessment+'" ><a style="cursor: pointer;"  onClick="fnShowAssessmentDashboard('+campaignData.campaignId+','+assessmentIds+')">'+campaignData.noOfAssessment+'</a></td></tr>';
		}else{
			totalAssessmentSpan= '<td title="'+campaignData.noOfAssessment+'" >'+campaignData.noOfAssessment+'</td></tr>';
		}
		// no of with out open lab govt school column
		if(withoutOpenLabsSchoolData.noOfGovtSchools!=0){
			withOutLabGovtSchoolSpan= '<td  title="'+withoutOpenLabsSchoolData.noOfGovtSchools+'" ><a style="cursor: pointer;" onClick="fnGetschoolDetailBySchoolIds('+govtWithoutOpenLabsSchoolIds+')">'+withoutOpenLabsSchoolData.noOfGovtSchools+'</a></td>';
		}else{
			withOutLabGovtSchoolSpan= '<td  title="'+withoutOpenLabsSchoolData.noOfGovtSchools+'" >'+withoutOpenLabsSchoolData.noOfGovtSchools+'</td>';
		}
		// no of with out open lab pvt school column
		if(withoutOpenLabsSchoolData.noOfPvtSchools!=0){
			withOutLabPvtSchoolSpan='<td  title="'+withoutOpenLabsSchoolData.noOfPvtSchools+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+pvtWithoutOpenLabschoolIds+')">'+withoutOpenLabsSchoolData.noOfPvtSchools+'</a></td>';
		}else{
			withOutLabPvtSchoolSpan='<td  title="'+withoutOpenLabsSchoolData.noOfPvtSchools+'" >'+withoutOpenLabsSchoolData.noOfPvtSchools+'</td>';
		}
		//  total govt school column
		if(totalGovtSchool!=0){
			totalGovtSchoolSpan='<td title="'+totalGovtSchool+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+allGovtSchoolIds+')">'+totalGovtSchool+'</a></td>';
		}
		else{
			totalGovtSchoolSpan='<td title="'+totalGovtSchool+'" >'+totalGovtSchool+'</td>';
		}
		//  total pvt school column
		if(totalPvtSchool!=0){
			totalPvtSchoolSpan=  '<td title="'+totalGovtSchool+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+allPvtSchools+')">'+totalPvtSchool+'</a></td>';
		}
		else{
			totalPvtSchoolSpan=  '<td title="'+totalGovtSchool+'" >'+totalPvtSchool+'</td>';
		}
		// all school
		if(campaignData.noOfSchools!=0)
		{
			totalSchoolSpan= '<td title="'+campaignData.noOfSchools+'" ><a style="cursor: pointer;"  onClick="fnGetschoolDetailBySchoolIds('+totalSchoolIds+')">'+campaignData.noOfSchools+'</a></td>';
		}else{
			totalSchoolSpan= '<td title="'+campaignData.noOfSchools+'" >'+campaignData.noOfSchools+'</td>';
		}

		if(campaignData.status==START_STATUS ){
			spanDiv="<span class='label label-default text-uppercase'>"+campaignData.status+"</span>";
		}else if(campaignData.status==IN_PROGRESS_STATUS){
			spanDiv="<span class='label label-warning text-uppercase'>"+campaignData.status+"</span>";
		}else if(campaignData.status==COMPLETED_STATUS){
			spanDiv="<span class='label label-success text-uppercase'>"+campaignData.status+"</span>";
		}
		var row;
		
		var g='',sub='',sch='';
		$.each(campaignReportData.gradeIds,function(index,data){			
			var x='G_'+data;
			g=g+x;	
		})
		$.each(campaignReportData.subjectIds,function(index,data){			
			var x='Sub_'+data;
			sub=sub+x;	
		})
		$.each(totalSchoolIds,function(index,data){			
			var x='Sch_'+data;
			sch=sch+x;	
		})
		
		row='<tr title="'+campaignData.startDate+'"  data-gradeid="'+campaignReportData.gradeIds+'">'+
		'<td title="'+campaignData.startDate+'" >'+campaignData.startDate+'</td>'+
		'<td title="'+campaignData.endDate+'" >'+campaignData.endDate+'</td>'+
		//'<td title="'+campaignData.campaignName+'"><a  title="View Metrics" style="cursor: pointer;" onclick=fnGetProjectLevelMetricData("'+campaignData.campaignId+'","'+g+'","'+sub+'","'+sch+'")>'+campaignData.campaignName+'</a></td>'+
		'<td title="'+campaignData.campaignName+'"><a  title="View Metrics" style="cursor: pointer;" onclick=fnGetProjectWiseSchoolData("'+campaignData.campaignId+'","'+g+'","'+sub+'","'+sch+'")>'+campaignData.campaignName+'</a></td>'+
		
		'<td title="'+campaignData.status+'" >'+spanDiv+'</td>'+
		openLabGovtSchoolSpan+openLabPvtSchoolSpan+withOutLabGovtSchoolSpan+
		withOutLabPvtSchoolSpan+totalGovtSchoolSpan+totalPvtSchoolSpan+
		totalSchoolSpan+totalAssessmentSpan;
		appendNewRow('campaignLevelReportTable', row);

	});
	$('.outer-loader').hide();
	
}

var fnGenrateCampaignLevelReport=function(){

	$('#tableDiv').css('display','block');
	$('#reportfilter').removeClass('in');
	var locationIds=[];
	var campaignStatusIds=[];
	var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
	if(aLocTypeCode=="U"){
		$('#selectBox_cityByBlock').each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					locationIds.push( parseInt($(option).val()));
			});	
		});
	}else{
		$('#selectBox_villagebyRv').each(function(index,ele){
			$(ele).find("option:selected").each(function(ind,option){
				if($(option).val()!="multiselect-all")
					locationIds.push( parseInt($(option).val()));
			});	
		});
	}

	$('#selectBoxCampaignStatus').each(function(index,ele){
		$(ele).find("option:selected").each(function(ind,option){
			if($(option).val()!="multiselect-all")
				campaignStatusIds.push( parseInt($(option).data('id')));
		});	
		});
	
	var startDateData=$('#startDatePicker').datepicker('getFormattedDate');
	var endDateData=$('#endDatePicker').datepicker('getFormattedDate');
	var jsonData={
			"locationType":aLocTypeCode,
			"campaignStartDate":startDateData,
			"campaignEndDate":endDateData,
			"campaignStatusIds":campaignStatusIds,
			"locationIds":locationIds
	}
	
	var campaignlevelReportData = customAjaxCalling("report/campaignlevel",jsonData,"POST");
	createcampaignlevelReport(campaignlevelReportData);

}

var genrateRow=function(){
	$('.outer-loader').show();
	$.wait(30).then(fnGenrateCampaignLevelReport);
}


var campaignId=0;
var schoolIds=[];
var showSchoolDashboardByCampaignIdSchoolIds=function(){
	$('.outer-loader').show();
	 schoolIds=[];
	$.each( arguments, function( index, argData ) {
		if(index==0){
			campaignId=argData;
		}else{
			schoolIds.push(argData);
		}
	});
	var jsonData={
			"campaignId":campaignId,
			"schoolIds":schoolIds
	}
	if(parseInt(campaignId)!=0){
   var requestData=	customAjaxCalling("report/campaignlevel/schoolDashBoard",jsonData,"POST");
	if(requestData=="shiksha-200"){
		window.open('report/schoolDashboard/', '_blank');
		$('.outer-loader').hide();
	}
	else{
		$('.outer-loader').hide();
	}
	$('.outer-loader').hide();
	}
	$('.outer-loader').hide();
}
var createCampaignAssessmentMapperRow=function(mapperData){
	$.each(mapperData, function(index,assessment) {
		$('#mdlCampaignAssessmentMapping #campaignTitleText').text(assessment.description)
		 
		var a = '', b = '', m = [],result;
		var chapterList= assessment.unitName;
		var temp ;
		temp = chapterList.split("#,");
		var totalChapter=0;
		$.each(temp, function (index, value) {
			totalChapter=totalChapter+1;
			m[index] = '' + value + '';
		});
		result=a+m+b;
		result=result.replace(/i>,/g , "i>");
		result=result.replace(/#/g , "");
		var row;
		row='<tr>'+
		'<td></td>'+
		'<td	data-assessmentName="'+assessment.assessmentName+'" title="'+assessment.assessmentName+'">'+assessment.assessmentName+'</td>'+
		'<td	data-gradeName="'+assessment.gradeName+'" title="'+assessment.gradeName+'">'+assessment.gradeName+'</td>'+
		'<td	data-stageName="'+assessment.stageName+'" title="'+assessment.stageName+'">'+assessment.stageName+'</td>'+
		'<td	data-subjectName="'+assessment.subjectName+'" title="'+assessment.subjectName+'">'+assessment.subjectName+'</td>'+
		'<td	data-sourceName="'+assessment.sourceName+'" title="'+assessment.sourceName+'">'+assessment.sourceName+'</td>'+
		'<td>'+totalChapter+'</td>'+
		'<td	data-unitName="'+assessment.unitName+'" title="'+assessment.unitName+'"><span title="'+result+'" class="truncate">'+result+'</span></td><tr>';
		
		appendNewRow('tableAssessmentDashboard', row);
		
		
		
	});
}
var asmtIds=[];
var fnShowAssessmentDashboard=function(){
	asmtIds=[];
	$.each( arguments, function( index, argData ) {
		if(index==0){
			campaignId=argData;
		}else{
			asmtIds.push(argData);
		}
	});
	var jsonData={
			"campaignId":campaignId,
			"assessmentIds":asmtIds
	}
	var campaignAssessmentMapperData=customAjaxCalling("report/campaignlevel/assessmentDetails", jsonData, "POST")
	deleteAllRow('tableAssessmentDashboard');
	
	createCampaignAssessmentMapperRow(campaignAssessmentMapperData);
	$('#mdlCampaignAssessmentMapping').modal();
	
}





//reset only location when click on reset button icon
var resetLocAndSchool =function(obj){	
	$("#mdlResetLoc").modal().show();
	$("#tableDiv").hide();
	resetButtonObj =obj;
	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	$("#tableDiv").hide();
	selectedIds=[];
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	resetFormValidatonForLocFilters(campaignLevelReportForm,allInputEleNamesOfCityFilter);
	resetFormValidatonForLocFilters(campaignLevelReportForm,allInputEleNamesOfVillageFilter);
	reinitializeSmartFilter(elementIdMap);
	var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
	var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
	displaySmartFilters(aLocTypeCode,aLLevelName);
	
	if (aLocTypeCode != "" && aLocTypeCode != null) {
		if (aLocTypeCode.toLowerCase() == 'U'.toLowerCase()) {
			
			fnUpdateFVElementStatus(campaignLevelReportForm,allInputEleNamesOfCityFilterSchool,'VALID');

		} else if (aLocTypeCode.toLowerCase() == 'R'.toLowerCase()){
			
			 $("#"+campaignLevelReportForm).data('formValidation').updateStatus('cityId', 'VALID');
		}
	}
	fnCollapseMultiselect();
	
	
	$(pageContextElement.resetLocButton).css('display','none');
	$('#tableDiv').css('display','none');
	deleteAllRow('campaignLevelReportTable');
}



//////////////////////
//Smart filter ended//
//////////////////////
var fnDateParsing=function(data){
	var sArray =data.split('-');
	var monthData =sArray[1];
	var dateData =sArray[0];
	var yearData =sArray[2];
	var dateStringData =Date.parse(monthData+' '+dateData+' ,'+yearData);
	return dateStringData;
}

var isValidEndDate=function(){

	var startDateData=fnDateParsing($('#startDatePicker').datepicker('getFormattedDate'));
	var endDateData=fnDateParsing($('#endDatePicker').datepicker('getFormattedDate'));
	if (endDateData<startDateData) {
		return false;
	} 
	
	return true;
	

}





////////////////metrices /////////////



var metric = {};

metric.modal ="#mdlprojectLevelMetrics";


var tabPan=function(tabId,tabPanelId,mdlId){
	$(mdlId+ " .active").each(function(index,ele){
		$(ele).removeClass('active');
	});
	$(tabId).addClass('active');
	$(tabPanelId).addClass('active');
	$(tabId).trigger('click');
	
	$(metric.modal).modal().show();
	
	
}

var fnGetProjectWiseSchoolData =function(zaCampignId,grade,subject,school){

	$(".outer-loader").css('display','block');
		
	xSubjectIds =[];
	xGradeIds=[];
	xSchoolIds =[];
	xSCampignId=zaCampignId;
	$.each(grade.split('G_'),function(i,ele){
		if(ele!=""){
		xGradeIds.push(parseInt(ele))
			}
		});
	$.each(subject.split('Sub_'),function(i,ele){
		if(ele!=""){
			xSubjectIds.push(parseInt(ele))
			}
		});
	$.each(school.split('Sch_'),function(i,ele){
		if(ele!=""){
			xSchoolIds.push(parseInt(ele))
			}
		});
	var startDateData=$('#startDatePicker').datepicker('getFormattedDate');
	var endDateData=$('#endDatePicker').datepicker('getFormattedDate');
	var jsonData={
			"campaignId":zaCampignId,
			"schoolIds":xSchoolIds,
			"gradeIds":xGradeIds,
			"subjectIds":xSubjectIds,
			"startDate":startDateData,
			"endDate":endDateData
	}
	if(parseInt(zaCampignId)!=0){
     var requestData=customAjaxCalling("report/campaignlevel/schoolDashBoard",jsonData,"POST");
	if(requestData=="shiksha-200"){
		window.open('report/schoolDashboard/', '_blank');
		
		$('.outer-loader').hide();
	}
	else{
		$('.outer-loader').hide();
	}
	$('.outer-loader').hide();
	}
	$('.outer-loader').hide();

	
	
}


var fnGetProjectLevelMetricData =function(zaCampignId,grade,subject,school){

	$(".outer-loader").css('display','block');
		
	xSubjectIds =[];
	xGradeIds=[];
	xSchoolIds =[];
	xSCampignId=zaCampignId;
	$.each(grade.split('G_'),function(i,ele){
		if(ele!=""){
			xGradeIds.push(parseInt(ele))
			}
		});
	$.each(subject.split('Sub_'),function(i,ele){
		if(ele!=""){
			xSubjectIds.push(parseInt(ele))
			}
		});
	$.each(school.split('Sch_'),function(i,ele){
		if(ele!=""){
			xSchoolIds.push(parseInt(ele))
			}
		});
	$.wait(100).then(fnShowContainer);			
	
	tabPan('#metricPerformance','#aPerformance','#mdlprojectLevelMetrics');
	
	
}

var tbPgNum=0;
var fnAppendNewRow =function(tableName,aRow){
	var oaTable =$("#"+tableName).DataTable();
    tbPgNum=oaTable.page();
    oaTable.row.add($(aRow)).draw( false );
	var currentRows = oaTable.data().toArray();  // current table data
	var newRow=currentRows.pop();
	var info = $('#'+tableName).DataTable().page.info();
	var startRow=info.start;
	currentRows.splice(startRow, 0,newRow );
	oaTable.clear();
	oaTable.rows.add(currentRows);

	oaTable.draw(false);


}
var fnGetschoolDetailBySchoolIds=function(){
	$('.outer-loader').show();
	var rSchoolIds=[];
	$.each( arguments, function( index, argData ) {
		rSchoolIds.push(argData);
	});


	var jsonData={
			"schoolIds":rSchoolIds
	}
	var schoolData=customAjaxCalling("school/getSchoolDetailBySchoolIds", jsonData, "POST");
	deleteAllRow('schoolTable');
	var validSchoolData=schoolData!=null?schoolData.length>0?true:false:false;
	if(validSchoolData){
		$.each(schoolData, function(index, school) {
			var isOpenLab=(school.isOpenLab==0?'No':"Yes");
			var schoolAddress = (school.address).replace(/\\n/g, ' ');	
			
			
			var cityName= (school.cityName!=null?school.cityName:'');			
			var villageName= (school.villageName!=null?school.villageName:'');
			var nearestLandMark= (school.nearestLandMark!=null?school.nearestLandMark:'');		
			
			var schoolLocationTypeName= (school.schoolLocationTypeName!=null?school.schoolLocationTypeName:'');			
			
			
			var noOfStudents= (school.noOfStudents!=null || school.noOfStudents!=0 || school.noOfStudents!=undefined?school.noOfStudents:'');
			
			var email=(school.email!=null?school.email:'');
			var row =
				"<tr id='"+school.id+"' data-id='"+school.id+"'>"+
				"<td data-schoolname='"+school.schoolName+"' title='"+school.schoolName+"'>"+escapeHtmlCharacters(school.schoolName)+"</td>"+
				"<td data-isopenLab='"+school.isOpenLab+"'>"+isOpenLab+"</td>"+
				"<td data-schooltypeid='"+school.schoolTypeId+"' title='"+school.schoolTypeName+"'>"+escapeHtmlCharacters(school.schoolTypeName)+"</td>"+	
				"<td data-school_loc_typeid='"+school.schoolLocationTypeId+"' title='"+schoolLocationTypeName+"'>"+escapeHtmlCharacters(schoolLocationTypeName)+"</td>"+
				"<td data-nearestlandmark='"+school.nearestLandMark+"' title='"+nearestLandMark+"'>"+escapeHtmlCharacters(nearestLandMark)+"</td>"+
				"<td data-address='"+school.address+"' title='"+school.address+"'>"+escapeHtmlCharacters(schoolAddress)+"</td>"+
				"<td data-phonenumber='"+school.phoneNumber+"' title='"+school.phoneNumber+"'>"+escapeHtmlCharacters(school.phoneNumber)+"</td>"+
				"<td data-noofstudents='"+school.noOfStudents+"' title='"+noOfStudents+"'>"+noOfStudents+"</td>"+
				"<td data-email='"+email+"'>"+email+"</td>"+
				"<td data-cityid='"+school.cityId+"' 	 title='"+cityName+"'>"+escapeHtmlCharacters(cityName)+"</td>"+
				"<td data-villageid='"+school.villageId+"'title='"+villageName+"'>"+escapeHtmlCharacters(villageName)+"</td>"+

				"</tr>";

			appendNewRow('schoolTable', row);
			
			
			
			
			
		});

		
		$("#mdlSchoolDetailView").modal();
		$('.outer-loader').hide();
	}


	$('.outer-loader').hide();
}


//Location type Both 
var fnHidLocFilter =function(){
	$("#divFilter").hide();
}









///////////////////////////
$(function() {
	
	
	
	thLabels =['Projects Launched within Time Period','Project Name  ','Project Status  ','Schools Under Project ','Question papers Under Project ','Open Labs ','Theta ','Total ','Grand Total ','Start Date ','End Date ','Govt. ','Private ','Govt. ','Private ','Govt ','Private '];
	fnSetReportDTColFilterPagination('#campaignLevelReportTable',17,[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		
	});
	
	thLabelsAsmt =['#','Question Paper ','Grade ','Stage ','Subject ','Source','Chapter(s) '];
	fnSetAssessmentDashBoardDTColFilterPagination('#tableAssessmentDashboard',8,[0,1,2,3,4,5,6,7],thLabelsAsmt);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		
	});

	
	
	thLabelsSchool =['# ','School Name ','Is Open Lab ','School Type ','School Location Type ','Nearest Land Mark ','Full Address ','Contact Number ','No. of Students ','Email ',,'City ','Village '];

	fnSetReportDTColFilterPagination('#schoolTable',12,[0,1,2,3,4,5,6,7,8,9,10,11],thLabelsSchool);
	
	
	fnMultipleSelAndToogleDTCol('.search_init',function(){
	
	});
	
	
	
	
	
	fnInitVars();
	setOptionsForMultipleSelect(pageContextElement.campaignStatus);
	fnInitDatePicker(pageContextElement.campaignStartDate);
	fnInitDatePicker(pageContextElement.campaignEndDate);

	$(pageContextElement.resetLocButton).css('display','none');
	

	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-minus-sign red gi-1p5x"></span></a>');
	});


	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	});

	$('#pnlMappingElements').on('show.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('Hide school configuration');
	});  
	$('#pnlMappingElements').on('hide.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('View school configuration');
	});
//enable submit button after project status selection
	$("#selectBoxCampaignStatus").on("change",function(){
		$("#btnViewReport").prop("disabled",false);
		$("#btnViewReport").removeClass("disabled"); 
	});
	//////////////////

	$('#frmCampaignLevelReport').formValidation({
		framework: 'bootstrap',
        icon: {
            valid: 'glyphicon glyphicon-ok',
            //invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        // This option will not ignore invisible fields which belong to inactive panels
        excluded: false,
        fields : {        
			 startDate: {
           	 validators: {
                    notEmpty: {
                        message: ' '
                    },
                }
           },
           endDate: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },
           cityId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },
           villageId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },

           revenueVillageId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },

           panchayatId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },

           nyayPanchayatId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },

           blockId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },

           tehsilId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },
           districtId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           }, zoneId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           }, stateId: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },
           locType: {
               validators: {
                   notEmpty: {
                       message: ' '
                   },
               }
           },
		}     
    }).on('changed.fu.datepicker dateClicked.fu.datepicker',function() {

		var startDate = $('#startDatePicker').datepicker('getFormattedDate');
		var endDate = $('#endDatePicker').datepicker('getFormattedDate');
		var isValidDate=isValidEndDate();
		
		if (startDate != "Invalid Date") {
			$("#frmCampaignLevelReport").data('formValidation').updateStatus('startDate', 'VALID');
			$('#endDateMsg').css('display','none');
			$('#btnViewReport').prop('disabled',false);
			$('#btnViewReport').removeClass('disabled');
			
		}
		if (endDate != "Invalid Date") {
				if(isValidDate){
					$('#endDateMsg').css('display','none');
					$('#btnViewReport').prop('disabled',false);
					$('#btnViewReport').removeClass('disabled');
					$("#frmCampaignLevelReport").data('formValidation').updateStatus('endDate', 'VALID');
				}else{
					$('#btnViewReport').prop('disabled',true);
					$('#endDateMsg').css('display','block');
					$("#frmCampaignLevelReport").data('formValidation').updateStatus('endDate', 'INVALID');
				}
			}
		
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		genrateRow();
	
});
});
function exportMe(ele){
	
	var pName =$('#campaignTitleText').text();
	var addInfo =["Project:"+pName];
	$(ele).tableExport(
					{
						type:'excel',
						escape:'false',
						additionalInfo:addInfo
						}
				);
}