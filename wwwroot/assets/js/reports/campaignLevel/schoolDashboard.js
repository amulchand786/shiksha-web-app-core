
$("#projectLabelBackButton").on('click',function(){
	location.replace('report/campaignlevel/');
});

var fnShowschoolwiseAssessmentScreen=function(){
	spinner.showSpinner();
	location.replace('schoolwiseAssessmentDetail');
	window.location =baseContextPath+"/report/schoolDashBoard/schoolwiseAssessmentDetail";
	//window.location =location.pathname+'schoolwiseAssessmentDetail';
}

var fnShowSchoolWiseAssessmentDetail=function(schoolId){
		var schoolIds=[];
		schoolIds.push(schoolId);
		var jsonData={
				"campaignId":campaignId,
				"schoolIds":schoolIds
		}
			
		if(parseInt(campaignId)!=0){
			   var requestData=	customAjaxCalling("report/schoolDashBoard/setSchoolData",jsonData,"POST");
				if(requestData=="shiksha-200"){
					window.open('report/schoolDashBoard/schoolwiseAssessmentDetail/', '_blank');
					
				}
				else{
					
				}
				}	
		//window.open('report/schoolDashboard/', '_blank');
	}
var mSchoolId=0;
$('input:radio[name="school"]').change(function() {
   mSchoolId=$(this).data('id');
   $("#viewSchoolwiseAssessmentDetailButton").prop("disabled",false);
   $("#viewMetricsButton").prop("disabled",false);
   
   
	});
var customAjaxCallingWithOutSpinner = function (URL, jsonData, requestType) {

   var msg = null;
	$.ajax({
		type: requestType,
		url: URL,
		contentType: "application/json",
		async: false,
		data: JSON.stringify(jsonData),
		success: function (data) {
			maxInactiveUserInterval=sessionTimeOutValue;
			msg = data;

		},
		error: function (data) {
			msg = data;
			networkstatus();
		}
	});
	return msg;
};
var fnViewSchoolWiseAssessmentDetail=function(){
	spinner.showSpinner();
	var schoolIds=[];
	schoolIds.push(mSchoolId);
	var jsonData={
			"campaignId":campaignId,
			"schoolIds":schoolIds
	}
		
	if(parseInt(campaignId)!=0){
		var requestData=	customAjaxCallingWithOutSpinner("report/schoolDashBoard/setSchoolData",jsonData,"POST");
		if(requestData=="shiksha-200"){
			fnShowschoolwiseAssessmentScreen();
		}
	}	
	//window.open('report/schoolDashboard/', '_blank');
}
/*function searchTable(inputVal,tableId)
{
	var table = $('#'+tableId);
	table.find('tr').each(function(index, row)
	{
		var allCells = $(row).find('td');
		if(allCells.length> 0)
		{
			var found = false;
			allCells.each(function(index, td)
			{
				var regExp = new RegExp(inputVal, 'i');
				if(regExp.test($(td).text()))
				{
					found = true;
					return false;
				}
			});
			if(found == true)$(row).show();else $(row).hide();
		}
	});
}*/

var metric = new Object();

metric.modal ="#mdlprojectLevelMetrics";


var tabPan=function(tabId,tabPanelId,mdlId){
	$(mdlId+ " .active").each(function(index,ele){
		$(ele).removeClass('active');
	});
	$(tabId).addClass('active');
	$(tabPanelId).addClass('active');
	$(tabId).trigger('click');
	
	$(metric.modal).modal().show();
	//$(".outer-loader").css('display','none');
	
}

var fnGetProjectLevelMetricData =function(){

	spinner.showSpinner();
	$('#mdlprojectLevelMetrics').find(".modal-body").scrollTop(0);
	xSubjectIds =uSubjectIds;
	xGradeIds=uGradeIds;
	xSchoolIds =[];
	xSCampignId=campaignId;
	xSchoolIds.push(mSchoolId);
	$.wait(100).then(prepareGraphData).then(bindAssessmentForGraph).then(fnShowContainer);			
	
	tabPan('#metricPerformance','#aPerformance','#mdlprojectLevelMetrics');
	
	
}
var prepareGraphData=function(){
	var fromDate=moment(startDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
	var toDate=moment(endDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
	var jsonData={
			"schoolIds":xSchoolIds,
			"fromDate":fromDate,
			"toDate":toDate,
			"gradeIds":xGradeIds,
			"subjectIds":xSubjectIds,
			"projectId":xSCampignId
	}
	if(xSchoolIds.length>0 && xGradeIds.length>0 && xSubjectIds.length>0){
		var metricsGraphData=customAjaxCalling("metricDashBoard/getAssessmentWiseDetailForMetricReport", jsonData, "POST");

		var result=metricsGraphData!=null?metricsGraphData.length>0?true:false:false;
		if(result==true){
			defaultGraphData=[];
			graphData=[];
			cAssessmentData=[];
			graphData=metricsGraphData;
			defaultGraphData=metricsGraphData;
			$.each(metricsGraphData, function(index,ametricsReportData) {
				var asmentName=ametricsReportData.assessmentName+"_"+ametricsReportData.campaignName;
				var pid=ametricsReportData.assessmentPlannerId;
				var assessmentMapData={
						"assessmentName":asmentName,
						"assessmentPlannerId":pid
				}

				////////////// prepare map for graph///////////
				aGraph[pid]=ametricsReportData;		

				cAssessmentData.push(assessmentMapData);
			});
		}
	}
}
var bindAssessmentForGraph=function(){
	var bindToElementDropId = $('#selectBox_zAssessment');
	$(bindToElementDropId).multiselect('destroy');
	$select	=bindToElementDropId;
	$select.html('');
	$.each(cAssessmentData,function(index,obj){
		$select.append('<option value="' + obj.assessmentPlannerId + '" selected="selected" data-id="' + obj.assessmentPlannerId + '"data-name="' + obj.assessmentName+ '">' +obj.assessmentName+ '</option>');

	});
	
	setOptionForGraphAssessment(bindToElementDropId);
}
var planIds=[];

var fnShowAssessmentWiseGraph=function(){
	graphData=[];
	$.each(planIds,function(index,objData){
		var cAssessmentPlanId=objData;
		var gObj=	aGraph.hasOwnProperty(cAssessmentPlanId)==true?aGraph[cAssessmentPlanId]:"";
		if(gObj!=""){
			graphData.push(gObj);
		}	
	});
	if(graphData.length>0){
		$(".outer-loader").show();
		$.wait(50).then(studentPerformanceAssessmentWiseInIt).then(showGraphGenderWiseForRCFS);
		$(".outer-loader").hide();

	}

}
var showGraphGenderWiseForRCFS=function(){
	if(superRoleCode=="FS" || superRoleCode=="RC"){
	//$.wait(5).then(studentPerformanceGenderWiseTaxonomyInIt).then(studentPerformanceGenderWiseInIt);
	}
	$(".outer-loader").hide();
}
var fnPrepareAssessmentWiseGraphDataOnModalClose=function(){
	graphData=[];
	$.each(cAssessmentData,function(index,objData){
		var cAssessmentPlanId=objData.assessmentPlannerId;
		var gObj=	aGraph.hasOwnProperty(cAssessmentPlanId)==true?aGraph[cAssessmentPlanId]:"";
		if(gObj!=""){
			graphData.push(gObj);
		}	
	});
	

}
var setOptionForGraphAssessment=function(){
	
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 180,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			onDropdownHide: function(event) {
				planIds=[];
				$('#selectBox_zAssessment').each(function(index,ele){
					$(ele).find("option:selected").each(function(ind,option){
						if($(option).val()!="multiselect-all")
							planIds.push( parseInt($(option).data('id')));
					});	
				});
				
				$("#graphDataDiv").hide();
			} 
		});

	});
	fnCollapseMultiselect();


	}

$(function() {
	$("#frmGraphMetrics") .formValidation({
		excluded : ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields : {
		 cAssessment: {
			validators: {
				callback: {
					message: '      ',
					callback: function(value, validator, $field) {
						// Get the selected options
						var options =$('#selectBox_zAssessment').find('option:selected').val();
						
						return (options !=null && options !=undefined);
					}
				}
			}
		}
		}
	
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		fnShowAssessmentWiseGraph(e);
	
});
	
	$('#mdlprojectLevelMetrics').on('hide.bs.modal', function (e) {
	    $(this).find(".modal-body").scrollTop(0);
	});
	
});

