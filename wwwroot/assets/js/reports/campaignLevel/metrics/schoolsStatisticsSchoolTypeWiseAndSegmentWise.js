
//////////////////////// Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";
var selectSchoolMessage="Please select atleast one School. ";
var percentageSign="";


var schoolsStatisticsSchoolTypeWiseAndSegmentWise= new Object();

schoolsStatisticsSchoolTypeWiseAndSegmentWise.chartTypeDiv="#chartType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWiseDiv";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.chartType="#chartType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWise";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.dataTypeDiv="#dataType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWiseDiv";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.dataType="#dataType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWise";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasDiv="#canvasSchoolsStatisticsSchoolTypeWiseAndSegmentWiseDiv";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvas="#chart-schoolsStatisticsSchoolTypeWiseAndSegmentWise";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan="#spanSchoolsStatisticsSchoolTypeWiseAndSegmentWise";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.thead="#schoolsStatisticsSchoolTypeWiseAndSegmentWiseTable thead";
schoolsStatisticsSchoolTypeWiseAndSegmentWise.tbody="#schoolsStatisticsSchoolTypeWiseAndSegmentWiseTable tbody"

schoolsStatisticsSchoolTypeWiseAndSegmentWise.school ="#selectBox_schoolsByLoc";
/////////////////////////////////

//////////////////////////////////////////

////////////////start Total Projects Monthly Sector Wise //////////////////
var schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart={
		fnBindChartTypeData:function(){
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.chartTypeDiv).show();
			destroyRefresh(schoolsStatisticsSchoolTypeWiseAndSegmentWise.chartType);
			schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnSetOptionsForChartType(schoolsStatisticsSchoolTypeWiseAndSegmentWise.chartType);
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.chartTypeDiv).show();
		},
		fnSetOptionsForChartType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnBindDataTypeData(true);
						schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnUpdateBarChart();
						//schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnBindDataTypeData:function(){
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.sectorDiv).show();
			destroyRefresh(schoolsStatisticsSchoolTypeWiseAndSegmentWise.dataType);
			schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnSetOptionsForDataType($(schoolsStatisticsSchoolTypeWiseAndSegmentWise.dataType));
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.dataTypeDiv).show();
		},
		fnSetOptionsForDataType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnUpdateBarChart();
						//schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnUpdateBarChart:function(){
		/*	var schoolIds=[];
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.school).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						schoolIds.push($(option).val());
					}
				});
			});
			
		;*/
			
			var dataTypeId=$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.dataType).find("option:selected").data('id');
			var chartTypeId=$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.chartType).find("option:selected").data('id');
			
			var jsonData={
					"schoolIds":xSchoolIds,
					"dataTypeId":dataTypeId,
					"chartTypeId":chartTypeId,
					
			}
			
			if(xSchoolIds.length>0){
				var pieChartData=null;
				if(dataTypeId==1){
					pieChartData=customAjaxCalling("reportDashBoard/getPieChartDataValueForSchoolsStatisticsSchoolTypeWiseAndSegmentWise", jsonData, "POST");
				}else{
					pieChartData=customAjaxCalling("reportDashBoard/getPieChartDataPercentageForSchoolsStatisticsSchoolTypeWiseAndSegmentWise", jsonData, "POST");
				}
				if (dataTypeId==2){
					percentageSign="%";
				}
				if(pieChartData!=""){
					$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(" ");
					$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).hide();
					
					schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnGenrateTable(pieChartData.labels,pieChartData.series);
					
					var schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChartDataConfig = {
							type: 'pie',
							data: {
								datasets: [{
									data:pieChartData.series,
									backgroundColor:pieChartData.backgroundColor ,
								}],
								labels: pieChartData.labels
							},
							options: {
								responsive: true,
								legend: {
									display: true,
									position: 'bottom',

								},
								animation:{
									animateScale:true
								},
								tooltips: {
									enabled: true,
								}
							}
					};

					$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvas).remove();
					$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasDiv).append('<canvas id="chart-schoolsStatisticsSchoolTypeWiseAndSegmentWise" />');
					var canvas = document.getElementById("chart-schoolsStatisticsSchoolTypeWiseAndSegmentWise");
					var context = canvas.getContext('2d');
					context.clear();
					window.myBar = new Chart(context, schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChartDataConfig);
					window.myBar.update();
				}
				else{
					$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).show();
					schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnEmptyCanvas(emptyChartMessage);
				}
			}

		},
		fnEmptyCanvas:function(message){
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(" ");
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvas).remove();
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).show();
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(message)
		},
		fnGenrateTable:function(labels,dataSet){
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.thead).children("tr").remove();
			$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.tbody).children("tr").remove();
			var thead='<tr style="display: table-row;">';
			var tData="";
			$.each(labels,function(index,thData){
				var count=index+1;
				tData=tData+'<th id="h'+count+'" class="nosort col-xs-2">'+thData+'</th>';
			});
            var finalThead=thead+tData+"</tr>";
            var tbody="";
            $.each(dataSet,function(indexData,data){
            	tbody=tbody+'<td class="textCenter">'+data+percentageSign+'</td>';
            	
            	
            });
           var finalTbody="<tr>"+tbody+"</tr>";
            $(schoolsStatisticsSchoolTypeWiseAndSegmentWise.thead).append(finalThead);
            $(schoolsStatisticsSchoolTypeWiseAndSegmentWise.tbody).append(finalTbody);
			
		},
		fnShowError:function(currentElement){
			//$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(" ");
			var elements =[schoolsStatisticsSchoolTypeWiseAndSegmentWise.school];
			var message =[selectSchoolMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(isErrorShwon==false && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).text(message[i]);
						$(schoolsStatisticsSchoolTypeWiseAndSegmentWise.canvasSpan).show();
						isErrorShwon =true;
						return false;
					}
				}
			});

		}
}

////////////////////// 

var schoolTypeWiseAndSegmentWiseInIt=function(){
	schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnBindChartTypeData(true);
	schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnBindDataTypeData(true);
	schoolsStatisticsSchoolTypeWiseAndSegmentWiseBarChart.fnUpdateBarChart();
}

/*$(document) .ready(
		function() {
			schoolTypeWiseAndSegmentWiseInIt();
		});*/