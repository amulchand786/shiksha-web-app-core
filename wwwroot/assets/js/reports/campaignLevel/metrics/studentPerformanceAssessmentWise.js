var chartYaxisName='Value';
////////////////////////Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";

var studentPerformanceAssessmentWisePercentage=new Object();
studentPerformanceAssessmentWisePercentage.canvasDiv="#canvasStudentPerformanceAssessmentWisePercentageDiv";
studentPerformanceAssessmentWisePercentage.canvas="#chart-studentPerformanceAssessmentWisePercentage";
studentPerformanceAssessmentWisePercentage.canvasElement="chart-studentPerformanceAssessmentWisePercentage";
studentPerformanceAssessmentWisePercentage.canvasSpan="#spanStudentPerformanceAssessmentWisePercentage";
studentPerformanceAssessmentWisePercentage.tbody="#studentPerformanceAssessmentWisePercentageTable tbody";
studentPerformanceAssessmentWisePercentage.table="#studentPerformanceAssessmentWisePercentageTable";
studentPerformanceAssessmentWisePercentage.tableDiv="#studentPerformanceAssessmentWisePercentageTableDiv";

var studentPerformanceAssessmentWiseValue=new Object();
studentPerformanceAssessmentWiseValue.canvasDiv="#canvasStudentPerformanceAssessmentWiseValueDiv";
studentPerformanceAssessmentWiseValue.canvas="#chart-studentPerformanceAssessmentWiseValue";
studentPerformanceAssessmentWiseValue.canvasElement="chart-studentPerformanceAssessmentWiseValue";
studentPerformanceAssessmentWiseValue.canvasSpan="#spanStudentPerformanceAssessmentWiseValue";
studentPerformanceAssessmentWiseValue.tbody="#studentPerformanceAssessmentWiseValueTable tbody";
studentPerformanceAssessmentWiseValue.table="#studentPerformanceAssessmentWiseValueTable";
studentPerformanceAssessmentWiseValue.tableDiv="#studentPerformanceAssessmentWiseValueTableDiv";

var studentPerformanceAssessmentWiseTaxonomy=new Object();
studentPerformanceAssessmentWiseTaxonomy.canvasDiv="#canvasStudentPerformanceAssessmentWiseTaxonomyDiv";
studentPerformanceAssessmentWiseTaxonomy.canvas="#chart-studentPerformanceAssessmentWiseTaxonomy";
studentPerformanceAssessmentWiseTaxonomy.canvasSpan="#spanStudentPerformanceAssessmentWiseTaxonomy";
studentPerformanceAssessmentWiseTaxonomy.tbody="#studentPerformanceAssessmentWiseTaxonomyTable tbody";
studentPerformanceAssessmentWiseTaxonomy.table="#studentPerformanceAssessmentWiseTaxonomyTable";
studentPerformanceAssessmentWiseTaxonomy.tableDiv="#studentPerformanceAssessmentWiseTaxonomyTableDiv";
studentPerformanceAssessmentWiseTaxonomy.canvasElement="chart-studentPerformanceAssessmentWiseTaxonomy";


var studentPerformanceAssessmentWiseTaxonomyValue=new Object();
studentPerformanceAssessmentWiseTaxonomyValue.canvasDiv="#canvasStudentPerformanceAssessmentWiseTaxonomyValueDiv";
studentPerformanceAssessmentWiseTaxonomyValue.canvas="#chart-studentPerformanceAssessmentWiseTaxonomyValue";
studentPerformanceAssessmentWiseTaxonomyValue.canvasSpan="#spanStudentPerformanceAssessmentWiseTaxonomyValue";
studentPerformanceAssessmentWiseTaxonomyValue.tbody="#studentPerformanceAssessmentWiseTaxonomyValueTable tbody";
studentPerformanceAssessmentWiseTaxonomyValue.table="#studentPerformanceAssessmentWiseTaxonomyValueTable";
studentPerformanceAssessmentWiseTaxonomyValue.tableDiv="#studentPerformanceAssessmentWiseTaxonomyValueTableDiv";
studentPerformanceAssessmentWiseTaxonomyValue.canvasElement="chart-studentPerformanceAssessmentWiseTaxonomyValue";

var studentPerformanceConsolidatedGenderWise=new Object();
studentPerformanceConsolidatedGenderWise.canvasDiv="#canvasStudentPerformanceConsolidatedGenderWiseDiv";
studentPerformanceConsolidatedGenderWise.canvas="#chart-studentPerformanceConsolidatedGenderWise";
studentPerformanceConsolidatedGenderWise.canvasSpan="#spanStudentPerformanceConsolidatedGenderWise";
studentPerformanceConsolidatedGenderWise.tbody="#studentPerformanceConsolidatedGenderWiseTable tbody";
studentPerformanceConsolidatedGenderWise.thead="#studentPerformanceConsolidatedGenderWiseTable thead";	
studentPerformanceConsolidatedGenderWise.table="#studentPerformanceConsolidatedGenderWiseTable";
studentPerformanceConsolidatedGenderWise.tableDiv="#studentPerformanceConsolidatedGenderWiseTableDiv";
studentPerformanceConsolidatedGenderWise.canvasElement="chart-studentPerformanceConsolidatedGenderWise";

var studentPerformanceData=new Object();
studentPerformanceData.canvasDiv="#canvasStudentPerformanceDiv";
studentPerformanceData.canvas="#chart-studentPerformance";
studentPerformanceData.canvasSpan="#spanStudentPerformance";
studentPerformanceData.tbody="#studentPerformanceTable tbody";
studentPerformanceData.thead="#studentPerformanceTable thead";	
studentPerformanceData.table="#studentPerformanceTable";
studentPerformanceData.tableDiv="#studentPerformanceTableDiv";
studentPerformanceData.canvasElement="chart-studentPerformanceValue";


var studentPerformanceAssessmentWiseBarChart={


		init:function(){
			self=studentPerformanceAssessmentWiseBarChart;
		},


		fnEmptyCanvas:function(message,canvas,canvasSpan){
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(" ");
			$(studentPerformanceAssessmentWisePercentage.canvas).remove();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
			$(studentPerformanceAssessmentWisePercentage.canvasSpan).text(message)
		},
		fnUpdateBarChart:function(){
			var asessmentMetricsReportData=graphData;

			var result=asessmentMetricsReportData!=null?asessmentMetricsReportData.length>0?true:false:false;
			var labels= ["Attempted", "Scored > 90%", "Scored < 90%"];
			var  dataSets =[];

			var labelsValue= ["Enrolled", "Appeared", "Absent","Pending","Scored >= 90%","Scored < 90%"];
			var  dataSetsValue =[];

			var labelsTaxonomy= ["K", "U", "O","An","Ap"];
			var  dataSetsTaxonomy =[];
			var  dataSetsTaxonomyForValue =[];
			if(result==true){

				$(studentPerformanceAssessmentWisePercentage.tbody).children("tr").remove();
				$(studentPerformanceAssessmentWiseValue.tbody).children("tr").remove();
				$(studentPerformanceAssessmentWiseTaxonomy.tbody).children("tr").remove();
				$(studentPerformanceAssessmentWiseTaxonomyValue.tbody).children("tr").remove();

				$(studentPerformanceData.thead).children("tr").remove();
				$(studentPerformanceData.tbody).children("tr").remove();

				$(studentPerformanceConsolidatedGenderWise.thead).children("tr").remove();
				$(studentPerformanceConsolidatedGenderWise.tbody).children("tr").remove();

				$.each(asessmentMetricsReportData, function(index,assessmentMetricsData) {	
					var row=studentPerformanceAssessmentWiseBarChart.createStudentPerformanceAssessmentWisePercentageRow(assessmentMetricsData);

					$(studentPerformanceAssessmentWisePercentage.tbody).append(row);
					var gData ={
							type: 'bar',
							label: assessmentMetricsData.assessmentName,
							backgroundColor: randomColor(),
							data: [assessmentMetricsData.percentageOfStudentsAttempted.totalStudent, assessmentMetricsData.percentageOfStudentsScoredGTEQ90.totalStudent, assessmentMetricsData.percentageOfStudentsScoredLTEQ90.totalStudent]
					}
					dataSets.push(gData);


					var performanceValueRow=studentPerformanceAssessmentWiseBarChart.createStudentPerformanceAssessmentWiseValueRow(assessmentMetricsData);

					$(studentPerformanceAssessmentWiseValue.tbody).append(performanceValueRow);

					var performanceValueData ={
							type: 'bar',
							label: assessmentMetricsData.assessmentName,
							backgroundColor: randomColor(),
							data: [assessmentMetricsData.numberOfStudentsEnrolled.totalStudent, 
							       assessmentMetricsData.totalNumberOfStudentsAppearedInAssessment.totalStudent, 
							       assessmentMetricsData.numberOfStudentsAbsentForAssessment.totalStudent,
							       assessmentMetricsData.numberOfStudentsYetToTakeAssessment.totalStudent, 
							       assessmentMetricsData.numberOfStudentsScoredGTEQ90.totalStudent, 
							       assessmentMetricsData.numberOfStudentsScoredLTEQ90.totalStudent]
					}
					dataSetsValue.push(performanceValueData);
					////////////////////////taxonomy value/////////////////////
					var totalStudentAppeared=assessmentMetricsData.totalNumberOfStudentsAppearedInAssessment.totalStudent;
					var taxonomyScoreMapData=assessmentMetricsData.totalTaxonomyScoreMap;
					var kTaxnomy=taxonomyScoreMapData.hasOwnProperty('Knowledge')==true?taxonomyScoreMapData.Knowledge:0;
					var uTaxnomy=taxonomyScoreMapData.hasOwnProperty('Understanding')==true?taxonomyScoreMapData.Understanding:0;
					var oTaxnomy=taxonomyScoreMapData.hasOwnProperty('Operation')==true?taxonomyScoreMapData.Operation:0;
					var anTaxnomy=taxonomyScoreMapData.hasOwnProperty('Analysis')==true?taxonomyScoreMapData.Analysis:0;
					var apTaxnomy=taxonomyScoreMapData.hasOwnProperty('Application')==true?taxonomyScoreMapData.Application:0;
					var totalKnowledgeScore=kTaxnomy*totalStudentAppeared;
					var totalUnderstandingScore=uTaxnomy*totalStudentAppeared;
					var totalOperationScore=oTaxnomy*totalStudentAppeared;
					var totalAnalysisScore=anTaxnomy*totalStudentAppeared;
					var totalApplicationScore=apTaxnomy*totalStudentAppeared;
					var taxonomyValueRow=   studentPerformanceAssessmentWiseBarChart.createStudentPerformanceAssessmentWiseTaxonomyValueRow(assessmentMetricsData);
					var taxonomyValueTempRow='<tr> <td></td> <td></td>  <td></td> <td></td>  <td>Total</td> <td>'+totalKnowledgeScore+'</td> <td>'+totalUnderstandingScore+'</td> <td>'+totalOperationScore+'</td> <td>'+totalAnalysisScore+'</td> <td>'+totalApplicationScore+'</td></tr>'



					$(studentPerformanceAssessmentWiseTaxonomyValue.tbody).append(taxonomyValueTempRow);
					$(studentPerformanceAssessmentWiseTaxonomyValue.tbody).append(taxonomyValueRow);

					var taxonomyValueData ={
							type: 'bar',
							label: assessmentMetricsData.assessmentName,
							backgroundColor: randomColor(),
							data: [assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_U,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_O,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_An,assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap]
					}
					dataSetsTaxonomyForValue.push(taxonomyValueData);

					//////////////////// taxonomy wise/////////////////////////
					var secondTableRow='';


					var scoredPercentageKnowledge=0;
					var scoredPercentageUnderstanding=0;
					var scoredPercentageOperation=0;
					var scoredPercentageAnalysis=0;
					var scoredPercentageApplication=0;
					scoredPercentageKnowledge=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_K)/(totalKnowledgeScore!=0?totalKnowledgeScore:1)).toFixed(2);
					scoredPercentageUnderstanding=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_U)/(totalUnderstandingScore!=0?totalUnderstandingScore:1)).toFixed(2);
					scoredPercentageOperation=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_O)/(totalOperationScore!=0?totalOperationScore:1)).toFixed(2);
					scoredPercentageAnalysis=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_An)/(totalAnalysisScore!=0?totalAnalysisScore:1)).toFixed(2);
					scoredPercentageApplication=(100*(assessmentMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap)/(totalApplicationScore!=0?totalApplicationScore:1)).toFixed(2);
					var totalK=100*(totalKnowledgeScore/(totalKnowledgeScore!=0?totalKnowledgeScore:1)).toFixed(0);
					var totalU=100*(totalUnderstandingScore/(totalUnderstandingScore!=0?totalUnderstandingScore:1)).toFixed(0);
					var totalO=100*(totalOperationScore/(totalOperationScore!=0?totalOperationScore:1)).toFixed(0);
					var totalAn=100*(totalAnalysisScore/(totalAnalysisScore!=0?totalAnalysisScore:1)).toFixed(0);
					var totalAp=100*(totalApplicationScore/(totalApplicationScore!=0?totalApplicationScore:1)).toFixed(0);
					var tempRowPercentage='<tr> <td></td> <td></td><td></td> <td></td>  <td></td> <td></td>  <td>Total %</td> <td>'+totalK+"%"+'</td> <td>'+totalU+"%"+'</td> <td>'+totalO+"%"+'</td> <td>'+totalAn+"%"+'</td> <td>'+totalAp+"%"+'</td></tr>'

					secondTableRow='<tr  id="'+assessmentMetricsData.assessmentId+'">'+
					'<td title="'+assessmentMetricsData.assessmentName+'" data-assessmentId="'+assessmentMetricsData.id+'">'+assessmentMetricsData.assessmentName+'</td>'+
					'<td title="'+assessmentMetricsData.campaignName+'" >'+assessmentMetricsData.campaignName+'</td>'+
					'<td title="'+assessmentMetricsData.status+'" >'+assessmentMetricsData.status+'</td>'+
					'<td title="'+assessmentMetricsData.gradeName+'" >'+assessmentMetricsData.gradeName+'</td>'+
					'<td title="'+assessmentMetricsData.stageName+'" >'+assessmentMetricsData.stageName+'</td>'+
					'<td title="'+assessmentMetricsData.subjectName+'" >'+assessmentMetricsData.subjectName+'</td>'+
					'<td> Scored % </td>'+
					'<td title="'+scoredPercentageKnowledge+'" >'+scoredPercentageKnowledge+"%"+'</td>'+
					'<td title="'+scoredPercentageUnderstanding+'" >'+scoredPercentageUnderstanding+"%"+'</td>'+
					'<td title="'+scoredPercentageOperation+'" >'+scoredPercentageOperation+"%"+'</td>'+
					'<td title="'+scoredPercentageAnalysis+'" >'+scoredPercentageAnalysis+"%"+'</td>'+
					'<td title="'+scoredPercentageApplication+'" >'+scoredPercentageApplication+"%"+'</td>'+
					'</tr>';
					$(studentPerformanceAssessmentWiseTaxonomy.tbody).append(tempRowPercentage);

					$(studentPerformanceAssessmentWiseTaxonomy.tbody).append(secondTableRow);

					var taxonomyPercentageData ={
							type: 'bar',
							label: assessmentMetricsData.assessmentName,
							backgroundColor: randomColor(),
							data: [scoredPercentageKnowledge,scoredPercentageUnderstanding,scoredPercentageOperation,scoredPercentageAnalysis,scoredPercentageApplication]
					}
					dataSetsTaxonomy.push(taxonomyPercentageData);
				});

				var studentPerformanceAssessmentWiseValueBarChartData = {
						labels : labelsValue,
						datasets : dataSetsValue
				};

				var studentPerformanceAssessmentWisePercentageBarChartData = {
						labels : labels,
						datasets : dataSets
				};
				var studentPerformanceAssessmentWiseTaxonomyBarChartData = {
						labels : labelsTaxonomy,
						datasets : dataSetsTaxonomy
				};
				var studentPerformanceAssessmentWiseTaxonomyValueBarChartData = {
						labels : labelsTaxonomy,
						datasets : dataSetsTaxonomyForValue
				};

				var studentPerformanceAssessmentWisePercentageBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWisePercentageBarChartData,"Percentage");
				var studentPerformanceAssessmentWiseTaxonomyBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWiseTaxonomyBarChartData,"Percentage");
				var studentPerformanceAssessmentWiseValueBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWiseValueBarChartData,"Value");
				var studentPerformanceAssessmentWiseTaxonomyValueBarChartDataConfig = studentPerformanceAssessmentWiseBarChart.barChartDataConfig(studentPerformanceAssessmentWiseTaxonomyValueBarChartData,"Value");
				studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWisePercentage,studentPerformanceAssessmentWisePercentageBarChartDataConfig);
				if(superRoleCode=="T" || superRoleCode=="RC" || superRoleCode=="FS"){
					studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWiseValue,studentPerformanceAssessmentWiseValueBarChartDataConfig);
					studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWiseTaxonomyValue,studentPerformanceAssessmentWiseTaxonomyValueBarChartDataConfig);
				}
				studentPerformanceAssessmentWiseBarChart.createChartData(studentPerformanceAssessmentWiseTaxonomy,studentPerformanceAssessmentWiseTaxonomyBarChartDataConfig);

				if(superRoleCode=="CT" || superRoleCode=="SU"){
					var graphDataSets =studentPerformanceAssessmentWiseBarChart.genrateConsolidatedGraphForGenderWiseTaxonomyWiseStageWise(defaultGraphData,"Percentage");
					var zzConfig =self.barChartDataConfig(graphDataSets,"Percentage");
					self.createChartData(studentPerformanceData,zzConfig);



					var graphZDataSets =studentPerformanceAssessmentWiseBarChart.genrateConsolidatedGraphForGenderWisePerformance(defaultGraphData,"Percentage");
					var zZConfig =self.barChartDataConfig(graphZDataSets,"Percentage");
					self.createChartData(studentPerformanceConsolidatedGenderWise,zZConfig);

				}

			}
			else{
				$(studentPerformanceAssessmentWisePercentage.canvasSpan).show();
				studentPerformanceAssessmentWiseBarChart.fnEmptyCanvas(emptyChartMessage,studentPerformanceAssessmentWisePercentage.canvas,studentPerformanceAssessmentWisePercentage.canvasSpan);
			}
			setTimeout(() => {
				$("#graphDataDiv").show();
			}, 5);

			//$("#graphDataDiv").show();
		},
		createStudentPerformanceAssessmentWisePercentageRow:function(assessmentData){

			var row='<tr  id="'+assessmentData.assessmentId+'">'+
			'<td title="'+assessmentData.assessmentName+'" data-assessmentId="'+assessmentData.id+'">'+assessmentData.assessmentName+'</td>'+
			'<td title="'+assessmentData.gradeName+'" >'+assessmentData.gradeName+'</td>'+
			'<td title="'+assessmentData.stageName+'" >'+assessmentData.stageName+'</td>'+
			'<td title="'+assessmentData.subjectName+'" >'+assessmentData.subjectName+'</td>'+
			'<td title="'+assessmentData.percentageOfStudentsAttempted.totalStudent+'" >'+assessmentData.percentageOfStudentsAttempted.totalStudent+'% </td>'+
			'<td title="'+assessmentData.percentageOfStudentsScoredGTEQ90.totalStudent+'" >'+assessmentData.percentageOfStudentsScoredGTEQ90.totalStudent+'% </td>'+
			'<td title="'+assessmentData.percentageOfStudentsScoredLTEQ90.totalStudent+'" >'+assessmentData.percentageOfStudentsScoredLTEQ90.totalStudent+'% </td>'+
			'</tr>';
			return row;

		},
		createStudentPerformanceAssessmentWiseValueRow:function(assessmentValueData){
			var studentEnroldedTD="";
			var studentAppearedTD="";
			var studentAbsentTD="";
			var studentYetToTakeTD="";
			var studentGTETD="";
			var studentLTETD="";

			studentEnroldedTD='<td title="'+assessmentValueData.numberOfStudentsEnrolled.totalStudent+'" >'+assessmentValueData.numberOfStudentsEnrolled.totalStudent+'</td>';
			studentGTETD='<td title="'+assessmentValueData.numberOfStudentsScoredGTEQ90.totalStudent+'" >'+assessmentValueData.numberOfStudentsScoredGTEQ90.totalStudent+'</td>';
			studentAppearedTD='<td title="'+assessmentValueData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'" >'+assessmentValueData.totalNumberOfStudentsAppearedInAssessment.totalStudent+'</td>';
			studentLTETD='<td title="'+assessmentValueData.numberOfStudentsScoredLTEQ90.totalStudent+'" >'+assessmentValueData.numberOfStudentsScoredLTEQ90.totalStudent+'</td>';
			studentYetToTakeTD=	'<td title="'+assessmentValueData.numberOfStudentsYetToTakeAssessment.totalStudent+'" >'+assessmentValueData.numberOfStudentsYetToTakeAssessment.totalStudent+'</td>';
			studentAbsentTD='<td title="'+assessmentValueData.numberOfStudentsAbsentForAssessment.totalStudent+'" >'+assessmentValueData.numberOfStudentsAbsentForAssessment.totalStudent+'</a></td>';

			var row ='<tr  id="'+assessmentValueData.assessmentId+'">'+
			'<td title="'+assessmentValueData.assessmentName+'" data-assessmentId="'+assessmentValueData.id+'">'+assessmentValueData.assessmentName+'</td>'+
			'<td title="'+assessmentValueData.gradeName+'" >'+assessmentValueData.gradeName+'</td>'+
			'<td title="'+assessmentValueData.stageName+'" >'+assessmentValueData.stageName+'</td>'+
			'<td title="'+assessmentValueData.subjectName+'" >'+assessmentValueData.subjectName+'</td>'+
			studentEnroldedTD+studentAppearedTD+ studentAbsentTD+studentYetToTakeTD+studentGTETD+ studentLTETD+
			'</tr>';


			return row
		},
		createStudentPerformanceAssessmentWiseTaxonomyValueRow:function(assessmentTaxonomyMetricsData){
			var row='<tr  id="'+assessmentTaxonomyMetricsData.assessmentId+'">'+

			'<td title="'+assessmentTaxonomyMetricsData.assessmentName+'" data-assessmentId="'+assessmentTaxonomyMetricsData.id+'">'+assessmentTaxonomyMetricsData.assessmentName+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.gradeName+'" >'+assessmentTaxonomyMetricsData.gradeName+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.stageName+'" >'+assessmentTaxonomyMetricsData.stageName+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.subjectName+'" >'+assessmentTaxonomyMetricsData.subjectName+'</td>'+
			'<td>Scored</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_K+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_U+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_U+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_O+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_O+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_An+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_An+'</td>'+
			'<td title="'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap+'" >'+assessmentTaxonomyMetricsData.parameterScoringForAssessmentTotalScoring.scored_Ap+'</td>'+
			'</tr>';

			return row;
		},
		createChartData:function(chartElement,configData){
			$(chartElement.canvas).remove();
			$(chartElement.canvasDiv).append('<canvas id="'+chartElement.canvasElement+'" />');
			var canvas = document.getElementById(chartElement.canvasElement);
			var context = canvas.getContext('2d');
			context.clear();
			window.myBar = new Chart(context, configData);
			window.myBar.update();
		},
		barChartDataConfig:function(data,label){
			var studentPerformanceBarChartDataConfig = {
					type: 'bar',
					data: data,
					options: {

						scales: {
							xAxes: [{
								display: true,
								gridLines: {
									display: false
								},
							}],
							yAxes: [{
								display: true,
								gridLines: {
									display: false
								},
								ticks: {
									beginAtZero: true,
									steps: 10,
									stepValue: 5,
								},
								scaleLabel: {
									display: true,
									labelString: label
								}
							}]
						},


						elements: {
							rectangle: {
								borderWidth: 0.5,
								borderColor: randomColor(),
								borderSkipped: 'bottom'
							}
						},
						responsive: true,
						legend: {
							position: 'bottom',
						},

					}


			}
			return studentPerformanceBarChartDataConfig;
		},
		genrateConsolidatedGraphForGenderWiseTaxonomyWiseStageWise:function(data,dataTypeSign){
			var ptAssessment=[];
			var patAssessment=[];
			var ptCode="PT";
			var patCode="PAT";
			$.each(data,function(index,obj){
				if(obj.stageCode==patCode){
					patAssessment.push(obj);
				}else if(obj.stageCode==ptCode){
					ptAssessment.push(obj);
				}
			});
			xDatasets =[];
			if(ptAssessment.length>0){
				var ptZData =self.fnPrepareDataForGraphAssessmentWise(ptAssessment,data[0].openLabType+" _"+ptCode);
				xDatasets.push(ptZData);
			}
			if(patAssessment.length>0){
				var patZData =self.fnPrepareDataForGraphAssessmentWise(patAssessment,data[0].openLabType+"_"+patCode);
				xDatasets.push(patZData);
			}

			var barChart9Data = new Object();
			barChart9Data.labels =["K Male", "K Female", "K Total", "U Male", "U Female", "U Total", "O Male", "O Female", "O Total", "An Male", "An Female", "An Total", "Ap Male", "Ap Female", "Ap Total" ];
			barChart9Data.datasets=xDatasets;
			self.fnCreateTableByGraphData(xDatasets,barChart9Data.labels,studentPerformanceData,"Phase-Stage ",dataTypeSign);
			return barChart9Data;

		},
		genrateConsolidatedGraphForGenderWisePerformance:function(data,dataTypeSign){
			var ptAssessment=[];
			var patAssessment=[];
			var ptCode="PT";
			var patCode="PAT";
			$.each(data,function(index,obj){
				if(obj.stageCode==patCode){
					patAssessment.push(obj);
				}else if(obj.stageCode==ptCode){
					ptAssessment.push(obj);
				}
			});
			gDatasets =[];
			if(ptAssessment.length>0){
				var ptGData =self.fnPrepareDataForPerformanceGenderWiseAssessmentWise(ptAssessment,data[0].schoolType+" _"+ptCode);
				gDatasets.push(ptGData);
			}
			if(patAssessment.length>0){
				var patGData =self.fnPrepareDataForPerformanceGenderWiseAssessmentWise(patAssessment,data[0].schoolType+"_"+patCode);
				gDatasets.push(patGData);
			}

			var barChartGData = new Object();
			barChartGData.labels =["Appeared Male", "Appeared Female", "Appeared Total", "Scored >= 90 Male", "Scored >= 90 Female", "Scored >= 90 Total", "Scored < 90 Male", "Scored < 90 Female", "Scored < 90 Total"];
			barChartGData.datasets=gDatasets;
			self.fnCreateTableByGraphData(gDatasets,barChartGData.labels,studentPerformanceConsolidatedGenderWise,"School type-Stage ",dataTypeSign);
			return barChartGData;

		},
		fnCreateTableByGraphData : function(pData,pLables,element,firstColName,adataTypeSign){
			var percentageSign="";
			if(adataTypeSign=="Percentage"){
				percentageSign="%";
			}
			//create heades
			var th ='';
			var openTh='<th>',closedTh='</th>';

			$.each(pLables,function(i,val){
				th=th+openTh+val+closedTh;
			});
			$(element.thead).append('<tr>'+openTh+firstColName+closedTh+th+'</tr>');

			var tr='<tr>',closedTr='</tr>',td='<td>',closedtd='</td>';
			$.each(pData,function(i,val){
				var nData =tr+td+(val.label)+closedtd;
				var mData='';
				$.each(val.data,function(i,e){
					mData=mData+td+(e)+percentageSign+closedtd;
				});
				nData =nData+mData+closedTr;
				$(element.tbody).append(nData);
			});

		},
		//create table data
		fnCreateTableAssessmentWise : function(pData,pLables,element){
			//create heades
			var th ='';
			var openTh='<th>',closedTh='</th>';

			$.each(pLables,function(i,val){
				th=th+openTh+val+closedTh;
			});
			$(element.thead).append('<tr>'+openTh+closedTh+th+'</tr>');

			var tr='<tr>',closedTr='</tr>',td='<td>',closedtd='</td>';
			$.each(pData,function(i,val){
				var nData =tr+td+(val.label)+closedtd;
				var mData='';
				$.each(val.data,function(i,e){
					mData=mData+td+(e)+closedtd;
				});
				nData =nData+mData+closedTr;
				$(element.tbody).append(nData);
			});

		},
		fnPrepareDataForPerformanceGenderWiseAssessmentWise : function(dataObj,type){

			var typeObj =new Object();
			var typeData=[];
			var attemptedMaleStudent=0,attemptedFemaleStudent=0,attemptedTotalStudent=0;
			var scoredGTE90MaleStudent=0,scoredGTE90FemaleStudent=0,scoredGTE90TotalStudent=0;
			var scoredLTE90MaleStudent=0,scoredLTE90FemaleStudent=0,scoredLTE90TotalStudent=0;
			var count=dataObj.length!=0?dataObj.length:1;
			$.each(dataObj,function(i,val){

				var attempted=	val.percentageOfStudentsAttempted;
				var scoredGTE90=val.percentageOfStudentsScoredGTEQ90;
				var scoredLTE90=val.percentageOfStudentsScoredLTEQ90;

				attemptedMaleStudent=attemptedMaleStudent+attempted.maleStudent;
				attemptedFemaleStudent=attemptedFemaleStudent+attempted.femaleStudent;
				attemptedTotalStudent=attemptedTotalStudent+attempted.totalStudent;
				scoredGTE90MaleStudent=scoredGTE90MaleStudent+scoredGTE90.maleStudent;
				scoredGTE90FemaleStudent=scoredGTE90FemaleStudent+scoredGTE90.femaleStudent;
				scoredGTE90TotalStudent=scoredGTE90TotalStudent+scoredGTE90.totalStudent;
				scoredLTE90MaleStudent=scoredLTE90MaleStudent+	scoredLTE90.maleStudent;
				scoredLTE90FemaleStudent=scoredLTE90FemaleStudent+scoredLTE90.femaleStudent;
				scoredLTE90TotalStudent=scoredLTE90TotalStudent+scoredLTE90.totalStudent;

			});
			typeData.push(
					(attemptedMaleStudent/count).toFixed(2),
					(attemptedFemaleStudent/count).toFixed(2),
					(attemptedTotalStudent/count).toFixed(2),
					(scoredGTE90MaleStudent/count).toFixed(2),
					(scoredGTE90FemaleStudent/count).toFixed(2),
					(scoredGTE90TotalStudent/count).toFixed(2),
					(scoredLTE90MaleStudent/count).toFixed(2),
					(scoredLTE90FemaleStudent/count).toFixed(2),
					(scoredLTE90TotalStudent/count).toFixed(2)

			);
			typeObj.label = type;
			typeObj.backgroundColor = randomColor();
			typeObj.data= typeData;
			return typeObj;

		},
		//prepare graph data for taxonomy and stage report
		fnPrepareDataForGraphAssessmentWise : function(dataObj,type){

			var typeObj =new Object();
			var typeData=[];

			var typeMaleTotal_K=0,typeFeMaleTotal_K=0,typeTotal_K=0,
			typeMaleTotal_U=0,typeFeMaleTotal_U=0,typeTotal_U=0,
			typeMaleTotal_O=0,typeFeMaleTotal_O=0,typeTotal_O=0,
			typeMaleTotal_An=0,typeFeMaleTotal_An=0,typeTotal_An=0,
			typeMaleTotal_Ap=0,typeFeMaleTotal_Ap=0,typeTotal_Ap=0;
			var maleSoreMap_K=0,maleScoreMap_U=0,maleScoreMap_O=0,maleScoreMap_An=0,maleScoreMap_Ap=0;
			var femaleScoreMap_K=0,femaleScoreMap_U=0,femaleScoreMap_O=0,femaleScoreMap_An=0,femaleScoreMap_Ap=0;
			var totalScoreMap_K=0,totalScoreMap_U=0,totalScoreMap_O=0,totalScoreMap_An=0,totalScoreMap_Ap=0;



			$.each(dataObj,function(i,val){
				var taxonomyScoreData=val.totalTaxonomyScoreMap;
				typeMaleTotal_K =typeMaleTotal_K+(val.parameterScoringForAssessmentMale.scored_K);
				typeMaleTotal_U =typeMaleTotal_U+(val.parameterScoringForAssessmentMale.scored_U);
				typeMaleTotal_O =typeMaleTotal_O+(val.parameterScoringForAssessmentMale.scored_O);
				typeMaleTotal_An =typeMaleTotal_An+(val.parameterScoringForAssessmentMale.scored_An);
				typeMaleTotal_Ap =typeMaleTotal_Ap+(val.parameterScoringForAssessmentMale.scored_Ap);


				typeFeMaleTotal_K =typeFeMaleTotal_K+(val.parameterScoringForAssessmentFemale.scored_K);
				typeFeMaleTotal_U =typeFeMaleTotal_U+(val.parameterScoringForAssessmentFemale.scored_U);
				typeFeMaleTotal_O =typeFeMaleTotal_O+(val.parameterScoringForAssessmentFemale.scored_O);
				typeFeMaleTotal_An =typeFeMaleTotal_An+(val.parameterScoringForAssessmentFemale.scored_An);
				typeFeMaleTotal_Ap =typeFeMaleTotal_Ap+(val.parameterScoringForAssessmentFemale.scored_Ap);


				typeTotal_K =typeTotal_K+(val.parameterScoringForAssessmentTotalScoring.scored_K);
				typeTotal_U =typeTotal_U+(val.parameterScoringForAssessmentTotalScoring.scored_U);
				typeTotal_O =typeTotal_O+(val.parameterScoringForAssessmentTotalScoring.scored_O);
				typeTotal_An =typeTotal_An+(val.parameterScoringForAssessmentTotalScoring.scored_An);
				typeTotal_Ap =typeTotal_Ap+(val.parameterScoringForAssessmentTotalScoring.scored_Ap);
				var kTaxnomyVal=taxonomyScoreData.hasOwnProperty('Knowledge')==true?taxonomyScoreData.Knowledge:0;
				var uTaxnomyVal=taxonomyScoreData.hasOwnProperty('Understanding')==true?taxonomyScoreData.Understanding:0;
				var oTaxnomyVal=taxonomyScoreData.hasOwnProperty('Operation')==true?taxonomyScoreData.Operation:0;
				var anTaxnomyVal=taxonomyScoreData.hasOwnProperty('Analysis')==true?taxonomyScoreData.Analysis:0;
				var apTaxnomyVal=taxonomyScoreData.hasOwnProperty('Application')==true?taxonomyScoreData.Application:0;

				var maleStudentCount=val.totalNumberOfStudentsAppearedInAssessment.maleStudent;
				var femaleStudentCount=val.totalNumberOfStudentsAppearedInAssessment.femaleStudent;
				var totalStudentCount=val.totalNumberOfStudentsAppearedInAssessment.totalStudent;

				maleSoreMap_K=maleSoreMap_K+kTaxnomyVal*maleStudentCount;
				maleScoreMap_U=maleScoreMap_U+uTaxnomyVal*maleStudentCount;
				maleScoreMap_O=maleScoreMap_O+oTaxnomyVal*maleStudentCount;
				maleScoreMap_An=maleScoreMap_An+anTaxnomyVal*maleStudentCount;
				maleScoreMap_Ap=maleScoreMap_Ap+apTaxnomyVal*maleStudentCount;

				femaleScoreMap_K=femaleScoreMap_K+kTaxnomyVal*femaleStudentCount;
				femaleScoreMap_U=femaleScoreMap_U+uTaxnomyVal*femaleStudentCount;
				femaleScoreMap_O=femaleScoreMap_O+oTaxnomyVal*femaleStudentCount;
				femaleScoreMap_An=femaleScoreMap_An+anTaxnomyVal*femaleStudentCount;
				femaleScoreMap_Ap=femaleScoreMap_Ap+apTaxnomyVal*femaleStudentCount;

				totalScoreMap_K=totalScoreMap_K+kTaxnomyVal*totalStudentCount;
				totalScoreMap_U=totalScoreMap_U+uTaxnomyVal*totalStudentCount;
				totalScoreMap_O=totalScoreMap_O+oTaxnomyVal*totalStudentCount;
				totalScoreMap_An=totalScoreMap_An+anTaxnomyVal*totalStudentCount;
				totalScoreMap_Ap=totalScoreMap_Ap+apTaxnomyVal*totalStudentCount;






			});
			typeData.push(
					(100*typeMaleTotal_K/(maleSoreMap_K!=0?maleSoreMap_K:1)).toFixed(2),
					(100*typeFeMaleTotal_K/(femaleScoreMap_K!=0?femaleScoreMap_K:1)).toFixed(2),
					(100*typeTotal_K/(totalScoreMap_K!=0?totalScoreMap_K:1)).toFixed(2),

					(100*typeMaleTotal_U/(maleScoreMap_U!=0?maleScoreMap_U:1)).toFixed(2),
					(100*typeFeMaleTotal_U/(femaleScoreMap_U!=0?femaleScoreMap_U:1)).toFixed(2),
					(100*typeTotal_U/(totalScoreMap_U!=0?totalScoreMap_U:1)).toFixed(2),

					(100*typeMaleTotal_O/(maleScoreMap_O!=0?maleScoreMap_O:1)).toFixed(2),
					(100*typeFeMaleTotal_O/(femaleScoreMap_O!=0?femaleScoreMap_O:1)).toFixed(2),
					(100*typeTotal_O/(totalScoreMap_O!=0?totalScoreMap_O:1)).toFixed(2),

					(100*typeMaleTotal_An/(maleScoreMap_An!=0?maleScoreMap_An:1)).toFixed(2),
					(100*typeFeMaleTotal_An/(femaleScoreMap_An!=0?femaleScoreMap_An:1)).toFixed(2),
					(100*typeTotal_An/(totalScoreMap_An!=0?totalScoreMap_An:1)).toFixed(2),

					(100*typeMaleTotal_Ap/(maleScoreMap_Ap!=0?maleScoreMap_Ap:1)).toFixed(2),
					(100*typeFeMaleTotal_Ap/(femaleScoreMap_Ap!=0?femaleScoreMap_Ap:1)).toFixed(2),
					(100*typeTotal_Ap/(totalScoreMap_Ap!=0?totalScoreMap_Ap:1)).toFixed(2)

			);



			typeObj.label = type;
			typeObj.backgroundColor = randomColor();
			typeObj.data= typeData;
			return typeObj;

		},

}





var studentPerformanceAssessmentWiseInIt=function(){
	studentPerformanceAssessmentWiseBarChart.fnUpdateBarChart();
}


$(function(){
	studentPerformanceAssessmentWiseBarChart.init();
})
