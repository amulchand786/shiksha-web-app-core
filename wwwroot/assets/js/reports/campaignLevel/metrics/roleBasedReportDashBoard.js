
xSubjectIds =[];
xGradeIds=[];
xSchoolIds =[];
xSCampignId=0;
startDate="";
endDate="";
//show container on submit
var fnShowContainer =function(){
	$('#reportfilter').removeClass('in');
	studentPerformanceAssessmentWiseInIt();
	stageWiseAndGenderWiseInIt();
	taxonomyStageWiseAndGenderWiseInIt();
	schoolTypeWiseAndSegmentWiseInIt();
	studentsTypeWiseAndSegmentWiseInIt();
	if(superRoleCode=="FS" || superRoleCode=="RC"){
		//studentPerformanceGenderWiseTaxonomyInIt();
		//studentPerformanceGenderWiseInIt();
	}

	$('#btnViewReport').prop('disabled',false);
	$('#btnViewReport').removeClass('disabled');

	$('.chartContainer').css('display','block');

}

//hide container
var fnHideContainer =function(){

	fnResetChartTypeFilters();

	$('.chartContainer').css('display','none');
}

//reset chart type and data type of chart containers

var fnResetChartTypeFilters =function(){


	$('#chartType_dropBoxStudentPerformanceStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxStudentPerformanceStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);

	$('#chartType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxStudentPerformanceOnTaxonomyStageWiseAndGenderWise').find('option:eq(0)').prop('selected',true);


	$('#chartType_dropBoxStudentsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxStudentsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);

	$('#chartType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);
	$('#dataType_dropBoxSchoolsStatisticsSchoolTypeWiseAndSegmentWise').find('option:eq(0)').prop('selected',true);

	$('.metrics').each(function(i,ele){
		$(ele).multiselect('refresh');
	});


}




var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}

var fnEmptyCanvas=function(aCanvas,aCanvasSpan){
	$(aCanvas).remove();
	$(aCanvasSpan).show();

}

var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');
	$(ele).multiselect('refresh');
}

var destroyRefresh =function(ele){
	$(ele).multiselect('destroy');
	$(ele).multiselect('refresh');
}
CanvasRenderingContext2D.prototype.clear = 
	CanvasRenderingContext2D.prototype.clear || function (preserveTransform) {
	if (preserveTransform) {
		this.save();
		this.setTransform(1, 0, 0, 1, 0, 0);
	}

	this.clearRect(0, 0, this.canvas.width, this.canvas.height);

	if (preserveTransform) {
		this.restore();
	}           
};



var randomColorFactor = function() {
	return Math.round(Math.random() * 255);
};
var randomColor = function(opacity) {
	return 'rgba(' + randomColorFactor() + ',' + randomColorFactor() + ',' + randomColorFactor() + ',' + (opacity || '0.5') + ')';
};

Chart.defaults.groupableBar = Chart.helpers.clone(Chart.defaults.bar);

var helpers = Chart.helpers;
Chart.controllers.groupableBar = Chart.controllers.bar.extend({
	calculateBarX: function (index, datasetIndex) {
		// position the bars based on the stack index
		var stackIndex = this.getMeta().stackIndex;
		return Chart.controllers.bar.prototype.calculateBarX.apply(this, [index, stackIndex]);
	},

	hideOtherStacks: function (datasetIndex) {
		var meta = this.getMeta();
		var stackIndex = meta.stackIndex;

		this.hiddens = [];
		for (var i = 0; i < datasetIndex; i++) {
			var dsMeta = this.chart.getDatasetMeta(i);
			if (dsMeta.stackIndex !== stackIndex) {
				this.hiddens.push(dsMeta.hidden);
				dsMeta.hidden = true;
			}
		}
	},

	unhideOtherStacks: function (datasetIndex) {
		var meta = this.getMeta();
		var stackIndex = meta.stackIndex;

		for (var i = 0; i < datasetIndex; i++) {
			var dsMeta = this.chart.getDatasetMeta(i);
			if (dsMeta.stackIndex !== stackIndex) {
				dsMeta.hidden = this.hiddens.unshift();
			}
		}
	},

	calculateBarY: function (index, datasetIndex) {
		this.hideOtherStacks(datasetIndex);
		var barY = Chart.controllers.bar.prototype.calculateBarY.apply(this, [index, datasetIndex]);
		this.unhideOtherStacks(datasetIndex);
		return barY;
	},

	calculateBarBase: function (datasetIndex, index) {
		this.hideOtherStacks(datasetIndex);
		var barBase = Chart.controllers.bar.prototype.calculateBarBase.apply(this, [datasetIndex, index]);
		this.unhideOtherStacks(datasetIndex);
		return barBase;
	},

	getBarCount: function () {
		var stacks = [];

		// put the stack index in the dataset meta
		Chart.helpers.each(this.chart.data.datasets, function (dataset, datasetIndex) {
			var meta = this.chart.getDatasetMeta(datasetIndex);
			if (meta.bar && this.chart.isDatasetVisible(datasetIndex)) {
				var stackIndex = stacks.indexOf(dataset.stack);
				if (stackIndex === -1) {
					stackIndex = stacks.length;
					stacks.push(dataset.stack);
				}
				meta.stackIndex = stackIndex;
			}
		}, this);

		this.getMeta().stacks = stacks;
		return stacks.length;
	},
});
