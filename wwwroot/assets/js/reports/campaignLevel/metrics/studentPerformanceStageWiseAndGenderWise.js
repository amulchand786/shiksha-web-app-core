var chartYaxisName='Value';
//////////////////////// Common message ////////////////////////
var emptyChartMessage="The chart contains no data. ";
var selectSchoolMessage="Please select atleast one School. ";
var selectGradeMessage="Please select atleast one Grade. ";
var selectSubjectMessage="Please select atleast one Subject. ";

var percentageSign="";
var studentPerformanceStageWiseAndGenderWise= new Object();

studentPerformanceStageWiseAndGenderWise.chartTypeDiv="#chartType_dropBoxStudentPerformanceStageWiseAndGenderWiseDiv";
studentPerformanceStageWiseAndGenderWise.chartType="#chartType_dropBoxStudentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.dataTypeDiv="#dataType_dropBoxStudentPerformanceStageWiseAndGenderWiseDiv";
studentPerformanceStageWiseAndGenderWise.dataType="#dataType_dropBoxStudentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.canvasDiv="#canvasStudentPerformanceStageWiseAndGenderWiseDiv";
studentPerformanceStageWiseAndGenderWise.canvas="#chart-studentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.canvasSpan="#spanStudentPerformanceStageWiseAndGenderWise";
studentPerformanceStageWiseAndGenderWise.thead="#studentPerformanceStageWiseAndGenderWiseTable thead"
studentPerformanceStageWiseAndGenderWise.tbody="#studentPerformanceStageWiseAndGenderWiseTable tbody"
studentPerformanceStageWiseAndGenderWise.schoolDiv ="#divSelectSchool";
studentPerformanceStageWiseAndGenderWise.gradeDiv ="#divSelectGrade";
studentPerformanceStageWiseAndGenderWise.subjectDiv ="#divSelectSubject";
studentPerformanceStageWiseAndGenderWise.school ="#selectBox_schoolsByLoc";
studentPerformanceStageWiseAndGenderWise.grade ="#selectBox_gradesBySchool";
studentPerformanceStageWiseAndGenderWise.subject ="#selectBox_subject";
studentPerformanceStageWiseAndGenderWise.startDate="#startDatePicker";
studentPerformanceStageWiseAndGenderWise.endDate="#endDatePicker";


/////////////////////////////////

////////////////start Total Projects Monthly Sector Wise //////////////////
var studentPerformanceStageWiseAndGenderWiseBarChart={

		fnBindChartTypeData:function(){
			$(studentPerformanceStageWiseAndGenderWise.chartTypeDiv).show();
			destroyRefresh(studentPerformanceStageWiseAndGenderWise.chartType);
			studentPerformanceStageWiseAndGenderWiseBarChart.fnSetOptionsForChartType($(studentPerformanceStageWiseAndGenderWise.chartType));
			$(studentPerformanceStageWiseAndGenderWise.chartTypeDiv).show();
		},
		fnSetOptionsForChartType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						studentPerformanceStageWiseAndGenderWiseBarChart.fnBindDataTypeData(true);
						studentPerformanceStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
						//studentPerformanceStageWiseAndGenderWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnBindDataTypeData:function(){
			$(studentPerformanceStageWiseAndGenderWise.sectorDiv).show();
			destroyRefresh(studentPerformanceStageWiseAndGenderWise.dataType);
			studentPerformanceStageWiseAndGenderWiseBarChart.fnSetOptionsForDataType($(studentPerformanceStageWiseAndGenderWise.dataType));
			$(studentPerformanceStageWiseAndGenderWise.dataTypeDiv).show();
		},
		fnSetOptionsForDataType:function(){
			$(arguments).each(function(index,arg){
				$(arg).multiselect('destroy');
				$(arg).multiselect({
					maxHeight: 180,
					includeSelectAllOption: true,
					enableFiltering: true,
					enableCaseInsensitiveFiltering: true,
					includeFilterClearBtn: true,
					filterPlaceholder: 'Search here...',
					onDropdownHide: function(event) {
						chartYaxisName =$(arg).find('option:selected').val();
						studentPerformanceStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
						//studentPerformanceStageWiseAndGenderWiseBarChart.fnShowError(arg);
						
					} ,

				});

			});
			fnCollapseMultiselect();

		},
		fnUpdateBarChart:function(){
		/*	var schoolIds=[];
			$(studentPerformanceStageWiseAndGenderWise.school).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						schoolIds.push($(option).val());
					}
				});
			});
			
			var selectedGradeId=[];
			$(studentPerformanceStageWiseAndGenderWise.grade).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selectedGradeId.push($(option).val());
					}
				});
			});
			var selectedSubjectId=[];
			$(studentPerformanceStageWiseAndGenderWise.subject).each(function(index, ele) {
				$(ele).find("option:selected").each(function(ind, option) {
					if ($(option).val() != "multiselect-all"){
						selectedSubjectId.push($(option).val());
					}
				});
			});*/

			var dataTypeId=$(studentPerformanceStageWiseAndGenderWise.dataType).find("option:selected").data('id');
			var chartTypeId=$(studentPerformanceStageWiseAndGenderWise.chartType).find("option:selected").data('id');
			//var startDate=$(studentPerformanceStageWiseAndGenderWise.startDate).datepicker('getFormattedDate');
			//var endDate=$(studentPerformanceStageWiseAndGenderWise.endDate).datepicker('getFormattedDate');
			var fromDate=moment(startDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
			var toDate=moment(endDate,"DD-MMM-YYYY").format('YYYY-MM-DD');
			
			
		
			if(superRoleCode=="FS" || superRoleCode=="RC"){
				chartTypeId=1;
			}else{
				chartTypeId=chartTypeId;

			}
			
			if(superRoleCode=="CT" || superRoleCode=="SU"){
				dataTypeId=2;
			}else{
				dataTypeId=dataTypeId;

			}
			var jsonData={
					"schoolIds":xSchoolIds,
					"fromDate":fromDate,
					"toDate":toDate,
					"gradeIds":xGradeIds,
					"subjectIds":xSubjectIds,
					"dataTypeId":dataTypeId,
					"chartTypeId":chartTypeId,
					"projectId":xSCampignId
			}
			var firstColumn="";
			if(chartTypeId==1){
				firstColumn="Gender ";
			}else if(chartTypeId==2){
				firstColumn="Phase-Stage ";
			}else if(chartTypeId==3){
				firstColumn="School type-Stage ";
			}
			if(xSchoolIds.length>0 && xGradeIds.length>0 && xSubjectIds.length>0){
				var barChartData=null;
				if(dataTypeId==1){
					chartYaxisName="Value";
					percentageSign="";
					 barChartData=customAjaxCalling("metricDashBoard/getBarChartDataForStudentPerformanceStageWiseAndGenderWise", jsonData, "POST");
				}else{
					chartYaxisName="Percentage";
					percentageSign="%";
					 barChartData=customAjaxCalling("metricDashBoard/getBarChartDataForStudentPercentagePerformanceStageWiseAndGenderWise", jsonData, "POST");
				}
				var result=barChartData.datasets!=null?barChartData.datasets.length>0?true:false:false;
				if(result==true){
					$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(" ");
					$(studentPerformanceStageWiseAndGenderWise.canvasSpan).hide();
					var studentPerformanceStageWiseAndGenderWiseBarChartData = {
							labels: barChartData.labels,
							datasets: barChartData.datasets

					};
					studentPerformanceStageWiseAndGenderWiseBarChart.fnGenrateTable(barChartData.labels,barChartData.datasets,firstColumn);
					var studentPerformanceStageWiseAndGenderWiseBarChartDataConfig = {
							type: 'bar',
							data: studentPerformanceStageWiseAndGenderWiseBarChartData,
							options: {
								
								 scales: {
					                    xAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
					                        }],
					                    yAxes: [{
					                            display: true,
					                            gridLines: {
									                display: false
									            },
									            ticks: {
					                                beginAtZero: true,
					                                steps: 10,
					                                stepValue: 5,
					                            },
					                            scaleLabel: {
					                                display: true,
					                                labelString: chartYaxisName
					                            }
					                        }]
					                },
								
								
								elements: {
									rectangle: {
										borderWidth: 0.5,
										borderColor: randomColor(),
										borderSkipped: 'bottom'
									}
								},
								responsive: true,
								legend: {
									position: 'bottom',
								},

							}


					}

					$(studentPerformanceStageWiseAndGenderWise.canvas).remove();
					$(studentPerformanceStageWiseAndGenderWise.canvasDiv).append('<canvas id="chart-studentPerformanceStageWiseAndGenderWise" />');
					var canvas = document.getElementById("chart-studentPerformanceStageWiseAndGenderWise");
					var context = canvas.getContext('2d');
					context.clear();
					window.myBar = new Chart(context, studentPerformanceStageWiseAndGenderWiseBarChartDataConfig);
					window.myBar.update();
				}
				else{
					$(studentPerformanceStageWiseAndGenderWise.canvasSpan).show();
					studentPerformanceStageWiseAndGenderWiseBarChart.fnEmptyCanvas(emptyChartMessage);
				}
			}

		},
		fnEmptyCanvas:function(message){
			$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(" ");
			$(studentPerformanceStageWiseAndGenderWise.canvas).remove();
			$(studentPerformanceStageWiseAndGenderWise.canvasSpan).show();
			$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(message)
		},
		fnGenrateTable:function(labels,dataSet,firstcolName){
			$(studentPerformanceStageWiseAndGenderWise.thead).children("tr").remove();
			$(studentPerformanceStageWiseAndGenderWise.tbody).children("tr").remove();
			var thead='<tr style="display: table-row;">';
			var tData="";
			$.each(labels,function(index,thData){
				var count=index+1;
				tData=tData+'<th id="h'+count+'" class="nosort col-xs-2">'+thData+'</th>';
			});
            var finalThead=thead+'<th class="nosort col-xs-3" >'+firstcolName+'</th>'+tData+"</tr>";
            var tbody="";
            $.each(dataSet,function(indexData,data){
            	tbody=tbody+'<tr><td class="text-center-bold">'+data.label+'</td>';
            	$.each(data.data,function(i,edata){
            		tbody=tbody+'<td class="textCenter">'+edata+percentageSign+'</td>';
            	});
            	tbody=tbody+("</tr>")
            });
           var finalTbody="<tr>"+tbody+"</tr>";
            $(studentPerformanceStageWiseAndGenderWise.thead).append(finalThead);
            $(studentPerformanceStageWiseAndGenderWise.tbody).append(tbody);
			
		},
		fnShowError:function(currentElement){
			//$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(" ");
			var elements =[studentPerformanceStageWiseAndGenderWise.grade,studentPerformanceStageWiseAndGenderWise.subject,studentPerformanceStageWiseAndGenderWise.school];
			var message =[selectGradeMessage, selectSubjectMessage,selectSchoolMessage];
			var isErrorShwon =false;
			$.each(elements,function(i,obj){
				if(isErrorShwon==false && currentElement!=obj){
					if($(obj).find("option:selected").length==0){
						$(studentPerformanceStageWiseAndGenderWise.canvasSpan).text(message[i]);
						$(studentPerformanceStageWiseAndGenderWise.canvasSpan).show();
						isErrorShwon =true;
						return false;
					}
				}
			});

		}
}

////////////////////// 

var stageWiseAndGenderWiseInIt=function(){
	studentPerformanceStageWiseAndGenderWiseBarChart.fnBindChartTypeData(true);
	studentPerformanceStageWiseAndGenderWiseBarChart.fnBindDataTypeData(true);
	studentPerformanceStageWiseAndGenderWiseBarChart.fnUpdateBarChart();
}

/*$(document) .ready(
		function() {
			stageWiseAndGenderWiseInIt();
		});*/