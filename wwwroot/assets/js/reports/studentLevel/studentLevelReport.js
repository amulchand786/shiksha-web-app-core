//global selectDivIds for smart filter
var studentReportPageContext = new Object();
studentReportPageContext.city = "#divSelectCity";
studentReportPageContext.block = "#divSelectBlock";
studentReportPageContext.NP = "#divSelectNP";
studentReportPageContext.GP = "#divSelectGP";
studentReportPageContext.RV = "#divSelectRV";
studentReportPageContext.village = "#divSelectVillage";
studentReportPageContext.school = "#divSelectSchool";


//Smart filter //

//school location type on change...get locations...
$('#divSchoolLocType input:radio[name=locType]').change(function() {
	
	fnHideGrSecFile();
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	$('#tabPane').css('display','none');
	lObj =this;
	var typeCode =$(this).data('code');		
	$('.outer-loader').show();		
	reinitializeSmartFilter(elementIdMap);	
	$("#hrDiv").show();
	if(typeCode.toLowerCase() =='U'.toLowerCase()){
		showDiv($(studentReportPageContext.city),$(studentReportPageContext.resetLocDivCity));
		hideDiv($(studentReportPageContext.block),$(studentReportPageContext.NP),$(studentReportPageContext.GP),$(studentReportPageContext.RV),$(studentReportPageContext.village));
		displaySmartFilters($(this).data('code'),$(this).data('levelname'));
		positionFilterDiv("U");
		fnCollapseMultiselect();
	}else{
		hideDiv($(studentReportPageContext.city),$(studentReportPageContext.resetLocDivCity));
		showDiv($(studentReportPageContext.block),$(studentReportPageContext.NP),$(studentReportPageContext.GP),$(studentReportPageContext.RV),$(studentReportPageContext.village));
		displaySmartFilters($(this).data('code'),$(this).data('levelname'));
		positionFilterDiv("R");
		fnCollapseMultiselect();
	}
	$('.outer-loader').hide();
	return;
});

//hide grade,section,upload file div
var fnHideGrSecFile =function(){
	$('#divSelectGrade').hide();
	$('#divSelectSection').hide();
	$('#divSelectAcademicYear').hide();
	$('#divSelectStudentName').hide();
	$("#displayResult").hide();
	$('#rowDiv2').hide();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();
	$('input[name="chapterwise"]').prop('checked', false);
	$('#btnViewStudentReport').hide();
}


//get grade list on selecting school
var getGradeList = function() {
	$('#rowDiv2').hide();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();
	$('input[name="chapterwise"]').prop('checked', false);
	var schoolId = $("#selectBox_schoolsByLoc").val();
	var bintToElementId = $('#selectBox_grade');
	$('.outer-loader').show();
	
	bindGradesBySchoolToListBox(bintToElementId, schoolId, 0);
	setOptionsForMultipleSelect('#selectBox_grade');
	if($('#selectBox_grade').children('option').length==0){
		var schoolName=$('#selectBox_schoolsByLoc').find('option:selected').text();
		$('#msgText').text("School "+schoolName+" is not mapped to any Grade");
		$('#mdlError').modal();
	}else{
		$('#divSelectGrade').show();
	}
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}	

var getSectionList = function(){	
	$('#rowDiv2').hide();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();//hideTable
	$('input[name="chapterwise"]').prop('checked', false);
	$('#divSelectSection').hide();
	$('#btnViewStudentReport').hide();
	$('#divSelectStudentName').hide();
	$('#divSelectAcademicYear').hide();//hide academic year dropdown
	$("#displayResult").hide();
	$('#selectBox_Section').find("option:selected").prop('selected', false);
	$('#selectBox_Section').multiselect('refresh').multiselect('destroy');
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	bindToElementId = $('#selectBox_Section');
	$('.outer-loader').show();
	bindSourceBySchoolAndGradeToListBox(bindToElementId,schoolId,gradeId,0);
	setOptionsForMultipleSelect('#selectBox_Section');
	$('.outer-loader').hide();
	$('#divSelectSection').show();
	
	fnCollapseMultiselect();
}

var getAcademicYearList = function(){
	$('#rowDiv2').hide();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();//hideTable
	$('input[name="chapterwise"]').prop('checked', false);
	$('#divSelectAcademicYear').hide();
	$("#displayResult").hide();
	$('#btnViewStudentReport').hide();
	$('#selectBox_AcademicYear').find("option:selected").prop('selected', false);
	$('#selectBox_AcademicYear').multiselect('refresh').multiselect('destroy');
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId=parseInt($("#selectBox_Section").find('option:selected').data('id'));
	deleteAllRow('studentTable');
	bindToElementId = $('#selectBox_AcademicYear');
	var currentYear = customAjaxCalling("academicYear/currentAcademicYear", json,"GET");
	 bindAcademicYearToListBox(bindToElementId,currentYear.academicYearId);
	 setOptionsForMultipleSelect('#selectBox_AcademicYear');
		$('.outer-loader').hide();
		$('#divSelectAcademicYear').show();
}

var getStudentList = function(){
	$('#rowDiv2').hide();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();//hideTable
	$('input[name="chapterwise"]').prop('checked', false);
	$("#displayResult").hide();
	$('#divSelectStudentName').hide();
	$('#btnViewStudentReport').hide();
	$('#selectBox_StudentName').find("option:selected").prop('selected', false);
	$('#selectBox_StudentName').multiselect('refresh').multiselect('destroy');
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId=parseInt($("#selectBox_Section").find('option:selected').data('id'));
	var academicYearId=parseInt($("#selectBox_AcademicYear").find('option:selected').data('id'));
	deleteAllRow('studentTable');
	bindToElementId = $('#selectBox_StudentName');
	$('.outer-loader').show();
	bindStudentBySchoolGradeSectionAcaYearToListBox(bindToElementId,schoolId,gradeId,sectionId,academicYearId,0);
	setOptionsForMultipleSelect('#selectBox_StudentName');
	$('.outer-loader').hide();
	if($('#selectBox_StudentName').children('option').length==0){
		var schoolName=$('#selectBox_schoolsByLoc').find('option:selected').text();
		$('#mdlErrorHeader').text("Student Level Report Alert !!");
		$('#msgText').text("Students are not available in this academic year");
		$('#mdlError').modal();
	}else{
		$('#divSelectStudentName').show();
	}
}

var shoWSubmitButton = function(){
	$("#displayResult").hide();
	$('#rowDiv2').hide();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();
	$('input[name="chapterwise"]').prop('checked', false);
	var chapterwiseBtnValue="Yes";
	$("input[name=chapterwise][value=" + chapterwiseBtnValue + "]").prop('checked', true);
	$('#btnViewStudentReport').show();
}

var genrateRow=function(){
	$('#tableDiv').css('display','block');
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId=parseInt($("#selectBox_Section").find('option:selected').data('id'));
	var academicYearId=parseInt($("#selectBox_AcademicYear").find('option:selected').data('id'));
	var studentId=parseInt($("#selectBox_StudentName").find('option:selected').data('id'));
	var jsonData={
			"schoolId":schoolId,
			"gradeId":gradeId,
			"sectionId":sectionId,
			"academicYearId":academicYearId,
			"studentId" :studentId 
		}
	var studentlevelReportData = customAjaxCalling("report/studentlevel",jsonData,"POST");
	createStudentlevelReport(studentlevelReportData);
	$('#resultStudentId').text($("#selectBox_StudentName").find('option:selected').data('rollnumber'));
	$('#resultStudentName').text($("#selectBox_StudentName").find('option:selected').data('studentname'));
	$('#resultFatherName').text($("#selectBox_StudentName").find('option:selected').data('fathername'));
	$('#resultGrade').text($("#selectBox_grade").find('option:selected').data('gradename'));
	
	
}

var createStudentlevelReport=function(studentlevelReportData){
	deleteAllRow('studentLevelReportTable');
	$.each(studentlevelReportData, function(index,studentReportData) {	
		appendNewRow('studentLevelReportTable',getNewRow(studentReportData));
	});
	$("#displayResult").show();
	$("#rowDiv2").show();
	$('#rowDiv3').hide();
	$('#rowDiv4').hide();
	$('#studentLevelReportTable').show();
	fnColSpanIfDTEmpty('studentLevelReportTable',12);
	$('#reportfilter').removeClass('in');
	
}


var getNewRow =function(studentReportData){
	var totalMarks=studentReportData.totalMarksScored==null?"":studentReportData.totalMarksScored;
	var percentageScore=studentReportData.percentageScore==-1?"":studentReportData.percentageScore;
	var toUnderGoPa = (studentReportData.toUnderGoPA==null)?"" 
														: (studentReportData.toUnderGoPA==true)?studentReportData.toUnderGoPA="YES" 
															: studentReportData.toUnderGoPA="NO";
	var knowledgeScore=studentReportData.parameterScoringForAssessmentVm.scored_K==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_K;
	var understandingScore=studentReportData.parameterScoringForAssessmentVm.scored_U==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_U;
	var operationScore=studentReportData.parameterScoringForAssessmentVm.scored_O==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_O;
	var analysisScore=studentReportData.parameterScoringForAssessmentVm.scored_An==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_An;
	var applicationScore=studentReportData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentReportData.parameterScoringForAssessmentVm.scored_Ap;

var	row="<tr>"+
	"<td title='"+studentReportData.assessmentName+"' >"+studentReportData.assessmentName+"</td>"+
	"<td title='"+studentReportData.statusOfCompletion+"' >"+studentReportData.statusOfCompletion+"</td>"+
	"<td title='"+studentReportData.stageName+"' >"+studentReportData.stageName+"</td>"+
	"<td title='"+studentReportData.subjectName+"' >"+studentReportData.subjectName+"</td>"+
	"<td title='"+totalMarks+"' >"+totalMarks+"</td>"+
	"<td title='"+percentageScore+"' >"+percentageScore+"</td>"+
	"<td title='"+toUnderGoPa+"' >"+toUnderGoPa+"</td>"+
	"<td title='"+knowledgeScore+"' >"+knowledgeScore+"</td>"+
	"<td title='"+understandingScore+"' >"+understandingScore+"</td>"+
	"<td title='"+operationScore+"' >"+operationScore+"</td>"+
	"<td title='"+analysisScore+"' >"+analysisScore+"</td>"+
	"<td title='"+applicationScore+"' >"+applicationScore+"</td>"+
	"</tr>";
	return row;
	};
	
	$('#chapterwiseResult input:radio[name=chapterwise]').change(function() {
		
		fnClearCustomSearch(['.r1','.r2','.r3']);
				
		var name =$(this).data('name');	
		if (name=="scoreCard"){
			
			$("#search1").hide();
			$("#search3").hide();
			$("#search2").show();
			
			genrateScoreCardRow();
		}else if (name=="learningDimensionWise"){
			$("#search1").hide();
			$("#search2").hide();
			$("#search3").show();
			genrateBloomTaxonomyRow();
		}else{
			$("#search1").show();
			$("#search2").hide();
			$("#search3").hide();
			genrateRow();
		}
	});
	


var genrateScoreCardRow=function(){
	$("#rowDiv2").hide();
	$("#rowDiv3").show();
	$("#rowDiv4").hide();
	$('#scoreBoardTable').show();
	$('#scoreBoardTableDiv').css('display','block');
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId=parseInt($("#selectBox_Section").find('option:selected').data('id'));
	var academicYearId=parseInt($("#selectBox_AcademicYear").find('option:selected').data('id'));
	var studentId=parseInt($("#selectBox_StudentName").find('option:selected').data('id'));
	var jsonData={
			"schoolId":schoolId,
			"gradeId":gradeId,
			"sectionId":sectionId,
			"academicYearId":academicYearId,
			"studentId" :studentId 
		}
	var studentlevelScoreCardData = customAjaxCalling("report/studentlevel/scoreCard",jsonData,"POST");
	$("#scoreBoardHeader").children("tr").remove();
	$("#scoreBoardHeader").append(createTableForScoreCard(studentlevelScoreCardData.maxNoOfRows));
	createStudentlevelScoreCardReport(studentlevelScoreCardData.studentLevelReportVm);
	
	
}

var createTableForScoreCard=function(maxNumberOfColumns){
	maxNumberOfColumns= maxNumberOfColumns == null?1:maxNumberOfColumns;
	var noOfColmns=maxNumberOfColumns+6;
	var rowSpan='';
		for(var i=1;i<=maxNumberOfColumns;i++){
			rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
		}
	
	var	thead="<tr>"+
	"<th  class='nosort' rowspan='3'><p>Question Paper Name</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
	"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
	"</tr>"+
	"<tr>"+
	"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise scores</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Marks Scored</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Total Marks</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Capterwise % score</p></th>"+
	"<th  class='nosort' rowspan='2'><p>Total marks scored</p></th>"+
	"<th  class='nosort' rowspan='2'><p>% score</p></th>"+
	"</tr>"+
	"<tr>"+
	rowSpan
	"</tr>";
	return thead;
};


var createStudentlevelScoreCardReport=function(studentlevelScoreCardData){
	//deleteAllRow('scoreBoardTable');
	$("#scoreBoardTable tbody tr").remove();
	if(studentlevelScoreCardData!=null && studentlevelScoreCardData!=""){
	$.each(studentlevelScoreCardData, function(index,studentScoreCardData) {	
		//appendNewRow('scoreBoardTable',getScoreCardNewRow(studentScoreCardData));		
		$("#scoreBoardTable tbody").append(getScoreCardNewRow(studentScoreCardData));
		
	});
	}else{
	$("#rowDiv2").hide();
	$("#rowDiv3").show();
	$("#rowDiv4").hide();
	$('#scoreBoardTable').show();
	$("#scoreBoardTable tbody").append("<tr>"+"<td colspan='10' style='text-align:center'>No data available in table</td>"+"</tr>");

}
}


var getScoreCardNewRow =function(studentScoreCardData){
	var totalMarks=studentScoreCardData.totalMarksScored==null?"":studentScoreCardData.totalMarksScored;
	var percentageScore=studentScoreCardData.percentageScore==-1?"":studentScoreCardData.percentageScore;
	
	var rowSpan='';
	var count=studentScoreCardData.numberOfChapters;
	$.each(studentScoreCardData.chapterReportVmList, function(index,chapterVm) {	
		
		if(index > 0){
			rowSpan+= "<tr>"
			}
		rowSpan +="<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
		$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
			if(qScoreMap == -1){
				qScoreMap="";
			}
			 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
		});
		var chapterWisePercent=chapterVm.chapterWisePercentage==-1?"":chapterVm.chapterWisePercentage;
		rowSpan+="<td title='"+chapterVm.chapterWiseScore+"' >"+chapterVm.chapterWiseScore+"</td>"+
		"<td title='"+chapterVm.chapterWiseTotalMarks+"' >"+chapterVm.chapterWiseTotalMarks+"</td>"+
		"<td title='"+chapterWisePercent+"' >"+chapterWisePercent+"</td>";
		
		if(index==0){
		rowSpan+= "<td title='"+totalMarks+"' rowspan="+count+">"+totalMarks+"</td>"+
		"<td title='"+percentageScore+"' rowspan="+count+">"+percentageScore+"</td></tr>";
			}else{
			rowSpan+="</tr>";
		}
		
	});
	var	row="<tr>"+
	"<td title='"+studentScoreCardData.assessmentName+"' rowspan="+count+">"+studentScoreCardData.assessmentName+"</td>"+
	"<td title='"+studentScoreCardData.statusOfCompletion+"' rowspan="+count+">"+studentScoreCardData.statusOfCompletion+"</td>"+
	"<td title='"+studentScoreCardData.stageName+"' rowspan="+count+">"+studentScoreCardData.stageName+"</td>"+
	rowSpan;
	return row;
	};
	
var genrateBloomTaxonomyRow=function(){
	$("#rowDiv2").hide();
	$("#rowDiv3").hide();
	$("#rowDiv4").show();
	//$('#bloomTaxonomyTable').show();
	//$('#bloomTaxonomyTable').css('display','block');
	var schoolId = parseInt($("#selectBox_schoolsByLoc").find('option:selected').data('id'));
	var gradeId = parseInt($("#selectBox_grade").find('option:selected').data('id'));
	var sectionId=parseInt($("#selectBox_Section").find('option:selected').data('id'));
	var academicYearId=parseInt($("#selectBox_AcademicYear").find('option:selected').data('id'));
	var studentId=parseInt($("#selectBox_StudentName").find('option:selected').data('id'));
	var jsonData={
			"schoolId":schoolId,
			"gradeId":gradeId,
			"sectionId":sectionId,
			"academicYearId":academicYearId,
			"studentId" :studentId 
		}
	var studentlevelBloomTaxonomyData = customAjaxCalling("report/studentlevel/bloomTaxonomy",jsonData,"POST");
	console.log(studentlevelBloomTaxonomyData);
	$("#bloomTaxonomyHeader").children("tr").remove();
	$("#bloomTaxonomyHeader").append(createTableForBloomTaxonomy(studentlevelBloomTaxonomyData.maxNoOfRows));
	createStudentlevelBloomTaxonomyReport(studentlevelBloomTaxonomyData.studentLevelReportVm);
	
}
		
var createTableForBloomTaxonomy=function(maxNumberOfColumns){
	maxNumberOfColumns= maxNumberOfColumns == 0?1:maxNumberOfColumns;
	
	var noOfColmns=maxNumberOfColumns+5;
	var rowSpan='';
		for(var i=1;i<=maxNumberOfColumns;i++){
			rowSpan +="<th  class='nosort'><p>Q"+i+"</p></th>";
		}
	
	var	thead="<tr>"+
	"<th  class='nosort' rowspan='3'><p>Question Paper Name</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Status of Completion</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Stage</p></th>"+
	"<th  class='nosort' rowspan='3'><p>Chapter Names</p></th>"+
	"<th  class='nosort'  colspan='"+noOfColmns+"' style='text-align:center;'><p>Score Card</p></th>"+
	"</tr>"+
	"<tr>"+
	"<th  class='nosort' colspan='"+maxNumberOfColumns+"' ><p>Question wise Parameter Score</p></th>"+
	"<th  class='nosort' colspan='5'><p>Overall Parameter Score</p></th>"+
	"</tr>"+
	"<tr>"+
	rowSpan+
	"<th  class='nosort' ><p>K</p></th>"+
	"<th  class='nosort' ><p>U</p></th>"+
	"<th  class='nosort' ><p>O</p></th>"+
	"<th  class='nosort' ><p>AN</p></th>"+
	"<th  class='nosort' ><p>AP</p></th>"+
	"</tr>";
	return thead;
};	


var createStudentlevelBloomTaxonomyReport=function(studentlevelBloomTaxonomyData){
	//deleteAllRow('bloomTaxonomyTable');
	$("#bloomTaxonomyTable tbody tr").remove();
	if(studentlevelBloomTaxonomyData!=null && studentlevelBloomTaxonomyData!=""){
	$.each(studentlevelBloomTaxonomyData, function(index,studentBloomTaxonomyData) {	
		//appendNewRow('bloomTaxonomyTable',getBloomtaxonomyNewRow(studentBloomTaxonomyData));
		$("#bloomTaxonomyTable tbody").append(getBloomtaxonomyNewRow(studentBloomTaxonomyData));
	});
	}else{
	$("#rowDiv4").show();
	//$('#bloomTaxonomyTable').show();
	//fnColSpanIfDTEmpty('bloomTaxonomyTable',10);
	$("#bloomTaxonomyTable tbody").append("<tr>"+"<td colspan='10' style='text-align:center'>No data available in table</td>"+"</tr>");

}
}

	

var getBloomtaxonomyNewRow =function(studentBloomTaxonomyData){
	var knowledgeScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_K;
	var understandingScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_U;
	var operationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_O;
	var analysisScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_An;
	var applicationScore=studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap==null?"0":studentBloomTaxonomyData.parameterScoringForAssessmentVm.scored_Ap;
	
	var rowSpan='';
	var count=studentBloomTaxonomyData.numberOfChapters;
	rowSpan +="<th title='' ></th>";
	
	$.each(studentBloomTaxonomyData.taxonomyHeaderList, function(indexNumber,taxonomyHeader) {	
	 rowSpan +="<th title='"+taxonomyHeader+"' >"+taxonomyHeader+"</th>";
	});
	
	$.each(studentBloomTaxonomyData.taxonomyWeightAge, function(indexNumber,taxonomyWeightage) {	
		 rowSpan +="<th title='"+taxonomyWeightage+"' >"+taxonomyWeightage+"</th>";
		});
	
	rowSpan+="</tr>";

	$.each(studentBloomTaxonomyData.chapterReportVmList, function(index,chapterVm) {	
		rowSpan +="<tr>"+ "<td title='"+chapterVm.chapterName+"' >"+chapterVm.chapterName+"</td>";
		$.each(chapterVm.questionScore[0], function(indexNumber,qScoreMap) {	
			if(qScoreMap == -1){
				qScoreMap="";
			}
			 rowSpan +="<td title='"+qScoreMap+"' >"+qScoreMap+"</td>";
		});
		if(index==0){
		rowSpan+= "<td title='"+knowledgeScore+"' rowspan="+count+" >"+knowledgeScore+"</td>"+
		"<td title='"+understandingScore+"' rowspan="+count+">"+understandingScore+"</td>"+
		"<td title='"+operationScore+"' rowspan="+count+">"+operationScore+"</td>"+
		"<td title='"+analysisScore+"' rowspan="+count+">"+analysisScore+"</td>"+
		"<td title='"+applicationScore+"' rowspan="+count+">"+applicationScore+"</td>"+
		"</tr>";
			}else{
			rowSpan+="</tr>";
		}
		
	});
	var	row="<tr>"+
	"<td title='"+studentBloomTaxonomyData.assessmentName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.assessmentName+"</td>"+
	"<td title='"+studentBloomTaxonomyData.statusOfCompletion+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.statusOfCompletion+"</td>"+
	"<td title='"+studentBloomTaxonomyData.stageName+"' rowspan="+(count+1)+">"+studentBloomTaxonomyData.stageName+"</td>"+
	rowSpan;
	return row;
	};
	
// positions of the school-location divs
var positionFilterDiv = function(type) {
	if (type == "R") {
		$(studentReportPageContext.NP).insertAfter(
				$(studentReportPageContext.block));
		$(studentReportPageContext.RV).insertAfter($(studentReportPageContext.GP));
		$(studentReportPageContext.school).insertAfter(
				$(studentReportPageContext.village));
	} else {
		$(studentReportPageContext.school).insertAfter(
				$(studentReportPageContext.city));
	}
}

//displaying smart-filters
var displaySmartFilters =function(lCode,lLevelName) {
	$(".outer-loader").show();
	elementIdMap = {
			"State" : '#statesForZone',
			"Zone" : '#selectBox_zonesByState',
			"District" : '#selectBox_districtsByZone',
			"Tehsil" : '#selectBox_tehsilsByDistrict',
			"Block" : '#selectBox_blocksByTehsil',
			"City" : '#selectBox_cityByBlock',
			"Nyaya Panchayat" : '#selectBox_npByBlock',
			"Gram Panchayat" : '#selectBox_gpByNp',
			"Revenue Village" : '#selectBox_revenueVillagebyGp',
			"Village" : '#selectBox_villagebyRv',
			"School" : '#selectBox_schoolsByLoc'
	};
	displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));

	var ele_keys = Object.keys(elementIdMap);
	$(ele_keys).each(
			function(ind, ele_key) {
				$(elementIdMap[ele_key]).find("option:selected").prop(
						'selected', false);
				$(elementIdMap[ele_key]).multiselect('destroy');
				enableMultiSelect(elementIdMap[ele_key]);
			});
	setTimeout(function() {
		$(".outer-loader").hide();
		$('#rowDiv1,#divFilter').show();
	}, 100);
};

//remove and reinitialize smart filter
var reinitializeSmartFilter = function(eleMap) {
	var ele_keys = Object.keys(eleMap);
	$(ele_keys).each(function(ind, ele_key) {
		$(eleMap[ele_key]).find("option:selected").prop('selected', false);
		$(eleMap[ele_key]).find("option[value]").remove();
		$(eleMap[ele_key]).multiselect('destroy');
		enableMultiSelect(eleMap[ele_key]);
	});
}


//reset only location when click on reset button icon
var resetLocAndSchool = function(obj) {
	$("#mdlResetLoc").modal().show();
	resetButtonObj = obj;
}

//invoking on click of resetLocation confirmation dialog box
var doResetLocation = function() {
	$("#selectBox_grade").multiselect('destroy').multiselect('refresh');
	$("#selectBox_Section").multiselect('destroy').multiselect('refresh');
	$("#selectBox_AcademicYear").multiselect('destroy').multiselect('refresh');
	$("#selectBox_StudentName").multiselect('destroy').multiselect('refresh');
	$("#divSelectGrade").hide();
	$('#btnViewStudentReport').hide();
	$("#divSelectSection").hide();
	$("#divSelectAcademicYear").hide();
	$("#divSelectStudentName").hide();
	$("#displayResult").hide();
	$("#rowDiv2").hide();
	$("#rowDiv3").hide();
	$("#rowDiv4").hide();
	$('input[name="chapterwise"]').prop('checked', false);
	enableMultiSelect($("#selectBox_Section"));
	enableMultiSelect($("#selectBox_grade"));
	deleteAllRow("studentTable");
	if ($(resetButtonObj).parents("form").attr("id") == "listSudentForm") {
		reinitializeSmartFilter(elementIdMap);
		var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
		var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
		displaySmartFilters(aLocTypeCode,aLLevelName);
		//displaySmartFilters();
	}
	fnCollapseMultiselect();
}

$(function() {
	
	fnDT('#studentLevelReportTable');
	//$('#scoreBoardTable').dataTable();
	
	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
	});  
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
	});
	
	$('#rowDiv1').show();
	
	fnCustomSearchFilter("#studentLevelReportTableSearch",".r1")
	fnCustomSearchFilter("#scoreBoardTableSearch",".r2")
	fnCustomSearchFilter("#bloomTaxonomyTableSearch",".r3")
	
	
	//fnColSpanIfDTEmpty('studentLevelReportTable',12);
});


//custom search bar
function fnCustomSearchFilter(searchBox,tblResult){
	$(searchBox).keyup(function () {
		var searchTerm = $(searchBox).val();
		var listItem = $(tblResult+' tbody').children('tr');
		var searchSplit = searchTerm.replace(/ /g, "'):containsi('")
	
		$.extend($.expr[':'], {'containsi': function(elem, i, match, array){
			return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
		}
		});
	
		$(tblResult+" tbody td").removeAttr("style");
		if($(searchBox).val().length!=0){
			$(tblResult+" tbody td:containsi('" + searchSplit + "')").each(function(e){			
				$(this).css('background','yellow');
			});
		}else{
			$(tblResult+" tbody td").removeAttr("style");
		}
	
	});
}


var fnDT =function(tableId){	
	$(tableId).dataTable({
		"pagingType": "numbers",	
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"><"pull-right"><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		
		"bLengthChange": true,
		/*"scrollX": true,*/	
		"bDestroy": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"fnDrawCallback": function ( oSettings ) {/*
			 Need to redo the counters if filtered or sorted 
			if ( oSettings.bSorted || oSettings.bFiltered ){
				for ( var i=0, iLen=oSettings.aiDisplay.length ; i<iLen ; i++ ){
					$('td:eq(0)', oSettings.aoData[ oSettings.aiDisplay[i] ].nTr ).html( i+1 );
				}
			}
		*/},
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		//"aaSorting": [[ 1, 'asc' ]]
		"aaSorting": []
	});	
}