var pageContextElement = new Object();
pageContextElement.campaign 		=		"#selectBoxCampaign";
pageContextElement.source			=		"#selectBoxSource";
pageContextElement.stage			=		"#selectBoxStage";
pageContextElement.subject 			=		"#selectBoxSubject";
pageContextElement.questionPaper	=		"#selectBoxQuestionPaper";
pageContextElement.school 			=		"#selectBoxSchool";
pageContextElement.grade 			=		"#selectBoxGrade";
pageContextElement.section 			=		"#selectBoxSection";

var pageContextDiv = new Object();	
pageContextDiv.questionPaper		=		"#divQuestionPaper";
pageContextDiv.source				=		"#divSource";
pageContextDiv.stage				=		"#divStage";
pageContextDiv.subject				=		"#divSubject";
pageContextDiv.school				=		"#divSchool";
pageContextDiv.grade				=		"#divGrade";
pageContextDiv.section				=		"#divSection";
pageContextDiv.tableDiv				=		"#rowDiv2";

assessmentData="";


//get Question paperList list on selecting school
var getQuestionPaperList = function() {
	$(pageContextDiv.questionPaper).hide();
	$(pageContextDiv.source).hide(); 
	$(pageContextDiv.stage).hide();
	$(pageContextDiv.subject).hide();
	$(pageContextDiv.school).hide();
	$(pageContextDiv.grade).hide();
	$(pageContextDiv.section).hide();
	$(pageContextDiv.tableDiv).hide();
	var campaignId = $(pageContextElement.campaign).val();
	var bintToElementId = $(pageContextElement.questionPaper);
	$('.outer-loader').show();
	$(pageContextElement.questionPaper ).multiselect('destroy');	
	bindQuestionPaperToListBoxBycampaignId(bintToElementId, campaignId);
	setOptionsForMultipleSelect(pageContextElement.questionPaper);
	$(pageContextDiv.questionPaper).show();
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}	


//bind Question Paper to respective select box
var bindQuestionPaperToListBoxBycampaignId =function(bindToElementId,campaignId){
	var assessmentVmData = customAjaxCalling("assessmentplanner/report/"+campaignId, null, "POST");
	$select	=bindToElementId;	
	$select.html('');
	$.each(assessmentVmData,function(index,obj){
		$select.append('<option data-id="' + obj.assessmentId + '" value="' + obj.assessmentId + '">' +obj.assessmentName + '</option>');
	});			
};


//get SourceList  on selectingQuestion paper
var getSourceList = function() {
	$(pageContextDiv.source).hide(); 
	$(pageContextDiv.stage).hide();
	$(pageContextDiv.subject).hide();
	$(pageContextDiv.school).hide();
	$(pageContextDiv.grade).hide();
	$(pageContextDiv.section).hide();
	$(pageContextDiv.tableDiv).hide();
	var assessmentId = $(pageContextElement.questionPaper).val();
	var bintToElementId = $(pageContextElement.source);
	$('.outer-loader').show();
	$(pageContextElement.source ).multiselect('destroy');	
	bindSourceToListBoxByassessmentId(bintToElementId, assessmentId);
	setOptionsForMultipleSelect(pageContextElement.source);
	$(pageContextDiv.source).show(); 
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}	


//bind Source to respective select box
var bindSourceToListBoxByassessmentId =function(bindToElementId,assessmentId){
	assessmentData = customAjaxCalling("assessment/report/"+assessmentId, null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option data-id="' + assessmentData.sourceId + '" value="' + assessmentData.sourceId + '">' +assessmentData.sourceName + '</option>');
};


//get Stagelist  on selectingQuestion paper
var getStageList = function() {
	$(pageContextDiv.stage).hide();
	$(pageContextDiv.subject).hide();
	$(pageContextDiv.school).hide();
	$(pageContextDiv.grade).hide();
	$(pageContextDiv.section).hide();
	$(pageContextDiv.tableDiv).hide();
	var assessmentId = $(pageContextElement.questionPaper).val();
	var bintToElementId = $(pageContextElement.stage);
	$('.outer-loader').show();
	$(pageContextElement.stage ).multiselect('destroy');	
	bindStageToListBoxByassessmentId(bintToElementId, assessmentId);
	setOptionsForMultipleSelect(pageContextElement.stage);
	$(pageContextDiv.stage).show();
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}	


//bind stage to respective select box
var bindStageToListBoxByassessmentId =function(bindToElementId,assessmentId){
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option data-id="' + assessmentData.stageId + '" value="' + assessmentData.stageId + '">' +assessmentData.stageName + '</option>');
};


//get SubjectList  on selectingQuestion paper
var getSubjectList = function() {
	$(pageContextDiv.subject).hide();
	$(pageContextDiv.school).hide();
	$(pageContextDiv.grade).hide();
	$(pageContextDiv.section).hide();
	$(pageContextDiv.tableDiv).hide();
	var assessmentId = $(pageContextElement.questionPaper).val();
	var bintToElementId = $(pageContextElement.subject);
	$('.outer-loader').show();
	$(pageContextElement.subject ).multiselect('destroy');	
	bindSubjectToListBoxByassessmentId(bintToElementId, assessmentId);
	setOptionsForMultipleSelect(pageContextElement.subject);
	$(pageContextDiv.subject).show();
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}	


//bind stage to respective select box
var bindSubjectToListBoxByassessmentId =function(bindToElementId,assessmentId){
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option data-id="' + assessmentData.subjectId + '" value="' + assessmentData.subjectId + '">' +assessmentData.subjectName + '</option>');
};


//get school List list on selecting subject
var getSchoolList = function() {
	$(pageContextDiv.school).hide();
	$(pageContextDiv.grade).hide();
	$(pageContextDiv.section).hide();
	$(pageContextDiv.tableDiv).hide();
	var campaignId = $(pageContextElement.campaign).val();
	var bintToElementId = $(pageContextElement.school);
	$('.outer-loader').show();
	$(pageContextElement.school ).multiselect('destroy');	
	bindSchoolToListBoxBycampaignId(bintToElementId, campaignId);
	setOptionsForMultipleSelect(pageContextElement.school);
	$(pageContextDiv.school).show();
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}	


//bind Question Paper to respective select box
var bindSchoolToListBoxBycampaignId =function(bindToElementId,campaignId){
	var schoolVmData = customAjaxCalling("school/Report/"+campaignId, null, "GET");
	$select	=bindToElementId;	
	$select.html('');
	$.each(schoolVmData,function(index,obj){
		$select.append('<option data-id="' + obj.id + '" value="' + obj.id + '">' +obj.schoolName + '</option>');
	});			
};


//get GradeList  on selecting School
var getGradeList = function() {
	$(pageContextDiv.grade).hide();
	$(pageContextDiv.section).hide();
	$(pageContextDiv.tableDiv).hide();
	var assessmentId = $(pageContextElement.questionPaper).val();
	var bintToElementId = $(pageContextElement.grade);
	$('.outer-loader').show();
	$(pageContextElement.grade ).multiselect('destroy');	
	bindGradeToListBoxByassessmentId(bintToElementId, assessmentId);
	setOptionsForMultipleSelect(pageContextElement.grade);
	$(pageContextDiv.grade).show();
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}	


//bind Grade to respective select box
var bindGradeToListBoxByassessmentId =function(bindToElementId,assessmentId){
	$select	=bindToElementId;	
	$select.html('');
	$select.append('<option data-id="' + assessmentData.gradeId + '" value="' + assessmentData.gradeId + '">' +assessmentData.gradeName + '</option>');
};

//get Section List  on selecting Grade
var getSectionList = function(){	
	$(pageContextDiv.section).hide();
	$(pageContextDiv.tableDiv).hide();
	$(pageContextElement.section).multiselect('refresh').multiselect('destroy');
	var schoolId = parseInt($(pageContextElement.school).val());
	var gradeId = parseInt($(pageContextElement.grade).val());
	bindToElementId = $(pageContextElement.section);
	$('.outer-loader').show();
	bindSectionBySchoolAndGradeToListBox(bindToElementId,schoolId,gradeId,0);
	setOptionsForMultipleSelect(pageContextElement.section);
	$(pageContextDiv.section).show();
	$('.outer-loader').hide();
	fnCollapseMultiselect();
}

var bindSectionBySchoolAndGradeToListBox = function(bindToElementId,selectedSchoolId,selectedGradeId,selectedId){	
	json ={
			"schoolId": selectedSchoolId,
			"gradeId": selectedGradeId
	}
	var sections = customAjaxCalling("sectionlist", json,"POST");
	if(sections!=null || sections!=""){
		$select	=bindToElementId;
		$select.html('');
		$.each(sections,function(index,obj){
			$select.append('<option data-id="' + obj.sectionId + '"data-sectionname="' + obj.sectionName+ '"value="'+obj.sectionId+'">' +obj.sectionName+ '</option>');
		});
	}
}


var getStudentViewDetail = function(){
	var campaignId = $(pageContextElement.campaign).val();
	var assessmentId = $(pageContextElement.questionPaper).val();
	var schoolId = parseInt($(pageContextElement.school).val());
	var sectionId = parseInt($(pageContextElement.section).val());
	var consolidatedStatusList = customAjaxCalling("report/studentViewReport/"+campaignId+"/"+assessmentId+"/"+schoolId+"/"+sectionId, null,"GET");
	if(consolidatedStatusList!=null || consolidatedStatusList!=""){
		deleteAllRow('studentViewReportTable');
		$.each(consolidatedStatusList,function(index,obj){
			var score=(obj.marks!=null?obj.marks:"")
			var result="";
			var percentage="";
			var comment=(obj.teacherComments!=null?obj.teacherComments:"");
			if(obj.statusName=="Completed"){
				result=(obj.flagNeedsMoreTests=='true' || obj.flagNeedsMoreTests==true?"Below":"Above");
				percentage=obj.percentage+"%";
			}
			var msgStatus=	addClasToStatus(obj.statusName);
			var row =
				"<tr id='"+obj.consolidatedStatusId+'_'+obj.isAppearedLate+"' data-id='"+obj.roll+"'>"+
				"<td title='"+obj.roll+"'>"+obj.roll+"</td>"+
				"<td title='"+obj.studentName+"'>"+obj.studentName+"</td>"+
				"<td title='"+obj.statusName+"'>"+msgStatus+"</td>"+
				"<td title='"+score+"'>"+score+"</td>"+
				"<td title='"+percentage+"'>"+percentage+"</td>"+
				"<td title='"+comment+"'>"+comment+"</td>"+
				"<td title='"+result+"'>"+result+"</td>"+
				

				"</tr>";                                                                                                                                                                 
			appendNewRow('studentViewReportTable',row);
			//CR-653 implementation
			/*if(obj.isAppearedLate==true){
				$(row).addClass('appread-late-student');
			}*/
			
			totalNoOfQuestion =obj.totalNoOfQuestion;
		});
		$("#h4") .text("");
		$("#h4") .text("Score /"+totalNoOfQuestion);
		$(pageContextDiv.tableDiv).show();
		fnColSpanIfDTEmpty('studentViewReportTable',7);
	}
};


var addClasToStatus=function(msg){
	if(msg=="Yet to start"){
		return "<span class='label label-default text-uppercase'>"+msg+"</span>"
	} else if(msg=="In progress") {
		return "<span class='label label-warning text-uppercase'>"+msg+"</span>"
	} else if(msg=="Completed") {
		return "<span class='label label-success text-uppercase'>"+msg+"</span>"
	} else if(msg=="Skipped") {
		return "<span class='label label-info text-uppercase'>Absent</span>"
	}
}



var setDTPaginationForStuReport =function(tableId){	
	$(tableId).dataTable({
		"pagingType": "numbers",	
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		"bLengthChange": true,
		/*"scrollX": true,*/	
		"bDestroy": true,
		"autoWidth": false,
		"iDisplayLength": 100,
		"stateSave": false,
		"aoColumnDefs": [{
			'bSortable': false,
			'aTargets': ['nosort']
		}],
		//"aaSorting": [[ 1, 'asc' ]]
		"aaSorting": []
	});	
}





///////////////////////////
$(function() {
	setDTPaginationForStuReport('#studentViewReportTable');
	setOptionsForMultipleSelect(pageContextElement.campaign);
	setOptionsForMultipleSelect(pageContextElement.source);
	setOptionsForMultipleSelect(pageContextElement.stage);
	setOptionsForMultipleSelect(pageContextElement.subject);
	setOptionsForMultipleSelect(pageContextElement.questionPaper);
	setOptionsForMultipleSelect(pageContextElement.school);
	setOptionsForMultipleSelect(pageContextElement.grade);
	setOptionsForMultipleSelect(pageContextElement.section);

	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-minus-sign red gi-1p5x"></span></a>');
	});


	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	});

	$('#pnlMappingElements').on('show.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('Hide school configuration');
	});  
	$('#pnlMappingElements').on('hide.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('View school configuration');
	});
});








