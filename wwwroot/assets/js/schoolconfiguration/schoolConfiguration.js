
//global selectDivIds for smart filter
var schoolConfigurationPageContext = {};	
	schoolConfigurationPageContext.city 	="#divSelectCity";
	schoolConfigurationPageContext.block 	="#divSelectBlock";
	schoolConfigurationPageContext.NP 	="#divSelectNP";
	schoolConfigurationPageContext.GP 	="#divSelectGP";
	schoolConfigurationPageContext.RV 	="#divSelectRV";
	schoolConfigurationPageContext.village ="#divSelectVillage";
	schoolConfigurationPageContext.school ="#divSelectSchool";
	
	schoolConfigurationPageContext.elementSchool ="#selectBox_schoolsByLoc";
	schoolConfigurationPageContext.grade ="#selectBox_grades";
	
	schoolConfigurationPageContext.resetLocDivCity ="#divResetLocCity";
	schoolConfigurationPageContext.resetLocDivVillage ="#aSelectedSchooldivResetLocVillage";
	
	var lObj;
	var elementIdMap;
	//remove and reinitialize smart filter
	var reinitializeSmartFilter =function(eleMap){
		var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
			$(eleMap[ele_key]).find("option:selected").prop('selected', false);
			$(eleMap[ele_key]).find("option[value]").remove();
			$(eleMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(eleMap[ele_key]);
		});	
	}
	
	//displaying smart-filters
	var displaySmartFilters =function(lCode,lLevelName){
		$(".outer-loader").show();
		elementIdMap = {
			"State" : '#statesForZone',
			"Zone" : '#selectBox_zonesByState',
			"District" : '#selectBox_districtsByZone',
			"Tehsil" : '#selectBox_tehsilsByDistrict',
			"Block" : '#selectBox_blocksByTehsil',
			"City" : '#selectBox_cityByBlock',
			"Nyaya Panchayat" : '#selectBox_npByBlock',
			"Gram Panchayat" : '#selectBox_gpByNp',
			"Revenue Village" : '#selectBox_revenueVillagebyGp',
			"Village" : '#selectBox_villagebyRv',
			"School" : '#selectBox_schoolsByLoc'
		};
		displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));
		
		var ele_keys = Object.keys(elementIdMap);
		$(ele_keys).each(function(ind, ele_key){
			$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
			$(elementIdMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(elementIdMap[ele_key]);
		});	
		setTimeout(function(){
			$(".outer-loader").hide();
			$('#rowDiv1,#divFilter').show();
			}, 100);	
	};

	//arrange filter div based on location type
	//if rural, hiding city and moving elements to upper 
	var positionFilterDiv =function(type){
		if(type=="R"){
			$(schoolConfigurationPageContext.NP).insertAfter( $(schoolConfigurationPageContext.block));
			$(schoolConfigurationPageContext.RV).insertAfter( $(schoolConfigurationPageContext.GP));
			$(schoolConfigurationPageContext.school).insertAfter( $(schoolConfigurationPageContext.village));
		}else{
			$(schoolConfigurationPageContext.school).insertAfter( $(schoolConfigurationPageContext.city));
		}
	}
	
//school location type on change...get locations...
$('#divSchoolLocType input:radio[name=locType]').change(function() {
		$('#locColspFilter').removeClass('in');
		$('#locationPnlLink').text('Select Location');
		
		$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
		
		
		$('#tabPane').css('display','none');
		
		lObj =this;
		var typeCode =$(this).data('code');		
		$('.outer-loader').show();		
		reinitializeSmartFilter(elementIdMap);	
		$("#hrDiv").show();
		if(typeCode.toLowerCase() =='U'.toLowerCase()){
			showDiv($(schoolConfigurationPageContext.city),$(schoolConfigurationPageContext.resetLocDivCity));
			hideDiv($(schoolConfigurationPageContext.block),$(schoolConfigurationPageContext.NP),$(schoolConfigurationPageContext.GP),$(schoolConfigurationPageContext.RV),$(schoolConfigurationPageContext.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("U");
		}else{
			hideDiv($(schoolConfigurationPageContext.city),$(schoolConfigurationPageContext.resetLocDivCity));
			showDiv($(schoolConfigurationPageContext.block),$(schoolConfigurationPageContext.NP),$(schoolConfigurationPageContext.GP),$(schoolConfigurationPageContext.RV),$(schoolConfigurationPageContext.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("R");
		}
		
		fnCollapseMultiselect();
		
		$('.outer-loader').hide();
		return;
});
	


//@author:Debendra
/*
* data table for grouping rows , column filtering
* @params:
* 		t:tableId (string)
* 		nc: total no.of cols (number)
* 		xc: excluded cols (array)
* 		xs: X-axis scrool bar (boolean)
* 		hc : hide col index (number)
* 
* */
var fnRowGroupingDataTable =function(t,nc){	
		 	
	$(t).dataTable({
		"pagingType": "simple_numbers",		
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": false,	
		 "bDestroy": true,
		 "bFilter": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,		
		  
		    
		 "fnDrawCallback": function () {	           
			 var api = this.api();
	            var rows = api.rows( {page:'current'} ).nodes();
	            var last=null;
	 
	            api.column(4, {page:'current'} ).data().each( function ( group, i ) {
	                if ( last !== group ) {
	                    $(rows).eq( i ).before(
	                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
	                    );
	 
	                    last = group;
	                }
	            } );           
	        },
	        "order": [[ 4, "asc" ]],
	        "acolumnDefs": [
			                   { "visible": false, "targets": [4] },			                
			    ],
			    "aoColumnDefs": [{
			        'bSortable': false,
			        'aTargets': ['nosort']
			    }],
	});
	
	
}

var dtPagination =function(tableId){	
	$(tableId).dataTable({
		"pagingType": "simple_numbers",		
		"sDom": '<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": false,
		 /*"scrollX": true,*/	
		 "bDestroy": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,		 
	     "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		 }],
		 "aaSorting": [[ 1, 'asc' ]]
	});	
}

var fnAppendNewRow =function(tableName,newRow){
	var table =$("#"+tableName).DataTable();
	table.row.add($(newRow)).draw( false );	
}
fnColSpanIfDTEmpty('tblGradeSection',4);
fnColSpanIfDTEmpty('tblGradeSectionSubjectTeacher',5);


///////////////////////////
$(function() {	
	fnInitGradeSectionGlobalVars();
	fnInitGlobalVarsForSchGrdSecTeacherMapping();
	
	
	dtPagination('#tblGradeSection');
	dtPagination('#tblGradeSectionSubjectTeacher');
	
	fnColSpanIfDTEmpty('tblGradeSectionSubjectTeacher',5);
	fnColSpanIfDTEmpty('tblGradeSection',4);
	
	

	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-minus-sign red gi-1p5x"></span></a>');
		});
		
		
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	});
	
	$('#pnlMappingElements').on('show.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('Hide school configuration');
	});  
	$('#pnlMappingElements').on('hide.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('View school configuration');
	});
	$(document).on("change",".select-all-days",function(){
		if($(this).is(":checked")){
			$(this).closest("table").find("tbody .day-check").prop("checked",true).trigger("change");
		} else {
			$(this).closest("table").find("tbody .day-check").prop("checked",false).trigger("change");
		}
	});
	$(document).on("change",".day-check",function(){
		if($(this).closest("tbody").find(".day-check:not(:checked)").length){
			$(this).closest("table").find(".select-all-days").prop("checked",false);
		} else {
			$(this).closest("table").find(".select-all-days").prop("checked",true);
		}
	});
	
});








//////////////////////
//Smart filter		//
//////////////////////




//reset only location when click on reset button icon
var resetLocAndSchool =function(){	
	$("#mdlResetLoc").modal().show();
	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	//it is for smart filter utility..SHK-369
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	$('#tabGradeSecSubTeacher').parent().removeClass('active');
	$('#tabGradeSec').parent().addClass('active');
	
	
	
	reinitializeSmartFilter(elementIdMap);
	var aLocTypeCode =$('#divSchoolLocType input:radio[name=locType]:checked').data('code');
	var aLLevelName =$('#divSchoolLocType input:radio[name=locType]:checked').data('levelname');
	displaySmartFilters(aLocTypeCode,aLLevelName);
	$('#tabPane').css('display','none');
	$('#gradeSectionMapper').css('display','none');
	
	fnCollapseMultiselect();
	
}



//////////////////////
//Smart filter ended//
//////////////////////








