//global vars
var tNoOfGrades;
var tIsNewMaping;
var tIsEditMaping;

var tMappedTeacherIds;
var tMasterGradeIds;
var tMasterSectionIds;
var tMasterSubjectIds;
var tMasterTeacherIds;

var cDelTeacherId=0;



var fnInitGlobalVarsForSchGrdSecTeacherMapping =function(){
	tGresponse=0;
	tIsNewMaping =false;
	tIsEditMaping =false;
	tMappedTeacherIds =[];
	tMasterGradeIds =[];
	tMasterSectionIds =[];
	tMasterSubjectIds =[];
	tMasterTeacherIds =[];
	isGradeChange =false;
}







/*
 * get school grade section subject  mapping data on click of grade section subject teacher panel button
 * 
 * */
var fnGetSchoolGradeSectionSubjectTeacherMapping =function(){
	
	
	$('.outer-loader').show();
	setTimeout(function(){$('.outer-loader').hide();},3000);
	
		
	$('#gradeSectionMapper').css('display','none');
	$('#divInfo').css('display','none');
	$('#divInfoGrdSubTeacher').css('display','none');
	
	
	$('#gradeSectionSubjectTeacherMapping #teachersBySchool').find('div').remove();
	$('#gradeSectionSubjectTeacherMapping #gradesBySchool').find('div').remove();
	$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade').find('div').remove();
	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade').find('div').remove();
	fnRemoveOptionsFromGrdSecAndSubject();
	$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',true);
	
	tMappedTeacherIds =[];
	
	fnInitGlobalVarsForSchGrdSecTeacherMapping();


	fnGetAllMappingData();
	
	fnGetTeachersBySchool();
	
	fnGetGradesForTeacherMapping();
	
	fnUncheckAllRadiosInTable(); //uncheck all the radio button inside table 
	
	fnColSpanIfDTEmpty('tblGradeSectionSubjectTeacher',5);
}


//get all mappings data
var fnGetAllMappingData =function(){

	fnUncheckAllRadiosInTable();
	var tDataResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionsubjectteachermapping/"+aSelectedSchool, null, "GET");
	
	deleteAllRow('tblGradeSectionSubjectTeacher');
	if(tDataResponse!=null && tDataResponse!=""){
		$.each(tDataResponse, function(key, value){
		    $.each(value, function(key, value){
		    	fnCreateNewDataRowAfterRetrieve(value);
		    	if(value!=null && value!=""){
		    		if(value.teacherVm!=null && value.teacherVm!='')
		    			tMappedTeacherIds.push(value.teacherVm.userId);
		    	}
		    		
		    });
		});
	}
	
	//fnRowGroupingDataTable('#tblGradeSectionSubjectTeacher',5);
}

var fnCreateNewDataRowAfterRetrieve=function(obj){
/*	var tId=obj.teacherVm.userId;
	var tName =obj.teacherVm.name;
	var gName =obj.gradeVm.gradeName;
	var gId =obj.gradeVm.gradeId;
	var sName =obj.sectionName;
	var sIds =obj.sectionIds;
	var subId =obj.subjectVm.subjectId;
	var subName =obj.subjectVm.subjectName;*/
	$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',true);
	
	
	$.each(obj.grades,function(i,val){		
		if(val!=null && val!=''){
			var tId=obj.teacherVm.userId;
			var tName =obj.teacherVm.name;
			var gName =val.gradeName;
			var gId =val.gradeId;
			var sName =val.sectionName;
			var sIds =val.sectionIds;
			var subId =obj.subjectVm.subjectId;
			var subName =obj.subjectVm.subjectName;
			var chkboxId =tId+"_"+gId+"_"+subId;
			var chxBoxBtn="<td><center><input type='checkbox' class='fooGradeSubTeacher' value='1' name='fooGrdSubBy[1][]' onchange=fnEditGradeSectionSubjectTeacherMapper(&quot;"+tId+"&quot,&quot;"+gId+"&quot,&quot;"+sIds+"&quot,&quot;"+subId+"&quot;) id=tGrdSecSubTeachRowRadio"+(chkboxId)+"></center></td>"
			var deleteGSSTMBtn="<td></td>";
			if (addSchoolGradeSectionTeacherSubjectPermission == false){
				chxBoxBtn = chxBoxBtn="<td><center><input type='checkbox' class='fooGradeSubTeacher' value='1' name='fooGrdSubBy[1][]' disabled='disabled'></center></td>"
			}
			if (deleteSchoolGradeSectionTeacherSubjectPermission == true){
				deleteGSSTMBtn="<td><div class='div-tooltip'><a class='fooGradeSubTeacherDelete tooltip tooltip-top'  style='margin-left: 10px;' value='1' name='fooGrdSubBy[1][]' onclick=fnDeleteGradeSectionSubjectTeacherMapping(&quot;"+tId+"&quot,&quot;"+gId+"&quot,&quot;"+sIds+"&quot,&quot;"+subId+"&quot,&quot;"+chkboxId+"&quot;) id=tGrdSecSubTeachRowBtn"+(chkboxId)+"><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>"
				
			}
			var xDataRow =
				"<tr id='"+tId+"' data-gradeid='"+gId+"'  data-secids='"+sIds+"'data-subjectid='"+subId+"'>"+
				/*"<td><center><input type='radio' name='tGrdSecSubTeacherRadioRow' onchange=fnEditGradeSectionSubjectTeacherMapper(&quot;"+tId+"&quot,&quot;"+gId+"&quot,&quot;"+sIds+"&quot,&quot;"+subId+"&quot;) id=tGrdSecSubTeachRowRadio"+gName+"></center></td>"+*/
				/*"<td><center><input type='checkbox' class='fooGradeSubTeacher' value='1' name='fooGrdSubBy[1][]' onchange=fnEditGradeSectionSubjectTeacherMapper(&quot;"+tId+"&quot,&quot;"+gId+"&quot,&quot;"+sIds+"&quot,&quot;"+subId+"&quot;) id=tGrdSecSubTeachRowRadio"+(chkboxId)+"></center></td>"+ */
				chxBoxBtn+
				"<td data-teacherid='"+tId+"'	title='"+escapeHtmlCharacters(tName)+"'>"+tName+"</td>"+
				"<td data-gradeid='"+gId+"'	title='"+escapeHtmlCharacters(gName)+"'>"+gName+"</td>"+
				"<td data-sectionids='"+sIds+"'	title='"+escapeHtmlCharacters(sName)+"'>"+sName+"</td>"+
				"<td data-subjectid='"+subId+"'	title='"+escapeHtmlCharacters(subName)+"'>"+subName+"</td>"+
				deleteGSSTMBtn+
			"</tr>";
			fnAppendNewRow('tblGradeSectionSubjectTeacher',xDataRow);
		}
	});
	
	
	
	/*var xDataRow =
		"<tr id='"+tId+"' data-gradeid='"+gId+"'  data-secids='"+sIds+"'data-subjectid='"+subId+"'>"+
		"<td><input type='radio' name='tGrdSecSubTeacherRadioRow' onchange=fnEditGradeSectionSubjectTeacherMapper(&quot;"+tId+"&quot,&quot;"+gId+"&quot,&quot;"+sIds+"&quot,&quot;"+subId+"&quot;) id=tGrdSecSubTeachRowRadio"+gName+"></td>"+			
		"<td data-gradeid='"+gId+"'	title='"+escapeHtmlCharacters(gName)+"'>"+gName+"</td>"+
		"<td data-sectionids='"+sIds+"'	title='"+escapeHtmlCharacters(sName)+"'>"+sName+"</td>"+
		"<td data-subjectid='"+subId+"'	title='"+escapeHtmlCharacters(subName)+"'>"+subName+"</td>"+
		"<td hidden='true'data-teacherid='"+tId+"'	title='"+escapeHtmlCharacters(tName)+"'>"+tName+"</td>"+
	"</tr>";*/
	//return xDataRow;
}
var showSpinner=function(event){
	$('.outer-loader').show();
};
//////////////////SHK-1379  ////////////////
var fnDeleteGradeSectionSubjectTeacherMapping =function(tEditTeachId,tEditGradeId,tEditSectionIds,tEditSubjectId,chkboxId){
	$(".fooGradeSubTeacher").prop("checked",false);
	var chkBoxGrdSecSubTeachRow="#tGrdSecSubTeachRowRadio"+chkboxId;
	$(chkBoxGrdSecSubTeachRow).prop('checked',true);
	tIsEditMaping =true;
	tIsNewMaping =false;
	cDelTeacherId =tEditTeachId;
	fnGetSectionsByGrade(tEditGradeId);
	fntRemoveTeacherFromMasterGroupExceptThis(tEditTeachId);
	
	var tsTempSections =tEditSectionIds.split(',');
	var tsTempSecFooArr =[];
	$.each(tsTempSections,function(i,val){
		tsTempSecFooArr.push(parseInt(val));
	});
	var tsTempGrade=[];
	tsTempGrade.push(parseInt(tEditGradeId));
	
	var tsTempSubject=[];
	tsTempSubject.push(parseInt(tEditSubjectId));
	
	
	fnTChekcSubMaster(tsTempSubject);
	fnTChekcSecMaster(tsTempSecFooArr);
	fnTChekcGradeMaster(tsTempGrade);
	
	

	//after radio button changed to checkbox
	$(".fooGradeSubTeacherDelete").click(function() {
		 if ($(this).is(":checked")) {
		        var group = "input:checkbox[name='" + $(this).attr("name") + "']";
		        $(group).prop("checked", false);
		        $(this).prop("checked", true);
		        
		    } else {
		        $(this).prop("checked", false);
		    }
		});
		
		
	/*var mFlagIschecked =false;
	var tkEditRows = $("#gradeSectionSubjectTeacherMapping #tblGradeSectionSubjectTeacher").dataTable().fnGetNodes();
	$.each(tkEditRows,function(i,row){		
		var tkERowId = '#'+$(row).find('input').prop('id');
		if(mFlagIschecked!=true){
			if($(tkERowId).prop('checked')==false){
				mFlagIschecked =true;
			}	
		}		
	});
	if(mFlagIschecked ==false){
		  fnTUnchekcGradeMaster(tMasterGradeIds);
	      fnTUnchekcSubMaster(tMasterSubjectIds);
	      fnTUnchekcSecMaster(tMasterSectionIds);
	      fnTUncheckTeacherMaster(tMasterTeacherIds);
	      fnGetTeachersBySchool();
	      $('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade').css('display','block');
	  	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade').css('display','block');
	}*/
	fnUncheckAllMasters();
	
	
	$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',false);
	fnDisableWarningMessage();
	fnInvokeDeleteGradeSectionSubjectTeacherMapping();
}


//getAll teachers of the selected school
var fnGetTeachersBySchool =function(){
	
	fnRemoveOptionsFromGrdSecAndSubject();
	$('#gradeSectionSubjectTeacherMapping #gradesBySchool').find('div').remove();
	fnGetGradesForTeacherMapping();
	
	var tempTableTeacherIds =fnGetTeacherIdsFromTable(); //get all existing teacher ids from table
	
	var tTeacherResponse =customAjaxCalling("schoolconfiguration/school/teacher/"+aSelectedSchool,null,"GET");
	tNoOfTeachers = tTeacherResponse.length;
	tMasterTeacherIds =[];
	
/*	var tIsMoreTeacherFoo =false;*/
	if(tTeacherResponse!=null && tTeacherResponse!=""){
		if(tTeacherResponse[0]!=null && tTeacherResponse[0]!=''){
			if(tTeacherResponse[0].response=="shiksha-200"){
				$('#gradeSectionSubjectTeacherMapping #teachersBySchool').find('div').remove();				
				if(tIsEditMaping!=true){
					$.each(tTeacherResponse,function(i,aTeacherObj){
						var tTeachRadioElement ="<div class='divTeacher ' id=divTeacher"+aTeacherObj.userId+" title='"+aTeacherObj.name+"'><input type='radio' id='tTeacher"+aTeacherObj.userId+"'data-id='"+aTeacherObj.userId+"'value='"+aTeacherObj.name+"'name='tTeachers'><label style='font-weight: 400; cursor: hand;margin-left: 5px;' for='tTeacher"+aTeacherObj.userId+"'><span class='truncate' style='max-width: 133px;display: block;line-height: 1;'>"+aTeacherObj.name+"</span></label></div>";				
						$('#gradeSectionSubjectTeacherMapping #teachersBySchool').append(tTeachRadioElement);
						tMasterTeacherIds.push(aTeacherObj.userId);
					});
				}else{
					$.each(tTeacherResponse,function(i,aTeacherObj){
						var tTeachRadioElement ="<div class='divTeacher ' id=divTeacher"+aTeacherObj.userId+" title='"+aTeacherObj.name+"'><input type='radio' id='tTeacher"+aTeacherObj.userId+"'data-id='"+aTeacherObj.userId+"'value='"+aTeacherObj.name+"'name='tTeachers'><label style='font-weight: 400; cursor: hand;margin-left: 5px;' for='tTeacher"+aTeacherObj.userId+"'>"+aTeacherObj.name+"</label></div>";
						$('#gradeSectionSubjectTeacherMapping #teachersBySchool').append(tTeachRadioElement);
					});
				}
			}
		}
	}else{
		
	}
}



//get the grades based on school id and bind to grade box
var fnGetGradesForTeacherMapping =function(){

	fnRemoveOptionsFromGrdSecAndSubject();
	
	
	var tGresponse =customAjaxCalling("schoolconfiguration/schoolgradesectionmapper/grade/"+aSelectedSchool,null,"GET");
	tNoOfGrades = tGresponse.length;
	tMasterGradeIds =[];
	
	if(tGresponse!=null && tGresponse!=""){
		if(tGresponse[0].response=="shiksha-200"){
			$('#gradeSectionSubjectTeacherMapping #gradesBySchool').find('div').remove();
			$.each(tGresponse,function(i,aTgObj){
				var tgRadioElement ="<div class='divTg ' id=divTG"+aTgObj.gradeId+" title='"+aTgObj.gradeName+"'><input type='radio' id='tG"+aTgObj.gradeId+"'data-id='"+aTgObj.gradeId+"'value='"+aTgObj.gradeName+"'name='tGrades' onchange=fnIsGradeChange();fnGetSectionsByGrade('"+aTgObj.gradeId+"');><label style='font-weight: 400; cursor: hand;margin-left: 5px;' for='tG"+aTgObj.gradeId+"'><span class='truncate' style='max-width: 133px;display: block;line-height: 1;'>"+aTgObj.gradeName+"</span></label></div>";			
				$('#gradeSectionSubjectTeacherMapping #gradesBySchool').append(tgRadioElement);
				tMasterGradeIds.push(aTgObj.gradeId);
			});
		}
	}else if(tGresponse!=null && tGresponse.length==0){
		$('#subteacherinfo').text('There are no grade(s) mapped to the selected school.');
		$('#subteacherinfo').css('display','block');
		$('#divInfoGrdSubTeacher').css('display','block');
	}	
}



//track is grade change during edit time
var fnIsGradeChange =function(){
	isGradeChange =true;
	fnUncheckAllRadiosInTable();
	$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',true);
}
//track is grade change during edit time
var fnIsSectionChange =function(){
	isSecChange =true;
	$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',true);
}


//onchange of grade, get the corresponding sections and bind to DOM
var fnGetSectionsByGrade =function(tSelectedGrade){
	
	$('.outer-loader').show();
	setTimeout(function(){$('.outer-loader').hide();},3000);
	
	$('#divInfoGrdSubTeacher').css('display','none');
	
	
	$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade').css('display','block');
	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade').css('display','block');
	
	$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade').find('div').remove();
	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade').find('div').remove();
	
	//fnUncheckAllRadiosInTable();
	if(tIsEditMaping==true && isGradeChange!=true){
		tIsNewMaping =false;
	}else{
		tIsNewMaping =true;
		tIsEditMaping==false;
		isGradeChange =false;
	}
	
	
	var tSecResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionmapper/section/"+aSelectedSchool+"/"+tSelectedGrade,null,"GET");
	tNoOfSections = tSecResponse.length;
	tMasterSectionIds =[];
	
	if(tSecResponse!=null && tSecResponse!=""){
		if(tSecResponse[0]!=null){
			if(tSecResponse[0].response=="shiksha-200"){
				$.each(tSecResponse,function(i,aTSecObj){
					var tSecRadioElement ="<div class='divTSec ' id=divTSec"+aTSecObj.sectionId+" title='"+aTSecObj.sectionName+"'><input type='checkbox' id='tSec"+aTSecObj.sectionId+"'data-id='"+aTSecObj.sectionId+"'value='"+aTSecObj.sectionName+"'name='tSections' onchange='fnIsSectionChange();'><label style='font-weight: 400; cursor: hand;margin-left: 5px;' for='tSec"+aTSecObj.sectionId+"'><span class='truncate' style='max-width: 133px;display: block;line-height: 1;'>"+aTSecObj.sectionName+"</span></label></div>";
					$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade').append(tSecRadioElement);
					tMasterSectionIds.push(aTSecObj.sectionId);
				});
			}
		}
	}else{
		$('#subteacherinfo').text('There are no section(s) mapped to the selected grade.');
		$('#subteacherinfo').css('display','block');
		$('#divInfoGrdSubTeacher').css('display','block');
	}
		
	var tSubResponse =customAjaxCalling("schoolconfiguration/gradesubjectmapper/subject/"+tSelectedGrade,null,"GET");
	tNoOfSubjects = tSubResponse.length;
	tMasterSubjectIds =[];
	
	if(tSubResponse!=null && tSubResponse!=""){
		if(tSubResponse[0].response=="shiksha-200"){
			
			$.each(tSubResponse,function(i,aTSubObj){
				var tSubRadioElement ="<div class='divTSub ' id=divTSub"+aTSubObj.subjectId+" title='"+aTSubObj.subjectName+"'><input type='checkbox' id='tSub"+aTSubObj.subjectId+"'data-id='"+aTSubObj.subjectId+"'value='"+aTSubObj.subjectName+"'name='tSubjects'><label style='font-weight: 400; cursor: hand;margin-left: 5px;' for='tSub"+aTSubObj.subjectId+"'><span class='truncate' style='max-width: 133px;display: block;line-height: 1;'>"+aTSubObj.subjectName+"</span></label></div>";				
				$('#gradeSectionSubjectTeacherMapping #subjectsByGrade').append(tSubRadioElement);$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',true);
				tMasterSubjectIds.push(aTSubObj.subjectId);
			});
			fnDisableMappedSubjectsOfThisGrade(tSelectedGrade,tSubResponse.length);
		}
	}else if(tSubResponse!=null &&tSubResponse.length==0){
		$('#subteacherinfo').text('There are no subject(s) mapped to the selected grade.');
		$('#subteacherinfo').css('display','block');
		$('#divInfoGrdSubTeacher').css('display','block');
	}
}



//get all existed  teacher ids from table
var fnGetTeacherIdsFromTable =function(){
	var tnDataRows =$("#tblGradeSectionSubjectTeacher").dataTable().fnGetNodes();
	var tdDataTeacherRowIds =[];
	$.each(tnDataRows,function(i,row){
		tdDataTeacherRowIds.push(parseInt($(row).prop('id')));
	});
	return tdDataTeacherRowIds;	
}


//save teachergradesubcetmapping
//invoke save feature
var fnInvokeSaveGradeSectionSubjectTeacherMapping =function(event){$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',true);
	var tTableDataRows =$("#tblGradeSectionSubjectTeacher").dataTable().fnGetNodes();
	var nTRows =tTableDataRows!=null?tTableDataRows.length:0;
	if(tIsNewMaping==true){
		fnSaveNewGradeSectionSubjectTeacherMapping(event);	
	}else{
		fnSaveEditedGradeSectionSubjectTeacherMapping(event);
	}
	
}

//save new mappings
var fnSaveNewGradeSectionSubjectTeacherMapping =function(event){
	
	var tNewGradeId;
	var tNewSubjectIds=[];
	var tNewTeacherId;
	var tNewSectionIds =[];
	
	var tNewGrdFoo =false;var tNewSecFoo =false;var tNewSubFoo =false;var tNewTechFoo =false;
	
	$('#gradeSectionSubjectTeacherMapping #gradesBySchool input:radio[name=tGrades]').each(function(i,tgEle){
		if($(tgEle).prop('checked')==true){
			tNewGradeId =parseInt($(tgEle).data('id'));
			tNewGrdFoo =true;
		}
	});
	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade input:checkbox[name=tSubjects]').each(function(i,tSubEle){
		if($(tSubEle).prop('checked')==true){
			var tNewSubjectId =parseInt($(tSubEle).data('id'));
			tNewSubjectIds.push(tNewSubjectId);
			tNewSubFoo =true;
		}
	});
	$('#gradeSectionSubjectTeacherMapping #teachersBySchool input:radio[name=tTeachers]').each(function(i,tTeacherEle){$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',true);
		if($(tTeacherEle).prop('checked')==true){
			tNewTeacherId =parseInt($(tTeacherEle).data('id'));
			tNewTechFoo =true;
		}
	});
	$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade input:checkbox[name=tSections]').each(function(i,tSecEle){		
		if($(tSecEle).prop('checked')==true){
			var sSections =parseInt($(tSecEle).data('id'));
			tNewSectionIds.push(sSections);
			tNewSecFoo =true;
		}			
	});
	
	
	if(tNewSecFoo==true&&tNewTechFoo==true&&tNewSubFoo==true&&tNewGrdFoo==true){
		
		tNewSubjectIds =fnIsAbleToSaveNew(tNewTeacherId,tNewGradeId,tNewSubjectIds);
				if(tNewSubjectIds!=false){
					var tSaveNewRequestData ={
							"schoolId":aSelectedSchool,
							"subjectIds":tNewSubjectIds,
							"gradeId":tNewGradeId,
							"teacherId":tNewTeacherId,
							"sectionIds":tNewSectionIds
					};
		
					var tSaveNewResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionsubjectteachermapping/",tSaveNewRequestData, "POST");	
					if(tSaveNewResponse!=null){
						if(tSaveNewResponse.response =="shiksha-200"){
							//fnAppendNewRow('tblGradeSectionSubjectTeacher',fnCreateNewRowAfterAdd(tSaveNewResponse));
							//tMappedTeacherIds.push(tSaveNewResponse.teacherId);
							//$('#divGradeSectionSubjectTeacherBtnSave #success-msg').show();
							//$("#divGradeSectionSubjectTeacherBtnSave #success-msg").fadeOut(2000);
							setTimeout(function() {   //calls click event after a one sec
								 AJS.flag({
								    type: 'success',
								    title: 'Success!',
								    body: '<p>Information added</p>',
								    close :'auto'
								})
								}, 1000);
							fnGetAllMappingData();
						}else{
							$('#mdlError #msgText').text('');
							$('#mdlError #msgText').text(tSaveNewResponse.responseMessage);
							$('#mdlError').modal().show();
							return false;
						}
					}
					
					fnGetTeachersBySchool();
			
				}
	}else{
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("Please select all mapping options.");
		$('#mdlError').modal().show();
		return false;
	}
}


var fnIsAbleToSaveNew =function(pTId,pGid,pSubIds){
	var tnDataRows =$("#tblGradeSectionSubjectTeacher").dataTable().fnGetNodes();
	var existedSubIdsOfThisGrd =[];
	$.each(tnDataRows,function(i,row){
		var rId =parseInt($(row).prop('id'));
		var gId =parseInt($(row).data('gradeid'));
		if(rId==pTId && pGid==gId){
			var subId =parseInt($(row).data('subjectid'));
			var foo =$.inArray(subId, pSubIds);
			if(foo!=-1)
				existedSubIdsOfThisGrd.push(subId);
		}
	});
	// get comparison result:
	var result = isSameSet(pSubIds, existedSubIdsOfThisGrd);
	if((!result) ==false){
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("The selected teacher has been mapped to the selected grade and subject(s) of this school. Please select to edit/delete.");
		$('#mdlError').modal().show();
		return false;
	}else{
		pSubIds= pSubIds.filter(function(val) {
			  return existedSubIdsOfThisGrd .indexOf(val) == -1;
			});
		return pSubIds;
	}
	
}
//compare arrays:
var isSameSet = function(arr1, arr2){
  return  $(arr1).not(arr2).length === 0 && $(arr2).not(arr1).length === 0;  
}




//create a new row after adding new
var fnCreateNewRowAfterAdd =function(tNewAddedData){
	var tempGradeId =tNewAddedData.gradeVm!=null ?tNewAddedData.gradeVm.gradeId : -1;
	var tempSecIds =tNewAddedData.sectionIds;
	var tempSubjectId =tNewAddedData.subjectVm !=null ?tNewAddedData.subjectVm.subjectId : -1;;
	
	var tempGradeName =tNewAddedData.gradeVm!=null ?tNewAddedData.gradeVm.gradeName : '';
	var tempSubjectName =tNewAddedData.subjectVm!=null ?tNewAddedData.subjectVm.subjectName : '';
	
	var tempTeacherName =tNewAddedData.teacherVm!=null ?tNewAddedData.teacherVm.name : '';
	
	var tSchGrdSecSubTeachNewRow =
		"<tr id='"+tNewAddedData.teacherId+"' data-gradeid='"+tempGradeId+"'  data-secids='"+tempSecIds+"'data-subjectid='"+tempSubjectId+"'>"+
		"<td><input type='radio' name='tGrdSecSubTeacherRadioRow' onchange=fnEditGradeSectionSubjectTeacherMapper(&quot;"+tNewAddedData.teacherId+"&quot,&quot;"+tempGradeId+"&quot,&quot;"+tempSecIds+"&quot,&quot;"+tempSubjectId+"&quot;) id=tGrdSecSubTeachRowRadio"+tempGradeId+"></td>"+			
		"<td data-gradeid='"+tempGradeId+"'	title='"+escapeHtmlCharacters(tempGradeName)+"'>"+tempGradeName+"</td>"+
		"<td data-sectionids='"+tempSecIds+"'	title='"+escapeHtmlCharacters(tNewAddedData.sectionName)+"'>"+tNewAddedData.sectionName+"</td>"+
		"<td data-subjectid='"+tempSubjectId+"'	title='"+escapeHtmlCharacters(tempSubjectName)+"'>"+tempSubjectName+"</td>"+
		"<td hidden='true'data-teacherid='"+tNewAddedData.teacherId+"'	title='"+escapeHtmlCharacters(tempTeacherName)+"'>"+tempTeacherName+"</td>"+
	"</tr>";	
	return tSchGrdSecSubTeachNewRow;
}





///////////////////////////////////////////////////
// edit///////////////////////////////////////////

var fnEditGradeSectionSubjectTeacherMapper =function(tEditTeachId,tEditGradeId,tEditSectionIds,tEditSubjectId){	
	
	tIsEditMaping =true;
	tIsNewMaping =false;
	fnGetSectionsByGrade(tEditGradeId);
	fntRemoveTeacherFromMasterGroupExceptThis(tEditTeachId);
	
	var tsTempSections =tEditSectionIds.split(',');
	var tsTempSecFooArr =[];
	$.each(tsTempSections,function(i,val){
		tsTempSecFooArr.push(parseInt(val));
	});
	var tsTempGrade=[];
	tsTempGrade.push(parseInt(tEditGradeId));
	
	var tsTempSubject=[];
	tsTempSubject.push(parseInt(tEditSubjectId));
	
	
	fnTChekcSubMaster(tsTempSubject);
	fnTChekcSecMaster(tsTempSecFooArr);
	fnTChekcGradeMaster(tsTempGrade);
	
	

	//after radio button changed to checkbox
	$(".fooGradeSubTeacher").click(function() {
	    if ($(this).is(":checked")) {
	        var group = "input:checkbox[name='" + $(this).attr("name") + "']";
	        $(group).prop("checked", false);
	        $(this).prop("checked", true);
	        
	    } else {
	        $(this).prop("checked", false);
	    }
	});
	
	fnUncheckAllMasters();
	
	
	$('#gradeSectionSubjectTeacherMapping #btnGrdSecSubTeacherDelete').prop('disabled',false);
	fnDisableWarningMessage();
}



var fnSaveEditedGradeSectionSubjectTeacherMapping =function(){
	var tEditGradeId;
	var tEditSubjectIds=[];
	var tEditTeacherId;
	var tEditSectionIds =[];
	
	var tEditGrdFoo =false;var tEditSecFoo =false;var tEditSubFoo =false;var tEditTechFoo =false;
	
	$('#gradeSectionSubjectTeacherMapping #gradesBySchool input:radio[name=tGrades]').each(function(i,tgEle){
		if($(tgEle).prop('checked')==true){
			tEditGradeId =parseInt($(tgEle).data('id'));
			tEditGrdFoo =true;
		}
	});
	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade input:checkbox[name=tSubjects]').each(function(i,tSubEle){
		if($(tSubEle).prop('checked')==true){
			var tEditSubId =parseInt($(tSubEle).data('id'));
			tEditSubjectIds.push(tEditSubId);
			tEditSubFoo =true;
		}
	});
	$('#gradeSectionSubjectTeacherMapping #teachersBySchool input:radio[name=tTeachers]').each(function(i,tTeacherEle){
		if($(tTeacherEle).prop('checked')==true){
			tEditTeacherId =parseInt($(tTeacherEle).data('id'));
			tEditTechFoo =true;
		}
	});
	$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade input:checkbox[name=tSections]').each(function(i,tSecEle){		
		if($(tSecEle).prop('checked')==true){
			var sSections =parseInt($(tSecEle).data('id'));
			tEditSectionIds.push(sSections);
			tEditSecFoo =true;
		}			
	});
	
	
	if(tEditGrdFoo==true&&tEditSecFoo==true&&tEditSubFoo==true&&tEditTechFoo==true){
		var tSaveEditRequestData ={
				"schoolId":aSelectedSchool,
				"subjectIds":tEditSubjectIds,
				"gradeId":tEditGradeId,
				"teacherId":tEditTeacherId,
				"sectionIds":tEditSectionIds
		};
		var tEditResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionsubjectteachermapping/",tSaveEditRequestData, "PUT");	
		if(tEditResponse!=null){
			if(tEditResponse.response =="shiksha-200"){
				/*fnGetAllMappingData();*/
				//$('#divGradeSectionSubjectTeacherBtnSave #success-msg').show();
				//$("#divGradeSectionSubjectTeacherBtnSave #success-msg").fadeOut(2000);
				setTimeout(function() {   //calls click event after a one sec
					 AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>Information Updated</p>',
					    close :'auto'
					})
					}, 1000);
				fnGetSchoolGradeSectionSubjectTeacherMapping();
			}else{
				$('#mdlError #msgText').text('');
				$('#mdlError #msgText').text(tEditResponse.responseMessage);
				$('#mdlError').modal().show();
				return false;
			}
		}

	/*	var tempEditUnchkGrade =[];
		tempEditUnchkGrade.push(tEditGradeId);
		var tempEditUnchkSubject =[];
		tempEditUnchkSubject.push(tEditSubjectId);

		fntRemoveTeacherFromMasterGroup(tMappedTeacherIds);
		fnTUnchekcGradeMaster(tempEditUnchkGrade);
		fnTUnchekcSecMaster(tEditSectionIds);
		fnTUnchekcSubMaster(tempEditUnchkSubject);*/
	}else{
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("Please select all mapping options.");
		$('#mdlError').modal().show();
		return false;
	}
}


//////////////////Delete functionality//////////////////

var fnInvokeDeleteGradeSectionSubjectTeacherMapping =function(){
	$("#deleteGradeSectionSubTeacher #delEleCustom").css('display','block');
	hideDiv($('#deleteGradeSectionSubTeacher #alertdiv'));
	$("#deleteGradeSectionSubTeacher").find('#errorMessage').hide();
	$("#deleteGradeSectionSubTeacher").find('#exception').text('');
	$("#deleteGradeSectionSubTeacher").find('#buttonGroup').css('display','block');
	$("#deleteGradeSectionSubTeacher").find('#okButton').css('display','none');
	$('#deleteGradeSectionSubTeacher').modal().show();
}
var fnDoDeleteGradeSectionSubMapper =function(event){
		
	
	var tDelGradeId;
	var tDelSubjectId;
	var tDelTeacherId;
	var tDelSectionIds =[];
	
	$('#gradeSectionSubjectTeacherMapping #gradesBySchool input:radio[name=tGrades]').each(function(i,tgEle){
		if($(tgEle).prop('checked')==true){
			tDelGradeId =parseInt($(tgEle).data('id'));	
		}
	});
	tDelSubjectId = $(".fooGradeSubTeacher:checked").closest("tr").find("td:nth-child(5)").attr("data-subjectid");
	/*$('#gradeSectionSubjectTeacherMapping #subjectsByGrade input:checkbox[name=tSubjects]').each(function(i,tSubEle){
		if($(tSubEle).prop('checked')==true){
			tDelSubjectId =parseInt($(tSubEle).data('id'));		
		}
	});*/
	$('#gradeSectionSubjectTeacherMapping #teachersBySchool input:radio[name=tTeachers]').each(function(i,tTeacherEle){
		if($(tTeacherEle).prop('checked')==true){
			tDelTeacherId =parseInt($(tTeacherEle).data('id'));
		}
	});
	$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade input:checkbox[name=tSections]').each(function(i,tSecEle){		
		if($(tSecEle).prop('checked')==true){
			var sDelSections =parseInt($(tSecEle).data('id'));
			tDelSectionIds.push(sDelSections);
		}			
	});
	
	tDelTeacherId =cDelTeacherId;
	var tDelRequestData ={
			"schoolId":aSelectedSchool,
			"subjectId":tDelSubjectId,
			"gradeId":tDelGradeId,
			"teacherId":tDelTeacherId,
			"sectionIds":tDelSectionIds
	};

	var tDeleteResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionsubjectteachermapping/",tDelRequestData, "DELETE");	
	if(tDeleteResponse!=null){
		if(tDeleteResponse.response =="shiksha-200"){
			cDelTeacherId=0;
			$('#deleteGradeSectionSubTeacher').modal('hide');
			//$('#divGradeSectionSubjectTeacherBtnSave #success-msg').show();
			//$("#divGradeSectionSubjectTeacherBtnSave #success-msg").fadeOut(2000);
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>Information Deleted</p>',
				    close :'auto'
				})
				}, 1000);
			fnGetSchoolGradeSectionSubjectTeacherMapping();
		}else if(tDeleteResponse.response =="shiksha-800"){
			//cDelTeacherId=0;
			$('#deleteGradeSectionSubTeacher').modal('hide');
			 setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'warning',
				    title: 'Info!',
				    body: '<p>'+tDeleteResponse.responseMessage+'</p>',
				    close :'auto'
				})
				}, 1000);
			fnGetSchoolGradeSectionSubjectTeacherMapping();
		}else{			
			showDiv($('#deleteGradeSectionSubTeacher #alertdiv'));
			$("#deleteGradeSectionSubTeacher").find('#successMessage').hide();			
			$("#deleteGradeSectionSubTeacher").find('#errorMessage').show();
			$("#deleteGradeSectionSubTeacher").find('#exception').show();
			$("#deleteGradeSectionSubTeacher").find('#exception').text(tDeleteResponse.responseMessage);
		}
		
	}else{
		fnGetSchoolGradeSectionSubjectTeacherMapping();
	}
}




////////////remove///////////////////////

//remove teacher from teacher master radio group except this teacher for edit purpose
var fntRemoveTeacherFromMasterGroupExceptThis =function(pEditMappedTeacherId){	
	$('.divTeacher').each(function(i,ele){
		var aaTid =$(ele).prop('id');
		var aCompareId="divTeacher"+pEditMappedTeacherId;
		var adInputId ="#tTeacher"+pEditMappedTeacherId;
		if(aaTid==aCompareId){
			$(adInputId).prop('checked','true');
			baTid ="#"+aaTid;
			$(baTid).css('display','block');
		}else{
			aaTid ="#"+aaTid;
			$(aaTid).css('display','none');
		}
	});
}
//remove teacher from teacher master radio group once it has been mapped
var fntRemoveTeacherFromMasterGroup =function(pMappedTeacherIds){
	$.each(pMappedTeacherIds,function(tIdx,tVal){
		var tIsExistTeacherId =$.inArray(tVal,pMappedTeacherIds);
		var taTeachId ='#gradeSectionSubjectTeacherMapping #teachersBySchool #divTeacher'+tVal;
		if(tIsExistTeacherId !=-1){						
			$(taTeachId).css('display','none');
		}else{
			$(taTeachId).css('display','block');
		}		
	});
}


///////////////////////
//remove all childs options  of grade......
var fnRemoveOptionsFromGrdSecAndSubject=function(){
	$('#gradeSectionSubjectTeacherMapping #gradesBySchool').find('div').remove();
	$('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade').find('div').remove();
	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade').find('div').remove();
}

////////


//uncheck grade after add new success

/*uncheck grades from grade master panel*/ 
var fnTUnchekcGradeMaster =function(tPGradeList){
	$.each(tPGradeList,function(taIdx,taVal){
		var tIsExistGradeId =$.inArray(taVal,tPGradeList);		
		var tbGid ='#gradeSectionSubjectTeacherMapping #gradesBySchool #tG'+taVal;
		$(tbGid).prop('checked',false);
	});
}

/*check grades from grade master panel*/ 
var fnTChekcGradeMaster =function(tPChkGradeList){
	$.each(tMasterGradeIds,function(taIdx,taVal){
		var tIsExistGradeId =$.inArray(taVal,tPChkGradeList);
		var tmGid ='#gradeSectionSubjectTeacherMapping #gradesBySchool #tG'+taVal;
		if(tIsExistGradeId !=-1){						
			$(tmGid).prop('checked',true);
		}else{
			$(tmGid).prop('checked',false);
		}	

	});
}

/*uncheck sections from section master panel*/ 
var fnTUnchekcSecMaster =function(tPSectionList){
	$.each(tPSectionList,function(tbIdx,tbVal){
		var tIsExistSecId =$.inArray(tbVal,tPSectionList);		
		var tbSecid ='#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade #tSec'+tbVal;
		$(tbSecid).prop('checked',false);	
	});
}
/*check sections from section master panel*/ 
var fnTChekcSecMaster =function(tPChkSectionList){
	$.each(tMasterSectionIds,function(tbIdx,tbVal){		
		var tIsExistSecId =$.inArray(tbVal,tPChkSectionList);
		var tbChkSecid ='#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade #tSec'+tbVal;
		if(tIsExistSecId !=-1){						
			$(tbChkSecid).prop('checked',true);
		}else{
			$(tbChkSecid).prop('checked',false);
		}
	});
}

/*uncheck subjects from subject master panel*/ 
var fnTUnchekcSubMaster =function(tPSubjectList){
	$.each(tPSubjectList,function(tcIdx,tcVal){
		var tIsExistSubId =$.inArray(tcVal,tPSubjectList);
		var tbSubid ='#gradeSectionSubjectTeacherMapping #subjectsByGrade #tSub'+tcVal;
		$(tbSubid).prop('checked',false);
	});
}

/*check subjects from subject master panel*/ 
var fnTChekcSubMaster =function(tPChkSubjectList){
	$.each(tMasterSubjectIds,function(tcIdx,tcVal){
		var tIsExistSubId =$.inArray(tcVal,tPChkSubjectList);
		var tbChkSubid ='#gradeSectionSubjectTeacherMapping #subjectsByGrade #tSub'+tcVal;
		var tbSubDiv ='#divTSub'+tcVal;
		if(tIsExistSubId !=-1){						
			$(tbChkSubid).prop('checked',true);
			$(tbSubDiv).css('display','block');
		}else{
			$(tbSubDiv).css('display','none');
		}
	});
}



//uncehck all the radio buttons inside table
var fnUncheckAllRadiosInTable =function(){
	//uncheck all radios in table
	var tkEditRows = $("#gradeSectionSubjectTeacherMapping #tblGradeSectionSubjectTeacher").dataTable().fnGetNodes();
	$.each(tkEditRows,function(i,row){		
		var tkERowId = '#'+$(row).find('input').prop('id');
		$(tkERowId).prop('checked',false);
	});
}

var fnTUncheckTeacherMaster =function(pMasterTeacherIds){
	$.each(pMasterTeacherIds,function(tcIdx,tcVal){		
		var tbTeacherId ='#gradeSectionSubjectTeacherMapping #teachersBySchool #tTeacher'+tcVal;
		$(tbTeacherId).prop('checked',false);
	});	
}





//uncehck all masters if nothig seleceted in table
var fnUncheckAllMasters =function(){
	var mFlagIschecked =false;
	var tkEditRows = $("#gradeSectionSubjectTeacherMapping #tblGradeSectionSubjectTeacher").dataTable().fnGetNodes();
	$.each(tkEditRows,function(i,row){		
		var tkERowId = '#'+$(row).find('input').prop('id');
		if(mFlagIschecked!=true){
			if($(tkERowId).prop('checked')==true){
				mFlagIschecked =true;
			}	
		}		
	});
	if(mFlagIschecked ==false){
		  fnTUnchekcGradeMaster(tMasterGradeIds);
	      fnTUnchekcSubMaster(tMasterSubjectIds);
	      fnTUnchekcSecMaster(tMasterSectionIds);
	      fnTUncheckTeacherMaster(tMasterTeacherIds);
	      fnGetTeachersBySchool();
	      $('#gradeSectionSubjectTeacherMapping #sectionsBySchoolGrade').css('display','none');
	  	$('#gradeSectionSubjectTeacherMapping #subjectsByGrade').css('display','none');
	}
}



//disable subjects of this grade if already mapped to any teacher of this grade of selected scholl
var fnDisableMappedSubjectsOfThisGrade =function(pSelectedGrade , pNoOfSubOfThisGrade){	
	var tnDataRows = $("#gradeSectionSubjectTeacherMapping #tblGradeSectionSubjectTeacher").dataTable().fnGetNodes();	
	var nDisableIds =[];
	$.each(tnDataRows,function(i,row){		
		var tsGradeId = parseInt($(row).data('gradeid'));
		
		if(tsGradeId==pSelectedGrade){
			var tsSubId = parseInt($(row).data('subjectid'));
			nDisableIds.push(tsSubId);
		}
	});
	fnDisableSubjectMaster(nDisableIds,pNoOfSubOfThisGrade);
}

var fnDisableSubjectMaster =function(pDisableSubId,aNoOfSubOfThisGrade){
	var aDisabledSubCount=0;
	var aDisableSubUniqueIds =$.unique(pDisableSubId);
	$.each(tMasterSubjectIds,function(tcIdx,tcVal){
		var tbIsExistSubId =$.inArray(tcVal,aDisableSubUniqueIds);
		var tbSubid ='#gradeSectionSubjectTeacherMapping #subjectsByGrade #tSub'+tcVal;
		var tbSubDiv ='#divTSub'+tcVal;
		if(tbIsExistSubId !=-1){						
			//$(tbSubDiv).css('display','none');
			aDisabledSubCount =aDisabledSubCount+1;
		}
	});
	if(aNoOfSubOfThisGrade ==aDisableSubUniqueIds.length){
		$('#subteacherinfo').text('Teachers mapped for all grades, sections and subjects. To edit, select a record in the table below.');
		$('#subteacherinfo').css('display','block');
		$('#divInfoGrdSubTeacher').css('display','block');	
	}
	
}


//disable warning callout
var fnDisableWarningMessage =function(){
	$('#divInfoGrdSubTeacher').css('display','none');
}

