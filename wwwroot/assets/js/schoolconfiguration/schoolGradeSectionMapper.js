//global vars
var aGradeMasterList;
var aSectionMasterList;
var aSelectedSchool;
var aSchGrdSecExistedGradeIds;
var aSchGrdSecExistedSecIds;
var aIsNewGrSec ;
var aIsEditGrSec;
var days=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
//get all grade master ids and push to an array
var fnGetAllGradeMastersForGradeSectionMapper =function(){
	aGradeMasterList=[];
	$('#gradeSectionMapper #gradeMasters').find('input').each(function(id,ele){
		var aGid =$(ele).prop('id').split('G')[1];
		aGradeMasterList.push(parseInt(aGid));
	});
}
//get all grade section ids and push to an array
var fnGetAllSectionMastersForGradeSectionMapper =function(){
	aSectionMasterList=[];
	$('#gradeSectionMapper #sectionMasters').find('input').each(function(id,ele){	
		var aSecid =$(ele).prop('id').split('Sec')[1];
		aSectionMasterList.push(parseInt(aSecid));
	});
}
var fnInitGradeSectionGlobalVars =function(){
	aSelectedSchool ='';
	aSchGrdSecExistedGradeIds =[]; //for getting the existed ids of grade from table and used to remove the grade masters radios
	aSchGrdSecExistedSecIds =[]; //for getting the existed ids of sections from table and used to remove the section masters radios
	
	
	fnGetAllGradeMastersForGradeSectionMapper();
	fnGetAllSectionMastersForGradeSectionMapper();
	
	aIsNewGrSec =false;
	aIsEditGrSec =false;
}


var showSpinner=function(){
	$('.outer-loader').show();
};

var fnDisplayGradeSectionMasters =function(){
	$('#pnlGradeMasters').css('display','block');
	$('#pnlSectionMasters').css('display','block');
	$('#sectionMasters').css('display','block');
	$('#gradeMasters').css('display','block');
	if(!$('#daysTable [type="checkbox"]').is(':checked')){
		$.each($("#daysTable [type='checkbox']"),function(){
			$(this).parent().siblings().find('.startTime').prop('disabled',true);
			$(this).parent().siblings().find('.endTime').prop('disabled',true);
		})
	}
	$('#divGradeSectionBtnSave').css('display','block');
}

//check or uncheck sections from section master panel on basis of existed sections list of this school grade
var fnCheckOrUnchekcSecMaster =function(pSchGrdSecExistedIds){
	$.each(aSectionMasterList,function(idx,val){
		var aaSecIsExist =$.inArray(val,pSchGrdSecExistedIds);		
		var xSecid ='#gradeSectionMapper #sectionMasters #Sec'+val;
		if(aaSecIsExist !=-1){						
			$(xSecid).prop('checked',true);
		}else{
			$(xSecid).prop('checked',false);
		}		
	});
}


//check or uncheck grades from grade master panel 
var fnCheckOrUnchekcGradeMaster =function(pxSchGrdExistedIds){
	$.each(aGradeMasterList,function(idx,val){
		var agIsExist =$.inArray(val,pxSchGrdExistedIds);		
		var cGid ='#gradeSectionMapper #gradeMasters #G'+val;
		if(agIsExist !=-1){						
			$(cGid).prop('checked',true);
		}else{
			$(cGid).prop('checked',false);
		}		
	});
}


//enable or disable grades from grade master panel on basis of existed grade list of this school
var fnDisableOrEnableGradeMaster =function(pSchGrdExistedIds){
	$.each(aGradeMasterList,function(idx,val){
		var gIsExist =$.inArray(val,pSchGrdExistedIds);
		var xGid ='#gradeSectionMapper #gradeMasters #divG'+val;
		if(gIsExist !=-1){						
			$(xGid).css('display','none');
		}else{
			$(xGid).css('display','block');
		}		
	});
}

//create schoolGradeSection new Row
var createSchoolGradeSectionRow =function(bData){
	
	var chBoxBtn =	"<td><center><input type='checkbox' class='fooGrade' value='1' name='fooby[1][]' onchange=fnGradeSectionEdit(&quot;"+bData.schoolGradeSectionMapperId+"&quot;,&quot;"+bData.sectionIds+"&quot;) id=grdSecRowRadio"+bData.gradeVm.gradeId+"></center></td>";
	
	if(!addSchoolGradeSectionPermission){
	 chBoxBtn =	"<td><center><input type='checkbox' class='fooGrade' disabled='disabled' value='1' name='fooby[1][]' ></center></td>";
	}

	 var deleteSGSBtn="<td></td>";
	if(deleteSchoolGradeSectionPermission){
		  deleteSGSBtn =	"<td><div class='div-tooltip'><a class='fooGradeDelete tooltip tooltip-top' value='1' style='margin-left: 10px;'  onclick=fnDeleteGradeSection(&quot;"+bData.gradeVm.gradeId+"&quot,&quot;"+bData.sectionIds+"&quot;) id=grdSecRowBtn"+bData.gradeVm.gradeId+"><span class='tooltiptext'>Delete</span><span class='glyphicon glyphicon-trash font-size12' ></span></a></div></td>";
			
	}	
	var zSgsecNewRow =
			"<tr id='"+bData.schoolGradeSectionMapperId+"' ids='"+bData.schoolGradeSectionMapperId+"' data-id='"+bData.schoolGradeSectionMapperId+"' data-schoolid='"+bData.schoolID+"' data-gradeid='"+bData.gradeVm.gradeId+"' data-secDays='"+bData.sectionDays+"'  data-secids='"+bData.sectionIds+"'data-isdelete_able='"+bData.isDeleteable+"'>"+
			/*"<td><center><input type='checkbox' class='fooGrade' value='1' name='fooby[1][]' onchange=fnGradeSectionEdit(&quot;"+bData.gradeVm.gradeId+"&quot,&quot;"+bData.sectionIds+"&quot;) id=grdSecRowRadio"+bData.gradeVm.gradeId+"></center></td>"+			*/
			chBoxBtn+
			"<td data-gradeid='"+bData.gradeVm.gradeId+"'	title='"+escapeHtmlCharacters(bData.gradeVm.gradeName)+"'>"+bData.gradeVm.gradeName+"</td>"+
			"<td data-gradeid='"+bData.gradeVm.gradeId+"'	title='"+escapeHtmlCharacters(bData.gradeVm.gradeCode)+"'>"+bData.gradeVm.gradeCode+"</td>"+
			"<td data-sectionids='"+bData.sectionIds+"'	title='"+escapeHtmlCharacters(bData.sectionNames)+"'>"+bData.sectionNames+"</td>"+	
			deleteSGSBtn+		
		"</tr>";
		return zSgsecNewRow;
};

//
var fnDisplayAlertInfo =function(){
	var acDataRows =$("#tblGradeSection").dataTable().fnGetNodes();
	var ncRows =acDataRows!=null?acDataRows.length:0;
	if(ncRows==totalGrades){
		$('#info').text('All grades have been mapped to sections for the selected school. To edit, select a grade in the table below.');
		$('#info').css('display','block');
		$('#divInfo').css('display','block');
	}
}


//disable sections from panel if all grades has been mapped
var fnDisableGradeSectionsAfterAllMapped =function(){
	var adDataRows =$("#tblGradeSection").dataTable().fnGetNodes();
	var ndRows =adDataRows!=null?adDataRows.length:0;
	if(ndRows==totalGrades){
		$('#pnlGradeMasters').css('display','none');
		$('#pnlSectionMasters').css('display','none');
		
		$('#sectionMasters').css('display','none');
		$('#gradeMasters').css('display','none');
		$('#divGradeSectionBtnSave').css('display','none');
	}
}

/*
 * get school grade section mapping data on click of grade section panel button
 * 
 * */
var fnGetSchoolGradeSectionMapping =function(){
		
	$('.outer-loader').show();
	setTimeout(function(){$('.outer-loader').hide();},3000);
	
	
	deleteAllRow("tblGradeSection");
	
	//uncheck all sections in section master
	fnCheckOrUnchekcSecMaster([]);
	fnResetDays();
	//uncheck all sections in grade master
	fnCheckOrUnchekcGradeMaster([]);

	$('#gradeSectionMapper').css('display','block');
	$('#divGradeSectionBtnSave #btnGradeSectionDelete').prop('disabled',true);
	$('#gradeSectionMapper #divGradeSectionBtnSave').css('display','block');
	
	aSelectedSchool =$(schoolConfigurationPageContext.elementSchool).find('option:selected').data('id');
	
	var aResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionmapper/"+aSelectedSchool, null,"GET");
	
	aSchGrdSecExistedGradeIds =[];
	if(aResponse!=null && aResponse!=""){
		if(aResponse[0].response=="shiksha-200"){			
			$(aResponse).each(function(index,dataObj){
				if(dataObj.gradeVm!=null){
					fnAppendNewRow('tblGradeSection',createSchoolGradeSectionRow(dataObj));
					aSchGrdSecExistedGradeIds.push(parseInt(dataObj.gradeVm.gradeId));
				}
			});
		}
	}	
	fnDisableOrEnableGradeMaster(aSchGrdSecExistedGradeIds);
	var acDataRows =$("#tblGradeSection").dataTable().fnGetNodes();
	var ncRows =acDataRows!=null?acDataRows.length:0;
	if(ncRows==totalGrades){

		fnDisplayAlertInfo();
		fnDisableGradeSectionsAfterAllMapped();
		
		$('#divGradeSectionBtnSave').css('display','none');
	}else{
		$('#divInfo').css('display','none');
		fnDisplayGradeSectionMasters();
		$('#divGradeSectionBtnSave').css('display','block');
	}
	fnColSpanIfDTEmpty('tblGradeSection',4);
}






/*
 * enable section master panel onchange of grade master option
 * and 
 * if any edit grade has been selected before
 * then remove that grade frome here and make all radios in table as unselected....
 * */
$('#gradeSectionMapper #gradeMasters input:radio[name=grades]').change(function() {
	
	aIsNewGrSec =true;
	aIsEditGrSec =false;
	$('#divGradeSectionBtnSave').css('display','block');
	$('#divGradeSectionBtnSave #btnGradeSectionDelete').prop('disabled',true);
	//remove the edited/existed(table grades) grade from grade master div
	fnDisableOrEnableGradeMaster(aSchGrdSecExistedGradeIds);
	
	//uncheck all sections in section master
	
	
	//uncheck all radios in table
	var editGRows = $("#gradeSectionMapper #tblGradeSection").dataTable().fnGetNodes();
	$.each(editGRows,function(i,row){		
		var eGRowId = '#'+$(row).find('input').prop('id');
		$(eGRowId).prop('checked',false);
	});
	
	$('#gradeSectionMapper #sectionMasters').css('display','block');
});

/*
 * Enable Save button for grade section mapper on change of section master options
 * 
 * */
$('.gradeSecCheckbox').change(function() {
	if(aIsEditGrSec){
		$('#divGradeSectionBtnSave').css('display','block');
		$('#divGradeSectionBtnSave #btnGradeSectionSave').prop('disabled',false);
		$('#divGradeSectionBtnSave #btnGradeSectionDelete').prop('disabled',true);
	}
	var aSecTempFoo =false;
	if(aIsNewGrSec){
		$('.gradeSecCheckbox').each(function(i,ele){
			
			if($(ele).prop('checked'))
				aSecTempFoo =true;
		});
		if(aSecTempFoo){
			$('#divGradeSectionBtnSave').css('display','block');
			$('#divGradeSectionBtnSave #btnGradeSectionSave').prop('disabled',false);
			$('#divGradeSectionBtnSave #btnGradeSectionDelete').prop('disabled',true);
		}
	}	
});

//check is there any more grades to be mapped...if not hide grade , section and button divs..
var fnCheckIsMoreGradeToMapper =function(){
	var acDataRows =$("#tblGradeSection").dataTable().fnGetNodes();
	var ncRows =acDataRows!=null?acDataRows.length:0;
	if(ncRows==totalGrades && !aIsEditGrSec){
		
		fnDisplayAlertInfo();
		$('#pnlGradeMasters').css('display','none');
		$('#pnlSectionMasters').css('display','none');

		$('#divGradeSectionBtnSave').css('display','none');
	}else{
		$('#divInfo').css('display','none');
		$('#pnlGradeMasters').css('display','block');
		$('#pnlSectionMasters').css('display','block');		
		$('#divGradeSectionBtnSave').css('display','block');
	}
}
var fnResetDays =function(){
	$.each(days,function(ind,obj){
		$.each($("#daysTable [name='"+obj+"']"),function(){
			var dayId=$(this).attr('id');
			$('#'+dayId).attr('disabled',false);
			$('#'+dayId).removeAttr('checked');
		})
	})
	$.each(days,function(ind,obj){
			$.each($("#daysTable [name='"+obj+"']"),function(){
				$(this).parent().siblings().find('.startTime').prop('disabled',true);
				$(this).parent().siblings().find('.endTime').prop('disabled',true);
			})
			
	})
	$.each($(".timepicker"), function(){  
		$(this).val('');
	})
	$('.select-all-days').removeAttr('checked');
	
}
//save edit grade section
var fnEditGradeSection =function(){
	var sEditGradeSections =[];
	var sEditGradeId='';
	var sectionDays=[];
	var day,startTime,endTime;
	var timings=false;
	var daysCheckBox=false;
	
	$('.gradeSecCheckbox').each(function(i,ele){
		if($(ele).prop('checked')){
			sEditGradeSections.push(parseInt($(ele).data('id')));
		}			
	});
	$('#gradeSectionMapper #gradeMasters input:radio[name=grades]').each(function(i,gEle){
		if($(gEle).prop('checked'))
			sEditGradeId =parseInt($(gEle).data('id'));
	});
	if($('#daysTable [type="checkbox"]').is(':checked')){
		daysCheckBox=true;
		var count=$("#daysTable input[type='checkbox']:checked").length;
		$.each($("#daysTable input[type='checkbox']:checked"), function(){
			if(($(this).parent().siblings().find('.startTime').val())!='' && ($(this).parent().siblings().find('.endTime').val())!='')
			{	count--;
				if(count==0)
					timings=true;
			}
		})	
	}
	$.each($("#daysTable input[type='checkbox']:checked"), function(){            
		day=$(this).val();
        startTime =$(this).parent().siblings().find('.startTime').val();
        endTime =$(this).parent().siblings().find('.endTime').val();
        var daysObject={
        		'day':day,
        		'startTime':startTime,
        		'endTime':endTime
        };
        sectionDays.push(daysObject);
    })
	if(sEditGradeSections.length<=0){
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("Section can not be empty for the selected grade.If you want to delete all sections, use delete functionality");
		$('#mdlError').modal().show();
		event.preventDefault();
		return false;
	} else if(!daysCheckBox){
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("Please, select section days");
		$('#mdlError').modal().show();
		event.preventDefault();
		return false;
	}else if(!timings){
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("Please,Enter start time and end time of selected days");
		$('#mdlError').modal().show();
		event.preventDefault();
		return false;
	}
	else{
		var gEditRequestData ={
				"schoolID":aSelectedSchool,
				"gradeId":sEditGradeId,
				"sectionIds":sEditGradeSections,
				"sectionDays":sectionDays
		};
		var gsecEditResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionmapper/", gEditRequestData, "PUT");
		if(gsecEditResponse!=null && gsecEditResponse!=""){
			if(gsecEditResponse.response=="shiksha-200"){
				
				$('.outer-loader').show();
				setTimeout(function(){$('.outer-loader').hide();},500);
				
				//get rowId by grade id from table for delete
				var delGrdRowId =0;
				$('#tblGradeSection').find('tr').each(function(i,gRow){
					if($(gRow).data('gradeid')==sEditGradeId)
						delGrdRowId =$(gRow).prop('id');
				});				
				if(gsecEditResponse.gradeVm!=null){
					deleteRow('tblGradeSection',delGrdRowId);
					fnAppendNewRow('tblGradeSection',createSchoolGradeSectionRow(gsecEditResponse));
					aIsEditGrSec =false;
				}
				setTimeout(function() {   //calls click event after a one sec
					 AJS.flag({
					    type: 'success',
					    title: 'Success!',
					    body: '<p>Information Updated</p>',
					    close :'auto'
					})
					}, 1000);
				
				setTimeout(fnCheckIsMoreGradeToMapper,3000);
			}else{
				$('#mdlEditError #msgText').text('');
				$('#mdlEditError #msgText').text(gsecEditResponse.responseMessage);
				$('#mdlEditError').modal().show();
				event.preventDefault();
				return false;
			}
		}
		//remove the edited/existed(table grades) grade from grade master div
		fnDisableOrEnableGradeMaster(aSchGrdSecExistedGradeIds);
		fnCheckOrUnchekcGradeMaster([]);
		//uncheck all sections in section master
		fnCheckOrUnchekcSecMaster([]);
		fnResetDays();
	}
}

//save new grade section
var fnSaveNewGradeSection =function(){
	
	var sGradeSections =[];
	var sGradeId='';
	var sectionDays=[];
	var day,startTime,endTime;
	$('.gradeSecCheckbox').each(function(i,ele){
		if($(ele).prop('checked')){
			sGradeSections.push(parseInt($(ele).data('id')));
		}			
	});
	$.each($("#daysTable input[type='checkbox']:checked"), function(){            
		day=$(this).val();
        startTime =$(this).parent().siblings().find('.startTime').val();
        endTime =$(this).parent().siblings().find('.endTime').val();
        var daysObject={
        		'day':day,
        		'startTime':startTime,
        		'endTime':endTime
        };
        sectionDays.push(daysObject);
    })
	
	$('#gradeSectionMapper #gradeMasters input:radio[name=grades]').each(function(i,gEle){
		if($(gEle).prop('checked'))
			sGradeId =parseInt($(gEle).data('id'));
	});
	
	var gRequestData ={
			"schoolID":aSelectedSchool,
			"gradeId":sGradeId,
			"sectionIds":sGradeSections,
			"sectionDays":sectionDays
	};
	var gsecAddResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionmapper/", gRequestData, "POST");
	if(gsecAddResponse!=null && gsecAddResponse!=""){
		if(gsecAddResponse[0].response=="shiksha-200"){
			
			$('.outer-loader').show();
			setTimeout(function(){$('.outer-loader').hide();},400);
			
			$(gsecAddResponse).each(function(index,dataObj){
				if(dataObj.gradeVm!=null){
					fnAppendNewRow('tblGradeSection',createSchoolGradeSectionRow(dataObj));
					aSchGrdSecExistedGradeIds.push(parseInt(dataObj.gradeVm.gradeId));
				}
			});
			
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>Information Updated</p>',
				    close :'auto'
				})
				}, 1000);
			
			
			setTimeout(fnCheckIsMoreGradeToMapper,3000);
		}
	}
	//remove the edited/existed(table grades) grade from grade master div
	fnDisableOrEnableGradeMaster(aSchGrdSecExistedGradeIds);
	fnCheckOrUnchekcGradeMaster([]);
	//uncheck all sections in section master
	fnCheckOrUnchekcSecMaster([]);	
	fnResetDays();
}



//save grade section mapper 
var fnInvokeSaveGradeSection =function(event){
	var adataRows =$("#tblGradeSection").dataTable().fnGetNodes();
	var nRows =adataRows!=null?adataRows.length:0;
	if(nRows==totalGrades && !aIsEditGrSec){
		
		
		fnDisplayAlertInfo();
		event.preventDefault();
		return false;
	}else{
		$('#divInfo').css('display','none');
	}
	
	var acSecCheckFoo =false; //check any section is checked or not
	var acGradeCheckFoo =false; //check any grade is selected or not
	var daysCheckBox=false;
	var timings=false;
	
	$('#gradeSectionMapper #gradeMasters input:radio[name=grades]').each(function(i,gEle){
		if($(gEle).prop('checked'))
			acGradeCheckFoo =true;
	});
	if($('#daysTable [type="checkbox"]').is(':checked')){
		daysCheckBox=true;
		var count=$("#daysTable input[type='checkbox']:checked").length;
		$.each($("#daysTable input[type='checkbox']:checked"), function(){
			if(($(this).parent().siblings().find('.startTime').val())!='' && ($(this).parent().siblings().find('.endTime').val())!='')
			{	count--;
				if(count==0)
					timings=true;
			}
		})
		
	}
	if(acGradeCheckFoo){
		if(aIsNewGrSec){
			$('.gradeSecCheckbox').each(function(i,ele){
				if($(ele).prop('checked'))
					acSecCheckFoo =true;
			});
			if(acSecCheckFoo){
				if(daysCheckBox){
					if(timings){
						fnSaveNewGradeSection();
					}else{
						$('#mdlError #msgText').text('');
						$('#mdlError #msgText').text("OOPs!!!Please Enter the end time for selected day");
						$('#mdlError').modal().show();
						event.preventDefault();
						return false;
					}
				}else{
					$('#mdlError #msgText').text('');
					$('#mdlError #msgText').text("OOPs!!! At least one day should be select.");
					$('#mdlError').modal().show();
					event.preventDefault();
					return false;
				}
			}else{
				$('#mdlError #msgText').text('');
				$('#mdlError #msgText').text("OOPs!!! At least one section should be select.");
				$('#mdlError').modal().show();
				event.preventDefault();
				return false;
			}
		}else if(aIsEditGrSec){
			fnEditGradeSection();
		}
	}else{
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("OOPs!!! Select grade first.");
		$('#mdlError').modal().show();
		event.preventDefault();
		return false;
	}
}






//onchange of grade section mapper table row radio for edit
var fnGradeSectionEdit =function(schoolGradeSectionMapperId,eSections){
	var ajaxCall=customAjaxCalling("schoolconfiguration/schoolgradesectionmapper/"+schoolGradeSectionMapperId, null,"POST");
	fnResetDays();
	$('#divInfo').css('display','none');
	fnDisplayGradeSectionMasters();
	aIsNewGrSec =false;
	aIsEditGrSec =true;
	var eGrdSecRowRadioId ='#grdSecRowRadio'+ajaxCall.gradeVm.gradeId;
	var eGid ='#gradeSectionMapper #gradeMasters #divG'+ajaxCall.gradeVm.gradeId;
	var tempGrdArr =[];
	var tempSecArr =[];
	var tempSecDaysArr ={};
	//copy the existed grades in to a temp array for disable purpose when editing this grade
	$.each(aSchGrdSecExistedGradeIds,function(i,val){
		if(val!=parseInt(ajaxCall.gradeVm.gradeId))
			tempGrdArr.push(val);
	})
	$.each(ajaxCall.sectionDays,function(index,object){
				$.each(days,function(ind,obj){
					if(object.day == obj){
						$('input[name="'+object.day+'"').prop('checked',true);
						/*$('input[name="'+object.day+'"').prop('disabled',true);*/
						$('#s'+object.day+'StartTime').prop('disabled',false);
						$('#s'+object.day+'StartTime').attr('batchDayId',object.id);
						$('#s'+object.day+'EndTime').prop('disabled',false);
						$('#s'+object.day+'StartTime').val(object.startTime);
						$('#s'+object.day+'EndTime').val(object.endTime);
						$('#btnDelete'+object.day).attr('value',object.id);
						$('#btnDelete'+object.day).show();
					}
				})
			})
	//copy the existed sections of this grade in to a temp array for check section purpose when editing this grade
	var eGradeSections =eSections.split(',');
	$.each(eGradeSections,function(i,val){		
		tempSecArr.push(parseInt(val));
	})
		
	
	if($(eGrdSecRowRadioId).prop('checked')){
		
		$(eGid).find('input').prop('checked',true);
		$(eGid).css('display','block');
		
		fnDisableOrEnableGradeMaster(tempGrdArr);
		
		fnCheckOrUnchekcSecMaster(tempSecArr);
		$('#gradeSectionMapper #gradeMasters').css('display','block');
		$('#gradeSectionMapper #sectionMasters').css('display','block');
		$('#divGradeSectionBtnSave #btnGradeSectionDelete').prop('disabled',false);
		$('#divGradeSectionBtnSave').css('display','block');
	}
	
	
	
	
	//after radio button changed to checkbox
	$(".fooGrade").click(function() {
	    if ($(this).is(":checked")) {
	        var group = "input:checkbox[name='" + $(this).attr("name") + "']";
	        $(group).prop("checked", false);
	        $(this).prop("checked", true);
	        
	    } else {
	        $(this).prop("checked", false);
	        fnCheckOrUnchekcSecMaster(tempSecArr);
	    }
	});
	//
	if(!$(eGrdSecRowRadioId).prop('checked')){
		$(eGid).css('display','none');
		fnCheckOrUnchekcSecMaster([]);
		fnResetDays();
		fnDisableGradeSectionsAfterAllMapped();
		fnDisplayAlertInfo();
	}
	
}



//invoke delete functionality for grade section
var fnInvokeDeleteGradeSection =function(){
	$("#deleteGradeSection #delEleCustom").css('display','block');
	hideDiv($('#deleteGradeSection #alertdiv'));
	$("#deleteGradeSection").find('#errorMessage').hide();
	$("#deleteGradeSection").find('#exception').text('');
	$("#deleteGradeSection").find('#buttonGroup').css('display','block');
	$("#deleteGradeSection").find('#okButton').css('display','none');
	$('#deleteGradeSection').modal().show();
}

//////////////////  SHK-1379  ////////////////
var fnDeleteGradeSection=function(eGradeId,eSections){
	$('#divInfo').css('display','none');
	fnDisplayGradeSectionMasters();
	aIsNewGrSec =false;
	aIsEditGrSec =true;
	
	var eGrdSecRowBtnId ='#grdSecRowRadio'+eGradeId;
	
	var eGid ='#gradeSectionMapper #gradeMasters #divG'+eGradeId;
	var tempGrdArr =[];
	var tempSecArr =[];
	$(eGrdSecRowBtnId).prop('checked',true);
	
	//copy the existed grades in to a temp array for disable purpose when editing this grade
	$.each(aSchGrdSecExistedGradeIds,function(i,val){
		if(val!=parseInt(eGradeId))
			tempGrdArr.push(val);
	})
	
	//copy the existed sections of this grade in to a temp array for check section purpose when editing this grade
	var eGradeSections =eSections.split(',');
	$.each(eGradeSections,function(i,val){		
		tempSecArr.push(parseInt(val));
	})
		
	
	if($(eGrdSecRowBtnId).prop('checked')){
		
		$(eGid).find('input').prop('checked',true);
		$(eGid).css('display','block');
		
		fnDisableOrEnableGradeMaster(tempGrdArr);
		
		fnCheckOrUnchekcSecMaster(tempSecArr);
		$('#gradeSectionMapper #gradeMasters').css('display','block');
		$('#gradeSectionMapper #sectionMasters').css('display','block');
		$('#divGradeSectionBtnSave #btnGradeSectionDelete').prop('disabled',false);
		$('#divGradeSectionBtnSave').css('display','block');
		
	}
	
	$(".fooGradeDelete").click(function() {
	    if ($(this).is(":checked")) {
	        var group = "input:checkbox[name='" + $(this).attr("name") + "']";
	        $(group).prop("checked", false);
	        $(this).prop("checked", true);
	        
	    } else {
	        $(this).prop("checked", false);
	      
	    }
	});
	//
	if(!$(eGrdSecRowBtnId).prop('checked')){
		$(eGid).css('display','none');
		fnCheckOrUnchekcSecMaster([]);
		fnResetDays();
		fnDisableGradeSectionsAfterAllMapped();
		fnDisplayAlertInfo();
	}
	
	
	fnInvokeDeleteGradeSection();
}



var fnDoDeleteGradeSection =function(){
	//get rowId by grade id from table for delete
	var delGradeId ='';
	$('#gradeSectionMapper #gradeMasters input:radio[name=grades]').each(function(i,gEle){
		if($(gEle).prop('checked'))
			delGradeId =parseInt($(gEle).data('id'));
	});
	var gDeleteRequestData ={
			"schoolID":aSelectedSchool,
			"gradeId":delGradeId,			
	};
	var gsecDelResponse =customAjaxCalling("schoolconfiguration/schoolgradesectionmapper/", gDeleteRequestData, "DELETE");
	if(gsecDelResponse!=null && gsecDelResponse!=""){
		if(gsecDelResponse.response=="shiksha-200"){
			$('.outer-loader').show();
			setTimeout(function(){$('.outer-loader').hide();},3000);
			
			//get rowId by grade id from table for delete
			var aDelGrdRowId =0;
			$('#tblGradeSection').find('tr').each(function(i,gRow){
				if($(gRow).data('gradeid')==delGradeId)
					aDelGrdRowId =$(gRow).prop('id');
			});				
			deleteRow('tblGradeSection',aDelGrdRowId);			
			aSchGrdSecExistedGradeIds = jQuery.grep(aSchGrdSecExistedGradeIds, function(value) {
				  return value != delGradeId;
			});
			
			//remove the edited/existed(table grades) grade from grade master div
			fnDisableOrEnableGradeMaster(aSchGrdSecExistedGradeIds);
			fnCheckOrUnchekcGradeMaster([]);
			//uncheck all sections in section master
			fnCheckOrUnchekcSecMaster([]);
			fnResetDays();
			$('#deleteGradeSection').modal('hide');
			setTimeout(function() {   //calls click event after a one sec
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>Information Updated</p>',
				    close :'auto'
				})
				}, 1000);
			
			$('#divInfo').css('display','none');
			$('#pnlGradeMasters').css('display','block');
			$('#pnlSectionMasters').css('display','block');
			$('#sectionMasters').css('display','block');
			$('#gradeMasters').css('display','block');
		}else{
			$("#deleteGradeSection #delEleCustom").css('display','none');
			showDiv($('#deleteGradeSection #alertdiv'));
			$("#deleteGradeSection").find('#errorMessage').show();
			$("#deleteGradeSection").find('#exception').text(gsecDelResponse.responseMessage);
			
		}
	}	
}
 var fndaysCheckboxChecked = function(element){
	if(element.is(':checked')){
		element.parent().siblings().find('.startTime').prop('disabled',false);
		element.parent().siblings().find('.endTime').prop('disabled',false);
		element.parent().siblings().find('.startTime').val('08:00 am');
		element.parent().siblings().find('.endTime').val('05:00 pm');
	}
	else{
		element.parent().siblings().find('.startTime').prop('disabled',true);
		element.parent().siblings().find('.endTime').prop('disabled',true);
		element.parent().siblings().find('.startTime').val('');
		element.parent().siblings().find('.endTime').val('');
	}
	
}


////////////

$(document).ready(function() {
	$("#daysTable [type='checkbox']").on('change',function(){
		fndaysCheckboxChecked($(this));
	});
	$('.timepicker').datetimepicker({
		format:"hh:mm a" 
	});
	$('.endTime').on('dp.change',function(){
		var id=$(this).attr('id').replace('End','Start');
		var start = new Date("May 9, 2017 " + $('#'+id).val());
		var end=new Date("May 9, 2017 " + $(this).val());
		if(end<=start)
			{$(this).val('');}
	})
	$('.startTime').on('dp.change',function(){
		var id=$(this).attr('id').replace('Start','End');
		 $('#'+id).val('');
	})
});
//