var isSelectAllClicked;
var selectedChkBox;

var dtPagination =function(tableId,nCols,exCols,hNames){	
	$(tableId).DataTable({
		"pagingType": "numbers",			
		"sDom": 't<"row view-filter"<"col-xs-12"<"pull-left"l><"pull-right"f><"clearfix">>>rt<"row view-pager"<"col-xs-12"<"text-center"ip>>>',
		"lengthMenu": [5,10,20,30,40,50,60,70,80,90,100 ],
		 "bLengthChange": true,
		 "bDestroy": true,
		 "bFilter": true,
		 "autoWidth": false,
		 "iDisplayLength": 100,
		 "stateSave": false,
		 'targets': 0,
         'searchable': false,      
         "fnDrawCallback": function ( oSettings ) {
        	  fnMarkSelectUnselectByFilterValue();
	      },
          "aoColumnDefs": [{
		        'bSortable': false,
		        'aTargets': ['nosort']
		    }],
		    "aaSorting": [],
		    bSortCellsTop:true,	
	});
	fnApplyColumnFilter(tableId,nCols,exCols,hNames);
	//collapse select filter
	$( ".multiselect-container" ).unbind( "mouseleave");
	$( ".multiselect-container" ).on( "mouseleave", function() {
		$(this).click();
	});
	
}

var fnAppendNewRow =function(tableName,newRow){
	var table =$("#"+tableName).DataTable();
	table.row.add($(newRow)).draw( false );	
}


var fnIsAllSelect =function(){	
	var tDataRows =$("#studentTable").DataTable().rows({ 'search': 'applied' }).nodes();
	var fooAllSelect =false ;
	$.each(tDataRows,function(i,row){		
		 if($(row).find('.chkboxStudent').prop('checked')){
			 fooAllSelect =true;
		}else{
			fooAllSelect =false;
			return false;
		}
	});
	
	return fooAllSelect;
}

//based on filter value and checked rows value, select-all checkbox will mark
var fnMarkSelectUnselectByFilterValue =function(){
	var filterVal =$('#studentTable_filter').find('input').val();
	
	var isAllSelect ;
	if(filterVal.length<=0){
		isAllSelect =fnIsAllSelect();

	}else if(isSelectAllClicked){
		isAllSelect =fnIsAllSelect();
	}else{
		isAllSelect =fnIsAllSelect();
	}
	
	if(!isAllSelect){
		$('#chkBoxSelectAll').prop('checked',false);
	}else{
		$('#chkBoxSelectAll').prop('checked',true);
	}
}


//select all header functionality for campaign school table
var fnDoSelectAll =function(status){	
	var rows = $("#studentTable").DataTable().rows({ 'search': 'applied' }).nodes();
	
	if(status){
		selectedChkBox =[];
		$.each(rows,function(i,row){		
			
			selectedChkBox.push($(row).data('id'));
		});
		
	}else if(selectedChkBox.length==0){
		$.each(rows,function(){			
			$('.chkboxStudent').prop('checked',false);			
		});	
	}
}


var fnEnableOrDisableButton =function(){
	var nDatarows = $("#studentTable").DataTable().rows({ 'search': 'applied' }).nodes();	
	var isOneSelected =false;
	$.each(nDatarows,function(i,row){		
		var id="#chkboxStudent"+$(row).prop('id').toString();
		if($(id).prop('checked')){
			isOneSelected =true;
			return false;
		}
	});
	
	
	
	if(isOneSelected){
		fnDisableViewButton();
	}else{
		fnEnableViewButton();
	}
}

//Handle click on "Select all" control
$('#chkBoxSelectAll').on('click', function(){
	isSelectAllClicked =true; 
	var isChecked =$('#chkBoxSelectAll').is(':checked');
	
	
	if(!isChecked){
		selectedChkBox =[];
	}
	var dRows = $("#studentTable").DataTable().rows({ 'search': 'applied' }).nodes();
	 $('input[type="checkbox"]', dRows).prop('checked', this.checked);
	 
	 fnDoSelectAll(isChecked);
	 fnEnableOrDisableButton();
});




var fnChkUnchk =function(id){
	 var idx = $.inArray(id, selectedChkBox);	
	 if (idx == -1) {
		 selectedChkBox.push(id);
		 selectedChkBox.length==noOfStudents? $('#chkBoxSelectAll').prop('checked',true): $('#chkBoxSelectAll').prop('checked',false);
	 } else {
		 selectedChkBox.splice(idx, 1);
		 $('#chkBoxSelectAll').prop('checked',false);
	 }
	 fnEnableOrDisableButton();
}



