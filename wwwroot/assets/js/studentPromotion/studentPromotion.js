

var lObj;
var elementIdMap;
var aGrades;
var $select;
var aSectionBySchoolGrade;
var aPreviousAcademicYear;
var bSelectedGrade;
var aPrevoiusAcademicYearId;
var thLabels;
var resetButtonObj;
var selectedIds;
var aSelectedSchool;
var aSelectedGrade;
var aSelectedAcademicYear;
var aSelectedSection;

var isSelectAllClicked;
var noOfStudents;
var selectedChkBox ;
var isSavedNew ;
var isPromotionEnable;
var fnInitVars =function(){
	aSelectedSchool ='';
	aSelectedGrade ='';
	aSelectedAcademicYear ='';
	aSelectedSection ='';
	
	isSelectAllClicked=false;
	noOfStudents =0;
	selectedChkBox =[];
	isSavedNew =false;
	isPromotionEnable=false;
}


//global selectDivIds for smart filter
var studentPromotionPageContext = {};	
	studentPromotionPageContext.city 	="#divSelectCity";
	studentPromotionPageContext.block 	="#divSelectBlock";
	studentPromotionPageContext.NP 	="#divSelectNP";
	studentPromotionPageContext.GP 	="#divSelectGP";
	studentPromotionPageContext.RV 	="#divSelectRV";
	studentPromotionPageContext.village ="#divSelectVillage";
	studentPromotionPageContext.school ="#divSelectSchool";
	studentPromotionPageContext.grade ="#divSelectGrade";
	studentPromotionPageContext.academicYear ="#divSelectAcademicYear";
	studentPromotionPageContext.section ="#divSelectSection";
	
	studentPromotionPageContext.elementSchool ="#selectBox_schoolsByLoc";
	studentPromotionPageContext.eleGrade ="#selectBox_gradesBySchool";
	studentPromotionPageContext.eleSection ="#selectBox_secBySchoolGrade";
	
	studentPromotionPageContext.eleAcademicYear ="#selectBox_academicYear";
	
	studentPromotionPageContext.resetLocDivVillage ="#divResetLocVillage";
	
	studentPromotionPageContext.btnViewStudents ="#btnViewStudents";
	studentPromotionPageContext.btnPromoteStudents ="#btnPromoteStudent";
		
	
var studentPromotionModal = {};
	studentPromotionModal.eleFromGrade ="#selectBox_fromGradesBySchool";
	studentPromotionModal.eleToGrade ="#selectBox_toGradesBySchool";
	studentPromotionModal.eleFromAY ="#selectBox_fromAcademicYear";
	studentPromotionModal.eleToAY ="#selectBox_toAcademicYear";
	studentPromotionModal.eleToSection ="#selectBox_toSection";
	
	

	var fnHideGradeAndSectionAndAcademicYearDiv =function(){
		$(studentPromotionPageContext.grade).hide();
		$(studentPromotionPageContext.academicYear).hide();
		$(studentPromotionPageContext.section).hide();
		$(studentPromotionPageContext.btnViewStudents).prop('hidden',true);
		$('#tableDiv').css('display','none');
	}
	
	var fnDisableAllButton =function(){
		$('#btnViewStudents').css('display','none');
		$('#btnPromoteStudents').css('display','none');
	}

	//remove and reinitialize smart filter
	var reinitializeSmartFilter =function(eleMap){
		var ele_keys = Object.keys(eleMap);
			$(ele_keys).each(function(ind, ele_key){
			$(eleMap[ele_key]).find("option:selected").prop('selected', false);
			$(eleMap[ele_key]).find("option[value]").remove();
			$(eleMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(eleMap[ele_key]);
		});	
	}
	
	//displaying smart-filters
	var displaySmartFilters =function(lCode,lLevelName){
		$(".outer-loader").show();
		elementIdMap = {
			"State" : '#statesForZone',
			"Zone" : '#selectBox_zonesByState',
			"District" : '#selectBox_districtsByZone',
			"Tehsil" : '#selectBox_tehsilsByDistrict',
			"Block" : '#selectBox_blocksByTehsil',
			"City" : '#selectBox_cityByBlock',
			"Nyaya Panchayat" : '#selectBox_npByBlock',
			"Gram Panchayat" : '#selectBox_gpByNp',
			"Revenue Village" : '#selectBox_revenueVillagebyGp',
			"Village" : '#selectBox_villagebyRv',
			"School" : '#selectBox_schoolsByLoc'
		};
		displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));
		
		var ele_keys = Object.keys(elementIdMap);
		$(ele_keys).each(function(ind, ele_key){
			$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
			$(elementIdMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(elementIdMap[ele_key]);
		});	
		setTimeout(function(){
			$(".outer-loader").hide();
			$('#rowDiv1,#divFilter').show();
			}, 100);	
	};
	
	//arrange filter div based on location type
	//if rural, hiding city and moving elements to upper 
	var positionFilterDiv =function(type){
		if(type=="R"){
			$(studentPromotionPageContext.NP).insertAfter( $(studentPromotionPageContext.block));
			$(studentPromotionPageContext.RV).insertAfter( $(studentPromotionPageContext.GP));
			$(studentPromotionPageContext.school).insertAfter( $(studentPromotionPageContext.village));
		}else{
			$(studentPromotionPageContext.school).insertAfter( $(studentPromotionPageContext.city));
		}
	}
	
//school location type on change...get locations...
$('#divSchoolLocType input:radio[name=locType]').change(function() {
		fnHideGradeAndSectionAndAcademicYearDiv();
		fnDisableAllButton();
	
		$('#locColspFilter').removeClass('in');
		$('#locationPnlLink').text('Select Location');		
		$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');

		
		lObj =this;
		var typeCode =$(this).data('code');		
		$('.outer-loader').show();		
		reinitializeSmartFilter(elementIdMap);	
		$("#hrDiv").show();
		if(typeCode.toLowerCase() =='U'.toLowerCase()){
			showDiv($(studentPromotionPageContext.city),$(studentPromotionPageContext.resetLocDivCity));
			hideDiv($(studentPromotionPageContext.block),$(studentPromotionPageContext.NP),$(studentPromotionPageContext.GP),$(studentPromotionPageContext.RV),$(studentPromotionPageContext.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("U");
		}else{
			hideDiv($(studentPromotionPageContext.city),$(studentPromotionPageContext.resetLocDivCity));
			showDiv($(studentPromotionPageContext.block),$(studentPromotionPageContext.NP),$(studentPromotionPageContext.GP),$(studentPromotionPageContext.RV),$(studentPromotionPageContext.village));
			displaySmartFilters($(this).data('code'),$(this).data('levelname'));
			positionFilterDiv("R");
		}
		
		fnCollapseMultiselect();
		
		$('.outer-loader').hide();
		return;
});


//empty all select box
var fnEmptyAll =function(){
	$.each(arguments,function(i,obj){
		$(obj).empty();
	});
}

//hide section and academic div
var fnHideSectionAndAcademicYearDiv =function(){
	$(studentPromotionPageContext.section).hide();
	$(studentPromotionPageContext.academicYear).hide();
	$(studentPromotionPageContext.btnViewStudents).prop('hidden',true);
	$('#tableDiv').css('display','none');
	
}


var fnBindSections =function(sEleId,secData){
	
	$(studentPromotionPageContext.eleSection).multiselect('destroy');
	
	if(secData!=null && secData!=""){
		$select	=sEleId;
		$select.html('');
		$.each(secData,function(index,obj){
			$select.append('<option  value="' + obj.sectionId + '"  data-id="' + obj.sectionId + '"data-section="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
		});
	}
	fnSetMultislect(studentPromotionPageContext.eleSection);
	
}


var fnBindAcademicYearToListBox =function(bEleAcademic,pCurrentAcademicYear){
	$select	=bEleAcademic;
	$select.html('');
	
	$select.append('<option  selected value="' + pCurrentAcademicYear.academicYearId + '"  data-id="' + pCurrentAcademicYear.academicYearId + '"data-academicyear="' +pCurrentAcademicYear.academicYear+ '">' +pCurrentAcademicYear.academicYear+ '</option>');
	
	fnSetMultislect(studentPromotionPageContext.eleAcademicYear);
}


//bind sections to list box
var fnBindSectionsToListBox	=function(){

	$('.outer-loader').show();

	fnHideSectionAndAcademicYearDiv();
	
	fnEmptyAll(studentPromotionPageContext.eleSection);
	fnEmptyAll(studentPromotionPageContext.eleAcademicYear);
	
	aSelectedGrade = $(studentPromotionPageContext.eleGrade).find('option:selected').val();
	
	try {
		var bindSecEleId = $(studentPromotionPageContext.eleSection);
		aSectionBySchoolGrade = customAjaxCalling("section/"+aSelectedSchool+'/'+aSelectedGrade,null,"GET");
		
		aPreviousAcademicYear =customAjaxCalling("academicYear/previousYear/",null,"GET");
	
		if(aSectionBySchoolGrade.length>0){
			
			fnBindSections(bindSecEleId,aSectionBySchoolGrade);
			
			fnBindAcademicYearToListBox( $(studentPromotionPageContext.eleAcademicYear),aPreviousAcademicYear);
			
			$(studentPromotionPageContext.section).show();
			
		}else{
			fnHideSectionAndAcademicYearDiv();
			$('#mdlError #msgText').text('');
			$('#mdlError #msgText').text("There are no sections for the selected school and grade.");
			$('#mdlError').modal().show();
		}
	} catch (e) {
		
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no sections for the selected school and grade.");
		$('#mdlError').modal().show();
	}
	
	fnCollapseMultiselect();
	
	$('.outer-loader').hide();

}

var fnEnableViewButton =function(){
	$('#btnViewStudents').css('display','block');
	$('#btnPromoteStudents').css('display','none');
	
}

var fnSetMultislect =function(pEle){
	$(pEle).multiselect({
		maxHeight: 150,
		enableFiltering: true,
		enableCaseInsensitiveFiltering: true,
		includeFilterClearBtn: true,
		filterPlaceholder: 'Search here...',	
		onChange : function() {
			$('#tableDiv').css('display','none');
			
			if(pEle=='#selectBox_gradesBySchool'){
				if($('#selectBox_gradesBySchool').find('option:selected').length>0){
					fnBindSectionsToListBox();
					fnDisableAllButton();
				}
			}else if(pEle=='#selectBox_secBySchoolGrade'){
				
				if($('#selectBox_secBySchoolGrade').find('option:selected').length>0){
					$(studentPromotionPageContext.academicYear).show();
					fnEnableViewButton();
				}else{
					$(studentPromotionPageContext.academicYear).hide();
					fnDisableAllButton();
				}			
			}
		},
		
	});	
}

var fnBindGradesToListBox =function(bEleId,gData){
	if(gData!=null && gData!=""){
		$select	=bEleId;
		$select.html('');
		$.each(gData,function(index,obj){
			$select.append('<option  value="' + obj.gradeId + '"  data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '"data-sequence="' + obj.sequenceNumber+ '">' +obj.gradeName+ '</option>');
		});
	}
	fnSetMultislect(studentPromotionPageContext.eleGrade);
	
}

//get grades by school and bind to grade box
var fnBindGradesBySchool =function(){
	$('.outer-loader').show();
	
	fnHideGradeAndSectionAndAcademicYearDiv();
	fnEmptyAll(studentPromotionPageContext.eleGrade);
	fnEmptyAll(studentPromotionPageContext.eleAcademicYear);
	
	aSelectedSchool = $(studentPromotionPageContext.elementSchool).find('option:selected').val();
	try {
		var bindToEleId = $(studentPromotionPageContext.eleGrade);
		aGrades = customAjaxCalling("gradelist/"+aSelectedSchool,null,"GET");
		
		if(aGrades.length>0){
			fnBindGradesToListBox(bindToEleId,aGrades);
			
			$(studentPromotionPageContext.grade).show();
		}else{
			fnHideGradeAndSectionAndAcademicYearDiv();
			$('#mdlError #msgText').text('');
			$('#mdlError #msgText').text("There are no grades for the selected school.");
			$('#mdlError').modal().show();
		}
	} catch (e) {
		$('#mdlError #msgText').text('');
		$('#mdlError #msgText').text("There are no grades for the selected school.");
		$('#mdlError').modal().show();
	}
	
	fnCollapseMultiselect();		
	$('.outer-loader').hide();
}


var fnCreateAndAppendNewRow =function(pData){
	deleteAllRow('studentTable');
	$.each(pData,	function(index, obj) {
		var row = "<tr id='"+ obj.studentId+ "' data-id='"	+obj.studentId+ "'>"+
					"<td data-id='"+ obj.studentId+"'><center><input class='chkboxStudent' onchange='fnChkUnchk("+obj.studentId+")'type='checkbox' onchange='' id='chkboxStudent"+obj.studentId+"'></input></center></td>"+
					"<td title='"+ obj.rollNumber+ "'>"+ obj.rollNumber+ "</td>"+
					"<td title='"+ obj.name+ "'>"+ obj.name+ "</td>"+
					"</tr>";
		fnAppendNewRow("studentTable", row);
	});
}


//get students by filters
var fnGetStudents =function(){

	$('#tableDiv').css('display','none');
	
	$('.outer-loader').show();
	setTimeout(function(){$('.outer-loader').hide();},1000);
	
	aSelectedSchool = $(studentPromotionPageContext.elementSchool).find('option:selected').val();
	aSelectedGrade =$(studentPromotionPageContext.eleGrade).find('option:selected').val();
	aSelectedAcademicYear =$(studentPromotionPageContext.eleAcademicYear).find('option:selected').val();
	aSelectedSection =$(studentPromotionPageContext.eleSection).find('option:selected').val();
	
	isSavedNew	=false;
	
	var aStudentData = customAjaxCalling('student/'+aSelectedSchool+'/'+aSelectedGrade+'/'+aSelectedSection+'/'+aSelectedAcademicYear,null,'GET');
	
	if(aStudentData!=null){
		noOfStudents =aStudentData.length;
		
		fnCreateAndAppendNewRow(aStudentData);
		$('#tableDiv').css('display','block');
		dtPagination('#studentTable',3,[0],thLabels);
		fnMultipleSelAndToogleDTCol('.search_init',function(){
			fnHideColumns('#studentTable',[]);
		});
		fnColSpanIfDTEmpty('studentTable',3);
		
		$( ".multiselect-container" ).unbind( "mouseleave");
		$( ".multiselect-container" ).on( "mouseleave", function() {
			$(this).click();
		});
		
		if(aStudentData.length==0){
			$('#mdlError #msgText').text('');
			$('#mdlError #msgText').text("There are no more students for grade promotion of the selected school, grade and section.");
			$('#mdlError').modal().show();
		}	
	}
}

//@Debendra
//customize all multiple select box
var fnSetOptionsForAllMultipleSelect = function(){
	$(arguments).each(function(index,arg){
		$(arg).multiselect({
			maxHeight: 100,
			includeSelectAllOption: true,
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			includeFilterClearBtn: true,
			filterPlaceholder: 'Search here...',
			
			
			onChange : function() {
				if(arg=='#selectBox_toGradesBySchool'){
					if($('#selectBox_toGradesBySchool').find('option:selected').length>0){
						fnBindSectionToPromoteListBox();				
					}
				}
			},
		});
	});
	fnCollapseMultiselect();
}

//bind academic year toSection list box
var fnBindSectionToPromoteListBox =function(){
	
	bSelectedGrade =$(studentPromotionModal.eleToGrade).find('option:selected').val();
	var  toSectionData = customAjaxCalling("section/"+aSelectedSchool+'/'+bSelectedGrade,null,"GET");
	if(toSectionData!=null){
		if(toSectionData.length>0){
			$('#toSection').prop('hidden',false);
			$(studentPromotionModal.eleToSection).multiselect('destroy');

			if(toSectionData!=""){
				$select	=$(studentPromotionModal.eleToSection);
				$select.html('');
				$.each(toSectionData,function(index,obj){
					$select.append('<option  value="' + obj.sectionId + '"  data-id="' + obj.sectionId + '"data-section="' + obj.sectionName+ '">' +obj.sectionName+ '</option>');
				});
			}
			fnSetOptionsForAllMultipleSelect(studentPromotionModal.eleToSection);
		}else{
			$('#toSection').prop('hidden',true);
			
		}
	}

	

}



//bind grades toGrade list box
var fnBindToGradesToPromoteListBox =function(){
	var sequenceNumber =$(studentPromotionModal.eleFromGrade).find('option:selected').data('sequence');
	var toGradeData =customAjaxCalling("grade/"+aSelectedSchool+"/"+sequenceNumber,null,"GET");
	
	if(toGradeData!=null && toGradeData!=""){
		if(toGradeData.length>0){
			fnEmptyAll(studentPromotionModal.eleToGrade);
			$(studentPromotionModal.eleToGrade).multiselect('destroy');
			
			$select	=$(studentPromotionModal.eleToGrade);
			$select.html('');
			$.each(toGradeData,function(index,obj){
				if(obj.gradeId != parseInt(aSelectedGrade))
					$select.append('<option  value="' + obj.gradeId + '"  data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '">' +obj.gradeName+ '</option>');
			});
			$('#mdlPromoteStudent').modal().show();
		}else{
			$('#mdlPromoteStudent').modal('hide');
			$('#mdlError #msgText').text('');
			$('#mdlError #msgText').text("There are no grades for promotion of students of the selected school .Check school configuration for more details.");
			$('#mdlError').modal().show();	
		}
	}
	fnSetOptionsForAllMultipleSelect(studentPromotionModal.eleToGrade);
}

//bind grades to  fromGrade list box
var fnBindFromGradesToPromoteListBox =function(pgData){
	
	fnEmptyAll(studentPromotionModal.eleFromGrade);
	$(studentPromotionModal.eleFromGrade).multiselect('destroy');
	if(pgData!=null && pgData!=""){
		$select	=$(studentPromotionModal.eleFromGrade);
		$select.html('');
		$.each(pgData,function(index,obj){
			if(obj.gradeId == parseInt(aSelectedGrade))
				$select.append('<option  selected value="' + obj.gradeId + '"  data-id="' + obj.gradeId + '"data-gradename="' + obj.gradeName+ '"data-sequence="' + obj.sequenceNumber+ '">' +obj.gradeName+ '</option>');
		});
	}
	fnSetOptionsForAllMultipleSelect(studentPromotionModal.eleFromGrade);
}

//bind academic year to  fromlist box
var fnBindFromAYToPromoteListBox =function(pPrevAY){
	
	aPrevoiusAcademicYearId =pPrevAY.academicYearId;
	
	fnEmptyAll(studentPromotionModal.eleFromAY);
	$(studentPromotionModal.eleFromAY).multiselect('destroy');
	$select	=$(studentPromotionModal.eleFromAY);
	$select.html('');
	$select.append('<option  selected value="' + pPrevAY.academicYearId + '"  data-id="' + pPrevAY.academicYearId + '"data-academicyear="' +pPrevAY.academicYear+ '">' +pPrevAY.academicYear+ '</option>');	
	fnSetOptionsForAllMultipleSelect(studentPromotionModal.eleFromAY);
}

//bind academic year toAY list box
var fnBindToAYToPromoteListBox =function(){
	
	
	var toAYData =customAjaxCalling("academicYear/currentAcademicYear", null,"GET");	
	fnEmptyAll(studentPromotionModal.eleToAY);
	$(studentPromotionModal.eleToAY).multiselect('destroy');
	
	if(toAYData!=null && toAYData!=""){
		$select	=$(studentPromotionModal.eleToAY);
		$select.html('');
					
		$select.append('<option  selected value="' + toAYData.academicYearId + '"  data-id="' + toAYData.academicYearId + '"data-academicyear="' +toAYData.academicYear+ '">' +toAYData.academicYear+ '</option>');
	}
	fnSetOptionsForAllMultipleSelect(studentPromotionModal.eleToAY);
}

//init promote dialog box
var fnInitPromote =function(){
	fnBindFromGradesToPromoteListBox(aGrades);
	fnBindToGradesToPromoteListBox();
	fnBindFromAYToPromoteListBox(aPreviousAcademicYear);
	fnBindToAYToPromoteListBox();
	$('#toSection').prop('hidden',true);
	resetFormById('formPromoteStudent');
	$("#formPromoteStudent").data('formValidation').updateStatus('fromGrade', 'VALID');
	$("#formPromoteStudent").data('formValidation').updateStatus('toAY', 'VALID');
	$("#formPromoteStudent").data('formValidation').updateStatus('fromAY', 'VALID');
}

















var fnDisableViewButton =function(){
	$('#btnViewStudents').css('display','none');
	$('#btnPromoteStudents').css('display','block');
}

//uncheck all data-row  in table
//will remove when data will populate on basis-of grade sequence number
var fnUncheckAllTableRow =function(){
	var nDataRows = $("#studentTable").DataTable().rows({ 'search': 'applied' }).nodes();
	$('input[type="checkbox"]', nDataRows).prop('checked', false);
	$.each(nDataRows,function(i,row){
		$('.chkboxStudent').prop('checked',false);
		var rChkBoxId="#chkboxStudent"+$(row).prop('id').toString();
		if($(rChkBoxId).prop('checked')){
			deleteRow("studentTable",$(row).data('id'));
		}
	});
	$('#chkBoxSelectAll').prop('checked',false);
	 
	fnEnableViewButton();
	deleteAllRow("studentTable");
	fnGetStudents();
	selectedChkBox =[];
	isSavedNew =true;
}


//save promoted data
var fnPromoteStudents =function(){

	var schoolId = $(studentPromotionPageContext.elementSchool).val();
	var fromGradeId =$(studentPromotionModal.eleFromGrade).find('option').val();
	var toGradeId =$(studentPromotionModal.eleToGrade).val();
	var fromSectionId =$(studentPromotionPageContext.eleSection).find('option:selected').val();
	var toSectionId =$(studentPromotionModal.eleToSection).val();
	var fromAY =$(studentPromotionModal.eleFromAY).find('option').val();
	var toAY =$(studentPromotionModal.eleToAY).find('option').val();
	
	var studentIds;
	studentIds =selectedChkBox;
	
	var aRequestData ={
		"studentIds":studentIds,
		"schoolId" :schoolId,
		"fromGradeId" :fromGradeId,
		"toGradeId" :toGradeId,
		"fromSectionId" :fromSectionId,
		"toSectionId" :toSectionId,
		"fromAcademicYearId" :fromAY,
		"toAcademicYearId" :toAY
		
	};
	
	var gResponse =customAjaxCalling("student/grade/promote", aRequestData, "POST");
	if(gResponse!=null){
		if(gResponse.response=="shiksha-200"){
			$('#mdlPromoteStudent').modal('hide');
			fnUncheckAllTableRow();
			setTimeout(function() {   
				 AJS.flag({
				    type: 'success',
				    title: 'Success!',
				    body: '<p>Information saved.</p>',
				    close :'auto'
				})
				}, 1000);
			
		}else{
			showDiv($('#mdlPromoteStudent #alertdiv'));
			$("#mdlPromoteStudent").find('#successMessage').hide();			
			$("#mdlPromoteStudent").find('#errorMessage').show();
			$("#mdlPromoteStudent").find('#exception').show();
			$("#mdlPromoteStudent").find('#exception').text(gResponse.responseMessage);
		}
	}	
}






///////////////////////////
$(function() {

	fnInitVars();

	fnSetMultislect(studentPromotionPageContext.eleGrade);
	fnSetMultislect(studentPromotionPageContext.eleAcademicYear);
	fnSetMultislect(studentPromotionPageContext.eleSection);	
	thLabels =['','Roll No  ','Student Name  '];
	dtPagination('#studentTable',3,[0],thLabels);
	fnMultipleSelAndToogleDTCol('.search_init',function(){
		fnHideColumns('#studentTable',[]);
	});
	fnColSpanIfDTEmpty('studentTable',3);

	fnHideGradeAndSectionAndAcademicYearDiv();
	fnDisableAllButton();



	fnSetOptionsForAllMultipleSelect(studentPromotionModal.eleFromGrade,studentPromotionModal.eleToGrade,
			studentPromotionModal.eleFromAY,studentPromotionModal.eleToAY,studentPromotionModal.eleToSection);



	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-minus-sign red gi-1p5x"></span></a>');
	});


	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
		$('#locationPnlLink')
		.append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	});

	$('#pnlMappingElements').on('show.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('Hide school configuration');
	});  
	$('#pnlMappingElements').on('hide.bs.collapse', function(){
		$('#tabPnlConfigHeading').text('View school configuration');
	});



	$("#formPromoteStudent").formValidation({
		excluded: ':disabled',
		feedbackIcons : {
			validating : 'glyphicon glyphicon-refresh'
		},
		fields:{
			fromGrade: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(studentPromotionModal.eleFromGrade).find('option:selected').val();
							return (option != null&& option!="NONE");
						}
					}
				}
			},
			toGrade : {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(studentPromotionModal.eleToGrade).find('option:selected').val();
							return (option != null&& option!="NONE");
						}
					}
				}
			},
			fromAY: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(studentPromotionModal.eleFromAY).find('option:selected').val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
			toAY: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(	studentPromotionModal.eleToAY).find('option:selected').val();							
							return (option != null && option!="NONE");
						}
					}
				}
			},
			toSection: {
				validators : {
					callback : {
						message : '      ',
						callback : function() {
							// Get the selected options
							var option = $(	studentPromotionModal.eleToSection).find('option:selected').val();
							return (option != null && option!="NONE");
						}
					}
				}
			},
		}
	}).on('success.form.fv', function(e) {
		e.preventDefault();
		fnPromoteStudents();
	});
});








//////////////////////
//Smart filter		//
//////////////////////




//reset only location when click on reset button icon
var resetLocAndSchool =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	selectedIds=[];//it is for smart filter utility..SHK-369
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('#locationPnlLink').append('<a href="#"  class="btn-collapse pull-right" data-type="minus" data-toggle="collapse"> <span class="glyphicon glyphicon-plus-sign green gi-1p5x"></span></a>');
	
	reinitializeSmartFilter(elementIdMap);
	var aLocTypeCode =$('#divSchoolLocType input:radio[name=locType]:checked').data('code');
	var aLLevelName =$('#divSchoolLocType input:radio[name=locType]:checked').data('levelname');
	displaySmartFilters(aLocTypeCode,aLLevelName);
	
	fnHideGradeAndSectionAndAcademicYearDiv();
	fnDisableAllButton();
	
	fnCollapseMultiselect();
	
}



//////////////////////
//Smart filter ended//
//////////////////////








