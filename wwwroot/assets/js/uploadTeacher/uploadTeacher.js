

//global selectDivIds for smart filter
var pageContext = {};	
	pageContext.city 	="#divSelectCity";
	pageContext.block 	="#divSelectBlock";
	pageContext.NP 	="#divSelectNP";
	pageContext.GP 	="#divSelectGP";
	pageContext.RV 	="#divSelectRV";
	pageContext.village ="#divSelectVillage";
	pageContext.school ="#divSelectSchool";
	
	pageContext.elementSchool ="#selectBox_schoolsByLoc";
	pageContext.grade ="#selectBox_grades";
	
	pageContext.resetLocDivCity ="#divResetLocCity";
	pageContext.resetLocDivVillage ="#divResetLocVillage";

var elementIdMap;
var resetButtonObj;

	//remove and reinitialize smart filter
	var reinitializeSmartFilter =function(eleMap){
		var ele_keys = Object.keys(eleMap);
		$(ele_keys).each(function(ind, ele_key){
			$(eleMap[ele_key]).find("option:selected").prop('selected', false);
			$(eleMap[ele_key]).find("option[value]").remove();
			$(eleMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(eleMap[ele_key]);
		});	
	}
	
	//displaying smart-filters
	var displaySmartFilters =function(lCode,lLevelName){
		$(".outer-loader").show();

		elementIdMap = {
					"State" : '#statesForZone',
					"Zone" : '#selectBox_zonesByState',
					"District" : '#selectBox_districtsByZone',
					"Tehsil" : '#selectBox_tehsilsByDistrict',
					"Block" : '#selectBox_blocksByTehsil',
					"City" : '#selectBox_cityByBlock',
					"Nyaya Panchayat" : '#selectBox_npByBlock',
					"Gram Panchayat" : '#selectBox_gpByNp',
					"Revenue Village" : '#selectBox_revenueVillagebyGp',
					"Village" : '#selectBox_villagebyRv',
					"School" : '#selectBox_schoolsByLoc'
		};
		displayLocationsOfSurvey(lCode,lLevelName,$('#divFilter'));

		var ele_keys = Object.keys(elementIdMap);
		$(ele_keys).each(function(ind, ele_key){
			$(elementIdMap[ele_key]).find("option:selected").prop('selected', false);
			$(elementIdMap[ele_key]).multiselect('destroy');		
			enableMultiSelect(elementIdMap[ele_key]);
		});	
		setTimeout(function(){
			$(".outer-loader").hide();
			$('#rowDiv1,#divFilter').show();
			}, 100);	
	};

	
	//arrange filter div based on location type
	//if rural, hiding city and moving elements to upper 
	var positionFilterDiv =function(type){
		if(type=="R"){		
			$(pageContext.NP).insertAfter( $(pageContext.block));
			$(pageContext.RV).insertAfter( $(pageContext.GP));
			$(pageContext.school).insertAfter( $(pageContext.village));
		}else{
			$(pageContext.school).insertAfter( $(pageContext.city));
		}
	}
	
//after UI chanaged......
$('input:radio[name=locType]').change(function() {
	$("#uploadFile").hide();
	$('#failureMessageDiv').hide();//hide error messsage
	$("#successMessageDiv").hide();///hide success messsage
	$('#alertIdForClientValidate').hide();
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	
	$('#divResetLocVillage').show();
	
	var typeCode =$(this).data('code');		
	$('.outer-loader').show();		
	reinitializeSmartFilter(elementIdMap);	
	$("#hrDiv").show();
	if(typeCode.toLowerCase() =='U'.toLowerCase()){			
		showDiv($(pageContext.city));
		hideDiv($(pageContext.block),$(pageContext.NP),$(pageContext.GP),$(pageContext.RV),$(pageContext.village));
		displaySmartFilters($(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
		positionFilterDiv("U");
	}else{
		hideDiv($(pageContext.city),$(pageContext.resetLocDivCity));
		showDiv($(pageContext.block),$(pageContext.NP),$(pageContext.GP),$(pageContext.RV),$(pageContext.village));
		displaySmartFilters($(this).data('code'),$(this).data('levelname'));
		fnCollapseMultiselect();
		positionFilterDiv("R");
	}
	$('.outer-loader').hide();
	return;

});







//reset only location when click on reset button icon
var resetLocAndSchool =function(obj){	
	$("#mdlResetLoc").modal().show();
	resetButtonObj =obj;
	
}
//invoking on click of resetLocation confirmation dialog box
var doResetLocation =function(){
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red");
	$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	$('#locColspFilter').removeClass('in');
	$('#locationPnlLink').text('Select Location');
	//it is for smart filter utility..SHK-369
	resetForm("#excelUploadForm");
	$('#exampleInputFile').val('');
	if($(resetButtonObj).parents("form").attr("id")=="excelUploadForm"){
		$('#uploadFile').hide();
		$("#divFilter").prop("hidden", true);
		reinitializeSmartFilter(elementIdMap);
		var aLocTypeCode =$('input:radio[name=locType]:checked').data('code');
		var aLLevelName =$('input:radio[name=locType]:checked').data('levelname');
		displaySmartFilters(aLocTypeCode,aLLevelName);
	}
	fnCollapseMultiselect();
}


//////////////////////
//Smart filter ended//
//////////////////////



var fnShowError=function(message){
	AJS.flag({
		type  : "error",
		title : "Error..!",
		body  : message,
		close : 'auto'
	});
}

var submitUploadFile = function(file){

	var formData = new FormData();
	formData.append( 'uploadfile',file );
	formData.append( 'school',$("#selectBox_schoolsByLoc").find("option:selected").val() );
	 
	spinner.showSpinner();
	$.ajax({
		url: "uploadTeacher",
		type: "POST",
		data: formData,
		enctype: 'multipart/form-data',
		processData: false,
		contentType: false,
		cache: false,
		success: function (response) {
			spinner.hideSpinner();
			console.log(response);
			if(response.response == "shiksha-200"){
				$('#exampleInputFile').val('');
				$("#failureMessageDiv").hide();
				$("#failureMessageDiv").empty();
				AJS.flag({
					type  : "success",
					title : "Success!",
					body  : response.responseMessage,
					close : 'auto'
				});		

			} else {
				$("#failureMessageDiv").empty();
				var msgDiv= '<div class="aui-message aui-message-error closeable" >'+
				'<p class="title"><strong><span id="errorHeader">'+response.responseMessage+'</span></strong></p>'+
				'<p class="teachers"><ul></ul> </p>'+
				'<p> <br><br>'+uploadMessages.correctAndUploadMsg+'</p>'+
				'<span class="aui-icon icon-close" role="button" id="messageCloseBtn" tabindex="0"></span> </div>';	
				$("#failureMessageDiv").append(msgDiv);
				$("#messageCloseBtn").on('click',function(){
					$("#failureMessageDiv").empty(); 
					$("#failureMessageDiv").hide();
				});

				if(response.responseMessages!=null?response.responseMessages.length>0:false){
					var item;
					$.each(response.responseMessages,function(index,value){
						item='<li>'+value+'</li>';
						$('.teachers').append(item);

					})
				}
				$("#failureMessageDiv").show();
			}
		},
		error: function (error) {
			spinner.hideSpinner();
			 fnShowError(appMessgaes.serverError);

		}
	});
}

var resetForm = function(formId){
	$(formId).find("label.error").remove();
	$(formId).find(".error").removeClass("error");
	$("#failureMessageDiv").hide();
	$("#failureMessageDiv").empty();
	$("#successMessageDiv").hide();
	$('#alertIdForClientValidate').hide();
}
$('#cancelButton').click(function () {
	$('#exampleInputFile').val('');
	resetForm("#excelUploadForm");
});
	
	
/*$("#uploadBtn").click(function(){
	var filename=	$('#exampleInputFile').val();
	var file_size = $('#exampleInputFile')[0].files[0].size;
	
	 Check input file extension. If different from “.xls” or “.xlsx” display error message 
	var fileExtensionName = filename.split('.').pop();
	if (fileExtensionName!="xlsx" ||file_size>3072000) {
		$('#alertIdForClientValidate,#alertIdForClientValidate .alert').show();
		
		return false;
	}
	if(filename.length>0){
		$(".outer-loader").show(); 
		submitUploadFile($("#exampleInputFile")[0].files[0]);
		//return true;
	}
});*/


var formValidate = function(validateFormName) {
	$(validateFormName).submit(function(e) {
		e.preventDefault();
	}).validate({
		ignore: '',
		rules: {
			uploadfile: {
				required: true,
				fileFormat:true
			},
		},
		messages: {
			uploadfile: {
				required:uploadMessages.fileRequired,
				fileFormat:uploadMessages.fileFormat.replace("@EXTENSION", ".xlsx"),
			},

		},
		errorPlacement: function(error, element) {
			$(error).insertAfter(element);
		},
		submitHandler : function(){
			submitUploadFile($("#exampleInputFile")[0].files[0]);
		}
	});
}

var fnDownloadTeacherTemplate=function(){
	var villageName=$('#selectBox_villagebyRv').find('option:selected').text();
	var schoolName=$('#selectBox_schoolsByLoc').find('option:selected').text();
	

	var excelFileName=villageName+"_"+schoolName+".xlsx";
	
	
	window.location.href=baseContextPath+'/user/download?type=teacher&excelFileName='+excelFileName;
}


///////////////////////////
$(function() {
	$('#locColspFilter').on('show.bs.collapse', function(){
		$('#locationPnlLink').text('Hide Location');
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red").removeClass("glyphicon-plus-sign").removeClass("green");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-minus-sign").addClass("red");
		
	});  
	$('#locColspFilter').on('hide.bs.collapse', function(){
		$('#locationPnlLink').text('Select Location');
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").removeClass("glyphicon-minus-sign").removeClass("red").removeClass("glyphicon-plus-sign").removeClass("green");
		$('[data-toggle="collapse"]').find(".btn-collapse").find("span").addClass("glyphicon-plus-sign").addClass("green");
	});
	/*$(window).bind("pageshow", function() {
	    var form = $('form'); 
	    form[0].reset();
	});*/
	formValidate("#excelUploadForm");
    $('#rowDiv1').show();
    $('input[name=uploadfile]').change(function() {
    	$("#failureMessageDiv").empty();
    	$('#failureMessageDiv').hide();//hide error messsage
    	$("#successMessageDiv").hide();///hide success messsage
    	resetForm("#excelUploadForm");
    	

    });
    jQuery.validator.addMethod("fileFormat", function() {

    	var filename=	$("#exampleInputFile").val();
    	var fileExtensionName = filename.split('.').pop();
		if (fileExtensionName!="xlsx") {
			return false;
		}
		return true;
	 
	}, "");

});