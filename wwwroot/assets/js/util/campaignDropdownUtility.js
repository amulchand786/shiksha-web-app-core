
//get the school location type and bind to select box
//keep the id as selectBox_schoolLocatonType
var getAllSchoolLocationType =function(){
	$select	=$('#selectBox_schoolLocatonType');	
	bindSchoolLocTypeoListBox($select,0)
};

var bindSchoolLocTypeoListBox =function(bindToElementId,selectedId){
	var jsonData = customAjaxCalling("getStateList", {}, "POST");
	$select	=bindToElementId;	
	$select.html('');
	/*$select.append('<option value="NONE" selected  hidden>--Select--</option>');*/
	var stateData = JSON.parse(jsonData);
	for (var key in stateData) {				
			if(stateData[key].stateId == selectedId)
				$select.append('<option value="' + stateData[key].stateId + '"selected>' +stateData[key].stateName + '</option>');
			else if(stateData[key].stateId!=null)
				$select.append('<option value="' + stateData[key].stateId + '">' +stateData[key].stateName + '</option>');
	}	
};

var addZoneToListBox=function(obj){	
	clearAllListBoxOnChange($('#selectBox_zonesByState'),$('#selectBox_districtsByZone'),$('#selectBox_tehsilsByDistrict'),
			$('#selectBox_blocksByTehsil'),$('#selectBox_npByBlock'),	$('#selectBox_gpByNp'),	$('#selectBox_revenueVillagebyGp'));
	getAllZoneByState(obj); //this method is from dropDownUtility.js
}
var addDistrictToListBox=function(obj){	
	clearAllListBoxOnChange($('#selectBox_districtsByZone'),$('#selectBox_tehsilsByDistrict'),$('#selectBox_blocksByTehsil'),
			$('#selectBox_npByBlock'),$('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'),$('.editVillage .district #selectBox_districtsByZone'));
	getAllDistrictByZone(obj);//this method is from dropDownUtility.js
}
var addTehsilToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_blocksByTehsil'),
			$('#selectBox_npByBlock'),$('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'));
	getAllTehsilsByDistrict(obj);
}
var addBlockToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_npByBlock'),$('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'));
	getAllBlockByTehsil(obj);
}
var addNyayPanchayatToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'));
	getAllNPByBlock(obj);
}

var addGpPanchayatToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_revenueVillagebyGp'));
	getAllGpByNp(obj);
}

var addRevenueVillageToListBox=function(obj){	
	getAllRevenueVillageByGp(obj);
}