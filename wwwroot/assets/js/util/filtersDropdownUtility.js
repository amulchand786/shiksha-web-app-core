
var addZoneToListBox=function(obj){	
	clearAllListBoxOnChange($('#selectBox_zonesByState'),$('#selectBox_districtsByZone'),$('#selectBox_tehsilsByDistrict'),
			$('#selectBox_blocksByTehsil'),$('#selectBox_npByBlock'),	$('#selectBox_gpByNp'),	$('#selectBox_revenueVillagebyGp'));
	getAllZoneByState(obj); //this method is from dropDownUtility.js
}
var addDistrictToListBox=function(obj){	
	clearAllListBoxOnChange($('#selectBox_districtsByZone'),$('#selectBox_tehsilsByDistrict'),$('#selectBox_blocksByTehsil'),
			$('#selectBox_npByBlock'),$('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'),$('.editVillage .district #selectBox_districtsByZone'));
	getAllDistrictByZone(obj);//this method is from dropDownUtility.js
}
var addTehsilToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_blocksByTehsil'),
			$('#selectBox_npByBlock'),$('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'));
	getAllTehsilsByDistrict(obj);
}
var addBlockToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_npByBlock'),$('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'));
	getAllBlockByTehsil(obj);
}
var addNyayPanchayatToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_gpByNp'),$('#selectBox_revenueVillagebyGp'));
	getAllNPByBlock(obj);
}

var addGpPanchayatToListBox=function(obj){
	clearAllListBoxOnChange($('#selectBox_revenueVillagebyGp'));
	getAllGpByNp(obj);
}

var addRevenueVillageToListBox=function(obj){	
	getAllRevenueVillageByGp(obj);
}